# Minikube K8S cluster

Follow the instructions to run the zayni framework (also an ASP.NET Core) example application in a minikube K8S cluster.

This document is only for `macOS` (10.15 version). I don't have windows and I don't use windows container, sorry.

## Git clone:

-  You will need to `git clone` or `download` the zayni framework's source code in your computer. It's free...

## Minikube Installation:

-  If you want to use **Minikube** K8S cluster to run the applications. First use `home brew` to install it.

   `brew update`

   `brew install minikube`

-  When the installation is finished. Enter `minikube` or `minikube version` command to make sure the installation is successful.

## Minikube Docker Engine:

-  Start sharing the localhost docker engine to the minikube's docker engine.<br/>
   `eval $(minikube docker-env)`

   Do **NOT** close the current terminal windows.

-  Stop sharing the localhost docker engine to the minikube's docker engine.<br/>
   `$(minikube docker-env -u)`

   Use this command when you don't want to share the localhost docker engine context to minikube.

## Get the example docker images:

-  Download the docker images from Docker Hub.

   `docker pull ponylin1985/webapi.test:alpine-x64`

   `docker pull ponylin1985/zayni.client.app:alpine-x64`

-  Rename the docker images.

   `docker tag ponylin1985/webapi.test:alpine-x64 webapi.test:alpine-x64`

   `docker tag ponylin1985/zayni.client.app:alpine-x64 zayni.client.app:alpine-x64`

   `docker rmi ponylin1985/webapi.test:alpine-x64`

   `docker rmi ponylin1985/zayni.client.app:alpine-x64`

## Deploy containers into K8S (minikube) cluster.

### Run the **server-side** K8S pod.

   -  Start the minikube K8S cluster and mount the localhost directory into minikube's VM.
      -  You can use the following command to start a minikube cluster and mount the **localhost path into minikube's VM**.
      
         `minikube start && minikube mount {{localhost_path}}:{{minikube's_VM_path}}`

         <font color="red">But DO NOT close the current terminal window until you want to close the minikube K8S cluster.</font> 

      -  You **won't** need to do this if you are using a real **K8S** such as AWS EKS, GKE or Azure AKS.

      -  You can reference the `start-minikube.sh` as an example.
         -  Mount the localhost path: `/Users/pony`. (Which is my macOS' home directoy.)
         -  Into the minikube's VM in `/MyFiles` directory.
         -  That will mount **all my directories and files from my macOS into the minikube's VM in `/MyFiles` directory.**

      -  Once you done that. You can use `minikube ssh` command to observe inside the minikube's VM and check is the volume mounting is correctly.

   -  Create the _pod_ by the following CLI.
      
      -  `cd ./K8S/webapi-test`

      -  Open the `deployment.yaml` file with your editor.

      -  Change the `volumes.hostPath.path` section.

         ```yml
         volumes:
         - name: configs
           hostPath:
             path: "{{localhost_path}}/K8S/webapi-test/configs"
             type: DirectoryOrCreate
         - name: logs
           hostPath:
             path: "{{localhost_path}}/ApplicationLog/WebAPI.Test"
             type: DirectoryOrCreate
         ```

      -  Create the K8S _deployment_ and _pod_.

         `kubectl create -f ./deployment.yaml`

   -  Create the _service_ by the following CLI.

      `cd ./K8S/webapi-test`

      `kubectl create -f ./service.yaml`

   -  Use the following CLI for troubleshooting.

      `kubectl get deployments`

      `kubectl get pods`

      `kubectl get services`

      `kubectl get pod {{pod_name}}`

      `kubectl describe pod {{pod_name}}`

      `kubectl describe service {{service_name}}`

      `kubectl logs {{pod_name}}`

      `kubectl exec -it {{pod_name}} sh`

   -  Get the minikube IP address.

      `minikube ip`


### Run the **client-side** K8S pod.

   -  Make sure you were starting and mount the localhost path correctly.

   -  Create the _pod_ in using CLI.
      
      -  `cd ./K8S/service-host-clientside`

      -  Open the `deployment.yaml` file with your editor.

      -  Change the `volumes.hostPath.path` section.

         ```yml
         volumes:
         - name: "app-config"
           hostPath:
             path: "{{localhost_path}}/K8S/service-host-clientside/configs/app.config"
             type: FileOrCreate
         - name: "service-client-config"
           hostPath:
             path: "{{localhost_path}}/K8S/service-host-clientside/configs/serviceClientConfig.json"
             type: FileOrCreate
         - name: logs
           hostPath:
             path: "{{localhost_path}}/ApplicationLog/ServiceHost.ClientSide.ConsoleApp"
             type: DirectoryOrCreate
         ```

      -  Create the K8S _deployment_ and _pod_.

         `kubectl create -f ./deployment.yaml`

   -  Use the following CLI for troubleshooting.

      `kubectl get deployments`

      `kubectl get pods`

      `kubectl get pod {{pod_name}}`

      `kubectl describe pod {{pod_name}}`

      `kubectl logs {{pod_name}}`

      `kubectl exec -it {{pod_name}} sh`


## Test

### Request from outside K8S.

   -  You can use **postman** or **curl** to test the _webapi-test_ pod.
      
      ```curl
      curl -X POST \
         http://{{Your Minikube IP address here}}:30001/values \
         -H 'Accept: */*' \
         -H 'Accept-Encoding: gzip, deflate' \
         -H 'Cache-Control: no-cache' \
         -H 'Connection: keep-alive' \
         -H 'Content-Length: 149' \
         -H 'Content-Type: application/json' \
         -H 'Host: {{Your Minikube IP address here}}:30001' \
         -H 'User-Agent: PostmanRuntime/7.20.1' \
         -H 'cache-control: no-cache' \
         -d '{
            "someMsg": "Happ Bear!!! XumIHL07Yl",
            "someDate": "2019-03-27",
            "magicNumber": 6679.2562,
            "luckyNumber": 46,
            "isSuperXMan": false
         }'
      ```

### Request from inside K8S.

   -  The following also demo how the zayni framework's `Middle.Service` module using `gRPC` or `RabbitMQ` protocol to communicate between each service hosts.

   -  Use the following commands.

      -  Go into the docker container.<br/>
         `kubectl exec -it {{service-host-client pod name here}} sh`

      -  Use the telnet to connect the zayni framework's remote CLI service host.<br/>
         `telnet localhost 2223`

         If the `telnet` was not installed, use `apk add busybox-extras` command to install it.

      -  Enter the password. Which is **Admin123**. (This is just a demo...)

      -  Now are connecting the zayni framework's command service host now! First create a service host connection between the `webapi-test` pod.
      
      -  Send a RPC request to the `webapi-test` pod.<br/>
         
         `send magic action`

         Now you are using the `gRPC` to send a RPC to the `webapi-test` inside the minikube K8S cluster.


      Have a nice day.

-  Reference:<br/>
  [ASP.NET Core run on Minikube](https://itnext.io/running-asp-net-core-on-minikube-ad69472c4c95)