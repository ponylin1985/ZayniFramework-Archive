#!/bin/bash

# Start the minikube K8S cluster and mount the localhost path into minikube VM.
# Do NOT close this terminal window until you want to close the minikube K8S cluster.
# minikube start && minikube mount /Users/pony:/MyFiles
minikube start --cpus 2 --memory 6000 && minikube mount /Users/pony:/MyFiles