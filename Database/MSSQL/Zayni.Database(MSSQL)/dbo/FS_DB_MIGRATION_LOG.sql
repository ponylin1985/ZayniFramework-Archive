﻿CREATE TABLE [dbo].[FS_DB_MIGRATION_LOG] (
    [LOG_SRNO]         INT             IDENTITY (1, 1) NOT NULL,
    [SCRIPT]           NVARCHAR (255)  NOT NULL,
    [SCRIPT_TYPE]      NVARCHAR (10)   NOT NULL,
    [SCRIPT_VERSION]   NVARCHAR (50)   NOT NULL,
    [SCRIPT_TIMESTAMP] DATETIME        NOT NULL,
    [SCRIPT_CONTENT]   NTEXT           NULL,
    [IS_SUCCESS]       BIT             NOT NULL,
    [MESSAGE]          NVARCHAR (1024) NULL,
    [LOG_TIME]         DATETIME        NOT NULL,
    PRIMARY KEY CLUSTERED ([LOG_SRNO] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'執行日誌時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION_LOG', @level2type = N'COLUMN', @level2name = N'LOG_TIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'執行 migration 腳本的結果訊息', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION_LOG', @level2type = N'COLUMN', @level2name = N'MESSAGE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'執行 migration 腳本結果是否成功', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION_LOG', @level2type = N'COLUMN', @level2name = N'IS_SUCCESS';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資料庫 migration 腳本內容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION_LOG', @level2type = N'COLUMN', @level2name = N'SCRIPT_CONTENT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資料庫 migration 腳本的時間戳記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION_LOG', @level2type = N'COLUMN', @level2name = N'SCRIPT_TIMESTAMP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資料庫 migration 腳本的版本號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION_LOG', @level2type = N'COLUMN', @level2name = N'SCRIPT_VERSION';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資料庫 migration 腳本類型: upgrade、downgrade', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION_LOG', @level2type = N'COLUMN', @level2name = N'SCRIPT_TYPE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資料庫 migration 腳本檔案名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION_LOG', @level2type = N'COLUMN', @level2name = N'SCRIPT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌記錄流水號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION_LOG', @level2type = N'COLUMN', @level2name = N'LOG_SRNO';

