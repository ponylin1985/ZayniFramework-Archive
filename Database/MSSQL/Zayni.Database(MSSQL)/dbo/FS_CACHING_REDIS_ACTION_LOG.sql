﻿CREATE TABLE [dbo].[FS_CACHING_REDIS_ACTION_LOG] (
    [LOG_SRNO]     BIGINT         IDENTITY (1, 1) NOT NULL,
    [REDIS_HOST]   NVARCHAR (100) NOT NULL,
    [REDIS_PORT]   INT            NOT NULL,
    [REQUEST_ID]   NVARCHAR (36)  DEFAULT ((0)) NOT NULL,
    [DIRECTION]    INT            NOT NULL,
    [ACTION]       NVARCHAR (100) NOT NULL,
    [DATA_CONTENT] TEXT           NULL,
    [MESSAGE]      NVARCHAR (500) NULL,
    [LOG_TIME]     DATETIME       NOT NULL,
    PRIMARY KEY CLUSTERED ([LOG_SRNO] ASC)
);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'原始請求識別代碼', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_CACHING_REDIS_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'REQUEST_ID';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Redis Server 的連接阜號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_CACHING_REDIS_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'REDIS_PORT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Redis Server 服務的位址', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_CACHING_REDIS_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'REDIS_HOST';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'框架內部訊息', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_CACHING_REDIS_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'MESSAGE';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌時間', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_CACHING_REDIS_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'LOG_TIME';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'日誌流水號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_CACHING_REDIS_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'LOG_SRNO';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_CACHING_REDIS_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'DIRECTION';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'資料內容', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_CACHING_REDIS_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'DATA_CONTENT';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'動作名稱', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_CACHING_REDIS_ACTION_LOG', @level2type = N'COLUMN', @level2name = N'ACTION';

