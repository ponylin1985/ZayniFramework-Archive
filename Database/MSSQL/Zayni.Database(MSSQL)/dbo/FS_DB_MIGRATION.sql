﻿CREATE TABLE [dbo].[FS_DB_MIGRATION] (
    [CURRENT_SCRIPT]           NVARCHAR (255) NOT NULL,
    [CURRENT_SCRIPT_VERSION]   NVARCHAR (50)  NOT NULL,
    [CURRENT_SCRIPT_TIMESTAMP] DATETIME       NOT NULL
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'目前資料庫版本的時間戳記', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION', @level2type = N'COLUMN', @level2name = N'CURRENT_SCRIPT_TIMESTAMP';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'目前資料庫 migration 腳本的版本號', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION', @level2type = N'COLUMN', @level2name = N'CURRENT_SCRIPT_VERSION';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'目前資料庫版本的腳本', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'FS_DB_MIGRATION', @level2type = N'COLUMN', @level2name = N'CURRENT_SCRIPT';

