﻿CREATE TABLE [dbo].[FS_UNIT_TEST_USER] (
    [ACCOUNT_ID] NVARCHAR (50) NOT NULL,
    [NAME]      NVARCHAR (30) NOT NULL,
    [AGE]       INT           NULL,
    [SEX]       INT           NULL,
    [BIRTHDAY]  DATETIME      NULL,
    [IS_VIP]     BIT           DEFAULT ((0)) NULL,
    [IS_GOOD]    BIT           DEFAULT ((0)) NULL,
    [DATA_FLAG]  ROWVERSION    NOT NULL,
    PRIMARY KEY CLUSTERED ([ACCOUNT_ID] ASC)
);

