-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: zayni
-- Generation Time: 2020-01-29 00:55:09.4450
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."FS_DB_MIGRATION";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."FS_DB_MIGRATION" (
    "CURRENT_SCRIPT" varchar(255) NOT NULL,
    "CURRENT_SCRIPT_VERSION" varchar(50) NOT NULL,
    "CURRENT_SCRIPT_TIMESTAMP" timestamptz NOT NULL
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_DB_MIGRATION"."CURRENT_SCRIPT" IS '目前資料庫版本的腳本';
COMMENT ON COLUMN "public"."FS_DB_MIGRATION"."CURRENT_SCRIPT_VERSION" IS '目前資料庫 migration 腳本的版本號';
COMMENT ON COLUMN "public"."FS_DB_MIGRATION"."CURRENT_SCRIPT_TIMESTAMP" IS '目前資料庫版本的時間戳記';

