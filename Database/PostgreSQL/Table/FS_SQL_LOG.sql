-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: zayni
-- Generation Time: 2020-01-29 00:57:29.6160
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."FS_SQL_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_SQL_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_SQL_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_SQL_LOG_LOG_SRNO_seq"'::regclass),
    "DAO_NAME" varchar(100),
    "COMMAND_ID" varchar(20),
    "COMMAND_DIRECTION" int4,
    "COMMAND_TYPE" int4,
    "COMMAND_TEXT" text,
    "COMMAND_RESULT" text,
    "HOST_NAME" varchar(100) NOT NULL,
    "LOG_TIME" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("LOG_SRNO")
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_SQL_LOG"."LOG_SRNO" IS 'SQL日誌紀錄流水號';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."DAO_NAME" IS '資料存取物件的類別名稱';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_ID" IS 'SQL指令的識別代碼';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_DIRECTION" IS 'SQL指令的執行方向，1: 請求指令，2: 執行指令的結果。';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_TYPE" IS 'SQL指令的型態，1: CommandText，2: Stored Procedure';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_TEXT" IS 'SQL指令的語法';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_RESULT" IS 'SQL指令的執行結果';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."HOST_NAME" IS '電腦名稱或IP位址';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."LOG_TIME" IS '紀錄時間';

