-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: zayni
-- Generation Time: 2020-01-29 00:54:34.8730
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."FS_CACHING_REDIS_ACTION_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_CACHING_REDIS_ACTION_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_CACHING_REDIS_ACTION_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_CACHING_REDIS_ACTION_LOG_LOG_SRNO_seq"'::regclass),
    "REDIS_HOST" varchar(100) NOT NULL,
    "REDIS_PORT" int4 NOT NULL,
    "REQUEST_ID" varchar(36) NOT NULL DEFAULT '0'::character varying,
    "DIRECTION" int4 NOT NULL DEFAULT 1,
    "ACTION" varchar(100) NOT NULL DEFAULT ' '::character varying,
    "DATA_CONTENT" text,
    "MESSAGE" varchar,
    "LOG_TIME" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("LOG_SRNO")
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."LOG_SRNO" IS '日誌流水號';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."REDIS_HOST" IS 'Redis Server 服務的位址';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."REDIS_PORT" IS 'Redis Server 的連接阜號';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."REQUEST_ID" IS '原始請求識別代碼';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."DIRECTION" IS '動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."ACTION" IS '動作名稱';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."DATA_CONTENT" IS '資料內容';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."MESSAGE" IS '框架內部訊息';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."LOG_TIME" IS '日誌時間';

