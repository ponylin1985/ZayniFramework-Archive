-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: zayni
-- Generation Time: 2020-01-29 00:56:54.5920
-- -------------------------------------------------------------


DROP TABLE IF EXISTS "public"."FS_EVENT_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_EVENT_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_EVENT_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_EVENT_LOG_LOG_SRNO_seq"'::regclass),
    "TEXT_LOGGER" varchar(100) NOT NULL DEFAULT NULL::character varying,
    "EVENT_LEVEL" varchar(30) NOT NULL DEFAULT 'NULL::character varying'::character varying,
    "EVENT_TITLE" varchar(500) DEFAULT NULL::character varying,
    "EVENT_MESSAGE" varchar(5120) DEFAULT 'NULL'::character varying,
    "HOST" varchar(100) DEFAULT 'NULL'::character varying,
    "LOG_TIME" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("LOG_SRNO")
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."LOG_SRNO" IS '日誌紀錄流水號';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."TEXT_LOGGER" IS '日誌器設定名稱';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."EVENT_LEVEL" IS '日誌事件層級';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."EVENT_TITLE" IS '日誌事件標題';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."EVENT_MESSAGE" IS '日誌事件訊息';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."HOST" IS '寫入日誌的主機位址';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."LOG_TIME" IS '日誌時間';

