-- -------------------------------------------------------------
-- TablePlus 3.1.0(290)
--
-- https://tableplus.com/
--
-- Database: zayni
-- Generation Time: 2020-01-29 00:46:27.6860
-- -------------------------------------------------------------




DROP TABLE IF EXISTS "public"."FS_CACHING_REDIS_ACTION_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_CACHING_REDIS_ACTION_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_CACHING_REDIS_ACTION_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_CACHING_REDIS_ACTION_LOG_LOG_SRNO_seq"'::regclass),
    "REDIS_HOST" varchar(100) NOT NULL,
    "REDIS_PORT" int4 NOT NULL,
    "REQUEST_ID" varchar(36) NOT NULL DEFAULT '0'::character varying,
    "DIRECTION" int4 NOT NULL DEFAULT 1,
    "ACTION" varchar(100) NOT NULL DEFAULT ' '::character varying,
    "DATA_CONTENT" text,
    "MESSAGE" varchar,
    "LOG_TIME" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("LOG_SRNO")
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."LOG_SRNO" IS '日誌流水號';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."REDIS_HOST" IS 'Redis Server 服務的位址';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."REDIS_PORT" IS 'Redis Server 的連接阜號';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."REQUEST_ID" IS '原始請求識別代碼';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."DIRECTION" IS '動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."ACTION" IS '動作名稱';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."DATA_CONTENT" IS '資料內容';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."MESSAGE" IS '框架內部訊息';
COMMENT ON COLUMN "public"."FS_CACHING_REDIS_ACTION_LOG"."LOG_TIME" IS '日誌時間';

DROP TABLE IF EXISTS "public"."FS_DB_MIGRATION";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."FS_DB_MIGRATION" (
    "CURRENT_SCRIPT" varchar(255) NOT NULL,
    "CURRENT_SCRIPT_VERSION" varchar(50) NOT NULL,
    "CURRENT_SCRIPT_TIMESTAMP" timestamptz NOT NULL
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_DB_MIGRATION"."CURRENT_SCRIPT" IS '目前資料庫版本的腳本';
COMMENT ON COLUMN "public"."FS_DB_MIGRATION"."CURRENT_SCRIPT_VERSION" IS '目前資料庫 migration 腳本的版本號';
COMMENT ON COLUMN "public"."FS_DB_MIGRATION"."CURRENT_SCRIPT_TIMESTAMP" IS '目前資料庫版本的時間戳記';

DROP TABLE IF EXISTS "public"."FS_DB_MIGRATION_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_DB_MIGRATION_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_DB_MIGRATION_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_DB_MIGRATION_LOG_LOG_SRNO_seq"'::regclass),
    "SCRIPT" varchar(255) NOT NULL,
    "SCRIPT_TYPE" varchar(10),
    "SCRIPT_VERSION" varchar(50),
    "SCRIPT_CONTENT" text,
    "IS_SUCCESS" bool NOT NULL,
    "MESSAGE" varchar(1024),
    "LOG_TIME" timestamptz NOT NULL DEFAULT now()
);

DROP TABLE IF EXISTS "public"."FS_EVENT_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_EVENT_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_EVENT_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_EVENT_LOG_LOG_SRNO_seq"'::regclass),
    "TEXT_LOGGER" varchar(100) NOT NULL DEFAULT NULL::character varying,
    "EVENT_LEVEL" varchar(30) NOT NULL DEFAULT 'NULL::character varying'::character varying,
    "EVENT_TITLE" varchar(500) DEFAULT NULL::character varying,
    "EVENT_MESSAGE" varchar(5120) DEFAULT 'NULL'::character varying,
    "HOST" varchar(100) DEFAULT 'NULL'::character varying,
    "LOG_TIME" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("LOG_SRNO")
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."LOG_SRNO" IS '日誌紀錄流水號';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."TEXT_LOGGER" IS '日誌器設定名稱';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."EVENT_LEVEL" IS '日誌事件層級';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."EVENT_TITLE" IS '日誌事件標題';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."EVENT_MESSAGE" IS '日誌事件訊息';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."HOST" IS '寫入日誌的主機位址';
COMMENT ON COLUMN "public"."FS_EVENT_LOG"."LOG_TIME" IS '日誌時間';

DROP TABLE IF EXISTS "public"."FS_SERVICE_CLIENT_ACTION_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_SERVICE_CLIENT_ACTION_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_SERVICE_CLIENT_ACTION_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_SERVICE_CLIENT_ACTION_LOG_LOG_SRNO_seq"'::regclass),
    "REQUEST_ID" bpchar(15) DEFAULT NULL::bpchar,
    "CLIENT_NAME" varchar(255) DEFAULT NULL::character varying,
    "CLIENT_HOST" varchar(20) DEFAULT NULL::character varying,
    "REMOTE_NAME" varchar(255) DEFAULT 'NULL::character varying'::character varying,
    "REMOTE_HOST" varchar(1024) DEFAULT 'NULL::character varying'::character varying,
    "DIRECTION" int2 NOT NULL,
    "ACTION_NAME" varchar(100) NOT NULL,
    "DATA_CONTENT" text,
    "REQUEST_TIME" timestamptz,
    "RESPONSE_TIME" timestamptz,
    "IS_SUCCESS" bool,
    "CODE" varchar(100),
    "MESSAGE" varchar(500),
    "LOG_TIME" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("LOG_SRNO")
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."LOG_SRNO" IS '日誌流水號';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."REQUEST_ID" IS '請求代碼，若 Direction = 0，RequestId 可以允許為 DBNull 或空字串。';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."CLIENT_NAME" IS '請求客戶端的設定名稱';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."CLIENT_HOST" IS '請求客戶端的主機位址';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."REMOTE_NAME" IS '遠端服務的設定名稱';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."REMOTE_HOST" IS '遠端服務的主機位址';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."DIRECTION" IS '動作方向，1: Request，2: Response，3: PublishMessage，4: ReceiveMessage。';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."ACTION_NAME" IS '動作方法名稱';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."DATA_CONTENT" IS '資料內容 (JSON字串格式)';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."REQUEST_TIME" IS '原始請求時間，當 Direction = 1 或 3 時才有資料。';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."RESPONSE_TIME" IS '服務回應的時間，當 Direction = 2 或 4 時才有資料。';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."IS_SUCCESS" IS '服務執行動作是否成功';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."CODE" IS '服務回應的代碼';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."MESSAGE" IS '動作的訊息';
COMMENT ON COLUMN "public"."FS_SERVICE_CLIENT_ACTION_LOG"."LOG_TIME" IS '日誌時間';

DROP TABLE IF EXISTS "public"."FS_SERVICE_HOST_ACTION_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_SERVICE_HOST_ACTION_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_SERVICE_HOST_ACTION_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_SERVICE_HOST_ACTION_LOG_LOG_SRNO_seq"'::regclass),
    "REQUEST_ID" bpchar(15),
    "SERVICE_NAME" varchar(100),
    "SERVICE_HOST" varchar(20),
    "DIRECTION" int2 NOT NULL,
    "ACTION_NAME" varchar(65) NOT NULL,
    "DATA_CONTENT" text,
    "REQUEST_TIME" timestamptz,
    "RESPONSE_TIME" timestamptz,
    "IS_SUCCESS" bool,
    "CODE" varchar(100),
    "MESSAGE" varchar(500),
    "LOG_TIME" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("LOG_SRNO")
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."LOG_SRNO" IS '日誌流水號';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."REQUEST_ID" IS '請求代碼，若 Direction = 0，RequestId 可以允許為 DBNull 或空字串。';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."SERVICE_NAME" IS '服務設定名稱';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."SERVICE_HOST" IS '服務主機位址';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."DIRECTION" IS '動作方向，1: Request，2: Response，3: ReceiveMessage，4: PublishMessage。';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."ACTION_NAME" IS '動作方法名稱';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."DATA_CONTENT" IS '資料內容 (JSON字串格式)';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."REQUEST_TIME" IS '原始請求時間，當 Direction = 1或 4 時才有資料。';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."RESPONSE_TIME" IS '服務回應的時間，當 Direction = 2 或 3 時才有資料。';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."IS_SUCCESS" IS '服務執行動作是否成功';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."CODE" IS '服務回應的代碼';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."MESSAGE" IS '動作的訊息';
COMMENT ON COLUMN "public"."FS_SERVICE_HOST_ACTION_LOG"."LOG_TIME" IS '日誌時間';

DROP TABLE IF EXISTS "public"."FS_SQL_LOG";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Sequence and defined type
CREATE SEQUENCE IF NOT EXISTS "FS_SQL_LOG_LOG_SRNO_seq";

-- Table Definition
CREATE TABLE "public"."FS_SQL_LOG" (
    "LOG_SRNO" int8 NOT NULL DEFAULT nextval('"FS_SQL_LOG_LOG_SRNO_seq"'::regclass),
    "DAO_NAME" varchar(100),
    "COMMAND_ID" varchar(20),
    "COMMAND_DIRECTION" int4,
    "COMMAND_TYPE" int4,
    "COMMAND_TEXT" text,
    "COMMAND_RESULT" text,
    "HOST_NAME" varchar(100) NOT NULL,
    "LOG_TIME" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("LOG_SRNO")
);

-- Column Comment
COMMENT ON COLUMN "public"."FS_SQL_LOG"."LOG_SRNO" IS 'SQL日誌紀錄流水號';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."DAO_NAME" IS '資料存取物件的類別名稱';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_ID" IS 'SQL指令的識別代碼';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_DIRECTION" IS 'SQL指令的執行方向，1: 請求指令，2: 執行指令的結果。';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_TYPE" IS 'SQL指令的型態，1: CommandText，2: Stored Procedure';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_TEXT" IS 'SQL指令的語法';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."COMMAND_RESULT" IS 'SQL指令的執行結果';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."HOST_NAME" IS '電腦名稱或IP位址';
COMMENT ON COLUMN "public"."FS_SQL_LOG"."LOG_TIME" IS '紀錄時間';

DROP TABLE IF EXISTS "public"."FS_UNIT_TEST_USER";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."FS_UNIT_TEST_USER" (
    "ACCOUNT_ID" varchar(50) NOT NULL,
    "NAME" varchar(50) NOT NULL,
    "AGE" int4 NOT NULL,
    "SEX" int4,
    "BIRTHDAY" timestamptz,
    "IS_VIP" bool,
    "IS_GOOD" bool,
    "DATA_FLAG" timestamptz NOT NULL DEFAULT now(),
    PRIMARY KEY ("ACCOUNT_ID")
);

DROP TABLE IF EXISTS "public"."FS_UNIT_TEST_USER2";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."FS_UNIT_TEST_USER2" (
    "ACCOUNT_ID" varchar(50) NOT NULL,
    "USER_ID" varchar(50) NOT NULL,
    "USER_NAME" varchar(50) NOT NULL,
    "USER_PASSWORD" varchar(50),
    "BIRTHDAY" timestamptz,
    "SEX" int4 NOT NULL,
    "MONEY" float8,
    "DESCRIPTION" varchar(50),
    PRIMARY KEY ("ACCOUNT_ID","USER_ID")
);

DROP TABLE IF EXISTS "public"."FS_UNIT_TEST_USER_PHONE_DETAIL";
-- This script only contains the table creation statements and does not fully represent the table in the database. It's still missing: indices, triggers. Do not use it as a backup.

-- Table Definition
CREATE TABLE "public"."FS_UNIT_TEST_USER_PHONE_DETAIL" (
    "ACCOUNT_ID" varchar(50) NOT NULL,
    "PHONE_TYPE" varchar(50) NOT NULL,
    "PHONE_NUMBER" varchar(50),
    PRIMARY KEY ("ACCOUNT_ID","PHONE_TYPE")
);

CREATE OR REPLACE FUNCTION public."REFRESH_DATAFLAG"()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
  NEW."DATA_FLAG" = NOW();
  RETURN NEW;
END;
$function$;


CREATE TRIGGER "TRG_USER_SET_DATAFALG"
BEFORE INSERT OR UPDATE ON "FS_UNIT_TEST_USER"
FOR EACH ROW
EXECUTE PROCEDURE "REFRESH_DATAFLAG"();


INSERT INTO "FS_UNIT_TEST_USER" ("ACCOUNT_ID", "NAME", "AGE", "SEX", "BIRTHDAY", "IS_VIP", "IS_GOOD")
VALUES ('Dexter Gordon', 'Dexter Gordon', 69, 1, '1942-07-30 00:48:26.000', true, false);

INSERT INTO "FS_UNIT_TEST_USER" ("ACCOUNT_ID", "NAME", "AGE", "SEX", "BIRTHDAY", "IS_VIP", "IS_GOOD")
VALUES ('Jaco Pastorius', 'Jaco Pastorius', 36, 1, '1970-11-23 00:47:18.000', false, false);

INSERT INTO "FS_UNIT_TEST_USER" ("ACCOUNT_ID", "NAME", "AGE", "SEX", "BIRTHDAY", "IS_VIP", "IS_GOOD")
VALUES ('Judy Bitch', 'Judy Bitch', 19, 0, '2000-05-24 00:45:21.000', true, false);

INSERT INTO "FS_UNIT_TEST_USER" ("ACCOUNT_ID", "NAME", "AGE", "SEX", "BIRTHDAY", "IS_VIP", "IS_GOOD")
VALUES ('Nancy Gili', 'Nancy Gili', 24, 0, '1998-08-06 00:46:22.000', true, true);

INSERT INTO "FS_UNIT_TEST_USER" ("ACCOUNT_ID", "NAME", "AGE", "SEX", "BIRTHDAY", "IS_VIP", "IS_GOOD")
VALUES ('WebAPI.Test.CacheManagerTester', 'WebAPI.Test.CacheManagerTester', 40, 1, '1980-12-12 00:14:14.000', true, true);