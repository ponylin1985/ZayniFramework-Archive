@echo off

rem 注意，要手動在 terminal 執行 docker build 指令，需要在 ZayniFramework 專案的根目錄下執行

rem 建置 WebAPI.Test 專案的 docker image
docker build --file ./Test/WebAPI.Test/Dockerfile . -t webapi.test:alpine-x64

rem 建置 ServiceHost.ServerSide.App 專案的 docker image
docker build --file ./Test/ServiceHostTest/ServiceHost.ServerSide.ConsoleApp/Dockerfile . -t zayni.server.app:alpine-x64

rem 建置 ServiceHost.Client.App 專案的 docker image
docker build --file ./Test/ServiceHostTest/ServiceHost.Client.ConsoleApp/Dockerfile . -t zayni.client.app:alpine-x64

rem 建置 Remote.ShareData.Service 遠端快取服務的 docker image
docker build --file ./Source/Remote.ShareData.ServiceApp/Dockerfile . -t zayni.sharedata.service:alpine-x64