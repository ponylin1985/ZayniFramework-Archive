﻿using System;
using ZayniFramework.Logging;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>文字日誌例外處理器
    /// </summary>
    public class TextLoggingHandler : IExceptionHandler
    {
        /// <summary>進行程式例外處理
        /// </summary>
        /// <param name="model">資料集合</param>
        public void HandleException( dynamic model )
        {
            string    path      = model.LogPath;
            string    title     = model.LogTitle;
            string    message   = model.LogMessage;
            Exception exception = model.Exception;

            var textLogEntry = new LogMetadata
            {
                IsEnable        = true,
                LogPath         = path,
                Title           = title,
                Message         = exception.Message,
                ExceptionObject = exception
            };

            try
            {
                EventMessageLogger.Instance.WriteLog( textLogEntry );
            }
            catch ( Exception ex )
            {
                throw ex;
            }
        }
    }
}
