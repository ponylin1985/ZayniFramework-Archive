﻿using System;
using System.Diagnostics;
using ZayniFramework.Logging;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>Fusoin框架的事件檢視器日誌例外處理器
    /// </summary>
    public class EventLoggingHandler : IExceptionHandler
    {
        /// <summary>執行例外處理
        /// </summary>
        /// <param name="model">資料集合</param>
        public void HandleException( dynamic model )
        {
            string    title     = model.LogTitle;
            string    message   = model.LogMessage;
            bool      isEnable  = model.IsEnable;
            Exception exception = model.Exception;

            try
            {
                EventLogger.WriteLog( title, message, EventLogEntryType.Error, isEnable, exception );
            }
            catch ( Exception ex )
            {
                throw ex;
            }
        }
    }
}
