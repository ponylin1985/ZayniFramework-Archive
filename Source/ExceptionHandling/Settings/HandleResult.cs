﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>執行例外處理政策結果
    /// </summary>
    public class HandleResult : BaseResult
    {
        /// <summary>是否需要將程式例外重新拋出
        /// </summary>
        public bool NeedRethrow { get; set; }

        /// <summary>程式例外物件
        /// </summary>
        public new Exception ExceptionObject { get; set; }
    }
}
