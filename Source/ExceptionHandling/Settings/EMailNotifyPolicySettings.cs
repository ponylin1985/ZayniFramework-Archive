﻿namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ZayniFramework 框架電子郵件通知例外處理政策設定值
    /// </summary>
    public static class EMailNotifyPolicySettings
    {
        /// <summary>是否啟用文字日誌例外處理
        /// </summary>
        public static bool IsTextLogEnable { get; internal set; }

        /// <summary>是否需要將程式異常重新拋出 (由Config檔中設定)
        /// </summary>
        public static bool NeedRethrow { get; internal set; }

        /// <summary>文字日誌檔案路徑
        /// </summary>
        public static string TextLogPath { get; internal set; }

        /// <summary>是否啟用事件檢視器記錄處理
        /// </summary>
        public static bool IsEventLogEnable { get; internal set; }

        /// <summary>是否啟用EMail電子郵件通知處理
        /// </summary>
        public static bool IsEMailNotifyEnable { get; internal set; }

        /// <summary>SMTP通訊協定阜號
        /// </summary>
        public static int SmtpPort { get; internal set; }

        /// <summary>SMTP Server伺服器主機名稱或IP位址
        /// </summary>
        public static string SmtpHost { get; internal set; }

        /// <summary>是否啟用SSL連線
        /// </summary>
        public static bool IsEnableSsl { get; internal set; }

        /// <summary>SMTP Server連線的網域
        /// </summary>
        public static string SmtpDomain { get; internal set; }

        /// <summary>SMTP帳號
        /// </summary>
        
        public static string SmtpAccount { get; internal set; }

        /// <summary>SMTP密碼
        /// </summary>
        public static string SmtpPassword { get; internal set; }

        /// <summary>寄件人電子郵件信箱地址
        /// </summary>
        public static string FromEmailAddress { get; internal set; }

        /// <summary>寄件人顯示名稱
        /// </summary>
        public static string FromDisplayName { get; internal set; }

        /// <summary>收件人電子郵件信箱地址
        /// </summary>
        public static string ToEmailAddress { get; internal set; }

        /// <summary>電子郵件主旨
        /// </summary>
        public static string MailSubject { get; internal set; }
    }
}
