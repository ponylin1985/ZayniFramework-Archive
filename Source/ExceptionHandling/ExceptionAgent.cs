﻿using ZayniFramework.Common;
using ZayniFramework.Logging;
using System;
using System.Collections.Generic;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ZayniFramework 框架的程式例外處理專員
    /// </summary>
    public static class ExceptionAgent
    {
        #region 宣告私有的靜態欄位

        /// <summary>ZayniFramework 框架預設的例外處理政策
        /// </summary>
        private static string DEFAULT_POLICY_NAME = "_ZayniDefaultPolicy_";

        /// <summary>ZayniFramework 框架異常Email通知信處理政策
        /// </summary>
        private static string EMAIL_POLICY_NAME = "EMailNotifyPolicy";

        /// <summary>程式例外處理政策池
        /// </summary>
        private static Dictionary<string, ExceptionPolicy> _policies = new Dictionary<string, ExceptionPolicy>();

        #endregion 宣告私有的靜態欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static ExceptionAgent() 
        {
            Initialize();
        }

        #endregion 宣告靜態建構子


        #region 宣告私有的靜態方法

        /// <summary>初始化
        /// </summary>
        private static void Initialize()
        {
            AddCustomPolicies();
            CreateEMailNotifyPolicy();
            CreateDefaultPolicy();
        }

        /// <summary>建立使用者自行定義的客製化例外處理政策
        /// </summary>
        private static void AddCustomPolicies()
        {
            var policiesConfig = ConfigReader.LoadCustomPoliciesSettings();

            foreach ( var c in policiesConfig )
            {
                #region 建立例外處理政策

                var config = (CustomPolicyElement)c;

                var policy = ExceptionPolicyFactory.Create( "custom", config );

                if ( policy.IsNull() )
                {
                    Logger.WriteErrorLog( "ExceptionAgent", "建立客製化例外處理政策失敗，請檢查Config檔中的CustomPolicies設定區段是否正確", "ExceptionAgent.AddCustomPolicy" );
                    return;
                }

                #endregion 建立例外處理政策

                #region 將例外處理政策加入例外處理池中

                try
                {
                    _policies.Add( policy.PolicyName, policy );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( "ExceptionAgent", ex, "ExceptionAgent.AddCustomPolicy ZayniFramework 框架加入例外處理政策{0}發生異常: {1}".FormatTo( policy.PolicyName, ex.ToString() ) );
                }

                #endregion 將例外處理政策加入例外處理池中
            }
        }

        /// <summary>建立ZayniFramework 框架預設的例外處理政策
        /// </summary>
        private static void CreateDefaultPolicy()
        {
            ExceptionPolicy policy = ExceptionPolicyFactory.Create( "defaulttextlog" );

            try
            {
                _policies.Add( DEFAULT_POLICY_NAME, policy );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( "ExceptionAgent", ex, "ExceptionAgent.CreateDefaultPolicy ZayniFramework 框架預設例外處理政策發生異常: {0}".FormatTo( ex.ToString() ) );
            }
        }

        /// <summary>建立ZayniFramework 框架的異常通知信處理政策
        /// </summary>
        private static void CreateEMailNotifyPolicy()
        {
            ExceptionPolicy policy = ExceptionPolicyFactory.Create( "emailnotify" );

            try
            {
                _policies.Add( EMAIL_POLICY_NAME, policy );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( "ExceptionAgent", ex, "ExceptionAgent.CreateDefaultPolicy ZayniFramework 框架異常Email通知信處理政策發生異常: {0}".FormatTo( ex.ToString() ) );
            }
        }

        /// <summary>執行例外處理政策的例外處理動作
        /// </summary>
        /// <param name="policy">例外處理政策</param>
        /// <param name="exception">程式例外物件</param>
        /// <param name="sender">處發例外事件的物件</param>
        /// <param name="title">事件訊息</param>
        /// <returns>是否真的有執行例外處理</returns>
        private static bool ExecuteExceptionHandler( ExceptionPolicy policy, Exception exception, object sender, string title )
        {
            if ( !policy.IsCustomPolicy )
            {
                HandlerExecuter.DoExceptionHandler( policy.Handler, exception, sender, title );
                return true;
            }

            // 20131211 Pony Says: 在執行客製化例外政策時，例外的型別必須要完全符合Config檔定義的情況才可以執行
            if ( CheckCustomExceptionType( policy, exception, sender, title ) )
            {
                HandlerExecuter.DoExceptionHandler( policy.Handler, exception, sender, title );
                return true;
            }

            return false;
        }

        /// <summary>檢查客製化例外的型態
        /// </summary>
        /// <param name="policy">例外處理政策</param>
        /// <param name="exception">程式例外物件</param>
        /// <param name="sender">處發例外事件的物件</param>
        /// <param name="title">事件訊息</param>
        /// <returns>例外型態是否正確</returns>
        private static bool CheckCustomExceptionType( ExceptionPolicy policy, Exception exception, object sender, string title )
        {
            var  assemblyPath = policy.ExceptionAssemblyPath;
            var  typeFullName = policy.ExceptionTypeFullName;
            var  handler      = CreateExceptionTypeCheckHandler( exception, assemblyPath, typeFullName );
            bool result       = handler();
            return result;
        }

        /// <summary>建立例外型態檢查委派
        /// </summary>
        /// <param name="exception">程式例外物件</param>
        /// <param name="assemblyPath">組件路徑</param>
        /// <param name="typeFullName">例外完整型別名稱</param>
        /// <returns>例外型態檢查委派</returns>
        private static Func<bool> CreateExceptionTypeCheckHandler( Exception exception, string assemblyPath, string typeFullName )
        {
            Func<bool> handler = () => {
                return exception.IsTypeof( assemblyPath, typeFullName );
            };

            if ( assemblyPath.IsNullOrEmpty() )
            {
                handler = () => {
                    return exception.IsTypeof( typeFullName );
                };
            }

            return handler;
        }

        #endregion 宣告私有的靜態方法


        #region 宣告公開的靜態方法

        /// <summary>執行ZayniFramework 框架預設的程式例外處理政策
        /// </summary>
        /// <param name="exception">程式例外物件</param>
        /// <param name="sender">處發例外事件的物件</param>
        /// <param name="eventTitle">事件訊息</param>
        /// <returns>處理結果</returns>
        public static HandleResult ExecuteDefaultExceptionPolicy( Exception exception, object sender = null, string eventTitle = "" )
        {
            HandleResult result = ExecuteExceptionPolicy( DEFAULT_POLICY_NAME, exception, sender, eventTitle );
            return result;
        }

        /// <summary>執行ZayniFramework 框架的電子郵件通知程式例外處理政策
        /// </summary>
        /// <param name="exception">程式例外物件</param>
        /// <param name="sender">處發例外事件的物件</param>
        /// <param name="eventTitle">事件訊息</param>
        /// <returns>處理結果</returns>
        public static HandleResult ExecuteEMailNotifyExceptionPolicy( Exception exception, object sender = null, string eventTitle = "" )
        {
            HandleResult result = ExecuteExceptionPolicy( EMAIL_POLICY_NAME, exception, sender, eventTitle );
            return result;
        }

        /// <summary>執行程式例外處理政策
        /// </summary>
        /// <param name="policyName">例外處理政策名稱</param>
        /// <param name="exception">程式例外物件</param>
        /// <param name="sender">處發例外事件的物件</param>
        /// <param name="eventTitle">事件訊息</param>
        /// <returns>處理結果</returns>
        public static HandleResult ExecuteExceptionPolicy( string policyName, Exception exception, object sender = null, string eventTitle = "" )
        {
            #region 初始化回傳值

            var result = new HandleResult()
            {
                Success       = false,
                ExceptionObject = exception
            };

            #endregion 初始化回傳值

            #region 檢查傳入的引數

            if ( policyName.IsNullOrEmpty() )
            {
                result.Message = "例外處理政策名稱為Null或空字串，ZayniFramework 框架無法執行程式例外處理。";
                return result;
            }

            #endregion 檢查傳入的引數

            #region 檢查目標例外處理政策

            bool isExist = _policies.ContainsKey( policyName );

            if ( !isExist )
            {
                result.Message = "找不到例外處理政策名稱為{0}的政策名稱，ZayniFramework 框架無法執行程式例外處理。".FormatTo( policyName );
                return result;
            }

            ExceptionPolicy policy = _policies[ policyName ];

            if ( policy.IsNull() )
            {
                result.Message = "{0}例外處理政策為Null值，ZayniFramework 框架無法執行程式例外處理。".FormatTo( policyName );
                return result;
            }

            if ( !policy.IsEnable )
            {
                result.Message = "{0}例外處理政策被停用，ZayniFramework 框架沒有執行程式例外處理。".FormatTo( policyName );
                return result;
            }

            #endregion 檢查目標例外處理政策

            #region 執行例外處理政策

            bool isExecute = false;

            try
            {
                isExecute = ExecuteExceptionHandler( policy, exception, sender, eventTitle );
            }
            catch ( Exception ex )
            {
                result.Message = "ZayniFramework 框架執行程式例外處理政策{0}時又發生程式異常: {1}".FormatTo( policyName, ex.ToString() );
                return result;
            }

            if ( !isExecute )
            {
                result.Message = "ZayniFramework 框架沒有執行例外處理政策，請檢查傳入的例外物件型別是否正確。";
                return result;
            }

            #endregion 執行例外處理政策

            #region 設定回傳值屬性

            result.NeedRethrow = policy.NeedRethrow;
            result.Message     = "ZayniFramework 框架成功執行例外處理政策。";
            result.Success   = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>對客製化例外處理政策註冊例外處理器
        /// </summary>
        /// <param name="policyName">政策名稱</param>
        /// <param name="handler">ZayniFramework 框架的外處理委派</param>
        /// <returns>對客製化例外處理政策註冊例外處理器是否成功</returns>
        public static Result RegisterCustomPolicyHandler( string policyName, ExceptionHandler handler )
        {
            #region 初始化回傳值

            var result = new Result() 
            {
                Success = false
            };

            #endregion 初始化回傳值

            #region 檢查傳入的引數

            if ( policyName.IsNullOrEmpty() )
            {
                result.Message = "例外處理政策名稱為Null或空字串，ZayniFramework 框架無法註冊程式例外處理。";
                return result;
            }

            #endregion 檢查傳入的引數

            #region 取得目標例外處理政策

            bool isExist = _policies.ContainsKey( policyName );

            if ( !isExist )
            {
                result.Message = "找不到例外處理政策名稱為{0}的政策名稱，ZayniFramework 框架無法註冊程式例外處理。".FormatTo( policyName );
                return result;
            }

            ExceptionPolicy policy = _policies[ policyName ];

            if ( policy.IsNull() )
            {
                result.Message = "{0}例外處理政策為Null值，ZayniFramework 框架無法註冊程式例外處理。".FormatTo( policyName );
                return result;
            }

            #endregion 取得目標例外處理政策

            policy.Handler = handler;

            result.Success = true;
            result.Message   = "ZayniFramework 框架成功註冊例外處理政策。";

            return result;
        }

        #endregion 宣告公開的靜態方法
    }
}
