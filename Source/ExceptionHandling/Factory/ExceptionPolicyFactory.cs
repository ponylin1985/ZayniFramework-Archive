﻿using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>例外處理政策工廠
    /// </summary>
    public static class ExceptionPolicyFactory
    {
        /// <summary>建立例外處理政策工廠
        /// </summary>
        /// <param name="policyType">處理政策代碼</param>
        /// <param name="customPolicyConfig">客製化例外處理政策Config元素</param>
        /// <returns>程式例外處理政策</returns>
        public static ExceptionPolicy Create( string policyType = "", CustomPolicyElement customPolicyConfig = null )
        {
            ExceptionPolicy result = null;

            switch ( policyType.ToLower() )
            {
                case "defaulttextlog":
                    result = new DefaultTextLoggingPolicy();
                    break;

                case "emailnotify":
                    result = new EMailNotifyPolicy();
                    break;

                case "custom":
                    result = CreateCutomExceptionPolicy( customPolicyConfig );
                    break;

                default:
                    result = new ExceptionPolicy();
                    break;
            }

            return result;
        }

        /// <summary>建立客製化例外處理政策
        /// </summary>
        /// <param name="customPolicyConfig">客製化例外處理政策Config元素</param>
        /// <returns>客製化例外處理政策</returns>
        private static ExceptionPolicy CreateCutomExceptionPolicy( CustomPolicyElement customPolicyConfig )
        {
            if ( customPolicyConfig.IsNull() )
            {
                return null;
            }

            var result = new ExceptionPolicy() 
            {
                PolicyName            = customPolicyConfig.PolicyName,
                IsEnable              = customPolicyConfig.IsEnable,
                ExceptionTypeFullName = customPolicyConfig.ExceptionTypeFullName,
                ExceptionAssemblyPath = customPolicyConfig.ExceptionAssemblyPath,
                IsCustomPolicy        = true, 
                NeedRethrow           = customPolicyConfig.NeedRethrow
            };

            if ( customPolicyConfig.TextLoggerHandlerName.IsNotNullOrEmpty() )
            {
                result.ExceptionHandlerName = customPolicyConfig.TextLoggerHandlerName;
                result.Handler = ( ex, sender, message ) => {
                    Logger.WriteExceptionLog( sender, ex, message, customPolicyConfig.TextLoggerHandlerName );
                };
            }

            return result;
        }
    }
}
