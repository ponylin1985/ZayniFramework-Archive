﻿namespace ZayniFramework.ExceptionHandling
{
    /// <summary>ZayniFramework 框架的例外處理器工廠
    /// </summary>
    public static class ExceptionHandlerFactory
    {
        /// <summary>建立程式例外處理器
        /// </summary>
        /// <param name="type">例外處理器種類</param>
        /// <returns>程式例外處理器</returns>
        public static IExceptionHandler Create( ExceptionHandlerType type )
        {
            IExceptionHandler result = null;

            switch ( type )
            {
                case ExceptionHandlerType.EventLogging:
                    result = new EventLoggingHandler();
                    break;

                case ExceptionHandlerType.TextLogging:
                    result = new TextLoggingHandler();
                    break;

                case ExceptionHandlerType.EMailNotify:
                    result = new EMailNotifyHandler();
                    break;

                default:
                    result = new TextLoggingHandler();
                    break;
            }

            return result;
        }
    }
}
