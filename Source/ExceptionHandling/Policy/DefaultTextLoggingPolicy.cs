﻿using ZayniFramework.Common;


namespace ZayniFramework.ExceptionHandling
{
    /// <summary>預設文字日誌
    /// </summary>
    public class DefaultTextLoggingPolicy : ExceptionPolicy
    {
        /// <summary>Zayni框架預設的例外處理政策的預設文字Log路徑
        /// </summary>
        private const string DEFAULT_POLICY_LOG_PATH = @"C:\ZayniFramework\Log\Exception.log";

        /// <summary>預設建構子
        /// </summary>
        public DefaultTextLoggingPolicy()
        {
            Initialize();
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="policyName">政策名稱</param>
        public DefaultTextLoggingPolicy( string policyName )
        {
            Initialize( "_ZayniDefaultPolicy_" );
        }

        /// <summary>初始化
        /// </summary>
        public void Initialize( string policyName = "" )
        {
            ConfigReader.LoadDefaultPolicySettings();

            base.PolicyName     = "_ZayniDefaultPolicy_";
            base.IsEnable       = DefaultPolicySettings.IsEnable;
            base.NeedRethrow    = DefaultPolicySettings.NeedRethrow;
            base.IsCustomPolicy = false;

            base.Handler = ( exception, sender, message ) => {
                string path = DefaultPolicySettings.DefaultPolicyLogPath.IsNullOrEmptyString( DEFAULT_POLICY_LOG_PATH, true );

                IExceptionHandler handler = ExceptionHandlerFactory.Create( ExceptionHandlerType.TextLogging );

                handler.HandleException( new {
                    LogPath    = path,
                    LogTitle   = message,
                    LogMessage = exception.Message,
                    Exception  = exception
                } );

                handler = ExceptionHandlerFactory.Create( ExceptionHandlerType.EventLogging );

                handler.HandleException( new {
                    LogTitle   = message,
                    LogMessage = exception.Message,
                    IsEnable   = DefaultPolicySettings.IsEnableDefaultPolicyEventLog,
                    Exception  = exception
                } );
            };
        }
    }
}
