﻿using System;
using System.Data;
using System.Linq;


namespace ZayniFramework.DataAccess
{
    /// <summary>ADO.NET DataRow 物件的資料轉換器
    /// </summary>
    public static class DataRowConverter
    {
        /// <summary>取得 Byte 數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Byte 數值</returns>
        public static byte GetByte( DataRow row, string columnName ) => 
            byte.TryParse( row[ columnName ] + "", out byte result ) ? result : default ( byte );

        /// <summary>取得 Int32 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Int32 整數值</returns>
        public static int GetInt32( DataRow row, string columnName ) => 
            int.TryParse( row[ columnName ] + "", out int result ) ? result : default ( int );

        /// <summary>取得 Int64 整數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Int64 整數值</returns>
        public static long GetInt64( DataRow row, string columnName ) =>
            long.TryParse( row[ columnName ] + "", out long result ) ? result : default ( long );

        /// <summary>取得 Double 浮點數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Double 浮點數值</returns>
        public static double GetDouble( DataRow row, string columnName ) => 
            double.TryParse( row[ columnName ] + "", out double result ) ? result : default ( double );

        /// <summary>取得 Decimal 數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Decimal 數值</returns>
        public static decimal GetDecimal( DataRow row, string columnName ) =>
            decimal.TryParse( row[ columnName ] + "", out decimal result ) ? result : default ( decimal );

        /// <summary>取得 DateTime 日期時間，包含資料庫中時間的毫秒數。
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>DateTime 日期時間，包含資料庫中時間的毫秒數。</returns>
        public static DateTime GetDateTime( DataRow row, string columnName )
        {
            try
            {
                return (DateTime)row[ columnName ];
            }
            catch ( Exception )
            {
                return default ( DateTime );
            }
        }

        /// <summary>取得 Byte Nullable 數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Byte Nullable 整數值</returns>
        public static byte? GetByteNullable( DataRow row, string columnName ) => byte.TryParse( row[ columnName ] + "", out var value ) ? (byte?)value : null;

        /// <summary>取得 Int32 Nullable 數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Int32 Nullable 整數值</returns>
        public static int? GetInt32Nullable( DataRow row, string columnName ) => int.TryParse( row[ columnName ] + "", out var value ) ? (int?)value : null;

        /// <summary>取得 Int64 Nullable 數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Int64 Nullable 整數值</returns>
        public static long? GetInt64Nullable( DataRow row, string columnName ) => long.TryParse( row[ columnName ] + "", out var value ) ? (long?)value : null;

        /// <summary>取得 Double Nullable 數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Double Nullable 整數值</returns>
        public static double? GetDoubleNullable( DataRow row, string columnName ) => double.TryParse( row[ columnName ] + "", out var value ) ? (double?)value : null;

        /// <summary>取得 Decimal Nullable 數值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>Decimal Nullable 整數值</returns>
        public static decimal? GetDecimalNullable( DataRow row, string columnName ) => decimal.TryParse( row[ columnName ] + "", out var value ) ? (decimal?)value : null;

        /// <summary>取得 DateTime? Nullable 日期時間
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>DateTime? Nullable 日期時間</returns>
        public static DateTime? GetDateTimeNullable( DataRow row, string columnName )
        {
            if ( !DateTime.TryParse( row[ columnName ] + "", out DateTime result ) )
            {
                return null;
            }

            return result;
        }

        /// <summary>取得字串值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>字串值</returns>
        public static string GetString( DataRow row, string columnName ) => row[ columnName ] + "";

        /// <summary>取得布林值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>布林值</returns>
        public static bool GetBoolean( DataRow row, string columnName )
        {
            object obj = row[ columnName ];

            if ( "System.Boolean" == obj.GetType().FullName )
            {
                return (bool)obj;
            }

            if ( !int.TryParse( obj + "", out int value ) )
            {
                return false;
            }

            if ( !new int[] { 0, 1 }.Contains( value ) )
            {
                return false;
            }

            return 1 == value;
        }

        /// <summary>取得 bool? Nullable 布林值
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>bool? Nullable 布林值</returns>
        public static bool? GetBooleanNullable( DataRow row, string columnName )
        {
            object obj = row[ columnName ];

            if ( "System.Boolean" == obj.GetType().FullName )
            {
                return (bool)obj;
            }

            if ( !int.TryParse( obj + "", out int value ) )
            {
                return null;
            }

            if ( !new int[] { 0, 1 }.Contains( value ) )
            {
                return null;
            }

            return 1 == value;
        }

        /// <summary>取得二進位陣列 (byte[]) 資料
        /// </summary>
        /// <param name="row">ADO.NET DataRow 資料列物件</param>
        /// <param name="columnName">欄位名稱</param>
        /// <returns>二進位資料</returns>
        public static byte[] GetBinary( DataRow row, string columnName ) => (byte[])row[ columnName ];
    }
}
