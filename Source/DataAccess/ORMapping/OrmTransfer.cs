﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using ZayniFramework.Common;
using ZayniFramework.Common.ORM;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>ORM 資料轉換處理元件，將來源的 DataTable 的資料轉換成物件資料模型
    /// </summary>
    public static class OrmTransfer
    {
        #region 宣告私有的轉換委派

        /// <summary>轉換 DataSet 資料集合成強型別資料模型動作的實體委派
        /// </summary>
        /// <typeparam name="TConvert">轉換後的目標型別</typeparam>
        /// <param name="source">資料來源DataSet</param>
        /// <returns>轉換後的強型別資料模型資料集合</returns>
        private delegate List<TConvert> ConvertHander<TConvert>( DataTable source )
            where TConvert : new();

        #endregion 宣告私有的轉換委派
        

        #region 宣告公開的靜態轉換方法

        /// <summary>嘗試轉換 DataSet 資料集合成強型別資料模型動作的實體委派
        /// </summary>
        /// <typeparam name="TConvert">轉換後的目標型別</typeparam>
        /// <param name="source">資料來源DataSet</param>
        /// <param name="tableName">來源DataSet中的DataTable資料來源表格名稱</param>
        /// <param name="result">轉換後的Model/Entity串列</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>轉換是否成功</returns>
        public static bool TryConvert<TConvert>( DataSet source, string tableName, out List<TConvert> result, out string message )
            where TConvert : new()
        {
            if ( !CheckInputArguments<TConvert>( source, tableName, out result, out message ) )
            {
                return false;
            }

            return DoTransfer<TConvert>( Convert<TConvert>, source, tableName, out result, out message );
        }

        /// <summary>嘗試將資料來源DataSet轉換成指定型別的Model/Entity
        /// </summary>
        /// <param name="source">資料來源DataSet</param>
        /// <param name="tableName">來源DataSet中的DataTable資料來源表格名稱</param>
        /// <param name="targetType">目標型別</param>
        /// <param name="result">轉換後的Model/Entity串列</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>轉換是否成功</returns>
        public static bool TryConvert( DataSet source, string tableName, Type targetType, out ArrayList result, out string message )
        {
            #region 檢查輸入的引數

            if ( !CheckInputArguments( source, tableName, out result, out message ) )
            {
                return false;
            }

            #endregion 檢查輸入的引數

            #region 進行轉換的工作

            if ( source.Tables[ tableName ].Rows.Count <= 0 )
            {
                message = $"No data in DataTable '{tableName}', no need to ORM convert.";
                return true;
            }

            DataTable table = source.Tables[ tableName ];

            try
            {
                result = Convert( targetType, table );
            }
            catch ( Exception ex )
            {
                message = $"{Logger.GetTraceLogTitle( nameof ( OrmTransfer ), nameof ( TryConvert ) )}, execute DataTable convert occur exception. {ex.ToString()}";
                return false;
            }

            #endregion 進行轉換的工作

            return true;
        }

        #endregion 宣告公開的靜態轉換方法


        #region 宣告私有的靜態方法

        /// <summary>檢查TransferManager入口方法的輸入引數是否合法
        /// </summary>
        /// <param name="source">資料來源DataSet</param>
        /// <param name="tableName">來源DataSet中的DataTable資料來源表格名稱</param>
        /// <param name="message">錯誤訊息</param>
        /// <param name="result">轉換後的Model/Entity串列</param>
        /// <returns>方法輸入引數是否合法</returns>
        private static bool CheckInputArguments<TConvert>( DataSet source, string tableName, out List<TConvert> result, out string message )
            where TConvert : new()
        {
            result  = new List<TConvert>();
            message = string.Empty;

            string logTitle = Logger.GetTraceLogTitle( nameof ( OrmTransfer ), nameof ( CheckInputArguments ) );

            if ( source.IsNull() )
            {
                result  = null;
                message = $"{logTitle}, DataSet argument is null. Can not process ORM transfer.";
                return false;
            }

            if ( tableName.IsNullOrEmpty() )
            {
                result  = null;
                message = $"{logTitle}, name of DataTable is null or empty string. Can not process ORM transfer.";
                return false;
            }

            if ( !source.Tables.Contains( tableName ) || source.Tables[ tableName ].IsNull() )
            {
                result  = null;
                message = $"{logTitle}, DataSet does not contain '{tableName}' DataTable or '{tableName}' DataTable is null in DataSet. Can not process ORM transfer.";
                return false;
            }

            return true;
        }

        /// <summary>檢查TransferManager入口方法的輸入引數是否合法
        /// </summary>
        /// <param name="source">資料來源DataSet</param>
        /// <param name="tableName">來源DataSet中的DataTable資料來源表格名稱</param>
        /// <param name="message">錯誤訊息</param>
        /// <param name="result">轉換後的Model/Entity串列</param>
        /// <returns>方法輸入引數是否合法</returns>
        private static bool CheckInputArguments( DataSet source, string tableName, out ArrayList result, out string message )
        {
            result  = new ArrayList();
            message = string.Empty;

            string logTitle = Logger.GetTraceLogTitle( nameof ( OrmTransfer ), nameof ( CheckInputArguments ) );

            if ( source.IsNull() )
            {
                result  = null;
                message = $"{logTitle}, DataSet argument is null. Can not process ORM transfer.";
                return false;
            }

            if ( tableName.IsNullOrEmpty() )
            {
                result  = null;
                message = $"{logTitle}, name of DataTable is null or empty string. Can not process ORM transfer.";
                return false;
            }

            if ( !source.Tables.Contains( tableName ) || source.Tables[ tableName ].IsNull() )
            {
                result  = null;
                message = $"{logTitle}, DataSet does not contain '{tableName}' DataTable or '{tableName}' DataTable is null in DataSet. Can not process ORM transfer.";
                return false;
            }

            return true;
        }

        /// <summary>執行轉換動作的方法
        /// </summary>
        /// <typeparam name="TConvert">轉換後的目標型別</typeparam>
        /// <param name="handler">轉換動作的委派</param>
        /// <param name="source">資料來源DataSet</param>
        /// <param name="tableName">來源DataSet中的DataTable資料來源表格名稱</param>
        /// <param name="result">轉換後的Model/Entity串列</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>轉換是否成功</returns>
        private static bool DoTransfer<TConvert>( ConvertHander<TConvert> handler, DataSet source, string tableName, out List<TConvert> result, out string message )
            where TConvert : new()
        {
            result  = new List<TConvert>();
            message = string.Empty;

            if ( source.Tables[ tableName ].Rows.Count <= 0 )
            {
                message = $"No data in DataTable '{tableName}', no need to process ORM convert.";
                return true;
            }

            try
            {
                DataTable table = source.Tables[ tableName ];
                result          = handler( table );
            }
            catch ( Exception ex )
            {
                message = $"{Logger.GetTraceLogTitle( nameof ( OrmTransfer ), nameof ( DoTransfer ) )}, execute ORM convert occur exception. {ex.ToString()}";
                return false;
            }

            return true;
        }

        /// <summary>判斷型別是否為數字
        /// </summary>
        /// <param name="typeName">型別的名稱</param>
        /// <returns>是否為一種數字的型別</returns>
        private static bool IsNumeric( string typeName )
        {
            switch ( typeName )
            {
                case "Byte":
                case "SByte":
                case "Int16":
                case "UInt16":
                case "Int32":
                case "UInt32":
                case "Int64":
                case "UInt64":
                case "Decimal":
                case "Double":
                case "Float":
                    return true;

                default:
                    return false;
            }
        }

        /// <summary>判斷是否為 DBNull 型別
        /// </summary>
        /// <param name="typeName">型別的名稱</param>
        /// <returns>是否為 DBNull 型別</returns>
        private static bool IsDBNull( string typeName )
        {
            bool result = "DBNull".Equals( typeName );
            return result;
        }

        /// <summary>處理 DBNull 的值
        /// </summary>
        /// <typeparam name="TConvert"></typeparam>
        /// <param name="isAllowNull">是否允許 Null 值</param>
        /// <param name="defaultValue">程式預設值字串</param>
        /// <param name="propertyInfo">屬性中介資料</param>
        /// <param name="model">資料模型</param>
        private static void HandleDBNullValue<TConvert>( bool isAllowNull, string defaultValue, PropertyInfo propertyInfo, TConvert model )
        {
            // 如果該屬性標記為不可以為 Null 值，直接判定轉換失敗!
            if ( !isAllowNull )
            {
                throw new Exception( $"ORM convert fail due to Property '{propertyInfo.Name}' is not allowed DBNull value." );
            }

            string propertyTypeName = propertyInfo.PropertyType.Name;
            // 20171028 Pony Says: 我決定當 PropertyType 為 Struct Nullable 型別時，就不支援 ORM Metadata DefaultValue 的預設值給定了...
            //Check.IsTrue( Reflector.IsNullableType( propertyInfo.PropertyType ), () => propertyTypeName = Reflector.GetNullableType( propertyInfo.PropertyType ).Name );

            // 如果該屬性標記為可以為 Null 值，則給定預設值
            switch ( propertyTypeName )
            {
                case "String":
                    propertyInfo.SetValue( model, defaultValue, null );
                    break;

                case "Decimal":
                    if ( defaultValue.IsNullOrEmpty() || !decimal.TryParse( defaultValue, out decimal decimalValue ) )
                    {
                        propertyInfo.SetValue( model, default ( decimal ), null );
                        break;
                    }

                    propertyInfo.SetValue( model, decimalValue, null );
                    break;

                case "Int32":
                    if ( defaultValue.IsNullOrEmpty() || !int.TryParse( defaultValue, out int intValue ) )
                    {
                        propertyInfo.SetValue( model, default ( int ), null );
                        break;
                    }

                    propertyInfo.SetValue( model, intValue, null );
                    break;

                case "DateTime":
                    if ( defaultValue.IsNullOrEmpty() || !DateTime.TryParse( defaultValue, out DateTime dateTimeValue ) )
                    {
                        propertyInfo.SetValue( model, default ( DateTime ), null );
                        break;
                    }

                    propertyInfo.SetValue( model, dateTimeValue, null );
                    break;

                case "Boolean":
                    if ( defaultValue.IsNullOrEmpty() || !bool.TryParse( defaultValue, out bool booleanValue ) )
                    {
                        propertyInfo.SetValue( model, default ( bool ), null );
                        break;
                    }

                    propertyInfo.SetValue( model, booleanValue, null );
                    break;
            }
        }

        #endregion 宣告私有的靜態方法


        #region 宣告私有的靜態轉換方法

        /// <summary>將傳入的 DataTable 轉換成特定型別的 Entity 物件
        /// </summary>
        /// <typeparam name="TConvert">轉換後的目標型別</typeparam>
        /// <param name="source">資料來源DataTable</param>
        /// <returns>轉換後的物件串列</returns>
        private static List<TConvert> Convert<TConvert>( DataTable source )
            where TConvert : new()
        {
            var type           = typeof ( TConvert );
            var convertResults = Convert( type, source );

            if ( convertResults.IsNull() )
            {
                throw new ApplicationException( $"{Logger.GetTraceLogTitle( nameof ( OrmTransfer ), nameof ( Convert ) )}, execute convert occur exception." );
            }

            object[]       models = convertResults.ToArray();
            List<TConvert> result = models.ToList<TConvert>();
            return result;
        }

        /// <summary>將傳入的 DataTable 轉換成特定型別的 Entity 物件
        /// </summary>
        /// <param name="type">目標型別</param>
        /// <param name="source">資料來源DataTable</param>
        /// <returns>轉換後的物件串列</returns>
        private static ArrayList Convert( Type type, DataTable source )
        {
            var result = new ArrayList();
            
            PropertyInfo[] properties = type.GetProperties( BindingFlags.Public    |
                                                            BindingFlags.NonPublic |
                                                            BindingFlags.Instance );

            if ( properties.IsNullOrEmptyArray() )
            {
                return result;
            }

            DataColumnCollection columns = source.Columns;

            foreach ( DataRow row in source.Rows )
            {
                var model = Activator.CreateInstance( type );

                foreach( PropertyInfo propertyInfo in properties )
                {
                    #region 設定資料模型屬性對應的欄位名稱

                    string columnName = propertyInfo.Name;

                    TableColumnAttribute ormAttr = Reflector.GetCustomAttribute<TableColumnAttribute>( propertyInfo );
                    ormAttr.IsNotNull( m => columnName = ormAttr.ColumnName );

                    // 如果找不到可以對應的 Property、Attribute 名稱與資料表欄位名稱，就先忽略
                    if ( !columns.Contains( columnName ) )
                    {
                        continue;
                    }

                    Type columnType = row[ columnName ].GetType();

                    #endregion 設定資料模型屬性對應的欄位名稱

                    #region 為了防止資料庫中的欄位真的有 DBNull 的情況，造成轉換失敗的處理

                    // 如果 DataTable 中的欄位值確實為 DBNull
                    if ( ormAttr.IsNotNull() && IsDBNull( columnType.Name ) )
                    {
                        HandleDBNullValue( ormAttr.IsAllowNull, ormAttr.DefaultValue, propertyInfo, model );
                        continue;
                    }

                    #endregion 為了防止資料庫中的欄位真的有 DBNull的情況，造成轉換失敗的處理

                    bool isTimestamp = null == ormAttr ? false : ormAttr.IsTimeStamp;
                    PropertyValueBinding( row, columnType, columnName, model, propertyInfo, isTimestamp );
                }

                result.Add( model );
            }

            return result;
        }

        /// <summary>進行資料模型屬性值繫結
        /// </summary>
        /// <param name="row">資料列</param>
        /// <param name="columnType">資料表欄位型別</param>
        /// <param name="columnName">資料表欄位名稱</param>
        /// <param name="model">目標資料模型</param>
        /// <param name="propertyInfo">目標屬性資訊</param>
        /// <param name="isTimestamp">是否為資料異動時間戳記 (IsTimestamp) 欄位</param>
        /// <returns>資料繫結過的資料模型</returns>
        private static object PropertyValueBinding( DataRow row, Type columnType, string columnName, object model,  PropertyInfo propertyInfo, bool isTimestamp )
        {
            #region 資料異動時間戳記 (Timestamp) 欄位處理

            if ( isTimestamp )
            {
                object dbTimestamp = row[ columnName ];
                long timestamp = 0;

                switch ( dbTimestamp )
                {
                    case DateTime dt:
                        timestamp = dt.Ticks;
                        break;

                    case byte[] bytes:
                        timestamp = BitConverter.ToInt64( bytes, 0 );
                        break;
                }
                
                propertyInfo.SetValue( model, timestamp );
                return model;
            }

            #endregion 資料異動時間戳記 (Timestamp) 欄位處理

            object value    = row[ columnName ];
            string strValue = value + "";

            string columnTypeName   = columnType.Name;
            string propertyTypeName = propertyInfo.PropertyType.Name;

            if ( "DBNull" == columnTypeName )
            {
                return model;
            }

            if ( "Nullable`1" == propertyTypeName )
            {
                propertyTypeName = Reflector.GetNullableType( propertyInfo.PropertyType ).Name;
                //propertyInfo.SetValue( model, value, null );
                //return model;
            }

            if ( columnTypeName == propertyTypeName )
            {
                propertyInfo.SetValue( model, value, null );
                return model;
            }

            if ( IsNumeric( columnType.Name ) )
            {
                #region 預設在資料庫數字型別的欄位可以繫結到字串型別的屬性上

                // 預設數字可轉為 String
                if ( "String" == propertyTypeName )
                {
                    propertyInfo.SetValue( model, strValue, null );     
                    return model;
                }

                #endregion 預設在資料庫數字型別的欄位可以繫結到字串型別的屬性上

                #region 20150824 Added by Pony: 布林值型別的屬性的轉換處理

                if ( "Boolean" == propertyTypeName )
                {
                    if ( !int.TryParse( strValue, out int numValue )  )
                    {
                        return model;
                    }

                    if ( 0 == numValue )
                    {
                        propertyInfo.SetValue( model, false, null );
                        return model;
                    }

                    propertyInfo.SetValue( model, true, null );
                    return model;
                }

                #endregion 20150824 Added by Pony: 布林值型別的屬性的轉換處理

                #region 20150824 Edited by Pony: 針對 Oracle 資料庫的 NUMBER 型別的欄位進行轉換與處理

                // 20140307 Pony Says: 以下是一種Special Case，因為在Oracle資料庫中，所有的數字型別對應到.NET CTS中的型別都會是Decimal
                // 20140307 Pony Says: 所以針對資料模型中的Int32型別的屬性有這樣的特殊處理
                // 20150827 Pony Says: 我不得不在這邊寫死，把所有可能在C#中的數字型別拿出來逐一檢查比對，這樣子做針對Oracle資料庫的數字型別才不會有問題。

                if ( "Decimal" == columnTypeName && "Byte" == propertyTypeName )
                {
                    byte byteValue = byte.Parse( strValue );
                    propertyInfo.SetValue( model, byteValue, null );
                    return model;
                }

                if ( "Decimal" == columnTypeName && "Int16" == propertyTypeName )
                {
                    short shortValue = byte.Parse( strValue );
                    propertyInfo.SetValue( model, shortValue, null );
                    return model;
                }

                if ( "Decimal" == columnTypeName && "Int32" == propertyTypeName )
                {
                    decimal decimalValue = decimal.Parse( strValue );
                    
                    if ( !decimalValue.IsRealDecimal() )
                    {
                        propertyInfo.SetValue( model, int.Parse( strValue ), null );
                        return model;
                    }
                }

                if ( "Decimal" == columnTypeName && "Int64" == propertyTypeName )
                {
                    long longValue = long.Parse( strValue );
                    propertyInfo.SetValue( model, longValue, null );
                    return model;
                }

                if ( "Decimal" == columnTypeName && "Double" == propertyTypeName )
                {
                    double doubleValue = long.Parse( strValue );
                    propertyInfo.SetValue( model, doubleValue, null );
                    return model;
                }

                if ( "Decimal" == columnTypeName && "Float" == propertyTypeName )
                {
                    float floatValue = long.Parse( strValue );
                    propertyInfo.SetValue( model, floatValue, null );
                    return model;
                }

                #endregion 20150824 Edited by Pony: 針對 Oracle 資料庫的 NUMBER 型別的欄位進行轉換與處理
            }

            string errorMsg = $"ORM transfering occur exception. Property value binding occur exception. DataColumn: '{columnName}', DataColumn Type: {columnType.Name}, Property Name: {propertyInfo.Name} , Property Type: {propertyInfo.PropertyType.Name}.";
            throw new Exception( errorMsg );
        }

        #endregion 宣告私有的靜態轉換方法
    }
}
