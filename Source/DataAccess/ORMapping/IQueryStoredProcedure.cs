﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>查詢的預存程序介面
    /// </summary>
    public interface IQueryStoredProcedure
    {
        /// <summary>執行結果資料集合
        /// </summary>
        ModelSet ResultSet { get; set; }

        /// <summary>執行結果料集合的型別
        /// </summary>
        Type ResultSetType { get; set; }

        /// <summary>ModelSet中的型別對應陣列
        /// </summary>
        Type[] ModelSetTypes { get; set; }
    }
}
