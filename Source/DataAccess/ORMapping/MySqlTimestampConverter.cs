﻿using Newtonsoft.Json;
using System;


namespace ZayniFramework.DataAccess
{
    /// <summary>MySQL 資料庫的資料異動時間戳記 Timestamp 序列化轉換器
    /// </summary>
    public class MySqlTimestampConverter : JsonConverter
    {
        /// <summary>檢查是否需要轉換 Timestampe
        /// </summary>
        /// <param name="objectType">資料的型別</param>
        /// <returns>是否需要轉換 Timestampe</returns>
        public override bool CanConvert( Type objectType ) => objectType == typeof ( DateTime );

        /// <summary>反序列化 DateTime 資料成 long 型別的資料異動戳記
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override object ReadJson( JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer )
        {
            if ( reader.Value is null )
            {
                return default ( Int64 );
            }

            object value = reader.Value;

            switch ( value )
            {
                case DateTime dtTimestamp:
                    return dtTimestamp.Ticks;

                case long int64Timestamp:
                    return int64Timestamp;

                default:
                    return default ( Int64 );
            }
        }

        /// <summary>序列化 long 型別的資料異動戳記成 DateTime 資料
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson( JsonWriter writer, object value, JsonSerializer serializer )
        {
            if ( value is null )
            {
                return;
            }

            switch ( value )
            {
                case long int64Timestamp:
                    writer.WriteValue( int64Timestamp );
                    return;

                case DateTime dtTimestamp:
                    writer.WriteValue( dtTimestamp.Ticks );
                    return;
            }
        }
    }
}
