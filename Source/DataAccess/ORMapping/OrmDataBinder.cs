﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Common.Dynamic;
using ZayniFramework.Common.ORM;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>ORM 資料繫結處理元件
    /// </summary>
    public class OrmDataBinder
    {
        /// <summary>將傳入的DataSet的資料載入到目標泛型的資料模型中
        /// </summary>
        /// <typeparam name="TModel">目標資料模型的泛型</typeparam>
        /// <param name="ds">資料來源DataSet</param>
        /// <param name="models">資料模型列表</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>是否成功</returns>
        public bool LoadDataToModel<TModel>( DataSet ds, out List<TModel> models, out string message )
            where TModel : new()
        {
            bool isSuccess = OrmTransfer.TryConvert<TModel>( ds, ds.Tables[ 0 ].TableName + "", out models, out message );

            if ( !isSuccess )
            {
                message = $"{Logger.GetTraceLogTitle( this, nameof ( LoadDataToModel ) )}, ORM trandfer fail. {message}";
                return false;
            }
            
            return true;
        }

        /// <summary>將傳入的資料讀取器的資料動態繫結到動態資料模型中 (Dynamic ORM機制)
        /// </summary>
        /// <param name="reader">資料來源資料讀取器</param>
        /// <param name="models">動態資料模型列表</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>是否成功</returns>
        public bool LoadDataToDynamicModel( IDataReader reader, out List<dynamic> models, out string message )
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( LoadDataToDynamicModel ) );

            models  = new List<dynamic>();
            message = null;

            try
            {
                while ( reader.Read() )
                {
                    dynamic model = DynamicHelper.CreateDynamicObject();

                    for ( int i = 0; i < reader.FieldCount; i++ )
                    {
                        string  name  = reader.GetName( i );
                        dynamic value = reader[ i ];

                        if ( !DynamicHelper.BindProperty( model, name, value ) )
                        {
                            reader.Close();
                            models  = null;
                            message = $"Dynamic object property binding occur error. PropertyName: {name}.";
                            Logger.WriteErrorLog( this, message, logTitle );
                            return false;
                        }
                    }

                    models.Add( model );
                }
            }
            catch ( Exception ex )
            {
                reader.Close();
                models  = null;
                message = $"Dynamic object property binding occur error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, logTitle, message );
                return false;
            }

            reader.Close();
            return true;
        }

        /// <summary>將傳入的 DataSet 的資料載入到對應的資料模型型別中，對應方式是按照 DataSet 中的 DataTabl e順序與 Type[] 中的順序相同
        /// </summary>
        /// <param name="ds">資料來源 DataSet</param>
        /// <param name="modelTypes">目標資料模型型別陣列</param>
        /// <param name="result">資料模型陣列的List</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>是否成功</returns>
        public bool LoadDataToModels( DataSet ds, Type[] modelTypes, out List<object[]> result, out string message ) 
        {
            result  = new List<object[]>();
            message = null;

            for ( int i = 0, length = ds.Tables.Count; i < length; i++ )
            {
                var tableName = ds.Tables[ i ].TableName + "";
                var type = modelTypes[ i ];

                if ( !OrmTransfer.TryConvert( ds, tableName, type, out ArrayList datas, out message ) )
                {
                    message = $"{Logger.GetTraceLogTitle( this, nameof ( LoadDataToModels ) )}, execute ORM transfer fail. {message}";
                    return false;
                }

                object[] models = datas.ToArray();
                result.Add( models );
            }

            return true;
        }

        /// <summary>將傳入的DataSet資料動態繫結到動態資料模型字典集合中 (Dynamic ORM機制)
        /// </summary>
        /// <param name="ds">資料來源DataSet</param>
        /// <param name="result">動態資料模型字典集合</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>是否成功</returns>
        public bool LoadDataToDynamicModelCollection( DataSet ds, out Dictionary<string, List<dynamic>> result, out string message )
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( LoadDataToDynamicModelCollection ) );

            result  = new Dictionary<string, List<dynamic>>();
            message = null;

            try
            {
                for ( int i = 0, length = ds.Tables.Count; i < length; i++ )
                {
                    var table     = ds.Tables[ i ];
                    var tableName = table.TableName + "";

                    List<dynamic> models = new List<dynamic>();

                    foreach ( DataRow row in table.Rows )
                    {
                        dynamic model = DynamicHelper.CreateDynamicObject();

                        foreach ( DataColumn colume in table.Columns )
                        {
                            string  name  = colume.ColumnName;
                            dynamic value = row[ name ];

                            if ( !DynamicHelper.BindProperty( model, name, value ) )
                            {
                                result  = null;
                                message = $"Dynamic object property binding occur error. PropertyName: {name}.";
                                Logger.WriteErrorLog( this, message, logTitle );
                                return false;
                            }
                        }

                        models.Add( model );
                    }

                    if ( !result.ContainsKey( tableName ) )
                    {
                        result.Add( tableName, models );
                    }
                }
            }
            catch ( Exception ex )
            {
                result  = null;
                message = $"Dynamic object property binding occur error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, logTitle, message );
                return false;
            }
            
            return true;
        }

        /// <summary>根據傳入的資料模型集合，依照ORM機制正確的載入到目標ModelSet中，ModelSet中的屬性需要正確的標記OrmData中介資料
        /// </summary>
        /// <param name="modelTypes">目標資料模型型別陣列</param>
        /// <param name="modelSet">資料模型集合</param>
        /// <param name="datas">資料來源Model</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>是否成功</returns>
        public bool LoadDataToModelSetByIndex( Type[] modelTypes, List<object[]> datas, ModelSet modelSet, out string message ) 
        {
            message = null;

            foreach ( var property in modelSet.GetType().GetProperties() )
            {
                OrmDataAttribute orm = Reflector.GetCustomAttribute<OrmDataAttribute>( property );

                if ( orm.IsNull() )
                {
                    continue;
                }

                try
                {
                    object value = ListReflector.CreateGenericList( modelTypes[ orm.Index ] );

                    if ( value.IsNull() )
                    {
                        message          = $"Create generic list occur exception.";
                        modelSet.Success = false;
                        return false;
                    }

                    object[] models = datas[ orm.Index ];

                    for ( int i = 0, len = models.Length; i < len; i++ )
                    {
                        if ( !ListReflector.AddItemToGenericList( value, models[ i ] ) )
                        {
                            message          = $"Add data into generic list occur error.";
                            modelSet.Success = false;
                            return false;
                        }
                    }
                    
                    property.SetValue( modelSet, value );
                }
                catch ( Exception ex )
                {
                    message          = ex.ToString();
                    modelSet.Success = false;
                    Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( LoadDataToModelSetByIndex ) ), message );
                    return false;
                }
            }

            modelSet.Success = true;
            modelSet.Message = null;
            return true;
        }

        /// <summary>根據傳入的資料模型集合，依照ORM機制正確的載入到目標ModelSet中，ModelSet中的屬性需要正確的標記OrmData中介資料
        /// </summary>
        /// <param name="modelTypes">目標資料模型型別陣列</param>
        /// <param name="modelSet">資料模型集合</param>
        /// <param name="datas">資料來源Model</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>是否成功</returns>
        public bool LoadDataToModelSet( Type[] modelTypes, List<object[]> datas, ModelSet modelSet, out string message ) 
        {
            message = null;

            foreach ( var property in modelSet.GetType().GetProperties() )
            {
                OrmDataAttribute orm = Reflector.GetCustomAttribute<OrmDataAttribute>( property );

                if ( orm.IsNull() )
                {
                    continue;
                }

                try
                {
                    Type type  = ListReflector.GetListFirstGenericType( property.PropertyType );
                    Type mType = modelTypes.Where( m => m.FullName == type.FullName ).FirstOrDefault();

                    object value = ListReflector.CreateGenericList( mType );

                    if ( value.IsNull() )
                    {
                        message          = $"Create generic list occur exception.";
                        modelSet.Success = false;
                        return false;
                    }

                    object[] models = datas.Where( m => m[ 0 ].GetType().FullName == type.FullName ).FirstOrDefault();

                    for ( int i = 0, len = models.Length; i < len; i++ )
                    {
                        bool isOk = ListReflector.AddItemToGenericList( value, models[ i ] );

                        if ( !isOk )
                        {
                            message          = $"Add data into generic list occur error.";
                            modelSet.Success = false;
                            return false;
                        }
                    }
                    
                    property.SetValue( modelSet, value );
                }
                catch ( Exception ex )
                {
                    message          = ex.ToString();
                    modelSet.Success = false;
                    Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( LoadDataToModelSet ) ), message );
                    return false;
                }
            }

            modelSet.Success = true;
            modelSet.Message = null;
            return true;
        }
    }
}
