﻿using System;
using System.Data.Common;
using System.Reflection;
using ZayniFramework.Common;
using ZayniFramework.Common.ORM;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫預存程序資料模型
    /// </summary>
    public abstract class StoredProcedureModel
    {
        #region 宣告內部類別

        /// <summary>內部資料存取Dao元件
        /// </summary>
        private class DataAccess : BaseDataAccess
        {
            /// <summary>資料庫名稱
            /// </summary>
            /// <param name="dbName">資料庫連線字串名稱</param>
            public DataAccess( string dbName ) : base( dbName )
            {
                // pass
            }
        }

        #endregion 宣告內部類別


        #region 宣告唯一公開的虛擬方法

        /// <summary>執行資料庫預存程序
        /// </summary>
        /// <returns>執行資料庫預存程序結果</returns>
        public virtual Result Execute()
        {
            #region 初始化回傳值

            var result = new Result() 
            {
                Success = false
            };

            #endregion 初始化回傳值

            #region 取得對應的預存程序的名稱，並且建立預存程序SQL命令

            Type type = this.GetType();

            StoredProcedureAttribute procedureAttr = Reflector.GetCustomAttribute<StoredProcedureAttribute>( type );

            if ( procedureAttr.IsNull() )
            {
                result.Message = "沒有在資料模型上正確標記StoredProcedure中介資料，無法執行資料庫預存程序。";
                Logger.WriteErrorLog( this, result.Message, "StoredProcedureModel.Execute" );
                return result;
            }
            
            string dbName = procedureAttr.DbName;
            string spName = procedureAttr.ProcedureName;

            if ( new string[] { dbName, spName }.HasNullOrEmptyElemants() )
            {
                result.Message = "StoredProcedure中介資料的DbName或ProcedureName屬性為Null或空字串，無法執行資料庫預存程序。";
                Logger.WriteErrorLog( this, result.Message, "StoredProcedureModel.Execute" );
                return result;
            }
            
            var dao = new DataAccess( dbName );
            DbCommand spCommand = dao.GetStoredProcCommand( spName );

            #endregion 取得對應的預存程序的名稱，並且建立預存程序SQL命令

            #region 取得所有的屬性並且加入所有預存程序的參數

            PropertyInfo[] properties = type.GetProperties();

            foreach ( var property in properties )
            {
                ProcedureParameterAttribute parameterAttr = Reflector.GetCustomAttribute<ProcedureParameterAttribute>( property );

                if ( parameterAttr.IsNull() )
                {
                    continue;
                }

                string parameterName  = parameterAttr.Name;
                int    parameterType  = parameterAttr.DbType;
                object parameterValue = property.GetValue( this );

                if ( parameterName.IsNullOrEmpty() )
                {
                    result.Message = "屬性{0}標記的ProcedureParameter中介資料Name屬性為Null或是空字串，無法執行資料庫預存程序。".FormatTo( property.Name );
                    Logger.WriteErrorLog( this, result.Message, "StoredProcedureModel.Execute" );
                    return result;
                }

                if ( parameterAttr.IsOutput )
                {
                    dao.AddOutParameter( spCommand, parameterName, parameterType, parameterAttr.MaxSize );
                }
                else
                {
                    dao.AddInParameter( spCommand, parameterName, parameterType, parameterValue );
                }
            }

            #endregion 取得所有的屬性並且加入所有預存程序的參數

            #region 執行資料庫預存程序

            bool isSuccess = false;

            PropertyInfo modelSetProp = type.GetProperty( "ResultSet" );

            if ( modelSetProp.IsNull() )
            {
                isSuccess = dao.ExecuteNonQuery( spCommand );
            }
            else
            {
                Type[]   types        = null;
                Type     modelSetType = null;
                ModelSet modelSet     = null;

                try
                {
                    types        = (Type[])type.GetProperty( "ModelSetTypes" ).GetValue( this );
                    modelSetType = (Type)type.GetProperty( "ResultSetType" ).GetValue( this );
                    modelSet     = (ModelSet)Activator.CreateInstance( modelSetType );
                }
                catch ( Exception ex )
                {
                    result.HasException    = true;
                    result.ExceptionObject = ex;
                    result.Message         = "StoredProcedureModel執行資料庫預存程序發生異常: {0}".FormatTo( ex.ToString() );
                    Logger.WriteExceptionLog( this, ex, result.Message );
                    return result;
                }
                
                isSuccess = dao.LoadModelSet( spCommand, modelSet, types );

                if ( !isSuccess )
                {
                    result.Message = "StoredProcedureModel執行資料庫預存程序失敗: {0}".FormatTo( dao.Message );
                    Logger.WriteErrorLog( this, result.Message, "StoredProcedureModel.Execute" );
                    return result;
                }

                try
                {
                    modelSetProp.SetValue( this, modelSet );
                }
                catch ( Exception ex )
                {
                    result.Message = "StoredProcedureModel執行資料庫預存程序發生異常: {0}".FormatTo( ex.ToString() );
                    Logger.WriteExceptionLog( this, ex, result.Message );
                    return result;
                }
            }

            #endregion 執行資料庫預存程序

            #region 設定回傳值屬性

            result.Message = "";
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion 宣告唯一公開的虛擬方法
    }
}
