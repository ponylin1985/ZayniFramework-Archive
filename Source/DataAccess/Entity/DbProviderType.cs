﻿namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫廠牌代碼
    /// </summary>
    public enum DbProviderType : int
    {
        /// <summary>Microsoft SQL Server 資料庫
        /// </summary>
        MSSql,

        /// <summary>Oralce 資料庫
        /// </summary>
        Oracle,

        /// <summary>MySQL 資料庫
        /// </summary>
        MySql,

        /// <summary>PostgreSQL 資料庫
        /// </summary>
        Postgres
    }
}
