﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料存取模組設定值
    /// </summary>
    internal static class DataAccessSettings
    {
        /// <summary>資料存取模組Config設定值
        /// </summary>
        internal static DataAccessSettingsConfig Settings { get; set; }
    }
}
