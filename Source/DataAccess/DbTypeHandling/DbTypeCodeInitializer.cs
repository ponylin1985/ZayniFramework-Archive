﻿using MySql.Data.MySqlClient;
using NpgsqlTypes;
using System;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Linq;
using System.Reflection;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫型別代碼初始化元件
    /// </summary>
    internal static class DbTypeCodeInitializer
    {
        #region 宣告資料庫型別代碼的字串陣列

        /// <summary>SqlDbType 的型別代碼字串
        /// </summary>
        private static string[] _sqlDbTypeCodes = 
        {
            "BigInt"              ,
            "Binary"              ,
            "Bit"                 ,
            "Char"                ,
            "DateTime"            ,

            "Decimal"             ,
            "Float"               ,
            "Image"               ,
            "Int"                 ,
            "Money"               ,

            "NChar"               ,
            "NText"               ,
            "NVarChar"            ,
            "Real"                ,
            "UniqueIdentifier"    ,

            "SmallDateTime"       ,
            "SmallInt"            ,
            "SmallMoney"          ,
            "Text"                ,
            "Timestamp"           ,

            "TinyInt"             ,
            "VarBinary"           ,
            "VarChar"             ,
            "Variant"             ,
            "Xml"                 ,

            "Udt"                 ,
            "Structured"          ,
            "Date"                ,
            "Time"                ,
            "DateTime2"           ,

            "DateTimeOffset"
        };

        /// <summary>Oracle 的型別代碼字串
        /// </summary>
        private static string[] _orlaceDbTypes = 
        {
            "BFile"               ,
            "Blob"                ,
            "Char"                ,
            "Clob"                ,
            "Cursor"              ,

            "DateTime"            ,
            "IntervalDayToSecond" ,
            "IntervalYearToMonth" ,
            "LongRaw"             ,
            "LongVarChar"         ,

            "NChar"               ,
            "NClob"               ,
            "Number"              ,
            "NVarChar"            ,
            "Raw"                 ,

            "RowId"               ,
            "Timestamp"           ,
            "TimestampLocal"      ,
            "TimestampWithTZ"     ,
            "VarChar"             ,

            "Byte"                ,
            "UInt16"              ,
            "UInt32"              ,
            "SByte"               ,
            "Int16"               ,

            "Int32"               ,
            "Float"               ,
            "Double"
        };

        /// <summary>MySQL 的型別代碼字串
        /// </summary>
        private static string[] _mySqlDbTypes = 
        {
            "Decimal"    ,
            "Byte"       ,
            "Int16"      ,
            "Int32"      ,
            "Float"      ,
            "Double"     ,
            "Timestamp"  ,
            "Int64"      ,
            "Int24"      ,
            "Date"       ,
            "Time"       ,
            "DateTime"   ,
            "Datetime"   ,
            "Year"       ,
            "Newdate"    ,
            "VarString"  ,
            "Bit"        ,
            "JSON"       ,
            "NewDecimal" ,
            "Enum"       ,
            "Set "       ,
            "TinyBlob"   ,
            "MediumBlob" ,
            "LongBlob"   ,
            "Blob"       ,
            "VarChar"    ,
            "String"     ,
            "Geometry"   ,
            "UByte"      ,
            "UInt16"     ,
            "UInt32"     ,
            "UInt64"     ,
            "UInt24"     ,
            "Binary"     ,
            "VarBinary"  ,
            "TinyText"   ,
            "MediumText" ,
            "LongText"   ,
            "Text"       ,
            "Guid"
        };

        /// <summary>PostgreSQL 的型別代碼字串
        /// </summary>
        private static string[] _pgSqlDbTypes = 
        {
            "Array",
            "Bigint",
            "Boolean",
            "Box",
            "Bytea",
            "Circle",
            "Char",
            "Date",
            "Double",
            "Integer",
            "Line",
            "LSeg",
            "Money",
            "Numeric",
            "Path",
            "Point",
            "Polygon",
            "Real",
            "Smallint",
            "Text",
            "Time",
            "Timestamp",
            "Varchar",
            "Refcursor",
            "Inet",
            "Bit",
            "TimestampTz",
            "Uuid",
            "Xml",
            "Oidvector",
            "Interval",
            "TimeTz",
            "Name",
            "MacAddr",
            "Json",
            "Jsonb",
            "Hstore",
            "InternalChar",
            "Varbit",
            "Unknown",
            "Oid",
            "Xid",
            "Cid",
            "Cidr",
            "TsVector",
            "TsQuery",
            "Regtype",
            "Geometry",
            "Citext",
            "Int2Vector",
            "Tid",
            "MacAddr8",
            "Geography",
            "Regconfig",
            "Range"
        };

        #endregion 宣告資料庫型別代碼的字串陣列


        #region 宣告內部的方法

        /// <summary>設定 MSSQL 資料庫的資料庫型別代碼
        /// </summary>
        /// <param name="codeCreater">資料庫型別代碼建立者</param>
        internal static void SetSqlDbTypeCode( IDbTypeCodeCreater codeCreater )
        {
            DbTypeCodeMappingElement dbTypeCodeMapping = DataAccessSettings.Settings.DbTypeCodeMapping;

            if ( dbTypeCodeMapping.IsNull() )
            {
                SetSqlDbTypeByDefault();
            }

            if ( !SetDbTypeCodeFromConfig<SqlDbTypeConfigCollection>( dbTypeCodeMapping, _sqlDbTypeCodes, "SqlDbTypeManpping", "MSSQL", codeCreater ) )
            {
                SetSqlDbTypeByDefault();
            }
        }

        /// <summary>設定 Oracle 資料庫的資料庫型別代碼
        /// </summary>
        /// <param name="codeCreater">資料庫型別代碼建立者</param>
        internal static void SetOracleDbTypeCode( IDbTypeCodeCreater codeCreater )
        {
            DbTypeCodeMappingElement dbTypeCodeMapping = DataAccessSettings.Settings.DbTypeCodeMapping;

            if ( dbTypeCodeMapping.IsNull() )
            {
                SetOracleDbTypeByDefault();
            }

            if ( !SetDbTypeCodeFromConfig<OracleTypeConfigCollection>( dbTypeCodeMapping, _orlaceDbTypes, "OracleDbTypeMapping", "Oracle", codeCreater ) )
            {
                SetOracleDbTypeByDefault();
            }
        }

        /// <summary>設定 MySQL 資料庫的資料庫型別代碼
        /// </summary>
        /// <param name="codeCreater">資料庫型別代碼建立者</param>
        internal static void SetMySqlDbTypeCode( IDbTypeCodeCreater codeCreater )
        {
            DbTypeCodeMappingElement dbTypeCodeMapping = DataAccessSettings.Settings.DbTypeCodeMapping;

            if ( dbTypeCodeMapping.IsNull() )
            {
                SetMySqlDbTypeByDefault();
            }

            if ( !SetDbTypeCodeFromConfig<MySqlDbTypeConfigCollection>( dbTypeCodeMapping, _mySqlDbTypes, "MySqlDbTypeMapping", "MySQL", codeCreater ) )
            {
                SetMySqlDbTypeByDefault();
            }
        }

        /// <summary>設定 PostgreSQL 資料庫的資料庫型別代碼
        /// </summary>
        /// <param name="codeCreater">資料庫型別代碼建立者</param>
        internal static void SetPgSqlDbTypeCode( IDbTypeCodeCreater codeCreater )
        {
            DbTypeCodeMappingElement dbTypeCodeMapping = DataAccessSettings.Settings.DbTypeCodeMapping;

            if ( dbTypeCodeMapping.IsNull() )
            {
                SetPgSqlDbTypeByDefault();
            }

            if ( !SetDbTypeCodeFromConfig<MySqlDbTypeConfigCollection>( dbTypeCodeMapping, _pgSqlDbTypes, "PgSqlDbTypeMapping", "PostgreSQL", codeCreater ) )
            {
                SetPgSqlDbTypeByDefault();
            }
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>使用 Config 組態值進行設定 DbTypeCode 代碼
        /// </summary>
        /// <typeparam name="TCollection">資料庫型別對應 Config 區段泛型</typeparam>
        /// <param name="dbTypeMapping">資料庫型別對應 Config 元素</param>
        /// <param name="sectionName">MSSQL、MySQL 或 PostgreSQL 資料庫 Config 設定區段</param>
        /// <param name="dbTypeStrings">資料庫的型別代碼字串</param>
        /// <param name="dbProviderName">MSSQL、MySQL 或 PostgreSQL 資料庫供應商名稱</param>
        /// <param name="codeCreater">資料庫型別代碼建立者</param>
        private static bool SetDbTypeCodeFromConfig<TCollection>( DbTypeCodeMappingElement dbTypeMapping, string[] dbTypeStrings, string sectionName, 
                string dbProviderName, IDbTypeCodeCreater codeCreater )
            where TCollection : ConfigurationElementCollection
        {
            #region 反射取得 Config 設定區段集合

            TCollection mappings = default ( TCollection );

            try
            {
                mappings = dbTypeMapping.GetType().GetProperty( sectionName ).GetValue( dbTypeMapping ) as TCollection;
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( "DbTypeCodeInitializer", ex, $"ZayniFramework.DataAccess set DbTypeCode from config occur exception. {ex.ToString()}", "DbTypeCodeInitializer.SetDbTypeCodeFromConfig" );
                return false;
            }

            // 20141212 Edited by Pony: 這邊應該還要檢查Config區段的數量，如果是0，代表應該要回傳false，讓DbTypeCode使用框架預設值
            if ( mappings.IsNull() || 0 == mappings.Count )
            {
                return false;
            }

            #endregion 反射取得 Config 設定區段集合

            #region 設定 DbTypeCode 資料庫型別代碼

            foreach ( var s in mappings )
            {
                #region 檢查 Config 的型別設定值是否存在

                DbTypeMappingElement mapping = (DbTypeMappingElement)s;

                string name  = mapping.Name  + "";
                string value = mapping.Value + "";

                if ( value.IsNullOrEmpty() )
                {
                    continue;
                }

                if ( !dbTypeStrings.Contains( value ) ) 
                {
                    continue;
                }

                #endregion 檢查 Config 的型別設定值是否存在

                #region 設定 DbTypeCode 代碼

                try
                {
                    var typeCode = codeCreater.Create( value );
                    var field    = typeof ( DbTypeCode ).GetFields().Where( f => f.Name == name ).ToList().FirstOrDefault();
                    field.SetValue( null, typeCode );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( "DbTypeCodeInitializer", ex, $"ZayniFramework.DataAccess set DbTypeCode from config occur exception. {ex.ToString()}", "DbTypeCodeInitializer.SetDbTypeCodeFromConfig" );
                    return false;
                }

                #endregion 設定 DbTypeCode 代碼
            }

            #endregion 設定DbTypeCode資料庫型別代碼

            return true;
        }

        /// <summary>使用程式預設值進行設定 MSSQL 資料庫的 DbTypeCode 代碼
        /// </summary>
        private static void SetSqlDbTypeByDefault() 
        {
            DbTypeCode.Binary 	= (int)SqlDbType.Binary;
            DbTypeCode.Byte 	= (int)SqlDbType.TinyInt;
            DbTypeCode.Boolean  = (int)SqlDbType.Bit;
            DbTypeCode.Date 	= (int)SqlDbType.Date;
            DbTypeCode.DateTime = (int)SqlDbType.DateTime;

            DbTypeCode.Double   = (int)SqlDbType.Float;
            DbTypeCode.Decimal  = (int)SqlDbType.Decimal;
            DbTypeCode.Int16    = (int)SqlDbType.SmallInt;
            DbTypeCode.Int32    = (int)SqlDbType.Int;
            DbTypeCode.Int64    = (int)SqlDbType.BigInt;

            DbTypeCode.Guid     = (int)SqlDbType.UniqueIdentifier;
            DbTypeCode.Object   = (int)SqlDbType.Variant;
            DbTypeCode.String   = (int)SqlDbType.NVarChar;
            DbTypeCode.Time     = (int)SqlDbType.Time;
            DbTypeCode.Xml      = (int)SqlDbType.Xml;
        }

        /// <summary>使用程式預設值進行設定Oracle資料庫的DbTypeCode代碼 (Oracle版的DbTypeCode有幾種欄位還不太確定!)
        /// </summary>
        private static void SetOracleDbTypeByDefault()
        {
            //DbTypeCode.Binary   = (int)OracleType.Blob;
            //DbTypeCode.Byte 	  = (int)OracleType.Byte;
            //DbTypeCode.Boolean  = (int)OracleType.Number;       // 目前網路上查到的文件，在Oracle這邊布林值的型別，NUMBER(1,0)似乎是比較正規的做法。參考聯結: http://charlesbc.blogspot.tw/2008/08/oracle-3-boolean.html 
            //DbTypeCode.Date 	  = (int)OracleType.DateTime;
            //DbTypeCode.DateTime = (int)OracleType.DateTime;

            //DbTypeCode.Double  = (int)OracleType.Double;
            //DbTypeCode.Decimal = (int)OracleType.Number;
            //DbTypeCode.Int16   = (int)OracleType.Number;
            //DbTypeCode.Int32   = (int)OracleType.Number;
            //DbTypeCode.Int64   = (int)OracleType.Number;

            //DbTypeCode.Guid   = (int)OracleType.VarChar;        // 不確定
            //DbTypeCode.Object = (int)OracleType.Blob;           // 不確定
            //DbTypeCode.String = (int)OracleType.VarChar;
            //DbTypeCode.Time   = (int)OracleType.DateTime;       // 不確定
            //DbTypeCode.Xml    = (int)OracleType.VarChar;        // 不確定
        }

        /// <summary>使用程式預設值進行設定 MySQL 資料庫的 DbTypeCode 代碼
        /// </summary>
        private static void SetMySqlDbTypeByDefault()
        {
            DbTypeCode.Binary 	= (int)MySqlDbType.Binary;
            DbTypeCode.Byte 	= (int)MySqlDbType.Byte;
            DbTypeCode.Boolean  = (int)MySqlDbType.Int32;
            DbTypeCode.Date 	= (int)MySqlDbType.Date;
            DbTypeCode.DateTime = (int)MySqlDbType.DateTime;

            DbTypeCode.Double   = (int)MySqlDbType.Float;
            DbTypeCode.Decimal  = (int)MySqlDbType.Decimal;
            DbTypeCode.Int16    = (int)MySqlDbType.Int16;
            DbTypeCode.Int32    = (int)MySqlDbType.Int32;
            DbTypeCode.Int64    = (int)MySqlDbType.Int64;

            DbTypeCode.Guid     = (int)MySqlDbType.Guid;
            DbTypeCode.Object   = (int)MySqlDbType.Blob;
            DbTypeCode.String   = (int)MySqlDbType.VarChar;
            DbTypeCode.Time     = (int)MySqlDbType.Time;
            DbTypeCode.Xml      = (int)MySqlDbType.VarChar;
        }

        /// <summary>使用程式預設值進行設定 PostgreSQL 資料庫的 DbTypeCode 代碼
        /// </summary>
        private static void SetPgSqlDbTypeByDefault()
        {
            DbTypeCode.Binary 	= (int)NpgsqlDbType.Bytea;
            DbTypeCode.Byte 	= (int)NpgsqlDbType.Smallint;
            DbTypeCode.Boolean  = (int)NpgsqlDbType.Boolean;
            DbTypeCode.Date 	= (int)NpgsqlDbType.TimestampTz;
            DbTypeCode.DateTime = (int)NpgsqlDbType.TimestampTz;

            DbTypeCode.Double   = (int)NpgsqlDbType.Numeric;
            DbTypeCode.Decimal  = (int)NpgsqlDbType.Numeric;
            DbTypeCode.Int16    = (int)NpgsqlDbType.Smallint;
            DbTypeCode.Int32    = (int)NpgsqlDbType.Integer;
            DbTypeCode.Int64    = (int)NpgsqlDbType.Bigint;

            DbTypeCode.Guid     = (int)NpgsqlDbType.Uuid;
            DbTypeCode.String   = (int)NpgsqlDbType.Varchar;
            DbTypeCode.Time     = (int)NpgsqlDbType.Time;
            DbTypeCode.Xml      = (int)NpgsqlDbType.Xml;
        }

        #endregion 宣告私有的方法
    }
}
