﻿using System;
using System.Data;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>MSSQL資料型別代碼建立者
    /// </summary>
    internal sealed class SqlDbTypeCodeCreater : IDbTypeCodeCreater
    {
        /// <summary>建立資料庫行別代碼
        /// </summary>
        /// <param name="source">資料庫行別代碼字串</param>
        /// <returns>數字資料庫行別代碼</returns>
        public int Create( string source )
        {
            int result = (int)CreateCode( source );
            return result;
        }
        
        /// <summary>建立MSSQL資料庫行別代碼
        /// </summary>
        /// <param name="source">MSSQL資料庫行別代碼字串</param>
        /// <returns>MSSQL資料庫行別代碼</returns>
        private SqlDbType CreateCode( string source )
        {
            switch ( source )
            {
                case "BigInt":
                    return SqlDbType.BigInt;

                case "Binary":
                    return SqlDbType.Binary;

                case "Bit":
                    return SqlDbType.Bit;

                case "Char":
                    return SqlDbType.Char;

                case "DateTime":
                    return SqlDbType.DateTime;

                case "Decimal":
                    return SqlDbType.Decimal;

                case "Float":
                    return SqlDbType.Float;

                case "Image":
                    return SqlDbType.Image;

                case "Int":             
                    return SqlDbType.Int;

                case "Money":
                    return SqlDbType.Money;

                case "NChar":
                    return SqlDbType.NChar;

                case "NText":
                    return SqlDbType.NText;

                case "NVarChar":
                    return SqlDbType.NVarChar;

                case "Real":
                    return SqlDbType.Real;

                case "UniqueIdentifier":
                    return SqlDbType.UniqueIdentifier;

                case "SmallDateTime":
                    return SqlDbType.SmallDateTime;

                case "SmallInt":
                    return SqlDbType.SmallInt;

                case "SmallMoney":
                    return SqlDbType.SmallMoney;

                case "Text":
                    return SqlDbType.Text;

                case "Timestamp":
                    return SqlDbType.Timestamp;

                case "TinyInt":          
                    return SqlDbType.TinyInt;

                case "VarBinary":        
                    return SqlDbType.VarBinary;

                case "VarChar":          
                    return SqlDbType.VarChar;

                case "Variant":          
                    return SqlDbType.Variant;

                case "Xml":              
                    return SqlDbType.Xml;

                case "Udt":           
                    return SqlDbType.Udt;

                case "Structured":       
                    return SqlDbType.Structured;

                case "Date":             
                    return SqlDbType.Date;

                case "Time":             
                    return SqlDbType.Time;

                case "DateTime2":        
                    return SqlDbType.DateTime2;

                case "DateTimeOffset":   
                    return SqlDbType.DateTimeOffset;

                default:
                    throw new ApplicationException( "建立MSSQL資料庫行別代碼對應失敗，請確認MSSQL列舉值中是否確實有 {0} 的項目".FormatTo( source ) );
            }
        }
    }
}
