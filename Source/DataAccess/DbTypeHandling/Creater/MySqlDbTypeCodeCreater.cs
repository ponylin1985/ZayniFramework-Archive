﻿using MySql.Data.MySqlClient;
using System;


namespace ZayniFramework.DataAccess
{
    /// <summary>MySQL 資料型別代碼建立者
    /// </summary>
    internal sealed class MySqlDbTypeCodeCreater : IDbTypeCodeCreater
    {
        /// <summary>建立資料庫型別代碼
        /// </summary>
        /// <param name="source">資料庫型別代碼字串</param>
        /// <returns>數字資料庫型別代碼</returns>
        public int Create( string source )
        {
            int result = (int)CreateCode( source );
            return result;
        }
        
        /// <summary>建立 MySQL 資料庫型別代碼
        /// </summary>
        /// <param name="source"> MySQL 資料庫型別代碼字串</param>
        /// <returns> MySQL 資料庫型別代碼</returns>
        private MySqlDbType CreateCode( string source )
        {
            switch ( source )
            {
                case "Binary":
                    return MySqlDbType.Binary;

                case "Bit":
                    return MySqlDbType.Bit;

                case "Decimal":
                    return MySqlDbType.Decimal;

                case "Byte":
                    return MySqlDbType.Byte;

                case "Int16":             
                    return MySqlDbType.Int16;

                case "Int24":             
                    return MySqlDbType.Int24;
                
                case "Int32":             
                    return MySqlDbType.Int32;

                case "Int64":             
                    return MySqlDbType.Int64;

                case "Float":
                    return MySqlDbType.Float;

                case "Double":
                    return MySqlDbType.Double;

                case "Timestamp":
                    return MySqlDbType.Timestamp;

                case "Date":
                    return MySqlDbType.Date;

                case "Time":
                    return MySqlDbType.Time;

                case "DateTime":
                    return MySqlDbType.DateTime;

                case "Year":
                    return MySqlDbType.Year;

                case "Newdate":
                    return MySqlDbType.Newdate;

                case "VarString":
                    return MySqlDbType.VarString;

                case "JSON":
                    return MySqlDbType.JSON;

                case "NewDecimal":
                    return MySqlDbType.NewDecimal;

                case "Enum":
                    return MySqlDbType.Enum;

                case "Set":
                    return MySqlDbType.Set;

                case "TinyBlob":
                    return MySqlDbType.TinyBlob;

                case "MediumBlob":
                    return MySqlDbType.MediumBlob;

                case "LongBlob":          
                    return MySqlDbType.LongBlob;

                case "Blob":        
                    return MySqlDbType.Blob;

                case "VarChar":          
                    return MySqlDbType.VarChar;

                case "String":          
                    return MySqlDbType.String;

                case "UByte":              
                    return MySqlDbType.UByte;

                case "UInt16":           
                    return MySqlDbType.UInt16;

                case "UInt32":       
                    return MySqlDbType.UInt32;

                case "UInt64":        
                    return MySqlDbType.UInt64;

                case "UInt24":   
                    return MySqlDbType.UInt24;

                case "VarBinary":   
                    return MySqlDbType.VarBinary;

                case "TinyText":   
                    return MySqlDbType.TinyText;

                case "MediumText":   
                    return MySqlDbType.MediumText;

                case "LongText":   
                    return MySqlDbType.LongText;

                case "Text":   
                    return MySqlDbType.Text;

                case "Guid":   
                    return MySqlDbType.Guid;

                default:
                    throw new ArgumentException( $"Create MySQL DbTypeCode occur error. Source: {source}." );
            }
        }
    }
}
