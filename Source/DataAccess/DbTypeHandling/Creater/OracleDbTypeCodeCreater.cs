﻿using System;


namespace ZayniFramework.DataAccess
{
    /// <summary>Oracle資料型別代碼建立者
    /// </summary>
    [Obsolete( "廢棄掉的 Oracle 相關類別", false )]
    internal sealed class OracleDbTypeCodeCreater
    {
    }
}
