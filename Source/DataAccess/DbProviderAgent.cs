﻿using System.Data;
using System.Data.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料存取處理專員
    /// </summary>
    internal sealed class DbProviderAgent
    {
        #region 宣告私有的欄位

        /// <summary>資料庫供應商
        /// </summary>
        private DbProvider _provider;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>建構子
        /// </summary>
        /// <param name="providerType">資料庫廠牌種類</param>
        public DbProviderAgent( DbProviderType providerType )
        {
            _provider = DbProviderFactory.Create( providerType );
        }

        /// <summary>解構子
        /// </summary>
        ~DbProviderAgent()
        {
            _provider = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>建立資料庫連線
        /// </summary>
        /// <returns>資料庫連線</returns>
        public DbConnection CreateDbConnection( string connectionString )
        {
            return _provider.CreateDbConnection( connectionString );
        }

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <returns>資料庫交易</returns>
        public DbTransaction BeginDbTransaction( DbConnection connection )
        {
            return _provider.BeginDbTransaction( connection );
        }

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="isolationLevel">交易格離層級</param>
        /// <returns>資料庫交易</returns>
        public DbTransaction BeginDbTransaction( DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted )
        {
            return _provider.BeginDbTransaction( connection, isolationLevel );
        }

        /// <summary>取得資料轉接器
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>資料轉接器</returns>
        public DbDataAdapter GetDataAdapter( DbCommand command )
        {
            return _provider.GetDataAdapter( command );
        }

        /// <summary>取得SQL敘述的資料庫指令
        /// </summary>
        /// <param name="commandTextOrSpName">SQL敘述字串 或 資料庫預存程序名稱</param>
        /// <param name="commandType">資料庫指令型態</param>
        /// <returns>SQL敘述的資料庫指令</returns>
        public DbCommand GetDbCommand( string commandTextOrSpName, CommandType commandType = CommandType.Text )
        {
            return _provider.GetDbCommand( commandTextOrSpName, commandType );
        }

        #endregion 宣告公開的方法
    }
}
