﻿namespace ZayniFramework.DataAccess
{
    /// <summary>資料庫供應商工廠
    /// </summary>
    internal sealed class DbProviderFactory
    {
        /// <summary>建立資料庫供應商
        /// </summary>
        /// <param name="providerType">資料庫廠牌種類</param>
        /// <returns>資料庫供應商</returns>
        internal static DbProvider Create( DbProviderType providerType )
        {
            DbProvider result = null;

            switch ( providerType )
            {
                case DbProviderType.MSSql:
                    result = new MSSqlProvider();
                    break;

                case DbProviderType.MySql:
                    result = new MySqlProvider();
                    break;

                case DbProviderType.Postgres:
                    result = new PgSqlProvider();
                    break;

                default:
                    result = new MSSqlProvider();
                    break;
            }

            return result;
        }
    }
}
