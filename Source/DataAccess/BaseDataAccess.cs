﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>資料存取類別基底。<para/>
    /// * 可根據 Zayni Framework 框架的 config 設定檔切換不同的來源資料庫。
    /// * 目前支援資料庫來源: MSSQL 或 MySQL
    /// </summary>
    public abstract class BaseDataAccess : IDataAccess
    {
        #region Private Fields

        /// <summary>資料存取物件
        /// </summary>
        private IDataAccess _dao;

        /// <summary>ORM 資料綁定器
        /// </summary>
        private OrmDataBinder _ormDataBinder;

        #endregion Private Fields
        

        #region Static Constructors

        /// <summary>靜態建構子
        /// </summary>
        static BaseDataAccess() => ConfigReader.LoadSettings();

        #endregion Static Constructors


        #region Constructors

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        public BaseDataAccess( string dbName )
        {
            if ( dbName.IsNullOrEmpty() )
            {
                throw new ArgumentException( $"Invalid argument of {nameof ( dbName )}. Can not be null or empty string." );
            }

            _ormDataBinder = new OrmDataBinder();
            _dao           = new DaoFactory().Create( dbName );
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架的資料庫連線字串 (若傳入為 Null 或空字串，SQL Log 功能將停用)</param>
        public BaseDataAccess( string dbName, string connectionString, string zayniConnectionString = null )
        {
            _ormDataBinder = new OrmDataBinder();
            _dao           = new DaoFactory().Create( dbName, connectionString, zayniConnectionString );
        }

        /// <summary>解構子
        /// </summary>
        ~BaseDataAccess()
        {
            _dao           = null;
            _ormDataBinder = null;
        }

        #endregion Constructors


        #region Public Properties

        /// <summary>執行結果訊息、執行結果錯誤訊息
        /// </summary>
        public string Message 
        {
            get => _dao.Message;
            set => _dao.Message = value;
        }
                       
        /// <summary>執行結果影響資料列數(唯獨屬性)
        /// </summary>
        public int DataCount => _dao.DataCount;

        /// <summary>執行時期異常物件
        /// </summary>
        public Exception ExceptionObject => _dao.ExceptionObject;

        /// <summary>資料連線字串
        /// </summary>
        public string ConnectionString => _dao.ConnectionString;

        #endregion Public Properties


        #region DbConnection and DbTransaction Methods

        /// <summary>建立全新的資料庫連線
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>新的資料庫連線</returns>
        public async Task<DbConnection> CreateConnectionAsync( string dbName = "" ) => await _dao.CreateConnectionAsync( dbName );

        /// <summary>建立全新的資料庫連線
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>新的資料庫連線</returns>
        public DbConnection CreateConnection( string dbName = "" ) => _dao.CreateConnection( dbName );

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">來源資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>資料庫交易</returns>
        public DbTransaction BeginTransaction( DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted ) =>
            _dao.BeginTransaction( connection, isolationLevel );

        /// <summary>確認目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否確認交易成功</returns>
        public bool Commit( DbTransaction transaction ) => _dao.Commit( transaction );
        
        /// <summary>取消目標資料庫交易
        /// </summary>
        /// <param name="transaction">目標資料庫交易</param>
        /// <returns>是否取消交易成功</returns>
        public bool Rollback( DbTransaction transaction ) => _dao.Rollback( transaction );

        #endregion DbConnection and DbTransaction Methods


        #region Public SQL Execute Methods

        /// <summary>執行 DbCommand 資料庫指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行成功</returns>
        public async Task<bool> ExecuteNonQueryAsync( DbCommand command ) => await _dao.ExecuteNonQueryAsync( command );

        /// <summary>執行 DbCommand 資料庫指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行成功</returns>
        public bool ExecuteNonQuery( DbCommand command ) => _dao.ExecuteNonQuery( command );

        /// <summary>執行資料庫 DbCommand 查詢指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>執行查詢 SQL 資料讀取器是否成功</returns>
        public async Task<Result<IDataReader>> ExecuteReaderAsync( DbCommand command ) => await _dao.ExecuteReaderAsync( command );

        /// <summary>執行資料庫 DbCommand 查詢指令。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行查詢 SQL 資料讀取器是否成功</returns>
        public bool ExecuteReader( DbCommand command, out IDataReader reader ) => _dao.ExecuteReader( command, out reader );

        /// <summary>執行 DbCommand 查詢指令，並且輸出單一純量值。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>執行是否成功</returns>
        public async Task<Result<object>> ExecuteScalarAsync( DbCommand command ) => await _dao.ExecuteScalarAsync( command );

        /// <summary>執行 DbCommand 查詢指令，並且輸出單一純量值。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="result">查詢單一純量值結果</param>
        /// <returns>執行是否成功</returns>
        public bool ExecuteScalar( DbCommand command, out object result ) => _dao.ExecuteScalar( command, out result );

        /// <summary>執行 DbCommand 查詢指令，回傳執行使否成功。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<bool> LoadDataSetAsync( DbCommand command, DataSet ds, params string[] tableNames ) => 
            await _dao.LoadDataSetAsync( command, ds, tableNames );

        /// <summary>執行 DbCommand 查詢指令，回傳執行使否成功。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="ds">資料集合</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataSet( DbCommand command, DataSet ds, params string[] tableNames ) => 
            _dao.LoadDataSet( command, ds, tableNames );

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<List<TModel>>> LoadDataToModelAsync<TModel>( DbCommand command ) where TModel : new() =>
            await _dao.LoadDataToModelAsync<TModel>( command );

        /// <summary>執行 DbCommand 查詢指令，輸出查詢結果資料模型集合。
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">資料庫指令</param>
        /// <param name="models">資料模型集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModel<TModel>( DbCommand command, out List<TModel> models ) where TModel : new() => 
            _dao.LoadDataToModel<TModel>( command, out models );

        /// <summary>執行 DbCommand 查詢指令，輸出動態物件資料集合。
        /// * ORM 資料綁定機制: 單一資料模型 ORM 對應，應用在 SQL 查詢語法中只有一個 SELECT 查詢語法，回傳單一資料表結果集合。
        /// * 對查詢單一結果資料表，進行動態 ORM 資料綁定。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<List<dynamic>>> LoadDataToDynamicModelAsync( DbCommand command ) => 
            await _dao.LoadDataToDynamicModelAsync( command );

        /// <summary>執行 DbCommand 查詢指令，輸出動態物件資料集合。
        /// * ORM 資料綁定機制: 單一資料模型 ORM 對應，應用在 SQL 查詢語法中只有一個 SELECT 查詢語法，回傳單一資料表結果集合。
        /// * 對查詢單一結果資料表，進行動態 ORM 資料綁定。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="models">動態資料模型串列集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicModel( DbCommand command, out List<dynamic> models ) => 
            _dao.LoadDataToDynamicModel( command, out models );

        /// <summary>執行 DbCommand 查詢指令，依照指定的 Type 陣列回傳多組 ORM 綁定的資料模型集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<List<object[]>>> LoadDataToModelsAsync( DbCommand command, Type[] modelTypes ) => 
            await _dao.LoadDataToModelsAsync( command, modelTypes );

        /// <summary>執行 DbCommand 查詢指令，依照指定的 Type 陣列回傳多組 ORM 綁定的資料模型集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="result">查詢結果資料模型集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModels( DbCommand command, Type[] modelTypes, out List<object[]> result ) => 
            _dao.LoadDataToModels( command, modelTypes, out result );

        /// <summary>執行 DbCommand 查詢指令，DbCommand 的 SQL 查詢語法可以支援多組 SELECT 查詢，並且輸出多組動態 ORM 資料綁定的查詢結果集合。
        /// </summary>
        /// <param name="command">資料庫指令
        /// * Key 值: 依照順序傳入的 tableName 參數名稱。
        /// * Value 值: 動態物件資料模型串列。
        /// </param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<Result<Dictionary<string, List<dynamic>>>> LoadDataToDynamicCollectionAsync( DbCommand command, params string[] tableName ) =>
            await _dao.LoadDataToDynamicCollectionAsync( command, tableName );

        /// <summary>執行 DbCommand 查詢指令，DbCommand 的 SQL 查詢語法可以支援多組 SELECT 查詢，並且輸出多組動態 ORM 資料綁定的查詢結果集合。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelCollection">查詢結果的資料集合，動態物件字典集合。<para/>
        /// * Key 值: 依照順序傳入的 tableName 參數名稱。
        /// * Value 值: 動態物件資料模型串列。
        /// </param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicCollection( DbCommand command, out Dictionary<string, List<dynamic>> modelCollection, params string[] tableName ) =>
            _dao.LoadDataToDynamicCollection( command, out modelCollection, tableName );

        /// <summary>執行 DbCommand 查詢指令，輸出自定義的 ModelSet 資料集合中。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <returns>是否執行查詢成功</returns>
        public async Task<bool> LoadModelSetAsync( DbCommand command, ModelSet modelSet, params Type[] modelTypes ) => 
            await _dao.LoadModelSetAsync( command, modelSet, modelTypes );

        /// <summary>執行 DbCommand 查詢指令，輸出自定義的 ModelSet 資料集合中。
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadModelSet( DbCommand command, ModelSet modelSet, params Type[] modelTypes ) =>
            _dao.LoadModelSet( command, modelSet, modelTypes );

        #endregion Public SQL Execute Methods


        #region Public DbCommand Methods

        /// <summary>取得資料庫指令物件
        /// </summary>
        /// <param name="commandText">SQL 敘述字串</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>資料庫指令</returns>
        public DbCommand GetSqlStringCommand( string commandText, DbConnection connection = null, DbTransaction transaction = null ) => 
            _dao.GetSqlStringCommand( commandText, connection, transaction );

        /// <summary>取得資料庫預存程序的資料庫指令
        /// </summary>
        /// <param name="storedProcName">資料庫預存程序名稱</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>資料庫指令</returns>
        public DbCommand GetStoredProcCommand( string storedProcName, DbConnection connection = null, DbTransaction transaction = null ) =>
            _dao.GetStoredProcCommand( storedProcName, connection, transaction );

        /// <summary>新增資料庫指令參數
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbTypeCode 靜態常數)</param>
        /// <param name="value">參數值</param>
        public DbCommand AddInParameter( DbCommand command, string parameterName, int dbType, object value ) =>
            _dao.AddInParameter( command, parameterName, dbType, value );

        /// <summary>新增資料庫指令的輸出參數
        /// </summary>
        /// <param name="command">資料庫指令</param>
        /// <param name="parameterName">資料庫指令的參數名稱</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的 DbTypeCode 靜態常數)</param>
        /// <param name="maxSize">變數的最大長度</param>
        public DbCommand AddOutParameter( DbCommand command, string parameterName, int dbType, int maxSize ) => 
            _dao.AddOutParameter( command, parameterName, dbType, maxSize );

        /// <summary>檢查是否包含指定的資料庫參數
        /// </summary>
        /// <param name="command">來源資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>是否包含目標參數</returns>
        public bool ContainsParameter( DbCommand command, string parameterName ) =>
            _dao.ContainsParameter( command, parameterName );

        /// <summary>取得資料庫指令參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>資料庫參數值</returns>
        public object GetParameterValue( DbCommand command, string parameterName ) =>
            _dao.GetParameterValue( command, parameterName );

        /// <summary>設定資料庫指令的參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <param name="value">參數值</param>
        /// <returns>設定參數值是否成功</returns>
        public bool SetParameterValue( DbCommand command, string parameterName, object value ) =>
            _dao.SetParameterValue( command, parameterName, value );

        /// <summary>清除資料庫指令的所有參數
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <returns>是否清除成功</returns>
        public bool ClearAllParameters( DbCommand command ) => 
            _dao.ClearAllParameters( command );

        /// <summary>取得資料庫的空字串值
        /// </summary>
        /// <returns>資料庫的空字串值</returns>
        public string GetDBEmptyString() => _dao.GetDBEmptyString();

        /// <summary>取得資料庫的空字串值 (在Config檔的DataAccessSettings/DatabaseProviders區段中，需要正確組態dataBaseProvider)
        /// </summary>
        /// <param name="dbName">資料庫連線名稱 (必須要組態在Config檔的DataAccessSettings/DatabaseProviders區段中)</param>
        /// <returns></returns>
        public static string GetDbEmptyString( string dbName )
        {
            string provider = "MSSQL";

            try
            {
                provider = DataAccessSettings.Settings.DatabaseProviders[ dbName ].DatabaseProvider + "";
            }
            catch ( Exception )
            {
                provider = "MSSQL";
            }

            switch ( provider.ToUpper() )
            {
                case "MSSQL":
                    return "";
                
                case "ORACLE":
                    return " ";

                default:
                    return " ";
            }

        }

        /// <summary>取得Oracle資料庫的空字串值
        /// </summary>
        /// <returns>Oracle資料庫的空字串值</returns>
        public static string GetOracleEmptyString()
        {
            return " ";
        }

        #endregion Public DbCommand Methods
    }
}
