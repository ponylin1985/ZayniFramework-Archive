﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>ZayniFramework DataAccess 模組的 Config 設定值讀取器
    /// </summary>
    internal static class ConfigReader
    {
        #region Public Methods

        /// <summary>讀取資料庫連線字串
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>讀取結果</returns>
        public static Result<string> GetDbConnectionString( string dbName )
        {
            var result   = Result.Create<string>();
            var logTitle = GetLogTitle( nameof ( GetDbConnectionString ) );

            var connectionString = "";

            try
            {
                var r = ConfigManagement.GetDbConnectionString( dbName );

                if ( !r.Success )
                {
                    result.Message = r.Message;
                    Logger.WriteErrorLog( nameof ( ConfigReader ), $"{result.Message}", logTitle );
                    ConsoleLogger.LogError( $"{logTitle} {result.Message}" );
                    return result;
                }

                connectionString = r.Data;

                result.Data    = connectionString;
                result.Success = true;
                return result;
            }
            catch ( Exception ex )
            {
                result.Message = $"Get db connection string in config file occur exception. dbName: {dbName}. {ex.ToString()}";
                Logger.WriteErrorLog( nameof ( ConfigReader ), $"{result.Message}", logTitle );
                ConsoleLogger.LogError( $"{logTitle} {result.Message}" );
                return result;
            }
        }

        #endregion Public Methods


        #region Internal Methods

        /// <summary>讀取 DataAccess 資料存取模組的 Config 設定值
        /// </summary>
        internal static void LoadSettings()
        {
            try
            {
                ZayniConfigSection zayniConfig = ConfigManagement.ZayniConfigs;
                DataAccessSettings.Settings    = zayniConfig.DataAccessSettings;
            }
            catch ( Exception ex )
            {
                string errorMsg = $"Load DataAccessSettings config section in config file occur exception. {ex.ToString()}";
                Logger.WriteExceptionLog( nameof ( LoadSettings ), ex, eventTitle: GetLogTitle( nameof ( LoadSettings ) ), message: errorMsg );
                ConsoleLogger.LogError( $"{GetLogTitle( nameof ( GetDbConnectionString ) )} {errorMsg}" );
                throw ex;
            }
        }

        /// <summary>讀取資料存取連線 Config 設定
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        /// <returns>資料存取連線 Config 設定</returns>
        internal static ConnectionElement GetDbProviderConfig( string dbName ) 
        {
            try
            {
                ZayniConfigSection zayniConfig = ConfigManagement.ZayniConfigs;
                DataAccessSettings.Settings    = zayniConfig.DataAccessSettings;

                foreach ( var config in DataAccessSettings.Settings.DatabaseProviders )
                {
                    var cfg = (ConnectionElement)config;
                    
                    if ( cfg.Name == dbName )
                    {
                        return cfg;
                    }
                }

                return null;
            }
            catch ( Exception ex )
            {
                string errorMsg = $"Load DataAccessSettings/DatabaseProviders config in config file occur exception. DbName: {dbName}. {ex.ToString()}";
                Logger.WriteExceptionLog( nameof ( GetDbProviderConfig ), ex, eventTitle: GetLogTitle( nameof ( GetDbProviderConfig ) ), message: errorMsg );
                ConsoleLogger.LogError( $"{GetLogTitle( nameof ( GetDbProviderConfig ) )} {errorMsg}" );
                throw ex;
            }
        }

        #endregion Internal Methods


        #region Private Methods

        /// <summary>取得日誌的標題
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的標題</returns>
        private static string GetLogTitle( string methodName ) => $"{nameof ( ConfigReader )}.{methodName}";

        #endregion Private Methods
    }
}
