using Npgsql;
using NpgsqlTypes;
using System.Data;
using System.Data.Common;
using System.Text;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>PostgreSQL 的 SQL Profile 日誌記錄員
    /// </summary>
    internal sealed class PgSqlLogger : BaseLogger
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        /// <param name="dbName">資料庫名稱</param>
        public PgSqlLogger( string dbName ) : base( dbName )
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫名稱</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架資料庫連線字串</param>
        public PgSqlLogger( string dbName, string zayniConnectionString ) : base( dbName, zayniConnectionString )
        {
        }

        #endregion 宣告建構子


        #region 實作公開的抽象

        /// <summary>從來源的資料庫指令中擷取要進行SQL紀錄的語法
        /// </summary>
        /// <param name="command">來源資料庫指令</param>
        /// <returns>SQL語法</returns>
        public override string FetchLogCommandText( DbCommand command )
        {
            var sbCommandText = new StringBuilder( command.CommandText );

            if ( CommandType.StoredProcedure == command.CommandType )
            {
                sbCommandText.Insert( 0, " EXEC {0} ".FormatTo( sbCommandText ) );
            }

            foreach ( DbParameter p in command.Parameters )
            {
                NpgsqlParameter parameter = (NpgsqlParameter)p;

                if (
                        ( parameter.NpgsqlDbType != NpgsqlDbType.Smallint )  &&
                        ( parameter.NpgsqlDbType != NpgsqlDbType.Integer )   &&
                        ( parameter.NpgsqlDbType != NpgsqlDbType.Bigint )    &&
                        ( parameter.NpgsqlDbType != NpgsqlDbType.Double )    &&
                        ( parameter.NpgsqlDbType != NpgsqlDbType.Numeric )
                   )
                {
                    if ( CommandType.StoredProcedure == command.CommandType )
                    {
                        sbCommandText.AppendFormat( " {0} = '{1}', ", parameter.ParameterName, parameter.Value.ToString() );
                    }
                    else
                    {
                        sbCommandText = sbCommandText.Replace( parameter.ParameterName, "'{0}'".FormatTo( parameter.Value.ToString() ) );
                    }
                }
                else
                {
                    if ( CommandType.StoredProcedure == command.CommandType )
                    {
                        sbCommandText.AppendFormat( " {0} = {1}, ", parameter.ParameterName, parameter.Value.ToString() );
                    }
                    else
                    {
                        sbCommandText = sbCommandText.Replace( parameter.ParameterName, parameter.Value + "" );
                    }
                }
            }

            string commandText = sbCommandText.ToString();

            if (  CommandType.StoredProcedure == command.CommandType )
            {
                commandText = commandText.Substring( 0, sbCommandText.Length - 1 );
            }

            return commandText;
        }

        #endregion 實作公開的抽象
    }
}