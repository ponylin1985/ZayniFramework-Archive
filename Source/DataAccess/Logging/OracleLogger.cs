﻿using System;
using System.Data;
using System.Data.Common;
using System.Text;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>Oracle的SQL日誌記錄員
    /// </summary>
    [Obsolete( "廢棄掉的 Oracle 相關類別", false )]
    internal sealed class OracleLogger : BaseLogger
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        /// <param name="dbName">資料庫名稱</param>
        public OracleLogger( string dbName ) : base( dbName )
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫名稱</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架資料庫連線字串</param>
        public OracleLogger( string dbName, string zayniConnectionString ) : base( dbName, zayniConnectionString )
        {
            // pass
        }

        #endregion 宣告建構子


        #region 實作公開的抽象

        /// <summary>從來源的資料庫指令中擷取要進行SQL紀錄的語法
        /// </summary>
        /// <param name="command">來源資料庫指令</param>
        /// <returns>SQL語法</returns>
        public override string FetchLogCommandText( DbCommand command )
        {
            throw new NotImplementedException();

            // var commandText = new StringBuilder( command.CommandText );

            // if ( command.CommandType == CommandType.StoredProcedure )
            // {
            //     commandText.Insert( 0, "Exec " );
            // }

            // foreach ( DbParameter p in command.Parameters )
            // {
            //     OracleParameter parameter = (OracleParameter)p;

            //     if ( parameter.Direction == ParameterDirection.Output || parameter.Direction == ParameterDirection.ReturnValue )
            //     {
            //         continue;
            //     }

            //     if ( "LogID_Update" == parameter.ParameterName )
            //     {
            //         continue;
            //     }

            //     if ( "LogID_Approve" == parameter.ParameterName )
            //     {
            //         continue;
            //     }

            //     if ( parameter.OracleType != OracleType.Number )
            //     {
            //         if ( command.CommandType == CommandType.StoredProcedure )
            //         {
            //             commandText.AppendFormat( " {0} = '{1}' , ", parameter.ParameterName, parameter.Value );
            //         }
            //         else
            //         {
            //             commandText = commandText.Replace( " :{0} ".FormatTo( parameter.ParameterName ), " '{0}' ".FormatTo( parameter.Value + "" ) );
            //         }
            //     }
            //     else
            //     {
            //         if ( command.CommandType == CommandType.StoredProcedure )
            //         {
            //             commandText.AppendFormat( " {0} = {1} , ", parameter.ParameterName, parameter.Value );
            //         }
            //         else
            //         {
            //             commandText = commandText.Replace( " :{0} ".FormatTo( parameter.ParameterName ), parameter.Value + "" );
            //         }
            //     }
            // }

            // if ( command.CommandType == CommandType.StoredProcedure )
            // {
            //     commandText = commandText.Remove( commandText.Length - 1, 1 );
            // }

            // return commandText.ToString();
        }

        #endregion 實作公開的抽象
    }
}
