﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Threading.Tasks;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>MSSQL 的 SQL Profile 日誌記錄資料存取類別
    /// </summary>
    internal class MSSqlLoggerDao : LoggerDao
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="connectionString">ZayniFramework 框架的資料庫連線字串</param>
        public MSSqlLoggerDao( string connectionString ) : base( connectionString )
        {
        }

        #endregion 宣告建構子


        #region 實作公開的抽象

        /// <summary>寫入 SQL Profile 日誌記錄到資料庫中
        /// </summary>
        /// <param name="model">SQL Statement 日誌記錄資訊</param>
        public override async Task WriteLogAsync( SQLLogInfo model )
        {
            SqlConnection conn = null;

            try
            {
                string sql = @" 
                    --  Insert FS_SQL_LOG
                    INSERT INTO FS_SQL_LOG (
                        DAO_NAME,
                        COMMAND_ID,
                        COMMAND_DIRECTION,
                        COMMAND_TYPE,
                        COMMAND_TXT,

                        COMMAND_RESULT,
                        HOST_NAME,
                        LOG_TIME
                    ) VALUES (
                        @DaoName,
                        @CommandId,
                        @CommandDirection,
                        @CommandType,
                        @CommandTxt,

                        @CommandResult,
                        @HostName,
                        @LogTime
                    ) ";

                using ( conn = new SqlConnection( base.ConnectionString ) )
                {
                    await conn.OpenAsync();

                    var cmd = new SqlCommand( sql, conn );
                    cmd.CommandTimeout = 60;
                    
                    AddInParameter( cmd, "@DaoName",          SqlDbType.NVarChar, model.DaoName );
                    AddInParameter( cmd, "@CommandId",        SqlDbType.NVarChar, model.CommandId );
                    AddInParameter( cmd, "@CommandDirection", SqlDbType.NVarChar, model.CommandDirection );
                    AddInParameter( cmd, "@CommandType",      SqlDbType.Int,      model.CommandType );
                    AddInParameter( cmd, "@CommandTxt",       SqlDbType.NVarChar, model.CommandText );

                    AddInParameter( cmd, "@CommandResult",    SqlDbType.NVarChar, model.CommandResult );
                    AddInParameter( cmd, "@HostName",         SqlDbType.NVarChar, model.HostName );
                    AddInParameter( cmd, "@LogTime",          SqlDbType.DateTime, new SqlDateTime( model.LogTime ).Value );

                    await cmd.ExecuteNonQueryAsync();
                    conn.Close();
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Insert MSSQL command SQL log to FS_SQL_LOG occur exception." );
            }
        }

        #endregion 實作公開的抽象


        #region 宣告私有的方法

        /// <summary>加入 SQL 敘述的參數
        /// </summary>
        /// <param name="cmd">目標SQL指令</param>
        /// <param name="parameterName">參數名稱</param>
        /// <param name="dbType">參數資料庫型別</param>
        /// <param name="value">參數值</param>
        private void AddInParameter( SqlCommand cmd, string parameterName, SqlDbType dbType, object value )
        {
            cmd.Parameters.Add( parameterName, dbType );
            cmd.Parameters[ parameterName ].Value     = value ?? DBNull.Value;
            cmd.Parameters[ parameterName ].Direction = ParameterDirection.Input;
        }

        #endregion 宣告私有的方法
    }
}
