﻿using System;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>Oracle的SQL日誌記錄資料存取類別
    /// </summary>
    [Obsolete( "廢棄掉的 Oracle 相關類別", false )]
    internal class OracleLoggerDao : LoggerDao
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="connectionString">ZayniFramework 框架的資料庫連線字串</param>
        public OracleLoggerDao( string connectionString ) : base( connectionString )
        {
        }

        #endregion 宣告建構子


        #region 實作公開的抽象

        /// <summary>寫入SQL日誌記錄到資料庫中
        /// </summary>
        /// <param name="model">SQL日誌記錄資訊</param>
        public override void WriteLog( SQLLogInfo model )
        {
            try
            {
                // using ( var connection = new OracleConnection( base.ConnectionString ) ) 
                // {
                //     string sql = @" INSERT INTO FS_SQL_LOG (
                //                         COMMAND_TYPE,
                //                         COMMAND_TXT,
                //                         HOST_NAME,
                //                         LOG_TIME
                //                     ) VALUES (
                //                         :CommandType,
                //                         :CommandTxt,
                //                         :HostName,
                //                         :LogTime
                //                     ) ";

                //     connection.Open();
                //     OracleTransaction trans = connection.BeginTransaction();
                
                //     OracleCommand cmd = new OracleCommand( sql, connection, trans );
                //     AddInParameter( cmd, ":CommandType", OracleType.Number,   model.CommandType );
                //     AddInParameter( cmd, ":CommandTxt",  OracleType.VarChar,  model.CommandText );
                //     AddInParameter( cmd, ":HostName",    OracleType.VarChar,  model.HostName );
                //     AddInParameter( cmd, ":LogTime",     OracleType.DateTime, model.LogTime );

                //     Action handler = cmd.ExecuteNonQuery() > 0 ? (Action)trans.Commit : (Action)trans.Rollback;
                //     handler();
                // }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, " ZayniFramework.DataAccess 框架執行SQL Log日誌記錄發生異常。" );
            }
        }

        #endregion 實作公開的抽象


        #region 宣告私有的方法

        // /// <summary>加入SQL敘述的參數
        // /// </summary>
        // /// <param name="cmd">目標SQL指令</param>
        // /// <param name="parameterName">參數名稱</param>
        // /// <param name="dbType">參數資料庫型別</param>
        // /// <param name="value">參數值</param>
        // private void AddInParameter( OracleCommand cmd, string parameterName, OracleType dbType, object value )
        // {
        //     cmd.Parameters.Add( parameterName, dbType );
        //     cmd.Parameters[ parameterName ].Value     = value ?? DBNull.Value;
        //     cmd.Parameters[ parameterName ].Direction = ParameterDirection.Input;
        // }

        #endregion 宣告私有的方法
    }
}
