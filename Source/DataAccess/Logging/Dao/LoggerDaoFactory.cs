﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>SQL日誌記錄資料存取類別工廠
    /// </summary>
    internal class LoggerDaoFactory
    {
        #region 宣告私有的靜態欄位

        /// <summary>預設資料庫連線字串名稱
        /// </summary>
        private static string CONNECTION_STRING_NAME = "Zayni";

        /// <summary>資料庫連線字串
        /// </summary>
        private static string _connectionString;

        #endregion 宣告私有的靜態欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static LoggerDaoFactory()
        {
            try
            {
                var g = ConfigReader.GetDbConnectionString( CONNECTION_STRING_NAME );

                if ( g.Success )
                {
                    _connectionString = g.Data;
                }
            }
            catch
            {
                // pass
            }
        }

        #endregion 宣告靜態建構子


        #region 宣告公開的靜態方法

        /// <summary>建立SQL日誌記錄資料存取物件
        /// </summary>
        /// <returns>SQL日誌記錄資料存取物件</returns>
        public static LoggerDao Create( out string zayniConnectionString )
        {
            zayniConnectionString = _connectionString;
            string zayniProvider  = "mssql";

            try
            {
                zayniProvider = DataAccessSettings.Settings.DatabaseProviders[ CONNECTION_STRING_NAME ].DatabaseProvider;
            }
            catch ( Exception ex )
            {
                Logger.WriteWarningLog( "LoggerDaoFactory", $"ZayniFramework/DataAccessSettings/DatabaseProviders config error. {ex.ToString()}", "LoggerDaoFactory.Create" );
            }

            switch ( zayniProvider.ToLower() )
            {
                case "mssql":
                    return new MSSqlLoggerDao( _connectionString );

                case "mysql":
                    return new MySqlLoggerDao( _connectionString );

                case "postgresql":
                    return new PgSqlLoggerDao( _connectionString );

                default:
                    return null;
            }
        }

        /// <summary>建立SQL日誌記錄資料存取物件
        /// </summary>
        /// <param name="zayniConnectionString">ZayniFramework 框架的資料庫連線字串</param>
        /// <returns>SQL日誌記錄資料存取物件</returns>
        public static LoggerDao Create( string zayniConnectionString = null )
        {
            zayniConnectionString.IsNotNullOrEmpty( s => _connectionString = zayniConnectionString );
            string zayniProvider = DataAccessSettings.Settings.DatabaseProviders[ CONNECTION_STRING_NAME ].DatabaseProvider;

            switch ( zayniProvider.ToLower() )
            {
                case "mssql":
                    return new MSSqlLoggerDao( _connectionString );

                case "mysql":
                    return new MySqlLoggerDao( _connectionString );

                case "postgresql":
                    return new PgSqlLoggerDao( _connectionString );

                default:
                    return null;
            }
        }

        #endregion 宣告公開的靜態方法
    }
}
