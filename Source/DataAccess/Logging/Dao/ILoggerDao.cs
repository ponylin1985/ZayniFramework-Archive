﻿using System.Threading.Tasks;


namespace ZayniFramework.DataAccess
{
    /// <summary>SQL日誌記錄資料存取介面
    /// </summary>
    internal interface ILoggerDao
    {
        /// <summary>寫入SQL日誌記錄到資料庫中
        /// </summary>
        /// <param name="model">SQL日誌記錄資訊</param>
        Task WriteLogAsync( SQLLogInfo model );
    }
}
