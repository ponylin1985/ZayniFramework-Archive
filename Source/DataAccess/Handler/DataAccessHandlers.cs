﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>執行ADO.NET資料庫非查詢的資料庫指令的委派
    /// </summary>
    /// <returns>受影響的資料筆數</returns>
    public delegate int ExecuteNonQueryHandler();

    /// <summary>執行ADO.NET資料庫非查詢的資料庫指令的委派
    /// </summary>
    /// <returns>受影響的資料筆數</returns>
    public delegate Task<int> ExecuteNonQueryAsyncHandler();

    /// <summary>執行ADO.NET資料庫查詢的資料庫指令的委派
    /// </summary>
    /// <returns>資料讀取器</returns>
    public delegate IDataReader ExecuteReaerHandler();

    /// <summary>執行ADO.NET資料庫查詢的資料庫指令的委派
    /// </summary>
    /// <returns>資料讀取器</returns>
    public delegate Task<IDataReader> ExecuteReaderAsyncHandler();

    /// <summary>執行ADO.NET資料庫單一純量查詢指令的委派
    /// </summary>
    /// <returns>單一純量結果值</returns>
    public delegate object ExecuteScalarHandler();

    /// <summary>執行ADO.NET資料庫單一純量查詢指令的委派
    /// </summary>
    /// <returns>單一純量結果值</returns>
    public delegate Task<object> ExecuteScalarAsyncHandler();

    /// <summary>執行載入DataSet查詢的處理委派
    /// </summary>
    /// <param name="ds">資料集合</param>
    /// <returns>查詢使否成功</returns>
    public delegate bool ExecuteDataSetHandler( DataSet ds );

    /// <summary>執行載入DataSet查詢的處理委派
    /// </summary>
    /// <param name="ds">資料集合</param>
    /// <returns>查詢使否成功</returns>
    public delegate Task<bool> ExecuteDataSetAsyncHandler( DataSet ds );

    /// <summary>執行IDataReader查詢的處理委派
    /// </summary>
    /// <param name="reader">資料讀取器</param>
    /// <returns>查詢使否成功</returns>
    public delegate bool ExecReaderHandler( out IDataReader reader );

    /// <summary>執行IDataReader查詢的處理委派
    /// </summary>
    /// <returns>查詢使否成功</returns>
    public delegate Task<Result<IDataReader>> ExecReaderAsyncHandler();

    /// <summary>執行載入資料模型集合查詢的處理委派
    /// </summary>
    /// <param name="models">資料模型集合列表</param>
    /// <returns>查詢使否成功</returns>
    public delegate bool ExecuteModelsHandler( out List<object[]> models );

    /// <summary>執行載入資料模型集合查詢的處理委派
    /// </summary>
    /// <returns>查詢使否成功</returns>
    public delegate Task<Result<List<object[]>>> ExecuteModelsAsyncHandler();
}
