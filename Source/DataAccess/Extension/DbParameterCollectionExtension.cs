using System;
using System.Data.Common;
using System.Linq;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess
{
    /// <summary>DbParameterCollection 的擴充方法類別
    /// </summary>
    public static class DbParameterCollectionExtension
    {
        /// <summary>取得所有 DbParameter 的名稱與參數值的 log 日誌訊息
        /// </summary>
        /// <param name="parameters">DbParameter 參數集合</param>
        /// <returns>所有 DbParameter 的名稱與參數值的 log 日誌訊息</returns>
        public static string GetParametersLog( this DbParameterCollection parameters ) 
        {
            try
            {
                if ( parameters.IsNullOrEmptyList() )
                {
                    return string.Empty;
                }

                return string.Join( ",", parameters?.Cast<DbParameter>()?.Select( p => $"{p.ParameterName} = {p.Value}{Environment.NewLine}" ) );
            }
            catch ( Exception )
            {
                return string.Empty;
            }
        }
    }
}