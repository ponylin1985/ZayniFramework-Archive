﻿using MySql.Data.MySqlClient;
using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using ZayniFramework.Common;
using ZayniFramework.Common.Dynamic;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>MySQL 資料存取類別基底 (only for MySQL... too bad... )<para/>
    /// * 建議繼承 BaseDataAccess，並且在 config 設定檔中的 ZayniFramework/DataAccessSettings/DatabaseProviders 段落，將 dataBaseProvider 屬性設定為 MySQL。同樣可以對 MySQL 資料庫進行存取。
    /// * 或是改為繼承 BaseMySqlDao 基底類別，同樣可以對 MySQL 資料庫進行存取。
    /// * 但此 BaseMySqlDal 的查詢 ORM 資料繫結機制會比 BaseMySqlDao 效能稍快一些。
    /// </summary>
    public abstract class BaseMySqlDal
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>資料庫 SQL 指令池
        /// </summary>
        private readonly DbCommandPool _dbCommands = new DbCommandPool();

        /// <summary>MySQL的SQL日誌記錄員
        /// </summary>
        private MySqlLogger _sqlLog;

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static BaseMySqlDal() => ConfigReader.LoadSettings();

        #endregion 宣告靜態建構子


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="connectionStringName">資料庫連線字串名稱</param>
        public BaseMySqlDal( string connectionStringName )
        {
            if ( connectionStringName.IsNullOrEmpty() )
            {
                Message = $"{nameof ( BaseMySqlDal )} ctor argument '{nameof ( connectionStringName )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, Message, $"{nameof ( BaseMySqlDal )}.ctor" );
                throw new ArgumentNullException( Message );
            }

            _sqlLog = new MySqlLogger( connectionStringName );

            try
            {
                var g = ConfigManagement.GetDbConnectionString( connectionStringName );

                if ( !g.Success )
                {
                    Message = $"Get db connection string occur error. {g.Message}";
                    Logger.WriteErrorLog( this, Message, $"{nameof ( BaseMySqlDal )}.ctor" );
                    throw new ConfigurationErrorsException( Message );
                }

                ConnectionString = g.Data;
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} ctor load connection string '{connectionStringName}' from config file occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, $"{nameof ( BaseMySqlDal )}.ctor" );
                throw new ConfigurationErrorsException( Message );
            }

            if ( string.IsNullOrWhiteSpace( ConnectionString ) )
            {
                Message = $"{nameof ( BaseMySqlDal )} ctor connection string '{connectionStringName}' is null or empty string. Please check '{connectionStringName}' in config again.";
                Logger.WriteErrorLog( this, Message, $"{nameof ( BaseMySqlDal )}.ctor" );
                throw new ConfigurationErrorsException( Message );
            }
        }

        /// <summary>解構子
        /// </summary>
        ~BaseMySqlDal()
        {
            _sqlLog          = null;
            ConnectionString = null;
            Message          = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>資料庫連線字串
        /// </summary>
        private string _connectionString;

        /// <summary>資料庫連線字串
        /// </summary>
        public string ConnectionString
        {
            get
            {
                using ( _asyncLock.Lock() )
                {
                    return _connectionString;
                }
            }
            private set
            {
                using ( _asyncLock.Lock() )
                {
                    _connectionString = value;
                }
            }
        }

        /// <summary>訊息
        /// </summary>
        private string _message;

        /// <summary>訊息
        /// </summary>
        public string Message
        {
            get
            {
                using ( _asyncLock.Lock() )
                {
                    return _message;
                }
            }
            protected set
            {
                using ( _asyncLock.Lock() )
                {
                    _message = value;
                }
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告保護的方法

        /// <summary>取得SQL資料庫的指令
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="sql">SQL 指令字串</param>
        /// <param name="timeout">資料庫指令執行 Timeout 秒數</param>
        /// <returns>MySQ L資料庫的指令</returns>
        protected MySqlCommand GetSqlStringCommand( MySqlConnection connection, string sql, int timeout = 60 )
        {
            return new MySqlCommand() 
            {
                Connection     = connection,
                CommandType    = CommandType.Text,
                CommandText    = sql,
                CommandTimeout = timeout
            };
        }

        /// <summary>加入 SQL 指令的輸入參數
        /// </summary>
        /// <param name="command">資料庫命令</param>
        /// <param name="parameterName">SQL參數名稱</param>
        /// <param name="dbType">資料欄位型別</param>
        /// <param name="value">資料值</param>
        protected void AddInParameter( MySqlCommand command, string parameterName, MySqlDbType dbType, object value )
        {
            if ( new string[] { command + "", parameterName }.Any( s => string.IsNullOrWhiteSpace( s ) ) )
            {
                throw new ArgumentException( $"{nameof ( BaseMySqlDal )}.{nameof ( AddInParameter )} method argument error: Argument '{nameof ( command )}' or '{nameof ( parameterName )}' can not be null or empty string." );
            }

            try
            {
                command.Parameters?.Add( parameterName, dbType );
                command.Parameters[ parameterName ].Value     = value ?? DBNull.Value;
                command.Parameters[ parameterName ].Direction = ParameterDirection.Input;
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} AddInParameter occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( AddInParameter ) ) );
                throw ex;
            }
        }

        /// <summary>清空資料庫指令中的參數值
        /// </summary>
        /// <param name="command">資料庫指令</param>
        protected void ClearSqlParameters( MySqlCommand command )
        {
            try
            {
                command?.Parameters?.Clear();
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} clear MySqlParameters collection occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( ClearSqlParameters ) ) );
                throw ex;
            }
        }

        /// <summary>建立資料庫連線
        /// </summary>
        /// <returns>MySQL 資料庫連線</returns>
        protected MySqlConnection CreateConnection() => new MySqlConnection( ConnectionString );

        /// <summary>根據連線字串名稱建立資料庫連線
        /// </summary>
        /// <param name="connectionStringName">資料庫連線字串名稱</param>
        /// <returns>MySQL 資料庫連線</returns>
        protected MySqlConnection CreateConnection( string connectionStringName )
        {
            var g = ConfigManagement.GetDbConnectionString( connectionStringName );

            if ( !g.Success )
            {
                Message = $"Get db connection string occur error. {g.Message}";
                Logger.WriteErrorLog( this, Message, Logger.GetTraceLogTitle( this, nameof ( CreateConnection ) ) );
                throw new ConfigurationErrorsException( Message );
            }

            string connectionString = g.Data;
            return new MySqlConnection( connectionString );
        }

        /// <summary>開啟資料庫連線
        /// </summary>
        /// <param name="connection">MySQL 資料庫連線</param>
        /// <returns>是否成功開啟資料庫連線</returns>
        protected bool OpenConnection( MySqlConnection connection )
        {
            if ( connection?.State == ConnectionState.Open )
            {
                return true;
            }

            try
            {
                connection.Open();
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} open database connection occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( OpenConnection ) ) );
                return false;
            }

            return true;
        }

        /// <summary>建立並且開啟資料庫連線。<para/>
        /// 1. 執行成功，則會回傳已經開啟連線的MySQL資料庫連線物件。<para/>
        /// 2. 執行失敗，則會回傳 Null 值，失敗原因可從 Message 屬性取得。
        /// </summary>
        /// <returns>開啟資料庫連線</returns>
        protected MySqlConnection CreateOpenConnection()
        {
            MySqlConnection connection = CreateConnection();

            if ( !OpenConnection( connection ) )
            {
                return null;
            }

            return connection;
        } 
        
        /// <summary>建立並且開啟資料庫連線。<para/>
        /// 1. 執行成功，則會回傳已經開啟連線的MySQL資料庫連線物件。<para/>
        /// 2. 執行失敗，則會回傳 Null 值，失敗原因可從 Message 屬性取得。
        /// </summary>
        /// <param name="connectionStringName">資料庫連線字串名稱</param>
        /// <returns>開啟資料庫連線</returns>
        protected MySqlConnection CreateOpenConnection( string connectionStringName )
        {
            MySqlConnection connection = CreateConnection( connectionStringName );

            if ( !OpenConnection( connection ) )
            {
                return null;
            }

            return connection;
        }

        /// <summary>關閉並且釋放資料庫連線
        /// </summary>
        /// <param name="connection">MySQL資料庫連線</param>
        protected void DisposeConnection( MySqlConnection connection )
        {
            try
            {
                connection?.Close();
                connection?.Dispose();
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} close and dispose database connection occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( DisposeConnection ) ) );
                throw ex;
            }
        }

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">MySQL 資料庫連線</param>
        /// <returns>MySQL 資料庫交易</returns>
        protected MySqlTransaction BeginTransaction( MySqlConnection connection )
        {
            MySqlTransaction transaction;

            try
            {
                transaction = connection.BeginTransaction();
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} begin database transaction occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( BeginTransaction ) ) );
                return null;
            }

            if ( null == transaction )
            {
                Message = $"{nameof ( BaseMySqlDal )} begin database transaction occur exception: MySqlTransaction is null.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( BeginTransaction ) ) );
                return null;
            }

            return transaction;
        }

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">MySQL資料庫連線</param>
        /// <param name="isolationLevel">交易隔離層級</param>
        /// <returns>MySQL 資料庫交易</returns>
        protected MySqlTransaction BeginTransaction( MySqlConnection connection, IsolationLevel isolationLevel )
        {
            MySqlTransaction transaction;

            try
            {
                transaction = connection.BeginTransaction( isolationLevel );
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} begin database transaction occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( BeginTransaction ) ) );
                return null;
            }

            if ( null == transaction )
            {
                Message = $"{nameof ( BaseMySqlDal )} begin database transaction occur exception: MySqlTransaction is null.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( BeginTransaction ) ) );
                return null;
            }

            return transaction;
        }

        /// <summary>確認資料庫交易
        /// </summary>
        /// <param name="transaction">MySQL 資料庫交易</param>
        /// <returns>確認資料庫交易是否成功</returns>
        protected bool Commit( MySqlTransaction transaction )
        {
            try
            {
                transaction.Commit();
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} commit database transaction occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( Commit ) ) );
                return false;
            }

            return true;
        }

        /// <summary>退回資料庫交易
        /// </summary>
        /// <param name="transaction">MySQL 資料庫交易</param>
        /// <returns>退回資料庫交易是否成功</returns>
        protected bool Rollback( MySqlTransaction transaction )
        {
            try
            {
                transaction.Rollback();
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} rollback database transaction occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( Rollback ) ) );
                return false;
            }

            return true;
        }

        /// <summary>執行查詢
        /// </summary>
        /// <param name="command">資料庫命令</param>
        /// <param name="tableName">查詢結果資料表名稱</param>
        /// <param name="ds">資料集合</param>
        /// <returns>執行查詢機制是否成功</returns>
        protected bool LoadDataSet( MySqlCommand command, string tableName, out DataSet ds )
        {
            ds = new DataSet();

            if ( tableName.IsNullOrEmpty() )
            {
                Message = $"{nameof ( BaseMySqlDal )} execute query fail due to argument '{nameof ( tableName )}' is null or empty string.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( LoadDataSet ) ) );
                return false;
            }

            try
            {
                if ( _dbCommands.Add( command, out string commandId ) )
                {
                    _sqlLog?.WriteDbCommandRequestLog( commandId, command, GetType().Name );
                }

                using ( var adapter = new MySqlDataAdapter( command ) )
                {
                    adapter.Fill( ds, tableName );
                }

                if ( commandId.IsNotNullOrEmpty() )
                {
                    _sqlLog?.WriteDbCommandResultLog( commandId, command, ds, GetType().Name );
                    _dbCommands.Remove( commandId );
                }
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} execute query SQL occur exception: {ex.ToString()}{Environment.NewLine}SQL Statement:{Environment.NewLine}{command.CommandText}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( LoadDataSet ) ) );
                return false;
            }
            
            return true;
        }

        /// <summary>執行查詢
        /// </summary>
        /// <param name="command">資料庫命令</param>
        /// <param name="tableNames">查詢結果資料表名稱陣列</param>
        /// <param name="ds">資料集合</param>
        /// <returns>執行查詢機制是否成功</returns>
        protected bool LoadDataSet( MySqlCommand command, string[] tableNames, out DataSet ds )
        {
            ds = new DataSet();

            if ( tableNames.IsNullOrEmptyArray() )
            {
                Message = $"{nameof ( BaseMySqlDal )} execute query fail due to argument '{nameof ( tableNames )}' is null or empty string.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( LoadDataSet ) ) );
                return false;
            }

            try
            {
                if ( _dbCommands.Add( command, out string commandId ) )
                {
                    _sqlLog?.WriteDbCommandRequestLog( commandId, command, GetType().Name );
                }

                using ( var adapter = new MySqlDataAdapter( command ) )
                {
                    string rootName = "Table";

                    for ( int i = 0, len = tableNames.Length; i < len; i++ )
                    {
                        string tableName = i == 0 ? rootName : rootName + i;
                        adapter.TableMappings.Add( tableName, tableNames[ i ] );
                    }

                    adapter.Fill( ds );
                }

                if ( commandId.IsNotNullOrEmpty() )
                {
                    _sqlLog?.WriteDbCommandResultLog( commandId, command, ds, GetType().Name );
                    _dbCommands.Remove( commandId );
                }
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} execute query SQL occur exception: {ex.ToString()}{Environment.NewLine}SQL Statement:{Environment.NewLine}{command.CommandText}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( LoadDataSet ) ) );
                return false;
            }
            
            return true;
        }

        /// <summary>執行 SQL 查詢指令
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">MySQL 資料庫命令</param>
        /// <param name="models">資料模型集合</param>
        /// <returns>執行查詢機制是否成功</returns>
        protected bool LoadData<TModel>( MySqlCommand command, out TModel models )
        {
            models = default ( TModel );

            if ( !LoadDataSet( command, "Table1", out DataSet ds ) )
            {
                return false;
            }

            DataTable dt = ds.Tables[ "Table1" ];
             
            try
            {
                // 據說這樣用 Json.NET 來做到 ORM 的效果，效能好像很好...
                string json = JsonConvert.SerializeObject( dt );
                models      = JsonConvert.DeserializeObject<TModel>( json );
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} load data from DataTable to {typeof ( TModel ).Name} object occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( LoadData ) ) );
                return false;
            }

            if ( null == models )
            {
                Message = $"{nameof ( BaseMySqlDal )} load data from DataTable to {typeof ( TModel ).Name} object fail.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( LoadData ) ) );
                return false;
            }
            
            return true;
        }

        /// <summary>執行 SQL 查詢指令
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">MySQL 資料庫命令</param>
        /// <param name="detailTables">子資料表查詢的屬性名稱陣列</param>
        /// <param name="models">資料模型集合</param>
        /// <returns>執行查詢機制是否成功</returns>
        protected bool LoadMasterDetailData<TModel>( MySqlCommand command, string[] detailTables, out List<TModel> models )
        {
            models = default ( List<TModel> );

            var tableNames = new List<string>() { "__master__" };
            tableNames.AddRange( detailTables );

            if ( !LoadDataSet( command, tableNames.ToArray(), out DataSet ds ) )
            {
                return false;
            }

            Type masterType     = typeof ( TModel );
            Type masterListType = typeof ( List<TModel> );

            try
            {
                foreach ( DataTable dt in ds.Tables )
                {
                    PropertyInfo prop = masterType.GetProperty( dt.TableName );

                    if ( prop.IsNull() )
                    {
                        models = (List<TModel>)JsonConvert.DeserializeObject( JsonConvert.SerializeObject( dt ), masterListType );
                        continue;
                    }

                    Type type = prop.PropertyType;

                    string json = JsonConvert.SerializeObject( dt );
                    object obj  = JsonConvert.DeserializeObject( json, type );

                    prop.SetValue( models.FirstOrDefault(), obj );
                }
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} load data from DataTable to {typeof ( TModel ).Name} object occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( LoadMasterDetailData ) ) );
                return false;
            }

            if ( models.IsNull() )
            {
                Message = $"{nameof ( BaseMySqlDal )} load data from DataTable to {typeof ( TModel ).Name} object fail.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( LoadMasterDetailData ) ) );
                return false;
            }

            return true;
        }

        /// <summary>執行 SQL 查詢指令
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">MySQL 資料庫命令</param>
        /// <param name="tableNames">資料表名稱陣列</param>
        /// <param name="result">查詢結果集</param>
        /// <returns>執行查詢機制是否成功</returns>
        protected bool LoadDataDynamic<TModel>( MySqlCommand command, string[] tableNames, out dynamic result )
        {
            result = DynamicHelper.CreateDynamicObject();

            if ( !LoadDataSet( command, tableNames, out DataSet ds ) )
            {
                return false;
            }

            Type masterType     = typeof ( TModel );
            Type masterListType = typeof ( List<TModel> );

            try
            {
                foreach ( DataTable dt in ds.Tables )
                {
                    PropertyInfo prop = masterType.GetProperty( dt.TableName );

                    object value = prop.IsNull() ? 
                        JsonConvert.DeserializeObject( JsonConvert.SerializeObject( dt ), masterListType ) :
                        JsonConvert.DeserializeObject( JsonConvert.SerializeObject( dt ), prop.PropertyType );

                    DynamicHelper.BindProperty( result, dt.TableName, value );
                }
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} load data from DataTable to {typeof ( TModel ).Name} object occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( LoadDataDynamic ) ) );
                return false;
            }

            return true;
        }

        /// <summary>執行非查詢的 SQL 指令
        /// </summary>
        /// <param name="command">MySQL 資料庫命令</param>
        /// <param name="count">執行 SQL 指令後受影響的資料筆數</param>
        /// <returns>執行 SQL 指令是否成功</returns>
        protected bool ExecuteNonQuery( MySqlCommand command, out int count )
        {
            count = -1;

            try
            {
                if ( _dbCommands.Add( command, out string commandId ) )
                {
                    _sqlLog?.WriteDbCommandRequestLog( commandId, command, GetType().Name );
                }

                count = command.ExecuteNonQuery();

                if ( commandId.IsNotNullOrEmpty() )
                {
                    _sqlLog?.WriteDbCommandResultLog( commandId, command, count, GetType().Name );
                    _dbCommands.Remove( commandId );
                }
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )} execute SQL occur exception: {ex.ToString()}{Environment.NewLine}SQL Statement:{Environment.NewLine}{command.CommandText}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( ExecuteNonQuery ) ) );
                return false;
            }

            return true;
        }

        /// <summary>執行 SQL 單一純量查詢
        /// </summary>
        /// <param name="command">MySQL資料庫命令</param>
        /// <param name="value">純量值</param>
        /// <returns>執行查詢是否成功</returns>
        protected bool ExecuteScalar( MySqlCommand command, out object value )
        {
            value = null;

            try
            {
                if ( _dbCommands.Add( command, out string commandId ) )
                {
                    _sqlLog?.WriteDbCommandRequestLog( commandId, command, GetType().Name );
                }

                value = command.ExecuteScalar();

                if ( commandId.IsNotNullOrEmpty() )
                {
                    _sqlLog?.WriteDbCommandResultLog( commandId, command, value, GetType().Name );
                    _dbCommands.Remove( commandId );
                }
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )}.{nameof ( ExecuteScalar )} execute SQL occur exception: {ex.ToString()}{Environment.NewLine}SQL Statement:{Environment.NewLine}{command.CommandText}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( ExecuteScalar ) ) );
                return false;
            }

            return true;
        }

        /// <summary>執行 SQL 單一純量查詢
        /// </summary>
        /// <typeparam name="TResult">純量值泛型</typeparam>
        /// <param name="command">MySQL資料庫命令</param>
        /// <param name="result">純量值</param>
        /// <returns>執行查詢是否成功</returns>
        protected bool ExecuteScalar<TResult>( MySqlCommand command, out TResult result )
        {
            result = default ( TResult );

            if ( !ExecuteScalar( command, out object obj ) )
            {
                return false;
            }

            try
            {
                result = (TResult)obj;
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )}.{nameof ( ExecuteScalar )} cast result value occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( ExecuteScalar ) ) );
                return false;
            }
            
            return true;
        }

        /// <summary>執行 SQL 單一整數純量值查詢，假若回傳為 Int32.MinValue 即代表異常。
        /// </summary>
        /// <param name="command">MySQL 資料庫命令</param>
        /// <returns>整數純量值，假若回傳為 Int32.MinValue 即代表異常。</returns>
        protected int ExecuteScalar( MySqlCommand command )
        {
            if ( !ExecuteScalar( command, out object obj ) )
            {
                return Int32.MinValue;
            }

            int result;

            try
            {
                result = int.Parse( obj + "" );
            }
            catch ( Exception ex )
            {
                Message = $"{nameof ( BaseMySqlDal )}.{nameof ( ExecuteScalar )} cast result to Int32 occur exception: {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( ExecuteScalar ) ) );
                return Int32.MinValue;
            }

            return result;
        }

        #endregion 宣告保護的方法


        #region 宣告私有的方法

        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( BaseMySqlDal )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
