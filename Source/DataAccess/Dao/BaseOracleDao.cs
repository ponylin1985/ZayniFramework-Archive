﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess
{
    /// <summary>Oracle 資料庫存取基底元件
    /// </summary>
    [Obsolete( "This is an obsolete object. Do NOT use this class anymore. The zayni framework is not supported BaseOracleDao_Obsolete.", true )]
    public class BaseOracleDao_Obsolete : BaseDao, IDataAccess, IDaoExceptionHandling
    {
        #region 宣告私有的欄位

        /// <summary>MSSQL的SQL日誌記錄員
        /// </summary>
        private OracleLogger _logger;

        /// <summary>ZayniFramework 框架資料庫連線字串
        /// </summary>
        private string _zayniConnectionString;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        public BaseOracleDao_Obsolete( string dbName ) : base( DbProviderType.Oracle )
        {
            _logger          = new OracleLogger( dbName );

            base.DbName      = dbName;
            base.OrmBinder   = new OrmDataBinder();

            InTransaction = false;
            base.LoadConnectionString();
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架的資料庫連線字串 (若傳入為Null或空字串，SQL Log功能將停用)</param>
        public BaseOracleDao_Obsolete( string dbName, string connectionString, string zayniConnectionString = null ) : base( DbProviderType.Oracle )
        {
            _zayniConnectionString = zayniConnectionString;
            _logger                = new OracleLogger( dbName, zayniConnectionString );
            base.ConnectionString  = connectionString;
            base.OrmBinder         = new OrmDataBinder();
            InTransaction          = false;
        }

        /// <summary>解構子
        /// </summary>
        ~BaseOracleDao_Obsolete()
        {
            _logger               = null;
            base.Transaction      = null;
            base.DbName           = null;
            base.OrmBinder        = null;
            base.Connection       = null;
            base.ConnectionString = null;
            InTransaction         = false;
        }

        #endregion 宣告建構子


        #region 實作公開的抽象

        /// <summary>取得Oracle資料庫的空字串值
        /// </summary>
        /// <returns>Oracle資料庫的空字串值</returns>
        public override string GetDBEmptyString()
        {
            return " ";
        }

        #endregion 實作公開的抽象


        #region 實作資料庫的連線交易方法

        /// <summary>使用 ZayniFramework.DataAccess 框架內部的連線開啟資料庫交易
        /// </summary>
        /// <returns>是否成功開起交易</returns>
        public bool BeginTransaction()
        {
            return base.DoBeginTransaction( null );
        }

        /// <summary>使用 ZayniFramework.DataAccess 框架內部的連線開啟資料庫交易
        /// </summary>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>是否成功開起交易</returns>
        public bool BeginTransaction( IsolationLevel isolationLevel = IsolationLevel.ReadCommitted )
        {
            return base.DoBeginTransaction( null, isolationLevel );
        }

        /// <summary>根據來源的資料庫連線開啟資料庫交易，傳入的來源資料庫連線不可以為Null值
        /// </summary>
        /// <remarks>傳入的來源資料庫連線不可以為Null值</remarks>
        /// <param name="connection">來源資料庫連線 (不可以為Null值)</param>
        /// <returns>是否成功開起交易</returns>
        public bool BeginTransaction( DbConnection connection )
        {
            if ( connection.IsNull() )
            {
                Message = "傳入的來源資料庫連線為Null， ZayniFramework.DataAccess 框架無法從外界傳入的連線可啟資料庫交易";
                Logger.WriteErrorLog( this, "傳入的來源資料庫連線為Null， ZayniFramework.DataAccess 框架無法從外界傳入的連線可啟資料庫交易", "BaseOracleDao.BeginTransaction" );
                return false;
            }
            
            return base.DoBeginTransaction( connection );
        }

        /// <summary>根據來源的資料庫連線開啟資料庫交易，傳入的來源資料庫連線不可以為Null值
        /// </summary>
        /// <remarks>傳入的來源資料庫連線不可以為Null值</remarks>
        /// <param name="connection">來源資料庫連線 (不可以為Null值)</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>是否成功開起交易</returns>
        public bool BeginTransaction( DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted )
        {
            if ( connection.IsNull() )
            {
                Message = "傳入的來源資料庫連線為Null， ZayniFramework.DataAccess 框架無法從外界傳入的連線可啟資料庫交易";
                Logger.WriteErrorLog( this, "傳入的來源資料庫連線為Null， ZayniFramework.DataAccess 框架無法從外界傳入的連線可啟資料庫交易", "BaseOracleDao.BeginTransaction" );
                return false;
            }
            
            return base.DoBeginTransaction( connection, isolationLevel );
        }

        /// <summary>使用 ZayniFramework.DataAccess 框架內部的連線開啟資料庫交易
        /// </summary>
        /// <returns>開啟資料庫交易結果</returns>
        public Result<DbTransaction> BeginDbTransaction()
        {
            return base.DoBeginDbTransaction( null );
        }

        /// <summary>使用 ZayniFramework.DataAccess 框架內部的連線開啟資料庫交易
        /// </summary>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>開啟資料庫交易結果</returns>
        public Result<DbTransaction> BeginDbTransaction( IsolationLevel isolationLevel = IsolationLevel.ReadCommitted )
        {
            return base.DoBeginDbTransaction( null, isolationLevel );
        }

        /// <summary>根據來源的資料庫連線開啟資料庫交易，傳入的來源資料庫連線不可以為Null值
        /// </summary>
        /// <remarks>傳入的來源資料庫連線不可以為Null值</remarks>
        /// <param name="connection">來源資料庫連線 (不可以為Null值)</param>
        /// <returns>開啟資料庫交易結果</returns>
        public Result<DbTransaction> BeginDbTransaction( DbConnection connection )
        {
            if ( connection.IsNull() )
            {
                Message = "傳入的來源資料庫連線為Null， ZayniFramework.DataAccess 框架無法從外界傳入的連線可啟資料庫交易";
                Logger.WriteErrorLog( this, Message, "BaseOracleDao.BeginTransaction" );
                return new Result<DbTransaction>() 
                {
                    IsSuccess = false,
                    Message   = this.Message
                };
            }
            
            return base.DoBeginDbTransaction( connection );
        }

        /// <summary>根據來源的資料庫連線開啟資料庫交易，傳入的來源資料庫連線不可以為Null值
        /// </summary>
        /// <remarks>傳入的來源資料庫連線不可以為Null值</remarks>
        /// <param name="connection">來源資料庫連線 (不可以為Null值)</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>開啟資料庫交易結果</returns>
        public Result<DbTransaction> BeginDbTransaction( DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted )
        {
            if ( connection.IsNull() )
            {
                Message = "傳入的來源資料庫連線為Null， ZayniFramework.DataAccess 框架無法從外界傳入的連線可啟資料庫交易";
                Logger.WriteErrorLog( this, Message, "BaseOracleDao.BeginTransaction" );
                return new Result<DbTransaction>() 
                {
                    IsSuccess = false,
                    Message   = this.Message
                };
            }
            
            return base.DoBeginDbTransaction( connection, isolationLevel );
        }

        #endregion 實作資料庫的連線交易方法


        #region 宣告執行SQL敘述的私有方法

        /// <summary>執行目標SQL敘述命令
        /// </summary>
        /// <param name="command">目標SQL敘述命令</param>
        /// <returns>執行目標SQL命令是否成功</returns>
        private bool DoExecuteNonQuery( OracleCommand command )
        {
            bool isSuccess = base.DoExecuteNonQuery( command, command.ExecuteNonQuery );
            
            if ( isSuccess )
            {
                _logger.IsNull( () => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger( base.DbName ) : new OracleLogger( base.DbName, _zayniConnectionString ) );
                _logger?.WriteLog( command );
            }

            return isSuccess;
        }

        /// <summary>執行目標SQL敘述，並且取得資料讀取器
        /// </summary>
        /// <param name="command">目標SQL敘述命令</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行目標SQL命令是否成功</returns>
        private bool DoExecuteReader( OracleCommand command, out IDataReader reader )
        {
            bool isSuccess = base.DoExecuteReader( command, command.ExecuteReader, out reader );

            if ( isSuccess )
            {
                _logger.IsNull( () => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger( base.DbName ) : new OracleLogger( base.DbName, _zayniConnectionString ) );
                _logger?.WriteLog( command );
            }

            return isSuccess;
        }

        /// <summary>執行目標SQL敘述，並且查詢單一純量值
        /// </summary>
        /// <param name="command">目標SQL敘述命令</param>
        /// <param name="obj">查詢結果(單一純量值)</param>
        /// <param name="transaction">(外界傳入的)資料庫交易</param>
        /// <returns>執行目標SQL命令是否成功</returns>
        private bool DoExecuteScalar( OracleCommand command, out object obj, DbTransaction transaction = null )
        {
            bool isSuccess = base.DoExecuteScalar( command, command.ExecuteScalar, out obj, transaction );

            if ( isSuccess )
            {
                _logger.IsNull( () => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger( base.DbName ) : new OracleLogger( base.DbName, _zayniConnectionString ) );
                _logger?.WriteLog( command );
            }

            return isSuccess;
        }

        /// <summary>執行Sql查詢指令，回傳執行使否成功
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="ds">要填入的DataSet</param>
        /// <param name="needDispose">是否需要進行自動資源釋放</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        private bool DoLoadDataSet( OracleCommand command, DataSet ds, bool needDispose, params string[] tableNames )
        {
            #region 針對Oracle資料庫的PLSQL查詢語法，如果是MultiSelect，多個查詢的情況的特別處理

            if ( tableNames.Length > 1 )
            {
                var sbDeclare  = new StringBuilder( "" );
                var sbSelects  = new StringBuilder( "" );
                var sbOutParas = new StringBuilder( "" );

                try
                {
                    string[] selectStrings = command.CommandText.FetchSubstring( "SELECT", ";", true, true ).ToArray();

                    for ( int i = 0, len = tableNames.Length; i < len ; i++ )
                    {
                        var strCursorVar = "cur{0}".FormatTo( i );
                        var sbCursorVar  = new StringBuilder( "{0} {1}".FormatTo( strCursorVar, " SYS_REFCURSOR; " ) );
                        sbDeclare.Append( sbCursorVar );

                        var sbCursorSelect = new StringBuilder( " OPEN {0} FOR {1} ".FormatTo( strCursorVar, selectStrings[ i ] ) );
                        sbSelects.Append( sbCursorSelect );

                        var sbParameter = new StringBuilder( " :paraCur{0} := {1}; ".FormatTo( i, strCursorVar ) );
                        sbOutParas.Append( sbParameter );
                    }

                    var sbDeclareCur = new StringBuilder( " DECLARE " ).Append( sbDeclare );

                    var sql   = " {0} {1} ".FormatTo( sbSelects.ToString(), sbOutParas.ToString() );
                    var sbSql = new StringBuilder( " BEGIN {0} END; ".FormatTo( sql ) );

                    command.CommandText = ( sbDeclareCur.Append( sbSql ).ToString() );

                    for ( int j = 0, len = tableNames.Length; j < len ; j++ )
                    {
                        AddOutParameter( command, "paraCur{0}".FormatTo( j ), (int)OracleType.Cursor, int.MaxValue );
                    }
                }
                catch ( Exception ex )
                {
                    Logger.WriteErrorLog( this, "BaseOracleDao.DoLoadDataSet 在處理PLSQL MultiSelect語法時發生程式異常: {0}".FormatTo( ex.ToString() ), "BaseOracleDao.DoLoadDataSet" );
                    return base.DoLoadDataSet( command, ds, needDispose, tableNames );
                }
            }

            #endregion 針對Oracle資料庫的PLSQL查詢語法，如果是MultiSelect，多個查詢的情況的特別處理

            bool isSuccess = base.DoLoadDataSet( command, ds, needDispose, tableNames );

            if ( isSuccess )
            {
                _logger.IsNull( () => _logger = _zayniConnectionString.IsNullOrEmpty() ? new OracleLogger( base.DbName ) : new OracleLogger( base.DbName, _zayniConnectionString ) );
                _logger?.WriteLog( command );
            }

            return isSuccess;
        }

        #endregion 宣告執行SQL敘述的私有方法

        
        #region 宣告執行SQL的實體方法

        #region 宣告ExecuteNonQuery執行SQL指令方法

        /// <summary>執行Sql命令並回傳是否執行成功，如果成功，可以在DataCount屬性取得受影響的列數 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <remarks>
        /// 1. 此方法對於資料庫的連線資源釋放，無論是使用元件內部的連線或交易，或是由外部傳入資料庫交易，都對外界保留完全的自由度讓外界自行進行資源釋放
        /// 2. 此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線
        /// </remarks>
        /// <param name="command">要執行的命令</param>
        /// <returns>是否執行成功</returns>
        public bool ExecuteNonQuery( DbCommand command )
        {
            OracleCommand cmd = command as OracleCommand;

            if ( cmd.IsNull() )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, null, null ) )
            {
                return false;
            }

            return DoExecuteNonQuery( cmd );
        }

        /// <summary>執行Sql命令並回傳是否執行成功，如果成功，可以在DataCount屬性取得受影響的列數 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <remarks>
        /// 1. 此方法對於資料庫的連線資源釋放，無論是使用元件內部的連線或交易，或是由外部傳入資料庫交易，都對外界保留完全的自由度讓外界自行進行資源釋放
        /// 2. 此方法會開起外界傳入的資料庫連線，傳入的資料庫連線不可以為Null值
        /// </remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <returns>是否執行成功</returns>
        public bool ExecuteNonQuery( DbCommand command, DbConnection connection )
        {
            OracleCommand cmd = command as OracleCommand;

            if ( cmd.IsNull() )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, connection, null ) )
            {
                return false;
            }

            return DoExecuteNonQuery( cmd );
        }

        /// <summary>執行Sql命令並回傳是否執行成功，如果成功，可以在DataCount屬性取得受影響的列數 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <remarks>
        /// 1. 此方法對於資料庫的連線資源釋放，無論是使用元件內部的連線或交易，或是由外部傳入資料庫交易，都對外界保留完全的自由度讓外界自行進行資源釋放
        /// 2. 此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線
        /// </remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>是否執行成功</returns>
        public bool ExecuteNonQuery( DbCommand command, DbTransaction transaction )
        {
            OracleCommand     cmd   = command     as OracleCommand;
            OracleTransaction trans = transaction as OracleTransaction;

            if ( !base.Check<OracleCommand, OracleTransaction>( command, transaction, "OracleCommand", "OracleTransaction" ) )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, null, trans ) )
            {
                return false;
            }

            return DoExecuteNonQuery( cmd );
        }

        /// <summary>執行Sql命令並回傳是否執行成功，如果成功，可以在DataCount屬性取得受影響的列數 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <remarks>
        /// 1. 此方法對於資料庫的連線資源釋放，無論是使用元件內部的連線或交易，或是由外部傳入資料庫交易，都對外界保留完全的自由度讓外界自行進行資源釋放
        /// 2. 此方法會開起外界傳入的資料庫連線，傳入的資料庫連線不可以為Null值
        /// </remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>是否執行成功</returns>
        public bool ExecuteNonQuery( DbCommand command, DbConnection connection, DbTransaction transaction )
        {
            OracleCommand     cmd   = command     as OracleCommand;
            OracleTransaction trans = transaction as OracleTransaction;

            if ( !base.Check<OracleCommand, OracleTransaction>( command, transaction, "OracleCommand", "OracleTransaction" ) )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, connection, trans ) )
            {
                return false;
            }
            
            return DoExecuteNonQuery( cmd );
        }

        #endregion 宣告ExecuteNonQuery執行SQL指令方法

        #region 宣告ExecuteReader查詢方法

        /// <summary>執行Sql命令並回傳資料讀取器 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <remarks>此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線</remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行查詢SQL資料讀取器是否成功</returns>
        public bool ExecuteReader( DbCommand command, out IDataReader reader )
        {
            reader = null;

            OracleCommand cmd = command as OracleCommand;

            if ( cmd.IsNull() )
            {
                Message = "傳入的command參數無法轉型為OracleCommand。";
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, null, null ) )
            {
                return false;
            }

            return DoExecuteReader( cmd, out reader );
        }

        /// <summary>執行Sql命令並回傳資料讀取器 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <remarks>此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線</remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="reader">資料讀取器</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <returns>執行查詢SQL資料讀取器是否成功</returns>
        public bool ExecuteReader( DbCommand command, out IDataReader reader, DbConnection connection )
        {
            reader = null;

            OracleCommand cmd = command as OracleCommand;

            if ( cmd.IsNull() )
            {
                Message = "傳入的command參數無法轉型為OracleCommand。";
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, connection, null ) )
            {
                return false;
            }

            return DoExecuteReader( cmd, out reader );
        }

        /// <summary>執行Sql命令並回傳資料讀取器 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <remarks>此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線</remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="reader">資料讀取器</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>執行查詢SQL資料讀取器是否成功</returns>
        public bool ExecuteReader( DbCommand command, out IDataReader reader, DbTransaction transaction )
        {
            reader = null;

            OracleCommand     cmd   = command     as OracleCommand;
            OracleTransaction trans = transaction as OracleTransaction;

            if ( !base.Check<OracleCommand, OracleTransaction>( command, transaction, "OracleCommand", "OracleTransaction" ) )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, null, trans ) )
            {
                return false;
            }

            return DoExecuteReader( cmd, out reader );
        }

        /// <summary>執行Sql命令並回傳資料讀取器 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <remarks>此方法會開起外界傳入的資料庫連線，傳入的資料庫連線不可以為Null值</remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="reader">資料讀取器</param>
        /// <returns>執行查詢SQL資料讀取器是否成功</returns>
        public bool ExecuteReader( DbCommand command, DbConnection connection, DbTransaction transaction, out IDataReader reader )
        {
            reader = null;

            OracleCommand     cmd   = command     as OracleCommand;
            OracleTransaction trans = transaction as OracleTransaction;

            if ( !base.Check<OracleCommand, OracleTransaction>( command, transaction, "OracleCommand", "OracleTransaction" ) )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, connection, trans ) )
            {
                return false;
            }

            return DoExecuteReader( cmd, out reader );
        }

        #endregion 宣告ExecuteReader查詢方法

        #region 宣告ExecuteScalar單一純量查詢方法

        /// <summary>執行Sql命令並回傳執行結果 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <param name="command">要執行的命令</param>
        /// <param name="result">執行結果物件</param>
        /// <returns>執行是否成功</returns>
        public bool ExecuteScalar( DbCommand command, out object result )
        {
            result = null;
            
            OracleCommand cmd = command as OracleCommand;

            if ( cmd.IsNull() )
            {
                Message = "傳入的command參數無法轉型為OracleCommand。";
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, null, null ) )
            {
                return false;
            }

            return DoExecuteScalar( cmd, out result, null );
        }

        /// <summary>執行Sql命令並回傳執行結果 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <remarks>此方法會開起外界傳入的資料庫連線，傳入的資料庫連線不可以為Null值</remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="result">執行結果物件</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <returns>執行是否成功</returns>
        public bool ExecuteScalar( DbCommand command, out object result, DbConnection connection )
        {
            result = null;
            
            OracleCommand cmd = command as OracleCommand;

            if ( cmd.IsNull() )
            {
                Message = "傳入的command參數無法轉型為OracleCommand。";
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, connection, null ) )
            {
                return false;
            }

            return DoExecuteScalar( cmd, out result, null );
        }

        /// <summary>執行Sql命令並回傳執行結果 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <remarks>此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線</remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="result">執行結果物件</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>執行是否成功</returns>
        public bool ExecuteScalar( DbCommand command, out object result, DbTransaction transaction )
        {
            result = null;
            
            OracleCommand     cmd   = command     as OracleCommand;
            OracleTransaction trans = transaction as OracleTransaction;

            if ( !base.Check<OracleCommand, OracleTransaction>( command, transaction, "OracleCommand", "OracleTransaction" ) )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, null, trans ) )
            {
                return false;
            }

            return DoExecuteScalar( cmd, out result, trans );
        }

        /// <summary>執行Sql命令並回傳執行結果 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <remarks>此方法會開起外界傳入的資料庫連線，傳入的資料庫連線不可以為Null值</remarks>
        /// <param name="command">要執行的命令</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="result">執行結果物件</param>
        /// <returns>執行是否成功</returns>
        public bool ExecuteScalar( DbCommand command, DbConnection connection, DbTransaction transaction, out object result )
        {
            result = null;
            
            OracleCommand     cmd   = command     as OracleCommand;
            OracleTransaction trans = transaction as OracleTransaction;

            if ( !base.Check<OracleCommand, OracleTransaction>( command, transaction, "OracleCommand", "OracleTransaction" ) )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, connection, trans ) )
            {
                return false;
            }

            return DoExecuteScalar( cmd, out result, trans );
        }

        #endregion 宣告ExecuteScalar單一純量查詢方法

        #region 宣告LoadDataSet查詢方法

        /// <summary>執行Sql查詢指令，回傳執行使否成功 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線，也會自動關閉與釋放框架內部的資料庫連線)
        /// </summary>
        /// <remarks>此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="ds">要填入的DataSet</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataSet( DbCommand command, DataSet ds, params string[] tableNames )
        {
            OracleCommand cmd = command as OracleCommand;

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, null, null ) )
            {
                Message = "傳入的command參數無法轉型為OracleCommand。";
                return false;
            }

            return DoLoadDataSet( cmd, ds, true, tableNames );
        }

        /// <summary>執行Sql查詢指令，回傳執行使否成功 (此方法會開起外界傳入的資料庫連線，連線關閉與釋放都讓外界控制，傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <remarks>此方法會開起外界傳入的資料庫連線，連線關閉與釋放都讓外界控制，傳入的資料庫連線不可以為Null值</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="ds">要填入的DataSet</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataSet( DbCommand command, DataSet ds, DbConnection connection, params string[] tableNames )
        {
            OracleCommand cmd = command as OracleCommand;

            if ( cmd.IsNull() )
            {
                Message = "傳入的command參數無法轉型為OracleCommand。";
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, connection, null ) )
            {
                return false;
            }

            return DoLoadDataSet( cmd, ds, false, tableNames );
        }

        /// <summary>執行Sql查詢指令，回傳執行使否成功 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線，但資料庫連線與交易都由外界控制)
        /// </summary>
        /// <remarks>此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="ds">要填入的DataSet</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataSet( DbCommand command, DataSet ds, DbTransaction transaction, params string[] tableNames )
        {
            OracleCommand     cmd   = command     as OracleCommand;
            OracleTransaction trans = transaction as OracleTransaction;

            if ( !base.Check<OracleCommand, OracleTransaction>( command, transaction, "OracleCommand", "OracleTransaction" ) )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, null, trans ) )
            {
                return false;
            }

            return DoLoadDataSet( cmd, ds, false, tableNames );
        }

        /// <summary>執行Sql查詢指令，回傳執行使否成功 (此方法會開起外界傳入的資料庫連線，連線關閉與釋放都讓外界控制，傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <remarks>此方法會開起外界傳入的資料庫連線，傳入的資料庫連線不可以為Null值</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="ds">要填入的DataSet</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="tableNames">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataSet( DbCommand command, DataSet ds, DbConnection connection, DbTransaction transaction, params string[] tableNames )
        {
            OracleCommand     cmd   = command     as OracleCommand;
            OracleTransaction trans = transaction as OracleTransaction;

            if ( !base.Check<OracleCommand, OracleTransaction>( command, transaction, "OracleCommand", "OracleTransaction" ) )
            {
                return false;
            }

            if ( !base.PrepareDbCommand<OracleTransaction>( cmd, connection, trans ) )
            {
                return false;
            }

            return DoLoadDataSet( cmd, ds, false, tableNames );
        }

        #endregion 宣告LoadDataSet查詢方法

        #region 宣告資料查詢與ORM機制的方法

        #region 宣告LoadDataToModel查詢方法

        /// <summary>執行Sql查詢指令，輸出目標型別的資料模型串列，回傳執行使否成功 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <remarks>此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線</remarks>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">要執行的指令</param>
        /// <param name="models">轉換後的Model/Entity串列</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModel<TModel>( DbCommand command, out List<TModel> models ) 
            where TModel : new()
        {
            return base.DoLoadDataToModel<TModel>( command, ds => { return LoadDataSet( command, ds ); }, out models );
        }

        /// <summary>執行Sql查詢指令，輸出目標型別的資料模型串列，回傳執行使否成功 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">要執行的指令</param>
        /// <param name="models">轉換後的Model/Entity串列</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModel<TModel>( DbCommand command, out List<TModel> models, DbConnection connection )
            where TModel : new()
        {
            return base.DoLoadDataToModel<TModel>( command, ds => { return LoadDataSet( command, ds, connection ); }, out models );
        }

        /// <summary>執行Sql查詢指令，輸出目標型別的資料模型串列，回傳執行使否成功 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <remarks>此方法會自動建立並且開啟 ZayniFramework.DataAccess 框架內部的資料庫連線</remarks>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">要執行的指令</param>
        /// <param name="models">轉換後的Model/Entity串列</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModel<TModel>( DbCommand command, out List<TModel> models, DbTransaction transaction )
            where TModel : new()
        {
            return base.DoLoadDataToModel<TModel>( command, ds => { return LoadDataSet( command, ds, transaction ); }, out models );
        }

        /// <summary>執行Sql查詢指令，輸出目標型別的資料模型串列，回傳執行使否成功 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <remarks>此方法會開起外界傳入的資料庫連線，傳入的資料庫連線不可以為Null值</remarks>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="command">要執行的指令</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="models">轉換後的Model/Entity串列</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModel<TModel>( DbCommand command, DbConnection connection, DbTransaction transaction, out List<TModel> models ) 
            where TModel : new()
        {
            return base.DoLoadDataToModel<TModel>( command, ds => { return LoadDataSet( command, ds, connection, transaction ); }, out models );
        }

        #endregion 宣告LoadDataToModel查詢方法

        #region 宣告LoadDataToDynamicModel的查詢方法

        /// <summary>執行Sql查詢指定，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)串列 (ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table)
        /// </summary>
        /// <remarks>ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table
        /// </remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="models">動態資料模型串列集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicModel( DbCommand command, out List<dynamic> models )
        {
            return base.DoLoadDataToDynamicModel( command, ( out IDataReader reader ) => { return ExecuteReader( command, out reader ); }, out models );
        }

        /// <summary>執行Sql查詢指定，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)串列 (ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table)
        /// </summary>
        /// <remarks>ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table
        /// </remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="models">動態資料模型串列集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicModel( DbCommand command, DbConnection connection, out List<dynamic> models )
        {
            return base.DoLoadDataToDynamicModel( command, ( out IDataReader reader ) => { return ExecuteReader( command, out reader, connection ); }, out models );
        }

        /// <summary>執行Sql查詢指定，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)串列 (ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table)
        /// </summary>
        /// <remarks>ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table
        /// </remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="models">動態資料模型串列集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicModel( DbCommand command, DbTransaction transaction, out List<dynamic> models )
        {
            return base.DoLoadDataToDynamicModel( command, ( out IDataReader reader ) => { return ExecuteReader( command, out reader, transaction ); }, out models );
        }

        /// <summary>執行Sql查詢指定，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)串列 (ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table)
        /// </summary>
        /// <remarks>ORM機制: 單一資料模型ORM對應，應用在SQL查詢語法中只有一個SELECT語法，回傳單一Table
        /// </remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="models">動態資料模型串列集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicModel( DbCommand command, DbConnection connection, DbTransaction transaction, out List<dynamic> models )
        {
            return base.DoLoadDataToDynamicModel( command, ( out IDataReader reader ) => { return ExecuteReader( command, connection, transaction, out reader ); }, out models );
        }

        #endregion 宣告LoadDataToDynamicModel的查詢方法

        #region 宣告LoadDataToModels的查詢方法

        /// <summary>執行Sql查詢指令，輸出資料模型的List物件陣列，回傳執行使否成功 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="result">資料模型的List</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModels( DbCommand command, Type[] modelTypes, out List<object[]> result )
        {
            return base.DoLoadDataToModels( command, modelTypes, ds => { return LoadDataSet( command, ds ); }, out result );
        }

        /// <summary>執行Sql查詢指令，輸出資料模型的List物件陣列，回傳執行使否成功 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="result">資料模型的List</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModels( DbCommand command, Type[] modelTypes, out List<object[]> result, DbConnection connection )
        {
            return base.DoLoadDataToModels( command, modelTypes, ds => { return LoadDataSet( command, ds, connection ); }, out result );
        }

        /// <summary>執行Sql查詢指令，輸出資料模型的List物件陣列，回傳執行使否成功 (自動開啟 ZayniFramework.DataAccess 框架內部的資料庫連線)
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="result">資料模型的List</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModels( DbCommand command, Type[] modelTypes, out List<object[]> result, DbTransaction transaction )
        {
            return base.DoLoadDataToModels( command, modelTypes, ds => { return LoadDataSet( command, ds, transaction ); }, out result );
        }

        /// <summary>執行Sql查詢指令，輸出資料模型的List物件陣列，回傳執行使否成功 (傳入的資料庫連線不可以為Null值)
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="result">資料模型的List</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToModels( DbCommand command, DbConnection connection, DbTransaction transaction, Type[] modelTypes, out List<object[]> result )
        {
            return base.DoLoadDataToModels( command, modelTypes, ds => { return LoadDataSet( command, ds, connection, transaction ); }, out result );
        }
        
        #endregion 宣告LoadDataToModels的查詢方法

        #region 宣告LoadDataToDynamicCollection查詢方法

        /// <summary>執行Sql查詢指令，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)字典集合 (ORM機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個SELECT語法，回傳多個Table)
        /// </summary>
        /// <remarks>ORM機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個SELECT語法，回傳多個Table</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelCollection">動態資料模型(Dynamic Model)字典集合</param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicCollection( DbCommand command, out Dictionary<string, List<dynamic>> modelCollection, params string[] tableName )
        {
            return base.DoLoadDataToDynamicCollection( command, ds => { return LoadDataSet( command, ds, tableName ); }, out modelCollection, tableName );
        }

        /// <summary>執行Sql查詢指令，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)字典集合 (ORM機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個SELECT語法，回傳多個Table)
        /// </summary>
        /// <remarks>ORM機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個SELECT語法，回傳多個Table</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="modelCollection">動態資料模型(Dynamic Model)字典集合</param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicCollection( DbCommand command, DbConnection connection, out Dictionary<string, List<dynamic>> modelCollection, params string[] tableName )
        {
            return base.DoLoadDataToDynamicCollection( command, ds => { return LoadDataSet( command, ds, connection, tableName ); }, out modelCollection, tableName );
        }

        /// <summary>執行Sql查詢指令，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)字典集合 (ORM機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個SELECT語法，回傳多個Table)
        /// </summary>
        /// <remarks>ORM機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個SELECT語法，回傳多個Table</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="modelCollection">動態資料模型(Dynamic Model)字典集合</param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicCollection( DbCommand command, DbTransaction transaction, out Dictionary<string, List<dynamic>> modelCollection, params string[] tableName )
        {
            return base.DoLoadDataToDynamicCollection( command, ds => { return LoadDataSet( command, ds, transaction, tableName ); }, out modelCollection, tableName );
        }

        /// <summary>執行Sql查詢指令，進行動態的Dynamic ORM資料繫結機制，輸出動態資料模型(Dynamic Model)字典集合 (ORM機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個SELECT語法，回傳多個Table)
        /// </summary>
        /// <remarks>ORM機制: 多種資料模型ORM對應，應用在SQL查詢語法中有多個SELECT語法，回傳多個Table</remarks>
        /// <param name="command">要執行的指令</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="modelCollection">動態資料模型(Dynamic Model)字典集合</param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadDataToDynamicCollection( DbCommand command, DbConnection connection, DbTransaction transaction, 
            out Dictionary<string, List<dynamic>> modelCollection, params string[] tableName )
        {
            return base.DoLoadDataToDynamicCollection( command, ds => { return LoadDataSet( command, ds, connection, transaction, tableName ); }, out modelCollection, tableName );
        }

        #endregion 宣告LoadDataToDynamicCollection查詢方法

        #region 宣告LoadModelSet查詢方法

        /// <summary>執行Sql查詢指令，將查詢結果的資料模型都自動載入到指定的DataResult物件中
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadModelSet( DbCommand command, ModelSet modelSet, params Type[] modelTypes )
        {
            return base.DoLoadModelSet( command, modelSet, ( out List<object[]> m ) => { return LoadDataToModels( command, modelTypes, out m ); }, modelTypes );
        }

        /// <summary>執行Sql查詢指令，將查詢結果的資料模型都自動載入到指定的DataResult物件中
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadModelSet( DbCommand command, ModelSet modelSet, DbConnection connection, params Type[] modelTypes )
        {
            return base.DoLoadModelSet( command, modelSet, ( out List<object[]> m ) => { return LoadDataToModels( command, modelTypes, out m, connection ); }, modelTypes );
        }

        /// <summary>執行Sql查詢指令，將查詢結果的資料模型都自動載入到指定的DataResult物件中
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadModelSet( DbCommand command, ModelSet modelSet, DbTransaction transaction, params Type[] modelTypes )
        {
            return base.DoLoadModelSet( command, modelSet, ( out List<object[]> m ) => { return LoadDataToModels( command, modelTypes, out m, transaction ); }, modelTypes );
        }

        /// <summary>執行Sql查詢指令，將查詢結果的資料模型都自動載入到指定的DataResult物件中
        /// </summary>
        /// <param name="command">要執行的指令</param>
        /// <param name="modelSet">查詢結果資料集合</param>
        /// <param name="connection">資料庫連線 (傳入的資料庫連線不可以為Null值)</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="modelTypes">資料模型型別陣列</param>
        /// <returns>是否執行查詢成功</returns>
        public bool LoadModelSet( DbCommand command, ModelSet modelSet, DbConnection connection, DbTransaction transaction, params Type[] modelTypes )
        {
            return base.DoLoadModelSet( command, modelSet, ( out List<object[]> m ) => { return LoadDataToModels( command, connection, transaction, modelTypes, out m ); }, modelTypes );
        }

        #endregion 宣告LoadModelSet查詢方法

        #endregion 宣告資料查詢與ORM機制的方法

        #endregion 宣告執行ORACLE的實體方法


        #region 宣告DbCommand相關的實體方法

        /// <summary>取得Oralce資料庫SQL字串的OracleCommand物件
        /// </summary>
        /// <param name="commandText">命令內容</param>
        /// <returns>OracleCommand物件</returns>
        public DbCommand GetSqlStringCommand( string commandText )
        {
            string sql;

            try
            {
                // 20140611 Added by Pony: 發現在某些PL/SQL語法中，如果有Windows作業系統內建的換行符號在Oracle資料庫中執行會發生異常，因此在這邊要把 \r 字元置換成空字串
                // 20140620 Edited by Pony: 必須要先把PL/SQL語法中的單行註解Trim掉，然後才可以進行換行符號Replace的動作，否則最後的SQL字串會有問題
                var stmt = new SqlStatement( commandText );
                sql = stmt.TrimSingleComment().Remove( "\r" ).Remove( "\n" ).ToString();
            }
            catch ( Exception ex ) 
            {
                Logger.WriteErrorLog( this, "BaseOracleDao.GetSqlStringCommand方法 Replace \\r \\n 換行字元 發生異常: {0}".FormatTo( ex.ToString() ), "BaseOracleDao.GetSqlStringCommand" );
                sql = commandText;
            }

            sql = SqlToOracleString( sql );
            return base.GetDbCommand( sql, CommandType.Text );
        }

        /// <summary>取得StoredProcedurce的OracleCommand物件
        /// </summary>
        /// <param name="storedProcName">資料庫預存程序名稱 (Stored Procedure的名稱)</param>
        /// <returns>OracleCommand物件</returns>
        public DbCommand GetStoredProcCommand( string storedProcName )
        {
            return base.GetDbCommand( storedProcName, CommandType.StoredProcedure );
        }

        /// <summary>加入Oralce輸入參數到指定的OracleCommand中，參數名稱不可以為Null或空字串
        /// </summary>
        /// <remarks>參數名稱不可以為Null或空字串</remarks>
        /// <param name="command">要加入的命令</param>
        /// <param name="parameterName">參數名稱 (不可以為Null或空字串)</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的DbTypeCode靜態常數)</param>
        /// <param name="value">參數內容</param>
        public DbCommand AddInParameter( DbCommand command, string parameterName, int dbType, object value )
        {
            OracleCommand cmd = command as OracleCommand;

            if ( new string[] { command + "", parameterName }.HasNullOrEmptyElemants( true ) )
            {
                Message = "參數名稱不可以為空字串或Null值。";
                return null;
            }
                     
            try
            {
                parameterName             = parameterName.TrimChars( '@' );

                OracleParameter parameter = cmd.Parameters.Add( parameterName, dbType );
                parameter.Value           = value ?? DBNull.Value;
                parameter.Direction       = ParameterDirection.Input;
            }
            catch ( Exception ex )
            {
                Message         = "加入Oracle輸入參數到OracleCommand發生異常，錯誤訊息: {0}".FormatTo( ex.ToString() );
                ExceptionObject = ex;
                Logger.WriteExceptionLog( this, ex, "BaseOracleDao.AddInParameter方法發生異常" );
                HandlerExecuter.DoExceptionHandler( AddInParameterExceptionCallback, ex, this, "BaseOralceDao.AddInParameter" );
                return null;
            }

            return cmd;
        }

        /// <summary>加入Oralce輸入參數到指定的OracleCommand中，參數名稱不可以為Null或空字串
        /// </summary>
        /// <remarks>參數名稱不可以為Null或空字串</remarks>
        /// <param name="command">要加入的命令</param>
        /// <param name="parameterName">參數名稱 (不可以為Null或空字串)</param>
        /// <param name="dbType">參數型態</param>
        /// <param name="value">參數內容</param>
        public DbCommand AddInParameter( DbCommand command, string parameterName, OracleType dbType, object value )
        {
            return this.AddInParameter( command, parameterName, (int)dbType, value );
        }

        /// <summary>加入Oracle輸出參數到指定的OracleCommand中
        /// </summary>
        /// <param name="command">要加入的命令</param>
        /// <param name="parameterName">參數名稱</param>
        /// <param name="dbType">參數型態 (可以使用ZayniFramework 框架的DbTypeCode靜態常數)</param>
        /// <param name="maxSize">變數的最大長度</param>
        public DbCommand AddOutParameter( DbCommand command, string parameterName, int dbType, int maxSize )
        {
            OracleCommand cmd = command as OracleCommand;

            if ( new string[] { command + "", parameterName }.HasNullOrEmptyElemants( true ) )
            {
                Message = "參數名稱不可以為空字串或Null值。";
                return null;
            }
            
            try
            {
                parameterName             = parameterName.TrimChars( '@' );

                OracleParameter parameter = cmd.Parameters.Add( parameterName, (OracleType)dbType, maxSize );
                parameter.Direction       = ParameterDirection.Output;
            }
            catch ( Exception ex )
            {
                Message         = "加入Oracle輸出參數到OracleCommand發生異常，錯誤訊息: {0}".FormatTo( ex.ToString() );
                ExceptionObject = ex;
                Logger.WriteExceptionLog( this, ex, "BaseOracleDao.AddOutParameter方法發生異常" );
                HandlerExecuter.DoExceptionHandler( AddOutParameterExceptionCallback, ex, this, "BaseOralceDao.AddOutParameter" );
                return null;
            }

            return cmd;
        }

        /// <summary>加入Oracle輸出參數到指定的OracleCommand中
        /// </summary>
        /// <param name="command">要加入的命令</param>
        /// <param name="parameterName">參數名稱</param>
        /// <param name="dbType">參數型態</param>
        /// <param name="maxSize">變數的最大長度</param>
        public DbCommand AddOutParameter( DbCommand command, string parameterName, OracleType dbType, int maxSize )
        {
            return this.AddOutParameter( command, parameterName, (int)dbType, maxSize );
        }

        /// <summary>來源的OracleCommand指令中是否包含目標參數
        /// </summary>
        /// <param name="command">來源資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>是否包含目標參數</returns>
        public bool ContainsParameter( DbCommand command, string parameterName )
        {
            if ( new string[] { command + "", parameterName }.HasNullOrEmptyElemants() )
            {
                return false;
            }

            parameterName = parameterName.TrimChars( '@' );

            if ( !command.Parameters.Contains( parameterName ) )
            {
                Message = "資料庫指令的參數集合中不包含 {0} 參數名稱的參數，無法取得參數值。".FormatTo( parameterName );
                return false;
            }

            return true;
        }

        /// <summary>從指定的OracleCommand中取得特定的Oracle參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <returns>資料庫參數值</returns>
        public object GetParameterValue( DbCommand command, string parameterName )
        {
            object result = null;

            if ( parameterName.IsNullOrEmpty() )
            {
                Message = "參數名稱不可以為空字串或Null值。";
                return null;
            }

            if ( new object[] { command, command.Parameters }.HasNullElements() )
            {
                Message = "資料庫指令不可以為Null值。";
                return null;
            }

            parameterName = parameterName.TrimChars( '@' );

            if ( !command.Parameters.Contains( parameterName ) )
            {
                Message = "資料庫指令的參數集合中不包含 {0} 參數名稱的參數，無法取得參數值。".FormatTo( parameterName );
                return null;
            }

            try
            {
                result = command.Parameters[ parameterName ].Value;
            }
            catch ( Exception ex )
            {
                Message         = "取得Oracle參數值發生異常，錯誤訊息: {0}".FormatTo( ex.ToString() );
                ExceptionObject = ex;
                Logger.WriteExceptionLog( this, ex, "BaseOracleDao.GetParameterValue方法發生異常" );
                HandlerExecuter.DoExceptionHandler( GetParameterValueExceptionCallback, ex, this, "BaseOralceDao.GetParameterValue" );
                return null;
            }

            return result;
        }

        /// <summary>對目標的OracleCommand指令中設定特定的SQL參數值
        /// </summary>
        /// <param name="command">目標資料庫命令</param>
        /// <param name="parameterName">資料庫參數名稱</param>
        /// <param name="value">參數值</param>
        /// <returns>設定參數值是否成功</returns>
        public bool SetParameterValue( DbCommand command, string parameterName, object value )
        {
            if ( new string[] { command + "", parameterName }.HasNullOrEmptyElemants() )
            {
                return false;
            }

            if ( !ContainsParameter( command, parameterName ) )
            {
                return false;
            }

            parameterName = parameterName.TrimChars( '@' );

            try
            {
                command.Parameters[ parameterName ].Value = value ?? DBNull.Value;
            }
            catch ( Exception ex )
            {
                Message         = "設定SQL參數值發生異常，錯誤訊息: {0}".FormatTo( ex.ToString() );
                ExceptionObject = ex;
                Logger.WriteExceptionLog( this, ex, "BaseOracleDao.SetParameterValue方法發生異常" );
                HandlerExecuter.DoExceptionHandler( SetParameterValueExceptionCallback, ex, this, "BaseOracleDao.SetParameterValue" );
                return false;
            }
            
            return true;
        }
        
        #endregion 宣告DbCommand相關的實體方法


        #region 將Sql字串轉型成Oracle字串
        
        /// <summary>將Sql字串轉型成Oracle字串
        /// </summary>
        /// <param name="commandText">OracleCommand字串</param>
        /// <returns>OracleCommand字串</returns>
        private string SqlToOracleString( string commandText )
        {
            string input          = commandText;
            string pattern        = @"\B[@]";
            string replacepattern = ":";
            string result         = "";

            Regex regex = new Regex( pattern, RegexOptions.IgnoreCase );

            try
            {
                result = Regex.IsMatch( input, pattern ) ? Regex.Replace( input, pattern, replacepattern ) : input;
            }
            catch ( Exception ex )
            {
                Message         = "OracleCommand字串轉型成OracleCommand字串發生異常，錯誤訊息: {0}".FormatTo( ex.ToString() );
                ExceptionObject = ex;
                Logger.WriteExceptionLog( this, ex, "BaseOracleDao.SqlToOracleString方法發生異常" );
                HandlerExecuter.DoExceptionHandler( OtherExceptionCallback, ex, this, "BaseOralceDao.SqlToOracleString" );
                return null;
            }

            return result;
        }
        
        #endregion 將Sql字串轉型成Oracle字串
    }
}
