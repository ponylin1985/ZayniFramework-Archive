﻿using System;


namespace ZayniFramework.DataAccess
{
    /// <summary>Oracle資料庫供應商
    /// </summary>
    [Obsolete( "廢棄掉的 Oracle 相關類別", false )]
    internal sealed class OracleProvider //: DbProvider
    {
    }
}
