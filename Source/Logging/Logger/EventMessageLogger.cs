﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using NeoSmart.AsyncLock;
using ZayniFramework.Common;
using ZayniFramework.Common.Tasks;


namespace ZayniFramework.Logging
{
    /// <summary>預設的訊息日誌記錄器
    /// </summary>
    public sealed class EventMessageLogger
    {
        #region 宣告私有的欄位

        /// <summary>私有 DefaultLogger 實體
        /// </summary>
        private static EventMessageLogger _instance;

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLockInstance = new AsyncLock();

        /// <summary>非同步作業鎖定物件: 鎖定 _logPaths 字典
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>日誌檔案路徑集合<para/>
        /// * Key 值: 格式為 {日誌物件設定名稱}###FilePath###<para/>
        /// * Value 值: LogFilePathInfo 物件<para/>
        /// </summary>
        private readonly Dictionary<string, LogFilePathInfo> _logPaths = new Dictionary<string, LogFilePathInfo>();

        /// <summary>預設的文字日誌路徑，預設路徑為:<para/>
        /// * ./Zayni/zayni-tracing.log
        /// </summary>
        private const string DEFAULT_PATH = @"./Zayni/zayni-tracing.log";

        /// <summary>非同步日誌工作佇列處理器
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncWorker _worker = new AsyncWorker( 2, $"{nameof ( EventMessageLogger )} AsyncWorker" );

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>靜態建構子
        /// </summary>
        static EventMessageLogger() => _worker.Start();

        /// <summary>私有建構子
        /// </summary>
        private EventMessageLogger() {}

        /// <summary>解構子
        /// </summary>
        ~EventMessageLogger() => _logPaths.Clear();

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>公開的獨體物件
        /// </summary>
        public static EventMessageLogger Instance
        {
            get
            {
                using ( _asyncLockInstance.Lock() )
                {
                    if ( _instance.IsNull() )
                    {
                        _instance = new EventMessageLogger();
                        return _instance;
                    }

                    return _instance;
                }
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告私有的方法

        /// <summary>檢查是否可以執行寫入日誌
        /// </summary>
        /// <param name="enable">文字日誌記錄資訊</param>
        /// <param name="logInfo">文字日誌記錄資訊</param>
        /// <returns>是否可以執行寫入日誌</returns>
        private bool CanLog( bool enable, LogMetadata logInfo )
        {
            if ( !enable )
            {
                return false;
            }

            // 20140201 Pony Says: 檢查目前要寫入的日誌事件層級是否有在 Config 設定中被過濾掉，沒有過濾掉的事件層級，才真正執行日誌寫入
            if ( logInfo.LogEventFilters.IsNotNullOrEmptyList() && logInfo.LogEventFilters.Contains( logInfo.EventType.ToString() ) )
            {
                return false;
            }

            return true;
        }

        /// <summary>取得目前日誌檔案的完整路徑
        /// </summary>
        /// <returns>目前日誌檔案的完整路徑</returns>
        private string GetCurrentPath( string loggerName )
        {
            try
            {
                var lastestTime = _logPaths.Values.Max( k => k.CreateTime );
                var key = _logPaths
                                .Where( m => m.Value.LoggerName == loggerName )
                                .Where( q => q.Value.CreateTime == lastestTime )
                                .Select( y => y.Key )
                                .FirstOrDefault();

                return _logPaths[ key ].FullPath;
            }
            catch ( Exception )
            {
                try
                {
                    return _logPaths[ $"{loggerName}###FilePath###" ].FullPath;
                }
                catch ( Exception )
                {
                    return DEFAULT_PATH;
                }
            }
        }

        /// <summary>檢查是否需要將Log File寫入到自動產生亂數檔名機制
        /// </summary>
        /// <param name="logFilePath">日誌檔案的完整路徑</param>
        /// <param name="logFileSize">目前Log檔案的大小 (單位為MB)</param>
        /// <param name="maxSize">Log檔案大小的上限值 (單位為MB)</param>
        /// <returns></returns>
        private bool IsNeedWriteToOtherFile( string logFilePath, decimal logFileSize, decimal maxSize ) =>
            logFileSize >= maxSize || FileHelper.IsFileLocked( logFilePath );

        #endregion 宣告私有的方法


        #region 宣告公開的方法

        /// <summary>寫入日誌訊息紀錄
        /// </summary>
        /// <param name="logMetadata">日誌記錄資訊</param>
        public void WriteLog( LogMetadata logMetadata )
        {
            #region 寫入文字日誌紀錄

            Check.IsTrue( CanLog( logMetadata.IsEnable, logMetadata ), () => _worker.Enqueue( async () => 
            {
                using ( await _asyncLock.LockAsync() )
                {
                    try
                    {
                        string logPath = logMetadata.LogPath;
                        string dir     = logPath.IsNullOrEmpty() ? DEFAULT_PATH : Path.GetDirectoryName( logPath );

                        string fileName = new FileInfo( logPath ).Name;
                        string fPath    = $"{logMetadata.Name}###FilePath###";

                        if ( !_logPaths.ContainsKey( fPath ) ) 
                        {
                            _logPaths.Add( fPath, new LogFilePathInfo() { CreateTime = DateTime.UtcNow, FullPath = logPath, LoggerName = logMetadata.Name } );
                        }

                        StreamWriter streamWriter = null;
            
                        string now            = DateTime.UtcNow.ToString( "yyyy-MM-dd HH:mm:ss.fff" );
                        string newLine        = Environment.NewLine;
                        string separator      = "==============================";

                        string time           = $"Event Time: {now}{newLine}";
                        string level          = $"Event Level: {logMetadata.EventType.ToString()}{newLine}";
                        string title          = $"Event Title: {logMetadata.Title}{newLine}";
                        string message        = $"Event Message: {LogMessageMaker.MakeExceptionEventLogMessage( logMetadata )}{newLine}";
                        string messageContent = $"{time}{level}{title}{message}{newLine}{separator}";
                
                        logPath = GetCurrentPath( logMetadata.Name );

                        if ( File.Exists( logPath ) )
                        {
                            var f = new FileInfo( logPath );

                            decimal logFileMBSize = f.Length / 1024.0M / 1024.0M;
                            decimal maxMBSize     = logMetadata.LogFileMaxSize;

                            // 20150828 Edited by Pony: 如果檢查到目前要寫入log的檔案，已經被其他的 Process 或 Thread 的 IO 串流咬住，一樣也要自動可以寫到另外產生的亂數檔名 Log File
                            if ( IsNeedWriteToOtherFile( logPath, logFileMBSize, maxMBSize ) )
                            {
                                var randomId = RandomTextHelper.Create( 6 );
                                var nowTime  = DateTime.UtcNow;
                                logPath = $@"{dir}{Path.DirectorySeparatorChar}{DateTime.UtcNow.ToString( "yyyy-MM-dd HH-mm-ss-fffffff" )}-{fileName}"; 
                                _logPaths.Add( randomId, new LogFilePathInfo() { CreateTime = nowTime, FullPath = logPath, LoggerName = logMetadata.Name } );
                            }

                            streamWriter = File.AppendText( logPath );
                        }
                        else 
                        {
                            if ( !Directory.Exists( dir ) ) 
                            {
                                Directory.CreateDirectory( dir );
                            }

                            streamWriter = new StreamWriter( logPath );
                        }

                        await streamWriter.WriteLineAsync( messageContent );
                        await streamWriter.WriteLineAsync();
                        await streamWriter.FlushAsync();
                        
                        streamWriter.Close();
                        streamWriter.Dispose();
                    }
                    catch ( Exception ex )
                    {
                        EventLogger.WriteLog( GetLogActionName( nameof ( WriteLog ) ), $"Write file log occur exception. {ex.ToString()}", EventLogEntryType.Error, true, ex );
                        return;
                    }
                }
            } ) );

            #endregion 寫入文字日誌紀錄

            #region 寫入到主控台 Console 日誌

            Check.IsTrue( CanLog( logMetadata.EnableConsoleOutput, logMetadata ), async () => 
            {
                try
                {
                    string msgSeparator = "==============================";
                    string newLine      = Environment.NewLine;
                    string eventLevel   = $"Event Level: {logMetadata.EventType.ToString()}{newLine}";
                    string eventTitle   = $"Event Title: {logMetadata.Title}{newLine}";
                    string eventMsg     = $"Event Message: {LogMessageMaker.MakeExceptionEventLogMessage( logMetadata )}{newLine}";
                    string msgContent   = $"{newLine}{eventLevel}{eventTitle}{eventMsg}{newLine}{msgSeparator}{newLine}";

                    Func<Task> outputCallback = logMetadata.EventType == EventLogEntryType.Error ?
                        (Func<Task>)( async () => await ConsoleLogger.LogErrorAsync( msgContent ) ) :
                        (Func<Task>)( async () => await ConsoleLogger.LogAsync( msgContent, color: logMetadata.ConsoleOutpotColor.ParseEnum<ConsoleColor>() ) );

                    await outputCallback();
                }
                catch ( Exception )
                {
                    // 如果日誌訊息輸出發生異常，還是要讓後續的 Logging 機制可以正常運行，因此不會再此 return 掉。
                }
            } );

            #endregion 寫入到主控台 Console 日誌

            #region 寫入資料庫日誌紀錄

            bool dbLogEnable = logMetadata.DbLoggerName.IsNotNullOrEmpty() && logMetadata.DbLoggerType.IsNotNullOrEmpty();

            Check.IsTrue( CanLog( dbLogEnable, logMetadata ), () => 
            {
                IEventLogDao dao = EventLogDaoFactory.Create( logMetadata.DbLoggerName, logMetadata.DbLoggerType );
                _worker.Enqueue( async () => await dao?.InsertLogAsync( logMetadata ) );
            } );

            #endregion 寫入資料庫日誌紀錄

            #region 寫入日誌紀錄到 ElasticSearch 服務

            bool esLogEnable = logMetadata.ESLoggerName.IsNotNullOrEmpty();

            Check.IsTrue( CanLog( esLogEnable, logMetadata ), () => 
            {
                var esEntry = new ElasticSearchLogEntry() 
                {
                    LoggerName   = logMetadata.Name,
                    EventLevel   = logMetadata.EventType.ToString(),
                    EventTitle   = logMetadata.Title,
                    EventMessage = LogMessageMaker.MakeExceptionEventLogMessage( logMetadata ),
                    Host         = IPAddressHelper.GetLocalIPAddress(),
                    LogTime      = DateTime.UtcNow
                };

                var esLogger = ElasticSearchLoggerContainer.Get( logMetadata.ESLoggerName );
                _worker.Enqueue( async () => await esLogger?.InsertLogAsync( esEntry ) );
            } );

            #endregion 寫入日誌紀錄到 ElasticSearch 服務

            #region 觸發日誌 Email 電子郵件警示通知機制

            Check.IsTrue( CanLog( logMetadata.EmailNotifyLoggerName.IsNotNullOrEmpty(), logMetadata ), () => _worker.Enqueue( async () => 
            {
                try
                {
                    #region 取得 EmailNotifyLogger 日誌設定

                    var r = LoggingSettingLoader.GetEmailNotifyLoggerSetting( logMetadata.EmailNotifyLoggerName );

                    if ( !r.Success )
                    {
                        return;
                    }

                    EmailNotifyLoggerElement emailLoggerCfg = r.Data;

                    if ( emailLoggerCfg.IsNull() )
                    {
                        return;
                    }
                    
                    string smtpAccount  = emailLoggerCfg.SmtpAccount;
                    string smtpPassword = emailLoggerCfg.SmtpPassword;

                    #endregion 取得 EmailNotifyLogger 日誌設定

                    #region 日誌訊息格式

                    string now            = DateTime.UtcNow.ToString( "yyyy-MM-dd HH:mm:ss.fff" );
                    string newLine        = Environment.NewLine;
                    string time           = $"Event Time: {now}{newLine}";
                    string level          = $"Event Level: {logMetadata.EventType.ToString()}{newLine}";
                    string title          = $"Event Title: {logMetadata.Title}{newLine}";
                    string message        = $"Event Message: {LogMessageMaker.MakeExceptionEventLogMessage( logMetadata )}{newLine}";
                    string messageContent = $"{logMetadata.Name}{newLine}{newLine}{time}{level}{title}{message}{newLine}";

                    #endregion 日誌訊息格式

                    #region 初始化 SmtpClient

                    var smtp = new SmtpClient
                    {
                        Port      = emailLoggerCfg.SmtpPort,
                        Host      = emailLoggerCfg.SmtpHost,
                        EnableSsl = emailLoggerCfg.IsEnableSsl
                    };

                    if ( smtpAccount.IsNotNullOrEmpty() && smtpPassword.IsNotNullOrEmpty() )
                    {
                        smtp.Credentials = new NetworkCredential() 
                        {
                            Domain   = emailLoggerCfg.SmtpDomain.IsNotNullOrEmpty() ? emailLoggerCfg.SmtpDomain : null,
                            UserName = smtpAccount,
                            Password = smtpPassword
                        };
                    }
                    else
                    {
                        smtp.UseDefaultCredentials = true;
                    }

                    #endregion 初始化 SmtpClient

                    #region 初始化 MailMessage 訊息

                    var mailMessage = new MailMessage() 
                    {
                        From    = emailLoggerCfg.FromDisplayName.IsNullOrEmpty() ? new MailAddress( emailLoggerCfg.FromEmailAddress ) : new MailAddress( emailLoggerCfg.FromEmailAddress, emailLoggerCfg.FromDisplayName, Encoding.UTF8 ),
                        Subject = logMetadata.Title,
                        Body    = messageContent
                    };
            
                    AddMailAddress( mailMessage, emailLoggerCfg.ToEmailAddress );

                    #endregion 初始化 MailMessage 訊息

                    #region 寄送日誌訊息至 Smtp Server 服務

                    try
                    {
                        await smtp.SendMailAsync( mailMessage );
                    }
                    catch ( SmtpException smtpEx )
                    {
                        EventLogger.WriteLog( GetLogActionName( nameof ( WriteLog ) ), $"Send email message log to SMTP server occur {nameof ( SmtpException )}. {smtpEx.Message}", EventLogEntryType.Error, true, smtpEx );
                        return;
                    }
                    catch ( Exception ex ) 
                    {
                        EventLogger.WriteLog( GetLogActionName( nameof ( WriteLog ) ), $"Send email message log to SMTP server occur exception. {ex.Message}", EventLogEntryType.Error, true, ex );
                        return;
                    }

                    #endregion 寄送日誌訊息至 Smtp Server 服務
                }
                catch ( Exception ex )
                {
                    EventLogger.WriteLog( GetLogActionName( nameof ( WriteLog ) ), $"Process email notify log message occur exception. {ex.Message}", EventLogEntryType.Error, true, ex );
                    return;
                }
            } ) );

            #endregion 觸發日誌 Email 電子郵件警示通知機制
        }

        #endregion 宣告公開的方法
        

        #region 宣告私有的方法

        /// <summary>加入收件人電子郵件地址
        /// </summary>
        /// <param name="targetMail">目標 MailMessage 物件</param>
        /// <param name="toAddresses">電子郵件地址字串</param>
        private void AddMailAddress( MailMessage targetMail, string toAddresses )
        {
            string[] addresses = toAddresses.SplitString( ";", true );

            foreach ( var address in addresses )
            {
                if ( address.IsNullOrEmpty() )
                {
                    continue;
                }

                targetMail.To.Add( address );
            }
        }

        /// <summary>取得方法名稱的日誌訊息
        /// </summary>
        /// <param name="methodName">目標方法名稱</param>
        /// <returns>方法名稱的日誌訊息</returns>
        private static string GetLogActionName( string methodName ) => $"ZayniFramework {nameof ( EventMessageLogger )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
