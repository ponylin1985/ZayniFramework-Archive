using Elasticsearch.Net;
using Nest;
using Nest.JsonNetSerializer;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>ElasticSearch 服務存取客戶端
    /// </summary>
    public class ElasticSearchBroker
    {
        #region Private Fields

        /// <summary>ElasticSearch 服務的 Uri 位址
        /// </summary>
        private readonly Uri _esConnString;

        /// <summary>ElasticSearch 服務預設的 Index 名稱
        /// </summary>
        private readonly string _esDefaultIndex;

        /// <summary>ElasticSearch 客戶端連線設定物件
        /// </summary>
        private readonly ConnectionSettings _esConnSettings;

        /// <summary>ElasticSearch 客戶端實體
        /// </summary>
        private readonly ElasticClient _esClient;

        #endregion Private Fields


        #region Constructors

        /// <summary>多載建構子
        /// </summary>
        /// <param name="esHostUrl">ElasticSearch 服務的 URL 位址</param>
        /// <param name="defaultIndex">ElasticSearch 服務預設的 Index 名稱</param>
        public ElasticSearchBroker( string esHostUrl, string defaultIndex = "default_data" ) 
        {
            _esConnString   = new Uri( esHostUrl );
            _esDefaultIndex = defaultIndex;
            _esConnSettings = new ConnectionSettings( new SingleNodeConnectionPool( _esConnString ), sourceSerializer: JsonNetSerializer.Default ).DefaultIndex( _esDefaultIndex );
            _esClient       = new ElasticClient( _esConnSettings );
        }

        #endregion Constructors


        #region Public Properties

        /// <summary>ElasticSearch 服務的 Uri 位址
        /// </summary>
        public Uri ESHostUrl => _esConnString;

        /// <summary>ElasticSearch 客戶端連線設定物件
        /// </summary>
        public ConnectionSettings ConnectionSetting => _esConnSettings;

        /// <summary>ElasticSearch 服務預設的 Index 名稱
        /// </summary>
        public string DefaultIndex => _esDefaultIndex;

        /// <summary>ElasticSearch 客戶端實體
        /// </summary>
        public ElasticClient ElasticClient => _esClient;

        #endregion Public Properties


        #region Public Methods

        /// <summary>寫入文件資料模型
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="id">文件 id 值</param>
        /// <param name="model">文件資料模型</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public IResult Upsert<TModel>( string id, TModel model ) 
            where TModel : class
        {
            var result = Common.Result.Create();

            if ( id.IsNullOrEmpty() ) 
            {
                result.Message = $"The '{nameof ( id )}' can not be null or empty string.";
                return result;
            }

            if ( model.IsNull() ) 
            {
                result.Message = $"The '{nameof ( model )}' can not be null..";
                return result;
            }

            if ( !_esClient.Indices.Exists( _esDefaultIndex ).Exists )
            {
                var i = _esClient.Indices.Create( _esDefaultIndex );

                if ( !i.IsValid )
                {
                    result.Message = $"Upsert document data to ElasticSearch service occur error due to create index to ElasticSearch service occur error. Index Name: {_esDefaultIndex}. {Environment.NewLine}{i.DebugInformation}";
                    return result;
                }
            }

            var r = _esClient.Index( model, i => i
                    .Index( _esDefaultIndex )
                    .Id( id )
                    .Refresh( Refresh.True ) 
                );

            if ( !r.IsValid ) 
            {
                result.Message = $"Upsert document data to ElasticSearch service occur error. Index Name: {_esDefaultIndex}, Id: {id}. {Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>寫入文件資料模型
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="id">文件 id 值</param>
        /// <param name="model">文件資料模型</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public async Task<IResult> UpsertAsync<TModel>( string id, TModel model ) 
            where TModel : class
        {
            var result = Common.Result.Create();

            if ( id.IsNullOrEmpty() ) 
            {
                result.Message = $"The '{nameof ( id )}' can not be null or empty string.";
                return result;
            }

            if ( model.IsNull() ) 
            {
                result.Message = $"The '{nameof ( model )}' can not be null..";
                return result;
            }

            if ( !( await _esClient.Indices.ExistsAsync( _esDefaultIndex ) ).Exists )
            {
                var i = await _esClient.Indices.CreateAsync( _esDefaultIndex );

                if ( !i.IsValid )
                {
                    result.Message = $"Upsert document data to ElasticSearch service occur error due to create index to ElasticSearch service occur error. Index Name: {_esDefaultIndex}. {Environment.NewLine}{i.DebugInformation}";
                    return result;
                }
            }

            var r = await _esClient.IndexAsync( model, i => i
                    .Index( _esDefaultIndex )
                    .Id( id )
                    .Refresh( Refresh.True ) 
                );

            if ( !r.IsValid ) 
            {
                result.Message = $"Upsert document data to ElasticSearch service occur error. Index Name: {_esDefaultIndex}, Id: {id}. {Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>寫入文件資料模型
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="indexName">ElasticSearch 文件庫 index 名稱</param>
        /// <param name="id">文件 id 值</param>
        /// <param name="model">文件資料模型</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public IResult Upsert<TModel>( string indexName, string id, TModel model ) 
            where TModel : class
        {
            var result = Common.Result.Create();

            if ( id.IsNullOrEmpty() ) 
            {
                result.Message = $"The '{nameof ( id )}' can not be null or empty string.";
                return result;
            }

            if ( model.IsNull() ) 
            {
                result.Message = $"The '{nameof ( model )}' can not be null..";
                return result;
            }

            if ( !_esClient.Indices.Exists( indexName ).Exists )
            {
                var i = _esClient.Indices.Create( indexName, j => j.Map<TModel>( m => m.AutoMap() ) );

                if ( !i.IsValid )
                {
                    result.Message = $"Upsert document data to ElasticSearch service occur error due to create index to ElasticSearch service occur error. Index Name: {indexName}. {Environment.NewLine}{i.DebugInformation}";
                    return result;
                }
            }

            var r = _esClient.Index( model, i => i
                    .Index( indexName )
                    .Id( id )
                    .Refresh( Refresh.True ) 
                );

            if ( !r.IsValid ) 
            {
                result.Message = $"Upsert document data to ElasticSearch service occur error. Index Name: {indexName}, Id: {id}. {Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>寫入文件資料模型
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="indexName">ElasticSearch 文件庫 index 名稱</param>
        /// <param name="id">文件 id 值</param>
        /// <param name="model">文件資料模型</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public async Task<IResult> UpsertAsync<TModel>( string indexName, string id, TModel model ) 
            where TModel : class
        {
            var result = Common.Result.Create();

            if ( id.IsNullOrEmpty() ) 
            {
                result.Message = $"The '{nameof ( id )}' can not be null or empty string.";
                return result;
            }

            if ( model.IsNull() ) 
            {
                result.Message = $"The '{nameof ( model )}' can not be null..";
                return result;
            }

            if ( !( await _esClient.Indices.ExistsAsync( indexName ) ).Exists )
            {
                var i = await _esClient.Indices.CreateAsync( indexName, j => j.Map<TModel>( m => m.AutoMap() ) );

                if ( !i.IsValid )
                {
                    result.Message = $"Upsert document data to ElasticSearch service occur error due to create index to ElasticSearch service occur error. Index Name: {indexName}. {Environment.NewLine}{i.DebugInformation}";
                    return result;
                }
            }

            var r = await _esClient.IndexAsync( model, i => i
                    .Index( indexName )
                    .Id( id )
                    .Refresh( Refresh.True ) 
                );

            if ( !r.IsValid ) 
            {
                result.Message = $"Upsert document data to ElasticSearch service occur error. Index Name: {indexName}, Id: {id}. {Environment.NewLine}{r.DebugInformation}";
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion Public Methods        
    }
}