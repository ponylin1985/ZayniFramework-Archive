using System;
using System.Collections.Generic;
using System.Timers;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>ElasticSearch 日誌器管理容器
    /// </summary>
    public static class ElasticSearchLoggerContainer
    {
        #region Private Fields

        /// <summary>ElasticSearchLogger 空物件
        /// </summary>
        /// <returns></returns>
        private static readonly NullElasticSearchLogger _nullObj = new NullElasticSearchLogger();

        /// <summary>ElasticSearch 日誌器容器字典
        /// </summary>
        /// <returns></returns>
        private static readonly Dictionary<string, ElasticSearchLogger> _container = new Dictionary<string, ElasticSearchLogger>();

        /// <summary>日誌清除定時器
        /// </summary>
        private static readonly Timer _logResetTimer;

        #endregion Private Fields


        #region Constructors

        /// <summary>靜態建構子
        /// </summary>
        static ElasticSearchLoggerContainer() 
        {
            var config = LoggingSettingLoader.GetLoggingModuleConfigs();
            
            if ( config.ESLogResetDays.IsNull() || config.ESLogResetDays <=0 )
            {
                return;
            }

            if ( _container.IsNull() )
            {
                return;
            }

            double interval = Convert.ToDouble( config.ESLogResetDays );

            _logResetTimer = new Timer();

            #if DEBUG
            _logResetTimer.Interval = TimeSpan.FromMinutes( 1 ).TotalMilliseconds;
            #else
            _logResetTimer.Interval = TimeSpan.FromDays( interval ).TotalMilliseconds;
            #endif
            
            _logResetTimer.Elapsed += async ( sender, e ) => 
            {
                try
                {
                    if ( _container.IsNullOrEmpty() )
                    {
                        return;
                    }

                    foreach ( var logPair in _container )
                    {
                        if ( logPair.IsNull() || logPair.Value.IsNull() )
                        {
                            continue;
                        }

                        var esLogger = logPair.Value;
                        await esLogger.ClearLogAsync();
                    }    
                }
                catch
                {
                    // pass
                }
            };

            _logResetTimer.Enabled  = true;
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>取得指定設定名稱的 ElasticSearch 日誌器實體
        /// </summary>
        /// <param name="name">ElasticSearch 日誌器設定名稱</param>
        /// <returns>ElasticSearch 日誌器實體</returns>
        public static ElasticSearchLogger Get( string name ) 
        {
            if ( name.IsNullOrEmpty() )
            {
                return _nullObj;
            }

            if ( _container.TryGetValue( name, out var esLogger ) && esLogger.IsNotNull() )
            {
                return esLogger;
            }

            esLogger = new ElasticSearchLogger( name );
            _container[ name ] = esLogger;
            return esLogger;
        }

        #endregion Public Methods
    }
}