using Elasticsearch.Net;
using Nest;
using Nest.JsonNetSerializer;
using System;
using System.Configuration;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>ElasticSearch 日誌器
    /// </summary>
    public class ElasticSearchLogger
    {
        #region Static Members

        /// <summary>所有 ElasticSearchLogger 客戶端 Config 區段設定集合
        /// </summary>
        private static readonly ElasticSearchLoggerConfigCollection _configCollection;

        /// <summary>靜態建構子
        /// </summary>
        /// <returns></returns>
        static ElasticSearchLogger() => _configCollection = LoggingSettingLoader.GetLoggingModuleConfigs()?.ElasticSearchSetting;

        #endregion Static Members


        #region Private Fields

        /// <summary>ElasticSearchLogger 客戶端實體 Config 設定元素
        /// </summary>
        private readonly ElasticSearchLoggerElement _esLoggerConfig;

        /// <summary>ElasticSearch 服務的 Uri 位址
        /// </summary>
        private readonly Uri _esConnString;

        /// <summary>ElasticSearch 服務預設的 Index 名稱
        /// </summary>
        private readonly string _esDefaultIndex;

        /// <summary>ElasticSearch 客戶端連線設定物件
        /// </summary>
        private readonly ConnectionSettings _esConnSettings;

        /// <summary>ElasticSearch 客戶端實體
        /// </summary>
        private readonly ElasticClient _esClient;

        #endregion Private Fields
        

        #region Constructors

        /// <summary>預設建構子
        /// </summary>
        public ElasticSearchLogger()
        {
            foreach ( var cfg in _configCollection )
            {
                var config = (ElasticSearchLoggerElement)cfg;

                if ( config.IsDefault )
                {
                    _esLoggerConfig = config;
                    _esConnString   = new Uri( config.HostUrl );
                    _esDefaultIndex = config.DefaultIndex;
                    _esConnSettings = new ConnectionSettings( new SingleNodeConnectionPool( _esConnString ), sourceSerializer: JsonNetSerializer.Default ).DefaultIndex( _esDefaultIndex );
                    _esClient       = new ElasticClient( _esConnSettings );
                    break;
                }
            }

            if ( _esLoggerConfig.IsNull() ) 
            {
                throw new ConfigurationErrorsException( $"No default ElasticSearchLogger found in app.config file's ElasticSearchLogger config section." );
            }
        }
        
        /// <summary>多載建構子
        /// </summary>
        /// <param name="name">日誌器設定名稱</param>
        public ElasticSearchLogger( string name ) 
        {
            foreach ( var cfg in _configCollection )
            {
                var config = (ElasticSearchLoggerElement)cfg;

                if ( config.Name == name )
                {
                    _esLoggerConfig = config;
                    _esConnString   = new Uri( config.HostUrl );
                    _esDefaultIndex = config.DefaultIndex;
                    _esConnSettings = new ConnectionSettings( new SingleNodeConnectionPool( _esConnString ), sourceSerializer: JsonNetSerializer.Default ).DefaultIndex( _esDefaultIndex );
                    _esClient       = new ElasticClient( _esConnSettings );
                    break;
                }
            }

            if ( _esLoggerConfig.IsNull() ) 
            {
                throw new ConfigurationErrorsException( $"No ElasticSearchLogger found in app.config file's ElasticSearchLogger config section. Name: {name}." );
            }
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="hostUri">ElasticSearch 服務的 URL 位址</param>
        /// <param name="defaultIndex">ElasticSearch 服務預設的 Index 名稱</param>
        public ElasticSearchLogger( string hostUri, string defaultIndex = "zayni" ) 
        {
            _esConnString   = new Uri( hostUri );
            _esDefaultIndex = defaultIndex;
            _esConnSettings = new ConnectionSettings( new SingleNodeConnectionPool( _esConnString ), sourceSerializer: JsonNetSerializer.Default ).DefaultIndex( _esDefaultIndex );
            _esClient       = new ElasticClient( _esConnSettings );
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>寫入日誌訊息到 ElasticSearch 服務中
        /// </summary>
        /// <param name="logEntry">ElasticSearch 日誌訊息載體</param>
        public virtual void InsertLog( ElasticSearchLogEntry logEntry ) 
        {
            try
            {
                if ( logEntry.IsNull() ) 
                {
                    return;
                }

                var model = new fs_event_log() 
                {
                    LoggerName   = logEntry.LoggerName,
                    EventLevel   = logEntry.EventLevel,
                    EventTitle   = logEntry.EventTitle,
                    EventMessage = logEntry.EventMessage,
                    Host         = logEntry.Host,
                    LogTime      = logEntry.LogTime
                };

                if ( !_esClient.Indices.Exists( _esDefaultIndex ).Exists )
                {
                    var i = _esClient.Indices.Create( 
                        _esDefaultIndex, 
                        j => j
                            .Map<fs_event_log>( m => m.AutoMap() ) 
                        );
                }

                var r = _esClient.Index( model, i => i
                        .Index( _esDefaultIndex )
                        .Id( RandomTextHelper.Create( 15 ) )
                        .Refresh( Refresh.WaitFor ) 
                    );
            }
            catch
            {
                // pass
            }
        }

        /// <summary>寫入日誌訊息到 ElasticSearch 服務中
        /// </summary>
        /// <param name="logEntry">ElasticSearch 日誌訊息載體</param>
        public virtual async Task InsertLogAsync( ElasticSearchLogEntry logEntry ) 
        {
            try
            {
                if ( logEntry.IsNull() ) 
                {
                    return;
                }

                var model = new fs_event_log() 
                {
                    LoggerName   = logEntry.LoggerName,
                    EventLevel   = logEntry.EventLevel,
                    EventTitle   = logEntry.EventTitle,
                    EventMessage = logEntry.EventMessage,
                    Host         = logEntry.Host,
                    LogTime      = logEntry.LogTime
                };

                if ( !( await _esClient.Indices.ExistsAsync( _esDefaultIndex ) ).Exists )
                {
                    var i = await _esClient.Indices.CreateAsync( 
                        _esDefaultIndex, 
                        j => j
                            .Map<fs_event_log>( m => m.AutoMap() ) 
                        );
                }

                var r = await _esClient.IndexAsync( model, i => i
                        .Index( _esDefaultIndex )
                        .Id( RandomTextHelper.Create( 15 ) )
                        .Refresh( Refresh.WaitFor ) 
                    );
            }
            catch
            {
                // pass
            }
        }

        /// <summary>寫入資料到 ElasticSearch 服務中，在 app.config 設定的 DefaultIndex 中。
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="id">資料 id 值</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public virtual void InsertLog<TModel>( TModel model, string id ) 
            where TModel : class
        {
            try
            {
                if ( model.IsNull() ) 
                {
                    return;
                }

                if ( !_esClient.Indices.Exists( _esDefaultIndex ).Exists )
                {
                    var i = _esClient.Indices.Create( 
                        _esDefaultIndex, 
                        j => j
                            .Map<TModel>( m => m.AutoMap() ) 
                        );
                }

                var r = _esClient.Index( model, i => i
                        .Index( _esDefaultIndex )
                        .Id( id )
                        .Refresh( Refresh.WaitFor ) 
                    );
            }
            catch
            {
                // pass
            }
        }

        /// <summary>寫入資料到 ElasticSearch 服務中，在 app.config 設定的 DefaultIndex 中。
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="id">資料 id 值</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public virtual async Task InsertLogAsync<TModel>( TModel model, string id ) 
            where TModel : class
        {
            try
            {
                if ( model.IsNull() ) 
                {
                    return;
                }

                if ( !( await _esClient.Indices.ExistsAsync( _esDefaultIndex ) ).Exists )
                {
                    var i = await _esClient.Indices.CreateAsync( 
                        _esDefaultIndex, 
                        j => j
                            .Map<TModel>( m => m.AutoMap() ) 
                        );
                }

                var r = await _esClient.IndexAsync( model, i => i
                        .Index( _esDefaultIndex )
                        .Id( id )
                        .Refresh( Refresh.WaitFor ) 
                    );
            }
            catch
            {
                // pass
            }
        }

        /// <summary>清除掉預設 Elasticsearch 服務 index 中所有的 log 日誌
        /// </summary>
        public virtual void ClearLog() 
        {
            try
            {
                if ( _esDefaultIndex.IsNullOrEmpty() || _esClient.IsNull() )
                {
                    return;
                }

                if ( !_esClient.Indices.Exists( _esDefaultIndex ).Exists ) 
                {
                    return;
                }

                _esClient.Indices.Delete( _esDefaultIndex );    
            }
            catch
            {
                // pass 
            }
        }

        /// <summary>清除掉預設 Elasticsearch 服務 index 中所有的 log 日誌
        /// </summary>
        public virtual async Task ClearLogAsync() 
        {
            try
            {
                if ( _esDefaultIndex.IsNullOrEmpty() || _esClient.IsNull() )
                {
                    return;
                }

                if ( !( await _esClient.Indices.ExistsAsync( _esDefaultIndex ) ).Exists ) 
                {
                    return;
                }

                await _esClient.Indices.DeleteAsync( _esDefaultIndex );
            }
            catch
            {
                // pass 
            }
        }

        #endregion Public Methods
    }

    /// <summary>ElasticSearchLogger 空類別
    /// </summary>
    public class NullElasticSearchLogger : ElasticSearchLogger
    {
        #region Constructors

        /// <summary>預設建構子
        /// </summary>
        public NullElasticSearchLogger()
        {
        }

        #endregion Constructors


        #region Override Methods

        /// <summary>寫入日誌訊息到 ElasticSearch 服務中
        /// </summary>
        /// <param name="logEntry">ElasticSearch 日誌訊息載體</param>
        public override void InsertLog( ElasticSearchLogEntry logEntry ) {}

        /// <summary>寫入日誌訊息到 ElasticSearch 服務中
        /// </summary>
        /// <param name="logEntry">ElasticSearch 日誌訊息載體</param>
        public override Task InsertLogAsync( ElasticSearchLogEntry logEntry ) => Task.FromResult( 0 );

        /// <summary>寫入資料到 ElasticSearch 服務中，在 app.config 設定的 DefaultIndex 中。
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="id">資料 id 值</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public override void InsertLog<TModel>( TModel model, string id ) {}

        /// <summary>寫入資料到 ElasticSearch 服務中，在 app.config 設定的 DefaultIndex 中。
        /// * 傳入的 model 文件資料模型，Property 必須宣告為 public string 型別的 Property。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Nest 套件支援的 [Text]、[Number]、[Date] 或 [Nested] 屬性標籤。
        /// * 傳入的 model 文件資料模型，Property 允許標記 Newtonsoft.Json 套件的 [JsonProperty] 屬性，可使用 System.Text.Json 提供的 [JsonPropertyName]，但對 ES 的操作會以 Newtonsoft.Json 進行 JSON 序列化操作。
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="id">資料 id 值</param>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        public override Task InsertLogAsync<TModel>( TModel model, string id ) => Task.FromResult( 0 );

        /// <summary>清除掉預設 Elasticsearch 服務 index 中所有的 log 日誌
        /// </summary>
        public override void ClearLog() {}

        /// <summary>清除掉預設 Elasticsearch 服務 index 中所有的 log 日誌
        /// </summary>
        public override Task ClearLogAsync() => Task.FromResult( 0 );

        #endregion Override Methods
    }
}