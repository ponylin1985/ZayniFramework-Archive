﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>日誌記錄元件類別
    /// </summary>
    public sealed class Logger
    {
        #region 宣告私有的欄位與委派

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>Logging 日誌功能設定值
        /// </summary>
        private static LogSettings _logInfo = null;

        /// <summary>文字檔 Logger 常數池
        /// </summary>
        private static Dictionary<string, LogMetadata> _loggers = new Dictionary<string, LogMetadata>();

        /// <summary>寫入日誌的委派
        /// </summary>
        /// <param name="sender">觸發來源物件</param>
        /// <param name="eventMessage">日誌記錄訊息</param>
        /// <param name="eventTitle">日誌記錄標題</param>
        /// <param name="loggerName">日誌檔名稱設定值</param>
        private delegate void WriteLogHandler( object sender, string eventMessage, string eventTitle, string loggerName = null );

        #endregion 宣告私有的欄位與委派


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static Logger() => StaticInitialize();

        #endregion 宣告靜態建構子


        #region 宣告私有的靜態方法

        /// <summary>靜態初始化
        /// </summary>
        private static void StaticInitialize()
        {
            LoadLoggingSettings();
            LoadTextLoggingSettings();
        }

        /// <summary>讀取 Zayni.Logging 模組的全域性設定值
        /// </summary>
        private static void LoadLoggingSettings()
        {
            _logInfo = LoggingSettingLoader.LoadLoggingSettings();
        }

        /// <summary>取得文字日誌功能Config設定值，並且加入到文字檔Log常數池內
        /// </summary>
        private static void LoadTextLoggingSettings()
        {
            #region 讀取日誌 Config 檔設定值

            var g = LoggingSettingLoader.LoadTextLogSettings();

            if ( !g.Success )
            {   
                // 20150807 Marked by Pony: 為了他媽的要暫時解小二那邊無法寫入事件檢視器可能會壞的的問題... 幹他娘的機八毛... 操你媽的Windows... 幹你機八毛的什麼爛作業系統... 幹...
                //EventLog.WriteEntry( "Logger.LoadTextLoggingSettings", message.FormatTo( rResult.Message ), EventLogEntryType.Error );
                ConsoleLogger.LogError( $"Load Zayni Framework Logging setting occur." );
                return;
            }

            #endregion 讀取日誌 Config 檔設定值

            var configs = g.Data;

            foreach ( var config in configs )
            {
                DefaultTextLoggerElement cfg = (DefaultTextLoggerElement)config;

                #region 初始化文字日誌記錄資訊

                var textLogSetting = new LogMetadata();

                try
                {
                    textLogSetting.Name                  = cfg.LogName;
                    textLogSetting.IsEnable              = cfg.DefaultTextLoggerEnable;
                    textLogSetting.LogPath               = cfg.LogFile?.Path;
                    textLogSetting.LogFileMaxSize        = cfg.MaxSize;
                    textLogSetting.EnableConsoleOutput   = cfg.EnableConsoleOutput;
                    textLogSetting.ConsoleOutpotColor    = cfg.ConsoleOutputColor;
                    textLogSetting.EmailNotifyLoggerName = cfg.EmailNotifyLoggerName;
                    textLogSetting.DbLoggerName          = cfg.DbLoggerName;
                    textLogSetting.DbLoggerType          = cfg.DbLoggerType;
                    textLogSetting.ESLoggerName          = cfg.ElasticSearchLoggerName;
                    textLogSetting.Message               = string.Empty;
                    textLogSetting.Title                 = string.Empty;
                }
                catch ( Exception ex )
                {
                    ConsoleLogger.LogError( ex.ToString() );
                    return;
                }

                #endregion 初始化文字日誌記錄資訊

                #region 檢查日誌記錄層級篩選器

                string strCategories = "";

                try
                {
                    if ( cfg.LogEventFilter.IsNotNull() )
                    {
                        strCategories = cfg.LogEventFilter.Category;
                    }

                    if ( strCategories.IsNotNullOrEmpty() )
                    {
                        string[] categories = strCategories.SplitString( ",", true );

                        if ( categories.IsNotNullOrEmptyArray() )
                        {
                            if ( categories.Contains( EventLogEntryType.Information.ToString() ) ) 
                            {
                                textLogSetting.LogEventFilters.Add( EventLogEntryType.Information.ToString() );
                            }

                            if ( categories.Contains( EventLogEntryType.Warning.ToString() ) )
                            {
                                textLogSetting.LogEventFilters.Add( EventLogEntryType.Warning.ToString() );
                            }
                        }
                    }
                }
                catch ( Exception ex )
                {
                    ConsoleLogger.LogError( ex.ToString() );
                    // 20150807 Marked by Pony: 為了他媽的要暫時解小二那邊無法寫入事件檢視器可能會壞的的問題... 幹他娘的機八毛... 操你媽的 Windows... 幹你機八毛的什麼爛作業系統... 幹...
                    //EventLog.WriteEntry( "Logger.LoadTextLoggingSettings", message.FormatTo( ex.ToString() ), EventLogEntryType.Error );
                    return;
                }

                #endregion 檢查日誌記錄層級篩選器

                #region 加入文字日誌記錄資訊到常數池中

                if ( _loggers.ContainsKey( cfg.LogName ) )
                {
                    continue;
                }

                try
                {
                    _loggers.Add( cfg.LogName, textLogSetting );
                }
                catch ( Exception ex )
                {
                    ConsoleLogger.LogError( ex.ToString() );
                    continue;
                }

                #endregion 加入文字日誌記錄資訊到常數池中
            }
        }

        #endregion 宣告私有的靜態方法


        #region 宣告建構子

        /// <summary>私有建構子
        /// </summary>
        private Logger()
        {
            // pass
        }

        #endregion 宣告建構子


        #region 宣告公開的靜態屬性

        /// <summary>Logger 物件實體
        /// </summary>
        private static Logger _instance;

        /// <summary>取得 Logger 獨體的物件
        /// </summary>
        public static Logger Instance
        {
            get
            {
                using ( _asyncLock.Lock() )
                {
                    if ( null == _instance )
                    {
                        _instance = new Logger();
                    }

                    return _instance;
                }
            }
        }

        #endregion 宣告公開的靜態屬性


        #region 宣告公開的事件委派
        
        /// <summary>紀錄程式異常的委派
        /// </summary>
        /// <param name="ex">程式例外物件</param>
        /// <param name="errorMsg">自訂的錯誤訊息</param>
        /// <param name="sender">傳送此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public delegate void ExceptionLogEventHandler( object sender, Exception ex, string errorMsg, string loggerName = null );

        /// <summary>紀錄事件資訊的委派
        /// </summary>
        /// <param name="eventMsg">事件訊息</param>
        /// <param name="eventTitle">事件標題</param>
        /// <param name="sender">傳送此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public delegate void InformationLogEventHandler( object sender, string eventMsg, string eventTitle, string loggerName = null );

        /// <summary>紀錄事件警告的委派
        /// </summary>
        /// <param name="eventMsg">事件訊息</param>
        /// <param name="eventTitle">事件標題</param>
        /// <param name="sender">傳送此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public delegate void WarningLogEventHandler( object sender, string eventMsg, string eventTitle, string loggerName = null );

        /// <summary>紀錄事件錯誤的委派
        /// </summary>
        /// <param name="errorMsg">事件訊息</param>
        /// <param name="errorTitle">事件標題</param>
        /// <param name="sender">傳送此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public delegate void ErrorLogEventHandler( object sender, string errorMsg, string errorTitle, string loggerName = null );

        #endregion 宣告公開的事件委派


        #region 宣告公開的事件

        /// <summary>紀錄程式異常的事件
        /// </summary>
        public event ExceptionLogEventHandler WriteExceptionLogEvent;

        /// <summary>紀錄程式資訊的事件
        /// </summary>
        public event InformationLogEventHandler WriteInfomationLogEvent;

        /// <summary>紀錄程式警告的事件
        /// </summary>
        public event WarningLogEventHandler WriteWarningLogEvent;

        /// <summary>紀錄程式錯誤的事件
        /// </summary>
        public event ErrorLogEventHandler WriteErrorLogEvent;

        #endregion 宣告公開的事件


        #region 宣告私有的事件靜態觸發方法

        // 20120403 Added by Pony: 以下觸發事件的方法，需要先檢查event物件本身是否已經註冊處理常式，如果沒有處理常式為 Null，則不能觸發事件
        /// <summary>觸發紀錄程式異常日誌紀錄的方法
        /// </summary>
        /// <param name="ex">程式異常物件</param>
        /// <param name="message">自訂的錯誤訊息</param>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        private static void OnExceptionLogEvent( object sender, Exception ex, string message, string loggerName = null )
        {
            using ( _asyncLock.Lock() )
            {
                if ( new object[] { ex, Instance.WriteExceptionLogEvent }.HasNullElements() )
                {
                    return;
                }

                Instance.WriteExceptionLogEvent?.Invoke( sender, ex, message, loggerName );
            }
        }

        /// <summary>觸發紀錄訊息日誌紀錄的方法
        /// </summary>
        /// <param name="eventMsg">日誌訊息</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        private static void OnInformationLogEvent( object sender, string eventMsg, string eventTitle, string loggerName = null )
        {
            using ( _asyncLock.Lock() )
            {
                if ( new string[] { eventMsg, eventTitle }.HasNullOrEmptyElemants() )
                {
                    return;
                }

                Instance.WriteInfomationLogEvent?.Invoke( sender, eventMsg, eventTitle, loggerName );
            }
        }

        /// <summary>觸發紀錄警告日誌紀錄的方法
        /// </summary>
        /// <param name="eventMsg">警告訊息</param>
        /// <param name="eventTitle">警告標題</param>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        private static void OnWarningLogEvent( object sender, string eventMsg, string eventTitle, string loggerName = null )
        {
            using ( _asyncLock.Lock() )
            {
                if ( new string[] { eventMsg, eventTitle }.HasNullOrEmptyElemants() )
                {
                    return;
                }

                Instance.WriteWarningLogEvent?.Invoke( sender, eventMsg, eventTitle, loggerName );
            }
        }

        /// <summary>觸發紀錄錯誤日誌紀錄的方法
        /// </summary>
        /// <param name="eventMsg">錯誤訊息</param>
        /// <param name="eventTitle">錯誤標題</param>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        private static void OnErrorLogEvent( object sender, string eventMsg, string eventTitle, string loggerName = null )
        {
            using ( _asyncLock.Lock() )
            {
                if ( new string[] { eventMsg, eventTitle }.HasNullOrEmptyElemants() )
                {
                    return;
                }

                Instance.WriteErrorLogEvent?.Invoke( sender, eventMsg, eventTitle, loggerName );
            }
        }

        #endregion 宣告私有的事件靜態觸發方法


        #region 宣告公開靜態方法

        /// <summary>取得日誌動作的標題名稱，格式為 $"{className}.{methodName}"
        /// </summary>
        /// <param name="className">類別名稱</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌動作的標題名稱</returns>
        public static string GetTraceLogTitle( string className, string methodName ) => $"{className}.{methodName}";

        /// <summary>取得日誌動作的標題名稱，格式為 $"{className}.{methodName}"
        /// </summary>
        /// <param name="type">類別的型別</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌動作的標題名稱</returns>
        public static string GetTraceLogTitle( Type type, string methodName ) => $"{type.Name}.{methodName}";

        /// <summary>取得日誌動作的標題名稱，格式為 $"{className}.{methodName}"
        /// </summary>
        /// <param name="sender">觸發寫入日誌的物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌動作的標題名稱</returns>
        public static string GetTraceLogTitle( object sender, string methodName ) => $"{sender?.GetType().Name}.{methodName}";

        /// <summary>紀錄程式異常到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="ex">程式例外物件</param>
        /// <param name="message">自訂的錯誤訊息</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void WriteExceptionLog( object sender, Exception ex, string message, string loggerName = null )
        {
            if ( _logInfo.IsEnable )
            {
                OnExceptionLogEvent( sender, ex, message );
            }

            EventLogger.WriteLog( message, ex.Message, EventLogEntryType.Error, _logInfo.IsEventLogEnable, ex );
            WriteTextLog( loggerName, message, ex.Message, EventLogEntryType.Error, ex );
        }

        /// <summary>紀錄程式異常到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="ex">程式例外物件</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="message">日誌訊息</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void WriteExceptionLog( object sender, Exception ex, string eventTitle, string message, string loggerName = null )
        {
            if ( _logInfo.IsEnable )
            {
                OnExceptionLogEvent( sender, ex, message );
            }

            EventLogger.WriteLog( eventTitle, message, EventLogEntryType.Error, _logInfo.IsEventLogEnable, ex );
            WriteTextLog( loggerName, eventTitle, message, EventLogEntryType.Error, ex );
        }

        /// <summary>紀錄資訊到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="eventMsg">日誌訊息</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void WriteInformationLog( object sender, string eventMsg, string eventTitle, string loggerName = null ) =>
            WriteLog( OnInformationLogEvent, sender, eventMsg, eventTitle, loggerName: loggerName );

        /// <summary>紀錄警告到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="eventMsg">日誌訊息</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void WriteWarningLog( object sender, string eventMsg, string eventTitle, string loggerName = null ) =>
            WriteLog( OnWarningLogEvent, sender, eventMsg, eventTitle, EventLogEntryType.Warning, loggerName );

        /// <summary>紀錄錯誤到日誌檔
        /// </summary>
        /// <param name="sender">觸發此事件的物件</param>
        /// <param name="eventMsg">日誌訊息</param>
        /// <param name="eventTitle">日誌標題</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        public static void WriteErrorLog( object sender, string eventMsg, string eventTitle, string loggerName = null ) =>
            WriteLog( OnErrorLogEvent, sender, eventMsg, eventTitle, EventLogEntryType.Error, loggerName );

        #endregion 宣告公開靜態方法


        #region 宣告私有的靜態方法

        /// <summary>執行日誌記錄
        /// </summary>
        /// <param name="handler">寫入日誌記錄委派</param>
        /// <param name="sender">觸發來源物件</param>
        /// <param name="eventMessage">日誌記錄訊息</param>
        /// <param name="eventTitle">日誌記錄標題</param>
        /// <param name="eventType">日誌事件層級種類</param>
        /// <param name="loggerName">日誌物件設定名稱</param>
        private static void WriteLog( WriteLogHandler handler, object sender, string eventMessage, string eventTitle, EventLogEntryType eventType = EventLogEntryType.Information, string loggerName = null )
        {
            if ( _logInfo.IsEnable )
            {
                try
                {
                    ExecuteLogHandler( handler, sender, eventMessage, eventTitle, loggerName );
                }
                catch ( Exception ex )
                {
                    var msg = new StringBuilder();
                    msg.AppendLine( $"Zayni Framework Logger try to write log occur runtime exception: {ex.ToString()}" );
                    msg.AppendLine( $"Original message:" );
                    msg.AppendLine( $"Event Title: {eventTitle}" );
                    msg.AppendLine( $"Event Message: {eventMessage}" );
                    EventLogger.WriteLog( $"ZayniFramework {nameof ( Logger )}", msg.ToString(), EventLogEntryType.Error, true, ex );
                }
            }

            EventLogger.WriteLog( eventTitle, eventMessage, eventType, _logInfo.IsEventLogEnable, null );
            WriteTextLog( loggerName, eventTitle, eventMessage, eventType );
        }

        /// <summary>執行寫入日誌的委派
        /// </summary>
        /// <param name="handler">寫入日誌記錄委派</param>
        /// <param name="sender">觸發來源物件</param>
        /// <param name="eventMessage">日誌記錄訊息</param>
        /// <param name="eventTitle">日誌記錄標題</param>
        /// <param name="loggerName">日誌檔名稱設定值</param>
        private static void ExecuteLogHandler( WriteLogHandler handler, object sender, string eventMessage, string eventTitle, string loggerName = null ) =>
            handler?.Invoke( sender, eventMessage, eventTitle, loggerName );

        /// <summary>寫入文字日誌記錄
        /// </summary>
        /// <param name="loggerName">日誌檔名稱設定值</param>
        /// <param name="eventTitle">事件標題</param>
        /// <param name="eventMessage">事件日誌訊息</param>
        /// <param name="eventType">事件紀錄層級</param>
        /// <param name="ex">程式異常物件</param>
        private static void WriteTextLog( string loggerName, string eventTitle, string eventMessage, EventLogEntryType eventType, Exception ex = null )
        {
            loggerName = loggerName.IsNullOrEmptyString( "_DefaultLog" );

            // 20151022 Edited by Pony: 只要沒有在 Config 檔中正確設定 LoggingSettings/DefaultTextLogger 的區段，基本上，就不會去執行文字檔的 Log 日誌記錄。
            // 包含連 Zayni Framework 框架內部的 Tracing Log 也一樣。
            if ( !_loggers.ContainsKey( loggerName ) )
            {
                return;
            }

            LogMetadata loggerCfg   = _loggers[ loggerName ];
            LogMetadata logMetadata = new LogMetadata();

            logMetadata.Name                  = loggerCfg.Name;
            logMetadata.IsEnable              = loggerCfg.IsEnable;
            logMetadata.LogEventFilters       = loggerCfg.LogEventFilters;
            logMetadata.LogPath               = loggerCfg.LogPath;
            logMetadata.LogFileMaxSize        = loggerCfg.LogFileMaxSize;
            logMetadata.DbLoggerName          = loggerCfg.DbLoggerName;
            logMetadata.DbLoggerType          = loggerCfg.DbLoggerType;
            logMetadata.ESLoggerName          = loggerCfg.ESLoggerName;
            logMetadata.EnableConsoleOutput   = loggerCfg.EnableConsoleOutput;
            logMetadata.ConsoleOutpotColor    = loggerCfg.ConsoleOutpotColor;
            logMetadata.EmailNotifyLoggerName = loggerCfg.EmailNotifyLoggerName;
            logMetadata.Title                 = eventTitle;
            logMetadata.Message               = eventMessage;
            logMetadata.EventType             = eventType;
            logMetadata.ExceptionObject       = ex;

            EventMessageLogger.Instance.WriteLog( logMetadata );
        }

        #endregion 宣告私有的靜態方法
    }
}
