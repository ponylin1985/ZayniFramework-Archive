﻿using System.Threading.Tasks;


namespace ZayniFramework.Logging
{
    /// <summary>資料庫的日誌訊息資料存取介面
    /// </summary>
    internal interface IEventLogDao
    {
        /// <summary>寫入日誌事件訊息
        /// </summary>
        /// <param name="model">文字日誌記錄資訊</param>
        Task InsertLogAsync( LogMetadata model );
    }
}
