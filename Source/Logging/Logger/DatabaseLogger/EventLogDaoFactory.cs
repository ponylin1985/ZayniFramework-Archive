﻿namespace ZayniFramework.Logging
{
    /// <summary>資料庫的日誌訊息資料存取物件的工廠
    /// </summary>
    internal static class EventLogDaoFactory
    {
        /// <summary>建立資料庫的日誌訊息資料存取物件
        /// </summary>
        /// <param name="dbName">日誌資料庫連線設定名稱</param>
        /// <param name="dbType">日誌資料庫種類: MSSQL 或 MySQL</param>
        /// <returns>資料庫的日誌訊息資料存取物件</returns>
        internal static IEventLogDao Create( string dbName, string dbType )
        {
            switch ( dbType?.ToUpper() )
            {
                case "MSSQL":
                    return new MSSqlEventLogDao( dbName );

                case "MYSQL":
                    return new MySqlEventLogDao( dbName );

                case "POSTGRESQL":
                    return new PgSqlEventLogDao( dbName );

                default:
                    return null;
            }
        }
    }
}
