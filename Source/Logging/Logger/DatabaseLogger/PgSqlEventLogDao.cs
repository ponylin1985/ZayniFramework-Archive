using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using System.Threading.Tasks;
using System.Timers;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>PostgreSQL 資料庫日誌訊息資料存取類別
    /// </summary>
    internal sealed class PgSqlEventLogDao : IEventLogDao
    {
        #region 宣告私有的欄位

        /// <summary>日誌清除定時器
        /// </summary>
        private static readonly Timer _logResetTimer;

        /// <summary>資料庫連線字串
        /// </summary>
        private string _dbConnectionString;

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static PgSqlEventLogDao() 
        {
            var config = LoggingSettingLoader.GetLoggingModuleConfigs();
            
            if ( config.DbLogResetDays.IsNull() || config.DbLogResetDays <=0 )
            {
                return;
            }

            string connString = null;

            try
            {
                // 這邊暫時只固定嘗試取得 config 設定檔中，名稱為 Zayni 的連線字串。
                var g = ConfigManagement.GetDbConnectionString( "Zayni" );

                if ( !g.Success )
                {
                    ConsoleLogger.LogError( $"{nameof ( PgSqlEventLogDao )} log reset timer error. {g.Message}" );
                    return;
                }

                connString = g.Data;
            }
            catch
            {
                return;
            }

            if ( connString.IsNullOrEmpty() )
            {
                return;
            }

            double interval = Convert.ToDouble( config.DbLogResetDays );
            _logResetTimer  = new Timer();

            #if DEBUG
            _logResetTimer.Interval = TimeSpan.FromMinutes( 1 ).TotalMilliseconds;
            #else
            _logResetTimer.Interval = TimeSpan.FromDays( interval ).TotalMilliseconds;
            #endif

            _logResetTimer.Elapsed += ( sender, e ) => ClearLog( connString );
            _logResetTimer.Enabled  = true;
        }

        #endregion 宣告靜態建構子


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        internal PgSqlEventLogDao( string dbName )
        {
            var g = ConfigManagement.GetDbConnectionString( dbName );

            if ( !g.Success )
            {
                ConsoleLogger.LogError( $"{nameof ( PgSqlEventLogDao )}.ctor initialize error. {g.Message}" );
                return;
            }

            _dbConnectionString = g.Data;
        }

        /// <summary>解構子
        /// </summary>
        ~PgSqlEventLogDao()
        {
            _dbConnectionString = null;
        }

        #endregion 宣告建構子


        #region 實作介面

        /// <summary>寫入日誌事件訊息
        /// </summary>
        /// <param name="model">文字日誌記錄資訊</param>
        public async Task InsertLogAsync( LogMetadata model )
        {
            try
            {
                using ( var connection = new NpgsqlConnection( _dbConnectionString ) )
                {
                    await connection.OpenAsync();

                    string sql = @" 
                        -- Insert FS_EVENT_LOG
                        INSERT INTO `FS_EVENT_LOG` (
                              `TEXT_LOGGER`
                            , `EVENT_LEVEL`
                            , `EVENT_TITLE`
                            , `EVENT_MESSAGE`
                            , `HOST`

                            , `LOG_TIME`
                        ) VALUES (
                              @TextLogger
                            , @EventLevel
                            , @EventTitle
                            , @EventMessage
                            , @Host

                            , @LogTime
                        ); ".Replace( "`", "\"" );

                    using ( var command = new NpgsqlCommand( sql, connection ) )
                    {
                        command.CommandTimeout = 60;
                        
                        AddInParameter( command, "TextLogger",   NpgsqlDbType.Varchar,     model.Name );
                        AddInParameter( command, "EventLevel",   NpgsqlDbType.Varchar,     model.EventType.ToString() );
                        AddInParameter( command, "EventTitle",   NpgsqlDbType.Varchar,     model.Title );
                        AddInParameter( command, "EventMessage", NpgsqlDbType.Varchar,     model.Message );
                        AddInParameter( command, "Host",         NpgsqlDbType.Varchar,     IPAddressHelper.GetLocalIPAddress() );
                        
                        AddInParameter( command, "LogTime",      NpgsqlDbType.TimestampTz, DateTime.UtcNow );
                        await command.ExecuteNonQueryAsync();
                    }

                    await connection.CloseAsync();
                }
            }
            catch ( Exception )
            {
                return;
            }
        }

        #endregion 實作介面


        #region 宣告私有的方法

        /// <summary>加入 SQL 敘述的參數
        /// </summary>
        /// <param name="cmd">目標 SQL 指令</param>
        /// <param name="parameterName">參數名稱</param>
        /// <param name="dbType">參數資料庫型別</param>
        /// <param name="value">參數值</param>
        private void AddInParameter( NpgsqlCommand cmd, string parameterName, NpgsqlDbType dbType, object value )
        {
            cmd.Parameters.Add( parameterName, dbType );
            cmd.Parameters[ parameterName ].Value     = value ?? DBNull.Value;
            cmd.Parameters[ parameterName ].Direction = ParameterDirection.Input;
        }

        /// <summary>清除在資料庫中所有的日誌。<para/>
        /// <para>* 以下的 log table 中的資料會被直接清除。</para>
        /// <para>  * FS_SQL_LOG</para>
        /// <para>  * FS_EVENT_LOG</para>
        /// <para>  * FS_SERVICE_HOST_ACTION_LOG</para>
        /// <para>  * FS_SERVICE_CLIENT_ACTION_LOG</para>
        /// <para>  * FS_CACHING_REDIS_ACTION_LOG</para>
        /// </summary>
        /// <param name="connString"></param>
        private static void ClearLog( string connString )
        {
            try
            {
                using ( var connection = new NpgsqlConnection( connString ) )
                {
                    connection.Open();

                    string sql = @" 
                        DELETE FROM `FS_SQL_LOG`;
                        DELETE FROM `FS_EVENT_LOG`;
                        DELETE FROM `FS_SERVICE_HOST_ACTION_LOG`;
                        DELETE FROM `FS_SERVICE_CLIENT_ACTION_LOG`;
                        DELETE FROM `FS_CACHING_REDIS_ACTION_LOG`; ";

                    using ( var command = new NpgsqlCommand( sql, connection ) )
                    {
                        command.CommandTimeout = 60;
                        command.ExecuteNonQuery();
                    }

                    connection.Close();
                }
            }
            catch ( Exception )
            {
                return;
            }
        }

        #endregion 宣告私有的方法
    }
}