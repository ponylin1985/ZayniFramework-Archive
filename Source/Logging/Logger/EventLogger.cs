﻿using NeoSmart.AsyncLock;
using System;
using System.Diagnostics;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>事件檢視器日誌記錄元件
    /// </summary>
    public static class EventLogger
    {
        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _lockThiz = new AsyncLock();

        /// <summary>將事件寫入事件檢視器
        /// </summary>
        /// <param name="eventTitle">事件標題</param>
        /// <param name="eventMessage">事件內容訊息</param>
        /// <param name="eventType">事件層級種類</param>
        /// <param name="isEventLogEnable">是否啟用事件檢視器紀錄功能</param>
        /// <param name="ex">程式例外物件</param>
        public static void WriteLog( string eventTitle, string eventMessage, EventLogEntryType eventType, bool isEventLogEnable, Exception ex = null ) 
        {
            if ( !isEventLogEnable )
            {
                return;
            }

            if ( new string[] { eventTitle, eventMessage }.HasNullOrEmptyElemants() )
            {
                return;
            }

            string message = ex.IsNotNull() ? LogMessageMaker.MakeExceptionEventLogMessage( ex, eventMessage ) : eventMessage;

            try
            {
                using ( _lockThiz.Lock() )
                {
                    // 20131206 Pony Says: 寫入事件檢視器也必須要要足夠的權限，請特別注意!
                    EventLog.WriteEntry( eventTitle, message, eventType );
                }
            }
            catch ( Exception )
            {
                // 20150807 Marked by Pony: 為了他媽的要暫時解小二那邊無法寫入事件檢視器可能會壞的的問題... 幹他娘的機八毛... 操你媽的 Windows... 幹你機八毛的什麼爛作業系統... 幹...
                // 通常連這個都會爆掉，那大概都是沒有權限寫入事件檢視器
                //throw e;
            }
        }
    }
}
