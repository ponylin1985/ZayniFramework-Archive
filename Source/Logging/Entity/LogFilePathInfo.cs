﻿using System;


namespace ZayniFramework.Logging
{
    /// <summary>文字日誌檔路徑資訊
    /// </summary>
    internal sealed class LogFilePathInfo
    {
        /// <summary>日誌名稱
        /// </summary>
        internal string LoggerName { get; set; }

        /// <summary>建立時間
        /// </summary>
        internal DateTime CreateTime { get; set; }

        /// <summary>日誌檔案完整路徑
        /// </summary>
        internal string FullPath { get; set; }
    }
}
