﻿namespace ZayniFramework.Logging
{
    /// <summary>日誌記錄功能設定值
    /// </summary>
    internal sealed class LogSettings
    {
        /// <summary>ZayniFramework 框架的日誌功能是否啟用
        /// </summary>
        internal bool IsEnable { get; set; }

        /// <summary>事件檢視器日誌功能是否啟用
        /// </summary>
        internal bool IsEventLogEnable { get; set; }
    }
}
