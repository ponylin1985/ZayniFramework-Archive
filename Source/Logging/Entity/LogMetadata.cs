﻿using System;
using System.Collections.Generic;
using System.Diagnostics;


namespace ZayniFramework.Logging
{
    /// <summary>日誌記錄資訊
    /// </summary>
    public sealed class LogMetadata
    {
        #region 宣告建構子

        /// <summary>解構子
        /// </summary>
        ~LogMetadata()
        {
            Name    = null;
            LogPath = null;
            Title   = null;
            Message = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>日誌物件設定名稱
        /// </summary>
        public string Name { get; internal set; } = "_DefaultLog";

        /// <summary>是否啟用 File Log 日誌功能
        /// </summary>
        public bool IsEnable { get; set; }

        /// <summary>文字日誌檔的路徑，可以為完整絕對路徑或相對路徑。<para/>
        /// * Windows 絕對路徑 C:\ZayniFramework\Log\Default.Tracing.log<para/>
        /// * Unix-like 絕對路徑 /Users/pony/GitRepo/MyGitLab/ZayniFramework/Test/ConsoleTest/Logging.ConsoleApp.Test/Log/Zayni.log<para/>
        /// * Unix-like 相對路徑 ./Test/ConsoleTest/Logging.ConsoleApp.Test/Log/Zayni.log<para/>
        /// </summary>
        public string LogPath { get; set; } = @"./Zayni/zayni-tracing.log";

        /// <summary>文字日誌檔的大小上限 (單位為KB)
        /// </summary>
        public decimal LogFileMaxSize { get; internal set; } = 8;

        /// <summary>是否啟用 Console 主控台日誌輸出
        /// </summary>
        public bool EnableConsoleOutput { get; internal set; }

        /// <summary>標準 Console 輸出的字體顏色
        /// </summary>
        public string ConsoleOutpotColor { get; internal set; } = "Gray";

        /// <summary>Email 信件警示的 Logger 設定名稱
        /// </summary>
        public string EmailNotifyLoggerName { get; internal set; }

        /// <summary>資料庫日誌紀錄的連線名稱
        /// </summary>
        public string DbLoggerName { get; internal set; }

        /// <summary>資料庫日誌的資料庫種類: MSSQL 或 MySQL
        /// </summary>
        public string DbLoggerType { get; internal set; }

        /// <summary>ElasticSerach 日誌器的設定名稱
        /// </summary>
        /// <value></value>
        public string ESLoggerName { get; internal set; }

        /// <summary>日誌標題
        /// </summary>
        public string Title { get; set; } = "ZayniFramework Tracing Log";

        /// <summary>日誌內容
        /// </summary>
        public string Message { get; set; }

        /// <summary>日誌事件層級類型
        /// </summary>
        public EventLogEntryType EventType { get; internal set; } = EventLogEntryType.Information;

        /// <summary>日記事件過濾
        /// </summary>
        public List<string> LogEventFilters { get; internal set; } = new List<string>();

        /// <summary>程式異常物件
        /// </summary>
        public Exception ExceptionObject { get; set; }

        #endregion 宣告公開的屬性
    }
}
