﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Logging
{
    /// <summary>日誌功能設定 Config 讀取器
    /// </summary>
    internal static class LoggingSettingLoader
    {
        #region 宣告靜態的私有欄位

        /// <summary>ZayniFramework 設定檔區段
        /// </summary>
        private static ZayniConfigSection _config;

        #endregion 宣告靜態的私有欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static LoggingSettingLoader() => _config = ConfigManagement.ZayniConfigs;

        #endregion 宣告靜態建構子


        #region 宣告內部的靜態方法

        /// <summary>取得 Config 設定檔中 LoggingSetting 區段的設定值
        /// </summary>
        /// <returns></returns>
        internal static LoggingSettingsConfigCollection GetLoggingModuleConfigs() => _config.LoggingSettings;

        /// <summary>讀取 Config 檔中 LoggingSetting 區段的設定值
        /// </summary>
        /// <returns>LoggingSetting 區段的設定值</returns>
        internal static LogSettings LoadLoggingSettings()
        {
            var result = new LogSettings 
            {
                IsEnable         = false,
                IsEventLogEnable = false
            };

            if ( _config.IsNull() )
            {
                ConsoleLogger.LogError( $"app.config is null. Can not load Zayni Framework config settings." );
                throw new Exception( $"app.config is null. Can not load Zayni Framework config settings." );
            }

            try
            {
                result.IsEnable         = _config.LoggingSettings.LogEnable;
                result.IsEventLogEnable = _config.LoggingSettings.EventLogEnable;
            }
            catch ( Exception ex )
            {
                ConsoleLogger.LogError( $"Load Zayni Framework Logging Setting occur exception. {ex.ToString()}" );
                throw ex;
            }

            return result;
        }

        /// <summary>讀取 Config 檔中預設文字日誌功能的設定值
        /// </summary>
        /// <returns>讀取設定值結果</returns>
        internal static Result<TextLoggerConfigCollection> LoadTextLogSettings()
        {
            var result = Result.Create<TextLoggerConfigCollection>();

            if ( _config.IsNull() )
            {
                result.Message = $"{GetLogActionName( nameof ( LoadTextLogSettings ) )}, read DefaultTextLogger config section occur error due to the ZayniConfigSection is null.";
                ConsoleLogger.LogError( result.Message );
                return result;
            }

            TextLoggerConfigCollection configs = null;

            try
            {
                configs = _config.LoggingSettings.DefaultTextLogSetting;
            }
            catch ( Exception ex )
            {
                result.Message = $"{GetLogActionName( nameof ( LoadTextLogSettings ) )}, read DefaultTextLogger config section occur exception. ZayniFramework config error. {ex.ToString()}";
                ConsoleLogger.LogError( result.Message );
                return result;
            }

            result.Data    = configs;
            result.Success = true;
            return result;
        }

        /// <summary>取得 Config 檔設定中指定的 EmailNotifyLogger 設定值
        /// </summary>
        /// <param name="loggerName">EmailNotifyLogger 組態設定名稱</param>
        /// <returns>取得結果</returns>
        internal static Result<EmailNotifyLoggerElement> GetEmailNotifyLoggerSetting( string loggerName )
        {
            var result = Result.Create<EmailNotifyLoggerElement>();

            if ( loggerName.IsNullOrEmpty() )
            {
                return result;
            }

            var configs = _config.LoggingSettings.EmailNotifySetting;

            foreach ( var cfg in configs )
            {
                var config = (EmailNotifyLoggerElement)cfg;

                if ( config.Name == loggerName )
                {
                    result.Data    = config;
                    result.Success = true;
                    return result;
                }
            }

            return result;
        }

        #endregion 宣告內部的靜態方法


        #region 宣告私有的方法

        /// <summary>取得方法名稱的日誌訊息
        /// </summary>
        /// <param name="methodName">目標方法名稱</param>
        /// <returns>方法名稱的日誌訊息</returns>
        private static string GetLogActionName( string methodName ) => $"{nameof ( LoggingSettingLoader )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
