﻿using System;
using System.Linq;
using System.Reflection;
using ZayniFramework.Common;


namespace ZayniFramework.Formatting
{
    /// <summary>ViewModel初始化器
    /// </summary>
    public static class ViewModelInitializer
    {
        #region 宣告公開的初始化方法

        /// <summary>對目標ViewModel的屬性進行初始化
        /// </summary>
        /// <param name="viewModel">目標ViewModel</param>
        public static void Initailize( object viewModel )
        {
            Type   type     = viewModel.GetType();
            string typeName = type.Name;

            foreach ( PropertyInfo p in type.GetProperties() )
            {
                Type   pType     = p.PropertyType;
                string pTypeName = pType.Name;

                MethodInfo setter = p.GetSetMethod();
                MethodInfo getter = p.GetGetMethod();

                CheckProperty( typeName, pTypeName, getter, setter );

                if ( !setter.IsPublic )
	            {
                    continue;
	            }

                if ( !getter.IsPublic )
	            {
                    continue;
	            }

                CheckPropertyType( typeName, pTypeName );
                p.SetValue( viewModel, "", null );
            }
        }

        #endregion 宣告公開的初始化方法


        #region 宣告私有的靜態方法

        /// <summary>檢查屬性是否都包含getter與setter
        /// </summary>
        /// <param name="vModelTypeName">ViewModel的型別名稱</param>
        /// <param name="propertyTypeName">屬性的型別名稱</param>
        /// <param name="getter">get方法資訊</param>
        /// <param name="setter">set方法資訊</param>
        private static void CheckProperty( string vModelTypeName, string propertyTypeName, MethodInfo getter, MethodInfo setter )
        {
            if ( new object[] { getter, setter }.Any( m => m.IsNull() ) )
            {
                string message = string.Format( "{0} 型別的ViewModel中的 {1} 公開屬性必須包含get與set!", vModelTypeName, propertyTypeName );
                throw new ApplicationException( message );
            }
        }

        /// <summary>檢查所有公開的屬性的型別是否為String
        /// </summary>
        /// <param name="vModelTypeName">ViewModel的型別名稱</param>
        /// <param name="propertyTypeName">屬性的型別名稱</param>
        private static void CheckPropertyType( string vModelTypeName, string propertyTypeName )
        {
            if ( "String" != propertyTypeName )
            {
                string message = string.Format( "{0} 型別的ViewModel中的 {1} 公開屬性型別不為String!", vModelTypeName, propertyTypeName );
                throw new ApplicationException( message );
            }
        }

        #endregion 宣告私有的靜態方法
    }
}
