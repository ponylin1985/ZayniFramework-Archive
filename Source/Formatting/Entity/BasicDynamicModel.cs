﻿using System;


namespace ZayniFramework.Formatting
{
    /// <summary>基礎資料模型 (ViewModel屬性為 ZayniFramework.Common.Dynamic 模組的動態物件)
    /// </summary>
    public class BasicDynamicModel : BasicModel
    {
        /// <summary>動態的畫面資料模型 (ZayniFramework.Common.Dynamic模組的動態物件)
        /// </summary>
        public dynamic ViewModel { get; set; }
    }
}
