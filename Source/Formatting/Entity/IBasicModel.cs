﻿using System;


namespace ZayniFramework.Formatting
{
    /// <summary>基礎資料模型介面
    /// </summary>
    public interface IBasicModel
    {
        /// <summary>畫面資料集合
        /// </summary>
        string this[ string key ]
        {
            get;
            set;
        }
    }

    /// <summary>基礎資料模型介面
    /// </summary>
    /// <typeparam name="TViewModel">ViewModel畫面資料模型泛型</typeparam>
    public interface IBasicModel<TViewModel> : IBasicModel
        where TViewModel : class
    {
        /// <summary>畫面資料模型
        /// </summary>
        TViewModel ViewModel
        {
            get;
            set;
        }
    }
}
