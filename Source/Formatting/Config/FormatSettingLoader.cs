﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Formatting
{
    /// <summary>格式化設定Config讀取器
    /// </summary>
    public class FormatSettingLoader
    {
        /// <summary>讀取Config檔中日期時間的格式化設定集合
        /// </summary>
        /// <returns>日期的格式化樣式設定值</returns>
        public static DateTimeFormatStyleConfigCollection LoadDateFormatStyle()
        {
            ZayniConfigSection zayniConfig = ConfigManagement.ZayniConfigs;

            if ( zayniConfig.IsNull() )
            {
                return null;
            }

            DateTimeFormatStyleConfigCollection result = null;

            try
            {
                result = zayniConfig.FormatSettings.DateTimeFormatStyle;
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( "FormatSettingLoader", ex, "框架讀取DateTimeFormatStyle設定值失敗。" );
                return null;
            }

            return result;
        }
    }
}
