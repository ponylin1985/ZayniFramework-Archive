﻿using System.Text;
using ZayniFramework.Common;


namespace ZayniFramework.Formatting
{
    /// <summary>格式化字串產生器
    /// </summary>
    public class FormatStringMaker
    {
        #region 宣告私有的建構子

        /// <summary>私有建構子
        /// </summary>
        private FormatStringMaker()
        {
            // pass
        }

        #endregion 宣告私有的建構子


        #region 宣告公開靜態的獨體屬性

        private static FormatStringMaker _instance;

        /// <summary>ForamtStringMaker獨體
        /// </summary>
        public static FormatStringMaker Instance
        {
            get
            {
                if ( _instance.IsNull() )
                {
                    _instance = new FormatStringMaker();
                }

                return _instance;
            }
        }

        #endregion 宣告公開靜態的獨體屬性


        #region 宣告公開的數字格式化字串產生方法

        /// <summary>產生指定小數位數的貨幣格式式化字串(加上千分為，小數位數)
        /// </summary>
        /// <param name="decLength">小數位數長度</param>
        /// <returns>指定小數位數的貨幣格式式化字串</returns>
        public string MakeCurrencyString( int decLength )
        {
            string format = string.Format( "0:N{0}", decLength );   // 自動四捨五入，小數位數decLength位
            string result = "{" + format + "}";
            return result;
        }

        /// <summary>產生指定小數位的的數字格式化字串
        /// </summary>
        /// <param name="length">小數位數長度</param>
        /// <returns>指定小數位的的數字格式化字串</returns>
        public string MakeNumberString( int length )
        {
            StringBuilder formatSign = new StringBuilder( "###,###,###,###,##0." );

            for ( int i = 0 ; i < length ; i++ )
            {
                formatSign.Append( "0" );
            }

            //若沒小數位數要把小數點拿掉
            if ( '.' == formatSign[ formatSign.Length - 1 ] )
            {
                formatSign.Remove( formatSign.Length - 1, 1 );
            }

            string result = "{0:" + formatSign + "}";
            return result;
        }

        #endregion 宣告公開的數字格式化字串產生方法
    }
}
