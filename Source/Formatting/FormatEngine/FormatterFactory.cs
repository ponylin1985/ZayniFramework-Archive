﻿using System;
using System.Collections.Generic;


namespace ZayniFramework.Formatting
{
    /// <summary>IFormatter靜態工廠物件
    /// </summary>
    public static class FormatterFactory
    {
        #region 宣告公開的靜態方法

        /// <summary>建構IFormatter物件
        /// </summary>
        /// <param name="info">格式化資訊</param>
        /// <returns>IFormatter物件</returns>
        public static IFormatter CreateFormatter( FormatInfo info )
        {
            IFormatter result = null;

            Dictionary<string, IFormatter> options = CreateOptions();

            if ( options.ContainsKey( info.TypeName ) )
            {
                result = options[ info.TypeName ];
            }
            
            return result;
        }

        #endregion 宣告公開的靜態方法


        #region 宣告私有的靜態方法

        /// <summary>建立Formatter字典集合
        /// </summary>
        /// <returns>Formatter字典集合</returns>
        private static Dictionary<string, IFormatter> CreateOptions()
        {
            var result = new Dictionary<string, IFormatter>();

            result.Add( "DateTime", new DateTimeFormatter() );
            result.Add( "Decimal",  new DecimalFormatter()  );

            return result;
        }

        #endregion 宣告私有的靜態方法
    }
}
