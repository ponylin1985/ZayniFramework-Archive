﻿using System;
using System.Globalization;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Formatting
{
    /// <summary>DateTime的格式化元件
    /// </summary>
    public class DateTimeFormatter : IFormatter
    {
        #region 宣告私有的欄位

        /// <summary>錯誤訊息
        /// </summary>
        private string _message = "DateTimeFormatter格式化錯誤: {0}";

        #endregion 宣告私有的欄位


        #region 實作IFormatter介面

        /// <summary>根據傳入的格式化資訊，對嘗試目標物件進行適當的格式化
        /// </summary>
        /// <param name="target">待格式化的目標物件</param>
        /// <param name="info">格式化資訊</param>
        /// <param name="result">格式化結果字串</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>嘗試格式化是否成功</returns>
        public bool TryFormat( object target, FormatInfo info, out string result, out string message )
        {
            result  = null;
            message = null;

            if ( !DateTime.TryParse( target + "", out DateTime targetTime ) )
            {
                message = _message.FormatTo( "待格式化的目標物件型別不為DateTime，無法進行格式化處理。" );
                Logger.WriteErrorLog( this, message, "DateTimeFormatter.TryFormat" );
                return false;
            }

            try
            {
                // 20180607 Bugfix by Pony: 這邊在進行 DateTime.ToString() 呼叫時，需要加上 CultureInfo.InvariantCulture，用以確保不會採用作業系統預設的 DateTime 日期格式!
                result = targetTime.ToString( info.FormatString, CultureInfo.InvariantCulture );
            }
            catch ( Exception ex )
            {
                message = "DateTime格式化失敗。".FormatTo( ex.ToString() );
                Logger.WriteExceptionLog( this, ex, "格式化動作發生程式異常" );
                return false;
            }

            return true;
        }

        #endregion 實作IFormatter介面
    }
}
