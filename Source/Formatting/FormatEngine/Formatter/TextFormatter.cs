﻿using System;
using System.Text;


namespace ZayniFramework.Formatting
{
    /// <summary>處理文字格式工具元件
    /// </summary>
    public class TextFormatter
    {
        #region 宣告公開的靜態方法

        /// <summary>對傳入得目標Decimal進行格式化
        /// 加上千分位，指定小數位數(但是會移除小數點後的0)，小數點後如果沒有小數位數，小數點會被移除掉
        /// 結果範例: 12546123.4550004316M => 12,546,123.455 
        ///          12546123.400000M => 12,546,123.4
        /// </summary>		
        /// <param name="value">要格式化的內容</param>
        /// <param name="length">小數位數長度</param>
        /// <returns>格式後結果</returns>
        public static string ToNumber( decimal value, int length )
        {
            StringBuilder formatSign = new StringBuilder( "###,###,###,###,##0." );

            for ( int i = 0 ; i < length ; i++ )
            {
                formatSign.Append( "0" );
            }

            //若沒小數位數要把小數點拿掉
            if ( '.' == formatSign[ formatSign.Length - 1 ] )
            {
                formatSign.Remove( formatSign.Length - 1, 1 );
            }

            string format = "{0:" + formatSign + "}";
            string result = string.Format( format, value );
            result		  = RemoveLastZero( result );
            return result;
        }

        /// <summary>對傳入得目標String進行格式化，目標字串必須可以轉型成Decimal
        /// </summary>		
        /// <param name="value">要格式化的內容</param>
        /// <param name="length">小數位數長度</param>
        /// <returns>格式後結果</returns>
        public static string ToNumber( string value, int length )
        {
            decimal innerValue = Convert.ToDecimal( value );
            StringBuilder formatSign = new StringBuilder( "###,###,###,###,##0." );

            for ( int i = 0 ; i < length ; i++ )
            {
                formatSign.Append( "0" );
            }

            //若沒小數位數要把小數點拿掉
            if ( '.' == formatSign[ formatSign.Length - 1 ] )
            {
                formatSign.Remove( formatSign.Length - 1, 1 );
            }

            string format = "{0:" + formatSign + "}";
            string result = string.Format( format, innerValue );
            result		  = RemoveLastZero( result );
            return result;
        }

        /// <summary>格式化為金額格式
        /// </summary>		
        /// <param name="value">要格式化的內容</param>
        /// <param name="length">小數位數長度</param>
        /// <returns>格式後結果</returns>
        public static string ToMoney( decimal value, int length )
        {
            return "$" + ToNumber( value, length );
        }

        /// <summary>格式化為金額格式
        /// </summary>		
        /// <param name="value">要格式化的內容</param>
        /// <param name="length">小數位數長度</param>
        /// <returns>格式後結果</returns>
        public static string ToMoney( string value, int length )
        {
            return "$" + ToNumber( value, length );
        }

        #endregion 宣告公開的靜態方法


        #region 宣告私有的靜態方法

        /// <summary>將小數點後為0的數字移除
        /// </summary>
        /// <param name="str">要處理的字串</param>
        /// <returns>處理結果</returns>
        private static string RemoveLastZero( string str )
        {
            int dotIndex = str.LastIndexOf( "." );

            if ( str.EndsWith( "0" ) && dotIndex > 0 && ( dotIndex < str.LastIndexOf( "0" ) ) )
            {

                str = str.Substring( 0, str.Length - 2 );
                str = RemoveLastZero( str );
            }

            if ( str.EndsWith( "." ) )
            {
                str = str.Substring( 0, str.Length - 2 );
            }

            return str;
        }

        #endregion 宣告私有的靜態方法
    }
}
