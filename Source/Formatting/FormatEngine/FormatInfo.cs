﻿using System;


namespace ZayniFramework.Formatting
{
    //  20130226 Created by Pony
    /// <summary>格式化資訊Entity物件
    /// </summary>
    public class FormatInfo
    {
        /// <summary>欄位的型別名稱
        /// </summary>
        public string TypeName
        {
            get;
            set;
        }

        /// <summary>格式化字串
        /// </summary>
        public string FormatString
        {
            get;
            set;
        }

        /// <summary>文化特性
        /// </summary>
        public string CultureInfo
        {
            get;
            set;
        }
    }
}
