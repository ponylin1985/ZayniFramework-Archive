﻿using System;


namespace ZayniFramework.Formatting
{
    /// <summary>格式化介面
    /// </summary>
    public interface IFormatter
    {
        /// <summary>根據傳入的格式化資訊，對目標物件嘗試進行格式化
        /// </summary>
        /// <param name="target">待被格式化的目標物件</param>
        /// <param name="formatInfo">格式化資訊</param>
        /// <param name="result">格式化的結果</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>格式化動作是否成功</returns>
        bool TryFormat( object target, FormatInfo formatInfo, out string result, out string message );
    }
}
