﻿using System;


namespace ZayniFramework.Formatting
{
    /// <summary>日期時間格式化處理Attribute
    /// </summary>
    [AttributeUsage( AttributeTargets.Property | AttributeTargets.Field )]
    public class DateTimeFormatAttribute : FormatAttribute
    {
    }
}
