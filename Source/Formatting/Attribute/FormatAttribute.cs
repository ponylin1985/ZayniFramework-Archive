﻿using System;


namespace ZayniFramework.Formatting
{
    /// <summary>格式化處理Attribute
    /// </summary>
    [AttributeUsage( AttributeTargets.Property | AttributeTargets.Field )]
    public class FormatAttribute : Attribute
    {
        #region 宣告私有的實體欄位

        private string _formatString;

        #endregion 宣告私有的實體欄位


        #region 宣告公開的實體屬性

        /// <summary>格式化字串
        /// </summary>
        public string FormatString
        {
            get { return _formatString; }
            set { _formatString = value; }
        }

        #endregion 宣告公開的實體屬性


        #region 宣告公開的建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="formatString">格式化字串</param>
        public FormatAttribute( string formatString )
        {
            string message;

            if ( !CheckCtorArguments( out message, formatString ) )
            {
                throw new ArgumentNullException( message );
            }

            _formatString = formatString;
        }

        /// <summary>無參數預設建構子
        /// </summary>
        public FormatAttribute()
            : this( "" )
        {
            // pass
        }

        #endregion 宣告公開的建構子


        #region 宣告私有的實體方法

        //  這裡的邏輯是，如果有傳入任何Null我就認定為不合法，我就回傳false出去 (但是我允許傳入空字串!)
        /// <summary>檢查建構子的輸入引數
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        /// <param name="args">建構子的引數</param>
        /// <returns>是否合法，true為合法，false為不合法</returns>
        private bool CheckCtorArguments( out string message, params string[] args )
        {
            message = "";

            foreach ( string m in args )
            {
                if ( null == m )
                {
                    message = string.Format( "傳入的建構子引數{0}為Null值不合法", m );
                    return false;
                }
            }

            return true;
        }

        #endregion 宣告私有的實體方法
    }
}
