﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Formatting
{
    /// <summary>格式化管理員
    /// </summary>
    public class FormatManager
    {
        #region 宣告私有的靜態欄位

        /// <summary>格式化字串產生器
        /// </summary>
        private static FormatStringMaker _maker = FormatStringMaker.Instance;
        
        /// <summary>格式化元件
        /// </summary>
        private static Formatter _formatter = Formatter.Instance;

        #endregion 宣告私有的靜態欄位


        #region 宣告私有的委派

        /// <summary>產生數字格式化字串的委派
        /// </summary>
        /// <param name="length">小數位數長度</param>
        /// <returns>數字格式化字串</returns>
        private delegate string NumberFormatStringHandler( int length );

        /// <summary>產生DateTime格式化字串的委派
        /// </summary>
        /// <returns>DateTime格式化字串</returns>
        private delegate string DateTimeFormatStringHandler();

        #endregion 宣告私有的委派


        #region 宣告公開靜態的貨幣格式化方法

        /// <summary>對傳入的目標Decimal根據指定的小數位數進行貨幣的格式化
        /// </summary>
        /// <param name="target">待格式化的目標Decimal</param>
        /// <param name="decLength">小數位數長度</param>
        /// <returns>格式化結果</returns>
        public static FormatResult ToCurrencyString( decimal target, int decLength )
        {
            FormatResult result = ExecuteNumberFormat( target, decLength, _maker.MakeCurrencyString  );
            return result;
        }

        /// <summary>對傳入的目標Decimal根據指定的小數位數進行貨幣的格式化
        /// </summary>
        /// <param name="target">待格式化的目標Decimal</param>
        /// <param name="decLength">小數位數長度</param>
        /// <returns>格式化結果</returns>
        public static FormatResult ToCurrencyString( decimal target, decimal decLength = 0M )  
        {
            int decLen = Convert.ToInt32( decLength );
            FormatResult result = ToCurrencyString( target, decLen );
            return result;
        }

        /// <summary>對傳入的目標Decimal根據指定的小數位數進行數字的格式化
        /// </summary>
        /// <param name="target">待格式化的目標Decimal</param>
        /// <param name="length">小數位數長度</param>
        /// <returns>格式化結果</returns>
        public static FormatResult ToNumber( decimal target, int length = 0 ) 
        {
            FormatResult result = ExecuteNumberFormat( target, length, _maker.MakeNumberString  );
            return result;
        }

        /// <summary>對傳入的目標字串根據指定的小數位數進行數字的格式化
        /// </summary>
        /// <param name="target">待格式化的目標字串</param>
        /// <param name="length">小數位數長度</param>
        /// <returns>格式化結果</returns>
        public static FormatResult ToNumber( string target, int length = 0 ) 
        {
            var result = new FormatResult()
            {
                Success = false,
                Message   = "傳入的目標字串不可以為Null或空字串"
            };

            target.IsNotNullOrEmpty( s => {
                decimal targetDeciaml = Convert.ToDecimal( s );
                result = ToNumber( targetDeciaml, length );
            } );

            return result;
        }

        /// <summary>格式化為金額格式
        /// </summary>		
        /// <param name="target">要格式化的內容</param>
        /// <param name="length">小數位數長度</param>
        /// <returns>格式化結果</returns>
        public static FormatResult ToMoney( decimal target, int length = 0 )
        {
            FormatResult result = ToNumber( target, length );

            if ( !result.Success )
            {
                return result;
            }

            result.Result = "$" + result.Result;
            return result;
        }

        /// <summary>格式化為金額格式
        /// </summary>		
        /// <param name="target">要格式化的內容</param>
        /// <param name="length">小數位數長度</param>
        /// <returns>格式化結果</returns>
        public static FormatResult ToMoney( string target, int length = 0 )
        {
            FormatResult result = ToNumber( target, length );

            if ( !result.Success )
            {
                return result;
            }

            result.Result = "$" + result.Result;
            return result;
        }

        #endregion 宣告公開靜態的貨幣格式化方法


        #region 宣告公開靜態的日期時間格式化方法

        /// <summary>對傳入的目標根據指定的格式進行日期時間的格式化
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <param name="formatString">日期格式化樣版字串</param>
        /// <returns>格式化結果</returns>
        public static FormatResult ExecuteDateTimeFormat( object target, string formatString )
        {
            var formatInfo = new FormatInfo() 
            {
                TypeName     = "DateTime",
                FormatString = formatString
            };

            FormatResult result = ExecuteFormat( target, formatInfo );
            return result;
        }

        #endregion 宣告公開靜態的日期時間格式化方法


        #region 宣告公開靜態的格式化方法

        /// <summary>對傳入的目標物件進行格式化
        /// </summary>
        /// <param name="target">待格式化的目標物件</param>
        /// <param name="formatString">完整格式化字串</param>
        /// <param name="typeName">目標物件的型別名稱</param>
        /// <param name="cultureInfo">文化特性</param>
        /// <returns>格式化結果</returns>
        public static FormatResult Format( object target, string formatString, string typeName, string cultureInfo = "" )
        {
            var formatInfo = new FormatInfo()
            {
                TypeName     = typeName,
                FormatString = formatString
            };

            FormatResult result = ExecuteFormat( target, formatInfo );
            return result;
        }

        #endregion 宣告公開靜態的格式化方法


        #region 宣告私有的靜態格式化方法

        /// <summary>執行數字的格式化
        /// </summary>
        /// <param name="target">待格式化的目標Decimal</param>
        /// <param name="length">小數位數長度</param>
        /// <param name="handler">產生數字格式化字串的委派</param>
        /// <returns>格式化結果</returns>
        private static FormatResult ExecuteNumberFormat( object target, int length, NumberFormatStringHandler handler )
        {
            string format = handler( length );

            var formatInfo = new FormatInfo() 
            {
                TypeName     = "Decimal",
                FormatString = format
            };

            FormatResult result = ExecuteFormat( target, formatInfo );
            return result;
        }

        /// <summary>執行格式化
        /// </summary>
        /// <param name="target">待格式化的目標DateTime</param>
        /// <param name="info">格式化資訊</param>
        /// <returns>格式化結果</returns>
        private static FormatResult ExecuteFormat( object target, FormatInfo info )
        {
            var result = new FormatResult() 
            {
                Success = false,
                Result    = ""
            };

            string formatResult;
            string message;

            bool isSuccess = _formatter.TryFormat( target, info, out formatResult, out message );

            if ( !isSuccess )
            {
                result.Message   = message;
                return result;
            }

            result.Result    = formatResult;
            result.Success = true;
            return result;
        }

        #endregion 宣告私有的靜態格式化方法
    }
}
