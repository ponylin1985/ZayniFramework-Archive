﻿using System;
using System.IO;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>組態設定讀取器
    /// </summary>
    internal class ConfigReader
    {
        /// <summary>Zayni Framework Database Migration CLI Application 的設定
        /// </summary>
        internal static DatabaseMigrationConfig Config { get; private set; }

        /// <summary>讀取組態設定值
        /// </summary>
        internal static void ReadConfig() 
        {
            var defaultCfg = new DatabaseMigrationConfig() 
            {
                TargetDatabase     = "Zayni",
                MigrationScriptDir = $".{Path.DirectorySeparatorChar}SchemaChange"
            };

            try
            {
                Config = ConfigManagement.ZayniConfigs.DatabaseMigrationSettings;
            }
            catch ( Exception ex )
            {
                Config = defaultCfg;
                Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, Logger.GetTraceLogTitle( nameof ( ConfigReader ), nameof ( ReadConfig ) ), $"Read Zayni Framework Database Migration config occur exception. Use default DatabaseMigrationConfig." );
                return;
            }
        } 
    }
}
