﻿using System.Data.Common;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>Database 資料庫 Schema Migration 資料存取引擎
    /// </summary>
    internal class MigrationEngineDao : BaseDataAccess
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線名稱</param>
        public MigrationEngineDao( string dbName ) : base( dbName )
        {
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>執行資料庫 Migration 腳本
        /// </summary>
        /// <param name="sqlMigration">資料庫 Migration SQL 腳本</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>執行結果</returns>
        public Result<int> Execute( string sqlMigration, DbConnection connection, DbTransaction transaction )
        {
            var result = new Result<int>()
            {
                Success = false,
                Data      = -1
            };

            var cmd = GetSqlStringCommand( sqlMigration, connection, transaction );

            if ( !base.ExecuteNonQuery( cmd ) )
            {
                result.Message = base.Message;
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion 宣告公開的方法
    }
}
