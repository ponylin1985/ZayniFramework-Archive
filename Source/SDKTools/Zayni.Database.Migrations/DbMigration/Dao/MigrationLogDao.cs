﻿using System;
using System.Data.Common;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>資料庫 Migration 執行日誌記錄資料存取類別
    /// </summary>
    internal class MigrationLogDao : BaseDataAccess
    {
        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線名稱</param>
        public MigrationLogDao( string dbName ) : base( dbName )
        {
        }

        #endregion 宣告建構子


        #region 宣告公開方法

        /// <summary>寫入資料庫 Migration 執行日誌記錄
        /// </summary>
        /// <param name="model">資料庫 Migration 日誌記錄資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>寫入結果</returns>
        public Result InsertLog( MigrationLogModel model, DbConnection connection, DbTransaction transaction )
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false,
                Message   = null
            };

            #endregion 初始化回傳值

            #region 宣告SQL字串

            string sql = @" INSERT INTO FS_DB_MIGRATION_LOG (
                                  SCRIPT
                                , SCRIPT_TYPE
                                , SCRIPT_VERSION
                                , SCRIPT_TIMESTAMP
                                , SCRIPT_CONTENT

                                , IS_SUCCESS
                                , MESSAGE
                                , LOG_TIME
                            ) VALUES (
                                  @Script
                                , @ScriptType
                                , @ScriptVersion
                                , @ScriptTimestamp
                                , @ScriptContent

                                , @IsSuccess
                                , @Message
                                , @LogTime
                            ) ";

            #endregion 宣告SQL字串

            #region 執行資料新增

            DbCommand cmd = base.GetSqlStringCommand( sql, connection, transaction );

            base.AddInParameter( cmd, "@Script",          DbTypeCode.String,   model.Script );
            base.AddInParameter( cmd, "@ScriptType",      DbTypeCode.String,   model.ScriptType );
            base.AddInParameter( cmd, "@ScriptVersion",   DbTypeCode.String,   model.ScriptVersion );
            base.AddInParameter( cmd, "@ScriptTimestamp", DbTypeCode.DateTime, model.ScriptTimestamp );
            base.AddInParameter( cmd, "@ScriptContent",   DbTypeCode.String,   model.ScriptContent );

            base.AddInParameter( cmd, "@IsSuccess",       DbTypeCode.Boolean,   model.IsSuccess );
            base.AddInParameter( cmd, "@Message",         DbTypeCode.String,    model.Message );
            base.AddInParameter( cmd, "@LogTime",         DbTypeCode.DateTime,  model.LogTime );

            if ( !base.ExecuteNonQuery( cmd ) )
            {
                result.Message = $"Execute SQL fail: {Environment.NewLine}{base.Message}{Environment.NewLine}{sql}";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( InsertLog ) ) );
                return result;
            }

            if ( 1 != DataCount )
            {
                result.Message = $"Execute SQL fail. No data insert to FS_DB_MIGRATION_LOG table. {Environment.NewLine}{sql}";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( InsertLog ) ) );
                return result;
            }

            #endregion 執行資料新增

            result.Success = true;
            return result;
        }

        #endregion 宣告公開方法
    }
}
