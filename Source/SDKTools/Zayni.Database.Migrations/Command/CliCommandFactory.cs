﻿using ZayniFramework.Common;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>CLI 命令工廠
    /// </summary>
    internal static class CliCommandFactory
    {
        /// <summary>建立 CLI 命令
        /// </summary>
        /// <param name="consoleCommands">原始 CLI 主控台命令集合</param>
        /// <returns>CLI 命令</returns>
        public static CliCommand Create( string[] consoleCommands )
        {
            if ( consoleCommands.IsNullOrEmptyArray() )
            {
                return new EmptyCliCommand();
            }

            if ( 0 == string.Compare( consoleCommands[ 0 ]?.Trim(), "help", true ) )
            {
                return new HelpCliCommand() { Command = consoleCommands[ 0 ] };
            }

            if ( 0 == string.Compare( consoleCommands[ 0 ]?.Trim(), "configure", true )  )
            {
                return new ConfigureCliCommand() { FullCommands = consoleCommands, Command = consoleCommands[ 0 ], Arguments = consoleCommands[ 1 ] };
            }

            if ( 0 == string.Compare( consoleCommands[ 0 ]?.Trim(), "configs", true )  )
            {
                return new ConfigsCliCommand() { FullCommands = consoleCommands, Command = consoleCommands[ 0 ] };
            }

            if ( 0 == string.Compare( consoleCommands[ 0 ]?.Trim(), "migration", true )  )
            {
                return new MigrationCliCommand() { FullCommands = consoleCommands, Command = consoleCommands[ 0 ] };
            }

            if ( 0 == string.Compare( consoleCommands[ 0 ]?.Trim(), "create", true )  )
            {
                return new CreateMigrationScriptCliCommand() { FullCommands = consoleCommands, Command = consoleCommands[ 0 ] };
            }

            return new EmptyCliCommand();
        }
    }
}
