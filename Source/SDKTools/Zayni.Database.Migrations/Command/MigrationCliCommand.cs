﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.DataAccess.Lightweight;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;

namespace ZayniFramework.Database.Migrations
{
    /// <summary>執行資料庫 Schema Migration 的 CLI 指令<para/>
    /// - zdm migration {version}<para/>
    /// - 範例: zdm migration 1.2.0<para/>
    /// - 範例: zdm migration 3.125.045.14456a
    /// </summary>
    internal sealed class MigrationCliCommand : CliCommand
    {
        #region 宣告私有的欄位

        /// <summary>日誌記錄資料存取物件
        /// </summary>
        private MigrationLogDao _logDao = new MigrationLogDao( ConfigReader.Config.TargetDatabase );

        #endregion 宣告私有的欄位


        #region 實作抽象

        /// <summary>CLI 命令的動作
        /// </summary>
        /// <returns>CLI 命令執行結果</returns>
        public override Result<int> Action()
        {
            var result  = Result.Create<int>();
            result.Data = -1;

            if ( FullCommands.Length != 2 )
            {
                result.Data    = CliReturnCode.INVALID_COMMAND;
                result.Message = $"Invalida CLI command.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                return result;
            }

            Arguments = FullCommands[ 1 ]?.Trim();

            if ( Arguments.IsNullOrEmpty() )
            {
                result.Data    = CliReturnCode.INVALID_COMMAND;
                result.Message = $"Invalida CLI command.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                return result;
            }

            string migrationVersion = Arguments.RemoveFirstAppeared( "v" );

            var dataContext = new MigrationDataContext( ConfigReader.Config.TargetDatabase );
            var q = dataContext.Select( new SelectInfo<MigrationModel>() );

            if ( !q.Success )
            {
                result.Data    = CliReturnCode.ERROR;
                result.Message = $"Get fs_db_migration data fail. {q.Message}";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                return result;
            }
            
            MigrationModel migrationModel = q.Data.FirstOrDefault();
            ConsoleLogger.WriteLine( $"migrationModel: {Environment.NewLine}{JsonConvertUtil.Serialize( migrationModel )}", ConsoleColor.Green );

            if ( migrationVersion == migrationModel?.ScriptVersion )
            {
                result.Success = true;
                result.Data    = CliReturnCode.SUCCESS;
                result.Message = $"Target database current script version is {migrationModel?.ScriptVersion}. Target migrate version is {migrationVersion}. No need to proceed database migration!";
                Logger.WriteInformationLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                // Environment.Exit( CliReturnCode.SUCCESS );
                return result;
            }

            string cfgDir = FileHelper.TrimLastSlash( ConfigReader.Config.MigrationScriptDir );
            string dir    = cfgDir;

            // 代表 config 中是「相對路徑的寫法」，則需要取得完整路徑
            if ( cfgDir.StartsWith( "." ) )
            {
                dir = FileHelper.GetFullPath( cfgDir );
                dir = FileHelper.TrimLastSlash( dir );
            }
            
            var scripts = Directory.GetFiles( dir );

            if ( scripts.IsNullOrEmptyArray() )
            {
                result.Data    = CliReturnCode.ERROR;
                result.Message = $"No database migration script file found in '{dir}' path.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                return result;
            }

            ConsoleLogger.WriteLine( $"scripts: {Environment.NewLine}{JsonConvertUtil.Serialize( scripts )}", ConsoleColor.Green );

            var allScripts       = new LinkedList<MigrationModel>();
            var upgradeScripts   = new LinkedList<MigrationModel>();
            var downgradeScripts = new LinkedList<MigrationModel>();

            foreach ( var script in scripts )
            {
                string fileName = Path.GetFileName( script );
                
                var gt = GetScriptInfo( fileName );

                if ( !gt.Success )
                {
                    continue;
                }

                ConsoleLogger.WriteLine( $"ScriptInfo {Environment.NewLine}{JsonConvertUtil.Serialize( gt.Data )}", ConsoleColor.Green );

                if ( !gt.Success && gt.Code == CliReturnCode.ERROR + "" )
                {
                    result.Data    = CliReturnCode.ERROR;
                    result.Message = gt.Message;
                    return result;
                }

                MigrationModel migrationInfo = gt.Data;

                if ( migrationModel is null )
                {
                    allScripts.AddLast( migrationInfo );
                    upgradeScripts.AddLast( migrationInfo );
                    continue;
                }

                if ( migrationModel.ScriptTimestamp != migrationInfo.ScriptTimestamp && migrationModel.ScriptVersion != migrationInfo.ScriptVersion )
                {
                    allScripts.AddLast( migrationInfo );
                }

                if ( migrationModel.ScriptTimestamp < migrationInfo.ScriptTimestamp )
                {
                    upgradeScripts.AddLast( migrationInfo );
                }
                else if ( migrationModel.ScriptTimestamp > migrationInfo.ScriptTimestamp )
                {
                    downgradeScripts.AddLast( migrationInfo );
                }
            }

            var targets = allScripts.Where( s => s.ScriptVersion == migrationVersion );

            ConsoleLogger.WriteLine( $"allScripts: {Environment.NewLine}{JsonConvertUtil.Serialize( allScripts )}", ConsoleColor.Green );

            if ( targets.IsNullOrEmptyCollection() )
            {
                result.Data    = CliReturnCode.ERROR;
                result.Message = $"1. Can not found '{migrationVersion}' version of migration script file in '{dir}' path. migrationVersion: {migrationVersion}, ";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                return result;
            }

            var migrationScripts    = new List<MigrationModel>();
            var perviousTimestamp   = migrationModel.IsNull() ? default ( DateTime ) : migrationModel.ScriptTimestamp;
            bool upgradeOrDowngrade = targets.FirstOrDefault().ScriptTimestamp > perviousTimestamp;   // true 代表 upgrade，false 代表 downgrade。

            string scriptType     = upgradeOrDowngrade ? "upgrade" : "downgrade";
            MigrationModel target = targets.Where( g => scriptType == g.ScriptType ).FirstOrDefault();

            ConsoleLogger.WriteLine( $"scriptType: {scriptType}", ConsoleColor.Green );

            if ( target is null )
            {
                result.Data    = CliReturnCode.ERROR;
                result.Message = $"2. Can not found 'v{migrationVersion}' version of migration script file in '{dir}' path.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                return result;
            }

            if ( upgradeOrDowngrade )
            {
                migrationScripts = upgradeScripts
                    .Where( up => up.ScriptTimestamp.IsBetween( perviousTimestamp, target.ScriptTimestamp, false, true ) )?
                    .Where( us => "upgrade" == us.ScriptType )?
                    .OrderBy( m => m.ScriptTimestamp )?
                    .ToList();
            }
            else
            {
                migrationScripts = downgradeScripts
                    .Where( down => down.ScriptTimestamp.IsBetween( target.ScriptTimestamp, migrationModel.ScriptTimestamp, true, false ) )?
                    .Where( ds => "downgrade" == ds.ScriptType )?
                    .OrderByDescending( z => z.ScriptTimestamp )?
                    .ToList();
            }

            if ( migrationScripts.IsNullOrEmptyList() )
            {
                result.Data    = CliReturnCode.ERROR;
                result.Message = $"3. Can not found 'v{migrationVersion}' version of migration script file in '{dir}' path.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                return result;
            }

            var dao = new MigrationEngineDao( ConfigReader.Config.TargetDatabase );

            DbConnection  conn  = null;
            DbTransaction trans = null;

            using ( conn = dao.CreateConnection() )
            using ( trans = dao.BeginTransaction( conn ) )
            {
                foreach ( var migrationScript in migrationScripts )
                {
                    string scriptPath = string.Empty;

                    try
                    {
                        scriptPath = Path.Combine( dir, migrationScript.ScriptFileName );
                        var sqlMigration = File.ReadAllText( scriptPath );

                        var sql = $"-- ZayniFramework Database Migration{Environment.NewLine}";
                        sql    += $"-- {migrationScript.ScriptFileName}{Environment.NewLine}{File.ReadAllText( scriptPath )}";

                        var r = dao.Execute( sql, conn, trans );

                        if ( !r.Success )
                        {
                            result.Data    = CliReturnCode.EXECUTE_MIGRATION_SCRIPT_FAIL;
                            result.Message = $"Execute database migration script fail. Migration script file: {scriptPath}. {Environment.NewLine}{r.Message}";
                            Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                            Log( conn, trans, migrationScript, sqlMigration, false, result.Message );
                            return result;
                        }

                        Log( conn, trans, migrationScript, sqlMigration, true );
                    }
                    catch ( Exception ex )
                    {
                        dataContext.Rollback( trans, out string message );
                        result.Data    = CliReturnCode.EXECUTE_MIGRATION_SCRIPT_FAIL;
                        result.Message = $"Execute database migration script occur exception. Migration script file: {scriptPath}.";
                        Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( Action ) ), result.Message );
                        return result;
                    }
                }

                if ( !dao.Commit( trans ) )
                {
                    dao.Rollback( trans );
                    result.Data    = CliReturnCode.EXECUTE_MIGRATION_SCRIPT_FAIL;
                    result.Message = $"Commit database migration script occur exception.";
                    Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                    return result;
                }

                var currentMigration = migrationScripts.LastOrDefault();
                var d = dataContext.InsertOrUpdate( currentMigration, migrationModel );

                if ( !d.Success )
                {
                    result.Data    = CliReturnCode.ERROR;
                    result.Message = $"Execute migration script success but write to fs_db_migration fail. {Environment.NewLine}{d.Message}";
                    result.Success = true;
                    Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Action ) ) );
                    return result;
                }
            }

            

            

            ConsoleLogger.WriteLine( $"Execute database migration successfully.", ConsoleColor.Green );
            Console.WriteLine();

            result.Data    = CliReturnCode.SUCCESS;
            result.Success = true;

            Environment.Exit( CliReturnCode.SUCCESS );
            return result;
        }

        #endregion 實作抽象


        #region 宣告私有的方法

        /// <summary>取得資料庫 Migration SQL 腳本的資訊
        /// </summary>
        /// <param name="scriptFileName">資料庫 Migration SQL 腳本的檔名</param>
        /// <returns>取得結果</returns>
        private Result<MigrationModel> GetScriptInfo( string scriptFileName )
        {
            var result = Result.Create<MigrationModel>();

            DateTime timestamp;
            string version;
            string type;

            try
            {
                var segments = scriptFileName.Split( '_' );

                #region 取得 Migration 腳本的時間戳記

                if ( segments.IsNullOrEmptyArray() || segments.Length < 2 )
                {
                    result.Code = CliReturnCode.SUCCESS + "";
                    return result;
                }

                string strTimestamp = $"{segments[ 0 ]}_{segments[ 1 ]}";
                timestamp = DateTimeConverter.Convert( strTimestamp, "yyyyMMdd_HHmmss" );

                #endregion 取得 Migration 腳本的時間戳記

                #region 取得 Migration 腳本的版本號

                string strVersion = segments.LastOrDefault()?.Trim();

                if ( strVersion.IsNullOrEmpty() || !strVersion.StartsWith( "v" ) )
                {
                    result.Code    = CliReturnCode.ERROR + "";
                    result.Message = $"Invalid file name of migration script file. Invalid version format. {scriptFileName}";
                    Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( GetScriptInfo ) ) );
                    return result;
                }

                version = strVersion.RemoveFirstAppeared( "v" );
                version = version.RemoveLastAppeared( ".sql" );

                #endregion 取得 Migration 腳本的版本號

                #region 取得 Migration 腳本類型

                type = segments[ 2 ];

                if ( !new string[] { "upgrade", "downgrade" }.Contains( type ) )
                {
                    result.Code    = CliReturnCode.ERROR + "";
                    result.Message = $"Invalid file name of migration script file. Invalid script type format. Only support 'upgrade' or 'downgrade' type. {scriptFileName}";
                    Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( GetScriptInfo ) ) );
                    return result;
                }

                #endregion 取得 Migration 腳本類型
            }
            catch ( Exception ex )
            {
                result.Code    = CliReturnCode.ERROR + "";
                result.Message = $"Get migration script information occur exception. Script file name: {scriptFileName}.";
                Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( GetScriptInfo ) ), result.Message );
                return result;
            }

            var model = new MigrationModel()
            {
                ScriptFileName  = scriptFileName,
                ScriptType      = type,
                ScriptVersion   = version,
                ScriptTimestamp = timestamp
            };

            result.Data    = model;
            result.Success = true;
            return result;
        }

        /// <summary>紀錄執行資料庫 migration 的日誌
        /// </summary>
        /// <param name="conn">資料庫連線</param>
        /// <param name="trans">資料庫交易</param>
        /// <param name="migrationModel">資料庫 migration 資料模型</param>
        /// <param name="migrationSql">資料庫 migration SQL 腳本內容</param>
        /// <param name="isSuccess">執行是否成功</param>
        /// <param name="message">執行後的結果訊息</param>
        private void Log( DbConnection conn, DbTransaction trans, MigrationModel migrationModel, string migrationSql, bool isSuccess, string message = null )
        {
            var model = new MigrationLogModel() 
            {
                Script          = migrationModel.ScriptFileName,
                ScriptType      = migrationModel.ScriptType,
                ScriptVersion   = migrationModel.ScriptVersion,
                ScriptTimestamp = migrationModel.ScriptTimestamp,
                ScriptContent   = migrationSql,

                IsSuccess       = isSuccess,
                Message         = message,
                LogTime         = DateTime.UtcNow
            };

            _logDao.InsertLog( model, conn, trans );
        }

        #endregion 宣告私有的方法
    }
}
