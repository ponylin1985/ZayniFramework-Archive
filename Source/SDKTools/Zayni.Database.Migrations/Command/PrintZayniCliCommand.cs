using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>印出 Zayni 符號的 CLI 指令
    /// </summary>
    internal class PrintZayniCliCommand : CliCommand
    {
        #region 實作抽象

        /// <summary>CLI 命令的動作
        /// </summary>
        /// <returns>CLI 命令執行結果</returns>
        public override Result<int> Action()
        {
            var result = Result.Create<int>();
            
            ConsoleLogger.WriteLine( Symbol, ConsoleColor.Green );
            
            result.Data    = CliReturnCode.SUCCESS;
            result.Success = true;
            return result;
        }

        #endregion 實作抽象


        #region Internal Properties

        /// <summary>Zayni 的 Symbol 符號
        /// </summary>
        internal static string Symbol = @"
*******      *     *        * *       * *******
     *      * *     *      *  **      *    *
    *      *   *     *    *   * *     *    *
   *      *     *     *  *    *  *    *    *
  *      *********     **     *   *   *    *
 *      *        *      *     *    *  *    *
*      *          *     *     *     * *    *
********************    *     *      ** *******";      

        #endregion Internal Properties
    }
}