using System;
using System.Configuration;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>顯示目前 zdm 使用的 zayni framework 的 config file 的路徑的 CLI 指令
    /// - zdm configs
    /// </summary>
    internal sealed class ConfigsCliCommand : CliCommand
    {
        #region 實作抽象

        /// <summary>CLI 命令的動作
        /// </summary>
        /// <returns>CLI 命令執行結果</returns>
        public override Result<int> Action()
        {
            var result = Result.Create<int>();
            
            var configPath = ConfigurationManager.AppSettings[ "zayniConfigPath" ];
            ConfigManagement.ConfigFullPath = configPath;
            ConfigReader.ReadConfig();

            ConsoleLogger.WriteLine( $"Config Path: ", ConsoleColor.Green );
            ConsoleLogger.WriteLine( configPath, ConsoleColor.Green );
            
            result.Data    = CliReturnCode.SUCCESS;
            result.Success = true;
            return result;
        }

        #endregion 實作抽象
    }
}