using System;
using System.IO;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>建立 migration script 腳本的 CLI 指令
    /// - zdm create -v {version} -n {database_name}
    /// - zdm create -v 1.0.0 -n zayni_test
    /// - 20180313_120000_upgrade_zayni_test_v1.0.0.sql
    /// - 20180313_120000_downgrade_zayni_test_v1.0.0.sql
    /// </summary>
    internal sealed class CreateMigrationScriptCliCommand : CliCommand
    {
        #region 實作抽象

        /// <summary>CLI 命令的動作
        /// </summary>
        /// <returns>CLI 命令執行結果</returns>
        public override Result<int> Action()
        {
            var result = Result.Create<int>();

            var fullCommand = "";

            foreach ( var cmd in FullCommands )
            {
                fullCommand += $"{cmd} ";
            }

            fullCommand = fullCommand.Trim();

            if ( !fullCommand.Contains( "-v" ) || !fullCommand.Contains( "-n" ) )
            {
                result.Data    = CliReturnCode.INVALID_COMMAND;
                result.Message = $"Invalid command.";
                return result;
            }

            var version  = "";
            var database = "";

            var command = fullCommand.RemoveFirstAppeared( "create" )?.Trim();
            var tokens  = command.Split( ' ' );
            var index   = 0;

            foreach ( var token in tokens )
            {
                if ( token.IsNullOrEmpty() )
                {
                    continue;
                }

                if ( token == "-v" )
                {
                    version = tokens[ index + 1 ];

                    if ( version.IsNullOrEmpty() || version == "-n" )
                    {
                        version = "";
                        continue;
                    }
                }

                if ( token == "-n" )
                {
                    database = tokens[ index + 1 ];

                    if ( database.IsNullOrEmpty() || database == "-v" )
                    {
                        database = "";
                        continue;
                    }
                }

                index++;
            }

            if ( version.IsNullOrEmpty() || database.IsNullOrEmpty() )
            {
                result.Data    = CliReturnCode.INVALID_COMMAND;
                result.Message = $"Invalid command.";
                return result;
            }

            var separator = Path.DirectorySeparatorChar;
            var cfgDir    = FileHelper.TrimLastSlash( ConfigReader.Config.MigrationScriptDir );

            var dir = cfgDir;

            // 代表 config 中是「相對路徑的寫法」，則需要取得完整路徑
            if ( cfgDir.StartsWith( "." ) )
            {
                dir = FileHelper.GetFullPath( cfgDir );
                dir = FileHelper.TrimLastSlash( dir );
            }
            
            var dtNow                   = DateTime.Now;
            var upgradeScriptFileName   = $"{dtNow.ToString( "yyyyMMdd" )}_{dtNow.ToString( "HHmmss" )}_upgrade_{database}_v{version}.sql";
            var downgradeScriptFileName = $"{dtNow.ToString( "yyyyMMdd" )}_{dtNow.ToString( "HHmmss" )}_downgrade_{database}_v{version}.sql";

            var updateScriptPath    = $"{dir}{separator}{upgradeScriptFileName}";
            var downgradeScriptPath = $"{dir}{separator}{downgradeScriptFileName}";

            File.Create( updateScriptPath ).Dispose();
            File.Create( downgradeScriptPath ).Dispose();

            ConsoleLogger.WriteLine( $"Migration scripts create success.", ConsoleColor.Green );
            ConsoleLogger.WriteLine( upgradeScriptFileName, ConsoleColor.Green );
            ConsoleLogger.WriteLine( downgradeScriptFileName, ConsoleColor.Green );
            
            result.Data    = CliReturnCode.SUCCESS;
            result.Success = true;
            return result;
        }

        #endregion 實作抽象
    }
}