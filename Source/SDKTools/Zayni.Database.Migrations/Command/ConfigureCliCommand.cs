using System;
using System.Configuration;
using System.IO;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>設定 zdm 的 zayni framework 的 config file 絕對路徑的 CLI 指令
    /// - zdm configure {config_path}
    /// </summary>
    internal sealed class ConfigureCliCommand : CliCommand
    {
        #region 實作抽象

        /// <summary>CLI 命令的動作
        /// </summary>
        /// <returns>CLI 命令執行結果</returns>
        public override Result<int> Action()
        {
            var result = Result.Create<int>();

            if ( Arguments.IsNullOrEmpty() )
            {
                result.Data    = CliReturnCode.INVALID_COMMAND;
                result.Message = $"Invalid command. Invalid configure path argument.";
                return result;
            }

            var zayniConfigPath = Arguments;

            if ( !File.Exists( zayniConfigPath ) )
            {
                result.Data    = CliReturnCode.INVALID_COMMAND;
                result.Message = $"Invalid command. The config file is not exists. {zayniConfigPath}";
                return result;
            }

            if ( !AppSettingsWritter.Instance.ModifyConfigSetting( "zayniConfigPath", zayniConfigPath, out var errorMsg ) )
            {
                result.Data    = CliReturnCode.INVALID_COMMAND;
                result.Message = $"Invalid command. Invalid configure path argument. {errorMsg}";
                return result;
            }
            
            var configPath = ConfigurationManager.AppSettings[ "zayniConfigPath" ];
            ConfigManagement.ConfigFullPath = configPath;
            ConfigReader.ReadConfig();

            ConsoleLogger.WriteLine( $"Configure zayni framework config file path success!", ConsoleColor.Green );
            ConsoleLogger.WriteLine( $"Config Path: ", ConsoleColor.Green );
            ConsoleLogger.WriteLine( configPath, ConsoleColor.Green );
            
            result.Data    = CliReturnCode.SUCCESS;
            result.Success = true;
            return result;
        }

        #endregion 實作抽象
    }
}