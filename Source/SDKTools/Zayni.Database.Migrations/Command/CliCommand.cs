﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>CLI 命令的基底
    /// </summary>
    internal abstract class CliCommand
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public CliCommand()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="command">原始 CLI 命令動詞</param>
        /// <param name="arguments">原始 CLI 命令的參數</param>
        /// <param name="opts">原始 CLI 命令的參數</param>
        public CliCommand( string command = null, string arguments = null, string opts = null )
        {
            Command   = command;
            Arguments = arguments;
            Options   = opts;
        }

        /// <summary>解構子
        /// </summary>
        ~CliCommand()
        {
            Command   = null;
            Arguments = null;
            Options   = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>原始 CLI 命令完整的主控台輸入參數集合
        /// </summary>
        public string[] FullCommands { get; set; }

        /// <summary>原始 CLI 命令動詞
        /// </summary>
        public string Command { get; set; }

        /// <summary>原始 CLI 命令的參數
        /// </summary>
        public string Arguments { get; set; }

        /// <summary>原始 CLI 命令的選項
        /// </summary>
        public string Options { get; set; }

        #endregion 宣告公開的屬性


        #region 宣告抽象

        /// <summary>CLI 命令的動作
        /// </summary>
        /// <returns>CLI 命令執行結果</returns>
        public abstract Result<int> Action();

        #endregion 宣告抽象
    }
}
