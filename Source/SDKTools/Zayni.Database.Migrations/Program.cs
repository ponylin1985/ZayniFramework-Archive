﻿using System;
using System.Collections.Generic;
using System.Configuration;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>Zayni Framework Database Migrations CLI Application<para/>
    /// 資料庫 Migration 腳本的檔名規則說明如下:<para/>
    /// 1. 基本格式: {yyyyMMdd}_{HHmmss}_{ScriptType}_{ProjectNameOrServiceName}_v{ScriptVersion}.sql<para/>
    /// 2. 全部以下底線當作分隔符號。<para/>
    /// 3. 相同的 ScriptVersion 版本的 ScriptTimestamp 必須完全一致，無論是 upgrade 或 downgrade 類型的腳本。<para/>
    /// 4. ProjectNameOrService 中間可以自行有其他的下底線，但是，ScriptVersion 絕對要放在最後一個段落。<para/>
    /// 5. ScriptTimestamp 基本上只是一個與版本號固定對應的時間戳記，可以自行給定，只要符合第三項原則即可。<para/>
    /// 6. ScriptType 注意有大小寫區別，整個檔案名稱實際上都有大小寫區別。<para/>
    /// 7. 副檔名限定必須是 .sql<para/>
    /// 範例如下:<para/>
    /// 20180313_120000_upgrade_zayni_test_v1.0.0.sql<para/>
    /// 20180315_140500_upgrade_zayni_test_v1.1.0.sql<para/>
    /// 20180315_140500_downgrade_zayni_test_v1.1.0.sql<para/>
    /// 20180315_143000_upgrade_zayni_test_v1.1.1.sql
    /// </summary>
    class Program
    {
        /// <summary>CLI 命令執行引擎
        /// </summary>
        private static CliEngine _cliEngine = new CliEngine();

        /// <summary>Program Enter Point<para/>
        /// CLI Command 命令基本結構如下:<para/>
        /// zdm {command} {args} {options}<para/>
        /// </summary>
        /// <param name="args">CLI 命令列參數</param>
        /// <returns>CLI 命令執行結果回傳值</returns>
        static int Main( params string[] args )
        {
            var zayniConfigPath = ConfigurationManager.AppSettings[ "zayniConfigPath" ];

            if ( zayniConfigPath.IsNullOrEmpty() )
            {
                ConsoleLogger.WriteLine( $"Please configure the zayni framework config file's path for zdm CLI first before any operation.", ConsoleColor.Yellow );
            }
            else 
            {
                ConfigManagement.ConfigFullPath = zayniConfigPath;
                ConfigReader.ReadConfig();
                ConsoleLogger.WriteLine( PrintZayniCliCommand.Symbol, ConsoleColor.Green );
            }

            var cliCommand = CliCommandFactory.Create( args );
            
            var r = _cliEngine.Execute( cliCommand );
            int retrunCode = r.Data;

            if ( !r.Success )
            {
                ConsoleLogger.WriteLine( r.Message, ConsoleColor.Red );
                Environment.Exit( retrunCode );
                return r.Data;
            }
            
            ConsoleLogger.WriteLine( r.Message, ConsoleColor.Green );
            Environment.Exit( retrunCode );
            return retrunCode;
        }
    }
}
