﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Database.Migrations
{
    /// <summary>CLI 命令執行引擎
    /// </summary>
    internal class CliEngine : IExecuteResult<int>
    {
        /// <summary>執行 CLI 命令
        /// </summary>
        /// <param name="execArgs">命令參數</param>
        /// <returns>執行結果</returns>
        public Result<int> Execute( params object[] execArgs )
        {
            var result = Result.Create<int>();

            CliCommand cliCommand = execArgs[ 0 ] as CliCommand;

            if ( cliCommand.IsNull() )
            {
                result.Data = -900;
                return result;
            }

            try
            {
                return cliCommand.Action();
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Data            = -100;
                result.Message         = $"{Logger.GetTraceLogTitle( this, nameof ( Execute ) )} occur exception. {Environment.NewLine}{ex.ToString()}";
                return result;
            }
        }
    }
}
