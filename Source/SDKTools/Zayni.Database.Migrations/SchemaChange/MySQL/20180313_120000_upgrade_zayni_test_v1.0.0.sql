CREATE TABLE `zayni_test` (
    `AccountId` VARCHAR(50) NOT NULL,
    `Name` VARCHAR(50) NOT NULL,
    `Age` INT(11) NOT NULL,
    `Sex` INT(11) NULL DEFAULT NULL,
    `Birthday` DATETIME NULL DEFAULT NULL,
    `IsVIP` BIT(1) NULL DEFAULT NULL COMMENT '資料庫中 Boolean 布林值型別欄位，在MySQL資料庫中: 1. MySQL資料庫可以開成 Bit(1) 或 TinyInt(1)。2. 程式中 MySqlDbType.Bit 可以直接對應。使用 MySqlDbType.Int32 也是可以，但比較不建議。3. 程式中資料值可以直接為 bool 型別，或是將 bool 依照 0 = false，1 = true 的規則，轉換成 Int32 之後都可以。4. 查詢使用者DataAdapter.Fill之後取得DataTable之後，直接使用 Newtonsoft.Json 進行ORM轉換，可以直接轉換成正確的 bool 型別。',
    `IsGood` BIT(1) NULL DEFAULT NULL,
    `DataFlag` TIMESTAMP (6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) on update CURRENT_TIMESTAMP(6)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;