﻿Zayni Framework Database Migration CLI

Create By: Pony Lin
Create Date: 2018-03-15
Update Date: 2018-03-15

=========================================

This is a RDBMS database schema and data migration CLI application. It helps to migrate the different schema and data to target database.
Now it can support MySQL, MSSQL & Oracle database.

=========================================

If you want to try out this CLI tool. Please refer to the following steps:

1. Create an empty database.
2. Setup the correct connectionStrings in the app.config file.
3. Rebuild the project.
4. Run the zdm CLI commands.

Example:

zdm migration 1.1.2
zdm migration 1.0.0

5. If you want to clean and restore all the migration verion. Please use the following SQL statements in your database.

TRUNCATE TABLE fs_db_migration;
DROP TABLE zayni_test;
DROP TABLE zayni_test2;