﻿using System;
using System.Linq;
using ZayniFramework.Common;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.IO;


namespace ZayniFramework.Crypto.Helper
{
    /// <summary>Zayni Framework Crypto Helper dotnet tool CLI application<para/>
    /// <para>* 以下為範例:</para>
    /// <para>  * `dotnet tool zch genkey -o /Users/pony/GitRepo/MyGitLab/ZayniFramework/Source/SDKTools/Zayni.Crypto.Helper/Crypto.key`</para>
    /// <para>  * `dotnet tool zch encrypt -f /Users/pony/GitRepo/MyGitLab/ZayniFramework/Source/SDKTools/Zayni.Crypto.Helper/Test.dll.config -k jXnKj2NdjGs7w`</para>
    /// </summary>
    class Program
    {
        /// <summary>主程式<para/>
        /// <para>* 產生金鑰檔案: `zch genkey -o {output_path}`</para>
        /// <para>* 執行對 .NET config 設定檔中的 db connection string 加密: `zch encrypt -f {config_path} -k {encryptKeyString}`</para>
        /// </summary>
        /// <param name="args">命令列參數</param>
        /// <returns>應用程式回傳碼</returns>
        static int Main( params string[] args )
        {
            try
            {
                args = args.Where( h => h.IsNotNullOrEmpty() ).ToArray();

                if ( args.IsNullOrEmptyArray() )
                {
                    Stdout( $"Welcome to use Zayni Framework Crypto Helper dotnet tool CLI application." );
                    Stdout( "dotnet tool version: 2.14.122." );
                    Stdout( "Use the following commands to generate key file for ZayniFramework.Cryptography module or" );
                    Stdout( "Encrypt the db connection string for specific .NET config file." );
                    Command.Stdout( "zch genkey -o {output_path}", ConsoleColor.Yellow, false );
                    Command.Stdout( "zch encrypt -f {config_path} -k {encryptKeyString}", ConsoleColor.Yellow, false );
                    return 0;
                }

                var command = args.FirstOrDefault()?.Trim()?.ToLower();

                if ( command != "genkey" && command != "encrypt" )
                {
                    StdErr( $"Unknow zch command." );
                    StdErr( "Use the following commands to generate key file for ZayniFramework.Cryptography module or" );
                    StdErr( "Encrypt the db connection string for specific .NET config file." );
                    Command.Stdout( "zch genkey -o {output_path}", ConsoleColor.Yellow, false );
                    Command.Stdout( "zch encrypt -f {config_path} -k {encryptKeyString}", ConsoleColor.Yellow, false );
                    return -1;
                }

                if ( command == "genkey" )
                {
                    if ( args.Length != 3 )
                    {
                        StdErr( $"Invalid zch genkey command." );
                        StdErr( "Valid command --> zch genkey -o {output_path}" );
                        return -1;
                    }

                    if ( args.Where( g => g == "-o" ).Count() != 1 )
                    {
                        StdErr( $"Invalid zch genkey command." );
                        StdErr( "Valid command --> zch genkey -o {output_path}" );
                        return -1;
                    }

                    int o = Array.FindIndex( args, z => z == "-o" );
                    var path = args[ o + 1 ]?.Trim();

                    if ( !path.EndsWith( ".key" ) )
                    {
                        StdErr( $"Invalid zch genkey command." );
                        StdErr( $"Invalid output_path argument. The output path must ends with '.key' extension name." );
                        return -1;
                    }

                    var sb = new StringBuilder();

                    new For().Do( 5, () => 
                    {
                        string text = RandomTextHelper.Create( 100 );
                        sb.AppendLine( text );
                    } );

                    var c = CreateKeyFile( path, sb.ToString() );

                    if ( !c.Success )
                    {
                        StdErr( $"Create key file occur error. {Environment.NewLine}{c.Message}" );
                        return -1;
                    }

                    Stdout( $"Encryption key file created!" );
                    Stdout( $"Key file: {path}" );
                    return 0;
                }

                if ( command == "encrypt" )
                {
                    if ( args.Length != 5 )
                    {
                        StdErr( $"Invalid zch encrypt command." );
                        StdErr( "Valid command --> zch encrypt -f {config_path} -k {encryptKeyString}" );
                        return -1;
                    }

                    if ( args.Where( g => g == "-f" ).Count() != 1 || args.Where( g => g == "-k" ).Count() != 1 )
                    {
                        StdErr( $"Invalid zch encrypt command." );
                        StdErr( "Valid command --> zch encrypt -f {config_path} -k {encryptKeyString}" );
                        return -1;
                    }

                    int f = Array.FindIndex( args, f => f == "-f" );
                    var configPath = args[ f + 1 ]?.Trim();

                    int k = Array.FindIndex( args, f => f == "-k" );
                    var cryptoKey = args[ k + 1 ]?.Trim();

                    if ( configPath.IsNullOrEmpty() )
                    {
                        StdErr( $"Invalid zch encrypt command." );
                        StdErr( "Valid command --> zch encrypt -f {config_path} -k {encryptKeyString}" );
                        return -1;
                    }

                    if ( !configPath.EndsWith( ".config" ) )
                    {
                        StdErr( $"Invalid zch encrypt command." );
                        StdErr( $"Invalid config_path argument. The path of .NET config file must ends with '.config' extension name." );
                        return -1;
                    }

                    var fileConfigSource  = new FileConfigSource( configPath );
                    var connStringSection = fileConfigSource.GetConnectionStringsSection();

                    if ( connStringSection.IsNullOrEmpty() )
                    {
                        Command.Stdout( $"No db connection string found in config file.", ConsoleColor.Yellow, false );
                        return 0;
                    }

                    foreach ( ConnectionStringSettings connStringConfig in connStringSection )
                    {
                        if ( 0 == string.Compare( connStringConfig.Name, "LocalSqlServer", true ) )
                        {
                            continue;
                        }

                        if ( connStringConfig.ConnectionString.IsNullOrEmpty() )
                        {
                            continue;
                        }

                        var encryptString = AesEncryptBase64( connStringConfig.ConnectionString, cryptoKey );
                        connStringConfig.ConnectionString = encryptString;
                    }

                    fileConfigSource.Save();
                    Stdout( $"Database connection string encrypt successfully!" );
                    return 0;
                }

                Console.WriteLine();
                return 0;
            }
            catch ( Exception ex )
            {
                StdErr( $"Error!{Environment.NewLine}{ex.ToString()}" );
                return -1;
            }
        }

        /// <summary>產生金鑰
        /// </summary>
        /// <param name="path">金鑰路徑</param>
        /// <param name="keyText">金鑰文字內容</param>
        /// <returns>產生金鑰是否成功</returns>
        private static Result CreateKeyFile( string path, string keyText )
        {
            var result = Result.Create();

            try
            {
                Check.IsTrue( File.Exists( path ), () => File.Delete( path ) );
                File.WriteAllText( path, keyText );
            }
            catch ( Exception ex )
            {
                result.Message = ex.ToString();
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>AES 對稱式加密
        /// </summary>
        /// <param name="source">加密前字串</param>
        /// <param name="cryptoKey">加密金鑰</param>
        /// <returns>加密後字串</returns>
        public static string AesEncryptBase64( string source, string cryptoKey )
        {
            string encrypt = "";

            try
            {
                var aes    = new AesCryptoServiceProvider();
                var md5    = new MD5CryptoServiceProvider();
                var sha256 = new SHA256CryptoServiceProvider();

                byte[] key = sha256.ComputeHash( Encoding.UTF8.GetBytes( cryptoKey ) );
                byte[] iv  = md5.ComputeHash( Encoding.UTF8.GetBytes( cryptoKey ) );

                aes.Key = key;
                aes.IV  = iv;

                byte[] dataByteArray = Encoding.UTF8.GetBytes( source );

                using ( var ms = new MemoryStream() )
                using ( var cs = new CryptoStream( ms, aes.CreateEncryptor(), CryptoStreamMode.Write ) )
                {
                    cs.Write( dataByteArray, 0, dataByteArray.Length );
                    cs.FlushFinalBlock();
                    encrypt = Convert.ToBase64String( ms.ToArray() );
                }
            }
            catch ( Exception ex )
            {
                throw ex;
            }

            return encrypt;
        }

        /// <summary>Console 主控台標準輸出
        /// </summary>
        /// <param name="message">訊息</param>
        private static void Stdout( string message ) => Command.Stdout( message, ConsoleColor.Green, false );

        /// <summary>Console 主控台標準錯誤輸出
        /// </summary>
        /// <param name="message">訊息</param>
        private static void StdErr( string message ) => Command.Stdout( message, ConsoleColor.Red, false );
    }
}
