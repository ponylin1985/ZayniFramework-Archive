﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization
{
    /// <summary>ZayniFramework 框架的序列化元件序列化元件工廠
    /// </summary>
    public static class SerializerFactory
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>序列化處理器容器
        /// </summary>
        /// <returns></returns>
        private static readonly Dictionary<string, ISerializer> _serializers = new Dictionary<string, ISerializer>();

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static SerializerFactory() 
        {
            using ( _asyncLock.Lock() )
            {
                _serializers.Add( "System.Text.Json", new JsonSerialize() );
                _serializers.Add( "Json.NET",         new JsonNetSerializer() );
                _serializers.Add( "XML",              new XmlSerializer() );
                _serializers.Add( "BinaryFormatter",  new BinaryFormatSerailizer() );
            }
        }

        #endregion 宣告靜態建構子


        #region 宣告公開的靜態方法

        /// <summary>註冊 ISerializer 序列化處理器物件到 ISerializerContainer 容器中。<para/>
        /// <para>內建已經包含以下的序列化元件:</para>
        /// <para>  * System.Text.Json</para>
        /// <para>  * Json.NET (Newtonsoft.Json)</para>
        /// <para>  * XML</para>
        /// <para>  * BinaryFormatter</para>
        /// </summary>
        /// <param name="name">ISerializer 序列化處理器的唯一識別名稱</param>
        /// <param name="serializer">ISerializer 序列化處理器物件</param>
        /// <returns>註冊結果</returns>
        public static IResult Register( string name, ISerializer serializer ) 
        {
            using ( _asyncLock.Lock() ) 
            {
                var result = new Result() 
                {
                    Success = false,
                    Message   = null
                };

                try
                {
                    if ( new List<string>() { "System.Text.Json", "Json.NET", "XML", "BinaryFormatter" }.Exists( g => 0 == string.Compare( g, name, true ) ) )
                    {
                        result.Success = true;
                        return result;   
                    }

                    if ( _serializers.ContainsKey( name ) )
                    {
                        result.Message = $"The serializer is duplicated: {name}.";
                        return result;
                    }

                    if ( serializer.IsNull() )
                    {
                        result.Message = $"Not allowed register null object into ISerializer container. Name: {name}";
                        return result;
                    }

                    _serializers.Add( name, serializer );                
                }
                catch ( Exception ex )
                {
                    result.ExceptionObject = ex;
                    result.Message = $"Register ISerializer object occur exception. Name: {name}. {Environment.NewLine}{ex.ToString()}";
                    return result;
                }

                result.Success = true;
                return result;
            }
        }


        /// <summary>建立序列化處理器<para/>
        /// * System.Text.Json (JSON String serialization, better JSON performance.)<para/>
        /// * Json.NET (JSON String serialization)<para/>
        /// * BinaryFormatter (Byte[] serialization)<para/>
        /// </summary>
        /// <param name="serializerName">序列化處理器種類:<para/>
        /// * System.Text.Json (JSON String serialization, better JSON performance.)<para/>
        /// * Json.NET (JSON String serialization)<para/>
        /// * BinaryFormatter (Byte[] serialization)<para/>
        /// </param>
        /// <returns>序列化處理器</returns>
        public static ISerializer Create( string serializerName ) 
        {
            using ( _asyncLock.Lock() )
            {
                if ( !_serializers.ContainsKey( serializerName ) )
                {
                    throw new ArgumentException( $"Unsupport serializerType: {serializerName}." );
                }

                return _serializers[ serializerName ];
            }
        }

        #endregion 宣告公開的靜態方法
    }
}
