﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization
{
    /// <summary>可進行XML序列化的字典資料集合
    /// </summary>
    /// <typeparam name="TKey">索引泛型</typeparam>
    /// <typeparam name="TValue">資料質泛型</typeparam>
    public class XmlSerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
    {
        #region 實作IXmlSerializable介面方法

        /// <summary>讀取XML綱要 (此方法尚未實作)
        /// </summary>
        /// <returns>XML綱要</returns>
        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }
 
        /// <summary>將XML字串反序列化為字典物件
        /// </summary>
        /// <param name="reader">XML讀取器</param>
        public void ReadXml( XmlReader reader )
        {
            TKey   key   = default ( TKey );
            TValue value = default ( TValue );
 
            if ( reader.IsNull() || reader.IsEmptyElement ) 
            {
                return;
            }
                
 
            while ( reader.Read() )
            {
                if ( XmlNodeType.Element != reader.NodeType )
                {
                    continue;
                }

                if ( "Item" == reader.LocalName )
                {
                    string keyXml   = reader.GetAttribute( "Key" );
                    string valueXml = reader.GetAttribute( "Value" );

                    if ( new string[] { keyXml, valueXml }.Any( y => y.IsNullOrEmpty() ) )
                    {
                        continue;
                    }

                    key   = (TKey)DeserializeXml( keyXml, typeof ( TKey ) );
                    value = (TValue)DeserializeXml( valueXml, typeof ( TValue ) );

                    if ( !base.ContainsKey( key ) )
                    {
                        base.Add( key, value );
                    }
                }
            }
        }
 
        /// <summary>將字典物件序列化成XML字串
        /// </summary>
        /// <param name="writer">XML寫入器</param>
        public void WriteXml( XmlWriter writer )
        {
            foreach ( TKey key in this.Keys )
            {
                writer.WriteStartElement( "Item" );

                var obj = this[ key ];

                if ( obj.IsNull() )
                {
                    continue;
                }

                string keyXml   = SerializeObject( key );
                string valueXml = SerializeObject( obj );

                writer.WriteAttributeString( "Key",   keyXml );
                writer.WriteAttributeString( "Value", valueXml );
                writer.WriteEndElement();
            }
        }

        #endregion 實作IXmlSerializable介面方法

        
        #region 宣告私有的序列化方法

        /// <summary>將目標物件序列化成XML字串
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <returns>XML字串</returns>
        private string SerializeObject( object target )
        {
            var serializer = new XmlSerializerComponent();
            SerializeResult sResult = serializer.Serialize( target, target.GetType() );

            if ( !sResult.Success )
            {
                return string.Empty;
            }

            string result = sResult.SerializedString;
            return result;
        }

        /// <summary>將目標XML字串反序列化成物件
        /// </summary>
        /// <param name="target">目標XML字串</param>
        /// <param name="type">物件型別</param>
        /// <returns>物件實體</returns>
        private object DeserializeXml( string target, Type type )
        {
            var serializer = new XmlSerializerComponent();
            DeserializeResult dResult = serializer.Deserialize( target, type );

            if ( !dResult.Success )
            {
                return null;
            }

            object result = dResult.Datas;
            return result;
        }

        #endregion 宣告私有的序列化方法
    }
}
