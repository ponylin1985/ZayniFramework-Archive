using Sys = System.Xml.Serialization;
using System;
using System.IO;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization
{
    /// <summary>XML格式序列化元件
    /// </summary>
    internal class XmlSerializerComponent : IStringSerializer
    {
        #region 宣告私有的實體欄位

        /// <summary>XmlSerializer元件的預設命名空間
        /// </summary>
        private string _xmlNamespace = "http://schemas.datacontract.org/2004/07/{0}";

        #endregion 宣告私有的實體欄位
        

        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        /// <param name="assemblyName">組件名稱</param>
        public XmlSerializerComponent( string assemblyName = null )
        {
            _xmlNamespace = string.IsNullOrWhiteSpace( assemblyName ) ? "" : _xmlNamespace = string.Format( _xmlNamespace, assemblyName );
        }

        #endregion 宣告建構子
        

        #region 實作IStringSerializer介面方法

        /// <summary>序列化目標物件
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <param name="type">目標物件的型別</param>
        /// <returns>序列化結果</returns>
        public SerializeResult Serialize( object target, Type type = null )
        {
            var result = new SerializeResult() 
            {
                Success        = false,
                SerializedString = ""
            };

            string xmlString = "";

            try
            {
                var xmlSerializer = new Sys.XmlSerializer( type );

                using ( MemoryStream stream = new MemoryStream() )
                {
                    xmlSerializer.Serialize( stream, target );
                    stream.Flush();
                    stream.Seek( 0, SeekOrigin.Begin );

                    using ( var reader = new StreamReader( stream ) )
                    {
                        xmlString = reader.ReadToEnd();
                        reader.Close();
                    }
                }
            }
            catch ( Exception ex )
            {
                result.HasException    = true;
                result.ExceptionObject = ex;
                result.Message         = "序列化目標物件成XML字串錯誤: {0}".FormatTo( ex.ToString() );
                return result;
            }

            result.Message          = "序列化目標物件成XML字串成功";
            result.SerializedString = xmlString;
            result.Success        = true;

            return result;
        }

        /// <summary>序列化目標物件
        /// </summary>
        /// <typeparam name="TSource">目標物件的泛型</typeparam>
        /// <param name="target">目標物件</param>
        /// <returns>序列化結果</returns>
        public SerializeResult Serialize<TSource>( object target )
        {
            return Serialize( target, typeof ( TSource ) );
        }

        /// <summary>反序列化目標字串
        /// </summary>
        /// <param name="target">目標字串</param>
        /// <param name="type">結果物件的型別</param>
        /// <returns>反序列化結果</returns>
        public DeserializeResult Deserialize( string target, Type type = null )
        {
            var result = new DeserializeResult() 
            {
                Success = false,
                Datas     = null
            };

            object obj = null;

            byte[] bytes = target.ToBytes();

            try
            {
                var xmlSerializer = _xmlNamespace.IsNullOrEmpty()  ? 
                                    new Sys.XmlSerializer( type )  : 
                                    new Sys.XmlSerializer( type, _xmlNamespace );

                using ( var ms = new MemoryStream( bytes ) )
                {
                    obj = xmlSerializer.Deserialize( ms );
                }
            }
            catch ( Exception ex )
            {
                result.HasException    = true;
                result.ExceptionObject = ex;
                result.Message         = "反序列化目標XML成物件失敗: {0}".FormatTo( ex.ToString() );
                return result;
            }

            result.Message   = "反序列化目標XML成物件成功";
            result.Datas     = obj;
            result.Success = true;

            return result;
        }

        /// <summary>反序列化目標字串
        /// </summary>
        /// <typeparam name="TSource">結果物件的泛型</typeparam>
        /// <param name="target">目標字串</param>
        /// <returns>反序列化結果</returns>
        public DeserializeResult<TSource> Deserialize<TSource>( string target )
            where TSource : class, new()
        {
            var result = new DeserializeResult<TSource>() 
            {
                Success = false,
                Datas     = null
            };

            Type type = typeof( TSource );

            DeserializeResult deResult = Deserialize( target, type );

            if ( !deResult.Success )
            {
                result.Message = deResult.Message;
                return result;
            }

            TSource model = deResult.Datas as TSource;

            if ( model.IsNull() )
            {
                result.Message = "反序列化目標XML成物件失敗";
                return result;
            }

            result.Message   = "反序列化目標XML成物件成功";
            result.Datas     = model;
            result.Success = true;
            return result;
        }

        /// <summary>以非同步序列化目標物件
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <param name="type">目標物件的型別</param>
        /// <returns>序列化結果</returns>
        public async Task<SerializeResult> SerializeAsync( object target, Type type = null )
        {
            return await Task.Factory.StartNew<SerializeResult>( () => { return Serialize( target, type ); } );
        }

        /// <summary>以非同步序列化目標物件
        /// </summary>
        /// <typeparam name="TSource">目標物件的泛型</typeparam>
        /// <param name="target">目標物件</param>
        /// <returns>序列化結果</returns>
        public async Task<SerializeResult> SerializeAsync<TSource>( object target )
        {
            return await SerializeAsync( target, typeof ( TSource ) );
        }

        /// <summary>以非同步反序列化目標字串
        /// </summary>
        /// <param name="target">目標字串</param>
        /// <param name="type">結果物件的型別</param>
        /// <returns>反序列化結果</returns>
        public async Task<DeserializeResult> DeserializeAsync( string target, Type type = null )
        {
            return await Task.Factory.StartNew<DeserializeResult>( () => { return Deserialize( target, type ); } );
        }

        /// <summary>以非同步反序列化目標字串
        /// </summary>
        /// <typeparam name="TSource">結果物件的泛型</typeparam>
        /// <param name="target">目標字串</param>
        /// <returns>反序列化結果</returns>
        public async Task<DeserializeResult<TSource>> DeserializeAsync<TSource>( string target ) 
            where TSource : class, new()
        {
            var result = new DeserializeResult<TSource>() 
            {
                Success = false,
                Datas     = null
            };

            Type type = typeof( TSource );

            DeserializeResult deResult = await DeserializeAsync( target, type );

            if ( !deResult.Success )
            {
                result.Message = deResult.Message;
                return result;
            }

            TSource model = deResult.Datas as TSource;

            if ( model.IsNull() )
            {
                result.Message = "反序列化目標XML成物件失敗";
                return result;
            }

            result.Message   = "反序列化目標XML成物件成功";
            result.Datas     = model;
            result.Success = true;
            return result;
        }

        #endregion 實作IStringSerializer介面方法
    }
}