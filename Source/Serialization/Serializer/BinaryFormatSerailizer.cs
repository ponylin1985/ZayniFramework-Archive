using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization
{
    /// <summary>BinaryFormatter 序列化處理器
    /// </summary>
    public class BinaryFormatSerailizer : ISerializer
    {
        /// <summary>序列化物件，回傳值為 byte[] 陣列
        /// </summary>
        /// <param name="obj">目標序列化物件</param>
        /// <returns>序列化結果 Byte[] 陣列</returns>
        public object Serialize( object obj )
        {
            var formatter = new BinaryFormatter();

            using ( var stream = new MemoryStream() )
            {
                formatter.Serialize( stream, obj );
                stream.Seek( 0, SeekOrigin.Begin );
                return stream.ToArray();
            }
        }

        /// <summary>序列化物件，回傳值為 byte[] 陣列
        /// </summary>
        /// <param name="obj">目標序列化物件</param>
        /// <returns>序列化結果 Byte[] 陣列</returns>
        public byte[] SerializeObject( object obj ) => (byte[])Serialize( obj );

        /// <summary>反序列化 byte[] 陣列
        /// </summary>
        /// <param name="obj">byte[] 陣列</param>
        /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
        /// <returns>反序列化後的物件</returns>
        public TResult Deserialize<TResult>( object obj )
        {
            var bytes     = (byte[])obj;
            var formatter = new BinaryFormatter();
            
            using ( var stream = new MemoryStream( bytes ) ) 
            {
                return (TResult)formatter.Deserialize( stream );
            }
        }

        /// <summary>反序列化 byte[] 陣列
        /// </summary>
        /// <param name="bytes">byte[] 陣列</param>
        /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
        /// <returns>反序列化後的物件</returns>
        public TResult DeserializeObject<TResult>( byte[] bytes ) => Deserialize<TResult>( bytes );
    }
}