﻿using Sys = System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Xml;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization
{
    /// <summary>XML 序列化處理器
    /// </summary>
    public class XmlSerializer : ISerializer
    {
        /// <summary>序列化物件，回傳值為 XML 字串
        /// </summary>
        /// <param name="obj">目標序列化物件</param>
        /// <returns>序列化結果 XML 字串</returns>
        public object Serialize( object obj )
        {
            var sb = new StringBuilder();
            var sw = new StringWriter( sb );
            var serializer = new Sys.XmlSerializer( obj.GetType() );
            serializer.Serialize( sw, obj );
            return sb.ToString();
        }

        /// <summary>序列化物件，回傳值為 XML 字串
        /// </summary>
        /// <param name="obj">目標序列化物件</param>
        /// <returns>序列化結果 XML 字串</returns>
        public string SerializeObject( object obj ) => (string)Serialize( obj );

        /// <summary>反序列化 XML 字串
        /// </summary>
        /// <param name="obj">XML 字串</param>
        /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
        /// <returns>反序列化後的物件</returns>
        public TResult Deserialize<TResult>( object obj )
        {
            var xdoc = new XmlDocument();
            var xml  = obj + "";
            xdoc.LoadXml( xml );

            var reader     = new XmlNodeReader( xdoc.DocumentElement );
            var serializer = new Sys.XmlSerializer( typeof ( TResult ) );
            return (TResult)serializer.Deserialize( reader );
        }

        /// <summary>反序列化 XML 字串
        /// </summary>
        /// <param name="xml">XML 字串</param>
        /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
        /// <returns>反序列化後的物件</returns>
        public TResult DeserializeObject<TResult>( string xml ) => Deserialize<TResult>( xml );
    }
}
