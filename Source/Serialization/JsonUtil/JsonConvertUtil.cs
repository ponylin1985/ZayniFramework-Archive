﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using ZayniFramework.Common;


namespace ZayniFramework.Serialization
{
    /// <summary>Newtonsoft.Json 序列化處理器
    /// </summary>
    public static class JsonConvertUtil
    {
        /// <summary>小寫駝峰 Camel-case 風格的 JSON 字串序列化設定
        /// </summary>
        private static readonly JsonSerializerSettings _camelCaseSetting = new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver() };

        /// <summary>將物件序列化成 JSON 字串 (強制支援 Formatting.Indented)
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <returns>JSON格式字串</returns>
        public static string Serialize( object obj ) => JsonConvert.SerializeObject( obj, Formatting.Indented );

        /// <summary>將物件序列化成 JSON 字串，日期時間格式以 UNIX Timestamp 時間戳記表達。(強制支援 Formatting.Indented)
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="timestampWithMs">UNIX Timestamp 時間戳記是否包含毫秒數</param>
        /// <returns>JSON格式字串</returns>
        public static string Serialize( object obj, bool timestampWithMs = false ) => 
            JsonConvert.SerializeObject( obj, Formatting.Indented, new TimestampConverter( timestampWithMs ) );

        /// <summary>將物件序列化成 Camel-case 風格的 JSON 字串 (強制支援 Formatting.Indented)
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <returns>JSON格式字串</returns>
        public static string SerializeInCamelCase( object obj ) => JsonConvert.SerializeObject( obj, Formatting.Indented, _camelCaseSetting );

        /// <summary>將物件序列化成 Camel-case 風格的 JSON 字串 (強制支援 Formatting.Indented)
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="timestampWithMs">UNIX Timestamp 時間戳記是否包含毫秒數</param>
        /// <returns>JSON格式字串</returns>
        public static string SerializeInCamelCase( object obj, bool timestampWithMs = false ) => 
            JsonConvert.SerializeObject( obj, Formatting.Indented, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter>() { new TimestampConverter( timestampWithMs ) } } );

        /// <summary>將物件序列化成 Camel-case 風格的 JSON 字串，但 JSON 字串沒有格式化處理!
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <returns>JSON格式字串</returns>
        public static string SerializeInCamelCaseNoFormatting( object obj ) => JsonConvert.SerializeObject( obj, _camelCaseSetting );

        /// <summary>將物件序列化成 Camel-case 風格的 JSON 字串，但 JSON 字串沒有格式化處理!
        /// </summary>
        /// <param name="obj">目標物件</param>
        /// <param name="timestampWithMs">UNIX Timestamp 時間戳記是否包含毫秒數</param>
        /// <returns>JSON格式字串</returns>
        public static string SerializeInCamelCaseNoFormatting( object obj, bool timestampWithMs = false ) => 
            JsonConvert.SerializeObject( obj, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter>() { new TimestampConverter( timestampWithMs ) } } );

        /// <summary>將來源的 JSON 字串以 Camel-case 風格進行格式化處理
        /// </summary>
        /// <param name="json">來源 JSON 字串</param>
        /// <returns>Camel-case 風格的 JSON 字串</returns>
        public static string FormatToCamelCase( string json ) => IsValidJson( json ) ? JValue.Parse( json ).ToString( Formatting.Indented ) : json;

        /// <summary>檢查傳入的來源字串是否為合法的 JSON 格式字串<para/>
        /// Reference: https://stackoverflow.com/questions/14977848/how-to-make-sure-that-string-is-valid-json-using-json-net
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <returns>是否為合法的 JSON 格式字串</returns>
        public static bool IsValidJson( string source )
        {
            if ( source.IsNullOrEmpty() )
            {
                return false;
            }

            source = source.Trim();

            if ( ( source.StartsWith( "{" ) && source.EndsWith( "}" ) ) ||
                 ( source.StartsWith( "[" ) && source.EndsWith( "]" ) ) )
            {
                try
                {
                    var obj = JToken.Parse( source );
                    return true;
                }
                catch ( JsonReaderException )
                {
                    return false;
                }
                catch ( Exception )
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>JSON 反序列化成物件 (可以接受 CamelCase 或不是 CamelCase 的 JSON 字串)
        /// </summary>
        /// <param name="json">JSON 字串</param>
        /// <returns>反序列化結果物件</returns>
        public static object Deserialize( string json ) => JsonConvert.DeserializeObject( json );

        /// <summary>將 Camel-case 風格的 JSON 反序列化成物件
        /// </summary>
        /// <param name="json">Camel-case 風格的 JSON 字串</param>
        /// <returns>反序列化結果物件</returns>
        public static object DeserializeFromCamelCase( string json ) => JsonConvert.DeserializeObject( json, _camelCaseSetting );

        /// <summary>將 Camel-case 風格的 JSON 反序列化成物件
        /// </summary>
        /// <param name="json">Camel-case 風格的 JSON 字串</param>
        /// <param name="timestampWithMs">UNIX Timestamp 時間戳記是否包含毫秒數</param>
        /// <returns>反序列化結果物件</returns>
        public static object DeserializeFromCamelCase( string json, bool timestampWithMs = false ) => 
            JsonConvert.DeserializeObject( json, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter>() { new TimestampConverter( timestampWithMs ) } } );

        /// <summary>JSON 反序列化成物件 (可以接受 CamelCase 或不是 CamelCase 的 JSON 字串)
        /// </summary>
        /// <typeparam name="TData">載體物件指定的泛型</typeparam>
        /// <param name="json">JSON 字串</param>
        /// <returns>反序列化結果物件</returns>
        public static TData Deserialize<TData>( string json ) => JsonConvert.DeserializeObject<TData>( json );

        /// <summary>JSON 反序列化成物件 (可以接受 CamelCase 或不是 CamelCase 的 JSON 字串)
        /// </summary>
        /// <typeparam name="TData">載體物件指定的泛型</typeparam>
        /// <param name="json">JSON 字串</param>
        /// <param name="timestampWithMs">UNIX Timestamp 時間戳記是否包含毫秒數</param>
        /// <returns>反序列化結果物件</returns>
        public static TData Deserialize<TData>( string json, bool timestampWithMs = false ) => 
            JsonConvert.DeserializeObject<TData>( json, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter>() { new TimestampConverter( timestampWithMs ) } } );

        /// <summary>將 Camel-case 風格的 JSON 反序列化成物件
        /// </summary>
        /// <typeparam name="TData">載體物件指定的泛型</typeparam>
        /// <param name="json">Camel-case 風格的 JSON 字串</param>
        /// <returns>反序列化結果物件</returns>
        public static TData DeserializeFromCamelCase<TData>( string json ) => JsonConvert.DeserializeObject<TData>( json, _camelCaseSetting );

        /// <summary>將 Camel-case 風格的 JSON 反序列化成物件
        /// </summary>
        /// <typeparam name="TData">載體物件指定的泛型</typeparam>
        /// <param name="json">Camel-case 風格的 JSON 字串</param>
        /// <param name="timestampWithMs">UNIX Timestamp 時間戳記是否包含毫秒數</param>
        /// <returns>反序列化結果物件</returns>
        public static TData DeserializeFromCamelCase<TData>( string json, bool timestampWithMs = false ) => 
            JsonConvert.DeserializeObject<TData>( json, new JsonSerializerSettings() { ContractResolver = new CamelCasePropertyNamesContractResolver(), Converters = new List<JsonConverter>() { new TimestampConverter( timestampWithMs ) } } );
    }
}
