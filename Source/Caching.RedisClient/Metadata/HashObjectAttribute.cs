﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Caching.RedisClientComponent
{
    /// <summary>Redis Hash 的資料型別
    /// </summary>
    [AttributeUsage( AttributeTargets.Property | AttributeTargets.Field )]
    public class HashObjectAttribute : Attribute
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public HashObjectAttribute()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="name">以 Hash 資料結構存放至 Redis Server 後的 SubKey 名稱</param>
        /// <param name="defaultValue">資料預設值</param>
        public HashObjectAttribute( string name, string defaultValue = null )
        {
            if ( name.IsNullOrEmpty() )
            {
                throw new ArgumentException( $"The argument '{nameof ( name )}' is not allowed null or empty string." );
            }

            Name         = name;
            DefaultValue = defaultValue;
        }

        /// <summary>解構子
        /// </summary>
        ~HashObjectAttribute()
        {
            Name         = null;
            DefaultValue = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>以 Hash 資料結構存放至 Redis Server 後的 SubKey 名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>資料預設值<para/>
        /// 1. 當進行 HashSet 資料存放時，如果檢查到資料為 Null 值，則以此預設值進行儲存。<para/>
        /// 2. 當進行 HashGet 查詢時，如果檢查到資料為 Null 值，則以此預設值設定屬性值。<para/>
        /// </summary>
        public string DefaultValue { get; set; }

        #endregion 宣告公開的屬性
    }
}
