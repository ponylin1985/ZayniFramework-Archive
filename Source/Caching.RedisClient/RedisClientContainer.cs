﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching.RedisClientComponent
{
    /// <summary>RedisClient 客戶端容器
    /// </summary>
    public static class RedisClientContainer
    {
        #region 宣告私有的欄位

        /// <summary>多執行緒所定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>RedisClient 客戶端容器
        /// </summary>
        private static readonly Dictionary<string, RedisClient> _container = new Dictionary<string, RedisClient>();

        #endregion 宣告私有的欄位


        #region 宣告公開的方法

        /// <summary>取得 Config 組態設定中預設的 RedisClient 客戶端個體
        /// </summary>
        /// <returns></returns>
        public static RedisClient Get()
        {
            string name = ConfigReader.GetDefaultClientConfig()?.Name;

            if ( name.IsNullOrEmpty() )
            {
                return null;
            }

            return Get( name );
        }

        /// <summary>取得 RedisClient 客戶端個體
        /// </summary>
        /// <param name="name">Redis Client 客戶端 Config 設定名稱</param>
        /// <returns></returns>
        public static RedisClient Get( string name )
        {
            using ( _asyncLock.Lock() )
            {
                if ( name.IsNullOrEmpty() )
                {
                    throw new ArgumentException( $"Invalid arguments '{nameof ( name )}'. Can not be null or empty string." );
                }

                if ( _container.ContainsKey( name ) )
                {
                    return _container[ name ];
                }

                var redisClient = new RedisClient( name );

                try
                {
                    _container.Add( name, redisClient );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( nameof ( RedisClientContainer ), ex, Logger.GetTraceLogTitle( nameof ( RedisClientContainer ), nameof ( Get ) ), $"Add RedisClient to container occur exception. RedisClientName: {name}." );
                    throw ex;
                }

                return redisClient;
            }
        }

        #endregion 宣告公開的方法
    }
}
