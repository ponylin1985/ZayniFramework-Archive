﻿using ZayniFramework.Common;


namespace ZayniFramework.Caching.RedisClientComponent
{
    /// <summary>組態設定讀取器
    /// </summary>
    internal static class ConfigReader
    {
        /// <summary>取得 RedisClient 客戶端個體的設定
        /// </summary>
        /// <param name="redisClientName">RedisClient 客戶端個體的 Config 設定名稱</param>
        /// <returns>RedisClient 客戶端個體的設定</returns>
        internal static RedisClientElement GetRedisClientConfig( string redisClientName ) => 
            ConfigManagement.ZayniConfigs?.RedisClientSettings?.RedisClients?[ redisClientName ];

        /// <summary>取得預設的 RedisClient 客戶端個體的設定
        /// </summary>
        /// <returns>預設的 RedisClient 客戶端個體的設定</returns>
        internal static RedisClientElement GetDefaultClientConfig()
        {
            if ( ConfigManagement.ZayniConfigs.IsNull() || ConfigManagement.ZayniConfigs.RedisClientSettings.IsNull() || ConfigManagement.ZayniConfigs.RedisClientSettings.RedisClients.IsNullOrEmpty() )
            {
                return null;
            }

            foreach ( var config in ConfigManagement.ZayniConfigs?.RedisClientSettings?.RedisClients )
            {
                RedisClientElement cfg = (RedisClientElement)config;

                if ( cfg.IsDefault )
                {
                    return cfg;
                }
            }

            return ConfigManagement.ZayniConfigs?.RedisClientSettings?.RedisClients[ 0 ];
        }
    }
}
