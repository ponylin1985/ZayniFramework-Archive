﻿namespace ZayniFramework.DataAccess.Lightweight
{
    // 20171028 Added by Pony: 因為還是有可能需要讓外界的程式知道，為什麼原因執行失敗，所以還是加上定義代碼
    // 譬如最重要的 DATA_FLAG_ERROR 情境
    /// <summary>執行結果狀態代碼常數
    /// </summary>
    public static class StatusConstant
    {
        /// <summary>資料已異動，資料時間戳記不正確
        /// </summary>
        public static readonly string DATA_FLAG_ERROR = "DATA_FLAG_MISMATCH_ERROR";

        /// <summary>查無資料異常
        /// </summary>
        public static readonly string NO_DATA_FOUND = "NO_DATA_FOUND";

        /// <summary>資料重覆
        /// </summary>
        public static readonly string DUPLICATE_DATA_ERROR = "DUPLICATE_DATA_ERROR";

        /// <summary>服務內部程式異常
        /// </summary>
        public static readonly string INTERNAL_ERROR = "INTERNAL_ERROR";
    }
}
