﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>輕量級 SQL 引擎
    /// </summary>
    public partial class SQLEngine
    {
        #region 宣告公開的資料庫連線方法

        /// <summary>建立資料庫連線
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        /// <returns>新的資料庫連線</returns>
        public async Task<DbConnection> CreateConnectionAsync( string dbName = null ) => await _dao.CreateConnectionAsync( dbName );

        #endregion 宣告公開的資料庫連線方法


        #region 宣告公開的 CRUD 方法

        #region Select Methods

        /// <summary>資料查詢 (根據指定的資料模型泛型的 ORM Metadata，進行 Full Table Scan 查詢!)
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，進行 Full Table Scan 查詢!</remarks>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<List<TModel>>> SelectAsync<TModel>( params string[] ignoreColumns )
            where TModel : new() => await SelectAsync<TModel>( default ( TModel ), ignoreColumns: ignoreColumns );

        /// <summary>資料查詢 (根據指定的資料模型泛型的 ORM Metadata，進行 Full Table Scan 查詢!)
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，進行 Full Table Scan 查詢!</remarks>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<List<TModel>>> SelectAsync<TModel>( DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : new() => await SelectAsync<TModel>( default ( TModel ), connection: connection, transaction: transaction, ignoreColumns: ignoreColumns );

        /// <summary>資料查詢<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢!<para/>
        /// 2. 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢! 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<List<TModel>>> SelectAsync<TModel>( TModel model, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( SelectAsync ) );
            var result = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT SQL 查詢語法

            var t = SqlStatementMaker.CreateSelectSql<TModel>( c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var h = SqlStatementMaker.HasPKeyConditions<TModel>( model );
            List<PrimaryKeyInfo> primaryKeysInfo = h.Data;

            if ( DoesNeedMakeWhereSql( h.Success, model ) )
            {
                var cp = SqlStatementMaker.CreatePkWhereConditions( primaryKeysInfo );

                if ( !cp.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = cp.Message;
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                string whereSql = cp.Data;

                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;

                if ( c.OrderByColumnsString.IsNullOrEmpty() )
                {
                    sql += where;
                }
                else 
                {
                    sql = sql.RemoveLastAppeared( "ORDER BY" );
                    sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                    sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 執行查詢語法

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<TModel> models;

            using ( var conn = await _dao.CreateConnectionAsync() )
            {
                var cmd = _dao.GetSqlStringCommand( sql, conn );

                if ( DoesNeedAddInPkeyParameters( h.Success, model, primaryKeysInfo, out var parameters ) )
                {
                    parameters.ForEach( p => _dao.AddInParameter( cmd, $"@{p.PrimaryKeyColumn.ColumnName}", p.DbType, p.Value ) );
                }

                var r = await _dao.LoadDataToModelAsync<TModel>( cmd );

                if ( !r.Success )
                {
                    conn.Close();
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Select fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                models = r.Data;
                conn.Close();
            }

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料查詢<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢!<para/>
        /// 2. 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢! 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<List<TModel>>> SelectAsync<TModel>( TModel model, DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( SelectAsync ) );
            var result   = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT SQL 查詢語法

            var t = SqlStatementMaker.CreateSelectSql<TModel>( c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var h = SqlStatementMaker.HasPKeyConditions<TModel>( model );
            List<PrimaryKeyInfo> primaryKeysInfo = h.Data;

            if ( DoesNeedMakeWhereSql( h.Success, model ) )
            {
                var cp = SqlStatementMaker.CreatePkWhereConditions( primaryKeysInfo );

                if ( !cp.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = cp.Message;
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                string whereSql = cp.Data;

                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;

                if ( c.OrderByColumnsString.IsNullOrEmpty() )
                {
                    sql += where;
                }
                else 
                {
                    sql = sql.RemoveLastAppeared( "ORDER BY" );
                    sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                    sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立資料庫 DbCommand 物件

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }
            
            DbCommand cmd = _dao.GetSqlStringCommand( sql, connection, transaction );

            if ( DoesNeedAddInPkeyParameters( h.Success, model, primaryKeysInfo, out var parameters ) )
            {
                parameters.ForEach( p => _dao.AddInParameter( cmd, $"@{p.PrimaryKeyColumn.ColumnName}", p.DbType, p.Value ) );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 執行查詢語法

            var r = await _dao.LoadDataToModelAsync<TModel>( cmd );

            if ( !r.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            var models = r.Data;

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion Select Methods

        #region SelectTop Methods

        /// <summary>前幾筆資料的查詢 (只根據 TOP 筆數回傳前 N 筆!)<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢!<para/>
        /// 2. 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="top">TOP 筆數: 查詢前 N 筆的資料</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<List<TModel>>> SelectTopAsync<TModel>( TModel model, int top, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( SelectTopAsync ) );
            var result   = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT TOP SQL 查詢語法

            var t = _sqlBuilder.CreateSelectTopSql<TModel>( top, c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT TOP SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var h = SqlStatementMaker.HasPKeyConditions<TModel>( model );
            List<PrimaryKeyInfo> primaryKeysInfo = h.Data;

            if ( DoesNeedMakeWhereSql( h.Success, model ) )
            {
                var cp = SqlStatementMaker.CreatePkWhereConditions( primaryKeysInfo );

                if ( !cp.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = cp.Message;
                    Logger.WriteErrorLog( this, result.Message, "SQLEngine.Select" );
                    return result;
                }

                string whereSql = cp.Data;

                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;

                if ( c.OrderByColumnsString.IsNullOrEmpty() )
                {
                    sql += where;
                }
                else 
                {
                    sql = sql.RemoveLastAppeared( "ORDER BY" );
                    sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                    sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 執行查詢語法

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<TModel> models;

            using ( var conn = await _dao.CreateConnectionAsync() )
            {
                var cmd = _dao.GetSqlStringCommand( sql, conn );

                if ( DoesNeedAddInPkeyParameters( h.Success, model, primaryKeysInfo, out var parameters ) )
                {
                    parameters.ForEach( p => _dao.AddInParameter( cmd, $"@{p.PrimaryKeyColumn.ColumnName}", p.DbType, p.Value ) );
                }

                var r = await _dao.LoadDataToModelAsync<TModel>( cmd );

                if ( !r.Success )
                {
                    conn.Close();
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute SelectTop fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                models = r.Data;
                conn.Close();
            }

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>前幾筆資料的查詢 (只根據 TOP 筆數回傳前 N 筆!)<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢!<para/>
        /// 2. 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="top">TOP 筆數: 查詢前 N 筆的資料</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<List<TModel>>> SelectTopAsync<TModel>( TModel model, int top, DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( SelectTopAsync ) );
            var result   = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT TOP SQL 查詢語法

            var t = _sqlBuilder.CreateSelectTopSql<TModel>( top, c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT TOP SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var h = SqlStatementMaker.HasPKeyConditions<TModel>( model );
            List<PrimaryKeyInfo> primaryKeysInfo = h.Data;

            if ( DoesNeedMakeWhereSql( h.Success, model ) )
            {
                var cp = SqlStatementMaker.CreatePkWhereConditions( primaryKeysInfo );

                if ( !cp.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = cp.Message;
                    Logger.WriteErrorLog( this, result.Message, "SQLEngine.Select" );
                    return result;
                }

                string whereSql = cp.Data;

                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;

                if ( c.OrderByColumnsString.IsNullOrEmpty() )
                {
                    sql += where;
                }
                else 
                {
                    sql = sql.RemoveLastAppeared( "ORDER BY" );
                    sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                    sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立資料庫 DbCommand 物件

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }
            
            DbCommand cmd = _dao.GetSqlStringCommand( sql, connection, transaction );

            if ( DoesNeedAddInPkeyParameters( h.Success, model, primaryKeysInfo, out var parameters ) )
            {
                parameters.ForEach( p => _dao.AddInParameter( cmd, $"@{p.PrimaryKeyColumn.ColumnName}", p.DbType, p.Value ) );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 執行查詢語法

            var r = await _dao.LoadDataToModelAsync<TModel>( cmd );

            if ( !r.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            var models = r.Data;

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion SelectTop Methods

        #region WhereBy Methods

        /// <summary>資料查詢，依照指定的欄位名稱當作 SQL Where 查詢條件
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">查詢條件欄位名稱集合</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<List<TModel>>> WhereByAsync<TModel>( TModel model, string[] wheres, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( WhereByAsync ) );
            var result   = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT SQL 查詢語法

            var t = SqlStatementMaker.CreateSelectSql<TModel>( c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var cw = SqlStatementMaker.CreateWhereConditions( model, wheres );

            if ( !cw.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<DbParameterEntry> sqlParameters = cw.Data;
            string whereSql = cw.SqlWhereStmt;

            if ( whereSql.IsNotNullOrEmpty() )
            {
                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;
            }

            if ( c.OrderByColumnsString.IsNullOrEmpty() )
            {
                sql += where;
            }
            else 
            {
                sql = sql.RemoveLastAppeared( "ORDER BY" );
                sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 執行查詢語法

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<TModel> models;

            using ( var conn = await _dao.CreateConnectionAsync() )
            {
                var cmd = _dao.GetSqlStringCommand( sql, conn );

                if ( sqlParameters.IsNotNullOrEmptyList() )
                {
                    sqlParameters.ForEach( p => _dao.AddInParameter( cmd, p.Name, p.DbType, p.Value ) );
                }

                var r = await _dao.LoadDataToModelAsync<TModel>( cmd );

                if ( !r.Success )
                {
                    conn.Close();
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Select fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                models = r.Data;
                conn.Close();
            }

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料查詢，依照指定的欄位名稱當作 SQL Where 查詢條件
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">查詢條件欄位名稱集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<List<TModel>>> WhereByAsync<TModel>( TModel model, string[] wheres, DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( WhereByAsync ) );
            var result   = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT SQL 查詢語法

            var t = SqlStatementMaker.CreateSelectSql<TModel>( c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var cw = SqlStatementMaker.CreateWhereConditions( model, wheres );

            if ( !cw.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<DbParameterEntry> sqlParameters = cw.Data;
            string whereSql = cw.SqlWhereStmt;

            if ( whereSql.IsNotNullOrEmpty() )
            {
                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;
            }

            if ( c.OrderByColumnsString.IsNullOrEmpty() )
            {
                sql += where;
            }
            else 
            {
                sql = sql.RemoveLastAppeared( "ORDER BY" );
                sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 執行查詢語法

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            var cmd = _dao.GetSqlStringCommand( sql, connection, transaction );

            if ( sqlParameters.IsNotNullOrEmptyList() )
            {
                sqlParameters.ForEach( p => _dao.AddInParameter( cmd, p.Name, p.DbType, p.Value ) );
            }

            var r = await _dao.LoadDataToModelAsync<TModel>( cmd );

            if ( !r.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            var models = r.Data;

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion WhereBy Methods

        #region Insert Methods

        /// <summary>資料新增<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!<para/>
        /// 2. 在資料模中有標記 TableColumnAttribute 的屬性，全部都會是 INSERT 的欄位，必須要給定值。(或給定預設值!)<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <returns>新增結果</returns>
        public async Task<Result<TModel>> InsertAsync<TModel>( TModel model )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( InsertAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db insert command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立新增的欄位字串

            var j = _sqlBuilder.CreateInsertIntoColumnsSql<TModel>( model );

            if ( !j.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {j.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立新增的欄位字串

            #region 建立 INSERT INTO 新增 SQL 語法

            var s = SqlStatementMaker.CreateInsertIntoSql<TModel>( j.ColumnsString, j.ParametersString );

            if ( !s.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {s.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 INSERT INTO 新增 SQL 語法

            #region 執行 DbCommand 資料新增

            using ( var conn  = await _dao.CreateConnectionAsync() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( s.Data, conn, trans );

                foreach ( var p in j.SqlParameters )
                {
                    DbParameterEntry parameter = p.Value;
                    _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
                }

                if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db insert command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db insert command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料新增

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料新增<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!<para/>
        /// 2. 在資料模中有標記 TableColumnAttribute 的屬性，全部都會是 INSERT 的欄位，必須要給定值。(或給定預設值!)<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>新增結果</returns>
        public async Task<Result<TModel>> InsertAsync<TModel>( TModel model, DbConnection connection, DbTransaction transaction )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( InsertAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db insert command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立新增的欄位字串

            var j = _sqlBuilder.CreateInsertIntoColumnsSql<TModel>( model );

            if ( !j.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {j.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立新增的欄位字串

            #region 建立 INSERT INTO 新增 SQL 語法

            var s = SqlStatementMaker.CreateInsertIntoSql<TModel>( j.ColumnsString, j.ParametersString );

            if ( !s.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {s.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 INSERT INTO 新增 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( s.Data, connection, transaction );

            foreach ( var p in j.SqlParameters )
            {
                DbParameterEntry parameter = p.Value;
                _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 新增語法

            if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 新增語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料新增<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!<para/>
        /// 2. 在資料模中有標記 TableColumnAttribute 的屬性，全部都會是 INSERT 的欄位，必須要給定值。(或給定預設值!)<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的table執行資料新增動作!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">忽略新增的欄位</param>
        /// <returns>新增結果</returns>
        public async Task<Result<TModel>> InsertAsync<TModel>( TModel model, params string[] ignoreColumns )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( InsertAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db insert command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立新增的欄位字串

            var j = _sqlBuilder.CreateInsertIntoColumnsSql<TModel>( model, ignoreColumns );

            if ( !j.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {j.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立新增的欄位字串

            #region 建立 INSERT INTO 新增 SQL 語法

            var s = SqlStatementMaker.CreateInsertIntoSql<TModel>( j.ColumnsString, j.ParametersString );

            if ( !s.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {s.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 INSERT INTO 新增 SQL 語法

            #region 執行 DbCommand 資料新增

            using ( var conn  = await _dao.CreateConnectionAsync() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( s.Data, conn, trans );

                foreach ( var p in j.SqlParameters )
                {
                    DbParameterEntry parameter = p.Value;
                    _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
                }

                if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db insert command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db insert command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料新增

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料新增<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!<para/>
        /// 2. 在資料模中有標記 TableColumnAttribute 的屬性，全部都會是 INSERT 的欄位，必須要給定值。(或給定預設值!)<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的table執行資料新增動作!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略新增的欄位</param>
        /// <returns>新增結果</returns>
        public async Task<Result<TModel>> InsertAsync<TModel>( TModel model, DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( InsertAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db insert command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立新增的欄位字串

            var j = _sqlBuilder.CreateInsertIntoColumnsSql<TModel>( model, ignoreColumns );

            if ( !j.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {j.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立新增的欄位字串

            #region 建立 INSERT INTO 新增 SQL 語法

            var s = SqlStatementMaker.CreateInsertIntoSql<TModel>( j.ColumnsString, j.ParametersString );

            if ( !s.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {s.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 INSERT INTO 新增 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( s.Data, connection, transaction );

            foreach ( var p in j.SqlParameters )
            {
                DbParameterEntry parameter = p.Value;
                _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 新增語法

            if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db insert command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 新增語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion Insert Methods

        #region Update Methods

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <returns>更新結果</returns>
        public async Task<Result<TModel>> UpdateAsync<TModel>( TModel model, DbParameterEntry[] wheres = null )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( UpdateAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db update command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立修改的欄位字串

            var g = SqlStatementMaker.CreateUpdateColumnsSql<TModel>( model );

            if ( !g.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {g.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立修改的欄位字串

            #region 建立 WHERE SQL 查詢條件

            string where = g.WhereConditionString;

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                var cw = SqlStatementMaker.CreateWhereConditions( wheres );

                if ( !cw.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Update fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }
                
                string whereSql = cw.SqlWhereStmt;

                if ( whereSql.IsNotNullOrEmpty() )
                {
                    where = whereSql;
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立 UPDATE 更新 SQL 語法

            var k = SqlStatementMaker.CreateUpdateSql<TModel>( g.ColumnsString, where );

            if ( !k.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {k.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 UPDATE 更新 SQL 語法

            #region 執行 DbCommand 資料更新

            using ( var conn  = await _dao.CreateConnectionAsync() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( k.Data, conn, trans );

                foreach ( var p in g.SqlParameters )
                {
                    DbParameterEntry parameter = p.Value;
                    _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
                }

                if ( wheres.IsNotNullOrEmptyArray() )
                {
                    foreach ( var parameterWhere in wheres )
                    {
                        if ( g.SqlParameters.ContainsKey( parameterWhere.Name ) )
                        {
                            continue;
                        }

                        _dao.AddInParameter( cmd, parameterWhere.Name, parameterWhere.DbType, parameterWhere.Value );
                    }
                }

                if ( await _dataChecker.CheckAsync( model, conn, trans ) ) 
                {
                    result.Code    = StatusConstant.DATA_FLAG_ERROR;
                    result.Message = $"{logTitle}, execute db update command fail. Check DataFlag from database fail. The data may has been modified.";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db update command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db update command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料更新

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }
        
        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <returns>更新結果</returns>
        public async Task<Result<TModel>> UpdateAsync<TModel>( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( UpdateAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db update command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立修改的欄位字串

            var g = SqlStatementMaker.CreateUpdateColumnsSql<TModel>( model );

            if ( !g.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {g.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立修改的欄位字串

            #region 建立 WHERE SQL 查詢條件

            string where = g.WhereConditionString;

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                var cw = SqlStatementMaker.CreateWhereConditions( wheres );

                if ( !cw.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Update fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }
                
                string whereSql = cw.SqlWhereStmt;

                if ( whereSql.IsNotNullOrEmpty() )
                {
                    where = whereSql;
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立 UPDATE 更新 SQL 語法

            var k = SqlStatementMaker.CreateUpdateSql<TModel>( g.ColumnsString, where );

            if ( !k.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {k.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 UPDATE 更新 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( k.Data, connection, transaction );

            foreach ( var p in g.SqlParameters )
            {
                DbParameterEntry parameter = p.Value;
                _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
            }

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                foreach ( var parameterWhere in wheres )
                {
                    if ( g.SqlParameters.ContainsKey( parameterWhere.Name ) )
                    {
                        continue;
                    }

                    _dao.AddInParameter( cmd, parameterWhere.Name, parameterWhere.DbType, parameterWhere.Value );
                }
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 更新語法

            if ( await _dataChecker.CheckAsync( model, connection, transaction ) ) 
            {
                result.Code    = StatusConstant.DATA_FLAG_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. Check DataFlag from database fail. The data may has been modified.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 更新語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        public async Task<Result<TModel>> UpdateAsync<TModel>( TModel model, DbParameterEntry[] wheres = null, params string[] ignoreColumns )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( UpdateAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db update command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立修改的欄位字串

            var g = SqlStatementMaker.CreateUpdateColumnsSql<TModel>( model, ignoreColumns );

            if ( !g.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {g.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立修改的欄位字串

            #region 建立 WHERE SQL 查詢條件

            string where = g.WhereConditionString;

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                var cw = SqlStatementMaker.CreateWhereConditions( wheres );

                if ( !cw.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Update fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }
                
                string whereSql = cw.SqlWhereStmt;

                if ( whereSql.IsNotNullOrEmpty() )
                {
                    where = whereSql;
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立 UPDATE 更新 SQL 語法

            var k = SqlStatementMaker.CreateUpdateSql<TModel>( g.ColumnsString, where );

            if ( !k.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {k.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 UPDATE 更新 SQL 語法

            #region 執行 DbCommand 資料更新

            using ( var conn  = await _dao.CreateConnectionAsync() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( k.Data, conn, trans );

                foreach ( var p in g.SqlParameters )
                {
                    DbParameterEntry parameter = p.Value;
                    _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
                }

                if ( wheres.IsNotNullOrEmptyArray() )
                {
                    foreach ( var parameterWhere in wheres )
                    {
                        if ( g.SqlParameters.ContainsKey( parameterWhere.Name ) )
                        {
                            continue;
                        }

                        _dao.AddInParameter( cmd, parameterWhere.Name, parameterWhere.DbType, parameterWhere.Value );
                    }
                }

                if ( await _dataChecker.CheckAsync( model, conn, trans ) ) 
                {
                    result.Code    = StatusConstant.DATA_FLAG_ERROR;
                    result.Message = $"{logTitle}, execute db update command fail. Check DataFlag from database fail. The data may has been modified.";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db update command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db update command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料更新

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        public async Task<Result<TModel>> UpdateAsync<TModel>( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null, params string[] ignoreColumns )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( UpdateAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db update command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立修改的欄位字串

            var g = SqlStatementMaker.CreateUpdateColumnsSql<TModel>( model, ignoreColumns );

            if ( !g.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {g.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立修改的欄位字串

            #region 建立 WHERE SQL 查詢條件

            string where = g.WhereConditionString;

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                var cw = SqlStatementMaker.CreateWhereConditions( wheres );

                if ( !cw.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Update fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }
                
                string whereSql = cw.SqlWhereStmt;

                if ( whereSql.IsNotNullOrEmpty() )
                {
                    where = whereSql;
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立 UPDATE 更新 SQL 語法

            var k = SqlStatementMaker.CreateUpdateSql<TModel>( g.ColumnsString, where );

            if ( !k.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {k.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 UPDATE 更新 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( k.Data, connection, transaction );

            foreach ( var p in g.SqlParameters )
            {
                DbParameterEntry parameter = p.Value;
                _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
            }

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                foreach ( var parameterWhere in wheres )
                {
                    if ( g.SqlParameters.ContainsKey( parameterWhere.Name ) )
                    {
                        continue;
                    }

                    _dao.AddInParameter( cmd, parameterWhere.Name, parameterWhere.DbType, parameterWhere.Value );
                }
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 更新語法

            if ( await _dataChecker.CheckAsync( model, connection, transaction ) ) 
            {
                result.Code    = StatusConstant.DATA_FLAG_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. Check DataFlag from database fail. The data may has been modified.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db update command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 更新語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion Update Methods

        #region Delete Methods

        /// <summary>資料刪除<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料刪除動作!<para/>
        /// 2. 若傳入的資料模型中，PKey 屬性未設定值，就對針對 ORM 對應的 table 中的資料全部刪除!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        public async Task<Result<TModel>> DeleteAsync<TModel>( TModel model, params string[] wheres )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( DeleteAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute delete command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立 DELETE 刪除 SQL 語法

            var r = SqlStatementMaker.CreateDeleteSql<TModel>( model, wheres );

            if ( !r.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute delete command fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 DELETE 刪除 SQL 語法

            #region 執行 DbCommand 資料刪除

            using ( var conn  = await _dao.CreateConnectionAsync() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( r.Sql, conn, trans );

                if ( r.Wheres.IsNotNullOrEmptyList() )
                {
                    r.Wheres.ForEach( pk => _dao.AddInParameter( cmd, pk.Name, pk.DbType, pk.Value ) );
                }

                if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db delete command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db delete command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料刪除

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料刪除<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料刪除動作!<para/>
        /// 2. 若傳入的資料模型中，PKey 屬性未設定值，就對針對 ORM 對應的 table 中的資料全部刪除!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        public async Task<Result<TModel>> DeleteAsync<TModel>( TModel model, DbConnection connection, DbTransaction transaction, params string[] wheres )
            where TModel : class
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( DeleteAsync ) );

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute delete command.";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立 DELETE 刪除 SQL 語法

            var r = SqlStatementMaker.CreateDeleteSql<TModel>( model, wheres );

            if ( !r.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute delete command fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 DELETE 刪除 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( r.Sql, connection, transaction );

            if ( r.Wheres.IsNotNullOrEmptyList() )
            {
                r.Wheres.ForEach( pk => _dao.AddInParameter( cmd, pk.Name, pk.DbType, pk.Value ) );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 刪除語法

            if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db delete command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 刪除語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料刪除，將指定的資料表中所有資料進行刪除。
        /// * TModel 資料模型泛型中的 必須要正確標記 [MappingTable( TableName = "TableName" )] 並且設置 TableName 屬性。
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <returns>刪除結果</returns>
        public async Task<IResult> DeleteAllAsync<TModel>() 
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( DeleteAllAsync ) );

            #region 建立 DELETE ALL 刪除 SQL 語法

            var r = SqlStatementMaker.CreateDeleteAllSql<TModel>();

            if ( !r.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute delete command fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 DELETE ALL 刪除 SQL 語法

            #region 執行 DbCommand 資料刪除

            using ( var conn  = await _dao.CreateConnectionAsync() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( r.Sql, conn, trans );

                if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db delete command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{logTitle}, execute db delete command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料刪除

            result.Success = true;
            return result;
        }

        /// <summary>資料刪除，將指定的資料表中所有資料進行刪除。
        /// * TModel 資料模型泛型中的 必須要正確標記 [MappingTable( TableName = "TableName" )] 並且設置 TableName 屬性。
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        public async Task<IResult> DeleteAllAsync<TModel>( DbConnection connection, DbTransaction transaction ) 
        {
            var result   = Result.Create<TModel>();
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( DeleteAllAsync ) );

            #region 建立 DELETE ALL 刪除 SQL 語法

            var r = SqlStatementMaker.CreateDeleteAllSql<TModel>();

            if ( !r.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute delete command fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 DELETE ALL 刪除 SQL 語法

            #region 準備資料庫交易，並且執行 SQL 刪除語法

            DbCommand cmd = _dao.GetSqlStringCommand( r.Sql, connection, transaction );

            if ( !await _dao.ExecuteNonQueryAsync( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{logTitle}, execute db delete command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 刪除語法

            result.Success = true;
            return result;
        }

        #endregion Delete Methods

        #endregion 宣告公開的 CRUD 方法
    }
}
