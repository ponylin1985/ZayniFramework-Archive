﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>輕量級 SQL 引擎
    /// </summary>
    public partial class SQLEngine
    {
        #region 宣告私有的欄位

        /// <summary>資料存取物件
        /// </summary>
        private Dao _dao;

        /// <summary>SQL敘述字串建立者
        /// </summary>
        private ISqlStringBuilder _sqlBuilder;

        /// <summary>資料異動檢查器
        /// </summary>
        private IDataFlagChecker _dataChecker;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        public SQLEngine( string dbName )
        {
            _dao = new Dao( dbName );
            Initialize( dbName );
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <param name="zayniConnectionString">ZayniFramework 框架的資料庫連線字串 (若傳入為Null或空字串，SQL Log功能將停用)</param>
        public SQLEngine( string dbName, string connectionString, string zayniConnectionString = null )
        {
            _dao = new Dao( dbName, connectionString, zayniConnectionString );
            Initialize( dbName );
        }

        /// <summary>解構子
        /// </summary>
        ~SQLEngine()
        {
            _dao        = null;
            _sqlBuilder = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的資料庫連線方法

        /// <summary>建立資料庫連線
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        /// <returns>新的資料庫連線</returns>
        public DbConnection CreateConnection( string dbName = null ) => _dao.CreateConnection( dbName );

        #endregion 宣告公開的資料庫連線方法


        #region 宣告公開的資料庫交易方法

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>開啟資料庫交易的結果</returns>
        public DbTransaction BeginTransaction( DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted ) => 
            _dao.BeginTransaction( connection, isolationLevel );

        /// <summary>確認資料庫交易
        /// </summary>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="message">執行結果的訊息</param>
        /// <returns>是否成功確認資料庫交易</returns>
        public bool Commit( DbTransaction transaction, out string message )
        {
            if ( !_dao.Commit( transaction ) )
            {
                message = _dao.Message;
                return false;
            }

            message = "";
            return true;
        }

        /// <summary>退回資料庫交易
        /// </summary>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="message">執行結果的訊息</param>
        /// <returns>是否成功退回資料庫交易</returns>
        public bool Rollback( DbTransaction transaction, out string message )
        {
            if ( !_dao.Rollback( transaction ) )
            {
                message = _dao.Message;
                return false;
            }

            message = "";
            return true;
        }

        #endregion 宣告公開的資料庫交易方法


        #region 宣告公開的 CRUD 方法

        #region Select Methods

        /// <summary>資料查詢 (根據指定的資料模型泛型的 ORM Metadata，進行 Full Table Scan 查詢!)
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，進行 Full Table Scan 查詢!</remarks>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public Result<List<TModel>> Select<TModel>( params string[] ignoreColumns )
            where TModel : new() => Select<TModel>( default ( TModel ), ignoreColumns: ignoreColumns );

        /// <summary>資料查詢 (根據指定的資料模型泛型的 ORM Metadata，進行 Full Table Scan 查詢!)
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，進行 Full Table Scan 查詢!</remarks>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public Result<List<TModel>> Select<TModel>( DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : new() => Select<TModel>( default ( TModel ), connection: connection, transaction: transaction, ignoreColumns: ignoreColumns );

        /// <summary>資料查詢<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢!<para/>
        /// 2. 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢! 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public Result<List<TModel>> Select<TModel>( TModel model, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( Select ) );
            var result = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT SQL 查詢語法

            var t = SqlStatementMaker.CreateSelectSql<TModel>( c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var h = SqlStatementMaker.HasPKeyConditions<TModel>( model );
            List<PrimaryKeyInfo> primaryKeysInfo = h.Data;

            if ( DoesNeedMakeWhereSql( h.Success, model ) )
            {
                var cp = SqlStatementMaker.CreatePkWhereConditions( primaryKeysInfo );

                if ( !cp.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = cp.Message;
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                string whereSql = cp.Data;

                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;

                if ( c.OrderByColumnsString.IsNullOrEmpty() )
                {
                    sql += where;
                }
                else 
                {
                    sql = sql.RemoveLastAppeared( "ORDER BY" );
                    sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                    sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 執行查詢語法

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<TModel> models;

            using ( var conn = _dao.CreateConnection() )
            {
                var cmd = _dao.GetSqlStringCommand( sql, conn );

                if ( DoesNeedAddInPkeyParameters( h.Success, model, primaryKeysInfo, out var parameters ) )
                {
                    parameters.ForEach( p => _dao.AddInParameter( cmd, $"@{p.PrimaryKeyColumn.ColumnName}", p.DbType, p.Value ) );
                }

                if ( !_dao.LoadDataToModel<TModel>( cmd, out models ) )
                {
                    conn.Close();
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Select fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                conn.Close();
            }

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料查詢<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢!<para/>
        /// 2. 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢! 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public Result<List<TModel>> Select<TModel>( TModel model, DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( Select ) );
            var result = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT SQL 查詢語法

            var t = SqlStatementMaker.CreateSelectSql<TModel>( c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var h = SqlStatementMaker.HasPKeyConditions<TModel>( model );
            List<PrimaryKeyInfo> primaryKeysInfo = h.Data;

            if ( DoesNeedMakeWhereSql( h.Success, model ) )
            {
                var cp = SqlStatementMaker.CreatePkWhereConditions( primaryKeysInfo );

                if ( !cp.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = cp.Message;
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                string whereSql = cp.Data;

                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;

                if ( c.OrderByColumnsString.IsNullOrEmpty() )
                {
                    sql += where;
                }
                else 
                {
                    sql = sql.RemoveLastAppeared( "ORDER BY" );
                    sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                    sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立資料庫 DbCommand 物件

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }
            
            DbCommand cmd = _dao.GetSqlStringCommand( sql, connection, transaction );

            if ( DoesNeedAddInPkeyParameters( h.Success, model, primaryKeysInfo, out var parameters ) )
            {
                parameters.ForEach( p => _dao.AddInParameter( cmd, $"@{p.PrimaryKeyColumn.ColumnName}", p.DbType, p.Value ) );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 執行查詢語法

            if ( !_dao.LoadDataToModel<TModel>( cmd, out List<TModel> models ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion Select Methods

        #region SelectTop Methods

        /// <summary>前幾筆資料的查詢 (只根據 TOP 筆數回傳前 N 筆!)<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢!<para/>
        /// 2. 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="top">TOP 筆數: 查詢前 N 筆的資料</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public Result<List<TModel>> SelectTop<TModel>( TModel model, int top, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( SelectTop ) );
            var result = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT TOP SQL 查詢語法

            var t = _sqlBuilder.CreateSelectTopSql<TModel>( top, c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT TOP SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var h = SqlStatementMaker.HasPKeyConditions<TModel>( model );
            List<PrimaryKeyInfo> primaryKeysInfo = h.Data;

            if ( DoesNeedMakeWhereSql( h.Success, model ) )
            {
                var cp = SqlStatementMaker.CreatePkWhereConditions( primaryKeysInfo );

                if ( !cp.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = cp.Message;
                    Logger.WriteErrorLog( this, result.Message, "SQLEngine.Select" );
                    return result;
                }

                string whereSql = cp.Data;

                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;

                if ( c.OrderByColumnsString.IsNullOrEmpty() )
                {
                    sql += where;
                }
                else 
                {
                    sql = sql.RemoveLastAppeared( "ORDER BY" );
                    sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                    sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 執行查詢語法

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<TModel> models;

            using ( var conn = _dao.CreateConnection() )
            {
                var cmd = _dao.GetSqlStringCommand( sql, conn );

                if ( DoesNeedAddInPkeyParameters( h.Success, model, primaryKeysInfo, out var parameters ) )
                {
                    parameters.ForEach( p => _dao.AddInParameter( cmd, $"@{p.PrimaryKeyColumn.ColumnName}", p.DbType, p.Value ) );
                }

                if ( !_dao.LoadDataToModel<TModel>( cmd, out models ) )
                {
                    conn.Close();
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute SelectTop fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                conn.Close();
            }

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>前幾筆資料的查詢 (只根據 TOP 筆數回傳前 N 筆!)<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的 PKey 屬性值，進行查詢!<para/>
        /// 2. 如果資料模型傳入為 Null 值，即為 Full Table Scan 查詢!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="top">TOP 筆數: 查詢前 N 筆的資料</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public Result<List<TModel>> SelectTop<TModel>( TModel model, int top, DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( SelectTop ) );
            var result = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT TOP SQL 查詢語法

            var t = _sqlBuilder.CreateSelectTopSql<TModel>( top, c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT TOP SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var h = SqlStatementMaker.HasPKeyConditions<TModel>( model );
            List<PrimaryKeyInfo> primaryKeysInfo = h.Data;

            if ( DoesNeedMakeWhereSql( h.Success, model ) )
            {
                var cp = SqlStatementMaker.CreatePkWhereConditions( primaryKeysInfo );

                if ( !cp.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = cp.Message;
                    Logger.WriteErrorLog( this, result.Message, "SQLEngine.Select" );
                    return result;
                }

                string whereSql = cp.Data;

                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;

                if ( c.OrderByColumnsString.IsNullOrEmpty() )
                {
                    sql += where;
                }
                else 
                {
                    sql = sql.RemoveLastAppeared( "ORDER BY" );
                    sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                    sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立資料庫 DbCommand 物件

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }
            
            DbCommand cmd = _dao.GetSqlStringCommand( sql, connection, transaction );

            if ( DoesNeedAddInPkeyParameters( h.Success, model, primaryKeysInfo, out var parameters ) )
            {
                parameters.ForEach( p => _dao.AddInParameter( cmd, $"@{p.PrimaryKeyColumn.ColumnName}", p.DbType, p.Value ) );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 執行查詢語法

            if ( !_dao.LoadDataToModel<TModel>( cmd, out var models ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute SelectTop fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion SelectTop Methods

        #region WhereBy Methods

        /// <summary>資料查詢，依照指定的欄位名稱當作 SQL Where 查詢條件
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">查詢條件欄位名稱集合</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public Result<List<TModel>> WhereBy<TModel>( TModel model, string[] wheres, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( WhereBy ) );
            var result = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT SQL 查詢語法

            var t = SqlStatementMaker.CreateSelectSql<TModel>( c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var cw = SqlStatementMaker.CreateWhereConditions( model, wheres );

            if ( !cw.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<DbParameterEntry> sqlParameters = cw.Data;
            string whereSql = cw.SqlWhereStmt;

            if ( whereSql.IsNotNullOrEmpty() )
            {
                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;
            }

            if ( c.OrderByColumnsString.IsNullOrEmpty() )
            {
                sql += where;
            }
            else 
            {
                sql = sql.RemoveLastAppeared( "ORDER BY" );
                sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 執行查詢語法

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<TModel> models;

            using ( var conn = _dao.CreateConnection() )
            {
                var cmd = _dao.GetSqlStringCommand( sql, conn );

                if ( sqlParameters.IsNotNullOrEmptyList() )
                {
                    sqlParameters.ForEach( p => _dao.AddInParameter( cmd, p.Name, p.DbType, p.Value ) );
                }

                if ( !_dao.LoadDataToModel<TModel>( cmd, out models ) )
                {
                    conn.Close();
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Select fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, logTitle );
                    return result;
                }

                conn.Close();
            }

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料查詢，依照指定的欄位名稱當作 SQL Where 查詢條件
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">查詢條件欄位名稱集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略查詢的欄位名稱</param>
        /// <returns>查詢結果</returns>
        public Result<List<TModel>> WhereBy<TModel>( TModel model, string[] wheres, DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : new()
        {
            var logTitle = Logger.GetTraceLogTitle( this, nameof ( WhereBy ) );
            var result = Result.Create<List<TModel>>();

            #region 建立查詢欄位 SQL 語法

            var c = SqlStatementMaker.CreateColumnSql<TModel>( ignoreColumns );

            if ( !c.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select column fail. {typeof ( TModel ).FullName}{Environment.NewLine}{c.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立查詢欄位 SQL 語法

            #region 建立 SELECT SQL 查詢語法

            var t = SqlStatementMaker.CreateSelectSql<TModel>( c.ColumnsString, c.OrderByColumnsString );

            if ( !t.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create select sql stmt fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{t.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 建立 SELECT SQL 查詢語法

            #region 建立 WHERE SQL 查詢條件

            string sql   = t.Data;
            string where = "";

            var cw = SqlStatementMaker.CreateWhereConditions( model, wheres );

            if ( !cw.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            List<DbParameterEntry> sqlParameters = cw.Data;
            string whereSql = cw.SqlWhereStmt;

            if ( whereSql.IsNotNullOrEmpty() )
            {
                where = SqlStatementMaker.CreateWhereSql( whereSql ).Data;
            }

            if ( c.OrderByColumnsString.IsNullOrEmpty() )
            {
                sql += where;
            }
            else 
            {
                sql = sql.RemoveLastAppeared( "ORDER BY" );
                sql = sql.RemoveLastAppeared( c.OrderByColumnsString );
                sql = sql + " {0} ORDER BY {1} ".FormatTo( where, c.OrderByColumnsString );
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 執行查詢語法

            if ( sql.IsNullOrEmpty() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Create select SQL stmt fail. The SQL stmt is null or empty string. Target model: {typeof ( TModel ).FullName}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            var cmd = _dao.GetSqlStringCommand( sql, connection, transaction );

            if ( sqlParameters.IsNotNullOrEmptyList() )
            {
                sqlParameters.ForEach( p => _dao.AddInParameter( cmd, p.Name, p.DbType, p.Value ) );
            }

            if ( !_dao.LoadDataToModel<TModel>( cmd, out var models ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"SQLEngine execute Select fail. Target model: {typeof ( TModel ).FullName}{Environment.NewLine}{_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, logTitle );
                return result;
            }

            #endregion 執行查詢語法

            #region 設定回傳值屬性

            result.Data    = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion WhereBy Methods

        #region Insert Methods

        /// <summary>資料新增<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!<para/>
        /// 2. 在資料模中有標記 TableColumnAttribute 的屬性，全部都會是 INSERT 的欄位，必須要給定值。(或給定預設值!)<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <returns>新增結果</returns>
        public Result<TModel> Insert<TModel>( TModel model )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db insert command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立新增的欄位字串

            var j = _sqlBuilder.CreateInsertIntoColumnsSql<TModel>( model );

            if ( !j.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {j.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 建立新增的欄位字串

            #region 建立 INSERT INTO 新增 SQL 語法

            var s = SqlStatementMaker.CreateInsertIntoSql<TModel>( j.ColumnsString, j.ParametersString );

            if ( !s.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {s.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 建立 INSERT INTO 新增 SQL 語法

            #region 執行 DbCommand 資料新增

            using ( var conn  = _dao.CreateConnection() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( s.Data, conn, trans );

                foreach ( var p in j.SqlParameters )
                {
                    DbParameterEntry parameter = p.Value;
                    _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
                }

                if ( !_dao.ExecuteNonQuery( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料新增

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料新增<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!<para/>
        /// 2. 在資料模中有標記 TableColumnAttribute 的屬性，全部都會是 INSERT 的欄位，必須要給定值。(或給定預設值!)<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>新增結果</returns>
        public Result<TModel> Insert<TModel>( TModel model, DbConnection connection, DbTransaction transaction )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db insert command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立新增的欄位字串

            var j = _sqlBuilder.CreateInsertIntoColumnsSql<TModel>( model );

            if ( !j.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {j.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 建立新增的欄位字串

            #region 建立 INSERT INTO 新增 SQL 語法

            var s = SqlStatementMaker.CreateInsertIntoSql<TModel>( j.ColumnsString, j.ParametersString );

            if ( !s.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {s.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 建立 INSERT INTO 新增 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( s.Data, connection, transaction );

            foreach ( var p in j.SqlParameters )
            {
                DbParameterEntry parameter = p.Value;
                _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 新增語法

            if ( !_dao.ExecuteNonQuery( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 新增語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料新增<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!<para/>
        /// 2. 在資料模中有標記 TableColumnAttribute 的屬性，全部都會是 INSERT 的欄位，必須要給定值。(或給定預設值!)<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的table執行資料新增動作!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">忽略新增的欄位</param>
        /// <returns>新增結果</returns>
        public Result<TModel> Insert<TModel>( TModel model, params string[] ignoreColumns )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db insert command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立新增的欄位字串

            var j = _sqlBuilder.CreateInsertIntoColumnsSql<TModel>( model, ignoreColumns );

            if ( !j.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {j.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 建立新增的欄位字串

            #region 建立 INSERT INTO 新增 SQL 語法

            var s = SqlStatementMaker.CreateInsertIntoSql<TModel>( j.ColumnsString, j.ParametersString );

            if ( !s.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {s.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 建立 INSERT INTO 新增 SQL 語法

            #region 執行 DbCommand 資料新增

            using ( var conn  = _dao.CreateConnection() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( s.Data, conn, trans );

                foreach ( var p in j.SqlParameters )
                {
                    DbParameterEntry parameter = p.Value;
                    _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
                }

                if ( !_dao.ExecuteNonQuery( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料新增

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料新增<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的 table 執行資料新增動作!<para/>
        /// 2. 在資料模中有標記 TableColumnAttribute 的屬性，全部都會是 INSERT 的欄位，必須要給定值。(或給定預設值!)<para/>
        /// </summary>
        /// <remarks>根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，對 ORM 對應資料庫的table執行資料新增動作!</remarks>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略新增的欄位</param>
        /// <returns>新增結果</returns>
        public Result<TModel> Insert<TModel>( TModel model, DbConnection connection, DbTransaction transaction, params string[] ignoreColumns )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db insert command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立新增的欄位字串

            var j = _sqlBuilder.CreateInsertIntoColumnsSql<TModel>( model, ignoreColumns );

            if ( !j.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {j.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 建立新增的欄位字串

            #region 建立 INSERT INTO 新增 SQL 語法

            var s = SqlStatementMaker.CreateInsertIntoSql<TModel>( j.ColumnsString, j.ParametersString );

            if ( !s.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {s.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 建立 INSERT INTO 新增 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( s.Data, connection, transaction );

            foreach ( var p in j.SqlParameters )
            {
                DbParameterEntry parameter = p.Value;
                _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 新增語法

            if ( !_dao.ExecuteNonQuery( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Insert ) )}, execute db insert command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Insert ) ) );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 新增語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion Insert Methods

        #region Update Methods

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <returns>更新結果</returns>
        public Result<TModel> Update<TModel>( TModel model, DbParameterEntry[] wheres = null )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db update command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立修改的欄位字串

            var g = SqlStatementMaker.CreateUpdateColumnsSql<TModel>( model );

            if ( !g.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {g.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 建立修改的欄位字串

            #region 建立 WHERE SQL 查詢條件

            string where = g.WhereConditionString;

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                var cw = SqlStatementMaker.CreateWhereConditions( wheres );

                if ( !cw.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Update fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    return result;
                }
                
                string whereSql = cw.SqlWhereStmt;

                if ( whereSql.IsNotNullOrEmpty() )
                {
                    where = whereSql;
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立 UPDATE 更新 SQL 語法

            var k = SqlStatementMaker.CreateUpdateSql<TModel>( g.ColumnsString, where );

            if ( !k.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {k.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 建立 UPDATE 更新 SQL 語法

            #region 執行 DbCommand 資料更新

            using ( var conn  = _dao.CreateConnection() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( k.Data, conn, trans );

                foreach ( var p in g.SqlParameters )
                {
                    DbParameterEntry parameter = p.Value;
                    _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
                }

                if ( wheres.IsNotNullOrEmptyArray() )
                {
                    foreach ( var parameterWhere in wheres )
                    {
                        if ( g.SqlParameters.ContainsKey( parameterWhere.Name ) )
                        {
                            continue;
                        }

                        _dao.AddInParameter( cmd, parameterWhere.Name, parameterWhere.DbType, parameterWhere.Value );
                    }
                }

                if ( _dataChecker.Check( model, conn, trans ) ) 
                {
                    result.Code    = StatusConstant.DATA_FLAG_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. Check DataFlag from database fail. The data may has been modified.";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.ExecuteNonQuery( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料更新

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }
        
        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <returns>更新結果</returns>
        public Result<TModel> Update<TModel>( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db update command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立修改的欄位字串

            var g = SqlStatementMaker.CreateUpdateColumnsSql<TModel>( model );

            if ( !g.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {g.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 建立修改的欄位字串

            #region 建立 WHERE SQL 查詢條件

            string where = g.WhereConditionString;

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                var cw = SqlStatementMaker.CreateWhereConditions( wheres );

                if ( !cw.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Update fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    return result;
                }
                
                string whereSql = cw.SqlWhereStmt;

                if ( whereSql.IsNotNullOrEmpty() )
                {
                    where = whereSql;
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立 UPDATE 更新 SQL 語法

            var k = SqlStatementMaker.CreateUpdateSql<TModel>( g.ColumnsString, where );

            if ( !k.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {k.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 建立 UPDATE 更新 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( k.Data, connection, transaction );

            foreach ( var p in g.SqlParameters )
            {
                DbParameterEntry parameter = p.Value;
                _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
            }

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                foreach ( var parameterWhere in wheres )
                {
                    if ( g.SqlParameters.ContainsKey( parameterWhere.Name ) )
                    {
                        continue;
                    }

                    _dao.AddInParameter( cmd, parameterWhere.Name, parameterWhere.DbType, parameterWhere.Value );
                }
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 更新語法

            if ( _dataChecker.Check( model, connection, transaction ) ) 
            {
                result.Code    = StatusConstant.DATA_FLAG_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. Check DataFlag from database fail. The data may has been modified.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            if ( !_dao.ExecuteNonQuery( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 更新語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        public Result<TModel> Update<TModel>( TModel model, DbParameterEntry[] wheres = null, params string[] ignoreColumns )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db update command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立修改的欄位字串

            var g = SqlStatementMaker.CreateUpdateColumnsSql<TModel>( model, ignoreColumns );

            if ( !g.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {g.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 建立修改的欄位字串

            #region 建立 WHERE SQL 查詢條件

            string where = g.WhereConditionString;

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                var cw = SqlStatementMaker.CreateWhereConditions( wheres );

                if ( !cw.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Update fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    return result;
                }
                
                string whereSql = cw.SqlWhereStmt;

                if ( whereSql.IsNotNullOrEmpty() )
                {
                    where = whereSql;
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立 UPDATE 更新 SQL 語法

            var k = SqlStatementMaker.CreateUpdateSql<TModel>( g.ColumnsString, where );

            if ( !k.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {k.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 建立 UPDATE 更新 SQL 語法

            #region 執行 DbCommand 資料更新

            using ( var conn  = _dao.CreateConnection() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( k.Data, conn, trans );

                foreach ( var p in g.SqlParameters )
                {
                    DbParameterEntry parameter = p.Value;
                    _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
                }

                if ( wheres.IsNotNullOrEmptyArray() )
                {
                    foreach ( var parameterWhere in wheres )
                    {
                        if ( g.SqlParameters.ContainsKey( parameterWhere.Name ) )
                        {
                            continue;
                        }

                        _dao.AddInParameter( cmd, parameterWhere.Name, parameterWhere.DbType, parameterWhere.Value );
                    }
                }

                if ( _dataChecker.Check( model, conn, trans ) ) 
                {
                    result.Code    = StatusConstant.DATA_FLAG_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. Check DataFlag from database fail. The data may has been modified.";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.ExecuteNonQuery( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料更新

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        public Result<TModel> Update<TModel>( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null, params string[] ignoreColumns )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute db update command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立修改的欄位字串

            var g = SqlStatementMaker.CreateUpdateColumnsSql<TModel>( model, ignoreColumns );

            if ( !g.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {g.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 建立修改的欄位字串

            #region 建立 WHERE SQL 查詢條件

            string where = g.WhereConditionString;

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                var cw = SqlStatementMaker.CreateWhereConditions( wheres );

                if ( !cw.Success )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"SQLEngine execute Update fail due to create where condition fail. {typeof ( TModel ).FullName}{Environment.NewLine}{cw.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                    return result;
                }
                
                string whereSql = cw.SqlWhereStmt;

                if ( whereSql.IsNotNullOrEmpty() )
                {
                    where = whereSql;
                }
            }

            #endregion 建立 WHERE SQL 查詢條件

            #region 建立 UPDATE 更新 SQL 語法

            var k = SqlStatementMaker.CreateUpdateSql<TModel>( g.ColumnsString, where );

            if ( !k.Success )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {k.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 建立 UPDATE 更新 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( k.Data, connection, transaction );

            foreach ( var p in g.SqlParameters )
            {
                DbParameterEntry parameter = p.Value;
                _dao.AddInParameter( cmd, parameter.Name, parameter.DbType, parameter.Value );
            }

            if ( wheres.IsNotNullOrEmptyArray() )
            {
                foreach ( var parameterWhere in wheres )
                {
                    if ( g.SqlParameters.ContainsKey( parameterWhere.Name ) )
                    {
                        continue;
                    }

                    _dao.AddInParameter( cmd, parameterWhere.Name, parameterWhere.DbType, parameterWhere.Value );
                }
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 更新語法

            if ( _dataChecker.Check( model, connection, transaction ) ) 
            {
                result.Code    = StatusConstant.DATA_FLAG_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. Check DataFlag from database fail. The data may has been modified.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            if ( !_dao.ExecuteNonQuery( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Update ) )}, execute db update command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Update ) ) );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 更新語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        #endregion Update Methods

        #region Delete Methods

        /// <summary>資料刪除<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料刪除動作!<para/>
        /// 2. 若傳入的資料模型中，PKey 屬性未設定值，就對針對 ORM 對應的 table 中的資料全部刪除!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        public Result<TModel> Delete<TModel>( TModel model, params string[] wheres )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute delete command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立 DELETE 刪除 SQL 語法

            var r = SqlStatementMaker.CreateDeleteSql<TModel>( model, wheres );

            if ( !r.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute delete command fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                return result;
            }

            #endregion 建立 DELETE 刪除 SQL 語法

            #region 執行 DbCommand 資料刪除

            using ( var conn  = _dao.CreateConnection() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( r.Sql, conn, trans );

                if ( r.Wheres.IsNotNullOrEmptyList() )
                {
                    r.Wheres.ForEach( pk => _dao.AddInParameter( cmd, pk.Name, pk.DbType, pk.Value ) );
                }

                if ( !_dao.ExecuteNonQuery( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute db delete command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute db delete command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料刪除

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料刪除<para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料刪除動作!<para/>
        /// 2. 若傳入的資料模型中，PKey 屬性未設定值，就對針對 ORM 對應的 table 中的資料全部刪除!<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        public Result<TModel> Delete<TModel>( TModel model, DbConnection connection, DbTransaction transaction, params string[] wheres )
            where TModel : class
        {
            var result = Result.Create<TModel>();

            #region 檢查傳入的資料模型

            if ( model.IsNull() )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, arg '{nameof ( model )}' is null. {nameof (SQLEngine)} can not execute delete command.";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                return result;
            }

            #endregion 檢查傳入的資料模型

            #region 建立 DELETE 刪除 SQL 語法

            var r = SqlStatementMaker.CreateDeleteSql<TModel>( model, wheres );

            if ( !r.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute delete command fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                return result;
            }

            #endregion 建立 DELETE 刪除 SQL 語法

            #region 建立資料庫 DbCommand 物件

            DbCommand cmd = _dao.GetSqlStringCommand( r.Sql, connection, transaction );

            if ( r.Wheres.IsNotNullOrEmptyList() )
            {
                r.Wheres.ForEach( pk => _dao.AddInParameter( cmd, pk.Name, pk.DbType, pk.Value ) );
            }

            #endregion 建立資料庫 DbCommand 物件

            #region 準備資料庫交易，並且執行 SQL 刪除語法

            if ( !_dao.ExecuteNonQuery( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute db delete command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 刪除語法

            #region 設定回傳值屬性

            result.Data    = model;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>資料刪除，將指定的資料表中所有資料進行刪除。
        /// * TModel 資料模型泛型中的 必須要正確標記 [MappingTable( TableName = "TableName" )] 並且設置 TableName 屬性。
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <returns>刪除結果</returns>
        public IResult DeleteAll<TModel>() 
        {
            var result = Result.Create<TModel>();

            #region 建立 DELETE ALL 刪除 SQL 語法

            var r = SqlStatementMaker.CreateDeleteAllSql<TModel>();

            if ( !r.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute delete command fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                return result;
            }

            #endregion 建立 DELETE ALL 刪除 SQL 語法

            #region 執行 DbCommand 資料刪除

            using ( var conn  = _dao.CreateConnection() )
            using ( var trans = _dao.BeginTransaction( conn ) )
            {
                var cmd = _dao.GetSqlStringCommand( r.Sql, conn, trans );

                if ( !_dao.ExecuteNonQuery( cmd ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute db delete command fail. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                    return result;
                }

                if ( !_dao.Commit( trans ) )
                {
                    result.Code    = StatusConstant.INTERNAL_ERROR;
                    result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute db delete command fail due to commit db transaction occur error. {_dao.Message}";
                    Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                    _dao.Rollback( trans );
                    conn.Close();
                    return result;
                }

                conn.Close();
            }

            #endregion 執行 DbCommand 資料刪除

            result.Success = true;
            return result;
        }

        /// <summary>資料刪除，將指定的資料表中所有資料進行刪除。
        /// * TModel 資料模型泛型中的 必須要正確標記 [MappingTable( TableName = "TableName" )] 並且設置 TableName 屬性。
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        public IResult DeleteAll<TModel>( DbConnection connection, DbTransaction transaction ) 
        {
            var result = Result.Create<TModel>();

            #region 建立 DELETE ALL 刪除 SQL 語法

            var r = SqlStatementMaker.CreateDeleteAllSql<TModel>();

            if ( !r.IsSuccess )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute delete command fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                return result;
            }

            #endregion 建立 DELETE ALL 刪除 SQL 語法

            #region 準備資料庫交易，並且執行 SQL 刪除語法

            DbCommand cmd = _dao.GetSqlStringCommand( r.Sql, connection, transaction );

            if ( !_dao.ExecuteNonQuery( cmd ) )
            {
                result.Code    = StatusConstant.INTERNAL_ERROR;
                result.Message = $"{GetLogTraceName( nameof ( Delete ) )}, execute db delete command fail. {_dao.Message}";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Delete ) ) );
                return result;
            }

            #endregion 準備資料庫交易，並且執行 SQL 刪除語法

            result.Success = true;
            return result;
        }

        #endregion Delete Methods

        #endregion 宣告公開的 CRUD 方法


        #region 宣告私有的方法

        /// <summary>初始化
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        private void Initialize( string dbName )
        {
            var r = StrategyFactory.Create( dbName, _dao );

            if ( !r.Success )
            {
                Logger.WriteErrorLog( this, r.Message, "SQLEngine.Initialize" );
                return;
            }

            _sqlBuilder  = r.SqlBuilder;
            _dataChecker = r.DataChecker;
        }

        /// <summary>檢查是否真的需要建立查詢的 Where 敘述
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="hasPk">資料模型泛型中，是否有設定PKey的屬性</param>
        /// <param name="model">資料模型</param>
        /// <returns>是否真的需要建立查詢的Where敘述</returns>
        private bool DoesNeedMakeWhereSql<TModel>( bool hasPk, TModel model ) => hasPk && model.IsNotNull();

        /// <summary>檢查是否真的需要加入 Pkey 的參數值到 DbCommand 中
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="hasPk">資料模型泛型中，是否有設定 PKey 的屬性</param>
        /// <param name="sqlOperator">SQ L運算子</param>
        /// <param name="model">資料模型</param>
        /// <returns>是否真的需要加入 Pkey 的參數值到 DbCommand 中</returns>
        private bool DoesNeedAddInPkeyParameter<TModel>( bool hasPk, string sqlOperator, TModel model ) => 
            hasPk && "LIKE" != sqlOperator?.ToUpper() && model.IsNotNull();

        /// <summary>檢查是否需要加入 PrimaryKey 主索引鍵的參數值，到 DbCommand 指令中。<para/>
        /// 1. 假若 hasPk 參數為 false，則直接回傳 false。<para/>
        /// 2. 假若 model 參數為 Null，則直接回傳 false。<para/>
        /// 3. 假若 primaryKeysInfo 參數中所有 SqlOperator 設定為 LIKE，則直接回傳 false。<para/>
        /// 4. 只有在 TModel 型別確實有指定 IsPrimaryKey = true，並且 SqlOperator 設定為 =，並且有傳入 model 與主索引鍵的參數值時，財會回傳 true。
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="hasPk">資料模型泛型中，是否有設定 PKey 的屬性</param>
        /// <param name="model">資料模型</param>
        /// <param name="primaryKeysInfo">資料表的主索引鍵資訊集合</param>
        /// <param name="pKeyParameters">主索引鍵參數集合</param>
        /// <returns>是否需要加入 PrimaryKey 主索引鍵的參數值，到 DbCommand 指令中</returns>
        private bool DoesNeedAddInPkeyParameters<TModel>( bool hasPk, TModel model, List<PrimaryKeyInfo> primaryKeysInfo, out List<PrimaryKeyInfo> pKeyParameters )
        {
            pKeyParameters = null;

            if ( !hasPk )
            {
                return false;
            }

            if ( model.IsNull() )
            {
                return false;
            }

            if ( primaryKeysInfo.IsNullOrEmptyList() )
            {
                return false;
            }

            if ( primaryKeysInfo.All( k => k.PrimaryKeyColumn.SqlOperator == "LIKE" ) )
            {
                return false;
            }

            pKeyParameters = primaryKeysInfo.Where( pk => 
                pk.PrimaryKeyColumn.IsPrimaryKey && 
                pk.PrimaryKeyColumn.SqlOperator == "=" &&
                pk.Value.IsNotNull() )?.ToList();

            if ( pKeyParameters.IsNullOrEmptyList() )
            {
                return false;
            }

            return true;
        }
        
        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( SQLEngine )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
