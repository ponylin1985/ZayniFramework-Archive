﻿using System.Collections.Generic;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>查詢 ORM 對應資訊介面
    /// </summary>
    /// <typeparam name="TModel">資料模型泛型</typeparam>
    public interface ISelectInfo<TModel> : IOrmInfo<TModel>
        where TModel : new()
    {
        /// <summary>TOP幾筆資料
        /// </summary>
        int Top { get; set; }

        /// <summary>要忽略查詢的資料欄位集合 (資料表欄位名稱集合)
        /// </summary>
        List<string> IgnoreColumns { get; set; }
    }
}
