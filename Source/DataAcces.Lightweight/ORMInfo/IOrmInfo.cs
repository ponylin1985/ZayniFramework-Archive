﻿namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>ORM對應資訊基底介面
    /// </summary>
    /// <typeparam name="TModel">資料模型泛型</typeparam>
    public interface IOrmInfo<TModel>
        where TModel : new()
    {
        /// <summary>資料內容集合
        /// </summary>
        TModel DataContent { get; set; }
    }
}
