﻿using System.Collections.Generic;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>查詢 ORM 對應資訊
    /// </summary>
    /// <typeparam name="TModel">資料模型泛型</typeparam>
    public class SelectInfo<TModel> : ISelectInfo<TModel>
        where TModel : new()
    {
        #region 宣告建構子

        /// <summary>解構子
        /// </summary>
        ~SelectInfo()
        {
            IgnoreColumns = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>資料內容集合
        /// </summary>
        public TModel DataContent { get; set; }

        /// <summary>TOP幾筆資料 (預設值為: -1)
        /// </summary>
        public int Top { get; set; } = -1;

        /// <summary>要忽略查詢的資料欄位集合 (資料表欄位名稱集合)
        /// </summary>
        public List<string> IgnoreColumns { get; set; } = new List<string>();

        #endregion 宣告公開的屬性
    }
}
