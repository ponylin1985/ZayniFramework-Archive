﻿using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>ORM DataContext 資料來源上下文介面
    /// </summary>
    /// <typeparam name="TQueryReqArgs">查詢資料模型泛型</typeparam>
    /// <typeparam name="TModel">(新增、刪除、修改)資料模型泛型</typeparam>
    public interface IDataContext<TQueryReqArgs, TModel>
        where TQueryReqArgs : new()
        where TModel        : class
    {
        /// <summary>資料查詢
        /// </summary>
        /// <param name="selectInfo">查詢ORM對應資訊</param>
        /// <returns>查詢結果集合</returns>
        Task<Result<List<TQueryReqArgs>>> SelectAsync( ISelectInfo<TQueryReqArgs> selectInfo );

        /// <summary>資料查詢
        /// </summary>
        /// <param name="selectInfo">查詢ORM對應資訊</param>
        /// <returns>查詢結果集合</returns>
        Result<List<TQueryReqArgs>> Select( ISelectInfo<TQueryReqArgs> selectInfo );

        /// <summary>資料查詢
        /// </summary>
        /// <param name="selectInfo">查詢ORM對應資訊</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>查詢結果集合</returns>
        Task<Result<List<TQueryReqArgs>>> SelectAsync( ISelectInfo<TQueryReqArgs> selectInfo, DbConnection connection, DbTransaction transaction );

        /// <summary>資料查詢
        /// </summary>
        /// <param name="selectInfo">查詢ORM對應資訊</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>查詢結果集合</returns>
        Result<List<TQueryReqArgs>> Select( ISelectInfo<TQueryReqArgs> selectInfo, DbConnection connection, DbTransaction transaction );

        /// <summary>資料新增
        /// </summary>
        /// <param name="model">新增資料模型</param>
        /// <returns>新增結果</returns>
        Task<Result<TModel>> InsertAsync( TModel model );

        /// <summary>資料新增
        /// </summary>
        /// <param name="model">新增資料模型</param>
        /// <returns>新增結果</returns>
        Result<TModel> Insert( TModel model );

        /// <summary>資料新增
        /// </summary>
        /// <param name="model">新增資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>新增結果</returns>
        Task<Result<TModel>> InsertAsync( TModel model, DbConnection connection, DbTransaction transaction );

        /// <summary>資料新增
        /// </summary>
        /// <param name="model">新增資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>新增結果</returns>
        Result<TModel> Insert( TModel model, DbConnection connection, DbTransaction transaction );

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <returns>更新結果</returns>
        Task<Result<TModel>> UpdateAsync( TModel model, DbParameterEntry[] wheres = null );

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <returns>更新結果</returns>
        Result<TModel> Update( TModel model, DbParameterEntry[] wheres = null );

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>更新結果</returns>
        Task<Result<TModel>> UpdateAsync( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null );

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>更新結果</returns>
        Result<TModel> Update( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null );

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        Task<Result<TModel>> UpdateAsync( TModel model, DbParameterEntry[] wheres = null, params string[] ignoreColumns );

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        Result<TModel> Update( TModel model, DbParameterEntry[] wheres = null, params string[] ignoreColumns );

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        Task<Result<TModel>> UpdateAsync( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null, params string[] ignoreColumns );

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        Result<TModel> Update( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null, params string[] ignoreColumns );

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <returns>刪除結果</returns>
        Task<Result<TModel>> DeleteAsync( TModel model );

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <returns>刪除結果</returns>
        Result<TModel> Delete( TModel model );

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        Task<Result<TModel>> DeleteAsync( TModel model, DbConnection connection, DbTransaction transaction );

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        Result<TModel> Delete( TModel model, DbConnection connection, DbTransaction transaction );

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        Task<Result<TModel>> DeleteAsync( TModel model, params string[] wheres );

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        Result<TModel> Delete( TModel model, params string[] wheres );

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        Task<Result<TModel>> DeleteAsync( TModel model, DbConnection connection, DbTransaction transaction, params string[] wheres );

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        Result<TModel> Delete( TModel model, DbConnection connection, DbTransaction transaction, params string[] wheres );

        /// <summary>資料刪除，刪除整個資料表的資料
        /// </summary>
        /// <returns>刪除結果</returns>
        Task<IResult> ClearAsync();

        /// <summary>資料刪除，刪除整個資料表的資料
        /// </summary>
        /// <returns>刪除結果</returns>
        IResult Clear();

        /// <summary>資料刪除，刪除整個資料表的資料
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        Task<IResult> ClearAsync( DbConnection connection = null, DbTransaction transaction = null );

        /// <summary>資料刪除，刪除整個資料表的資料
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        IResult Clear( DbConnection connection = null, DbTransaction transaction = null );
    }
}
