﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料來源上下文 CRUD 操作基底 (支援同步作業執行 CRUD ORM 操作)
    /// </summary>
    public abstract partial class DataContext<TQueryReqArgs, TModel> : IDataContext<TQueryReqArgs, TModel>
        where TQueryReqArgs : new()
        where TModel        : class
    {
        #region 宣告公開的資料庫連線方法

        /// <summary>建立一個新的資料庫連線，但是資料庫連線狀態尚未開啟
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        /// <returns>新的資料庫連線</returns>
        public async Task<DbConnection> CreateConnectionAsync( string dbName = null ) => await SqlEngine.CreateConnectionAsync( dbName );

        #endregion 宣告公開的資料庫連線方法
        

        #region 實作 IDataContext 介面

        /// <summary>資料查詢
        /// </summary>
        /// <param name="selectInfo">查詢ORM對應資訊</param>
        /// <returns>查詢結果集合</returns>
        public virtual async Task<Result<List<TQueryReqArgs>>> SelectAsync( ISelectInfo<TQueryReqArgs> selectInfo )
        {
            async Task<Result<List<T>>> SelectAsync<T>( Func<Task<Result<List<T>>>> asyncFunc ) => await asyncFunc.Invoke();

            Func<Task<Result<List<TQueryReqArgs>>>> queryHandler = selectInfo.Top <= 0 ? 
                (Func<Task<Result<List<TQueryReqArgs>>>>)( () => SqlEngine.SelectAsync<TQueryReqArgs>( selectInfo.DataContent, selectInfo.IgnoreColumns.ToArray() ) ):
                (Func<Task<Result<List<TQueryReqArgs>>>>)( () => SqlEngine.SelectTopAsync<TQueryReqArgs>( selectInfo.DataContent, selectInfo.Top, selectInfo.IgnoreColumns.ToArray() ) );

            try
            {
                return await SelectAsync<TQueryReqArgs>( queryHandler );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( SelectAsync ) )}, execute select occur exception: {ex.ToString()}" );
                return Result.Create<List<TQueryReqArgs>>( false, null, StatusConstant.INTERNAL_ERROR, $"{GetLogTraceName( nameof ( SelectAsync ) )}, execute select occur exception: {ex.Message}" );
            }
        }

        /// <summary>資料查詢
        /// </summary>
        /// <param name="selectInfo">查詢ORM對應資訊</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>查詢結果集合</returns>
        public virtual async Task<Result<List<TQueryReqArgs>>> SelectAsync( ISelectInfo<TQueryReqArgs> selectInfo, DbConnection connection, DbTransaction transaction )
        {
            async Task<Result<List<T>>> SelectAsync<T>( Func<Task<Result<List<T>>>> asyncFunc ) => await asyncFunc.Invoke();

            Func<Task<Result<List<TQueryReqArgs>>>> queryHandler = selectInfo.Top <= 0 ? 
                (Func<Task<Result<List<TQueryReqArgs>>>>)( () => SqlEngine.SelectAsync<TQueryReqArgs>( selectInfo.DataContent, connection, transaction, selectInfo.IgnoreColumns.ToArray() ) ):
                (Func<Task<Result<List<TQueryReqArgs>>>>)( () => SqlEngine.SelectTopAsync<TQueryReqArgs>( selectInfo.DataContent, selectInfo.Top, connection, transaction, selectInfo.IgnoreColumns.ToArray() ) );

            try
            {
                return await SelectAsync<TQueryReqArgs>( queryHandler );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( SelectAsync ) )}, execute select occur exception: {ex.ToString()}" );
                return Result.Create<List<TQueryReqArgs>>( false, null, StatusConstant.INTERNAL_ERROR, $"{GetLogTraceName( nameof ( SelectAsync ) )}, execute select occur exception: {ex.Message}" );
            }
        }

        /// <summary>資料新增
        /// </summary>
        /// <param name="model">新增資料模型</param>
        /// <returns>新增結果</returns>
        public virtual async Task<Result<TModel>> InsertAsync( TModel model )
        {
            try
            {
                return await SqlEngine.InsertAsync<TModel>( model );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( InsertAsync ) )}, execute insert occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( InsertAsync ) )}, execute insert occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料新增
        /// </summary>
        /// <param name="model">新增資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>新增結果</returns>
        public virtual async Task<Result<TModel>> InsertAsync( TModel model, DbConnection connection, DbTransaction transaction )
        {
            try
            {
                return await SqlEngine.InsertAsync<TModel>( model, connection, transaction );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( InsertAsync ) )}, execute insert occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( InsertAsync ) )}, execute insert occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <returns>更新結果</returns>
        public virtual async Task<Result<TModel>> UpdateAsync( TModel model, DbParameterEntry[] wheres = null ) 
        {
            try
            {
                return await SqlEngine.UpdateAsync<TModel>( model, wheres );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( UpdateAsync ) )}, execute update occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( UpdateAsync ) )}, execute update occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>更新結果</returns>
        public virtual async Task<Result<TModel>> UpdateAsync( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null ) 
        {
            try
            {
                return await SqlEngine.UpdateAsync<TModel>( model, connection, transaction, wheres );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( UpdateAsync ) )}, execute update occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( UpdateAsync ) )}, execute update occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        public virtual async Task<Result<TModel>> UpdateAsync( TModel model, DbParameterEntry[] wheres = null, params string[] ignoreColumns ) 
        {
            try
            {
                return await SqlEngine.UpdateAsync<TModel>( model, wheres, ignoreColumns );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( UpdateAsync ) )}, execute update occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( UpdateAsync ) )}, execute update occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        public virtual async Task<Result<TModel>> UpdateAsync( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null, params string[] ignoreColumns ) 
        {
            try
            {
                return await SqlEngine.UpdateAsync<TModel>( model, connection, transaction, wheres, ignoreColumns );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( UpdateAsync ) )}, execute update occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( UpdateAsync ) )}, execute update occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <returns>刪除結果</returns>
        public virtual async Task<Result<TModel>> DeleteAsync( TModel model )
        {
            try
            {
                return await SqlEngine.DeleteAsync<TModel>( model );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( DeleteAsync ) )}, Execute delete occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( DeleteAsync ) )}, Execute delete occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        public virtual async Task<Result<TModel>> DeleteAsync( TModel model, DbConnection connection, DbTransaction transaction )
        {
            try
            {
                return await SqlEngine.DeleteAsync<TModel>( model, connection, transaction );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( DeleteAsync ) )}, Execute delete occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( DeleteAsync ) )}, Execute delete occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        public virtual async Task<Result<TModel>> DeleteAsync( TModel model, params string[] wheres )
        {
            try
            {
                return await SqlEngine.DeleteAsync<TModel>( model, wheres );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( DeleteAsync ) )}, Execute delete occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( DeleteAsync ) )}, Execute delete occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        public virtual async Task<Result<TModel>> DeleteAsync( TModel model, DbConnection connection, DbTransaction transaction, params string[] wheres )
        {
            try
            {
                return await SqlEngine.DeleteAsync<TModel>( model, connection, transaction, wheres );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( DeleteAsync ) )}, Execute delete occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( DeleteAsync ) )}, Execute delete occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除，刪除整個資料表的資料
        /// </summary>
        /// <returns>刪除結果</returns>
        public virtual async Task<IResult> ClearAsync()
        {
            try
            {
                return await SqlEngine.DeleteAllAsync<TModel>();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( ClearAsync ) )}, Execute delete all occur exception: {ex.ToString()}" );
                
                return new Result() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( ClearAsync ) )}, Execute delete all occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除，刪除整個資料表的資料
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        public virtual async Task<IResult> ClearAsync( DbConnection connection, DbTransaction transaction )
        {
            try
            {
                return await SqlEngine.DeleteAllAsync<TModel>( connection, transaction );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( ClearAsync ) )}, Execute delete all occur exception: {ex.ToString()}" );
                
                return new Result() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( ClearAsync ) )}, Execute delete all occur exception: {ex.Message}"
                };
            }
        }

        #endregion 實作 IDataContext 介面
    }
}
