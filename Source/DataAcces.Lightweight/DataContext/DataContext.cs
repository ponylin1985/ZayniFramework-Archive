﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料來源上下文 CRUD 操作基底 (支援同步作業執行 CRUD ORM 操作)
    /// </summary>
    /// <typeparam name="TQueryReqArgs">查詢請求參數的泛型</typeparam>
    /// <typeparam name="TModel">(新增、刪除、修改) 資料模型泛型</typeparam>
    public abstract partial class DataContext<TQueryReqArgs, TModel> : IDataContext<TQueryReqArgs, TModel>
        where TQueryReqArgs : new()
        where TModel        : class
    {
        #region 宣告保護的屬性

        /// <summary>Lightweight ORM 框架的輕量級 SQL 引擎
        /// </summary>
        protected SQLEngine SqlEngine { get; private set; }

        #endregion 宣告保護的屬性


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dataSourceContextName">資料來源名稱</param>
        public DataContext( string dataSourceContextName )
        {
            SqlEngine = new SQLEngine( dataSourceContextName );
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dataSourceContextName">資料來源名稱</param>
        /// <param name="dataSourceConnectionString">資料來源的資料庫連線字串</param>
        /// <param name="zayniConnnectionString">Zayni Framework 框架的資料庫連線字串</param>
        public DataContext( string dataSourceContextName, string dataSourceConnectionString, string zayniConnnectionString )
        {
            SqlEngine = new SQLEngine( dataSourceContextName, dataSourceConnectionString, zayniConnnectionString );
        }

        /// <summary>解構子
        /// </summary>
        ~DataContext()
        {
            SqlEngine = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的資料庫連線方法

        /// <summary>建立一個新的資料庫連線，但是資料庫連線狀態尚未開啟
        /// </summary>
        /// <param name="dbName">資料庫連線設定名稱</param>
        /// <returns>新的資料庫連線</returns>
        public DbConnection CreateConnection( string dbName = null ) => SqlEngine.CreateConnection( dbName );

        #endregion 宣告公開的資料庫連線方法


        #region 宣告公開的資料庫交易方法

        /// <summary>開啟資料庫交易
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="isolationLevel">交易鎖定層級</param>
        /// <returns>開啟資料庫交易的結果</returns>
        public DbTransaction BeginTransaction( DbConnection connection, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted ) => 
            SqlEngine.BeginTransaction( connection, isolationLevel );

        /// <summary>確認資料庫交易
        /// </summary>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="message">執行結果的訊息</param>
        /// <returns>是否成功確認資料庫交易</returns>
        public bool Commit( DbTransaction transaction, out string message ) =>
            SqlEngine.Commit( transaction, out message );

        /// <summary>退回資料庫交易
        /// </summary>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="message">執行結果的訊息</param>
        /// <returns>是否成功退回資料庫交易</returns>
        public bool Rollback( DbTransaction transaction, out string message ) => 
            SqlEngine.Rollback( transaction, out message );

        #endregion 宣告公開的資料庫交易方法


        #region 實作 IDataContext 介面

        /// <summary>資料查詢
        /// </summary>
        /// <param name="selectInfo">查詢ORM對應資訊</param>
        /// <returns>查詢結果集合</returns>
        public virtual Result<List<TQueryReqArgs>> Select( ISelectInfo<TQueryReqArgs> selectInfo )
        {
            var result = Result.Create<List<TQueryReqArgs>>();

            Action callback = selectInfo.Top <= 0 ? 
                (Action)( () => { result = SqlEngine.Select<TQueryReqArgs>( selectInfo.DataContent, selectInfo.IgnoreColumns.ToArray() ); } ) :
                (Action)( () => { result = SqlEngine.SelectTop<TQueryReqArgs>( selectInfo.DataContent, selectInfo.Top, selectInfo.IgnoreColumns.ToArray() ); } );

            try
            {
                callback();
            }
            catch ( Exception ex )
            {
                result.HasException    = true;
                result.ExceptionObject = ex;
                result.Code            = StatusConstant.INTERNAL_ERROR;
                result.Message         = $"{GetLogTraceName( nameof ( Select ) )}, execute select occur exception: {ex.Message}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Select ) )}, execute select occur exception: {ex.ToString()}" );
                return result;
            }

            return result;
        }

        /// <summary>資料查詢
        /// </summary>
        /// <param name="selectInfo">查詢ORM對應資訊</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>查詢結果集合</returns>
        public virtual Result<List<TQueryReqArgs>> Select( ISelectInfo<TQueryReqArgs> selectInfo, DbConnection connection, DbTransaction transaction )
        {
            var result = Result.Create<List<TQueryReqArgs>>();

            Action callback = selectInfo.Top <= 0 ? 
                (Action)( () => { result = SqlEngine.Select<TQueryReqArgs>( selectInfo.DataContent, connection, transaction, selectInfo.IgnoreColumns.ToArray() ); } ) :
                (Action)( () => { result = SqlEngine.SelectTop<TQueryReqArgs>( selectInfo.DataContent, selectInfo.Top, connection, transaction, selectInfo.IgnoreColumns.ToArray() ); } );

            try
            {
                callback();
            }
            catch ( Exception ex )
            {
                result.HasException    = true;
                result.ExceptionObject = ex;
                result.Code            = StatusConstant.INTERNAL_ERROR;
                result.Message         = $"{GetLogTraceName( nameof ( Select ) )}, execute select occur exception: {ex.Message}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Select ) )}, execute select occur exception: {ex.ToString()}" );
                return result;
            }

            return result;
        }

        /// <summary>資料新增
        /// </summary>
        /// <param name="model">新增資料模型</param>
        /// <returns>新增結果</returns>
        public virtual Result<TModel> Insert( TModel model )
        {
            try
            {
                return SqlEngine.Insert<TModel>( model );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Insert ) )}, execute insert occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Insert ) )}, execute insert occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料新增
        /// </summary>
        /// <param name="model">新增資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>新增結果</returns>
        public virtual Result<TModel> Insert( TModel model, DbConnection connection, DbTransaction transaction )
        {
            try
            {
                return SqlEngine.Insert<TModel>( model, connection, transaction );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Insert ) )}, execute insert occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Insert ) )}, execute insert occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <returns>更新結果</returns>
        public virtual Result<TModel> Update( TModel model, DbParameterEntry[] wheres = null ) 
        {
            try
            {
                return SqlEngine.Update<TModel>( model, wheres );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Update ) )}, execute update occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Update ) )}, execute update occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>更新結果</returns>
        public virtual Result<TModel> Update( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null ) 
        {
            try
            {
                return SqlEngine.Update<TModel>( model, connection, transaction, wheres );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Update ) )}, execute update occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Update ) )}, execute update occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        public virtual Result<TModel> Update( TModel model, DbParameterEntry[] wheres = null, params string[] ignoreColumns ) 
        {
            try
            {
                return SqlEngine.Update<TModel>( model, wheres, ignoreColumns );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Update ) )}, execute update occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Update ) )}, execute update occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料更新 <para/>
        /// 1. 根據指定的資料模型泛型的 ORM Metadata，和傳入的資料模型中的資料，利用設定為 PKey 屬性值當作 Where 條件對資料庫 table 進行資料更新動作!<para/>
        /// 2. PKey 欄位不可以更新，只當作 Where 條件參數!<para/>
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料更新的 Where 條件集合</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="ignoreColumns">忽略更新的欄位</param>
        /// <returns>更新結果</returns>
        public virtual Result<TModel> Update( TModel model, DbConnection connection, DbTransaction transaction, DbParameterEntry[] wheres = null, params string[] ignoreColumns ) 
        {
            try
            {
                return SqlEngine.Update<TModel>( model, connection, transaction, wheres, ignoreColumns );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Update ) )}, execute update occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Update ) )}, execute update occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <returns>刪除結果</returns>
        public virtual Result<TModel> Delete( TModel model )
        {
            try
            {
                return SqlEngine.Delete<TModel>( model );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Delete ) )}, Execute delete occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Delete ) )}, Execute delete occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        public virtual Result<TModel> Delete( TModel model, DbConnection connection, DbTransaction transaction )
        {
            try
            {
                return SqlEngine.Delete<TModel>( model, connection, transaction );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Delete ) )}, Execute delete occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Delete ) )}, Execute delete occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        public virtual Result<TModel> Delete( TModel model, params string[] wheres )
        {
            try
            {
                return SqlEngine.Delete<TModel>( model, wheres );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Delete ) )}, Execute delete occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Delete ) )}, Execute delete occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除
        /// </summary>
        /// <param name="model">刪除資料模型</param>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <param name="wheres">資料刪除的 Where 條件集合</param>
        /// <returns>刪除結果</returns>
        public virtual Result<TModel> Delete( TModel model, DbConnection connection, DbTransaction transaction, params string[] wheres )
        {
            try
            {
                return SqlEngine.Delete<TModel>( model, connection, transaction, wheres );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Delete ) )}, Execute delete occur exception: {ex.ToString()}" );

                return new Result<TModel>() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Delete ) )}, Execute delete occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除，刪除整個資料表的資料
        /// </summary>
        /// <returns>刪除結果</returns>
        public virtual IResult Clear()
        {
            try
            {
                return SqlEngine.DeleteAll<TModel>();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Clear ) )}, Execute delete all occur exception: {ex.ToString()}" );
                
                return new Result() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Clear ) )}, Execute delete all occur exception: {ex.Message}"
                };
            }
        }

        /// <summary>資料刪除，刪除整個資料表的資料
        /// </summary>
        /// <param name="connection">資料庫連線</param>
        /// <param name="transaction">資料庫交易</param>
        /// <returns>刪除結果</returns>
        public virtual IResult Clear( DbConnection connection, DbTransaction transaction )
        {
            try
            {
                return SqlEngine.DeleteAll<TModel>( connection, transaction );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{GetLogTraceName( nameof ( Clear ) )}, Execute delete all occur exception: {ex.ToString()}" );
                
                return new Result() 
                {
                    Success         = false,
                    HasException    = true,
                    ExceptionObject = ex,
                    Code            = StatusConstant.INTERNAL_ERROR,
                    Message         = $"{GetLogTraceName( nameof ( Clear ) )}, Execute delete all occur exception: {ex.Message}"
                };
            }
        }

        #endregion 實作 IDataContext 介面


        #region 宣告私有的方法

        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( DataContext<TQueryReqArgs, TModel> )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
