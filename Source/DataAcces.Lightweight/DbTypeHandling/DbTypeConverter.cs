﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料庫型別轉換器
    /// </summary>
    internal class DbTypeConverter
    {
        /// <summary>將 C# 型別轉換為資料庫型別代碼 (Zayni Framework框架定義的 DbTypeCode 資料庫型別代碼)
        /// </summary>
        /// <param name="type">C# 型別</param>
        /// <returns>資料庫型別代碼 (Zayni Framework框架定義的 DbTypeCode 資料庫型別代碼)</returns>
        internal static int Convert( Type type )
        {
            string typeName = type.Name;

            // 20170929 Edited by Pony: 為了支援資料庫中有 Nullable 的欄位，因此，這邊還是支援 C# Nullable 的型別轉換。
            if ( Reflector.IsNullableType( type ) && type.IsValueType )
            {
                typeName = Reflector.GetNullableType( type ).Name;
            }

            switch ( typeName )
            {
                case "String":
                    return DbTypeCode.String;

                case "Boolean":
                    return DbTypeCode.Boolean;

                case "Int32":
                    return DbTypeCode.Int32;

                case "Int64":
                    return DbTypeCode.Int64;

                case "Decimal":
                    return DbTypeCode.Decimal;

                case "Double":
                    return DbTypeCode.Double;

                case "Byte[]":
                    return DbTypeCode.Binary;

                case "DateTime":
                    return DbTypeCode.DateTime;

                default:
                    throw new DbTypeConvertErrorException();
            }
        }
    }
}
