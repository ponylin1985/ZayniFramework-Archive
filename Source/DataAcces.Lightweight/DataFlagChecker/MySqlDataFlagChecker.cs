﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Common.ORM;


namespace ZayniFramework.DataAccess.Lightweight
{
    // MySQL 資料庫更新鎖定的參考
    // http://fishjerky.blogspot.tw/2013/06/update-db-transaction-lock.html
    // http://www.neo.com.tw/archives/863
    // http://blog.xuite.net/vexed/tech/22289223-%E7%94%A8+SELECT+...+FOR+UPDATE+%E9%81%BF%E5%85%8D+Race+condition
    /// <summary>MySQL 資料庫的資料異動檢查器
    /// </summary>
    internal class MySqlDataFlagChecker : DataFlagChecker, IDataFlagChecker
    {
        /// <summary>多載建構子
        /// </summary>
        /// <param name="dao">資料存取物件</param>
        public MySqlDataFlagChecker( Dao dao ) : base( dao )
        {
            // pass
        }

        /// <summary>進行資料是否有異動的檢查 
        /// * 注意! 目前 DataFlag 資料異動時間戳記檢查機制，只能依照資料模型中的 Primary Key 當作 where 條件進行檢查!
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="conn">資料庫連線</param>
        /// <param name="trans">資料庫交易</param>
        /// <returns>資料是否有異動，true 代表有異動，false 代表沒有異動。</returns>
        public async Task<bool> CheckAsync( object model, DbConnection conn = null, DbTransaction trans = null )
        {
            Type type = model.GetType();

            var tableMetadata = Reflector.GetCustomAttribute<MappingTableAttribute>( type );

            string tableName           = tableMetadata.TableName;
            string timestampColumnName = string.Empty;
            object timestampValue      = null;

            string sql = @"
                -- CheckDateFlagTimestmap
                SELECT {timestampColumn} FROM {table} ";

            var pKeys = new Dictionary<string, DbParameterEntry>();

            foreach ( var p in type.GetProperties() )
            {
                TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( p );

                if ( metadata is null )
                {
                    continue;
                }

                object value = null;

                try
                {
                    value = p.GetValue( model );
                }
                catch ( Exception )
                {
                    continue;
                }

                if ( value.IsNull() )
                {
                    continue;
                }

                int dbType = DbTypeConverter.Convert( p.PropertyType );

                if ( metadata.IsTimeStamp )
                {
                    timestampColumnName = metadata.ColumnName;
                    timestampValue      = new DateTime( Convert.ToInt64( value ) );
                    continue;
                }

                if ( metadata.IsPrimaryKey )
                {
                    pKeys.Add( metadata.ColumnName, new DbParameterEntry() 
                    {
                        Name   = metadata.ColumnName,
                        Value  = value,    
                        DbType = DbTypeConverter.Convert( p.PropertyType )
                    } );
                }
            }

            if ( timestampColumnName.IsNullOrEmpty() )
            {
                return false;
            }

            // 當傳入的資料模型，IsTimestamp = true 的屬性值為 Null 或 PKey 屬性值都為 Null 的情況下，就略過不檢查 DataFlag timestamp 資料異動時間戳記。
            if ( timestampValue.IsNull() || pKeys.IsNullOrEmptyCollection() )
            {
                return true;
            }

            sql  = sql.Format( table => tableName, timestampColumn => timestampColumnName );
            sql += $" WHERE 1 = 1 ";

            foreach ( var pKey in pKeys )
            {
                sql += $" AND {pKey.Value.Name} = @{pKey.Value.Name} ";
            }

            sql += $"{Environment.NewLine} FOR UPDATE ";

            DateTime dataFlag = default ( DateTime );

            try
            {
                dataFlag = (DateTime)timestampValue;
            }
            catch ( Exception )
            {
                return true;
            }

            var cmd = base.Dao.GetSqlStringCommand( sql, conn, trans );

            foreach ( var pKey in pKeys )
            {
                DbParameterEntry parameter = pKey.Value;
                base.Dao.AddInParameter( cmd, $"@{parameter.Name}", parameter.DbType, parameter.Value );
            }

            var r = await base.Dao.ExecuteScalarAsync( cmd );

            // 如果嘗試查詢資料庫中的 timestamp 值失敗，或資料庫中的 timestamp 值為 DBNull，則不允許放行!
            if ( !r.Success || r.Data.IsNull() )
            {
                return true;
            }

            DateTime timestamp = Convert.ToDateTime( r.Data );
            return dataFlag != timestamp;
        }

        /// <summary>進行資料是否有異動的檢查 
        /// * 注意! 目前 DataFlag 資料異動時間戳記檢查機制，只能依照資料模型中的 Primary Key 當作 where 條件進行檢查!
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="conn">資料庫連線</param>
        /// <param name="trans">資料庫交易</param>
        /// <returns>資料是否有異動，true 代表有異動，false 代表沒有異動。</returns>
        public bool Check( object model, DbConnection conn = null, DbTransaction trans = null )
        {
            Type type = model.GetType();

            var tableMetadata = Reflector.GetCustomAttribute<MappingTableAttribute>( type );

            string tableName           = tableMetadata.TableName;
            string timestampColumnName = string.Empty;
            object timestampValue      = null;

            string sql = @"
                -- CheckDateFlagTimestmap
                SELECT {timestampColumn} FROM {table} ";

            var pKeys = new Dictionary<string, DbParameterEntry>();

            foreach ( var p in type.GetProperties() )
            {
                TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( p );

                if ( metadata is null )
                {
                    continue;
                }

                object value = null;

                try
                {
                    value = p.GetValue( model );
                }
                catch ( Exception )
                {
                    continue;
                }

                if ( value.IsNull() )
                {
                    continue;
                }

                int dbType = DbTypeConverter.Convert( p.PropertyType );

                if ( metadata.IsTimeStamp )
                {
                    timestampColumnName = metadata.ColumnName;
                    timestampValue      = new DateTime( Convert.ToInt64( value ) );
                    continue;
                }

                if ( metadata.IsPrimaryKey )
                {
                    pKeys.Add( metadata.ColumnName, new DbParameterEntry() 
                    {
                        Name   = metadata.ColumnName,
                        Value  = value,    
                        DbType = DbTypeConverter.Convert( p.PropertyType )
                    } );
                }
            }

            if ( timestampColumnName.IsNullOrEmpty() )
            {
                return false;
            }

            // 當傳入的資料模型，IsTimestamp = true 的屬性值為 Null 或 PKey 屬性值都為 Null 的情況下，就略過不檢查 DataFlag timestamp 資料異動時間戳記。
            if ( timestampValue.IsNull() || pKeys.IsNullOrEmptyCollection() )
            {
                return true;
            }

            sql  = sql.Format( table => tableName, timestampColumn => timestampColumnName );
            sql += $" WHERE 1 = 1 ";

            foreach ( var pKey in pKeys )
            {
                sql += $" AND {pKey.Value.Name} = @{pKey.Value.Name} ";
            }

            sql += $"{Environment.NewLine} FOR UPDATE ";

            DateTime dataFlag = default ( DateTime );

            try
            {
                dataFlag = (DateTime)timestampValue;
            }
            catch ( Exception )
            {
                return true;
            }

            var cmd = base.Dao.GetSqlStringCommand( sql, conn, trans );

            foreach ( var pKey in pKeys )
            {
                DbParameterEntry parameter = pKey.Value;
                base.Dao.AddInParameter( cmd, $"@{parameter.Name}", parameter.DbType, parameter.Value );
            }

            bool success = base.Dao.ExecuteScalar( cmd, out var obj );

            // 如果嘗試查詢資料庫中的 timestamp 值失敗，或資料庫中的 timestamp 值為 DBNull，則不允許放行!
            if ( !success || obj.IsNull() )
            {
                return true;
            }

            DateTime timestamp = Convert.ToDateTime( obj );
            return dataFlag != timestamp;
        }
    }
}
