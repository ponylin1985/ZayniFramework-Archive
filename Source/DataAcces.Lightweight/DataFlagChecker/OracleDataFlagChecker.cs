﻿using System.Data.Common;
using System.Threading.Tasks;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>Oracle資料庫的資料異動檢查器
    /// </summary>
    internal class OracleDataFlagChecker : DataFlagChecker, IDataFlagChecker
    {
        /// <summary>多載建構子
        /// </summary>
        /// <param name="dao">資料存取物件</param>
        internal OracleDataFlagChecker( Dao dao ) : base( dao )
        {
            // pass
        }

        /// <summary>進行資料是否有異動的檢查 (Oracle版本暫時未實作此功能。)
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="conn">資料庫連線</param>
        /// <param name="trans">資料庫交易</param>
        /// <returns>資料是否有異動，true代表有異動，false代表沒有異動。</returns>
        public Task<bool> CheckAsync( object model, DbConnection conn = null, DbTransaction trans = null ) => Task.FromResult( false );

        /// <summary>進行資料是否有異動的檢查 (Oracle版本暫時未實作此功能。)
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <param name="conn">資料庫連線</param>
        /// <param name="trans">資料庫交易</param>
        /// <returns>資料是否有異動，true代表有異動，false代表沒有異動。</returns>
        public bool Check( object model, DbConnection conn = null, DbTransaction trans = null ) => false;
    }
}
