﻿using System.Collections.Generic;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>建立刪除 SQL 語法結果
    /// </summary>
    internal sealed class CreateDeleteSqlResult
    {
        /// <summary>是否成功
        /// </summary>
        internal bool IsSuccess { get; set; }

        /// <summary>刪除SQL語法字串
        /// </summary>
        internal string Sql { get; set; }

        /// <summary>資料刪除 Where 條件集合
        /// </summary>
        internal List<PrimaryKeyInfo> Wheres { get; set; } = new List<PrimaryKeyInfo>();

        /// <summary>刪除語法是否有包含 Primary Key 主索引鍵的Where條件
        /// </summary>
        internal bool HasPKeyCondition { get; set; }

        /// <summary>執行結果訊息
        /// </summary>
        internal string Message { get; set; }
    }
}
