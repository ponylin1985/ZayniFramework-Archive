﻿using System;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>DbCommand 的參數資訊集
    /// </summary>
    [Serializable()]
    public sealed class DbParameterEntry
    {
        /// <summary>資料庫欄位名稱
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>參數名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>參數資料庫型別代碼 (Zayni Framework Data Access module 的 DbTypeCode 值)
        /// * Pass the DbTypeCode enum value.
        /// </summary>
        public int DbType { get; set; }

        /// <summary>參數值
        /// </summary>
        public object Value { get; set; }
    }
}
