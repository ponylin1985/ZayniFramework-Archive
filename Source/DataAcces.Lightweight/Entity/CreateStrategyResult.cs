﻿using ZayniFramework.Common;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>建立物件的結果
    /// </summary>
    internal sealed class CreateStrategyResult : BaseResult
    {
        /// <summary>SQL敘述字串建立者
        /// </summary>
        internal ISqlStringBuilder SqlBuilder { get; set; }

        /// <summary>資料異動檢查器
        /// </summary>
        internal IDataFlagChecker DataChecker { get; set; }
    }
}
