﻿using ZayniFramework.Common.ORM;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料表主索引鍵資訊
    /// </summary>
    internal sealed class PrimaryKeyInfo
    {
        /// <summary>資料表欄位對應 ORM 中介資料
        /// </summary>
        internal TableColumnAttribute PrimaryKeyColumn { get; set; }

        /// <summary>主索引鍵參數名稱
        /// </summary>
        internal string Name { get; set; }

        /// <summary>主索引鍵參數值
        /// </summary>
        internal object Value { get; set; }

        /// <summary>主索引鍵資料庫型別代碼
        /// </summary>
        internal int DbType { get; set; }
    }
}
