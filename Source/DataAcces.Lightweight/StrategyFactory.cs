﻿namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>SQL字串與資料異動檢查器的工廠
    /// </summary>
    internal class StrategyFactory
    {
        /// <summary>建立執行時期所需的實體
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <param name="dao">資料存取物件</param>
        /// <returns>建立結果</returns>
        internal static CreateStrategyResult Create( string dbName, Dao dao )
        {
            var result = new CreateStrategyResult()
            {
                Success = false
            };

            string dbProvider = DataAccessConfig.GetDbProvider( dbName );

            switch ( dbProvider.ToUpper() )
            {
                case "MSSQL":
                    result.SqlBuilder  = new TSQLStringBuilder();
                    result.DataChecker = new MSSqlDataFlagChecker( dao );
                    result.Success     = true;
                    break;

                case "ORACLE":
                    result.SqlBuilder  = new PLSQLStringBuilder();
                    result.DataChecker = new OracleDataFlagChecker( dao );
                    result.Success     = true;
                    break;

                case "MYSQL":
                    result.SqlBuilder  = new MySqlStringBuilder();
                    result.DataChecker = new MySqlDataFlagChecker( dao );
                    result.Success     = true;
                    break;

                default:
                    result.SqlBuilder  = new TSQLStringBuilder();
                    result.DataChecker = new MSSqlDataFlagChecker( dao );
                    result.Success     = true;
                    break;
            }

            return result;
        }
    }
}
