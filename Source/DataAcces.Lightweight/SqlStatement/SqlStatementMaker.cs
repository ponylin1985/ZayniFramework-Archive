﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Common.ORM;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>資料庫 SQL 字串產生器
    /// </summary>
    internal class SqlStatementMaker
    {
        #region 宣告內部的靜態方法

        /// <summary>建立資料庫查詢欄位的 SQL 語法
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <returns>建立 SQL 字串結果</returns>
        internal static CreateColumnsSqlResult CreateColumnSql<TModel>( string[] ignoreColumns = null )
        {
            var result = new CreateColumnsSqlResult() 
            {
                IsSuccess            = false,
                ColumnsString        = "",
                OrderByColumnsString = ""
            };

            string cacheId = "{0}:CreateColumnSql".FormatTo( GetCacheId<TModel>() );

            GetSomethingFromDataSource callback = () => 
            {
                var r = new CreateColumnsSqlResult() 
                {
                    IsSuccess            = false,
                    ColumnsString        = "",
                    OrderByColumnsString = ""
                };

                var sbColumns = new StringBuilder();
                var sbOrderBy = new StringBuilder();

                try
                {
                    foreach ( var prop in typeof ( TModel ).GetProperties() )
                    {
                        TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( prop );

                        if ( metadata.IsNull() )
                        {
                            continue;
                        }

                        if ( NeedIgnore( ignoreColumns, metadata.ColumnName ) )
                        {
                            continue;
                        }

                        string s = "{0}{1}".FormatTo( SqlStatementTemplate.Column.FormatTo( metadata.ColumnName ), Environment.NewLine );
                        sbColumns.Append( s );

                        if ( metadata.OrderBy )
                        {
                            string desc    = metadata.OrderByDesc ? "DESC" : "";
                            string orderBy = " {0} {1}, {2} ".FormatTo( metadata.ColumnName, desc, Environment.NewLine );
                            sbOrderBy.Append( orderBy );
                        }
                    }

                    r.ColumnsString        = RemoveFirstComma( sbColumns.ToString() );
                    r.OrderByColumnsString = sbOrderBy.ToString().IsNullOrEmpty() ? "" : RemoveLastComma( sbOrderBy.ToString() );
                }
                catch ( Exception ex )
                {
                    r.Message = "建立資料庫查詢欄位的SQL語法發生異常: {0}".FormatTo( ex.Message );
                    return r;
                }

                r.IsSuccess = true;
                return r;
            };

            // 20141227 Edited by Pony: 如果在忽略欄位清單確實有傳入的狀況下，就不能去在記憶體快去中直接取得查詢欄位字串，以免誤判!!!
            if ( ignoreColumns.IsNotNullOrEmptyArray() )
            {
                result = (CreateColumnsSqlResult)callback();
                return result;
            }

            var g = MemoryCache.Get<CreateColumnsSqlResult>( cacheId, callback, 60 );

            if ( !g.Success )
            {
                result = (CreateColumnsSqlResult)callback();
                MemoryCache.Put( cacheId, result, 60 );
                return result;
            }

            if ( !g.CacheData.IsSuccess )
            {
                result.Message = "建立資料庫查詢欄位的SQL語法失敗。";
                return result;
            }

            result           = g.CacheData;
            result.IsSuccess = true;
            return result;
        }

        /// <summary>建立資料庫查詢 SELECT 的 SQL 語法
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="sqlColumns">SQL Select查詢語法字串</param>
        /// <param name="orderByColumns">SQL Order By 排序的欄位字串</param>
        /// <returns>建立 SQL 字串結果</returns>
        internal static Result<string> CreateSelectSql<TModel>( string sqlColumns, string orderByColumns )
        {
            var result = new Result<string>() 
            {
                Success = false,
                Data      = ""
            };

            string cacheId = "{0}:CreateSelectSql".FormatTo( GetCacheId<TModel>() );

            GetSomethingFromDataSource callback = () => 
            {
                MappingTableAttribute orm = null;

                try
                {
                    orm = Reflector.GetCustomAttribute<MappingTableAttribute>( typeof ( TModel ) );
                }
                catch ( Exception ex )
                {
                    throw new CreateSqlStatementException( "建立資料庫查詢SELECT的SQL語法發生異常: {0}".FormatTo( ex.Message ) );
                }

                if ( orm.IsNull() )
                {
                    throw new CreateSqlStatementException( "建立資料庫查詢SELECT的SQL語法發生異常: 反射取得資料模型的MappingTableAttribute中介資料為Null值。" );
                }

                if ( orm.TableName.IsNullOrEmpty() )
                {
                    throw new CreateSqlStatementException( "建立資料庫查詢SELECT的SQL語法發生異常: 反射取得資料模型中對應資料庫的資料表名稱為空字串或Null。" );
                }

                // 20141227 Edited by Pony: 這邊只應該要快取資料表名稱，而不是整個完整的查詢字串，否則，在外界有傳入忽略欄位清單時，會有誤判的情況!!!
                return orm.TableName;
            };

            string tableName = "";

            var g = MemoryCache.Get<string>( cacheId, callback, 60 );

            if ( !g.Success )
            {
                tableName = callback() + "";
                MemoryCache.Put( cacheId, tableName, 60 );
            }

            if ( g.CacheData.IsNullOrEmpty() )
            {
                result.Message = "建立資料庫查詢SELECT的SQL語法失敗。";
                return result;
            }

            tableName   = g.CacheData;
            result.Data = orderByColumns.IsNullOrEmpty() ? 
                    SqlStatementTemplate.Select.FormatTo( sqlColumns, tableName ) :
                    SqlStatementTemplate.Select.FormatTo( sqlColumns, tableName ) + " ORDER BY {0} ".FormatTo( orderByColumns );
            
            result.Success = true;
            return result;
        }

        /// <summary>根據資料表的 Primary Key 建立 SQL 查詢語法的主要 Where 查詢條件 (如果條件的運算子為 LIKE，pkValue 參數必須要傳入!)
        /// </summary>
        /// <remarks>如果條件的運算子為 LIKE，pkValue 參數必須要傳入!</remarks>
        /// <param name="metadata">ORM資料對應中介資料</param>
        /// <param name="pkValue">如果條件的運算子為 LIKE，pkValue 參數必須要傳入</param>
        /// <returns>建立 SQL 字串結果</returns>
        internal static Result<string> CreatePkWhereCondition( TableColumnAttribute metadata, string pkValue = "" )
        {
            var result = new Result<string>() 
            {
                Success = false,
                Data      = ""
            };

            string sqlOperator = metadata.SqlOperator.IsNullOrEmptyString( "=" );

            try
            {
                string value = "=" == sqlOperator ? $"@{metadata.ColumnName}" : $"'{pkValue}%'";
                result.Data  = SqlStatementTemplate.WhereCondition.FormatTo( metadata.ColumnName, sqlOperator, value );
            }
            catch ( Exception ex )
            {
                result.Message = $"Create where SQL condition by primary key ORM metadata occur exception. ColumnName: {metadata.ColumnName}, SqlOperator: {metadata.SqlOperator}. {Environment.NewLine}{ex.ToString()}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>根據資料表的 Primary Key 建立 SQL 查詢語法的主要 Where 查詢條件
        /// </summary>
        /// <param name="primaryKeys">資料表主索引鍵資訊集合</param>
        /// <returns>建立 SQL 字串結果</returns>
        internal static Result<string> CreatePkWhereConditions( List<PrimaryKeyInfo> primaryKeys )
        {
            var result = new Result<string>() 
            {
                Success = false,
                Data      = null
            };

            var whereSqlPrimaryKeys = new List<string>();

            foreach ( var primaryKey in primaryKeys )
            {
                string pKeyValue = primaryKey.Value?.ToString();

                if ( pKeyValue.IsNullOrEmpty() )
                {
                    continue;
                }

                var r = CreatePkWhereCondition( primaryKey.PrimaryKeyColumn, pKeyValue );

                if ( !r.Success )
                {
                    result.Message = r.Message;
                    return result;
                }

                string sql = r.Data;

                if ( sql.IsNullOrEmpty() )
                {
                    continue;
                }

                whereSqlPrimaryKeys.Add( sql );
            }

            string sqlWhere = null;

            try
            {
                sqlWhere = whereSqlPrimaryKeys.ToArray().JoinToken( "\nAND" );
            }
            catch ( Exception ex )
            {
                var sb = new StringBuilder();
                whereSqlPrimaryKeys?.Select( j => sb.AppendLine( j ) );
                result.Message = $"Create multiple where SQL condition by primary key ORM metadata occur exception. Where SQL Stmt: {sb.ToString()}{Environment.NewLine}{ex.ToString()}";
                return result;
            }

            result.Data      = sqlWhere;
            result.Success = true;
            return result;
        }

        /// <summary>根據資料模型的 ORM Metadata 和屬性值建立 SQL 查詢語法的 Where 條件
        /// </summary>
        /// <typeparam name="TModel">資料模型的泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="whereConditions">查詢條件欄位名稱集合</param>
        /// <returns>建立 SQL Where 查詢條件的結果</returns>
        internal static CreateWhereResult CreateWhereConditions<TModel>( TModel model, string[] whereConditions )
        {
            var result = new CreateWhereResult()
            {
                Success = false,
                Data      = null
            };

            var properties = model.GetType().GetProperties( BindingFlags.Public | BindingFlags.Instance );

            if ( properties.IsNullOrEmptyArray() )
            {
                result.Success = true;
                return result;
            }

            var parameters = new List<DbParameterEntry>();
            var sqlWheres  = new List<string>();

            foreach ( var property in properties )
            {
                string value = property.GetValue( model )?.ToString();

                if ( value.IsNullOrEmpty() )
                {
                    continue;
                }

                TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( property );

                if ( metadata.IsNull() )
                {
                    continue;
                }

                if ( whereConditions.IsNotNullOrEmptyArray() && !whereConditions.Contains( metadata.ColumnName ) )
                {
                    continue;
                }

                string sql = SqlStatementTemplate.WhereCondition.FormatTo( metadata.ColumnName, "=", $"@{metadata.ColumnName}" );

                if ( sql.IsNullOrEmpty() )
                {
                    continue;
                }

                var parameter = new DbParameterEntry()
                {
                    Name   = $"@{metadata.ColumnName}",
                    Value  = value,
                    DbType = DbTypeConverter.Convert( property.PropertyType ),
                };

                if ( Reflector.IsBooleanType( property.PropertyType ) )
                {
                    parameter.Value = Convert.ToBoolean( value ) ? 1 : 0;
                }

                parameters.Add( parameter );
                sqlWheres.Add( sql );
            }

            if ( sqlWheres.IsNullOrEmptyList() )
            {
                result.Success = true;
                return result;
            }

            string sqlWhere = null;

            try
            {
                sqlWhere = sqlWheres.ToArray().JoinToken( "\nAND" );
            }
            catch ( Exception ex )
            {
                var sb = new StringBuilder();
                sqlWheres?.Select( j => sb.AppendLine( j ) );
                result.Message = $"Create multiple where SQL condition by ORM metadata occur exception. Where SQL Stmt: {sb.ToString()}{Environment.NewLine}{ex.ToString()}";
                return result;
            }

            result.Data         = parameters;
            result.SqlWhereStmt = sqlWhere;
            result.Success    = true;
            return result;
        }

        /// <summary>根據資料模型的 ORM Metadata 和屬性值建立 SQL 語法的 Where 條件
        /// </summary>
        /// <param name="whereConditions">Where 條件的資料集合</param>
        /// <returns>建立 SQL Where 查詢條件的結果</returns>
        internal static CreateWhereResult CreateWhereConditions( DbParameterEntry[] whereConditions )
        {
            var result = new CreateWhereResult()
            {
                Success = false,
                Data    = null
            };

            var sqlWheres  = new List<string>();

            foreach ( var where in whereConditions )
            {
                sqlWheres.Add( $" {where.ColumnName} = @{where.Name} " );
            }

            string sqlWhere = null;

            try
            {
                sqlWhere = sqlWheres.ToArray().JoinToken( "\nAND" );
            }
            catch ( Exception ex )
            {
                var sb = new StringBuilder();
                sqlWheres?.Select( j => sb.AppendLine( j ) );
                result.Message = $"Create multiple where SQL condition by ORM metadata occur exception. Where SQL Stmt: {sb.ToString()}{Environment.NewLine}{ex.ToString()}";
                return result;
            }

            result.Data         = whereConditions.ToList();
            result.SqlWhereStmt = sqlWhere;
            result.Success    = true;
            return result;
        }

        /// <summary>建立 SQL 查詢語法完整的 Where 敘述
        /// </summary>
        /// <param name="whereCondition">SQL Where條件的字串</param>
        /// <returns>建立SQL字串結果</returns>
        internal static Result<string> CreateWhereSql( string whereCondition )
        {
            var result = new Result<string>() 
            {
                Success = false,
                Data    = ""
            };

            try
            {
                result.Data = SqlStatementTemplate.Where.FormatTo( whereCondition );
            }
            catch ( Exception ex )
            {
                result.Message = $"Generate WHERE SQL stmt occur exception. {ex.ToString()}";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>檢查資料模型中是否有設定 Primary Key 主索引鍵
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <returns>檢查結果</returns>
        internal static Result<TableColumnAttribute> HasPkCondition<TModel>( TModel model )
        {
            var result = new Result<TableColumnAttribute>() 
            {
                Success = false,
                Data      = null
            };

            if ( model.IsNull() )
            {
                // 故意在這邊給一個預設值
                result.Data = new TableColumnAttribute() 
                {
                    SqlOperator = "LIKE"    
                };

                return result;
            }

            try
            {
                foreach ( var p in typeof ( TModel ).GetProperties() )
                {
                    TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( p );

                    if ( metadata.IsNull() )
                    {
                        continue;
                    }

                    // 如果發現為Primary Key的屬性，取得屬性值、轉換該屬性對應到資料庫的型別、並且回傳該屬性的ORM metadata
                    if ( metadata.IsPrimaryKey ) 
                    {
                        dynamic d = (dynamic)result;
                        d.Value   = p.GetValue( model );
                        d.DbType  = DbTypeConverter.Convert( p.PropertyType );

                        result.Data      = metadata;
                        result.Success = true;
                        return result;
                    }
                }
            }
            catch ( Exception ex )
            {
                result.Message = "檢查資料模型中是否有設定Primary Key主索引鍵發生異常: {0}".FormatTo( ex.Message );
                return result;
            }

            return result;
        }

        /// <summary>檢查資料模型中是否有設定 Primary Key 主索引鍵
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <returns>檢查結果</returns>
        internal static Result<List<PrimaryKeyInfo>> HasPKeyConditions<TModel>( TModel model )
        {
            var result = new Result<List<PrimaryKeyInfo>>() 
            {
                Success = false,
                Data      = null
            };

            if ( model.IsNull() )
            {
                result.Message = $"No defined primary key ORM metadata in '{typeof ( TModel ).FullName}' model.";
                return result;
            }

            var pKeyColumns = new List<PrimaryKeyInfo>();

            try
            {
                foreach ( var property in typeof ( TModel ).GetProperties() )
                {
                    TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( property );

                    if ( metadata.IsNull() )
                    {
                        continue;
                    }

                    if ( !metadata.IsPrimaryKey )
                    {
                        continue;
                    }

                    var primaryKey = new PrimaryKeyInfo
                    {
                        PrimaryKeyColumn = metadata,
                        DbType           = DbTypeConverter.Convert( property.PropertyType ),
                        Value            = property.GetValue( model )
                    };

                    pKeyColumns.Add( primaryKey );
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Check primary key ORM metadata of target model occur excepion. Target model type: {typeof ( TModel ).FullName}. {Environment.NewLine}{ex.ToString()}";
                return result;
            }

            result.Data      = pKeyColumns;
            result.Success = true;
            return result;
        }

        /// <summary>建立資料庫新增 INSERT INTO 的 SQL 語法
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="columns">新增欄位SQL字串</param>
        /// <param name="parameters">欄位參數SQL字串</param>
        /// <returns>建立資料庫新增語法結果</returns>
        internal static Result<string> CreateInsertIntoSql<TModel>( string columns, string parameters ) 
        {
            var result = new Result<string>() 
            {
                Success = false,
                Data      = null
            };

            string cacheId = $"{GetCacheId<TModel>()}:CreateInsertIntoSql";

            GetSomethingFromDataSource callback = () => 
            {
                MappingTableAttribute orm = null;

                try
                {
                    orm = Reflector.GetCustomAttribute<MappingTableAttribute>( typeof ( TModel ) );
                }
                catch ( Exception ex )
                {
                    throw new CreateSqlStatementException( $"Create insert SQL stmt occur exception. Target model type: {typeof ( TModel ).FullName}. {Environment.NewLine}{ex.ToString()}" );
                }

                if ( orm.IsNull() )
                {
                    throw new CreateSqlStatementException( $"Create insert SQL stmt fail. Can not reflect to get {nameof ( MappingTableAttribute )} metadata ORM attribute. Target model type: {typeof ( TModel ).FullName}." );
                }

                if ( orm.TableName.IsNullOrEmpty() )
                {
                    throw new CreateSqlStatementException( $"Create insert SQL stmt fail. Can not reflect to get {nameof ( MappingTableAttribute )} metadata ORM attribute. Target model type: {typeof ( TModel ).FullName}." );
                }

                return SqlStatementTemplate.InsertInto.FormatTo( orm.TableName, columns, parameters );
            };

            var g = MemoryCache.Get<string>( cacheId, callback, 60 );

            if ( !g.Success )
            {
                result.Data = callback() + "";
                MemoryCache.Put( cacheId, result.Data, 60 );
                return result;
            }

            if ( g.CacheData.IsNullOrEmpty() )
            {
                result.Message = $"Create insert SQL stmt fail. Target model type: {typeof ( TModel ).FullName}.";
                return result;
            }

            result.Data      = g.CacheData;
            result.Success = true;
            return result;
        }

        /// <summary>建立更新欄位 SQL 字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">要忽略更新的欄位集合</param>
        /// <returns>建立更新欄位 SQL 字串結果</returns>
        internal static CreateColumnsSqlResult CreateUpdateColumnsSql<TModel>( TModel model, string[] ignoreColumns = null )
        {
            var result = new CreateColumnsSqlResult() 
            {
                IsSuccess = false,
                Message   = null
            };

            var sbColumns = new StringBuilder();
            var sbWheres  = new StringBuilder();

            try
            {
                foreach ( var p in typeof ( TModel ).GetProperties() )
                {
                    TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( p );

                    if ( metadata.IsNull() )
                    {
                        continue;
                    }

                    if ( metadata.ColumnName.IsNullOrEmpty() )
                    {
                        continue;
                    }

                    if ( metadata.IsTimeStamp )
                    {
                        continue;
                    }

                    object value = null;

                    try
                    {
                        value = p.GetValue( model );
                    }
                    catch ( Exception )
                    {
                        continue;
                    }

                    // 20171027 Pony Bugfix: 只有在資料庫中的欄位不允許為 DBNull 的情況下，在執行 Update 指令時，才允許忽略更新該欄位。
                    // 如果 IsAllowNull = true 的情況下，有可能外界的程式就是要將資料庫中這個欄位更新為 DBNull
                    if ( value.IsNull() && !metadata.IsAllowNull )
                    {
                        continue;
                    }

                    if ( ignoreColumns.IsNotNullOrEmptyArray() && ignoreColumns.Contains( metadata.ColumnName ) )
                    {
                        continue;
                    }

                    int dbType = DbTypeConverter.Convert( p.PropertyType );
                    var pName  = $"@{metadata.ColumnName}";

                    if ( metadata.IsPrimaryKey )
                    {
                        string sqlOperator = metadata.SqlOperator.IsNullOrEmptyString( "=" );
                        sbWheres.Append( $"{SqlStatementTemplate.WhereCondition.FormatTo( metadata.ColumnName, sqlOperator, pName )} AND " );
                    }
                    else 
                    {
                        string strColumn = $" {metadata.ColumnName} = @{metadata.ColumnName}, ";
                        sbColumns.Append( strColumn );
                    }

                    result.SqlParameters.Add( pName, new DbParameterEntry() 
                    {
                        Name   = pName,
                        DbType = dbType,
                        Value  = value
                    } );
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Create update columns SQL stmt occur exception. Target model type: {typeof ( TModel ).FullName}.{Environment.NewLine}{ex.ToString()}";
                return result;
            }

            result.WhereConditionString = RemoveLastAndOperator( sbWheres.ToString() );
            result.ColumnsString        = RemoveLastComma( sbColumns.ToString() );
            result.IsSuccess            = true;
            return result;
        }

        // 20190524 Pony Bugfix: 這裡是不能有記憶體快取的! 因為每一次傳入的 where 條件 SQL 語法可能不同，一但有快取則可能造成 runtime 更新異常。
        /// <summary>建立資料庫更新 UDPATE 的 SQL 語法
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應資料模型泛型</typeparam>
        /// <param name="columns">新增欄位 SQL 字串</param>
        /// <param name="whereCondition">Where 條件字串</param>
        /// <returns>建立資料庫更新語法結果</returns>
        internal static Result<string> CreateUpdateSql<TModel>( string columns, string whereCondition )
        {
            var result = Result.Create<string>();

            MappingTableAttribute tableMetadata = null;

            try
            {
                tableMetadata = Reflector.GetCustomAttribute<MappingTableAttribute>( typeof ( TModel ) );
            }
            catch ( Exception ex )
            {
                throw new CreateSqlStatementException( $"Create update SQL stmt occur exception. Target model type: {typeof ( TModel ).FullName}. {Environment.NewLine}{ex.ToString()}" );
            }

            if ( tableMetadata.IsNull() )
            {
                throw new CreateSqlStatementException( $"Create update SQL stmt fail. Can not reflect to get {nameof ( MappingTableAttribute )} metadata ORM attribute. Target model type: {typeof ( TModel ).FullName}." );
            }

            if ( tableMetadata.TableName.IsNullOrEmpty() )
            {
                throw new CreateSqlStatementException( $"Create update SQL stmt fail. Reflect to get table name from {nameof ( MappingTableAttribute )} metadata ORM attribute retrive null or empty string. Target model type: {typeof ( TModel ).FullName}." );
            }

            result.Data    = SqlStatementTemplate.Update.FormatTo( tableMetadata.TableName, columns, whereCondition );
            result.Success = true;
            return result;
        }

        /// <summary>建立資料庫刪除 DELETE 的 SQL 語法
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="wheres">資料刪除 Where 條件集合</param>
        /// <returns>建立刪除欄位 SQL 字串結果</returns>
        internal static CreateDeleteSqlResult CreateDeleteSql<TModel>( TModel model, string[] wheres = null )
        {
            var result = new CreateDeleteSqlResult() 
            {
                IsSuccess = false,
                Sql       = null
            };

            var sbWhere = new StringBuilder();
            string strWhereCondition = null;

            object value = null;

            try
            {
                foreach ( var p in typeof ( TModel ).GetProperties() )
                {
                    TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( p );

                    if ( metadata.IsNull() || metadata.ColumnName.IsNullOrEmpty() )
                    {
                        continue;
                    }

                    try
                    {
                        value = p.GetValue( model );
                    }
                    catch ( Exception )
                    {
                        continue;
                    }

                    if ( value.IsNull() )
                    {
                        continue;
                    }

                    string sqlOperator = metadata.SqlOperator.IsNullOrEmptyString( "=" );

                    if ( wheres.IsNullOrEmptyArray() )
                    {
                        if ( !metadata.IsPrimaryKey )
                        {
                            continue;
                        }
                    
                        result.Wheres.Add( new PrimaryKeyInfo()
                        {
                            Name   = $"@{metadata.ColumnName}",
                            DbType = DbTypeConverter.Convert( p.PropertyType ),
                            Value  = value
                        } );

                        sbWhere.Append( $"{SqlStatementTemplate.WhereCondition.FormatTo( metadata.ColumnName, sqlOperator, $"@{metadata.ColumnName}" )} AND " );
                    }
                    else
                    {
                        var where = wheres.Where( h => h == metadata.ColumnName )?.FirstOrDefault();

                        if ( where.IsNullOrEmpty() )
                        {
                            continue;
                        }

                        result.Wheres.Add( new PrimaryKeyInfo()
                        {
                            Name   = $"@{metadata.ColumnName}",
                            DbType = DbTypeConverter.Convert( p.PropertyType ),
                            Value  = value
                        } );

                        sbWhere.Append( $"{SqlStatementTemplate.WhereCondition.FormatTo( where, sqlOperator, $"@{metadata.ColumnName}" )} AND " );
                    }
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Create delete SQL stmt occur exception. {Environment.NewLine}{ex.ToString()}";
                return result;
            }

            string tableName;

            try
            {
                MappingTableAttribute orm = Reflector.GetCustomAttribute<MappingTableAttribute>( typeof ( TModel ) );
                tableName = orm.TableName;
            }
            catch ( Exception ex )
            {
                result.Message = $"Create delete SQL stmt occur exception. {Environment.NewLine}{ex.ToString()}";
                return result;
            }

            if ( tableName.IsNullOrEmpty() )
            {
                result.Message = $"Create delete SQL stmt fail due to can not get table name from metadata. Reflect to get table name retrive null or empty string. Model type: {typeof ( TModel ).FullName}.";
                return result;
            }

            if ( sbWhere.Length > 0 )
            {
                strWhereCondition = RemoveLastAndOperator( sbWhere.ToString() );
            }

            if ( strWhereCondition.IsNullOrEmpty() )
            {
                result.Sql              = SqlStatementTemplate.Delete.FormatTo( tableName, string.Empty ).Replace( "WHERE", string.Empty );
                result.HasPKeyCondition = false;
                result.IsSuccess        = true;
                return result;
            }

            result.Sql              = SqlStatementTemplate.Delete.FormatTo( tableName, strWhereCondition );
            result.HasPKeyCondition = result.Wheres.Count > 0;
            result.IsSuccess        = true;
            return result;
        }

        /// <summary>建立資料庫刪除資料表所有資料的 DELETE SQL 語法
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應資料模型泛型</typeparam>
        /// <returns>建立刪除欄位 SQL 字串結果</returns>
        internal static CreateDeleteSqlResult CreateDeleteAllSql<TModel>() 
        {
            var result = new CreateDeleteSqlResult() 
            {
                IsSuccess = false,
                Sql       = null
            };

            string tableName = null;

            try
            {
                MappingTableAttribute orm = Reflector.GetCustomAttribute<MappingTableAttribute>( typeof ( TModel ) );
                tableName = orm.TableName;
            }
            catch ( Exception ex )
            {
                result.Message = $"Create delete all SQL stmt occur exception. {Environment.NewLine}{ex.ToString()}";
                return result;
            }

            if ( tableName.IsNullOrEmpty() )
            {
                result.Message = $"Create delete all SQL stmt fail due to can not get table name from metadata. Reflect to get table name retrive null or empty string. Model type: {typeof ( TModel ).FullName}.";
                return result;
            }

            result.Sql       = SqlStatementTemplate.DeleteAll.Format( table => tableName );
            result.IsSuccess = true;
            return result;
        }

        #endregion 宣告內部的靜態方法


        #region 宣告私有的靜態方法

        /// <summary>取得資料快取的識別代碼
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <returns>資料快取識別代碼</returns>
        private static string GetCacheId<TModel>() => $"LightweightORM:{typeof ( TModel ).FullName}";

        /// <summary>是否要忽略此欄位
        /// </summary>
        /// <param name="ignoreColumns">要被忽略的清單</param>
        /// <param name="target">要被忽略的欄位名稱</param>
        /// <returns>是否要忽略此欄位</returns>
        private static bool NeedIgnore( string[] ignoreColumns, string target ) => 
            ignoreColumns.IsNotNullOrEmptyArray() && ignoreColumns.Contains( target );

        /// <summary>移除來源字串中第一個半型逗號
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <returns>移除來源字串中第一個半型逗號的字串</returns>
        private static string RemoveFirstComma( string source ) => source.RemoveFirstAppeared( "," );

        /// <summary>移除來源字串中最後一個半型逗號
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <returns>移除最後一個半型逗號後的字串</returns>
        private static string RemoveLastComma( string source ) => source.RemoveLastAppeared( "," );

        /// <summary>移除來原字串中最後一個 AND 運算子
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <returns>移除來原字串中最後一個 AND 運算子後的字串</returns>
        private static string RemoveLastAndOperator( string source ) => source.RemoveLastAppeared( "AND" );

        #endregion 宣告私有的靜態方法
    }
}
