﻿using System;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>建立 SQL 敘述字串的程式異常
    /// </summary>
    public class CreateSqlStatementException : Exception
    {
        /// <summary>建立 SQL 敘述字串的程式異常建構子
        /// </summary>
        /// <param name="message">異常訊息</param>
        public CreateSqlStatementException( string message = "Zayni Framework Lightweight.ORM create SQL occur exception." ) : base( message )
        {
            // pass
        }
    }
}
