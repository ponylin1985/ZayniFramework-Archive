﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Common.ORM;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>SQL敘述字串建立者基底
    /// </summary>
    internal abstract class SqlStringBuilder
    {
        #region 宣告公開的抽象

        /// <summary>取得資料庫的空字串值
        /// </summary>
        /// <returns>資料庫的空字串值</returns>
        public abstract string GetDbEmptyString();

        #endregion 宣告公開的抽象


        #region 宣告保護的方法

        /// <summary>建立資料庫查詢欄位的SQL語法 (有TOP的語法)
        /// </summary>
        /// <param name="top">TOP筆數</param>
        /// <typeparam name="TModel">ORM 資料對應泛型</typeparam>
        /// <param name="sqlColumns">SQL Select 查詢語法字串</param>
        /// <param name="tSqlTopStmt">T-SQL的 TOP 敘述語法</param>
        /// <param name="plSqlTopStmt">PL/SQL的 TOP 敘述語法</param>
        /// <param name="mySqlTopStmt">MySQL 的 TOP 敘述語法</param>
        /// <param name="orderByColumns">SQL Order By 排序的欄位字串</param>
        /// <returns>建立結果</returns>
        protected Result<string> CreateSelectTopSql<TModel>( int top, string sqlColumns, string tSqlTopStmt, string plSqlTopStmt, string mySqlTopStmt, string orderByColumns )
        {
            var result = new Result<string>() 
            {
                Success = false,
                Data      = ""
            };

            tSqlTopStmt.IsNotNullOrEmpty(  t => tSqlTopStmt  = tSqlTopStmt.FormatTo( top ) );
            plSqlTopStmt.IsNotNullOrEmpty( p => plSqlTopStmt = plSqlTopStmt.FormatTo( top ) );
            mySqlTopStmt.IsNotNullOrEmpty( m => mySqlTopStmt = mySqlTopStmt.FormatTo( top ) );

            string cacheId = "{0}:BuilderCreateSelectSql".FormatTo( GetCacheId<TModel>() );

            GetSomethingFromDataSource callback = () => 
            {
                MappingTableAttribute orm = null;

                try
                {
                    orm = Reflector.GetCustomAttribute<MappingTableAttribute>( typeof ( TModel ) );
                }
                catch ( Exception ex )
                {
                    throw new CreateSqlStatementException( "建立資料庫查詢欄位的SQL語法發生異常: {0}".FormatTo( ex.Message ) );
                }

                if ( orm.IsNull() )
                {
                    throw new CreateSqlStatementException( "建立資料庫查詢欄位的SQL語法發生異常: 反射取得資料模型的MappingTableAttribute中介資料為Null值。" );
                }

                if ( orm.TableName.IsNullOrEmpty() )
                {
                    throw new CreateSqlStatementException( "建立資料庫查詢欄位的SQL語法發生異常: 反射取得資料模型中對應資料庫的資料表名稱為空字串或Null。" );
                }

                return orm;
            };

            MappingTableAttribute metadata = null;

            var g = MemoryCache.Get<MappingTableAttribute>( cacheId, callback, 60 );

            if ( !g.Success )
            {
                metadata = (MappingTableAttribute)callback();
                MemoryCache.Put( cacheId, metadata, 60 );
                return result;
            }
            else 
            {
                metadata = g.CacheData;
            }

            if ( metadata.IsNull() )
            {
                result.Message = "建立資料庫查詢欄位的SQL語法發生異常: 反射取得資料模型的 MappingTableAttribute 中介資料為Null值。";
                return result;
            }

            result.Data = orderByColumns.IsNullOrEmpty() ? 
                SqlStatementTemplate.SelectTop.FormatTo( tSqlTopStmt, sqlColumns, metadata.TableName, plSqlTopStmt ) :
                SqlStatementTemplate.SelectTop.FormatTo( tSqlTopStmt, sqlColumns, metadata.TableName, plSqlTopStmt ) + " ORDER BY {0} ".FormatTo( orderByColumns );
            
            result.Success = true;
            return result;
        }

        /// <summary>建立新增欄位 SQL 字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)
        /// </summary>
        /// <typeparam name="TModel">ORM 資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <returns>建立新增欄位 SQL 字串結果</returns>
        protected CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>( TModel model )
        {
            var result = new CreateColumnsSqlResult() 
            {
                IsSuccess = false,
                Message   = null
            };

            var sbColumns    = new StringBuilder();
            var sbParameters = new StringBuilder();

            try
            {
                foreach ( var property in typeof ( TModel ).GetProperties() )
                {
                    TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( property );

                    if ( metadata.IsNull() )
                    {
                        continue;
                    }

                    if ( metadata.ColumnName.IsNullOrEmpty() )
                    {
                        continue;
                    }

                    if ( metadata.IsTimeStamp )
                    {
                        continue;
                    }

                    string strColumn    = $" {metadata.ColumnName} , ";
                    string strParameter = $" @{metadata.ColumnName} , ";

                    sbColumns.Append( strColumn );
                    sbParameters.Append( strParameter );

                    object value = null;

                    try
                    {
                        value = property.GetValue( model ) ?? metadata.DefaultValue?.ToString( CultureInfo.InvariantCulture );
                    }
                    catch ( Exception )
                    {
                        continue;
                    }

                    int    dbType = DbTypeConverter.Convert( property.PropertyType );
                    string pName  = $"@{metadata.ColumnName}";

                    result.SqlParameters.Add( pName, new DbParameterEntry() 
                    {
                        Name   = pName,
                        DbType = dbType,
                        Value  = value
                    } );
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"{GetLogTraceName( nameof ( CreateInsertIntoColumnsSql ) )}, create insert SQL stmt occur exception: {ex.ToString()}";
                return result;
            }

            result.ColumnsString    = RemoveLastComma( sbColumns.ToString() );
            result.ParametersString = RemoveLastComma( sbParameters.ToString() );
            result.IsSuccess        = true;
            return result;
        }

        /// <summary>建立新增欄位SQL字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">忽略新增的欄位</param>
        /// <returns>建立新增欄位SQL字串結果</returns>
        protected CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>( TModel model, params string[] ignoreColumns )
        {
            var result = new CreateColumnsSqlResult() 
            {
                IsSuccess = false,
                Message   = null
            };

            var sbColumns    = new StringBuilder();
            var sbParameters = new StringBuilder();

            try
            {
                foreach ( var property in typeof ( TModel ).GetProperties() )
                {
                    TableColumnAttribute metadata = Reflector.GetCustomAttribute<TableColumnAttribute>( property );

                    if ( metadata.IsNull() )
                    {
                        continue;
                    }

                    if ( metadata.ColumnName.IsNullOrEmpty() )
                    {
                        continue;
                    }

                    if ( metadata.IsTimeStamp )
                    {
                        continue;
                    }

                    if ( NeedIgnore( ignoreColumns, metadata.ColumnName ) )
                    {
                        continue;
                    }

                    string strColumn    = $" {metadata.ColumnName} , ";
                    string strParameter = $" @{metadata.ColumnName} , ";

                    sbColumns.Append( strColumn );
                    sbParameters.Append( strParameter );

                    object value = null;

                    try
                    {
                        value = property.GetValue( model ) ?? metadata.DefaultValue?.ToString( CultureInfo.InvariantCulture );
                    }
                    catch ( Exception )
                    {
                        continue;
                    }

                    int    dbType = DbTypeConverter.Convert( property.PropertyType );
                    string pName  = $"@{metadata.ColumnName}";

                    result.SqlParameters.Add( pName, new DbParameterEntry() 
                    {
                        Name   = pName,
                        DbType = dbType,
                        Value  = value
                    } );
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"{GetLogTraceName( nameof ( CreateInsertIntoColumnsSql ) )}, create insert SQL stmt occur exception: {ex.ToString()}";
                return result;
            }

            result.ColumnsString    = RemoveLastComma( sbColumns.ToString() );
            result.ParametersString = RemoveLastComma( sbParameters.ToString() );
            result.IsSuccess        = true;
            return result;
        }

        #endregion 宣告保護的方法


        #region 宣告私有的方法

        /// <summary>取得資料快取ID
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <returns>資料快取ID</returns>
        private string GetCacheId<TModel>() => $"LightweightORM:{typeof ( TModel ).FullName}";

        /// <summary>移除來源字串中第一個半型逗號
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <returns>移除來源字串中第一個半型逗號的字串</returns>
        private string RemoveFirstComma( string source ) => source.RemoveFirstAppeared( "," );

        /// <summary>移除來源字串中最後一個半型逗號
        /// </summary>
        /// <param name="source">來源字串</param>
        /// <returns>移除最後一個半型逗號後的字串</returns>
        private string RemoveLastComma( string source ) => source.RemoveLastAppeared( "," );

        /// <summary>是否要忽略此欄位
        /// </summary>
        /// <param name="ignoreColumns">要被忽略的清單</param>
        /// <param name="target">要被忽略的欄位名稱</param>
        /// <returns>是否要忽略此欄位</returns>
        private bool NeedIgnore( string[] ignoreColumns, string target ) => ignoreColumns.IsNotNullOrEmptyArray() && ignoreColumns.Contains( target );

        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( SqlStringBuilder )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
