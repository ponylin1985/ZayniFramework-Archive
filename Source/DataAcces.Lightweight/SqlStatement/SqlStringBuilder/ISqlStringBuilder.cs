﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>SQL敘述字串建立者介面
    /// </summary>
    internal interface ISqlStringBuilder
    {
        /// <summary>建立資料庫查詢欄位的SQL語法 (有TOP的語法)
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="top">TOP筆數</param>
        /// <param name="sqlColumns">SQL Select查詢語法字串</param>
        /// <param name="orderByColumns">SQL Order By 排序的欄位字串</param>
        /// <returns>建立結果</returns>
        Result<string> CreateSelectTopSql<TModel>( int top, string sqlColumns, string orderByColumns );

        /// <summary>建立SQL查詢語法完整的Where敘述
        /// </summary>
        /// <param name="whereCondition">SQL Where條件的字串</param>
        /// <returns>建立SQL字串結果</returns>
        Result<string> CreateWhereSql( string whereCondition );

        /// <summary>建立新增欄位SQL字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <returns>建立新增欄位SQL字串結果</returns>
        CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>( TModel model );

        /// <summary>建立新增欄位SQL字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">忽略新增的欄位</param>
        /// <returns>建立新增欄位SQL字串結果</returns>
        CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>( TModel model, params string[] ignoreColumns );
    }
}
