﻿using ZayniFramework.Common;


namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>MSSQL 資料庫 T-SQL 敘述字串建立者
    /// </summary>
    internal class TSQLStringBuilder : SqlStringBuilder, ISqlStringBuilder
    {
        #region 實作 ISqlStringBuilder 介面

        /// <summary>建立資料庫查詢欄位的 SQL 語法 (有TOP的語法)<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應泛型</typeparam>
        /// <param name="top">TOP 筆數 (只查詢前 N 筆的資料筆數)</param>
        /// <param name="sqlColumns">SQL Select查詢語法字串</param>
        /// <param name="orderByColumns">SQL Order By 排序的欄位字串</param>
        /// <returns>建立結果</returns>
        public Result<string> CreateSelectTopSql<TModel>( int top, string sqlColumns, string orderByColumns )
        {
            return base.CreateSelectTopSql<TModel>( top, sqlColumns, tSqlTopStmt: " TOP ( {0} ) ", plSqlTopStmt: string.Empty, mySqlTopStmt: string.Empty, orderByColumns: orderByColumns );
        }

        /// <summary>建立 SQL 查詢語法完整的 Where 敘述<para/>
        /// </summary>
        /// <param name="whereCondition">SQL Where 條件的字串</param>
        /// <returns>建立 SQL 字串結果</returns>
        public Result<string> CreateWhereSql( string whereCondition )
        {
            return SqlStatementMaker.CreateWhereSql( whereCondition );
        }

        /// <summary>建立新增欄位SQL字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <returns>建立新增欄位SQL字串結果</returns>
        public new CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>( TModel model )
        {
            return base.CreateInsertIntoColumnsSql<TModel>( model );
        }

        /// <summary>建立新增欄位SQL字串 (包含建立新增欄位、欄位參數、參數資料庫型別、取得參數值)<para/>
        /// </summary>
        /// <typeparam name="TModel">ORM資料對應資料模型泛型</typeparam>
        /// <param name="model">資料模型</param>
        /// <param name="ignoreColumns">忽略新增的欄位</param>
        /// <returns>建立新增欄位SQL字串結果</returns>
        public new CreateColumnsSqlResult CreateInsertIntoColumnsSql<TModel>( TModel model, params string[] ignoreColumns )
        {
            return base.CreateInsertIntoColumnsSql<TModel>( model, ignoreColumns );
        }

        #endregion 實作 ISqlStringBuilder 介面


        #region 實作 SqlStringBuilder 抽象

        /// <summary>取得 MSSQL 資料庫的空字串值
        /// </summary>
        /// <returns>MSSQL 資料庫的空字串值</returns>
        public override string GetDbEmptyString() => string.Empty;

        #endregion 實作 SqlStringBuilder 抽象
    }
}
