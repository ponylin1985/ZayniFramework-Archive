﻿namespace ZayniFramework.DataAccess.Lightweight
{
    /// <summary>內部資料存取類別
    /// </summary>
    internal class Dao : BaseDataAccess
    {
        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        internal Dao( string dbName ) : base( dbName )
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <param name="zayniConnectionString">ZayniFramework 的資料庫連線字串 (若傳入為Null或空字串，SQL Logging 功能將停用)</param>
        internal Dao( string dbName, string connectionString, string zayniConnectionString = "" ) : base( dbName, connectionString, zayniConnectionString )
        {
            // pass
        }
    }
}
