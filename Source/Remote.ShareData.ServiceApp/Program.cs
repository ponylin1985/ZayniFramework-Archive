﻿using System;
using System.Threading.Tasks;
using ZayniFramework.Caching.RemoteCacheService;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.TelnetService;


namespace ZayniFramework.Remote.ShareData.Service
{
    /// <summary>主程式
    /// </summary>
    sealed class Program
    {
        #region 宣告私有的欄位

        /// <summary>遠端快取的 Middle ServiceHost 自我裝載服務
        /// </summary>
        /// <returns></returns>
        private static readonly RemoteCacheService _service = new RemoteCacheService();

        #endregion 宣告私有的欄位


        #region 宣告主要進入點方法

        /// <summary>程式進入點主方法
        /// </summary>
        /// <param name="args"></param>
        static void Main_Sync( string[] args )
        {
            var r = StartServiceHost();
            if ( !r.Success ) return;

            ConsoleLogger.WriteLine( $"Zayni Framework Remote Share Data Service start successfully." );

            var commandContainer = RegisterCommands();
            Check.IsTrue( bool.TryParse( ConfigManagement.AppSettings[ "EnableTelnetCommandService" ], out bool enableTelnetCmdService ) ? enableTelnetCmdService : true, () => TelnetCommandService.StartDefaultTelnetService( commandContainer ) );
            Check.IsTrue( bool.TryParse( ConfigManagement.AppSettings[ "EnableConsoleCommandService" ], out bool enableConsoleCmdService ) ? enableConsoleCmdService : false, () => ConsoleCommandService.StartDefaultConsoleService( commandContainer ) );
        }

        /// <summary>非同步作業的程式進入點主方法
        /// </summary>
        /// <param name="args"></param>
        static async Task Main( string[] args )
        {
            var r = StartServiceHost();
            if ( !r.Success ) return;

            await StartServiceHostAsync();
            if ( !r.Success ) return;

            await ConsoleLogger.WriteLineAsync( $"Zayni Framework Remote Share Data Service start successfully." );

            var asyncCommandContainer = await RegisterCommandsAsync();

            if ( bool.TryParse( ConfigManagement.AppSettings[ "EnableTelnetCommandService" ], out bool enableTelnetCmdService ) )
                TelnetCommandService.StartDefaultAsyncTelnetService( asyncCommandContainer );

            if ( bool.TryParse( ConfigManagement.AppSettings[ "EnableConsoleCommandService" ], out bool enableConsoleCmdService ) )
                await ConsoleCommandService.StartDefaultConsoleServiceAsync( asyncCommandContainer );
        }

        #endregion 宣告主要進入點方法


        #region 宣告私有的方法

        /// <summary>啟動自我裝載的 Middle ServiceHost 快取服務
        /// </summary>
        /// <returns>啟動結果</returns>
        private static IResult StartServiceHost() 
        {
            var result = Result.Create();
            
            var r = _service.StartService();

            if ( !r.Success )
            {
                result.Message = $"Start remote cache service error. {Environment.NewLine}{r.Message}";
                ConsoleLogger.LogError( $"{result.Message}{Environment.NewLine}" );
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>啟動自我裝載的 Middle ServiceHost 快取服務
        /// </summary>
        /// <param name="extensionModules">是否註冊並且啟動 Middle.Service Extension Modules 的 Service Host，預設不啟動。</param>
        /// <returns>啟動結果</returns>
        private static async Task<IResult> StartServiceHostAsync( bool extensionModules = false ) 
        {
            var result = Result.Create();
            
            var r = await _service.StartServiceAsync( extensionModules );

            if ( !r.Success )
            {
                result.Message = $"Start remote cache service error. {Environment.NewLine}{r.Message}";
                await ConsoleLogger.LogErrorAsync( $"{result.Message}{Environment.NewLine}" );
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>註冊命令處理物件
        /// </summary>
        /// <returns>命令容器池</returns>
        private static CommandContainer RegisterCommands() 
        {
            var commandContainer = new CommandContainer();
            commandContainer.RegisterCommand<GetConsoleCommand>( "get", commandText => commandText.StartsWith( "get" ) );
            commandContainer.RegisterCommand<GetHashPropConsoleCommand>( "get hash prop", commandText => commandText.StartsWith( "get hash prop" ) );
            commandContainer.RegisterCommand<ListKeyConsoleCommand>( "list key", commandText => "list key" == commandText );
            commandContainer.RegisterCommand<QuitConsoleCommand>( "quit", commandText => "quit" == commandText );
            commandContainer.RegisterCommand<HelpConsoleCommand>( "help", commandText => "help" == commandText );
            commandContainer.RegisterCommand<ExitConsoleCommand>( "exit", commandText => "exit" == commandText );
            commandContainer.RegisterCommand<ClearConsoleCommand>( "cls", commandText => "cls" == commandText );
            commandContainer.RegisterUnknowCommand<UnknowRemoteCommand>();
            return commandContainer;
        }

        /// <summary>註冊命令處理物件
        /// </summary>
        /// <returns>命令容器池</returns>
        private static async Task<CommandContainer> RegisterCommandsAsync() 
        {
            var commandContainer = new CommandContainer();
            await commandContainer.RegisterCommandAsync<GetConsoleCommandAsync>( "get", commandText => commandText.StartsWith( "get" ) );
            await commandContainer.RegisterCommandAsync<GetHashPropConsoleCommandAsync>( "get hash prop", commandText => commandText.StartsWith( "get hash prop" ) );
            await commandContainer.RegisterCommandAsync<ListKeyConsoleCommandAsync>( "list key", commandText => "list key" == commandText );
            await commandContainer.RegisterCommandAsync<QuitConsoleCommandAsync>( "quit", commandText => "quit" == commandText );
            await commandContainer.RegisterCommandAsync<HelpConsoleCommandAsync>( "help", commandText => "help" == commandText );
            await commandContainer.RegisterCommandAsync<ExitConsoleCommandAsync>( "exit", commandText => "exit" == commandText );
            await commandContainer.RegisterCommandAsync<ClearConsoleCommandAsync>( "cls", commandText => "cls" == commandText );
            await commandContainer.RegisterUnknowCommandAsync<UnknowRemoteCommandAsync>();
            return commandContainer;
        }

        #endregion 宣告私有的方法
    }
}
