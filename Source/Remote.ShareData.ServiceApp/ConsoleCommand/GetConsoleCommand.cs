﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace ZayniFramework.Remote.ShareData.Service
{
    /// <summary>Get 主控台指令處理器 (原始指令: get {cacheId})
    /// </summary>
    internal sealed class GetConsoleCommand : RemoteCommand
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            string cacheKey = CommandText?.RemoveFirstAppeared( "get" )?.Trim();

            if ( !CacheManager.Contains( cacheKey ) )
            {
                Stdout( $"Cache key not found. Key: {cacheKey}." );
                Result.Success = true;
                return Result;
            }

            var g = CacheManager.Get( cacheKey );

            if ( !g.Success )
            {
                StdoutErr( $"Get cache value occur error. {Environment.NewLine}{g.Message}" );
                return Result;
            }

            StdoutLn();
            Stdout( JsonConvertUtil.Serialize( g.CacheData ) );
            StdoutLn();

            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }

    /// <summary>Get 主控台指令處理器 (原始指令: get {cacheId})
    /// </summary>
    internal sealed class GetConsoleCommandAsync : RemoteCommandAsync
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            string cacheKey = CommandText?.RemoveFirstAppeared( "get" )?.Trim();

            if ( !await CacheManager.ContainsAsync( cacheKey ) )
            {
                await StdoutAsync( $"Cache key not found. Key: {cacheKey}." );
                Result.Success = true;
                return Result;
            }

            var g = await CacheManager.GetAsync( cacheKey );

            if ( !g.Success )
            {
                await StdoutErrAsync( $"Get cache value occur error. {Environment.NewLine}{g.Message}" );
                return Result;
            }

            if ( g.CacheData.IsNull() )
            {
                await StdoutAsync( $"No cache data found." );
                Result.Success = true;
                return Result;
            }

            await StdoutLnAsync();

            if ( g.CacheData is JsonElement payload )
            {
                await StdoutAsync( $"{Environment.NewLine}{payload.ToString()}", ConsoleColor.Yellow );
            }
            else
            {
                await StdoutAsync( $"{Environment.NewLine}{JsonConvertUtil.Serialize( g )}", ConsoleColor.Yellow );
            }

            await StdoutLnAsync();

            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }
}
