﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;


namespace ZayniFramework.Remote.ShareData.Service
{
    /// <summary>Help 主控台指令處理器 (原始指令: help)
    /// </summary>
    internal sealed class HelpConsoleCommand : RemoteCommand
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            StdoutLn();
            Stdout( "Command                             Descriptions" );
            Stdout( "================================================" );
            Stdout( "list key                            List all keys in this cache service." );
            Stdout( "get {cacheId}                       Get cache value with specific cacheId." );
            Stdout( "get hash prop {cacheId}             Get HashProperty value with specific cacheId." );
            Stdout( "get hash prop {cacheId} {subkey}    Get HashProperty value with specific cacheId and subkey." );
            Stdout( "quit                                Close the telnet connection between ShareData Cache Service." );
            Stdout( "exit                                Shutdown the ShareData Cache Service and exit program." );
            StdoutLn();

            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }

    /// <summary>Help 主控台指令處理器 (原始指令: help)
    /// </summary>
    internal sealed class HelpConsoleCommandAsync : RemoteCommandAsync
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            await StdoutLnAsync();
            await StdoutAsync( "Command                             Descriptions" );
            await StdoutAsync( "================================================" );
            await StdoutAsync( "list key                            List all keys in this cache service." );
            await StdoutAsync( "get {cacheId}                       Get cache value with specific cacheId." );
            await StdoutAsync( "get hash prop {cacheId}             Get HashProperty value with specific cacheId." );
            await StdoutAsync( "get hash prop {cacheId} {subkey}    Get HashProperty value with specific cacheId and subkey." );
            await StdoutAsync( "quit                                Close the telnet connection between ShareData Cache Service." );
            await StdoutAsync( "exit                                Shutdown the ShareData Cache Service and exit program." );
            await StdoutLnAsync();

            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }
}
