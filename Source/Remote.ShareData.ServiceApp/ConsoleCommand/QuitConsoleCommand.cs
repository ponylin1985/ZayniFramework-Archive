﻿using System.IO;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;


namespace ZayniFramework.Remote.ShareData.Service
{
    /// <summary>Quit 主控台指令 (原始指令: quit)
    /// </summary>
    internal class QuitConsoleCommand : RemoteCommand
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            if ( TelnetClient.IsNull() )
            {
                return Result;
            }

            StreamWriter.BaseStream.Close();
            StreamWriter.BaseStream.Dispose();
            StreamReader.Close();
            StreamWriter.Close();

            StreamReader.Dispose();
            StreamWriter.Dispose();

            TelnetClient.Close();
            TelnetClient.Dispose();

            TelnetClient = null;
            StreamWriter = null;
            StreamReader = null;

            Stdout( $"Telnet client disconnect success." );
            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }

    /// <summary>Quit 主控台指令 (原始指令: quit)
    /// </summary>
    internal class QuitConsoleCommandAsync : RemoteCommandAsync
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            if ( TelnetClient.IsNull() )
            {
                return Result;
            }

            await StdoutAsync( $"Disconnenting now..." );

            StreamWriter.BaseStream.Close();
            StreamWriter.BaseStream.Dispose();
            StreamReader.Close();
            StreamWriter.Close();

            StreamReader.Dispose();
            StreamWriter.Dispose();

            TelnetClient.Close();
            TelnetClient.Dispose();

            TelnetClient = null;
            StreamWriter = null;
            StreamReader = null;

            await StdoutAsync( $"Telnet client disconnect success." );
            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }
}
