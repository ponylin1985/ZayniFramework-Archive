﻿using System;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace ZayniFramework.Remote.ShareData.Service
{
    /// <summary>Get Hash Prop 主控台指令處理器 (原始指令: get hash prop {cacheId})
    /// </summary>
    internal sealed class GetHashPropConsoleCommand : RemoteCommand
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameterCollection">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameterCollection )
        {
            string strParameters = CommandText.RemoveFirstAppeared( "get hash prop" )?.Trim();
            var parameters = strParameters.Split( ' ' );

            if ( parameters.IsNullOrEmptyArray() )
            {
                Stdout( $"Invalid get hash prop parameters." );
                return Result;
            }

            string cacheId = parameters.FirstOrDefault();
            string subkey  = 2 == parameters.Length ? subkey = parameters[ 1 ] : null;

            dynamic gp = null;

            if ( subkey.IsNullOrEmpty() )
            {
                gp = CacheManager.GetHashProperties( cacheId );
            }
            else
            {
                gp = CacheManager.GetHashProperty( cacheId, subkey );
            }

            if ( !gp.Success )
            {
                StdoutErr( $"Get hash property cache value occur error. {Environment.NewLine}{gp.Message}" );
                return Result;
            }

            string strSubkey = subkey.IsNullOrEmpty() ? "." : $", Subkey: {subkey}.";

            StdoutLn();
            Stdout( $"Get hash property cache value success. CacheId: {cacheId}{strSubkey}" );
            Stdout( JsonConvertUtil.Serialize( gp ) );
            StdoutLn();

            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }

    /// <summary>Get Hash Prop 主控台指令處理器 (原始指令: get hash prop {cacheId})
    /// </summary>
    internal sealed class GetHashPropConsoleCommandAsync : RemoteCommandAsync
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameterCollection">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameterCollection )
        {
            string strParameters = CommandText.RemoveFirstAppeared( "get hash prop" )?.Trim();
            var parameters = strParameters.Split( ' ' );

            if ( parameters.IsNullOrEmptyArray() )
            {
                await StdoutAsync( $"Invalid get hash prop parameters." );
                return Result;
            }

            string cacheId = parameters.FirstOrDefault();
            string subkey  = 2 == parameters.Length ? subkey = parameters[ 1 ] : null;

            dynamic gp = null;

            if ( subkey.IsNullOrEmpty() )
            {
                gp = await CacheManager.GetHashPropertiesAsync( cacheId );
            }
            else
            {
                gp = await CacheManager.GetHashPropertyAsync( cacheId, subkey );
            }

            if ( !gp.Success )
            {
                await StdoutErrAsync( $"Get hash property cache value occur error. {Environment.NewLine}{gp.Message}" );
                return Result;
            }

            string strSubkey = subkey.IsNullOrEmpty() ? "." : $", Subkey: {subkey}.";

            await StdoutLnAsync();
            await StdoutAsync( $"Get hash property cache value success. CacheId: {cacheId}{strSubkey}" );
            await StdoutAsync( JsonConvertUtil.Serialize( gp ) );
            await StdoutLnAsync();

            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }
}
