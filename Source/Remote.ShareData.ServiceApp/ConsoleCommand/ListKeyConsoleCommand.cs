﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;


namespace ZayniFramework.Remote.ShareData.Service
{
    /// <summary>List Key 主控台指令處理器 (原始指令: list key)
    /// </summary>
    internal sealed class ListKeyConsoleCommand : RemoteCommand
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            var g = CacheManager.GetCacheKeys();

            if ( !g.Success )
            {
                Stdout( $"List all cache keys occur error. {g.Message}" );
                return Result;
            }

            List<string> keys = g.Data;

            if ( keys.IsNullOrEmptyList() )
            {
                Stdout( $"No any cache keys in cache service." );
                Result.Success = true;
                return Result;
            }

            StdoutLn();
            Stdout( $"Total key count: {keys.Count}." );
            keys.OrderBy( y => y ).ToList().ForEach( k => Stdout( k ) );
            StdoutLn();

            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }

    /// <summary>List Key 主控台指令處理器 (原始指令: list key)
    /// </summary>
    internal sealed class ListKeyConsoleCommandAsync : RemoteCommandAsync
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            var g = CacheManager.GetCacheKeys();

            if ( !g.Success )
            {
                await StdoutAsync( $"List all cache keys occur error. {g.Message}" );
                return Result;
            }

            List<string> keys = g.Data;

            if ( keys.IsNullOrEmptyList() )
            {
                await StdoutAsync( $"No any cache keys in cache service." );
                Result.Success = true;
                return Result;
            }

            await StdoutLnAsync();
            await StdoutAsync( $"Total key count: {keys.Count}." );

            foreach ( var key in keys.OrderBy( y => y ) )
            {
                await StdoutAsync( key );
            }

            await StdoutLnAsync();

            Result.Success = true;
            return Result;
        }

        #endregion 宣告公開的方法
    }
}
