﻿using System;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Cryptography
{
    /// <summary>基底雜湊加密元件
    /// </summary>
    public abstract class BaseHashEncryptor : IHashEncryptor
    {
        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static BaseHashEncryptor()
        {
            ConfigReader.LoadHashNeedSalt();
        }

        #endregion 宣告靜態建構子


        #region 宣告抽象方法

        /// <summary>對來源明文字串進行雜湊加密 (如果雜湊過後的密文需要Salt，此參數一定要傳入，且不可以為空字串)
        /// </summary>
        /// <param name="source">來源明文字串</param>
        /// <param name="saltKey">加鹽唯一字串(如果雜湊過後的密文需要Salt，此參數一定要傳入，且不可以為空字串)</param>
        /// <returns>雜湊加密過後的密文字串</returns>
        public abstract string HashEncrypt( string source, string saltKey );

        #endregion 宣告抽象方法


        #region 宣告公開的屬性

        /// <summary>執行加解密過程中發生例外的處理回呼委派
        /// </summary>
        public ExceptionHandler ExceptionCallback
        {
            get;
            set;
        }

        #endregion 宣告公開的屬性


        #region 宣告保護的委派

        /// <summary>雜湊處理委派
        /// </summary>
        /// <param name="source">未加密的明文二進位資料</param>
        /// <returns>雜湊過的二進位資料</returns>
        protected delegate byte[] HashHandler( byte[] source );

        #endregion 宣告保護的委派


        #region 宣告保護的方法

        /// <summary>轉換Binary陣列元素轉換為對應的字串
        /// </summary>
        /// <param name="target">目標Byte陣列</param>
        /// <returns>對應的字串</returns>
        protected string ByteArrayConvertToString( byte[] target ) 
        {
            var sb = new StringBuilder( "" );
            
            for ( int i = 0; i < target.Length; i++ ) 
            {
                sb.Append( target[ i ].ToString( "X2" ) );
            }

            return sb.ToString();
        }

        /// <summary>建立雜湊加密的Salt
        /// </summary>
        /// <param name="saltKey">Salt來源字串(唯一值)</param>
        /// <returns>Salt字串</returns>
        protected string CreateSalt( string saltKey )
        {
            byte[] userBytes = ASCIIEncoding.ASCII.GetBytes( saltKey );

            string salt;
            
            long XORED = 0x00;

            foreach ( int x in userBytes ) 
            {
                XORED = XORED ^ x;
            }

            Random rand = new Random( Convert.ToInt32( XORED ) );

            salt  = rand.Next().ToString();
            salt += rand.Next().ToString();
            salt += rand.Next().ToString();
            salt += rand.Next().ToString();

            return salt;
        }

        /// <summary>對來源明文字串進行雜湊加密 (如果雜湊過後的密文需要Salt，此參數一定要傳入，且不可以為空字串)
        /// </summary>
        /// <param name="source">來源明文字串</param>
        /// <param name="saltKey">加鹽唯一字串(如果雜湊過後的密文需要Salt，此參數一定要傳入，且不可以為空字串)</param>
        /// <param name="handler">雜湊處理委派</param>
        /// <returns>雜湊加密過後的密文字串</returns>
        protected string ComputeHash( string source, string saltKey, HashHandler handler )
        {
            string result = "";
            string salt   = "";

            if ( CryptographySettings.HashNeedSalt )
            {
                salt   = CreateSalt( saltKey );
                source = string.Concat( salt, source );
            }

            var encoder = new UTF8Encoding();

            try
            {
                byte[] bytes = handler( encoder.GetBytes( source ) );
                
                result = CryptographySettings.HashNeedSalt ? 
                         string.Concat( ByteArrayConvertToString( bytes ), salt ) :
                         ByteArrayConvertToString( bytes );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "BaseHashEncryptor對目標字串加密發生程式異常: {0}".FormatTo( ex.ToString() ) );
                HandlerExecuter.DoExceptionHandler( ExceptionCallback, ex );
            }
            
            return result;
        }

        #endregion 宣告保護的方法
    }
}
