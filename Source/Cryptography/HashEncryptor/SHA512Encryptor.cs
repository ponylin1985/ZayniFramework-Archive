﻿using System;
using System.Security.Cryptography;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Cryptography
{
    /// <summary>SHA512雜湊加密元件
    /// </summary>
    public class SHA512Encryptor : BaseHashEncryptor
    {
        #region 實作IHashCryptographyer介面方法

        /// <summary>對來源明文字串進行雜湊加密 (如果雜湊過後的密文需要Salt，此參數一定要傳入，且不可以為空字串)
        /// </summary>
        /// <param name="source">來源明文字串</param>
        /// <param name="saltKey">加鹽唯一字串(如果雜湊過後的密文需要Salt，此參數一定要傳入，且不可以為空字串)</param>
        /// <returns>雜湊加密過後的密文字串</returns>
        public override string HashEncrypt( string source, string saltKey )
        {
            var    hasher = new SHA512Managed();
            string result = base.ComputeHash( source, saltKey, hasher.ComputeHash );
            return result;
        }

        #endregion 實作IHashCryptographyer介面方法
    }
}
