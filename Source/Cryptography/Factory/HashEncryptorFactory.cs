﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Cryptography
{
    /// <summary>雜湊加密元件工廠
    /// </summary>
    public class HashEncryptorFactory
    {
        /// <summary>建立雜湊加密元件
        /// </summary>
        /// <param name="type">雜湊演算法種類</param>
        /// <returns>雜湊加密元件</returns>
        public static IHashEncryptor Create( string type = "sha256" )
        {
            IHashEncryptor result = null;

            switch ( type.ToLower() )
            {
                case "sha256":
                    result = new SHA256Encryptor();
                    break;

                case "sha384":
                    result = new SHA384Encryptor();
                    break;

                case "sha512":
                    result = new SHA512Encryptor();
                    break;

                default:
                    result = new SHA256Encryptor();
                    break;
            }

            return result;
        }
    }
}
