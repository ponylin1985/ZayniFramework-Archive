﻿using System;
using System.IO;
using System.Security.Cryptography;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Cryptography
{
    /// <summary>AES對稱式加解密演算法元件 (密碼以外的資料，若需要加密處理，建議使用此AES對稱式加解密演算法，加密強度與效能皆比較好。)
    /// </summary>
    /// <remarks>
    /// 1. 密碼以外的資料，若需要加密處理，建議使用此AES對稱式加解密演算法，加密強度與效能皆比較好。
    /// 2. AES演算法的Block Size只能支援為128 bit。
    /// 3. AES演算法的Key Size可以支援: 128 或 256 bit。
    /// 參考文章如下:
    /// https://social.msdn.microsoft.com/Forums/vstudio/en-US/0d22648f-149e-4aea-918e-c3e86a30b23a/keysize-and-block-size-of-aes-128-192-and-256-bits
    /// http://stackoverflow.com/questions/17171893/algorithm-is-the-rijndaelmanaged-class-in-c-sharp-equivalent-to-aes-encryption
    /// https://msdn.microsoft.com/en-us/library/system.security.cryptography.aescryptoserviceprovider.keysize(v=vs.110).aspx
    /// </remarks>
    public class AesEncryptor : BaseEncryptor
    {
        #region 實作ICryptographyer介面方法

        /// <summary>對目標明文字串進行加密
        /// </summary>
        /// <param name="targetText">目標明文字串</param>
        /// <returns>加密過後的秘文字串</returns>
        public override string Encrypt( string targetText )
        {
            string result = "";

            try
            {
                using ( AesCryptoServiceProvider aesProvider = GetProvider() )
                {
                    // 20151012 Bugfix by Pony: 這邊在把明文字串轉換成Binary資料時，編碼必須要使用UTF8，否則類似像中文字元、日文字元，解密出來的時候會變成亂碼。
                    byte[] sourceBytes    = targetText.GetUtf8Bytes();
                    ICryptoTransform ictE = aesProvider.CreateEncryptor();

                    using ( MemoryStream msS = new MemoryStream() )
                    using ( CryptoStream csS = new CryptoStream( msS, ictE, CryptoStreamMode.Write ) )
                    {
                        csS.Write( sourceBytes, 0, sourceBytes.Length );
                        csS.FlushFinalBlock();

                        byte[] encryptedBytes = msS.ToArray();  //.ToArray() is important, don't mess with the buffer
                        result = Convert.ToBase64String( encryptedBytes  );
                    }
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "AesEncryptor對目標字串加密發生程式異常: {0}".FormatTo( ex.ToString() ) );
                HandlerExecuter.DoExceptionHandler( base.ExceptionCallback, ex );
            }

            return result;
        }

        /// <summary>對目標秘文字串進行解密
        /// </summary>
        /// <param name="cipherText">目標秘文字串</param>
        /// <returns>解密過後的明文字串</returns>
        public override string Decrypt( string cipherText )
        {
            string result = "";

            try
            {
                using ( AesCryptoServiceProvider aesProvider = GetProvider() )
                {
                    byte[] rawBytes       = Convert.FromBase64String( cipherText );
                    ICryptoTransform ictD = aesProvider.CreateDecryptor();

                    using ( MemoryStream msD    = new MemoryStream( rawBytes, 0, rawBytes.Length ) )
                    using ( CryptoStream csD    = new CryptoStream( msD, ictD, CryptoStreamMode.Read ) )
                    using ( StreamReader stream = new StreamReader( csD ) ) 
                    {
                        result = stream.ReadToEnd();
                    }
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "AesEncryptor對目標字串解密發生程式異常: {0}".FormatTo( ex.ToString() ) );
                HandlerExecuter.DoExceptionHandler( base.ExceptionCallback, ex );
            }

            return result;
        }

        #endregion 實作ICryptographyer介面方法


        #region 宣告私有的方法

        /// <summary>取得AES加解密演算法的Provider
        /// </summary>
        /// <returns>AES加解密演算法Provider</returns>
        private AesCryptoServiceProvider GetProvider()
        {
            return (AesCryptoServiceProvider)GetAlgorithmProvider( typeof ( AesCryptoServiceProvider ) );
        }

        #endregion 宣告私有的方法
    }
}
