﻿using System;
using System.IO;
using System.Security.Cryptography;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Cryptography
{
    /// <summary>DES對稱式加解密演算法元件 (此加密演算法已經被證實強度比較弱，而且在軟體中執行效能也比較差，建議還是使用AES對稱式加密演算法!)
    /// <remarks>
    /// 1. 此加密演算法已經被證實強度比較弱，而且在軟體中執行效能也比較差，建議還是使用AES對稱式加密演算法!
    /// 2. DES加密演算法的Block Size和Key Size都只支援到64 bit。
    /// 參考文章: 
    /// http://stackoverflow.com/questions/14031518/maximum-valid-block-size-value-for-descryptoserviceprovider
    /// http://stackoverflow.com/questions/8471449/is-there-any-restriction-of-my-key-length-passed-to-encoding-using-asciiencoding
    /// </remarks>
    /// </summary>
    public class DesEncryptor : BaseEncryptor
    {
        #region 實作ICryptographyer介面方法

        /// <summary>對目標明文字串進行加密
        /// </summary>
        /// <param name="targetText">目標明文字串</param>
        /// <returns>加密過後的秘文字串</returns>
        public override string Encrypt( string targetText )
        {
            string result = "";

            try
            {
                // 20151012 Bugfix by Pony: 這邊在把明文字串轉換成Binary資料時，編碼必須要使用UTF8，否則類似像中文字元、日文字元，解密出來的時候會變成亂碼。
                DESCryptoServiceProvider desProvider = GetProvider();
                byte[] dataByteArray = targetText.GetUtf8Bytes();
            
                using ( MemoryStream ms = new MemoryStream() )
                using ( CryptoStream cs = new CryptoStream( ms, desProvider.CreateEncryptor(), CryptoStreamMode.Write ) )
                {
                    cs.Write( dataByteArray, 0, dataByteArray.Length );
                    cs.FlushFinalBlock();
                    result = Convert.ToBase64String( ms.ToArray() );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "DesEncryptor對目標字串加密發生程式異常: {0}".FormatTo( ex.ToString() ) );
                HandlerExecuter.DoExceptionHandler( base.ExceptionCallback, ex );
            }

            return result;
        }

        /// <summary>對目標秘文字串進行解密
        /// </summary>
        /// <param name="cipherText">目標秘文字串</param>
        /// <returns>解密過後的明文字串</returns>
        public override string Decrypt( string cipherText )
        {
            string result = "";

            try
            {
                DESCryptoServiceProvider desProvider = GetProvider();
                byte[] dataByteArray = Convert.FromBase64String( cipherText );
            
                using ( MemoryStream ms = new MemoryStream() )
                {
                    using ( CryptoStream cs = new CryptoStream( ms, desProvider.CreateDecryptor(), CryptoStreamMode.Write ) )
                    {
                        cs.Write( dataByteArray, 0, dataByteArray.Length );
                        cs.FlushFinalBlock();
                        result = ms.ToArray().GetStringFromUtf8Bytes();
                    }
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "DesEncryptor對目標字串解密發生程式異常: {0}".FormatTo( ex.ToString() ) );
                HandlerExecuter.DoExceptionHandler( base.ExceptionCallback, ex );
                return string.Empty;
            }

            return result;
        }

        #endregion 實作ICryptographyer介面方法


        #region 宣告私有的方法

        /// <summary>取得DES加解密演算法的Provider
        /// </summary>
        /// <returns>DES加解密演算法的Provider</returns>
        private DESCryptoServiceProvider GetProvider()
        {
            return (DESCryptoServiceProvider)base.GetAlgorithmProvider( typeof ( DESCryptoServiceProvider ) );
        }

        #endregion 宣告私有的方法
    }
}
