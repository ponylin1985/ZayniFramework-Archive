﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Cryptography
{
    /// <summary>Rijndael對稱式加解密演算法元件
    /// </summary>
    /// <remarks>
    /// 1. Rijndael演算法的Block Size支援: 128、160、192、224和256 bit。
    /// 2. Rijndael演算法的Key Size支援: 128、192和256 bit。
    /// 參考文章如下:
    /// http://stackoverflow.com/questions/17171893/algorithm-is-the-rijndaelmanaged-class-in-c-sharp-equivalent-to-aes-encryption
    /// </remarks>
    public class RijndaelEncryptor : BaseEncryptor
    {
        #region 實作ICryptographyer介面方法

        /// <summary>對目標明文字串進行加密
        /// </summary>
        /// <param name="targetText">目標明文字串</param>
        /// <returns>加密過後的秘文字串</returns>
        public override string Encrypt( string targetText )
        {
            // 20151012 Bugfix by Pony: 這邊在把明文字串轉換成Binary資料時，編碼必須要使用UTF8，否則類似像中文字元、日文字元，解密出來的時候會變成亂碼。
            byte[] clearBytes    = targetText.GetUtf8Bytes();
            byte[] encryptedData = Encrypt( clearBytes );
            string result        = Convert.ToBase64String( encryptedData );
            return result;
        }

        /// <summary>對目標秘文字串進行解密
        /// </summary>
        /// <param name="cipherText">目標秘文字串</param>
        /// <returns>解密過後的明文字串</returns>
        public override string Decrypt( string cipherText )
        {
            byte[] cipherBytes   = Convert.FromBase64String( cipherText );
            byte[] decryptedData = Decrypt( cipherBytes );
            string result        = decryptedData.GetStringFromUtf8Bytes();
            return result;
        }

        #endregion 實作ICryptographyer介面方法


        #region 宣告私有個實體方法

        /// <summary>對目標位元組進行加密
        /// </summary>
        /// <param name="clearText">目標位元組</param>
        /// <returns>加密過後的位元組</returns>
        private byte[] Encrypt( byte[] clearText )
        {
            byte[] result = null;

            try
            {
                using ( MemoryStream ms = new MemoryStream() )
                {
                    Rijndael rijProvider = GetProvider();

                    using ( CryptoStream cs = new CryptoStream( ms, rijProvider.CreateEncryptor(), CryptoStreamMode.Write ) ) 
                    {
                        cs.Write( clearText, 0, clearText.Length );
                        cs.Close();
            
                        result = ms.ToArray();
                    }
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "RijndaelEncryptor對目標字串加密發生程式異常: {0}".FormatTo( ex.ToString() ) );
                HandlerExecuter.DoExceptionHandler( base.ExceptionCallback, ex );
                return null;
            }
            
            return result;
        }

        /// <summary>對目標位元組解密
        /// </summary>
        /// <param name="cipherData">目標位元組</param>
        /// <returns>解密過的位元組</returns>
        private byte[] Decrypt( byte[] cipherData )
        {
            byte[] result = null;

            try
            {
                using ( MemoryStream ms = new MemoryStream() )
                {
                    Rijndael rijProvider = GetProvider();

                    using ( CryptoStream cs = new CryptoStream( ms, rijProvider.CreateDecryptor(), CryptoStreamMode.Write ) )
                    {
                        cs.Write( cipherData, 0, cipherData.Length );
                        cs.Close();
                    }

                    result = ms.ToArray();
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "RijndaelEncryptor對目標字串解密發生程式異常: {0}".FormatTo( ex.ToString() ) );
                HandlerExecuter.DoExceptionHandler( base.ExceptionCallback, ex );
                return null;
            }
            
            return result;
        }

        /// <summary>取得Rijndael加解密演算法的Provider
        /// </summary>
        /// <returns>Rijndael加解密演算法的Provider</returns>
        private Rijndael GetProvider()
        {
            return (Rijndael)base.GetAlgorithmProvider( typeof ( Rijndael ) );
        }

        #endregion 宣告私有個實體方法
    }
}
