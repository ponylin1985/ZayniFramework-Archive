﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Cryptography
{
    /// <summary>對稱式加解密元件基底
    /// </summary>
    public abstract class BaseEncryptor : ISymmetricEncryptor
    {
        #region 宣告私有的靜態成員

        /// <summary>資料加解密的密碼
        /// </summary>
        private static string _password;

        /// <summary>資料加解密的 Salt
        /// </summary>
        private static byte[] _salt;

        #endregion 宣告私有的靜態成員


        #region 宣告靜態的建構子

        /// <summary>靜態建構子
        /// </summary>
        static BaseEncryptor()
        {
            ConfigReader.LoadSymmetricAlgorithmKeyPath();
            ConfigReader.LoadAesEncryptorSettings();
            ConfigReader.LoadRijndaelEncryptorSettings();
        }

        #endregion 宣告靜態的建構子


        #region 宣告公開的屬性

        /// <summary>執行加解密過程中發生例外的處理回呼委派
        /// </summary>
        public ExceptionHandler ExceptionCallback { get; set; }

        #endregion 宣告公開的屬性


        #region 宣告ICryptographyer介面的抽象

        /// <summary>對目標明文字串進行加密
        /// </summary>
        /// <param name="targetText">目標明文字串</param>
        /// <returns>加密過後的秘文字串</returns>
        public abstract string Encrypt( string targetText );

        /// <summary>對目標秘文字串進行解密
        /// </summary>
        /// <param name="cipherText">目標秘文字串</param>
        /// <returns>解密過後的明文字串</returns>
        public abstract string Decrypt( string cipherText );

        #endregion 宣告ICryptographyer介面的抽象


        #region 宣告保護的方法

        /// <summary>取得正確設定值的資料加解密演算法服務 (包含BlcokSize、KeySize、Key、IV、Mode、Padding... 等等設定值。)
        /// </summary>
        /// <param name="algorithmProviderType">對稱式演算法的型別: 目前只支援AesCryptoServiceProvider、Rijndael和DESCryptoServiceProvider。</param>
        /// <returns>對稱式加密演算法的服務</returns>
        protected SymmetricAlgorithm GetAlgorithmProvider( Type algorithmProviderType )
        {
            if ( algorithmProviderType.IsNull() )
            {
                Logger.WriteErrorLog( this, "傳入的引數為Null，無法取得資料加解密演算法服務", "BaseEncryptor.GetAlgorithmProvider" );
                return null;
            }

            if ( !algorithmProviderType.IsSubclassOf( typeof ( SymmetricAlgorithm ) ) )
            {
                Logger.WriteErrorLog( this, "傳入的引數不合法，目前只支援以下演算法: AesCryptoServiceProvider、Rijndael和DESCryptoServiceProvider", "BaseEncryptor.GetAlgorithmProvider" );
                return null;
            }

            SymmetricAlgorithm result = null;

            try
            {
                switch ( algorithmProviderType.Name )
                {
                    // 20150909 Pony Says: AES 演算法的 BlockSize 只能設定為 128 bit，KeySize 支援: 128 和 256 bit，參考文章如下:
                    // https://social.msdn.microsoft.com/Forums/vstudio/en-US/0d22648f-149e-4aea-918e-c3e86a30b23a/keysize-and-block-size-of-aes-128-192-and-256-bits
                    // http://stackoverflow.com/questions/17171893/algorithm-is-the-rijndaelmanaged-class-in-c-sharp-equivalent-to-aes-encryption
                    // https://msdn.microsoft.com/en-us/library/system.security.cryptography.aescryptoserviceprovider.keysize(v=vs.110).aspx
                    case "AesCryptoServiceProvider":
                        result           = new AesCryptoServiceProvider();
                        result.BlockSize = 128;     
                        result.KeySize   = CryptographySettings.AESKeySize;
                        break;

                    // 20150909 Pony Says: Rijndael 演算法的 BlockSize 支援: 128、160、192、224 和 256 bit，KeySize 支援: 128、192 和 256 bit，參考文章如下:
                    // http://stackoverflow.com/questions/17171893/algorithm-is-the-rijndaelmanaged-class-in-c-sharp-equivalent-to-aes-encryption
                    case "Rijndael":
                        result           = Rijndael.Create();
                        result.BlockSize = CryptographySettings.RijndaelBlockSize;
                        result.KeySize   = CryptographySettings.RijndaelKeySize;
                        break;

                    // 20150909 Pony Says: DES 加密演算法的 Block Size 和 Key Size 都只支援到 64 bit，參考文章如下:
                    // http://stackoverflow.com/questions/14031518/maximum-valid-block-size-value-for-descryptoserviceprovider
                    // http://stackoverflow.com/questions/8471449/is-there-any-restriction-of-my-key-length-passed-to-encoding-using-asciiencoding
                    case "DESCryptoServiceProvider":
                        result           = new DESCryptoServiceProvider();
                        result.BlockSize = 64;
                        result.KeySize   = 64;
                        break;

                    default:
                        result = new AesCryptoServiceProvider();
                        break;
                }
            
                result.Mode      = CipherMode.CBC;
                result.Padding   = PaddingMode.PKCS7;

                ReadEncryptionKey();

                var key     = new Rfc2898DeriveBytes( _password, _salt );
                result.Key  = key.GetBytes( result.KeySize / 8 );
                result.IV   = key.GetBytes( result.BlockSize / 8 );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "BaseEncryptor 取得正確設定值的資料加解密演算法服務發生異常: {0}".FormatTo( ex.ToString() ) );
                return null;
            }
            
            return result;
        }

        #endregion 宣告保護的方法


        #region 宣告私有的方法

        /// <summary>讀取加解密的金鑰
        /// </summary>
        private void ReadEncryptionKey()
        {
            if ( _password.IsNotNullOrEmpty() || _salt.IsNotNullOrEmptyArray() )
            {
                return;
            }

            string[] randomTexts = null;

            try
            {
                var sb = new StringBuilder();
                var keyPath = CryptographySettings.SymmetricAlgorithmKeyPath;

                #region 註解掉舊有的程式碼 20190417 Marked by Pony

                // Pony Says: File.ReadAllLines 方法，內建已經可以支援 unix-like 作業系統上「絕對路徑」和「相對路徑」的寫法! 
                // 因此這邊不需要自行再處理: 假弱勢相對路徑的寫法。

                // string prefix  = "~\\";
                // if ( CryptographySettings.SymmetricAlgorithmKeyPath.StartsWith( prefix ) )
                // {
                //     var path = CryptographySettings.SymmetricAlgorithmKeyPath.RemoveFirstAppeared( prefix );
                //     var dir  = Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location );
                //     keyPath  = Path.Combine( dir, path );

                //     if ( !File.Exists( keyPath ) )
                //     {
                //         dir     = Directory.GetCurrentDirectory();
                //         keyPath = Path.Combine( dir, path );
                //     }
                // }

                #endregion 註解掉舊有的程式碼

                // 20190417 Pony Says: 這邊的 File.ReadAllLines 方法，內建已經可以支援 unix-like 作業系統上「絕對路徑」和「相對路徑」的寫法!
                randomTexts = File.ReadAllLines( keyPath );

                foreach ( var t in randomTexts )
                {
                    sb.Append( t );
                }

                string r  = sb.ToString();
                _password = sb.ToString().Substring( 0, 50 );
                _salt     = Encoding.ASCII.GetBytes( r );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"{nameof ( BaseEncryptor )}, Read ZayniFramework cryptoryphy key file occur exception: {ex.ToString()}" );
            }
        }

        #endregion 宣告私有的方法
    }
}
