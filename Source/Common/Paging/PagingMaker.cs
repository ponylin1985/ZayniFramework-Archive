﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>資料分頁處理器
    /// </summary>
    public static class PagingMaker
    {
        /// <summary>執行資料分頁 (如果有大量資料要進行分頁，建議呼叫 PagingAsync 方法)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="page">目前要從第幾頁開始進行分頁</param>
        /// <param name="take">每一個分頁要取幾筆資料</param>
        /// <returns>分頁過後的資料模型集合</returns>
        public static IEnumerable<TModel> Paging<TModel>( IEnumerable<TModel> models, int page, int take )
        {
            var result = default ( IEnumerable<TModel> );

            try
            {
                result = models.Skip( ( page - 1 ) * take ).Take( take );
            }
            catch ( Exception ex )
            {
                throw ex;
            }
            
            return result;
        }

        /// <summary>執行資料分頁 (如果有大量資料要進行分頁，建議呼叫 PagingAsync 方法)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <typeparam name="TResult">分頁結果資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="page">目前要從第幾頁開始進行分頁</param>
        /// <param name="take">每一個分頁要取幾筆資料</param>
        /// <returns>分頁處理結果</returns>
        public static TResult Paging<TModel, TResult>( IEnumerable<TModel> models, int page, int take )
            where TResult : IPagingResult<TModel>
        {
            var result = default ( TResult );
            var data   = default ( List<TModel> );

            try
            {
                data = models.Skip( ( page - 1 ) * take ).Take( take ).ToList();
            }
            catch ( Exception ex )
            {
                throw ex;
            }

            result            = Activator.CreateInstance<TResult>();
            result.TotalCount = models.Count();
            result.PagedData  = data;
            return result;
        }

        /// <summary>已非同步作業執行資料分頁 (如果有大量資料要進行分頁，建議呼叫此方法)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="page">目前要從第幾頁開始進行分頁</param>
        /// <param name="take">每一個分頁要取幾筆資料</param>
        /// <returns>分頁過後的資料模型集合</returns>
        public static async Task<IEnumerable<TModel>> PagingAsync<TModel>( IEnumerable<TModel> models, int page, int take )
        {
            var result = await Task.Factory.StartNew<IEnumerable<TModel>>( () => 
            {
                var r = default ( IEnumerable<TModel> );

                try
                {
                    r = models.Skip( ( page - 1 ) * take ).Take( take );
                }
                catch ( Exception ex )
                {
                    throw ex;
                }

                return r;
            } );
            
            return result;
        }

        /// <summary>已非同步作業執行資料分頁 (如果有大量資料要進行分頁，建立呼叫此方法)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <typeparam name="TResult">分頁結果資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="page">目前要從第幾頁開始進行分頁</param>
        /// <param name="take">每一個分頁要取幾筆資料</param>
        /// <returns>分頁處理結果</returns>
        public static async Task<TResult> PagingAsync<TModel, TResult>( IEnumerable<TModel> models, int page, int take )
            where TResult : IPagingResult<TModel>
        {
            var result = default ( TResult );

            var pagingModels = await Task.Factory.StartNew<List<TModel>>( () => 
            {
                List<TModel> r = default ( List<TModel> );

                try
                {
                    r = models.Skip( ( page - 1 ) * take ).Take( take ).ToList();
                }
                catch ( Exception ex )
                {
                    throw ex;
                }

                return r;
            } );

            result            = Activator.CreateInstance<TResult>();
            result.TotalCount = models.Count();
            result.PagedData  = pagingModels;
            return result;
        }

        /// <summary>執行資料分頁 (如果有大量資料要進行分頁，建議呼叫 PagingAsync 方法)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="pagingInfo">分頁處理資訊</param>
        /// <returns>分頁過後的資料模型集合</returns>
        public static IEnumerable<TModel> Paging<TModel>( IEnumerable<TModel> models, IPaging pagingInfo )
        {
            var result = default ( IEnumerable<TModel> );

            try
            {
                result = models.Skip( ( pagingInfo.Page - 1 ) * pagingInfo.Take ).Take( pagingInfo.Take );
            }
            catch ( Exception ex )
            {
                throw ex;
            }

            return result;
        }

        /// <summary>執行資料分頁 (如果有大量資料要進行分頁，建議呼叫 PagingAsync 方法)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <typeparam name="TResult">分頁結果資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="pagingInfo">分頁處理資訊</param>
        /// <returns>分頁處理結果</returns>
        public static TResult Paging<TModel, TResult>( IEnumerable<TModel> models, IPaging pagingInfo )
            where TResult : IPagingResult<TModel>
        {
            TResult result = default ( TResult );
            var data = default ( List<TModel> );

            try
            {
                data = models.Skip( ( pagingInfo.Page - 1 ) * pagingInfo.Take ).Take( pagingInfo.Take ).ToList();
            }
            catch ( Exception ex )
            {
                throw ex;
            }

            result            = Activator.CreateInstance<TResult>();
            result.TotalCount = models.Count();
            result.PagedData  = data;
            return result;
        }

        /// <summary>已非同步作業執行資料分頁 (如果有大量資料要進行分頁，建議呼叫此方法)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="pagingInfo">分業處理資訊</param>
        /// <returns>分頁過後的資料模型集合</returns>
        public static async Task<IEnumerable<TModel>> PagingAsync<TModel>( IEnumerable<TModel> models, IPaging pagingInfo )
        {
            var result = await Task.Factory.StartNew<IEnumerable<TModel>>( () => 
            {
                var r = default ( IEnumerable<TModel> );

                try
                {
                    r = models.Skip( ( pagingInfo.Page - 1 ) * pagingInfo.Take ).Take( pagingInfo.Take );
                }
                catch ( Exception ex )
                {
                    throw ex;
                }

                return r;
            } );
            
            return result;
        }

        /// <summary>執行資料分頁 (如果有大量資料要進行分頁，建議呼叫 PagingAsync 方法)
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <typeparam name="TResult">分頁結果資料模型泛型</typeparam>
        /// <param name="models">資料模型集合</param>
        /// <param name="pagingInfo">分頁處理資訊</param>
        /// <returns>分頁處理結果</returns>
        public static async Task<TResult> PagingAsync<TModel, TResult>( IEnumerable<TModel> models, IPaging pagingInfo )
            where TResult : IPagingResult<TModel>
        {
            TResult result = default ( TResult );

            var pagedModels = await Task.Factory.StartNew<List<TModel>>( () => 
            {
                var r = default ( List<TModel> );

                try
                {
                    r = models.Skip( ( pagingInfo.Page - 1 ) * pagingInfo.Take ).Take( pagingInfo.Take ).ToList();
                }
                catch ( Exception ex )
                {
                    throw ex;
                }

                return r;
            } );
            
            result            = Activator.CreateInstance<TResult>();
            result.TotalCount = models.Count();
            result.PagedData  = pagedModels;
            return result;
        }
    }
}
