﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>通用委派執行器<para/>
    /// 1. 目前以 Delegate.Method.Invoke 反射的方式實作。<para/>
    /// 2. 實測結果:<para/>
    /// Delegate.DynamicInvoke() 反而比較慢。<para/>
    /// Delegate.Method.Invoke() 效能會比較好。
    /// </summary>
    public static class DelegateInvoker
    {
        /// <summary>執行委派的動作
        /// </summary>
        /// <param name="action">目標委派</param>
        /// <param name="parameters">目標委派的參數</param>
        public static void Exec( Delegate action, params object[] parameters ) =>
            action.Method.Invoke( action.Target, parameters );

        /// <summary>執行委派的動作，並且取得回傳值
        /// </summary>
        /// <typeparam name="TResult">委派執行回傳值的泛型</typeparam>
        /// <param name="action">目標委派</param>
        /// <param name="parameters">目標委派的參數</param>
        /// <returns>執行委派的回傳值</returns>
        public static TResult ExecResult<TResult>( Delegate action, params object[] parameters )
        {
            object result = action.Method.Invoke( action.Target, parameters );
            return result.IsNotNull() ? (TResult)result : default ( TResult );
        }   
    }
}
