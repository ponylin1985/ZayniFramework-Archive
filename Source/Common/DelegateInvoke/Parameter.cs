﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>委派的參數
    /// </summary>
    [Serializable()]
    public class Parameter
    {
        /// <summary>預設建構子
        /// </summary>
        public Parameter()
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="name">參數名稱</param>
        /// <param name="value">參數值</param>
        public Parameter( string name, object value )
        {
            Name  = name;
            Value = value;
        }

        /// <summary>解構子
        /// </summary>
        ~Parameter()
        {
            Name  = null;
            Value = null;
        }

        /// <summary>參數名稱
        /// </summary>
        public string Name { get; private set; }

        /// <summary>參數值
        /// </summary>
        public object Value { get; private set; }
    }
}
