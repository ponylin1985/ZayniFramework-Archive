﻿using System;
using System.Linq;
using System.Reflection;


namespace ZayniFramework.Common.Dynamic
{
    /// <summary>Duck Typing Programming 的轉換器
    /// </summary>
    public static class DuckTypingConverter
    {
        #region 宣告公開靜態的鴨子轉換方法

        /// <summary>執行鴨子編程模型轉換
        /// </summary>
        /// <typeparam name="TModel">欲轉換的目標型別</typeparam>
        /// <param name="source">來源物件</param>
        /// <param name="dynamicObject">ZayniFramework 框架的動態物件</param>
        /// <returns>轉換結果</returns>
        public static Result<dynamic> CastTo<TModel>( object source, dynamic dynamicObject = null )
        {
            #region 初始化回傳值

            var result = new Result<dynamic>() 
            {
                Success = false,
                Data    = null
            };

            #endregion 初始化回傳值

            #region 取得目標型別的公開方法與屬性

            Type targetType = typeof ( TModel );
            Type sourceType = source.GetType();

            var methods = targetType.GetMethods( BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly )
                                    .Where( m => !m.IsSpecialName )
                                    .ToArray();

            var properties = targetType.GetProperties( BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly );

            if ( !ContainsValidMethodsOrProperties( methods, properties ) )
            {
                result.Message = "目標型別中未定義任何公開方法或公開屬性，ZayniFramework 框架無法執行DuckTyping轉換。";
                return result;
            }

            #endregion 取得目標型別的公開方法與屬性

            #region 初始化動態物件

            if ( null == dynamicObject )
            {
                dynamicObject = DynamicHelper.CreateDynamicObject();
            }
            else
            {
                if ( !CheckDynamicObjectType( dynamicObject ) )
                {
                    result.Message = "傳入的動態物件不為ZayniFramework 框架DynamicBroker產生的動態物件，ZayniFramework 框架無法執行DuckTyping轉換。";
                    return result;
                }
            }

            #endregion 初始化動態物件

            #region 對動態物件進行方法繫結

            foreach ( var m in methods )
            {
                string methodName = m.Name;

                MethodInfo method = null;

                try
                {
                    method = sourceType.GetMethod( methodName );
                }
                catch ( Exception ex )
                {
                    result.HasException    = true;
                    result.ExceptionObject = ex;
                    result.Message         = "ZayniFramework 框架進行動態DuckTyping轉換發生異常: {0}".FormatTo( ex.ToString() );
                    return result;
                }

                Delegate handler = Reflector.CreateDelegate( method, source );

                if ( handler.IsNull() )
                {
                    result.Message = "ZayniFramework 框架進行動態DuckTyping轉換失敗: 反射器動態產生 {0} 方法的執行時期委派失敗".FormatTo( methodName );
                    return result;
                }

                bool isSuccess = DynamicHelper.BindMethod( dynamicObject, methodName, handler );

                if ( !isSuccess )
                {
                    result.Message = "ZayniFramework 框架進行動態DuckTyping轉換失敗: 進行動態物件 {0} 的方法繫結失敗!".FormatTo( methodName );
                    return result;
                }
            }

            #endregion 對動態物件進行方法繫結

            #region 對動態物件進行屬性繫結

            foreach ( var p in properties )
            {
                string propertyName = p.Name;
                
                PropertyInfo property = null;

                try
                {
                    property = sourceType.GetProperty( propertyName );
                }
                catch ( Exception ex )
                {
                    result.HasException    = true;
                    result.ExceptionObject = ex;
                    result.Message         = "ZayniFramework 框架進行動態DuckTyping轉換發生異常: {0}".FormatTo( ex.ToString() );
                    return result;
                }

                if ( property.IsNull() )
                {
                    result.Message = "ZayniFramework 框架進行動態DuckTyping轉換失敗: 反射取得來源物件的{0}屬性失敗，來源物件中沒有包含名稱為{0}的公開屬性。".FormatTo( propertyName );
                    return result;
                }

                object propertyValue = property.GetValue( source );

                bool isOk = DynamicHelper.BindProperty( dynamicObject, propertyName, propertyValue );

                if ( !isOk )
                {
                    result.Message = "ZayniFramework 框架進行動態DuckTyping轉換失敗: 進行動態物件 {0} 的屬性繫結失敗!".FormatTo( propertyName );
                    return result;
                }
            }

            #endregion 對動態物件進行屬性繫結

            #region 準備成功的回傳值物件

            result.Data      = dynamicObject;
            result.Message   = "ZayniFramework 框架進行動態DuckTyping轉換成功。";
            result.Success = true;
            
            #endregion 準備成功的回傳值物件

            return result;
        }

        #endregion 宣告公開靜態的鴨子轉換方法


        #region 宣告私有的靜態方法

        /// <summary>檢查目標型別是否有合法的方法或屬性
        /// </summary>
        /// <param name="methods">方法資訊陣列</param>
        /// <param name="properties">屬性資訊陣列</param>
        /// <returns>目標型別是否有合法的方法或屬性</returns>
        private static bool ContainsValidMethodsOrProperties( MethodInfo[] methods, PropertyInfo[] properties )
        {
            bool result = new Array[] { methods, properties }.Any( q => q.IsNotNullOrEmptyArray() );
            return result;
        }

        /// <summary>檢查目標動態物件是否為ZayniFramework 框架的動態物件
        /// </summary>
        /// <param name="obj">目標受檢物件</param>
        /// <returns>目標動態物件是否為ZayniFramework 框架的動態物件</returns>
        private static bool CheckDynamicObjectType( object obj )
        {
            bool result = "System.Dynamic.ExpandoObject" == obj.GetType().FullName;
            return result;
        }

        #endregion 宣告私有的靜態方法
    }
}
