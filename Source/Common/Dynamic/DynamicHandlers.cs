﻿namespace ZayniFramework.Common.Dynamic
{
    /// <summary>對ZayniFramework 框架的動態物件的迭代委派
    /// </summary>
    /// <param name="name">成員名稱</param>
    /// <param name="value">成員值</param>
    public delegate void DynamicEachHandler( string name, dynamic value );
}
