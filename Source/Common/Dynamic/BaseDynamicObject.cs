﻿using Microsoft.CSharp.RuntimeBinder;
using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Runtime.CompilerServices;


namespace ZayniFramework.Common.Dynamic
{
    /************************************************************************************************************
     * Reference: http://stackoverflow.com/questions/12057516/c-sharp-dynamicobject-dynamic-properties
     ************************************************************************************************************/

    /// <summary>Zayni Framework 框架的基底動態物件
    /// </summary>
    [Serializable()]
    public abstract class BaseDynamicObject : DynamicObject
    {
        #region 宣告私有欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>RuntimeErrorMessage 屬性的非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLockMsg = new AsyncLock();

        /// <summary>錯誤訊息
        /// </summary>
        private string _message;

        /// <summary>在執行時期動態物件的「成員 (Class Member)」屬性池
        /// </summary>
        private readonly Dictionary<string, object> _properties = new Dictionary<string, object>();

        #endregion 宣告私有欄位


        #region 宣告私有的屬性

        /// <summary>錯誤訊息
        /// </summary>
        private string RuntimeErrorMessage
        {
            get
            {
                using ( _asyncLockMsg.Lock() )
                {
                    return _message;
                }
            }
            set
            {
                using ( _asyncLockMsg.Lock() )
                {
                    _message = value;
                }
            }
        }

        #endregion 宣告私有的屬性


        #region 覆寫 DynamicObject 的虛擬方法

        /// <summary>提供叫用物件之作業的實作
        /// </summary>
        /// <param name="binder">提供已呼叫動態作業之物件的相關資訊</param>
        /// <param name="resultObj">取得作業的結果</param>
        /// <returns>是否執行成功</returns>
        public override bool TryGetMember( GetMemberBinder binder, out object resultObj )
        {
            using ( _asyncLock.Lock() )
            {
                resultObj = null;

                if ( binder.IsNull() )
                {
                    return false;
                }

                bool result = false;

                try
                {
                    string name = binder.Name.ToLower();
                    result = _properties.TryGetValue( name, out resultObj );
                }
                catch ( Exception ex )
                {
                    RuntimeErrorMessage = $"{GetLogTraceName( nameof ( TryGetMember ) )}, get member from dynamic object occur runtime exception. {ex.ToString()}";
                    return false;
                }
            
                return result;
            }
        }

        /// <summary>提供設定成員值之作業的實作
        /// </summary>
        /// <param name="binder">提供已呼叫動態作業之物件的相關資訊</param>
        /// <param name="value">要設定給成員的值</param>
        /// <returns>是否執行成功</returns>
        public override bool TrySetMember( SetMemberBinder binder, object value )
        {
            using ( _asyncLock.Lock() )
            {
                try
                {
                    _properties[ binder.Name.ToLower() ] = value;
                }
                catch ( Exception ex )
                {
                    RuntimeErrorMessage = $"{GetLogTraceName( nameof ( TrySetMember ) )}, set member tp dynamic object occur runtime exception. {ex.ToString()}";
                    return false;
                }

                return true;
            }
        }

        #endregion 覆寫 DynamicObject 的虛擬方法


        #region 宣告公開的方法

        /// <summary>取得動態物件的執行時期錯誤訊息
        /// </summary>
        /// <returns>動態物件的執行時期錯誤訊息</returns>
        public string GetErrorMessage() => RuntimeErrorMessage;

        /// <summary>取得動態物件的成員
        /// </summary>
        /// <param name="memberName">成員名稱</param>
        /// <returns>成員的值</returns>
        public object GetMember( string memberName )
        {
            using ( _asyncLock.Lock() )
            {
                if ( memberName.IsNullOrEmpty() )
                {
                    return null;
                }
            
                object result = null;

                try
                {
                    var binder = Binder.GetMember(
                        CSharpBinderFlags.None,
                        memberName, 
                        this.GetType(),
                        new List<CSharpArgumentInfo>
                        {
                            CSharpArgumentInfo.Create( CSharpArgumentInfoFlags.None, null )
                        }
                    );

                    var callsite = CallSite<Func<CallSite, object, object>>.Create( binder );
                    result = callsite.Target( callsite, this );
                }
                catch ( Exception ex )
                {
                    RuntimeErrorMessage = $"{GetLogTraceName( nameof ( GetMember ) )}, get runtime value from dynamic object member occur exception. {ex.ToString()}";
                    return null;
                }

                return result;
            }
        }

        /// <summary>設定動態物件的成員
        /// </summary>
        /// <param name="memberName">成員名稱</param>
        /// <param name="value">成員的值</param>
        public bool SetMember( string memberName, object value = null )
        {
            using ( _asyncLock.Lock() )
            {
                if ( memberName.IsNullOrEmpty() )
                {
                    return false;
                }

                try
                {
                    var binder = Binder.SetMember(
                        CSharpBinderFlags.None,
                        memberName, 
                        this.GetType(),
                        new List<CSharpArgumentInfo>()
                        {
                            CSharpArgumentInfo.Create( CSharpArgumentInfoFlags.None, null ),
                            CSharpArgumentInfo.Create( CSharpArgumentInfoFlags.None, null )
                        }
                    );

                    var callsite = CallSite<Func<CallSite, object, object, object>>.Create( binder );
                    callsite.Target( callsite, this, value );
                }
                catch ( Exception ex )
                {
                    RuntimeErrorMessage = $"{GetLogTraceName( nameof ( GetMember ) )}, set runtime value to dynamic object member occur exception. {ex.ToString()}";
                    return false;
                }

                return true;
            }
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( BaseDynamicObject )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
