﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>Zayni Framework框架的程式例外，此例外代表「該方法在此具象子類別中無需實作!」
    /// </summary>
    public sealed class NoNeedToImplementException : Exception
    {
        /// <summary>預設建構子
        /// </summary>
        public NoNeedToImplementException() : base( "該方法在此具象子類別中無需實作!" )
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="message"></param>
        public NoNeedToImplementException( string message ) : base( message )
        {
            // pass
        }
    }
}
