﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>ZayniFramework 框架的方法AOP攔截器
    /// </summary>
    [AttributeUsage( AttributeTargets.Method, AllowMultiple = true )]
    public abstract class BaseMethodInterceptor : Attribute
    {
        /// <summary>預設建構子
        /// </summary>
        public BaseMethodInterceptor()
        {
            Order = string.Empty;
        }
        
        /// <summary>多載建構子
        /// </summary>
        /// <param name="order">攔截器執行順序</param>
        public BaseMethodInterceptor( string order = "" )
        {
            Order = order;
        }

        /// <summary>攔截器執行順序
        /// </summary>
        public string Order { get; set; }
    }
}
