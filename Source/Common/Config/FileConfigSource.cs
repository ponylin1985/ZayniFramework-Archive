﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>.NET Frameowrk Config 檔案來源
    /// </summary>
    public sealed class FileConfigSource
    {
        #region 宣告私有的常數

        /// <summary>Zayni Framework 框架的 Config 設定區段名稱
        /// </summary>
        private const string ZAYNI_CONFIG_SECTION = "ZayniFramework";

        #endregion 宣告私有的常數


        #region 宣告公開的屬性

        /// <summary>來源 Config 檔路徑
        /// </summary>
        public string ConfigPath { get; private set; } 

        /// <summary>目標 Config 檔
        /// </summary>
        public Configuration Config { get; private set; }

        #endregion 宣告公開的屬性


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="configPath">來源 Config 檔的完整路徑</param>
        public FileConfigSource( string configPath )
        {
            if ( configPath.IsNullOrEmpty() )
            {
                Command.StdoutErr( $"The argument '{nameof ( configPath )}' can not be null or empty string." );
                throw new ArgumentNullException( $"The argument '{nameof ( configPath )}' can not be null or empty string." );
            }

            ConfigPath = configPath;

            try
            {
                var file = new ExeConfigurationFileMap() 
                {
                    ExeConfigFilename = ConfigPath
                };

                Config = ConfigurationManager.OpenMappedExeConfiguration( file, ConfigurationUserLevel.None );
            }
            catch ( Exception ex )
            {
                Command.StdoutErr( $"{nameof ( FileConfigSource )} load config file occur exception. configPath: {configPath}. {ex.ToString()}" );
                throw ex;
            }
        }

        /// <summary>解構子
        /// </summary>
        ~FileConfigSource()
        {
            ConfigPath = null;
            Config     = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>取得來源Config檔中的目標區段
        /// </summary>
        /// <param name="sectionName">目標區段名稱</param>
        /// <returns>Config區段</returns>
        public ConfigurationSection GetSection( string sectionName )
        {
            try
            {
                return Config.GetSection( sectionName );
            }
            catch ( Exception ex )
            {
                Command.StdoutErr( $"{nameof ( GetSection )}, get config section occur exception sectionName: {sectionName}. {ex.ToString()}" );
                throw ex;
            }
        }

        /// <summary>取得資料庫連線字串的設定區段
        /// </summary>
        /// <returns>資料庫連線字串的設定區段</returns>
        public ConnectionStringSettingsCollection GetConnectionStringsSection() => Config.ConnectionStrings.ConnectionStrings;

        /// <summary>取得 AppSettings 應用程式組態設定區段
        /// </summary>
        /// <returns></returns>
        public KeyValueConfigurationCollection GetAppSettingsSection() => Config.AppSettings.Settings;

        /// <summary>取得 Zayni Framework 框架的 Config 設定區段
        /// </summary>
        /// <returns>Zayni Framework 框架的 Config 設定區段</returns>
        public ZayniConfigSection GetZayniFrameworkConfigSection() => GetSection( ZAYNI_CONFIG_SECTION ) as ZayniConfigSection;

        /// <summary>儲存 Config 檔案
        /// </summary>
        public void Save() => Config.Save();

        #endregion 宣告公開的方法
    }
}
