﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>Zayni Framework 的 Config 設定區段類別
    /// </summary>
    public sealed class ZayniConfigSection : ConfigurationSection
    {
        #region 宣告在 Config 檔中 ZayniFramework 框架的各個模組的設定區段名稱

        /// <summary>.NET CLR 執行時期 Managed Thread Pool 組態設定
        /// </summary>
        private const string THREAD_POOL_SETTINGS = "ThreadPoolSettings";

        /// <summary>ZayniFramework.DataAccess 模組的 Config 設定名稱
        /// </summary>
        private const string DATA_ACCESS = "DataAccessSettings";

        /// <summary>ZayniFramework Database Migration 的 Config 設定名稱
        /// </summary>
        private const string DATABASE_MIGRATION = "DatabaseMigration";

        /// <summary>ZayniFramework.Logging 模組的 Config 設定名稱
        /// </summary>
        private const string LOGGING = "LoggingSettings";

        /// <summary>Fusoin.ExceptionHandling 模組的 Config 設定名稱
        /// </summary>
        private const string EXCEPTION_HANDLING = "ExceptionHandling";

        /// <summary>ZayniFramework.Formatting 模組的 Config 設定名稱
        /// </summary>
        private const string FORMATTING = "FormattingSettings";

        /// <summary>ZayniFramework.Service.Proxy模組的Config設定名稱
        /// </summary>
        private const string WEBAPI_SERVICE_CLIENT = "";

        /// <summary>ZayniFramework.Caching 模組的 Config 設定名稱
        /// </summary>
        private const string CACHING = "CachingSettings";

        /// <summary>ZayniFramework.Caching.RedisClient 模組的 Config 設定名稱
        /// </summary>
        private const string REDIS_CLIENT_SETTINGS = "RedisClientSettings";

        /// <summary>ZayniFramework.Cryptography 模組的 Config 設定名稱
        /// </summary>
        private const string CRYPTOGRAPHY = "CryptographySettings";

        #endregion 宣告在Config檔中 ZayniFramework 框架的各個模組的設定區段名稱


        #region 宣告Config檔中 ZayniFramework 框架的區段設定存取屬性

        /// <summary>.NET CLR 執行時期 Managed Thread Pool 組態設定
        /// </summary>
        [ConfigurationProperty( THREAD_POOL_SETTINGS, IsRequired = false )]
        public ThreadPoolConfigElement ThreadPoolSettings
        {
            get
            {
                return (ThreadPoolConfigElement)this[ THREAD_POOL_SETTINGS ];
            }
            set
            {
                this[ THREAD_POOL_SETTINGS ] = value;
            }
        }

        /// <summary>DataAccess模組的設定值節點
        /// </summary>
        [ConfigurationProperty( DATA_ACCESS, IsRequired = false )]
        public DataAccessSettingsConfig DataAccessSettings
        {
            get
            {
                return (DataAccessSettingsConfig)this[ DATA_ACCESS ];
            }
            set
            {
                this[ DATA_ACCESS ] = value;
            }
        }

        /// <summary>Database Migration CLI Tool 的設定值節點
        /// </summary>
        [ConfigurationProperty( DATABASE_MIGRATION, IsRequired = false )]
        public DatabaseMigrationConfig DatabaseMigrationSettings
        {
            get
            {
                return (DatabaseMigrationConfig)this[ DATABASE_MIGRATION ];
            }
            set
            {
                this[ DATABASE_MIGRATION ] = value;
            }
        }

        /// <summary>Logging模組的設定值節點
        /// </summary>
        [ConfigurationProperty( LOGGING, IsRequired = false )]
        public LoggingSettingsConfigCollection LoggingSettings
        {
            get
            {
                return (LoggingSettingsConfigCollection)this[ LOGGING ];
            }
            set
            {
                this[ LOGGING ] = value;
            }
        }

        /// <summary>ExceptionHandling模組的設定值節點
        /// </summary>
        [ConfigurationProperty( EXCEPTION_HANDLING, IsRequired = false )]
        public ExceptionHandlingSettingsConfigCollection ExceptionHandlingSettings
        {
            get
            {
                return (ExceptionHandlingSettingsConfigCollection)this[ EXCEPTION_HANDLING ];
            }
            set
            {
                this[ EXCEPTION_HANDLING ] = value;
            }
        }

        /// <summary>Formatting模組的設定值節點
        /// </summary>
        [ConfigurationProperty( FORMATTING, IsRequired = false )]
        public FormattingSettingsConfigCollection FormatSettings
        {
            get
            {
                return (FormattingSettingsConfigCollection)this[ FORMATTING ];
            }
            set
            {
                this[ FORMATTING ] = value;
            }
        }

        /// <summary>Caching模組的設定值節點
        /// </summary>
        [ConfigurationProperty( CACHING, IsRequired = false )]
        public CachingSettingsConfigCollection CachingSettings
        {
            get
            {
                return (CachingSettingsConfigCollection)this[ CACHING ];
            }
            set
            {
                this[ CACHING ] = value;
            }
        }

        /// <summary>Caching.RedisClient 模組的設定值節點
        /// </summary>
        [ConfigurationProperty( REDIS_CLIENT_SETTINGS, IsRequired = false )]
        public RedisClientsSettingConfigCollection RedisClientSettings
        {
            get
            {
                return (RedisClientsSettingConfigCollection)this[ REDIS_CLIENT_SETTINGS ];
            }
            set
            {
                this[ REDIS_CLIENT_SETTINGS ] = value;
            }
        }

        /// <summary>Cryptography模組的設定值節點
        /// </summary>
        [ConfigurationProperty( CRYPTOGRAPHY, IsRequired = false )]
        public CryptographySettingsConfigCollection CryptographySettings
        {
            get
            {
                return (CryptographySettingsConfigCollection)this[ CRYPTOGRAPHY ];
            }
            set
            {
                this[ CRYPTOGRAPHY ] = value;
            }
        }

        #endregion 宣告Config檔中 ZayniFramework 框架的區段設定存取屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
