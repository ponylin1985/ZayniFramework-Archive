﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>Logging模組設定區段集合
    /// </summary>
    public class LoggingSettingsConfigCollection : ConfigurationElementCollection
    {
        #region 宣告私有的字串常數

        /// <summary>是否啟用日誌記錄功能屬性名稱
        /// </summary>
        private const string LOG_ENABLE = "logEnable";
        
        /// <summary>是否啟用事件檢視器日誌屬性名稱
        /// </summary>
        private const string EVENT_LOG_ENABLE = "eventLogEnable";

        /// <summary>定時清除 Elasticsearch 服務中 log 日誌的天數
        /// </summary>
        private const string ES_LOG_RESET_DAYS = "esLogResetDays";

        /// <summary>定時清除 Database 資料庫中 log 日誌的天數
        /// </summary>
        private const string DB_LOG_RESET_DAYS = "dbLogResetDays";

        /// <summary>預設文字日誌功能設定節點名稱
        /// </summary>
        private const string DEFAULT_TEXT_LOGGER = "DefaultTextLogger";

        /// <summary>電子郵件日誌警示的設定節點名稱
        /// </summary>
        private const string EMAIL_NOTIFY_LOGGER = "EmailNotifyLogger";

        /// <summary>ElasticSearch 日誌功能的設定節點名稱
        /// </summary>
        private const string ES_LOGGER = "ElasticSearchLogger";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>ZayniFramework 框架的日誌功能是否啟用
        /// </summary>
        [ConfigurationProperty( LOG_ENABLE, IsRequired = false )]
        public bool LogEnable
        {
            get
            {
                bool result;

                string setting = this[ LOG_ENABLE ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "設定值必須為布林值" );
                }

                return result;
            }
            set
            {
                this[ LOG_ENABLE ] = value;
            }
        }

        /// <summary>是否啟用事件檢視器日誌功能
        /// </summary>
        [ConfigurationProperty( EVENT_LOG_ENABLE, IsRequired = false )]
        public bool EventLogEnable
        {
            get
            {
                bool result;

                string setting = this[ EVENT_LOG_ENABLE ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "設定值必須為布林值" );
                }

                return result;
            }
            set
            {
                this[ EVENT_LOG_ENABLE ] = value;
            }
        }

        /// <summary>定時清除 Elasticsearch 服務中 log 日誌的天數，最大天數設定為 23 天。
        /// </summary>
        [ConfigurationProperty( ES_LOG_RESET_DAYS, IsRequired = false )]
        public int? ESLogResetDays
        {
            get
            {
                string setting = this[ ES_LOG_RESET_DAYS ] + "";

                if ( setting.IsNullOrEmpty() )
                {
                    return null;
                }

                if ( !int.TryParse( setting, out int result ) || result < 0 )
                {
                    throw new ConfigurationErrorsException( $"The 'LoggingSettings/esLogResetDays' config value is incorrect. Must be a positive integer." );
                }

                if ( result > 23 )
                {
                    return 23;
                }

                return result;
            }
            set
            {
                this[ ES_LOG_RESET_DAYS ] = value;
            }
        }

        /// <summary>定時清除 Database 資料庫中 log 日誌的天數，最大天數設定為 23 天。
        /// </summary>
        [ConfigurationProperty( DB_LOG_RESET_DAYS, IsRequired = false )]
        public int? DbLogResetDays
        {
            get
            {
                string setting = this[ DB_LOG_RESET_DAYS ] + "";

                if ( setting.IsNullOrEmpty() )
                {
                    return null;
                }

                if ( !int.TryParse( setting, out int result ) || result < 0 )
                {
                    throw new ConfigurationErrorsException( $"The 'LoggingSettings/dbLogResetDays' config value is incorrect. Must be a positive integer." );
                }

                if ( result > 23 )
                {
                    return 23;
                }

                return result;
            }
            set
            {
                this[ DB_LOG_RESET_DAYS ] = value;
            }
        }

        /// <summary>預設文字日誌功能設定區段
        /// </summary>
        [ConfigurationProperty( DEFAULT_TEXT_LOGGER, IsRequired = false )]
        public TextLoggerConfigCollection DefaultTextLogSetting
        {
            get
            {
                return (TextLoggerConfigCollection)this[ DEFAULT_TEXT_LOGGER ];
            }
            set
            {
                this[ DEFAULT_TEXT_LOGGER ] = value;
            }
        }

        /// <summary>電子郵件日誌警示的設定區段
        /// </summary>
        [ConfigurationProperty( EMAIL_NOTIFY_LOGGER, IsRequired = false )]
        public EmailNotifyLoggerConfigCollection EmailNotifySetting
        {
            get
            {
                return (EmailNotifyLoggerConfigCollection)this[ EMAIL_NOTIFY_LOGGER ];
            }
            set
            {
                this[ EMAIL_NOTIFY_LOGGER ] = value;
            }
        }

        /// <summary>ElasticSearchLogger 日誌功能的設定區段
        /// </summary>
        [ConfigurationProperty( ES_LOGGER, IsRequired = false )]
        public ElasticSearchLoggerConfigCollection ElasticSearchSetting
        {
            get
            {
                return (ElasticSearchLoggerConfigCollection)this[ ES_LOGGER ];
            }
            set
            {
                this[ ES_LOGGER ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法


        #region 實作ConfigurationElementCollection的抽象

        /// <summary>
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new LoggingSettingsConfigCollection();
        }

        /// <summary>
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element )
        {
            throw new NotImplementedException();
        }

        #endregion 實作ConfigurationElementCollection的抽象
    }
}
