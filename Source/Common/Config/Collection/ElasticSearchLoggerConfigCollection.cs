using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>ElasticSearchLogger 客戶端 Config 區段設定集合
    /// </summary>
    public class ElasticSearchLoggerConfigCollection : ConfigurationElementCollection
    {
        #region 覆寫父類別的方法

        /// <summary>Config 集合型態
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.AddRemoveClearMap;

        /// <summary>產生一個新的 ElasticSearchLoggerElement Config元素
        /// </summary>
        /// <returns>新的 ElasticSearchLoggerElement Config元素</returns>
        protected override ConfigurationElement CreateNewElement() => new ElasticSearchLoggerElement();

        /// <summary>取得目標 Config 元素的 Key 值
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element ) => ( (ElasticSearchLoggerElement)element ).Name;

        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法

        #endregion 覆寫父類別的方法


        #region 宣告公開的索引子與方法

        /// <summary>ElasticSearchLoggerElement 區段的索引子
        /// </summary>
        /// <param name="index">數值索引值</param>
        /// <returns>元素內容</returns>
        public ElasticSearchLoggerElement this[ int index ]
        {
            get
            {
                return (ElasticSearchLoggerElement)BaseGet( index );
            }
            set
            {
                if ( null != base.BaseGet( index ) )
                {
                    base.BaseRemoveAt( index );
                }

                base.BaseAdd( index, value );
            }
        }

        /// <summary>ElasticSearchLoggerElement 區段的索引子
        /// </summary>
        /// <param name="name">字串索引值</param>
        /// <returns>元素內容</returns>
        public new ElasticSearchLoggerElement this[ string name ]
        {
            get
            {
                return (ElasticSearchLoggerElement)BaseGet( name );
            }
            set
            {
                if ( null != base.BaseGet( name ) )
                {
                    base.BaseRemove( name );
                }

                base.BaseAdd( value );
            }
        }

        /// <summary>加入一個新的 ElasticSearchLoggerElement 元素
        /// </summary>
        /// <param name="element">要被加入的目標 ElasticSearchLoggerElement 元素</param>
        public void Add( ElasticSearchLoggerElement element ) => base.BaseAdd( element );

        #endregion 宣告公開的索引子與方法
    }
}