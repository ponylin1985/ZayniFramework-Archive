﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>RedisClient 個體 Config 設定集合
    /// </summary>
    public class RedisClientsSettingConfigCollection : ConfigurationElementCollection
    {
        #region 宣告字串常數

        /// <summary>RedisClient 客戶端 Config 設定集合 
        /// </summary>
        private const string REDIS_CLIENTS = "RedisClients";

        #endregion 宣告字串常數


        #region 宣告公開的屬性

        /// <summary>RedisClient 客戶端 Config 設定集合
        /// </summary>
        [ConfigurationProperty( REDIS_CLIENTS, IsRequired = false )]
        public RedisClientsConfigCollection RedisClients
        {
            get
            {
                return (RedisClientsConfigCollection)this[ REDIS_CLIENTS ];
            }
            set
            {
                this[ REDIS_CLIENTS ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法


        #region 實作 ConfigurationElementCollection 的抽象

        /// <summary>建立新的 RedisClientsConfigCollection 設定子節點
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement() => new RedisClientsConfigCollection();

        /// <summary>取得設定元素 Key 值
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element ) => throw new NotImplementedException();

        #endregion 實作ConfigurationElementCollection的抽象
    }
}
