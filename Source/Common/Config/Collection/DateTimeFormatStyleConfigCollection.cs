﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
	// 20130712 Created by Pony
    // 20150730 Edited by Pony: 日期時間的格式化需要支援多國語系
	/// <summary>日期格式樣式設定值節點
	/// </summary>
	public class DateTimeFormatStyleConfigCollection : ConfigurationElementCollection
	{
        #region 覆寫父類別的方法

        /// <summary>Config集合型態
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        /// <summary>產生一個新的DateTimeFormatElement Config元素
        /// </summary>
        /// <returns>新的DateTimeFormatElement Config元素</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new DateTimeFormatElement();
        }

        /// <summary>取得目標Config元素的Key值
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element )
        {
            return ( (DateTimeFormatElement)element ).Language;
        }

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫覆類別的方法


        #region 宣告公開的索引子與方法

        /// <summary>區段的索引子
        /// </summary>
        /// <param name="index">數值索引值</param>
        /// <returns>元素內容</returns>
        public DateTimeFormatElement this[ int index ]
        {
            get
			{
				return (DateTimeFormatElement)BaseGet( index );
			}
			set
			{
				if ( null != base.BaseGet( index ) )
				{
					base.BaseRemoveAt( index );
				}

				base.BaseAdd( index, value );
			}
        }

        /// <summary>區段的索引子
        /// </summary>
        /// <param name="name">字串索引值</param>
        /// <returns>元素內容</returns>
        public new DateTimeFormatElement this[ string name ]
        {
            get
			{
				return (DateTimeFormatElement)BaseGet( name );
			}
			set
			{
				if( null != base.BaseGet( name ) )
				{
					base.BaseRemove( name );
				}

				base.BaseAdd( value );
			}
        }

        /// <summary>加入一個新的DateTimeFormatElement元素
		/// </summary>
		/// <param name="element">要被加入的目標DateTimeFormatElement元素</param>
		public void Add( DateTimeFormatElement element )
		{
			base.BaseAdd( element );
		}

        #endregion  宣告公開的索引子與方法
	}
}
