﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>Caching模組設定區段Config集合
    /// </summary>
    public class CachingSettingsConfigCollection : ConfigurationElementCollection
    {
        #region 宣告字串常數

        /// <summary>快取管理員模式
        /// </summary>
        private const string MODE = "managerMode";
        
        /// <summary>快取池設定元素
        /// </summary>
        private const string CACHE_POOL = "CachePool";
        
        /// <summary>快取資料設定資料元素
        /// </summary>
        private const string CACHE_DATA = "CacheData";

        /// <summary>快取文件檔案設定元素
        /// </summary>
        private const string CACHE_FILE = "CacheFile";

        /// <summary>Redis 快取服務設定元素
        /// </summary>
        private const string REDIS_CACHE = "RedisCache";

        /// <summary>MySQL Memory Engine 快取服務設定元素
        /// </summary>
        private const string MYSQL_MEMORY_CACHE = "MySQLMemoryCache";

        /// <summary>Zayni Framework 框架的遠端快取設定元素
        /// </summary>
        private const string REMOTE_CACHE = "RemoteCache";

        #endregion 宣告字串常數


        #region 宣告公開的屬性

        /// <summary>快取倉儲的模式<para/>
        /// Memory: InProcess Memory 內部程序記憶體快取 (效能最快)<para/>
        /// Redis: Redis 遠端服務快取 (效能普通)<para/>
        /// MySQLMemory: MySQL Memory Engine 遠端服務快取<para/>
        /// RemoteCache: Zayni Framework 框架內建的遠端快取<para/>
        /// File: 本機文件檔案快取 (效能很差)<para/>
        /// LocalDB: SQLServer LocalDB 本機資料快取 (已廢棄)<para/>
        /// </summary>
        [ConfigurationProperty( MODE, IsRequired = false )]
        public string CacheManagerMode
        {
            get
            {
                return this[ MODE ] + "";
            }
            set
            {
                this[ MODE ] = value;
            }
        }

        /// <summary>快取池設定Config元素
        /// </summary>
        [ConfigurationProperty( CACHE_POOL, IsRequired = false )]
        public CachePoolElement CachePool
        {
            get
            {
                return (CachePoolElement)this[ CACHE_POOL ];
            }
            set
            {
                this[ CACHE_POOL ] = value;
            }
        }

        /// <summary>快取資料設定Config元素
        /// </summary>
        [ConfigurationProperty( CACHE_DATA, IsRequired = false )]
        public CacheDataElement CacheData
        {
            get
            {
                return (CacheDataElement)this[ CACHE_DATA ];
            }
            set
            {
                this[ CACHE_DATA ] = value;
            }
        }

        /// <summary>快取文件檔案設定元素
        /// </summary>
        [ConfigurationProperty( CACHE_FILE, IsRequired = false )]
        public CacheFileElement CacheFile
        {
            get
            {
                return (CacheFileElement)this[ CACHE_FILE ];
            }
            set
            {
                this[ CACHE_FILE ] = value;
            }
        }

        /// <summary>Redis 快取服務設定 Config 集合
        /// </summary>
        [ConfigurationProperty( REDIS_CACHE, IsRequired = false )]
        public RedisCacheConfigCollection RedisCache
        {
            get
            {
                return (RedisCacheConfigCollection)this[ REDIS_CACHE ];
            }
            set
            {
                this[ REDIS_CACHE ] = value;
            }
        }

        /// <summary>Zayni 遠端快取服務 Config 設定元素
        /// </summary>
        [ConfigurationProperty( REMOTE_CACHE, IsRequired = false )]
        public RemoteCacheElement RemoteCache
        {
            get
            {
                return (RemoteCacheElement)this[ REMOTE_CACHE ];
            }
            set
            {
                this[ REMOTE_CACHE ] = value;
            }
        }

        /// <summary>MySQL Memory Engine 快取服務設定元素
        /// </summary>
        [ConfigurationProperty( MYSQL_MEMORY_CACHE, IsRequired = false )]
        public MySQLMemoryCacheElement MySQLMemoryCache
        {
            get
            {
                return (MySQLMemoryCacheElement)this[ MYSQL_MEMORY_CACHE ];
            }
            set
            {
                this[ MYSQL_MEMORY_CACHE ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法


        #region 實作 ConfigurationElementCollection 的抽象

        /// <summary>建立新的 CachingSettingsConfigCollection 設定子節點
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement() => new CachingSettingsConfigCollection();

        /// <summary>取得設定元素 Key 值
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element ) => throw new NotImplementedException();

        #endregion 實作ConfigurationElementCollection的抽象
    }
}
