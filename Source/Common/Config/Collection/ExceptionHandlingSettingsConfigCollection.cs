﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>ExceptionHandling模組設定區段集合
    /// </summary>
    public class ExceptionHandlingSettingsConfigCollection : ConfigurationElementCollection
    {
        #region 宣告私有的字串常數

        /// <summary>ZayniFramework 框架預設文字日誌例外處理政策
        /// </summary>
        private const string DEFAULT_TEXT_LOG_POLICY = "DefaultTextLoggingPolicy";

        /// <summary>ZayniFramework 框架電子郵件通知例外處理政策
        /// </summary>
        private const string EMAIL_NOTIFY_POLICY = "EMailNotifyPolicy";

        /// <summary>客製化自訂的例外處理政策集合
        /// </summary>
        private const string CUSTOM_POLICIES = "CustomPolicies";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>ZayniFramework 框架預設文字日誌例外處理政策
        /// </summary>
        [ConfigurationProperty( DEFAULT_TEXT_LOG_POLICY, IsRequired = false )]
        public DefaultTextLoggingPolicyElement DefaultTextLoggingPolicy
        {
            get
            {
                return (DefaultTextLoggingPolicyElement)this[ DEFAULT_TEXT_LOG_POLICY ];
            }
            set
            {
                this[ DEFAULT_TEXT_LOG_POLICY ] = value;
            }
        }

        /// <summary>ZayniFramework 框架電子郵件通知例外處理政策
        /// </summary>
        [ConfigurationProperty( EMAIL_NOTIFY_POLICY, IsRequired = false )]
        public EMailNotifyPolicyElement EMailNotifyPolicy
        {
            get
            {
                return (EMailNotifyPolicyElement)this[ EMAIL_NOTIFY_POLICY ];
            }
            set
            {
                this[ EMAIL_NOTIFY_POLICY ] = value;
            }
        }

        /// <summary>客製化自訂的例外處理政策集合
        /// </summary>
        [ConfigurationProperty( CUSTOM_POLICIES, IsRequired = false )]
        public CustomPoliciesConfigCollection CustomExceptionPolicies
        {
            get
            {
                return (CustomPoliciesConfigCollection)this[ CUSTOM_POLICIES ];
            }
            set
            {
                this[ CUSTOM_POLICIES ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法


        #region 實作ConfigurationElementCollection的抽象

        /// <summary>
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new LoggingSettingsConfigCollection();
        }

        /// <summary>
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element )
        {
            throw new NotImplementedException();
        }

        #endregion 實作ConfigurationElementCollection的抽象
    }
}
