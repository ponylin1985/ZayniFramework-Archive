﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>MSSQL資料庫型別對應Config區段集合
    /// </summary>
    public class SqlDbTypeConfigCollection : ConfigurationElementCollection
    {
        #region 覆寫父類別的方法

        /// <summary>Config集合型態
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        /// <summary>產生一個新的DbTypeMappingElement Config元素
        /// </summary>
        /// <returns>新的ActionInfo Config元素</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new DbTypeMappingElement();
        }

        /// <summary>取得目標Config元素的Key值
        /// </summary>
        /// <param name="element">Config元素</param>
        /// <returns>目標Config元素</returns>
        protected override object GetElementKey( ConfigurationElement element )
        {
            return ( (DbTypeMappingElement)element ).Name;
        }

        #endregion 覆寫父類別的方法


        #region 宣告公開的索引子與方法

        /// <summary>SqlDbType區段的索引子
        /// </summary>
        /// <param name="index">數值索引值</param>
        /// <returns>元素內容</returns>
        public DbTypeMappingElement this[ int index ]
        {
            get
			{
				return (DbTypeMappingElement)BaseGet( index );
			}
			set
			{
				if( null != base.BaseGet( index ) )
				{
					base.BaseRemoveAt( index );
				}

				base.BaseAdd( index, value );
			}
        }

        /// <summary>SqlDbType區段的索引子
        /// </summary>
        /// <param name="name">字串索引值</param>
        /// <returns>元素內容</returns>
        public new DbTypeMappingElement this[ string name ]
        {
            get
			{
				return (DbTypeMappingElement)BaseGet( name );
			}
			set
			{
				if( null != base.BaseGet( name ) )
				{
					base.BaseRemove( name );
				}

				base.BaseAdd( value );
			}
        }

        /// <summary>加入一個新的DbTypeMappingElement元素
		/// </summary>
		/// <param name="element">要被加入的目標DbTypeMappingElement元素</param>
		public void Add( DbTypeMappingElement element )
		{
			base.BaseAdd( element );
		}

        #endregion  宣告公開的索引子與方法
    }
}
