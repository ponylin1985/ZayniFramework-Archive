﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>電子郵件日誌警示的 Config 區段設定集合
    /// </summary>
    public class EmailNotifyLoggerConfigCollection : ConfigurationElementCollection
    {
        #region 覆寫父類別的方法

        /// <summary>Config集合型態
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.AddRemoveClearMap;

        /// <summary>產生一個新的 EmailNotifyLoggerElement Config元素
        /// </summary>
        /// <returns>新的 EmailNotifyLoggerElement Config元素</returns>
        protected override ConfigurationElement CreateNewElement() => new EmailNotifyLoggerElement();

        /// <summary>取得目標Config元素的Key值
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element ) => ( (EmailNotifyLoggerElement)element ).Name;

        #endregion 覆寫父類別的方法


        #region 宣告公開的索引子與方法

        /// <summary>EmailNotifyLoggerElement 區段的索引子
        /// </summary>
        /// <param name="index">數值索引值</param>
        /// <returns>元素內容</returns>
        public EmailNotifyLoggerElement this[ int index ]
        {
            get
            {
                return (EmailNotifyLoggerElement)BaseGet( index );
            }
            set
            {
                if ( null != base.BaseGet( index ) )
                {
                    base.BaseRemoveAt( index );
                }

                base.BaseAdd( index, value );
            }
        }

        /// <summary>EmailNotifyLoggerElement 區段的索引子
        /// </summary>
        /// <param name="name">字串索引值</param>
        /// <returns>元素內容</returns>
        public new EmailNotifyLoggerElement this[ string name ]
        {
            get
            {
                return (EmailNotifyLoggerElement)BaseGet( name );
            }
            set
            {
                if ( null != base.BaseGet( name ) )
                {
                    base.BaseRemove( name );
                }

                base.BaseAdd( value );
            }
        }

        /// <summary>加入一個新的 EmailNotifyLoggerElement 元素
        /// </summary>
        /// <param name="element">要被加入的目標 EmailNotifyLoggerElement 元素</param>
        public void Add( EmailNotifyLoggerElement element ) => base.BaseAdd( element );

        #endregion  宣告公開的索引子與方法
    }
}
