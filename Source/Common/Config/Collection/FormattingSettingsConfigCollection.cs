﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    //  20130712 Created by Pony
    /// <summary>Formatting模組設定區段集合
    /// </summary>
    public class FormattingSettingsConfigCollection : ConfigurationElementCollection
    {
        /// <summary>日期格式化設定值名稱
        /// </summary>
        private const string DATETIME_FORMAT_STYLE = "DateTimeFormatStyle";

        /// <summary>日期格式樣式設定值節點
        /// </summary>
        [ConfigurationProperty( DATETIME_FORMAT_STYLE, IsRequired = false )]
        public DateTimeFormatStyleConfigCollection DateTimeFormatStyle
        {
            get
            {
                return (DateTimeFormatStyleConfigCollection)this[ DATETIME_FORMAT_STYLE ];
            }
            set
            {
                this[ DATETIME_FORMAT_STYLE ] = value;
            }
        }


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法


        #region 實作ConfigurationElementCollection的抽象

        /// <summary>
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new FormattingSettingsConfigCollection();
        }

        /// <summary>
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            throw new NotImplementedException();
        }

        #endregion 實作ConfigurationElementCollection的抽象
    }
}
