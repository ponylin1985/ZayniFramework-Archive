﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>客製化自訂例外處理政策Config區段集合
    /// </summary>
    public class CustomPoliciesConfigCollection : ConfigurationElementCollection
    {
        #region 覆寫父類別的方法

        /// <summary>Config集合型態
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        /// <summary>產生一個新的CustomPolicyElement Config元素
        /// </summary>
        /// <returns>新的CustomPolicyElement Config元素</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new CustomPolicyElement();
        }

        /// <summary>取得目標Config元素的Key值
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element )
        {
            return ( (CustomPolicyElement)element ).PolicyName;
        }

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父類別的方法


        #region 宣告公開的索引子與方法

        /// <summary>CustomPolicies區段的索引子
        /// </summary>
        /// <param name="index">數值索引值</param>
        /// <returns>元素內容</returns>
        public CustomPolicyElement this[ int index ]
        {
            get
			{
				return (CustomPolicyElement)BaseGet( index );
			}
			set
			{
				if ( null != base.BaseGet( index ) )
				{
					base.BaseRemoveAt( index );
				}

				base.BaseAdd( index, value );
			}
        }

        /// <summary>CustomPolicies區段的索引子
        /// </summary>
        /// <param name="name">字串索引值</param>
        /// <returns>元素內容</returns>
        public new CustomPolicyElement this[ string name ]
        {
            get
			{
				return (CustomPolicyElement)BaseGet( name );
			}
			set
			{
				if ( null != base.BaseGet( name ) )
				{
					base.BaseRemove( name );
				}

				base.BaseAdd( value );
			}
        }

        /// <summary>加入一個新的CustomPolicyElement元素
		/// </summary>
		/// <param name="element">要被加入的目標CustomPolicyElement元素</param>
		public void Add( CustomPolicyElement element )
		{
			base.BaseAdd( element );
		}

        #endregion  宣告公開的索引子與方法
    }
}
