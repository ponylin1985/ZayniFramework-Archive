﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>Redis 快取服務設定 Config 集合
    /// </summary>
    public class RedisCacheConfigCollection : ConfigurationElementCollection
    {
        #region 覆寫父類別的方法

        /// <summary>Config集合型態
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.AddRemoveClearMap;

        /// <summary>產生一個新的 RedisCacheElement Config元素
        /// </summary>
        /// <returns>新的 RedisCacheElement Config元素</returns>
        protected override ConfigurationElement CreateNewElement() => new RedisCacheElement();

        /// <summary>取得目標 RedisCacheElement 元素的 Key 值
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element ) => ( (RedisCacheElement)element ).Name;

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父類別的方法


        #region 宣告公開的索引子與方法

        /// <summary>RedisCache 區段的索引子
        /// </summary>
        /// <param name="index">數值索引值</param>
        /// <returns>元素內容</returns>
        public RedisCacheElement this[ int index ]
        {
            get
            {
                return (RedisCacheElement)BaseGet( index );
            }
            set
            {
                if ( null != base.BaseGet( index ) )
                {
                    base.BaseRemoveAt( index );
                }

                base.BaseAdd( index, value );
            }
        }

        /// <summary>RedisCache 區段的索引子
        /// </summary>
        /// <param name="name">字串索引值</param>
        /// <returns>元素內容</returns>
        public new RedisCacheElement this[ string name ]
        {
            get
            {
                return (RedisCacheElement)BaseGet( name );
            }
            set
            {
                if ( null != base.BaseGet( name ) )
                {
                    base.BaseRemove( name );
                }

                base.BaseAdd( value );
            }
        }

        /// <summary>加入一個新的 RedisCacheElement 元素
        /// </summary>
        /// <param name="element">要被加入的目標 RedisCacheElement 元素</param>
        public void Add( RedisCacheElement element ) => base.BaseAdd( element );

        #endregion  宣告公開的索引子與方法
    }
}
