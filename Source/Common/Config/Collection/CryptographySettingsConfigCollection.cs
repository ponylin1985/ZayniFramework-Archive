﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>Cryptography資料加解密模組Config設定區段集合
    /// </summary>
    public class CryptographySettingsConfigCollection : ConfigurationElementCollection
    {
        #region 宣告私有的字串常數

        /// <summary>AES加解密元件設定區段
        /// </summary>
        private const string AES_ENCRYPTOR_SETTINGS = "AesEncryptor";

        /// <summary>Rijndael加解密元件設定區段
        /// </summary>
        private const string RIJ_ENCRYPTOR_SETTINGS = "RijndaelEncryptor";

        /// <summary>雜湊加密元色設定區段
        /// </summary>
        private const string HASH_ENCRYPTOR_SETTINGS = "HashEncryptor";

        /// <summary>對稱式加解密金鑰完整路徑
        /// </summary>
        private const string SYMMETRIC_ALGORITHM_KEY_PATH = "symmetricAlgorithmKeyPath";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>AES加解密元件設定區段
        /// </summary>
        [ConfigurationProperty( AES_ENCRYPTOR_SETTINGS, IsRequired = false )]
        public AesEncryptorElement AesEncryptor
        {
            get
            {
                return (AesEncryptorElement)this[ AES_ENCRYPTOR_SETTINGS ];
            }
            set
            {
                this[ AES_ENCRYPTOR_SETTINGS ] = value;
            }
        }

        /// <summary>Rijndael加解密元件設定區段
        /// </summary>
        [ConfigurationProperty( RIJ_ENCRYPTOR_SETTINGS, IsRequired = false )]
        public RijndaelEncryptorElement RijndaelEncryptor
        {
            get
            {
                return (RijndaelEncryptorElement)this[ RIJ_ENCRYPTOR_SETTINGS ];
            }
            set
            {
                this[ RIJ_ENCRYPTOR_SETTINGS ] = value;
            }
        }

        /// <summary>雜湊加密元色設定區段
        /// </summary>
        [ConfigurationProperty( HASH_ENCRYPTOR_SETTINGS, IsRequired = false )]
        public HashEncryptorElement HashEncryptor
        {
            get
            {
                return (HashEncryptorElement)this[ HASH_ENCRYPTOR_SETTINGS ];
            }
            set
            {
                this[ HASH_ENCRYPTOR_SETTINGS ] = value;
            }
        }

        /// <summary>對稱式加解密金鑰完整路徑
        /// </summary>
        [ConfigurationProperty( SYMMETRIC_ALGORITHM_KEY_PATH, IsRequired = false )]
        public string SymmetricAlgorithmKeyPath
        {
            get
            {
                return this[ SYMMETRIC_ALGORITHM_KEY_PATH ] + "";
            }
            set
            {
                this[ SYMMETRIC_ALGORITHM_KEY_PATH ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法


        #region 實作ConfigurationElementCollection的抽象

        /// <summary>
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new LoggingSettingsConfigCollection();
        }

        /// <summary>
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element )
        {
            throw new NotImplementedException();
        }

        #endregion 實作ConfigurationElementCollection的抽象
    }
}
