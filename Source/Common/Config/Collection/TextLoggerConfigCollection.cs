﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>文字日誌記錄Config區段集合
    /// </summary>
    public class TextLoggerConfigCollection : ConfigurationElementCollection
    {
        #region 覆寫覆類別的方法

        /// <summary>Config集合型態
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        /// <summary>產生一個新的DefaultTextLoggerElement Config元素
        /// </summary>
        /// <returns>新的DefaultTextLoggerElement Config元素</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new DefaultTextLoggerElement();
        }

        /// <summary>取得目標Config元素的Key值
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element )
        {
            return ( (DefaultTextLoggerElement)element ).LogName;
        }

        #endregion 覆寫覆類別的方法


        #region 宣告公開的索引子與方法

        /// <summary>DefaultTextLogger區段的索引子
        /// </summary>
        /// <param name="index">數值索引值</param>
        /// <returns>元素內容</returns>
        public DefaultTextLoggerElement this[ int index ]
        {
            get
            {
                return (DefaultTextLoggerElement)BaseGet( index );
            }
            set
            {
                if( null != base.BaseGet( index ) )
                {
                    base.BaseRemoveAt( index );
                }

                base.BaseAdd( index, value );
            }
        }

        /// <summary>DefaultTextLogger區段的索引子
        /// </summary>
        /// <param name="name">字串索引值</param>
        /// <returns>元素內容</returns>
        public new DefaultTextLoggerElement this[ string name ]
        {
            get
            {
                return (DefaultTextLoggerElement)BaseGet( name );
            }
            set
            {
                if( null != base.BaseGet( name ) )
                {
                    base.BaseRemove( name );
                }

                base.BaseAdd( value );
            }
        }

        /// <summary>加入一個新的DefaultTextLoggerElement元素
        /// </summary>
        /// <param name="element">要被加入的目標DefaultTextLoggerElement元素</param>
        public void Add( DefaultTextLoggerElement element )
        {
            base.BaseAdd( element );
        }

        #endregion  宣告公開的索引子與方法
    }
}
