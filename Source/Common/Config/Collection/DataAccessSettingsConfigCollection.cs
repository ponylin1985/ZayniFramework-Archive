﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>DataAccess模組的Config設定區段集合
    /// </summary>
    public class DataAccessSettingsConfig : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>資料庫供應商Config區段集合
        /// </summary>
        private const string DATABASE_PROVIDERS = "DatabaseProviders";

        /// <summary>資料庫型別代碼對應Config設定元素
        /// </summary>
        private const string DB_TYPE_CODE_MAPPING = "DbTypeCodeMapping";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>資料庫供應商Config區段集合
        /// </summary>
        [ConfigurationProperty( DATABASE_PROVIDERS, IsRequired = false )]
        public DatabaseProvidersConfigCollection DatabaseProviders
        {
            get
            {
                return (DatabaseProvidersConfigCollection)this[ DATABASE_PROVIDERS ];
            }
            set
            {
                this[ DATABASE_PROVIDERS ] = value;
            }
        }

        /// <summary>資料庫型別代碼對應Config設定元素
        /// </summary>
        [ConfigurationProperty( DB_TYPE_CODE_MAPPING, IsRequired = false )]
        public DbTypeCodeMappingElement DbTypeCodeMapping
        {
            get
            {
                return (DbTypeCodeMappingElement)this[ DB_TYPE_CODE_MAPPING ];
            }
            set
            {
                this[ DB_TYPE_CODE_MAPPING ] = value;
            }
        }

        #endregion 宣告公開的屬性
    }
}
