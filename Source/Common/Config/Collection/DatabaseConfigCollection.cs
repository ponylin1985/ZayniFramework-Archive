﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>資料庫供應商Config區段集合
    /// </summary>
    public class DatabaseProvidersConfigCollection : ConfigurationElementCollection
    {
        #region 覆寫父類別的方法

        /// <summary>Config集合型態
        /// </summary>
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        /// <summary>產生一個新的ConnectionElement Config元素
        /// </summary>
        /// <returns>新的ConnectionElement Config元素</returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new ConnectionElement();
        }

        /// <summary>取得目標Config元素的Key值
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey( ConfigurationElement element )
        {
            return ( (ConnectionElement)element ).Name;
        }

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父類別的方法
        

        #region 宣告公開的索引子與方法

        /// <summary>DataAccessSettings區段的索引子
        /// </summary>
        /// <param name="index">數值索引值</param>
        /// <returns>元素內容</returns>
        public ConnectionElement this[ int index ]
        {
            get
			{
				return (ConnectionElement)BaseGet( index );
			}
			set
			{
				if( null != base.BaseGet( index ) )
				{
					base.BaseRemoveAt( index );
				}

				base.BaseAdd( index, value );
			}
        }

        /// <summary>DataAccessSettings區段的索引子
        /// </summary>
        /// <param name="name">字串索引值</param>
        /// <returns>元素內容</returns>
        public new ConnectionElement this[ string name ]
        {
            get
			{
				return (ConnectionElement)BaseGet( name );
			}
			set
			{
				if ( null != base.BaseGet( name ) )
				{
					base.BaseRemove( name );
				}

				base.BaseAdd( value );
			}
        }

        /// <summary>加入一個新的ConnectionElement元素
		/// </summary>
		/// <param name="element">要被加入的目標ConnectionElement元素</param>
		public void Add( ConnectionElement element )
		{
			base.BaseAdd( element );
		}

        #endregion  宣告公開的索引子與方法
    }
}
