﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Config檔AppSetting設定值讀取器
    /// </summary>
    public sealed class AppSettingsLoader
    {
        #region 宣告私有的建構子

        /// <summary>宣告私有的建構子
        /// </summary>
        private AppSettingsLoader()
        {
            // pass
        }

        #endregion 宣告私有的建構子


        #region 宣告公開且靜態的獨體屬性

        private static AppSettingsLoader _instance;

        /// <summary>取得AppSettingsLoader實體
        /// </summary>
        public static AppSettingsLoader Instance
        {
            get
            {
                if ( null == _instance )
                {
                    _instance = new AppSettingsLoader();
                }

                return _instance;
            }
        }

        #endregion 宣告公開且靜態的獨體屬性

        
        #region 宣告公開的實體方法

        /// <summary>讀取WebConfig appSetting區段的步林的Value設定值
        /// </summary>
        /// <param name="key">appSetting區段的key值</param>
        /// <param name="value">appSetting區段的Value設定值</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>讀取appSetting設定值是否成功</returns>
        public bool LoadBooleanConfigSetting( string key, out bool value, out string message )
        {
            bool   result = LoadConfigSetting<bool>( key, bool.TryParse, out value, out message );
            return result;
        }

        /// <summary>讀取WebConfig appSetting區段的步林的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果，資料: 參考型別的Bool物件</returns>
        public Result<Bool> LoadBooleanConfigSetting( string appSettingsKey )
        {
            var result = new Result<Bool>() 
            {
                Success = false,
                Data    = new Bool( false )
            };
            
            bool   value;
            string message;

            bool isSuccess = LoadBooleanConfigSetting( appSettingsKey, out value, out message );

            if ( !isSuccess )
            {
                result.Message = message;
                return result;
            }

            result.Data      = new Bool( value );
            result.Success = true;
            return result;
        }

        /// <summary>以非同步作業讀取WebConfig appSetting區段的步林的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果，資料: 參考型別的Bool物件</returns>
        public async Task<Result<Bool>> LoadBooleanConfigSettingAsync( string appSettingsKey )
        {
            bool   value;
            string message;

            return await Task.Factory.StartNew<Result<Bool>>( () => 
            {
                var result = new Result<Bool>() 
                {
                    Success = false,
                    Data      = new Bool( false )
                };

                bool isSuccess = LoadBooleanConfigSetting( appSettingsKey, out value, out message );

                if ( !isSuccess )
                {
                    result.Message = message;
                    return result;
                }

                result.Data      = new Bool( value );
                result.Success = true;
                return result;
            } );
        }

        /// <summary>讀取WebConfig appSetting區段的TimeSpan的Value設定值
        /// </summary>
        /// <param name="key">appSetting區段的key值</param>
        /// <param name="value">appSetting區段的Value設定值</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>讀取appSetting設定值是否成功</returns>
        public bool LoadTimeSpanConfigSetting( string key, out TimeSpan value, out string message )
        {
            bool result = LoadConfigSetting<TimeSpan>( key, TimeSpan.TryParse, out value, out message );
            return result;
        }

        /// <summary>讀取WebConfig appSetting區段的TimeSpan的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果，資料: 參考型別的TimeSpanR物件</returns>
        public Result<TimeSpanR> LoadTimeSpanConfigSetting( string appSettingsKey )
        {
            var result = new Result<TimeSpanR>() 
            {
                Success = false,
                Data    = new TimeSpanR()
            };
            
            TimeSpan value;
            string   message;

            bool isSuccess = LoadTimeSpanConfigSetting( appSettingsKey, out value, out message );

            if ( !isSuccess )
            {
                result.Message = message;
                return result;
            }

            result.Data      = new TimeSpanR( value );
            result.Success = true;
            return result;
        }

        /// <summary>以非同步作業讀取WebConfig appSetting區段的TimeSpan的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果，資料: 參考型別的TimeSpanR物件</returns>
        public async Task<Result<TimeSpanR>> LoadTimeSpanConfigSettingAsync( string appSettingsKey )
        {
            TimeSpan value;
            string   message;

            return await Task.Factory.StartNew<Result<TimeSpanR>>( () => 
            {
                var result = new Result<TimeSpanR>() 
                {
                    Success = false,
                    Data      = new TimeSpanR()
                };

                bool isSuccess = LoadTimeSpanConfigSetting( appSettingsKey, out value, out message );

                if ( !isSuccess )
                {
                    result.Message = message;
                    return result;
                }

                result.Data      = new TimeSpanR( value );
                result.Success = true;
                return result;
            } );
        }

        /// <summary>讀取WebConfig appSetting區段的Int的Value設定值
        /// </summary>
        /// <param name="key">appSetting區段的key值</param>
        /// <param name="value">appSetting區段的Value設定值</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>讀取appSetting設定值是否成功</returns>
        public bool LoadIntConfigSetting( string key, out int value, out string message )
        {
            bool result = LoadConfigSetting<int>( key, int.TryParse, out value, out message );
            return result;
        }

        /// <summary>讀取WebConfig appSetting區段的Int的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果，資料: 參考型別的Int物件</returns>
        public Result<Int> LoadIntConfigSetting( string appSettingsKey )
        {
            var result = new Result<Int>() 
            {
                Success = false,
                Data    = new Int()
            };
            
            int    value;
            string message;

            bool isSuccess = LoadIntConfigSetting( appSettingsKey, out value, out message );

            if ( !isSuccess )
            {
                result.Message = message;
                return result;
            }

            result.Data      = new Int( value );
            result.Success = true;
            return result;
        }

        /// <summary>以非同步作業讀取WebConfig appSetting區段的Int的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果，資料: 參考型別的Int物件</returns>
        public async Task<Result<Int>> LoadIntConfigSettingAsync( string appSettingsKey )
        {
            int    value;
            string message;

            return await Task.Factory.StartNew<Result<Int>>( () => 
            {
                var result = new Result<Int>() 
                {
                    Success = false,
                    Data    = new Int()
                };

                bool isSuccess = LoadIntConfigSetting( appSettingsKey, out value, out message );

                if ( !isSuccess )
                {
                    result.Message = message;
                    return result;
                }

                result.Data      = new Int( value );
                result.Success = true;
                return result;
            } );
        }

        /// <summary>讀取WebConfig appSetting區段的Decimal的Value設定值
        /// </summary>
        /// <param name="key">appSetting區段的key值</param>
        /// <param name="value">appSetting區段的Value設定值</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>讀取appSetting設定值是否成功</returns>
        public bool LoadDecimalConfigSetting( string key, out decimal value, out string message )
        {
            bool result = LoadConfigSetting<decimal>( key, decimal.TryParse, out value, out message );
            return result;
        }

        /// <summary>讀取WebConfig appSetting區段的Decimal的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果，資料: 參考型別的DecimalR物件</returns>
        public Result<DecimalR> LoadDecimalConfigSetting( string appSettingsKey )
        {
            var result = new Result<DecimalR>() 
            {
                Success = false,
                Data    = new DecimalR()
            };
            
            decimal value;
            string  message;

            bool isSuccess = LoadDecimalConfigSetting( appSettingsKey, out value, out message );

            if ( !isSuccess )
            {
                result.Message = message;
                return result;
            }

            result.Data      = new DecimalR( value );
            result.Success = true;
            return result;
        }

        /// <summary>非同步作業讀取WebConfig appSetting區段的Decimal的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果，資料: 參考型別的DecimalR物件</returns>
        public async Task<Result<DecimalR>> LoadDecimalConfigSettingAsync( string appSettingsKey )
        {
            decimal value;
            string  message;

            return await Task.Factory.StartNew<Result<DecimalR>>( () => 
            {
                var result = new Result<DecimalR>() 
                {
                    Success = false,
                    Data    = new DecimalR()
                };

                bool isSuccess = LoadDecimalConfigSetting( appSettingsKey, out value, out message );

                if ( !isSuccess )
                {
                    result.Message = message;
                    return result;
                }

                result.Data      = new DecimalR( value );
                result.Success = true;
                return result;
            } );
        }

        /// <summary>讀取WebConfig appSetting區段字串的Value設定值
        /// </summary>
        /// <param name="key">appSetting區段的key值</param>
        /// <param name="result">設定在appSetting區段的value設定值</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>讀取appSetting設定值是否成功</returns>
        public bool LoadConfigSetting( string key, out string result, out string message )
        {
            result  = "";
            message = "";

            try
            {
                result = ConfigManagement.AppSettings[ key ];
            }
            catch
            {
                message = $"Get appSetting '{key}' from config file occur exception.";
                return false;
            }

            if ( string.IsNullOrEmpty( result ) )
            {
                message = $"Retrive null value from config file AppSetting key: {key}.";
                return false;
            }

            return true;
        }

        /// <summary>讀取WebConfig appSetting區段字串的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果</returns>
        public Result<string> LoadStringTextConfigSetting( string appSettingsKey )
        {
            string result ;
            string message;

            return new Result<string>() 
            {
                Success = LoadConfigSetting( appSettingsKey, out result, out message ),
                Data      = result,
                Message   = message
            };
        }

        /// <summary>以非同步作業讀取WebConfig appSetting區段字串的Value設定值
        /// </summary>
        /// <param name="appSettingsKey">appSetting區段的key值</param>
        /// <returns>讀取結果</returns>
        public async Task<Result<string>> LoadStringTextConfigSettingAsync( string appSettingsKey )
        {
            string value;
            string message;

            return await Task.Factory.StartNew<Result<string>>( () => 
            {
                var result = new Result<string>() 
                {
                    Success = false,
                    Data    = null
                };

                bool isSuccess = LoadConfigSetting( appSettingsKey, out value, out message );

                if ( !isSuccess )
                {
                    result.Message = message;
                    return result;
                }

                result.Data      = value;
                result.Success = true;
                return result;
            } );
        }

        #endregion 宣告公開的實體方法


        #region 宣告私有的實體方法

        /// <summary>讀取Config設定檔，並且作適當的轉型
        /// </summary>
        /// <typeparam name="TResult">轉型後的泛型</typeparam>
        /// <param name="key">appSetting區段的key值</param>
        /// <param name="handler">轉換的委派</param>
        /// <param name="result">appSetting區段的Value設定值</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>讀取appSetting設定值是否成功</returns>
        private bool LoadConfigSetting<TResult>( string key, TryParseHandler<TResult> handler, out TResult result, out string message )
        {
            result = default( TResult );
            
            string value;

            bool isSuccess = this.LoadConfigSetting( key, out value, out message );

            if ( !isSuccess )
            {
                return false;
            }

            if ( !handler( value, out result ) )
            {
                message = "讀取Config設定檔appSettings key值 {0} 轉型失敗，請檢查value的設定值型別是否正確。";
                return false;
            }

            return true;
        }

        #endregion 宣告私有的實體方法
    }
}
