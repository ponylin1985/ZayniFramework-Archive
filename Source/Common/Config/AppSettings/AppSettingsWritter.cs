﻿using NeoSmart.AsyncLock;
using System;
using System.Configuration;
using System.Reflection;


namespace ZayniFramework.Common
{
    /// <summary>Config 組態 AppSetting 設定值寫入器 (新增、刪除、修改)
    /// </summary>
    public sealed class AppSettingsWritter
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly static AsyncLock _asyncLock = new AsyncLock();

        /// <summary>獨體參考
        /// </summary>
        private static AppSettingsWritter _instance;

        /// <summary>組態設定
        /// </summary>
        private Configuration _config;
        
        /// <summary>Config檔中的AppSettings設定區段
        /// </summary>
        private AppSettingsSection _section;

        #endregion 宣告私有的欄位


        #region 宣告私有的建構子
        
        /// <summary>私有建構子
        /// </summary>
        private AppSettingsWritter()
        {
            try
            {
                // 20151026 Edited by Pony: 如果是要確實修改 Config 檔案的 appSettings 設定值，而且還要 Disk IO 寫入變更 Config 檔案的話，載入Config 檔案必須要以以下的方式進行
                // 參考文章: http://stackoverflow.com/questions/11149556/app-config-change-value 
                // 但是目前以下這段寫法，不支援 ASP.NET Web Application 或任何架設在 IIS 上的 .NET Service 的 Web.config 檔

                // 20190606 Bugfix by Pony: 
                // 重新修正取得 EntryAssembly 的 config file 的路徑的寫法... 這個東西... 實在他媽的靠杯...
                var fullName   = Assembly.GetEntryAssembly().Location;
                var configFile = $"{fullName}.config";

                ExeConfigurationFileMap configFileMap = new ExeConfigurationFileMap();
                configFileMap.ExeConfigFilename       = configFile;
                
                _config  = ConfigurationManager.OpenMappedExeConfiguration( configFileMap, ConfigurationUserLevel.None );
                _section = _config.AppSettings;
            }
            catch ( Exception ex )
            {
                throw ex;
            }
        }

        #endregion 宣告私有的建構子


        #region 宣告公開且靜態的獨體屬性

        /// <summary>取得AppSettingsWritter實體
        /// </summary>
        public static AppSettingsWritter Instance
        {
            get
            {
                using ( _asyncLock.Lock() )
                {
                    if ( _instance.IsNull() )
                    {
                        _instance = new AppSettingsWritter();
                    }

                    return _instance;
                }
            }
        }

        #endregion 宣告公開且靜態的獨體屬性


        #region 宣告公開的實體方法

        /// <summary>新增appSettings區段的設定值
        /// </summary>
        /// <param name="key">appSettings區段的key值</param>
        /// <param name="value">欲新增的設定值</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>新增設定值是否成功</returns>
        public bool AddConfigSetting( string key, string value, out string message )
        {
            return ProcessConfigSetting( key, value, out message, () => _section.Settings.Add( key, value ) );
        }

        /// <summary>新增appSettings區段的設定值
        /// </summary>
        /// <param name="key">appSettings區段的key值</param>
        /// <param name="value">欲新增的設定值</param>
        /// <returns>新增結果</returns>
        public Result AddConfigSetting( string key, string value )
        {
            string message;

            return new Result() 
            {
                Success = ProcessConfigSetting( key, value, out message, () => _section.Settings.Add( key, value ) ),
                Message   = message
            };
        }

        /// <summary>刪除appSettings區段的設定值
        /// </summary>
        /// <param name="key">appSettings區段的key值</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>刪除設定值是否成功</returns>
        public bool RemoveConfigSetting( string key, out string message )
        {
            return ProcessConfigSetting( key, "", out message, () =>  _section.Settings.Remove( key ) ); 
        }

        /// <summary>修改appSettings區段的設定值
        /// </summary>
        /// <param name="key">appSettings區段的key值</param>
        /// <param name="value">欲修改的設定值</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>修改設定值是否成功</returns>
        public bool ModifyConfigSetting( string key, string value, out string message )
        {
            return ProcessConfigSetting( key, value, out message, () => _section.Settings[ key ].Value = value );
        }

        #endregion 宣告公開的實體方法


        #region 宣告私有的實體方法

        /// <summary>執行Config檔appSetting區段新增、刪除、修改的動作
        /// </summary>
        /// <param name="key">appSettings區段的key值</param>
        /// <param name="value">欲修改的設定值</param>
        /// <param name="handler">處理動作委派</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>處理是否成功</returns>
        private bool ProcessConfigSetting( string key, string value, out string message, Action handler )
        {
            message = "";

            if ( key.IsNullOrEmpty() )
            {
                message = $"The key of appSetting is not allow null or empty.";
                return false;
            }

            try
            {
                handler();

                _config.Save( ConfigurationSaveMode.Modified );
                ConfigurationManager.RefreshSection( "appSettings" );
            }
            catch ( Exception ex )
            {
                message = ex.Message;
                return false;
            }

            return true;
        }

        #endregion 宣告私有的實體方法
    }
}
