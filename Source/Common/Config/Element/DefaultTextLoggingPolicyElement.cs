﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>ZayniFramework 框架預設的文字日誌異常處理政策Config元素
    /// </summary>
    public sealed class DefaultTextLoggingPolicyElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>是否啟用ZayniFramework 框架預設的例外處理政策
        /// </summary>
        private const string ENABLE_DEFAULT_POLICY = "enableDefaultPolicy";

        /// <summary>是否啟用ZayniFramework 框架預設的例外處理政策的事件檢視器記錄功能
        /// </summary>
        private const string DEFAULT_POLICY_EVENTLOG_ENABLE = "enableDefaultPolicyEventLog";
        
        /// <summary>ZayniFramework 框架預設例外處理政策的文字Log檔路徑
        /// </summary>
        private const string DEFAULT_POLICY_LOG_FILE_PATH = "defaultPolicyLogFilePath";

        /// <summary>是否需要重新拋出程式異常
        /// </summary>
        private const string NEED_RETHROW = "needRethrow";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>是否啟用ZayniFramework 框架預設的例外處理政策
        /// </summary>
        [ConfigurationProperty( ENABLE_DEFAULT_POLICY, IsRequired = false )]
        public bool IsEnableDefaultPolicy
        {
            get
            {
                bool result;

                string setting = this[ ENABLE_DEFAULT_POLICY ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    //throw new ConfigurationErrorsException( "enableDefaultPolicy設定值必須為布林值" );
                    return false;
                }

                return result;
            }
            set
            {
                this[ ENABLE_DEFAULT_POLICY ] = value + "";
            }
        }

        /// <summary>是否啟用ZayniFramework 框架預設的例外處理政策的事件檢視器記錄功能
        /// </summary>
        [ConfigurationProperty( DEFAULT_POLICY_EVENTLOG_ENABLE, IsRequired = false )]
        public bool IsEnableDefaultPolicyEventLog
        {
            get
            {
                bool result;

                string setting = this[ DEFAULT_POLICY_EVENTLOG_ENABLE ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "enableDefaultPolicyEventLog設定值必須為布林值" );
                }

                return result;
            }
            set
            {
                this[ DEFAULT_POLICY_EVENTLOG_ENABLE ] = value;
            }
        }

        /// <summary>ZayniFramework 框架預設例外處理政策的文字Log檔路徑
        /// </summary>
        [ConfigurationProperty( DEFAULT_POLICY_LOG_FILE_PATH, IsRequired = false )]
        public string DefaultPolicyLogPath
        {
            get
            {
                return this[ DEFAULT_POLICY_LOG_FILE_PATH ] + "";
            }
            set
            {
                this[ DEFAULT_POLICY_LOG_FILE_PATH ] = value;
            }
        }

        /// <summary>是否需要重新拋出程式異常
        /// </summary>
        [ConfigurationProperty( NEED_RETHROW, IsRequired = false )]
        public bool NeedRethrow
        {
            get
            {
                bool result;

                string setting = this[ NEED_RETHROW ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "needRethrow設定值必須為布林值" );
                }

                return result;
            }
            set
            {
                this[ NEED_RETHROW ] = value + "";
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
