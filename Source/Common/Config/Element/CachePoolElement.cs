﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>記憶體快取池的設定Config元素
    /// </summary>
    public sealed class CachePoolElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>是否啟用回收機制
        /// </summary>
        private const string RESET_ENABLE     = "resetEnable";
        
        /// <summary>回收時間間隔(單位為秒)
        /// </summary>
        private const string RESET_INTERVAL   = "cleanerIntervalSecond";

        /// <summary>最大容量數
        /// </summary>
        private const string MAX_CAPACITY     = "maxCapacity";

        /// <summary>常駐資料更新時間間隔(單位為分鐘)
        /// </summary>
        private const string REFRESH_INTERVAL = "refreshIntervalMinute";

        /// <summary>快取處理器集合
        /// </summary>
        private const string HANDLERS         = "CachingHandlers";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>是否啟用回收機制
        /// </summary>
        [ConfigurationProperty( RESET_ENABLE, IsRequired = false )]
        public bool IsResetEnable
        {
            get
            {
                bool result;

                string setting = this[ RESET_ENABLE ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    return true;
                }

                return result;
            }
            set
            {
                this[ RESET_ENABLE ] = value;
            }
        }

        /// <summary>自動回收時間間隔(單位為秒)
        /// </summary>
        [ConfigurationProperty( RESET_INTERVAL, IsRequired = false )]
        public int CleanerIntervalSecond
        {
            get
            {
                int result;

                string setting = this[ RESET_INTERVAL ] + "";

                if ( !int.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "cleanerIntervalSecond設定值必須為正整數" );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "cleanerIntervalSecond設定值必須為正整數" );
                }

                return result;
            }
            set
            {
                this[ RESET_INTERVAL ] = value;
            }
        }

        /// <summary>最大容量數
        /// </summary>
        [ConfigurationProperty( MAX_CAPACITY, IsRequired = false )]
        public int MaxCapcaity
        {
            get
            {
                int result;

                string setting = this[ MAX_CAPACITY ] + "";

                if ( !int.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "maxCapacity設定值必須為正整數" );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "maxCapacity設定值必須為正整數" );
                }

                return result;
            }
            set
            {
                this[ MAX_CAPACITY ] = value;
            }
        }

        /// <summary>常駐資料更新時間間隔(單位為分鐘)
        /// </summary>
        [ConfigurationProperty( REFRESH_INTERVAL, IsRequired = false )]
        public int RefreshInterval
        {
            get
            {
                int result;

                string setting = this[ REFRESH_INTERVAL ] + "";

                if ( !int.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "refreshIntervalMinute設定值必須為正整數" );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "refreshIntervalMinute設定值必須為正整數" );
                }

                return result;
            }
            set
            {
                this[ REFRESH_INTERVAL ] = value;
            }
        }

        /// <summary>快取處理器集合
        /// </summary>
        [ConfigurationProperty( HANDLERS, IsRequired = false )]
        public CachingHandlersConfigCollection CachingHandlers
        {
            get
            {
                return (CachingHandlersConfigCollection)this[ HANDLERS ];
            }
            set
            {
                this[ HANDLERS ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
