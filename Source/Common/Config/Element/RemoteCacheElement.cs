﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>遠端快取設定的 Config 元素
    /// </summary>
    public sealed class RemoteCacheElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>存取 RemoteCache 遠端快取的 serviceClientConfig.json 設定檔的完整絕對路徑<para/>
        /// - 當 serviceClientConfig.json 設定檔的路徑與 runtime 執行時期的 dll 組件，不再相同的目錄時，並且又要使用 RemoteCache 的時候，才會需要設定此參數。<para/>
        /// - 目前主要使用情境為在 MSTest unit test 執行測試的情況時，會需要使用到此參數設定。<para/>
        /// </summary>
        private const string SERVICE_CLIENT_CONFIG_PATH = "serviceClientConfigPath";

        /// <summary>預設的遠端快取客戶端名稱
        /// </summary>
        private const string DEFAULT_CACHE_CLIENT = "defaultCacheClientName";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>存取 RemoteCache 遠端快取的 serviceClientConfig.json 設定檔的完整絕對路徑<para/>
        /// - 當 serviceClientConfig.json 設定檔的路徑與 runtime 執行時期的 dll 組件，不再相同的目錄時，並且又要使用 RemoteCache 的時候，才會需要設定此參數。<para/>
        /// - 目前主要使用情境為在 MSTest unit test 執行測試的情況時，會需要使用到此參數設定。<para/>
        /// </summary>
        [ConfigurationProperty( SERVICE_CLIENT_CONFIG_PATH, IsRequired = false )]
        public string ServiceClientConfigPath
        {
            get
            {
                return this[ SERVICE_CLIENT_CONFIG_PATH ] + "";
            }
            set
            {
                this[ SERVICE_CLIENT_CONFIG_PATH ] = value;
            }
        }

        /// <summary>預設的遠端快取客戶端名稱
        /// </summary>
        [ConfigurationProperty( DEFAULT_CACHE_CLIENT, IsRequired = false )]
        public string DefaultCacheClientName
        {
            get
            {
                return this[ DEFAULT_CACHE_CLIENT ] + "";
            }
            set
            {
                this[ DEFAULT_CACHE_CLIENT ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法
    }
}
