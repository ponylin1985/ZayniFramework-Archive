﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>Redis 服務快取的 Config 設定元素
    /// </summary>
    public sealed class RedisCacheElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>Redis Cache 的連線設定名稱
        /// </summary>
        private const string NAME = "name";

        /// <summary>是否為預設的 Redis Cache Server 連線
        /// </summary>
        private const string IS_DEFAULT = "isDefault";

        /// <summary>是否啟用資料庫動作紀錄
        /// </summary>
        private const string DB_ACTION_LOG = "dbActionLog";

        /// <summary>ElasticSearch 日誌紀錄器設定名稱
        /// </summary>
        private const string ES_LOGGER_NAME = "esLoggerName";

        /// <summary>Redis Cache Server 的 IP 或主機位址
        /// </summary>
        private const string HOST = "host";
        
        /// <summary>Redis Cache Server 的通訊連接阜號
        /// </summary>
        private const string PORT = "port";

        /// <summary>連接至 Redis Cache Server 的密碼
        /// </summary>
        private const string PASSWORD = "password";

        /// <summary>Redis Cache Server 的資料庫編號
        /// </summary>
        private const string DB_NUMBER = "dbNumber";

        /// <summary>Redis Cache Server 的建立連線 Timeout 的毫秒數
        /// </summary>
        private const string CONNECT_TIMEOUT = "connectTimeout";

        /// <summary>對 Redis Cache Server 的發送同步指令的 Timeout 毫秒數
        /// </summary>
        private const string SYNC_TIMEOUT = "syncTimeout";

        /// <summary>等待 Redis Cache Server 的同步指令回應的 Timeout 毫秒數
        /// </summary>
        private const string RESPONSE_TIMEOUT = "responseTimeout";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>Redis Cache 的連線設定名稱
        /// </summary>
        [ConfigurationProperty( NAME, IsRequired = false )]
        public string Name
        {
            get
            {
                return this[ NAME ] + "";
            }
            set
            {
                this[ NAME ] = value;
            }
        }

        /// <summary>是否為預設的 Redis Cache Server 連線
        /// </summary>
        [ConfigurationProperty( IS_DEFAULT, IsRequired = false )]
        public bool IsDefault
        {
            get
            {
                string setting = this[ IS_DEFAULT ] + "";

                if ( !bool.TryParse( setting, out bool result ) )
                {
                    return false;
                }

                return result;
            }
            set
            {
                this[ IS_DEFAULT ] = value;
            }
        }

        /// <summary>是否啟用資料庫動作紀錄
        /// </summary>
        [ConfigurationProperty( DB_ACTION_LOG, IsRequired = false )]
        public bool DbActionLog
        {
            get
            {
                string setting = this[ DB_ACTION_LOG ] + "";

                if ( !bool.TryParse( setting, out bool result ) )
                {
                    return false;
                }

                return result;
            }
            set
            {
                this[ DB_ACTION_LOG ] = value;
            }
        }

        /// <summary>ElasticSearch 日誌紀錄器設定名稱
        /// </summary>
        [ConfigurationProperty( ES_LOGGER_NAME, IsRequired = false )]
        public string ESLoggerName
        {
            get
            {
                return this[ ES_LOGGER_NAME ] + "";
            }
            set
            {
                this[ ES_LOGGER_NAME ] = value;
            }
        }

        /// <summary>Redis Cache Server 的 IP 或主機位址
        /// </summary>
        [ConfigurationProperty( HOST, IsRequired = false )]
        public string Host
        {
            get
            {
                return this[ HOST ] + "";
            }
            set
            {
                this[ HOST ] = value;
            }
        }

        /// <summary>Redis Cache Server 的通訊連接阜號
        /// </summary>
        [ConfigurationProperty( PORT, IsRequired = false )]
        public int Port
        {
            get
            {
                string setting = this[ PORT ] + "";

                if ( !int.TryParse( setting, out int result ) )
                {
                    throw new ConfigurationErrorsException( "ZayniFramework/CachingSettings/RedisCache port must positive integer." );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "ZayniFramework/CachingSettings/RedisCache port must positive integer." );
                }

                return result;
            }
            set
            {
                this[ PORT ] = value;
            }
        }

        /// <summary>連接至 Redis Cache Server 的密碼
        /// </summary>
        [ConfigurationProperty( PASSWORD, IsRequired = false )]
        public string Password
        {
            get
            {
                return this[ PASSWORD ] + "";
            }
            set
            {
                this[ PASSWORD ] = value;
            }
        }

        /// <summary>Redis Cache Server 的資料庫編號
        /// </summary>
        [ConfigurationProperty( DB_NUMBER, IsRequired = false )]
        public int DatabaseNo
        {
            get
            {
                string setting = this[ DB_NUMBER ] + "";

                if ( !int.TryParse( setting, out int result ) )
                {
                    return 0;
                }

                if ( result < 0 )
                {
                    return 0;
                }

                return result;
            }
            set
            {
                this[ DB_NUMBER ] = value;
            }
        }

        /// <summary>Redis Cache Server 的建立連線 Timeout 的毫秒數，預設為 3 秒。
        /// </summary>
        [ConfigurationProperty( CONNECT_TIMEOUT, IsRequired = false )]
        public int ConnectTimeout
        {
            get
            {
                string setting = this[ CONNECT_TIMEOUT ] + "";

                if ( !int.TryParse( setting, out int result ) )
                {
                    return (int)TimeSpan.FromSeconds( 3 ).TotalMilliseconds;
                }

                if ( result <= 0 )
                {
                    return (int)TimeSpan.FromSeconds( 3 ).TotalMilliseconds;
                }

                return result;
            }
            set
            {
                this[ CONNECT_TIMEOUT ] = value;
            }
        }

        /// <summary>對 Redis Cache Server 的發送同步指令的 Timeout 毫秒數，預設為 3 秒。
        /// </summary>
        [ConfigurationProperty( SYNC_TIMEOUT, IsRequired = false )]
        public int SyncTimeout
        {
            get
            {
                string setting = this[ SYNC_TIMEOUT ] + "";

                if ( !int.TryParse( setting, out int result ) )
                {
                    return (int)TimeSpan.FromSeconds( 3 ).TotalMilliseconds;
                }

                if ( result <= 0 )
                {
                    return (int)TimeSpan.FromSeconds( 3 ).TotalMilliseconds;
                }

                return result;
            }
            set
            {
                this[ SYNC_TIMEOUT ] = value;
            }
        }

        /// <summary>等待 Redis Cache Server 的同步指令回應的 Timeout 毫秒數，預設為 3 秒。
        /// </summary>
        [ConfigurationProperty( RESPONSE_TIMEOUT, IsRequired = false )]
        public int ResponseTimeout
        {
            get
            {
                string setting = this[ RESPONSE_TIMEOUT ] + "";

                if ( !int.TryParse( setting, out int result ) )
                {
                    return (int)TimeSpan.FromSeconds( 3 ).TotalMilliseconds;
                }

                if ( result <= 0 )
                {
                    return (int)TimeSpan.FromSeconds( 3 ).TotalMilliseconds;
                }

                return result;
            }
            set
            {
                this[ RESPONSE_TIMEOUT ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法
    }
}
