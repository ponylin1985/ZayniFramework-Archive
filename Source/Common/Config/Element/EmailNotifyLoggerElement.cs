﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>電子郵件日誌警示 Config 設定元素
    /// </summary>
    public sealed class EmailNotifyLoggerElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>設定名稱
        /// </summary>
        private const string LOGGER_NAME = "name";

        /// <summary>SMTP 通訊協定阜號
        /// </summary>
        private const string SMTP_PORT = "smtpPort";

        /// <summary>SMTP Server伺服器主機名稱或IP位址
        /// </summary>
        private const string SMTP_HOST = "smtpHost";

        /// <summary>是否啟用SSL連線
        /// </summary>
        private const string ENABLE_SSL = "enableSsl";

        /// <summary>SMTP Server連線的網域
        /// </summary>
        private const string SMTP_DOMAIN = "smtpDomain";

        /// <summary>SMTP 帳號
        /// </summary>
        private const string SMTP_ACCOUNT = "smtpAccount";

        /// <summary>SMTP 密碼
        /// </summary>
        private const string SMTP_PASSWORD = "smtpPassword";

        /// <summary>寄件人電子郵件信箱地址
        /// </summary>
        private const string FROM_ADDRESS = "fromEmailAddress";

        /// <summary>寄件人顯示名稱
        /// </summary>
        private const string FROM_DISPLAY_NAME = "fromDisplayName";

        /// <summary>收件人電子郵件信箱地址
        /// </summary>
        private const string TO_ADDRESS = "toEmailAddress";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>Logger 設定名稱
        /// </summary>
        [ConfigurationProperty( LOGGER_NAME, IsRequired = false )]
        public string Name
        {
            get
            {
                return this[ LOGGER_NAME ] + "";
            }
            set
            {
                this[ LOGGER_NAME ] = value;
            }
        }

        /// <summary>SMTP通訊協定阜號
        /// </summary>
        [ConfigurationProperty( SMTP_PORT, IsRequired = false )]
        public int SmtpPort
        {
            get
            {
                string setting = this[ SMTP_PORT ] + "";

                if ( !int.TryParse( setting, out int result ) )
                {
                    throw new ConfigurationErrorsException( "smtpPort 設定值必須為正整數" );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "smtpPort 設定值必須為正整數" );
                }

                return result;
            }
            set
            {
                this[ SMTP_PORT ] = value;
            }
        }

        /// <summary>SMTP Server伺服器主機名稱或IP位址
        /// </summary>
        [ConfigurationProperty( SMTP_HOST, IsRequired = false )]
        public string SmtpHost
        {
            get
            {
                return this[ SMTP_HOST ] + "";
            }
            set
            {
                this[ SMTP_HOST ] = value;
            }
        }

        /// <summary>是否啟用SSL連線
        /// </summary>
        [ConfigurationProperty( ENABLE_SSL, IsRequired = false )]
        public bool IsEnableSsl
        {
            get
            {
                string setting = this[ ENABLE_SSL ] + "";

                if ( !bool.TryParse( setting, out bool result ) )
                {
                    return false;
                }

                return result;
            }
            set
            {
                this[ ENABLE_SSL ] = value + "";
            }
        }

        /// <summary>SMTP Server連線的網域
        /// </summary>
        [ConfigurationProperty( SMTP_DOMAIN, IsRequired = false )]
        public string SmtpDomain
        {
            get
            {
                return this[ SMTP_DOMAIN ] + "";
            }
            set
            {
                this[ SMTP_DOMAIN ] = value;
            }
        }

        /// <summary>SMTP帳號
        /// </summary>
        [ConfigurationProperty( SMTP_ACCOUNT, IsRequired = false )]
        public string SmtpAccount
        {
            get
            {
                return this[ SMTP_ACCOUNT ] + "";
            }
            set
            {
                this[ SMTP_ACCOUNT ] = value;
            }
        }

        /// <summary>SMTP密碼
        /// </summary>
        [ConfigurationProperty( SMTP_PASSWORD, IsRequired = false )]
        public string SmtpPassword
        {
            get
            {
                return this[ SMTP_PASSWORD ] + "";
            }
            set
            {
                this[ SMTP_PASSWORD ] = value;
            }
        }

        /// <summary>寄件人電子郵件信箱地址
        /// </summary>
        [ConfigurationProperty( FROM_ADDRESS, IsRequired = false )]
        public string FromEmailAddress
        {
            get
            {
                return this[ FROM_ADDRESS ] + "";
            }
            set
            {
                this[ FROM_ADDRESS ] = value;
            }
        }

        /// <summary>寄件人顯示名稱
        /// </summary>
        [ConfigurationProperty( FROM_DISPLAY_NAME, IsRequired = false )]
        public string FromDisplayName
        {
            get
            {
                return this[ FROM_DISPLAY_NAME ] + "";
            }
            set
            {
                this[ FROM_DISPLAY_NAME ] = value;
            }
        }

        /// <summary>收件人電子郵件信箱地址
        /// </summary>
        [ConfigurationProperty( TO_ADDRESS, IsRequired = false )]
        public string ToEmailAddress
        {
            get
            {
                return this[ TO_ADDRESS ] + "";
            }
            set
            {
                this[ TO_ADDRESS ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法
    }
}
