﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>MySQL Memory Engine 遠端資料快取的 Config 組態元素
    /// </summary>
    public sealed class MySQLMemoryCacheElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>預設的快取資料庫連線名稱
        /// </summary>
        private const string DEFAULT_CONNECTION_NAME = "defaultConnectionName";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>預設的快取資料庫連線名稱
        /// </summary>
        [ConfigurationProperty( DEFAULT_CONNECTION_NAME, IsRequired = false )]
        public string DefaultConnectionName
        {
            get
            {
                return this[ DEFAULT_CONNECTION_NAME ] + "";
            }
            set
            {
                this[ DEFAULT_CONNECTION_NAME ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法
    }
}
