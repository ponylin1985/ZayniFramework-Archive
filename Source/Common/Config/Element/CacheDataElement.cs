﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>快取資料的設定Config元素
    /// </summary>
    public sealed class CacheDataElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>快取資料的逾時時間間隔設定(單位為分鐘)
        /// </summary>
        private const string TIMEOUT = "timeoutInterval";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>快取資料的逾時時間間隔設定(單位為分鐘)
        /// </summary>
        [ConfigurationProperty( TIMEOUT, IsRequired = false )]
        public int TimeoutInterval
        {
            get
            {
                int result;

                string setting = this[ TIMEOUT ] + "";

                if ( !int.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "timeoutInterval設定值必須為正整數" );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "timeoutInterval設定值必須為正整數" );
                }

                return result;
            }
            set
            {
                this[ TIMEOUT ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
