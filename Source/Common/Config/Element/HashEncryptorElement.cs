﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>雜湊加密元色設定元素
    /// </summary>
    public sealed class HashEncryptorElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>是否需要Salt
        /// </summary>
        private const string NEED_SALT = "needSalt";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>雜湊加密是否需要Salt
        /// </summary>
        [ConfigurationProperty( NEED_SALT, IsRequired = false )]
        public bool NeedSalt
        {
            get
            {
                bool   result;

                string setting = this[ NEED_SALT ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "needSalt設定值必須為布林值" );
                }

                return result;
            }
            set
            {
                this[ NEED_SALT ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
