﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>預設文字日誌記錄Config元素
    /// </summary>
    public sealed class DefaultTextLoggerElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>日誌名稱
        /// </summary>
        private const string LOG_NAME = "name";

        /// <summary>是否啟用預設的文字日誌記錄功能屬性名稱
        /// </summary>
        private const string ENABLE_DEFAULT_LOGGER = "enable";

        /// <summary>文字日誌檔大小上限 (單位為MB)
        /// </summary>
        private const string MAX_SIZE = "maxSize";

        /// <summary>是否啟用 Console 主控台日誌輸出
        /// </summary>
        private const string ENABLE_CONSOLE_OUTPUT = "consoleOutput";

        /// <summary>標準 Console 輸出的字體顏色
        /// </summary>
        private const string CONSOLE_OUTPUT_COLOR = "consoleColor";

        /// <summary>Email 信件警示的 Logger 設定名稱
        /// </summary>
        private const string EMAIL_NOTIFY_LOGGER_NAME = "emailNotifyLoggerName";

        /// <summary>ElasticSearch Logger 日誌器設定名稱
        /// </summary>
        private const string ES_LOGGER_NAME = "esLoggerName";

        /// <summary>資料庫日誌紀錄的連線名稱
        /// </summary>
        private const string DB_LOGGER_NAME = "dbLoggerName";

        /// <summary>資料庫日誌的資料庫種類: MSSQL 或 MySQL
        /// </summary>
        private const string DB_LOGGER_TYPE = "dbLoggerType";

        /// <summary>日誌記錄文件Config元素
        /// </summary>
        private const string LOG_FILE_ELEMENT = "LogFile";

        /// <summary>日誌事件層級過濾Config元素
        /// </summary>
        private const string FILTER_ELEMENT = "Filter";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>日誌名稱
        /// </summary>
        [ConfigurationProperty( LOG_NAME, IsRequired = false )]
        public string LogName
        {
            get
            {
                return this[ LOG_NAME ] + "";
            }
            set
            {
                this[ LOG_NAME ] = value;
            }
        }

        /// <summary>是否啟用預設的文字日誌記錄功能
        /// </summary>
        [ConfigurationProperty( ENABLE_DEFAULT_LOGGER, IsRequired = false )]
        public bool DefaultTextLoggerEnable
        {
            get
            {
                string setting = this[ ENABLE_DEFAULT_LOGGER ] + "";

                if ( !bool.TryParse( setting, out bool result ) )
                {
                    throw new ConfigurationErrorsException( $"Config LoggingSettings/DefaultTextLogger/add/enable must be valid boolean literal." );
                }

                return result;
            }
            set
            {
                this[ ENABLE_DEFAULT_LOGGER ] = value;
            }
        }

        /// <summary>文字日誌檔大小上限 (單位為 MB，上限為 15 MB)
        /// </summary>
        [ConfigurationProperty( MAX_SIZE, IsRequired = false )]
        public decimal MaxSize
        {
            get
            {
                string setting = this[ MAX_SIZE ] + "";

                if ( !decimal.TryParse( setting, out decimal result ) )
                {
                    // 20140616 Pony Says: 預設給定 15MB 為上限
                    return 15M;
                }

                return result;
            }
            set
            {
                this[ MAX_SIZE ] = value;
            }
        }

        /// <summary>是否啟用 Console 主控台日誌輸出
        /// </summary>
        [ConfigurationProperty( ENABLE_CONSOLE_OUTPUT, IsRequired = false )]
        public bool EnableConsoleOutput
        {
            get
            {
                string setting = this[ ENABLE_CONSOLE_OUTPUT ] + "";

                if ( !bool.TryParse( setting, out bool result ) )
                {
                    throw new ConfigurationErrorsException( $"Config LoggingSettings/DefaultTextLogger/add/consoleOutput must be valid boolean literal." );
                }

                return result;
            }
            set
            {
                this[ ENABLE_CONSOLE_OUTPUT ] = value;
            }
        }

        /// <summary>標準 Console 輸出的字體顏色
        /// </summary>
        [ConfigurationProperty( CONSOLE_OUTPUT_COLOR, IsRequired = false, DefaultValue = "Gray" )]
        public string ConsoleOutputColor
        {
            get
            {
                return this[ CONSOLE_OUTPUT_COLOR ] + "";
            }
            set
            {
                this[ CONSOLE_OUTPUT_COLOR ] = value;
            }
        }

        /// <summary>Email 信件警示的 Logger 設定名稱
        /// </summary>
        [ConfigurationProperty( EMAIL_NOTIFY_LOGGER_NAME, IsRequired = false )]
        public string EmailNotifyLoggerName
        {
            get
            {
                return this[ EMAIL_NOTIFY_LOGGER_NAME ] + "";
            }
            set
            {
                this[ EMAIL_NOTIFY_LOGGER_NAME ] = value;
            }
        }

        /// <summary>ElasticSearch 的 Logger 日誌器設定名稱
        /// </summary>
        [ConfigurationProperty( ES_LOGGER_NAME, IsRequired = false )]
        public string ElasticSearchLoggerName
        {
            get
            {
                return this[ ES_LOGGER_NAME ] + "";
            }
            set
            {
                this[ ES_LOGGER_NAME ] = value;
            }
        }

        /// <summary>資料庫日誌紀錄的連線名稱
        /// </summary>
        [ConfigurationProperty( DB_LOGGER_NAME, IsRequired = false )]
        public string DbLoggerName
        {
            get
            {
                return this[ DB_LOGGER_NAME ] + "";
            }
            set
            {
                this[ DB_LOGGER_NAME ] = value;
            }
        }

        /// <summary>資料庫日誌的資料庫種類: MSSQL 或 MySQL
        /// </summary>
        [ConfigurationProperty( DB_LOGGER_TYPE, IsRequired = false )]
        public string DbLoggerType
        {
            get
            {
                return this[ DB_LOGGER_TYPE ] + "";
            }
            set
            {
                this[ DB_LOGGER_TYPE ] = value;
            }
        }

        /// <summary>日誌記錄文件Config元素
        /// </summary>
        [ConfigurationProperty( LOG_FILE_ELEMENT, IsRequired = false )]
        public LogFileElement LogFile
        {
            get
            {
                return (LogFileElement)this[ LOG_FILE_ELEMENT ];
            }
            set
            {
                this[ LOG_FILE_ELEMENT ] = value;
            }
        }

        /// <summary>日誌事件層級過濾Config元素
        /// </summary>
        [ConfigurationProperty( FILTER_ELEMENT, IsRequired = false )]
        public FilterElement LogEventFilter
        {
            get
            {
                return (FilterElement)this[ FILTER_ELEMENT ];
            }
            set
            {
                this[ FILTER_ELEMENT ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
