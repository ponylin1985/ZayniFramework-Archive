﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>資料庫型別代碼對應Config設定元素
    /// </summary>
    public sealed class DbTypeCodeMappingElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>MSSQL資料庫型別對應Config設定區段
        /// </summary>
        private const string MSSQL_SECTION = "SqlDbType";

        /// <summary>Oracle資料庫型別對應Config設定區段
        /// </summary>
        private const string ORACLE_SECTION = "OracleType";

        /// <summary>MySQL資料庫型別對應Config設定區段
        /// </summary>
        private const string MYSQL_SECTION = "MySqlDbType";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>MSSQL資料庫型別對應Config設定區段
        /// </summary>
        [ConfigurationProperty( MSSQL_SECTION, IsRequired = false )]
        public SqlDbTypeConfigCollection SqlDbTypeManpping
        {
            get
            {
                return (SqlDbTypeConfigCollection)this[ MSSQL_SECTION ];
            }
            set
            {
                this[ MSSQL_SECTION ] = value;
            }
        }

        /// <summary>Oracle資料庫型別對應Config設定區段
        /// </summary>
        [ConfigurationProperty( ORACLE_SECTION, IsRequired = false )]
        public OracleTypeConfigCollection OracleDbTypeMapping
        {
            get
            {
                return (OracleTypeConfigCollection)this[ ORACLE_SECTION ];
            }
            set
            {
                this[ ORACLE_SECTION ] = value;
            }
        }

        /// <summary>MySQL資料庫型別對應Config設定區段
        /// </summary>
        [ConfigurationProperty( MYSQL_SECTION, IsRequired = false )]
        public MySqlDbTypeConfigCollection MySqlDbTypeMapping
        {
            get
            {
                return (MySqlDbTypeConfigCollection)this[ MYSQL_SECTION ];
            }
            set
            {
                this[ MYSQL_SECTION ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
