﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>資料庫行別代碼對應Config元素
    /// </summary>
    public sealed class DbTypeMappingElement : ConfigurationElement
    {
        #region 宣告Config元素屬性的字串常數
        
        /// <summary>DbTypeCode型別的名稱
        /// </summary>
        private const string NAME = "name";

        /// <summary>實際對應型別的代碼名稱
        /// </summary>
        private const string VALUE = "value";

        #endregion 宣告Config元素屬性的字串常數


        #region 宣告Config元素設定值的公開屬性存取子

        /// <summary>DbTypeCode型別的名稱
        /// </summary>
        [ConfigurationProperty( NAME, IsRequired = false )]
        public string Name
        {
            get
            {
                return this[ NAME ] + "";
            }
            set
            {
                this[ NAME ] = value;
            }
        }

        /// <summary>實際對應型別的代碼名稱
        /// </summary>
        [ConfigurationProperty( VALUE, IsRequired = false )]
        public string Value
        {
            get
            {
                return this[ VALUE ] + "";
            }
            set
            {
                this[ VALUE ] = value;
            }
        }

        #endregion 宣告Config元素設定值的公開屬性存取子


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
