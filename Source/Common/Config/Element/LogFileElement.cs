﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>日誌記錄文件Config元素
    /// </summary>
    public sealed class LogFileElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>日誌檔案的完整路徑
        /// </summary>
        private const string PATH = "path";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>日誌檔案的完整路徑
        /// </summary>
        [ConfigurationProperty( PATH, IsRequired = true )]
        public string Path
        {
            get
            {
                return this[ PATH ] + "";
            }
            set
            {
                this[ PATH ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
