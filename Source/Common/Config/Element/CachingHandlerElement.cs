﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>快取處理器Config元素
    /// </summary>
    public sealed class CachingHandlerElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>處理器名稱
        /// </summary>
        private const string NAME = "name";

        /// <summary>執行時間間隔 (單位為分鐘)
        /// </summary>
        private const string INTERVAL = "intervalMinute";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>處理器名稱
        /// </summary>
        [ConfigurationProperty( NAME, IsRequired = false )]
        public string HandlerName
        {
            get
            {
                return this[ NAME ] + "";
            }
            set
            {
                this[ NAME ] = value;
            }
        }

        /// <summary>執行時間間隔 (單位為分鐘)
        /// </summary>
        [ConfigurationProperty( INTERVAL, IsRequired = false )]
        public int IntervalMinute
        {
            get
            {
                int result;

                string setting = this[ INTERVAL ] + "";

                if ( !int.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "intervalMinute設定值必須為正整數" );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "intervalMinute設定值必須為正整數" );
                }

                return result;
            }
            set
            {
                this[ INTERVAL ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
