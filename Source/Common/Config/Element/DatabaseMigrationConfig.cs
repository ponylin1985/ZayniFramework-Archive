﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>資料庫 Schema Migration Config 設定元素
    /// </summary>
    public class DatabaseMigrationConfig : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>目標 Migration 資料庫設定名稱
        /// </summary>
        private const string TARGET_DATABASE = "targetDatabase";

        /// <summary>資料庫 Schema Migration 的腳本路徑
        /// </summary>
        private const string MIGRATION_SCRIPT_DIR = "migrationScriptDirectory";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>目標 Migration 資料庫設定名稱
        /// </summary>
        [ConfigurationProperty( TARGET_DATABASE, IsRequired = false )]
        public string TargetDatabase
        {
            get
            {
                return this[ TARGET_DATABASE ] + "";
            }
            set
            {
                this[ TARGET_DATABASE ] = value;
            }
        }

        /// <summary>資料庫 Schema Migration 的腳本路徑
        /// </summary>
        [ConfigurationProperty( MIGRATION_SCRIPT_DIR, IsRequired = false )]
        public string MigrationScriptDir
        {
            get
            {
                return this[ MIGRATION_SCRIPT_DIR ] + "";
            }
            set
            {
                this[ MIGRATION_SCRIPT_DIR ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns>是否為唯讀</returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫方法
    }
}
