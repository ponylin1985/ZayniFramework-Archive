﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>資料存取連線 Config 元素
    /// </summary>
    public sealed class ConnectionElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>連線字串組態名稱
        /// </summary>
        private const string NAME = "name";

        /// <summary>資料庫提供者
        /// </summary>
        private const string PROVIDER = "dataBaseProvider";

        /// <summary>是否啟用SQL Logging
        /// </summary>
        private const string SQL_LOG_ENABLE = "sqlLogEnable";

        /// <summary>ElasticSearch 日誌紀錄器設定名稱
        /// </summary>
        private const string ES_LOGGER_NAME = "esLoggerName";

        /// <summary>資料庫連線字串 AES 加密的金鑰字串
        /// </summary>
        private const string ENCRYPT_KEY = "encryptKey";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>連線字串組態名稱
        /// </summary>
        [ConfigurationProperty( NAME, IsRequired = false )]
        public string Name
        {
            get
            {
                return this[ NAME ] + "";
            }
            set
            {
                this[ NAME ] = value;
            }
        }

        /// <summary>資料庫提供者
        /// </summary>
        [ConfigurationProperty( PROVIDER, IsRequired = false )]
        public string DatabaseProvider
        {
            get
            {
                return this[ PROVIDER ] + "";
            }
            set
            {
                this[ PROVIDER ] = value;
            }
        }

        /// <summary>是否啟用 SQL Profile Logging
        /// </summary>
        [ConfigurationProperty( SQL_LOG_ENABLE, IsRequired = false )]
        public bool IsSQLLogEnable
        {
            get
            {
                bool result;

                string setting = this[ SQL_LOG_ENABLE ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    return false;
                }

                return result;
            }
            set
            {
                this[ SQL_LOG_ENABLE ] = value + "";
            }
        }

        /// <summary>ElasticSearch 日誌紀錄器設定名稱
        /// </summary>
        [ConfigurationProperty( ES_LOGGER_NAME, IsRequired = false )]
        public string ESLoggerName
        {
            get
            {
                return this[ ES_LOGGER_NAME ] + "";
            }
            set
            {
                this[ ES_LOGGER_NAME ] = value;
            }
        }

        /// <summary>資料庫連線字串 AES 加密的金鑰字串
        /// * 如果在 config 中，此設定值不為 null 或空字串，代表資料庫連線字串有被加密。
        /// </summary>
        [ConfigurationProperty( ENCRYPT_KEY, IsRequired = false )]
        public string EncryptKey 
        {
            get => this[ ENCRYPT_KEY ] + "";
            set => this[ ENCRYPT_KEY ] = value;
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法
    }
}
