﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>日誌事件層級過濾Config元素
    /// </summary>
    public sealed class FilterElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>過濾方式
        /// </summary>
        private const string FILTER_TYPE = "filterType";

        /// <summary>要過濾掉的事件層級種類
        /// </summary>
        private const string CATEGORY = "category";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>過濾方式
        /// </summary>
        [ConfigurationProperty( FILTER_TYPE, IsRequired = false )]
        public string FilterType
        {
            get
            {
                return this[ FILTER_TYPE ] + "";
            }
            set
            {
                this[ FILTER_TYPE ] = value;
            }
        }

        /// <summary>要過濾掉的事件層級種類
        /// </summary>
        [ConfigurationProperty( CATEGORY, IsRequired = false )]
        public string Category
        {
            get
            {
                return this[ CATEGORY ] + "";
            }
            set
            {
                this[ CATEGORY ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
