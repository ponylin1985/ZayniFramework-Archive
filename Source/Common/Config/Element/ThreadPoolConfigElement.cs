using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>.NET CLR 執行時期 Managed Thread Pool 組態設定
    /// </summary>
    public class ThreadPoolConfigElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>最小 Worker Thread 數量
        /// </summary>
        private const string MIN_WORKER_THREADS = "minWorkerThreads";

        /// <summary>最小 IOCP Thread 數量
        /// </summary>
        private const string MIN_IOCP_THREADS = "minIOCPThreads";

        /// <summary>最大 Worker Thread 數量
        /// </summary>
        private const string MAX_WORKER_THREADS = "maxWorkerThreads";

        /// <summary>最大 IOCP Thread 數量
        /// </summary>
        private const string MAX_IOCP_THREADS = "maxIOCPThreads";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>最小 Worker Thread 數量
        /// </summary>
        [ConfigurationProperty( MIN_WORKER_THREADS, IsRequired = false )]
        public int MinWorkerThreads
        {
            get => int.TryParse( this[ MIN_WORKER_THREADS ] + "", out var result ) ? result : 0;
            set => this[ MIN_WORKER_THREADS ] = value;
        }

        /// <summary>最小 IOCP Thread 數量
        /// </summary>
        [ConfigurationProperty( MIN_IOCP_THREADS, IsRequired = false )]
        public int MinIOCPThreads
        {
            get => int.TryParse( this[ MIN_IOCP_THREADS ] + "", out var result ) ? result : 0;
            set => this[ MIN_IOCP_THREADS ] = value;
        }

        /// <summary>最大 Worker Thread 數量
        /// </summary>
        [ConfigurationProperty( MAX_WORKER_THREADS, IsRequired = false )]
        public int MaxWorkerThreads
        {
            get => int.TryParse( this[ MAX_WORKER_THREADS ] + "", out var result ) ? result : 0;
            set => this[ MAX_WORKER_THREADS ] = value;
        }

        /// <summary>最大 IOCP Thread 數量
        /// </summary>
        [ConfigurationProperty( MAX_IOCP_THREADS, IsRequired = false )]
        public int MaxIOCPThreads
        {
            get => int.TryParse( this[ MAX_IOCP_THREADS ] + "", out var result ) ? result : 0;
            set => this[ MAX_IOCP_THREADS ] = value;
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法
    }
}
