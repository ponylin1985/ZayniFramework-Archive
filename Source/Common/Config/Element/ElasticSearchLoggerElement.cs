using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>ElasticSearchLogger 客戶端實體 Config 設定元素
    /// </summary>
    public sealed class ElasticSearchLoggerElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>設定名稱
        /// </summary>
        private const string LOGGER_NAME = "name";

        /// <summary>是否為預設的 ElasticSearchLogger 客戶端實體
        /// </summary>
        private const string IS_DEFAULT = "isDefault";

        /// <summary>ElasticSearch 服務的主機 Http URL 位址
        /// </summary>
        private const string ES_HOST_URL = "esHostUrl";

        /// <summary>ElasticSearch 服務的預設 Index 資料庫名稱
        /// </summary>
        private const string ES_DEFAULT_INDEX = "defaultIndex";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>Logger 設定名稱
        /// </summary>
        [ConfigurationProperty( LOGGER_NAME, IsRequired = false )]
        public string Name
        {
            get
            {
                return this[ LOGGER_NAME ] + "";
            }
            set
            {
                this[ LOGGER_NAME ] = value;
            }
        }

        /// <summary>是否為預設的 ElasticSearchLogger 客戶端實體
        /// </summary>
        [ConfigurationProperty( IS_DEFAULT, IsRequired = false )]
        public bool IsDefault
        {
            get
            {
                string setting = this[ IS_DEFAULT ] + "";

                if ( !bool.TryParse( setting, out bool result ) )
                {
                    return false;
                }

                return result;
            }
            set
            {
                this[ IS_DEFAULT ] = value + "";
            }
        }

        /// <summary>ElasticSearch 服務的主機 Http URL 位址
        /// </summary>
        [ConfigurationProperty( ES_HOST_URL, IsRequired = false )]
        public string HostUrl
        {
            get
            {
                return this[ ES_HOST_URL ] + "";
            }
            set
            {
                this[ ES_HOST_URL ] = value;
            }
        }

        /// <summary>ElasticSearch 服務的預設 Index 資料庫名稱
        /// </summary>
        [ConfigurationProperty( ES_DEFAULT_INDEX, IsRequired = false )]
        public string DefaultIndex
        {
            get
            {
                return this[ ES_DEFAULT_INDEX ] + "";
            }
            set
            {
                this[ ES_DEFAULT_INDEX ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly() => false;

        #endregion 覆寫父型別的方法
    }
}
