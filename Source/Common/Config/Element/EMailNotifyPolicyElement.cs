﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>ZayniFramework 框架電子郵件通知例外處理政策Config元素
    /// </summary>
    public sealed class EMailNotifyPolicyElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>是否啟用文字日誌例外處理
        /// </summary>
        private const string TEXT_LOG_ENABLE = "textLogEnable";

        /// <summary>文字日誌檔案路徑
        /// </summary>
        private const string TEXT_LOG_PATH = "textLogPath";

        /// <summary>是否需要重新拋出程式異常
        /// </summary>
        private const string NEED_RETHROW = "needRethrow";

        /// <summary>是否啟用事件檢視器記錄處理
        /// </summary>
        private const string EVENT_LOG_ENABLE = "eventLogEnable";

        /// <summary>是否啟用EMail電子郵件通知處理
        /// </summary>
        private const string EMAIL_NOTIFY_ENABLE = "emailNotifyEnable";

        /// <summary>SMTP通訊協定阜號
        /// </summary>
        private const string SMTP_PORT = "smtpPort";

        /// <summary>SMTP Server伺服器主機名稱或IP位址
        /// </summary>
        private const string SMTP_HOST = "smtpHost";

        /// <summary>是否啟用SSL連線
        /// </summary>
        private const string ENABLE_SSL = "enableSsl";

        /// <summary>SMTP Server連線的網域
        /// </summary>
        private const string SMTP_DOMAIN = "smtpDomain";

        /// <summary>SMTP帳號
        /// </summary>
        private const string SMTP_ACCOUNT = "smtpAccount";

        /// <summary>SMTP密碼
        /// </summary>
        private const string SMTP_PASSWORD = "smtpPassword";

        /// <summary>寄件人電子郵件信箱地址
        /// </summary>
        private const string FROM_ADDRESS = "fromEmailAddress";

        /// <summary>寄件人顯示名稱
        /// </summary>
        private const string FROM_DISPLAY_NAME = "fromDisplayName";

        /// <summary>收件人電子郵件信箱地址
        /// </summary>
        private const string TO_ADDRESS = "toEmailAddress";

        /// <summary>電子郵件信件主旨
        /// </summary>
        private const string MAIL_SUBJECT = "mailSubject";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>是否需要重新拋出程式異常
        /// </summary>
        [ConfigurationProperty( NEED_RETHROW, IsRequired = false )]
        public bool NeedRethrow
        {
            get
            {
                bool result;

                string setting = this[ NEED_RETHROW ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "needRethrow設定值必須為布林值" );
                }

                return result;
            }
            set
            {
                this[ NEED_RETHROW ] = value + "";
            }
        }

        /// <summary>是否啟用文字日誌例外處理
        /// </summary>
        [ConfigurationProperty( TEXT_LOG_ENABLE, IsRequired = false )]
        public bool IsTextLogEnable
        {
            get
            {
                bool result;

                string setting = this[ TEXT_LOG_ENABLE ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    //throw new ConfigurationErrorsException( "enableDefaultPolicy設定值必須為布林值" );
                    return true;
                }

                return result;
            }
            set
            {
                this[ TEXT_LOG_ENABLE ] = value + "";
            }
        }

        /// <summary>文字日誌檔案路徑
        /// </summary>
        [ConfigurationProperty( TEXT_LOG_PATH, IsRequired = false )]
        public string TextLogPath
        {
            get
            {
                return this[ TEXT_LOG_PATH ] + "";
            }
            set
            {
                this[ TEXT_LOG_PATH ] = value;
            }
        }

        /// <summary>是否啟用事件檢視器記錄處理
        /// </summary>
        [ConfigurationProperty( EVENT_LOG_ENABLE, IsRequired = false )]
        public bool IsEventLogEnable
        {
            get
            {
                bool result;

                string setting = this[ EVENT_LOG_ENABLE ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    //throw new ConfigurationErrorsException( "enableDefaultPolicy設定值必須為布林值" );
                    return false;
                }

                return result;
            }
            set
            {
                this[ EVENT_LOG_ENABLE ] = value + "";
            }
        }

        /// <summary>是否啟用EMail電子郵件通知處理
        /// </summary>
        [ConfigurationProperty( EMAIL_NOTIFY_ENABLE, IsRequired = false )]
        public bool IsEMailNotifyEnable
        {
            get
            {
                bool result;

                string setting = this[ EMAIL_NOTIFY_ENABLE ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    //throw new ConfigurationErrorsException( "enableDefaultPolicy設定值必須為布林值" );
                    return true;
                }

                return result;
            }
            set
            {
                this[ EMAIL_NOTIFY_ENABLE ] = value + "";
            }
        }

        /// <summary>SMTP通訊協定阜號
        /// </summary>
        [ConfigurationProperty( SMTP_PORT, IsRequired = false )]
        public int SmtpPort
        {
            get
            {
                int result;

                string setting = this[ SMTP_PORT ] + "";

                if ( !int.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "smtpPort設定值必須為正整數" );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "smtpPort設定值必須為正整數" );
                }

                return result;
            }
            set
            {
                this[ SMTP_PORT ] = value;
            }
        }

        /// <summary>SMTP Server伺服器主機名稱或IP位址
        /// </summary>
        [ConfigurationProperty( SMTP_HOST, IsRequired = false )]
        public string SmtpHost
        {
            get
            {
                return this[ SMTP_HOST ] + "";
            }
            set
            {
                this[ SMTP_HOST ] = value;
            }
        }

        /// <summary>是否啟用SSL連線
        /// </summary>
        [ConfigurationProperty( ENABLE_SSL, IsRequired = false )]
        public bool IsEnableSsl
        {
            get
            {
                bool result;

                string setting = this[ ENABLE_SSL ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    //throw new ConfigurationErrorsException( "enableDefaultPolicy設定值必須為布林值" );
                    return false;
                }

                return result;
            }
            set
            {
                this[ ENABLE_SSL ] = value + "";
            }
        }

        /// <summary>SMTP Server連線的網域
        /// </summary>
        [ConfigurationProperty( SMTP_DOMAIN, IsRequired = false )]
        public string SmtpDomain
        {
            get
            {
                return this[ SMTP_DOMAIN ] + "";
            }
            set
            {
                this[ SMTP_DOMAIN ] = value;
            }
        }

        /// <summary>SMTP帳號
        /// </summary>
        [ConfigurationProperty( SMTP_ACCOUNT, IsRequired = false )]
        public string SmtpAccount
        {
            get
            {
                return this[ SMTP_ACCOUNT ] + "";
            }
            set
            {
                this[ SMTP_ACCOUNT ] = value;
            }
        }

        /// <summary>SMTP密碼
        /// </summary>
        [ConfigurationProperty( SMTP_PASSWORD, IsRequired = false )]
        public string SmtpPassword
        {
            get
            {
                return this[ SMTP_PASSWORD ] + "";
            }
            set
            {
                this[ SMTP_PASSWORD ] = value;
            }
        }

        /// <summary>寄件人電子郵件信箱地址
        /// </summary>
        [ConfigurationProperty( FROM_ADDRESS, IsRequired = false )]
        public string FromEmailAddress
        {
            get
            {
                return this[ FROM_ADDRESS ] + "";
            }
            set
            {
                this[ FROM_ADDRESS ] = value;
            }
        }

        /// <summary>寄件人顯示名稱
        /// </summary>
        [ConfigurationProperty( FROM_DISPLAY_NAME, IsRequired = false )]
        public string FromDisplayName
        {
            get
            {
                return this[ FROM_DISPLAY_NAME ] + "";
            }
            set
            {
                this[ FROM_DISPLAY_NAME ] = value;
            }
        }

        /// <summary>收件人電子郵件信箱地址
        /// </summary>
        [ConfigurationProperty( TO_ADDRESS, IsRequired = false )]
        public string ToEmailAddress
        {
            get
            {
                return this[ TO_ADDRESS ] + "";
            }
            set
            {
                this[ TO_ADDRESS ] = value;
            }
        }

        /// <summary>電子郵件主旨
        /// </summary>
        [ConfigurationProperty( MAIL_SUBJECT, IsRequired = false )]
        public string MailSubject
        {
            get
            {
                return this[ MAIL_SUBJECT ] + "";
            }
            set
            {
                this[ MAIL_SUBJECT ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
