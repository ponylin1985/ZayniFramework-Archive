﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>客製化例外處理政策Config元素
    /// </summary>
    public sealed class CustomPolicyElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>政策名稱
        /// </summary>
        private const string NAME = "name";

        /// <summary>是否啟用此政策
        /// </summary>
        private const string IS_ENABLE = "isEnable";

        /// <summary>程式例外型別完整名稱
        /// </summary>
        private const string EXCEPTION_TYPE_NAME = "exceptionTypeFullName";

        /// <summary>程式例外組件完整路徑
        /// </summary>
        private const string EXCEPTION_ASSEMBLY_PATH = "exceptionAssemblyPath";

        /// <summary>文字日誌記錄處理器名稱
        /// </summary>
        private const string TEXT_LOGGING_HANDLER_NAME = "textLoggerName";

        /// <summary>是否需要重新拋出程式異常
        /// </summary>
        private const string NEED_RETHROW = "needRethrow";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>政策名稱
        /// </summary>
        [ConfigurationProperty( NAME, IsRequired = false )]
        public string PolicyName
        {
            get
            {
                return this[ NAME ] + "";
            }
            set
            {
                this[ NAME ] = value;
            }
        }

        /// <summary>是否啟用此政策
        /// </summary>
        [ConfigurationProperty( IS_ENABLE, IsRequired = false )]
        public bool IsEnable
        {
            get
            {
                bool result;

                string setting = this[ IS_ENABLE ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    return true;
                }

                return result;
            }
            set
            {
                this[ IS_ENABLE ] = value + "";
            }
        }

        /// <summary>程式例外型別完整名稱
        /// </summary>
        [ConfigurationProperty( EXCEPTION_TYPE_NAME, IsRequired = false )]
        public string ExceptionTypeFullName
        {
            get
            {
                return this[ EXCEPTION_TYPE_NAME ] + "";
            }
            set
            {
                this[ EXCEPTION_TYPE_NAME ] = value;
            }
        }

        /// <summary>程式例外組件完整路徑
        /// </summary>
        [ConfigurationProperty( EXCEPTION_ASSEMBLY_PATH, IsRequired = false )]
        public string ExceptionAssemblyPath
        {
            get
            {
                return this[ EXCEPTION_ASSEMBLY_PATH ] + "";
            }
            set
            {
                this[ EXCEPTION_ASSEMBLY_PATH ] = value;
            }
        }

        /// <summary>文字日誌記錄處理器名稱
        /// </summary>
        [ConfigurationProperty( TEXT_LOGGING_HANDLER_NAME, IsRequired = false )]
        public string TextLoggerHandlerName
        {
            get
            {
                return this[ TEXT_LOGGING_HANDLER_NAME ] + "";
            }
            set
            {
                this[ TEXT_LOGGING_HANDLER_NAME ] = value;
            }
        }

        /// <summary>是否需要重新拋出程式異常
        /// </summary>
        [ConfigurationProperty( NEED_RETHROW, IsRequired = false )]
        public bool NeedRethrow
        {
            get
            {
                bool result;

                string setting = this[ NEED_RETHROW ] + "";

                if ( !bool.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "needRethrow設定值必須為布林值" );
                }

                return result;
            }
            set
            {
                this[ NEED_RETHROW ] = value + "";
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
