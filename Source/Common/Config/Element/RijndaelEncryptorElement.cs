﻿using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>Rijndael加解密元件Config設定元素
    /// </summary>
    public sealed class RijndaelEncryptorElement : ConfigurationElement
    {
        #region 宣告私有的字串常數

        /// <summary>Block長度
        /// </summary>
        private const string BLOCK_SIZE = "blockSize";

        /// <summary>金鑰長度
        /// </summary>
        private const string KEY_SIZE = "keySize";

        #endregion 宣告私有的字串常數


        #region 宣告公開的屬性

        /// <summary>金鑰長度
        /// </summary>
        [ConfigurationProperty( BLOCK_SIZE, IsRequired = false )]
        public int BlockSize
        {
            get
            {
                int result;

                string setting = this[ BLOCK_SIZE ] + "";

                if ( !int.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "blockSize設定值必須為正整數" );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "blockSize設定值必須為正整數" );
                }

                return result;
            }
            set
            {
                this[ BLOCK_SIZE ] = value;
            }
        }

        /// <summary>金鑰長度
        /// </summary>
        [ConfigurationProperty( KEY_SIZE, IsRequired = false )]
        public int KeySize
        {
            get
            {
                int result;

                string setting = this[ KEY_SIZE ] + "";

                if ( !int.TryParse( setting, out result ) )
                {
                    throw new ConfigurationErrorsException( "keySize設定值必須為正整數" );
                }

                if ( result < 0 )
                {
                    throw new ConfigurationErrorsException( "keySize設定值必須為正整數" );
                }

                return result;
            }
            set
            {
                this[ KEY_SIZE ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
