﻿using System;
using System.Configuration;


namespace ZayniFramework.Common
{
    /// <summary>日期時間格式化設定Config元素
    /// </summary>
    public class DateTimeFormatElement : ConfigurationElement
    {
        #region 宣告私有的常數

        /// <summary>語系代碼
        /// </summary>
        private const string LANGUAGE = "language";

        /// <summary>格式化字串
        /// </summary>
        private const string FORMAT = "format";

        /// <summary>是否為預設語系設定
        /// </summary>
        private const string IS_DEFAULT = "isDefault";

        #endregion 宣告私有的常數


        #region 宣告公開的屬性

        /// <summary>語系代碼
        /// </summary>
        [ConfigurationProperty( LANGUAGE, IsRequired = false )]
        public string Language
        {
            get
            {
                return this[ LANGUAGE ] + "";
            }
            set
            {
                this[ LANGUAGE ] = value;
            }
        }

        /// <summary>日期時間格式化字串
        /// </summary>
        [ConfigurationProperty( FORMAT, IsRequired = false )]
        public string FormatString
        {
            get
            {
                return this[ FORMAT ] + "";
            }
            set
            {
                this[ FORMAT ] = value;
            }
        }

        /// <summary>是否為預設語系設定
        /// </summary>
        [ConfigurationProperty( IS_DEFAULT, IsRequired = false )]
        public bool IsDefault
        {
            get
            {
                bool result;

                string settings = this[ IS_DEFAULT ] + "";

                if ( !bool.TryParse( settings, out result ) )
                {
                    throw new ConfigurationErrorsException( "isDefault設定值必須為布林值" );
                }

                return result;
            }
            set
            {
                this[ IS_DEFAULT ] = value;
            }
        }

        #endregion 宣告公開的屬性


        #region 覆寫父型別的方法

        /// <summary>是否為唯讀
        /// </summary>
        /// <returns></returns>
        public override bool IsReadOnly()
        {
            return false;
        }

        #endregion 覆寫父型別的方法
    }
}
