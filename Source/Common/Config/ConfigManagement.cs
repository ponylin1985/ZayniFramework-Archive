﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;


namespace ZayniFramework.Common
{
    /// <summary>Zayni Framework 的 Config (.config) 組態管理員
    /// </summary>
    public static class ConfigManagement
    {
        #region 宣告私有的欄位

        /// <summary>連線字串 Config 參數設定集合
        /// </summary>
        private static ConnectionStringSettingsCollection _connectionStrings;

        /// <summary>應用程式設定 AppSettings 集合
        /// </summary>
        private static Dictionary<string, string> _appSettings;

        /// <summary>ZayniFramework 的 Config 設定區段
        /// </summary>
        private static ZayniConfigSection _zayniConfig;

        #endregion 宣告私有的欄位


        #region 宣告公開的屬性

        /// <summary>應用程式 Appconfig 或 Webconfig 的完整絕對路徑。<para/>
        /// * 當 app.config 設定檔與 Entry Assembly 位於相同的 path 路徑下，此 ConfigFullPath 屬性不需要進行設定。<para/>
        /// * 當 app.config 設定檔與 Entry Assembly 位於不同的 path 路徑下的情況，此 ConfigFullPath 一定必須在任何 ZayniFramework 型別被 CLR runtime 載入前就設定正確。<para/>
        /// * 在第二種情況下，建議在程式 runtime 一起動得時候，就直接設定此 ConfigFullPath 屬性。
        /// </summary>
        /// <value></value>
        public static string ConfigFullPath { get; set; }
        
        /// <summary>Zayni Framework 的 Config 設定區段
        /// </summary>
        public static ZayniConfigSection ZayniConfigs
        {
            get 
            {
                if ( _zayniConfig.IsNotNull() )
                {
                    return _zayniConfig;
                }

                // 當 app.config 設定檔與 Entry Assembly 位於相同的 path 路徑下的情況。
                if ( ConfigFullPath.IsNullOrEmpty() )
                {
                    _zayniConfig = ConfigurationManager.GetSection( "ZayniFramework" ) as ZayniConfigSection;
                    return _zayniConfig;
                }

                // 當 app.config 設定檔與 Entry Assembly 位於不同的 path 路徑下的情況。
                var fileConfigSource = new FileConfigSource( ConfigFullPath );
                _zayniConfig = fileConfigSource.GetZayniFrameworkConfigSection();
                return _zayniConfig;
            }
        }

        /// <summary>連線字串 Config 參數設定集合
        /// </summary>
        public static ConnectionStringSettingsCollection ConnectionStrings 
        {
            get 
            {
                if ( _connectionStrings.IsNotNull() )
                {
                    return _connectionStrings;
                }

                if ( ConfigFullPath.IsNullOrEmpty() )
                {
                    _connectionStrings = ConfigurationManager.ConnectionStrings;
                    return _connectionStrings;
                }

                var fileConfigSource = new FileConfigSource( ConfigFullPath );
                _connectionStrings = fileConfigSource.GetConnectionStringsSection();
                return _connectionStrings;
            }
        }

        /// <summary>應用程式 AppSettings 設定組態的 string 字典集合
        /// </summary>
        /// <value></value>
        public static Dictionary<string, string> AppSettings 
        {
            get 
            {
                if ( _appSettings.IsNotNull() )
                {
                    return _appSettings;
                }

                var configPath = ConfigFullPath.IsNullOrEmptyString( $"{Assembly.GetEntryAssembly().Location}.config" );

                var fileConfigSource      = new FileConfigSource( configPath );
                var appSettingsCollection = fileConfigSource.GetAppSettingsSection();

                var appSettings = new Dictionary<string, string>();

                foreach ( var key in appSettingsCollection.AllKeys )
                {
                    var appSetting = appSettingsCollection[ key ];
                    appSettings.Add( appSetting.Key, appSetting.Value );
                }

                _appSettings = appSettings;
                return _appSettings;
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>讀取資料庫連線字串
        /// </summary>
        /// <param name="dbName">資料庫連線字串名稱</param>
        /// <returns>讀取結果</returns>
        public static Result<string> GetDbConnectionString( string dbName )
        {
            var result   = Result.Create<string>();
            var logTitle = $"{nameof ( ConfigManagement )}.{nameof ( GetDbConnectionString )}";

            var connectionString = "";

            var g = ConnectionStringPool.Get( dbName );

            if ( g.Success )
            {
                connectionString = g.Data;
                result.Data      = connectionString;
                result.Success   = true;
                return result;
            }

            try
            {
                var chiperConnString = ConfigManagement.ConnectionStrings[ dbName ].ConnectionString;

                if ( chiperConnString.IsNullOrEmpty() )
                {
                    result.Message = $"Retrive null or empty db connection string from config file. DbName: {dbName}.";
                    return result;
                }

                ConnectionElement config = null;

                foreach ( ConnectionElement cfg in _zayniConfig.DataAccessSettings.DatabaseProviders )
                {
                    if ( cfg.Name == dbName )
                    {
                        config = cfg;
                    }
                }

                if ( config.IsNotNull() && config.EncryptKey.IsNotNullOrEmpty() )
                {
                    connectionString = AesDecryptBase64( chiperConnString, config.EncryptKey );
                }
                else
                {
                    connectionString = chiperConnString;
                }

                if ( connectionString.IsNullOrEmpty() )
                {
                    result.Message = $"Retrive null or empty db connection string. DbName: {dbName}.";
                    return result;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Get db connection string in config file occur exception. dbName: {dbName}. {ex.ToString()}";
                return result;
            }

            result.Data    = connectionString;
            result.Success = true;
            return result;
        }

        #endregion 宣告公開的方法


        /// <summary>AES 對稱式字串解密
        /// </summary>
        /// <param name="source">解密前字串</param>
        /// <param name="cryptoKey">解密金鑰</param>
        /// <returns>解密後字串</returns>
        private static string AesDecryptBase64( string source, string cryptoKey )
        {
            string decrypt = "";

            try
            {
                var aes    = new AesCryptoServiceProvider();
                var md5    = new MD5CryptoServiceProvider();
                var sha256 = new SHA256CryptoServiceProvider();

                byte[] key = sha256.ComputeHash( Encoding.UTF8.GetBytes( cryptoKey ) );
                byte[] iv  = md5.ComputeHash( Encoding.UTF8.GetBytes( cryptoKey ) );

                aes.Key = key;
                aes.IV  = iv;

                byte[] dataByteArray = Convert.FromBase64String( source );

                using ( var ms = new MemoryStream() )
                using ( var cs = new CryptoStream( ms, aes.CreateDecryptor(), CryptoStreamMode.Write ) )
                {
                    cs.Write( dataByteArray, 0, dataByteArray.Length );
                    cs.FlushFinalBlock();
                    decrypt = Encoding.UTF8.GetString( ms.ToArray() );
                }
            }
            catch ( Exception ex )
            {
                throw ex;
            }

            return decrypt;
        }
    }
}
