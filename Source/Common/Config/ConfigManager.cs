﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ZayniFramework.Common
{
    /// <summary>Zayni Framework 的 Config 組態管理員
    /// </summary>
    public static class ConfigManager
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        #endregion 宣告私有的欄位


        #region 宣告公開的方法

        /// <summary>取得指定路徑的 Conifg 組態檔案。<para/>
        /// - 這邊的 configPath 參數，目前不可接受 unix-like 或 linux 作業系統，以 ~ 字元當作家目錄的路徑。
        /// - 必須要傳入 windows 或 unix-like 作業系統的完整絕對路徑。
        /// </summary>
        /// <typeparam name="TConfig">Config 組態的資料泛型</typeparam>
        /// <param name="configPath">Config 組態的完整路徑</param>
        /// <returns>Config 組態設定</returns>
        public static TConfig GetConfig<TConfig>( string configPath )
            where TConfig : class
        {
            try
            {
                lock ( _asyncLock )
                {
                    string jsonConfig = File.ReadAllText( configPath );
                    return JsonConvert.DeserializeObject<TConfig>( jsonConfig );
                }    
            }
            catch ( Exception ex )
            {
                Command.StdoutErr( $"Load json config file occur exception. configPath: {configPath} {ex.ToString()}" );
                throw ex;
            }
        }

        /// <summary>取得指定路徑的 Conifg 組態檔案。<para/>
        /// - 這邊的 configPath 參數，目前不可接受 unix-like 或 linux 作業系統，以 ~ 字元當作家目錄的路徑。
        /// - 必須要傳入 windows 或 unix-like 作業系統的完整絕對路徑。
        /// </summary>
        /// <typeparam name="TConfig">Config 組態的資料泛型</typeparam>
        /// <param name="configPath">Config 組態的完整路徑</param>
        /// <returns>Config 組態設定</returns>
        public static async Task<TConfig> GetConfigAsync<TConfig>( string configPath )
            where TConfig : class
        {
            try
            {
                using ( await _asyncLock.LockAsync() )
                {
                    string jsonConfig = await Task.Factory.StartNew( () => File.ReadAllText( configPath ) );
                    return JsonConvert.DeserializeObject<TConfig>( jsonConfig );
                }    
            }
            catch ( Exception ex )
            {
                Command.StdoutErr( $"Load json config file occur exception. configPath: {configPath} {ex.ToString()}" );
                throw ex;
            }
        }

        /// <summary>取得指定路徑的 Conifg 組態檔案。<para/>
        /// - 這邊的 configPath 參數，目前不可接受 unix-like 或 linux 作業系統，以 ~ 字元當作家目錄的路徑。
        /// - 必須要傳入 windows 或 unix-like 作業系統的完整絕對路徑。
        /// </summary>
        /// <param name="configPath">Config 組態的完整路徑</param>
        /// <returns>Config 組態設定</returns>
        public static dynamic GetConfig( string configPath )
        {
            try
            {
                lock ( _asyncLock )
                {
                    // Pony Says: 這邊的 File.ReadAllText() 方法接受的路徑參數，不可以接受 linux like 作業系統，以 ~ 字元當作家目錄。
                    // 必須要傳入完整的絕對路徑才可以正常取得設定檔。
                    string jsonConfig = File.ReadAllText( configPath );
                    return JsonConvert.DeserializeObject( jsonConfig );
                }    
            }
            catch ( Exception ex )
            {
                Command.StdoutErr( $"Load json config file occur exception. configPath: {configPath} {ex.ToString()}" );
                throw ex;
            }
        }

        /// <summary>取得指定路徑的 Conifg 組態檔案。<para/>
        /// - 這邊的 configPath 參數，目前不可接受 unix-like 或 linux 作業系統，以 ~ 字元當作家目錄的路徑。
        /// - 必須要傳入 windows 或 unix-like 作業系統的完整絕對路徑。
        /// </summary>
        /// <param name="configPath">Config 組態的完整路徑</param>
        /// <returns>Config 組態設定</returns>
        public static async Task<dynamic> GetConfigAsync( string configPath )
        {
            try
            {
                using ( await _asyncLock.LockAsync() )
                {
                    // Pony Says: 這邊的 File.ReadAllText() 方法接受的路徑參數，不可以接受 linux like 作業系統，以 ~ 字元當作家目錄。
                    // 必須要傳入完整的絕對路徑才可以正常取得設定檔。
                    string jsonConfig = await Task.Factory.StartNew( () => File.ReadAllText( configPath ) );
                    return JsonConvert.DeserializeObject( jsonConfig );
                }    
            }
            catch ( Exception ex )
            {
                Command.StdoutErr( $"Load json config file occur exception. configPath: {configPath} {ex.ToString()}" );
                throw ex;
            }
        }

        #endregion 宣告公開的方法
    }
}
