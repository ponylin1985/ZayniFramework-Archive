﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>序列化結果
    /// </summary>
    [Serializable()]
    public class SerializeResult : BaseResult
    {
        /// <summary>序列化成功的字串
        /// </summary>
        public string SerializedString { get; set; }
    }
}
