﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>執行結果
    /// </summary>
    [Serializable()]
    public class Result : BaseResult
    {
        #region 宣告建構子

        /// <summary>預設無參數建構子
        /// </summary>
        public Result()
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="success">執行結果是否成功</param>
        /// <param name="code">執行結果回傳代碼</param>
        /// <param name="message">執行結果訊息</param>
        public Result( bool success, string code = null, string message = null )
        {
            Success = success;
            Code    = code;
            Message = message;
        }

        /// <summary>解構子
        /// </summary>
        ~Result() 
        {
            Code    = null;
            Message = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>建立 Result 執行結果物件
        /// </summary>
        /// <returns>Result 執行結果物件</returns>
        public static Result Create() => new Result();

        /// <summary>建立 Result 執行結果物件
        /// </summary>
        /// <param name="success">執行結果是否成功</param>
        /// <param name="code">執行結果回傳代碼</param>
        /// <param name="message">執行結果訊息</param>
        /// <returns>Result 執行結果物件</returns>
        public static Result Create( bool success, string code = null, string message = null ) =>
            new Result( success, code, message );

        /// <summary>建立 Result 執行結果物件
        /// </summary>
        /// <typeparam name="T">執行結果資料泛型</typeparam>
        /// <returns>Result 執行結果物件</returns>
        public static Result<T> Create<T>() => new Result<T>();

        /// <summary>建立動態的 Result 執行結果物件
        /// </summary>
        /// <returns>動態的 Result 執行結果物件</returns>
        public static dynamic CreateDynamicResult() => Create();

        /// <summary>建立動態的 Result 執行結果物件
        /// </summary>
        /// <param name="success">執行結果是否成功</param>
        /// <param name="code">執行結果回傳代碼</param>
        /// <param name="message">執行結果訊息</param>
        /// <returns>動態的 Result 執行結果物件</returns>
        public static dynamic CreateDynamicResult( bool success, string code = null, string message = null ) => 
            Create( success, code, message );

        /// <summary>建立 Result 執行結果物件
        /// </summary>
        /// <param name="success">執行結果是否成功</param>
        /// <param name="data">執行結果的資料集合</param>
        /// <param name="code">執行結果回傳代碼</param>
        /// <param name="message">執行結果訊息</param>
        /// <typeparam name="T">執行結果資料泛型</typeparam>
        /// <returns>Result 執行結果物件</returns>
        public static Result<T> Create<T>( bool success, T data = default ( T ), string code = null, string message = null ) =>
            new Result<T>( success, data, code, message );

        /// <summary>建立動態的 Result 執行結果物件
        /// </summary>
        /// <typeparam name="T">執行結果資料泛型</typeparam>
        /// <returns>動態的 Result 執行結果物件</returns>        
        public static dynamic CreateDynamicResult<T>() => Create<T>();

        /// <summary>建立動態的 Result 執行結果物件
        /// </summary>
        /// <param name="success">執行結果是否成功</param>
        /// <param name="data">執行結果的資料集合</param>
        /// <param name="code">執行結果回傳代碼</param>
        /// <param name="message">執行結果訊息</param>
        /// <typeparam name="T">執行結果資料泛型</typeparam>
        /// <returns>動態的 Result 執行結果物件</returns>
        public static dynamic CreateDynamicResult<T>( bool success, T data = default ( T ), string code = null, string message = null ) =>
            Create<T>( success, data, code, message );

        #endregion 宣告公開的方法
    }

    /// <summary>執行結果
    /// </summary>
    /// <typeparam name="TData">執行結果資料泛型</typeparam>
    [Serializable()]
    public class Result<TData> : BaseResult<TData>
    {
        #region 宣告建構子

        /// <summary>預設無參數建構子
        /// </summary>
        public Result()
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="success">執行結果是否成功</param>
        /// <param name="data">執行結果的資料集合</param>
        /// <param name="code">執行結果回傳代碼</param>
        /// <param name="message">執行結果訊息</param>
        public Result( bool success, TData data = default ( TData ), string code = null, string message = null ) 
        {
            Success = success;
            Data    = data;
            Code    = code;
            Message = message;
        }

        /// <summary>解構子
        /// </summary>
        ~Result() 
        {
            Code    = null;
            Message = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>建立 Result 執行結果物件
        /// </summary>
        /// <typeparam name="T">執行結果資料泛型</typeparam>
        /// <returns>Result 執行結果物件</returns>
        public static Result<T> Create<T>() => new Result<T>();

        /// <summary>建立 Result 執行結果物件
        /// </summary>
        /// <param name="success">執行結果是否成功</param>
        /// <param name="data">執行結果的資料集合</param>
        /// <param name="code">執行結果回傳代碼</param>
        /// <param name="message">執行結果訊息</param>
        /// <typeparam name="T">執行結果資料泛型</typeparam>
        /// <returns>Result 執行結果物件</returns>
        public static Result<T> Create<T>( bool success, T data = default ( T ), string code = null, string message = null ) =>
            new Result<T>( success, data, code, message );

        /// <summary>建立動態的 Result 執行結果物件
        /// </summary>
        /// <typeparam name="T">執行結果資料泛型</typeparam>
        /// <returns>動態的 Result 執行結果物件</returns>        
        public static dynamic CreateDynamicResult<T>() => Create<T>();

        /// <summary>建立動態的 Result 執行結果物件
        /// </summary>
        /// <param name="success">執行結果是否成功</param>
        /// <param name="data">執行結果的資料集合</param>
        /// <param name="code">執行結果回傳代碼</param>
        /// <param name="message">執行結果訊息</param>
        /// <typeparam name="T">執行結果資料泛型</typeparam>
        /// <returns>動態的 Result 執行結果物件</returns>
        public static dynamic CreateDynamicResult<T>( bool success, T data = default ( T ), string code = null, string message = null ) =>
            Create<T>( success, data, code, message );

        /// <summary>轉換成無泛型 Result 型別的物件
        /// </summary>
        /// <returns>無泛型 Result 型別的物件</returns>
        public Result GetResult() => new Result() { Success = Success, Code = Code, Message = Message, ExceptionObject = ExceptionObject, HasException = HasException };

        #endregion 宣告公開的方法
    }
}
