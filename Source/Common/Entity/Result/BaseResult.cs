﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;
using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Common
{
    /// <summary>執行結果基底
    /// </summary>
    [Serializable()]
    public abstract class BaseResult : BaseDynamicObject, IResult
    {
        /// <summary>執行結果是否成功
        /// </summary>
        [JsonProperty( PropertyName = "success" )]
        [JsonPropertyName( "success" )]
        public virtual bool Success { get; set; }

        /// <summary>執行結果回傳代碼
        /// </summary>
        [JsonProperty( PropertyName = "code" )]
        [JsonPropertyName( "code" )]
        public virtual string Code { get; set; }

        /// <summary>執行結果訊息
        /// </summary>
        [JsonProperty( PropertyName = "message" )]
        [JsonPropertyName( "message" )]
        public string Message { get; set; }

        /// <summary>執行過程中是否發生程式異常
        /// </summary>
        [Newtonsoft.Json.JsonIgnore()]
        [System.Text.Json.Serialization.JsonIgnore()]
        public bool HasException { get; set; }

        /// <summary>程式異常物件
        /// </summary>
        [Newtonsoft.Json.JsonIgnore()]
        [System.Text.Json.Serialization.JsonIgnore()]
        public Exception ExceptionObject { get; set; }
    }

    /// <summary>執行結果基底
    /// </summary>
    /// <typeparam name="TData">執行結果資料泛型</typeparam>
    [Serializable()]
    public abstract class BaseResult<TData> : BaseResult, IResult<TData>
    {
        /// <summary>執行結果的資料集合
        /// </summary>
        [JsonProperty( PropertyName = "data" )]
        [JsonPropertyName( "data" )]
        public TData Data { get; set; }
    }
}
