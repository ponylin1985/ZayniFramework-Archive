﻿using System;
using System.Collections.Generic;


namespace ZayniFramework.Common
{
    /// <summary>驗證結果
    /// </summary>
    [Serializable()]
    public sealed class ValidationResult
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public ValidationResult()
        {
            Message            = string.Empty;
            InvalidMessages    = new Dictionary<string, List<string>>();
            ValidationMessages = new List<Dictionary<string,List<string>>>();
        }

        /// <summary>解構子
        /// </summary>
        ~ValidationResult()
        {
            Message            = null;
            InvalidMessages    = null;
            ValidationMessages = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>驗證是否成功
        /// </summary>
        public bool IsValid { get; set; }

        /// <summary>驗證錯誤訊息
        /// </summary>
        public string Message { get; set; }

        /// <summary>驗證失敗的資訊集合 (Zayni Framework框架內部使用，外界請勿使用!)
        /// </summary>
        public Dictionary<string, List<string>> InvalidMessages { get; set; }

        /// <summary>驗證失敗的資訊集合
        /// </summary>
        public List<Dictionary<string, List<string>>> ValidationMessages { get; set; }

        #endregion 宣告公開的屬性
    }
}
