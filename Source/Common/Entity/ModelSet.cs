﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>資料模型集合基底
    /// </summary>
    [Serializable()]
    public class ModelSet : IResult
    {
        /// <summary>執行結果是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>執行結果訊息 (錯誤訊息)
        /// </summary>
        public string Message { get; set; }

        /// <summary>執行結果回傳代碼
        /// </summary>
        public string Code { get; set; }
    }
}
