﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;


namespace ZayniFramework.Common
{
    /// <summary>Array 物件的擴充類別
    /// </summary>
    public static class ArrayExtension
    {
        #region 對 Array 的擴充方法

        /// <summary>陣列是否為 Null 或是一個長度為 0 的空陣列
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否為 Null 或是一個長度為 0 的空陣列</returns>
        public static bool IsNullOrEmptyArray( this Array source )
        {
            if ( null == source || 0 == source.Length )
            {
                return true;
            }

            return false;
        }

        /// <summary>陣列是否不為一個 Null 且不是一個長度為 0 的空陣列
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否不為一個 Null 且不是一個長度為0的空陣列</returns>
        public static bool IsNotNullOrEmptyArray( this Array source )
        {
            if ( null != source && 0 != source.Length )
            {
                return true;
            }

            return false;
        }

        /// <summary>串連字串陣列的所有元素，並在每個元素之間使用指定的分隔符號
        /// </summary>
        /// <param name="source"></param>
        /// <param name="token">字串分隔符號</param>
        /// <returns>串連完成之後的字串</returns>
        public static string JoinToken( this string[] source, string token )
        {
            var sb = new StringBuilder();

            foreach ( var str in source )
            {
                sb.Append( $"{str}{token}" );
            }

            string result = sb.ToString().RemoveLastAppeared( token );
            return result;
        }

        #endregion 對 Array 的擴充方法


        #region 對 byte[] 的擴充方法

        /// <summary>對GZip格式二進位資料(byte陣列資料)進行解壓縮處理成一般字串
        /// </summary>
        /// <param name="self"></param>
        /// <returns>GZip解壓縮過的字串</returns>
        public static string GZipDecompress2String( this byte[] self )
        {
            try
            {
                using ( var msi = new MemoryStream( self ) )
                using ( var mso = new MemoryStream() )
                {
                    using ( var gs = new GZipStream( msi, CompressionMode.Decompress ) )
                    {
                        gs.CopyTo( mso );
                    }

                    string result = Encoding.UTF8.GetString( mso.ToArray() );
                    return result;
                }
            }
            catch ( Exception ex )
            {
                throw ex;
            }
        }

        /// <summary>對原本 UTF8 編碼的字串 Binary 資料，進行轉換成原始字串
        /// </summary>
        /// <param name="self"></param>
        /// <returns>轉換過的成原始字串</returns>
        public static string GetStringFromUtf8Bytes( this byte[] self ) => Encoding.UTF8.GetString( self );

        /// <summary>對目標的 byte[] 陣列進行比較，比較與自己原陣列的長度與元素內容是否一致
        /// </summary>
        /// <param name="self"></param>
        /// <param name="target">目標byte[]陣列</param>
        /// <returns>比較與自己原陣列的長度與元素內容是否一致</returns>
        public static bool IsEquals( this byte[] self, byte[] target )
        {
            // .NET 4.0之後內建的新API，參考如下: http://stackoverflow.com/questions/43289/comparing-two-byte-arrays-in-net
            return StructuralComparisons.StructuralEqualityComparer.Equals( self, target );
        }

        /// <summary>移除掉 byte[] 陣列所有結尾為 \0 的元素。(效能不是非常好!)<para/>
        /// 參考: https://stackoverflow.com/questions/11263297/remove-trailing-zeros-from-byte
        /// </summary>
        /// <param name="array"></param>
        /// <returns>結果 byte[] 陣列</returns>
        public static byte[] TrimZeroEnd( this byte[] array )
        {
            int lastIndex = Array.FindLastIndex( array, b => b != 0 );
            Array.Resize( ref array, lastIndex + 1 );
            return array;
        }

        /// <summary>串接兩個 Byte 陣列
        /// </summary>
        /// <param name="first">第一個 Byte 陣列</param>
        /// <param name="second">第二個 Byte 陣列</param>
        /// <returns>串接後的結果 Byte 陣列</returns>
        public static byte[] Combine( this byte[] first, byte[] second )
        {
            byte[] result = new byte[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, result, 0, first.Length);
            Buffer.BlockCopy(second, 0, result, first.Length, second.Length);
            return result;
        }

        /// <summary>串接兩個 Byte 陣列
        /// </summary>
        /// <param name="a1">第一個 Byte 陣列</param>
        /// <param name="a2">第二個 Byte 陣列</param>
        /// <returns>串接後的結果 Byte 陣列</returns>
        public static IEnumerable<byte> Combine2( this byte[] a1, byte[] a2 )
        {
            foreach ( byte b in a1 )
                yield return b;
            foreach ( byte b in a2 )
                yield return b;
        }

        /// <summary>對來源的 Byte 陣列，進行搜尋目標的 Byte 陣列 (效能最佳實作!) <para/>
        /// 參考: https://stackoverflow.com/questions/4859023/find-an-array-byte-inside-another-array/4859186#4859186
        /// </summary>
        /// <param name="source">來源的 Bytes 陣列</param>
        /// <param name="target">目標的 Byte 陣列</param>
        /// <returns>搜尋到的 Index 索引位置</returns>
        public static int SearchBytes( this byte[] source, byte[] target )
        {
            var len   = target.Length;
            var limit = source.Length - len;

            for ( var i = 0;  i <= limit; i++ )
            {
                var k = 0;

                for ( ;  k < len;  k++ )
                {
                    if ( target[ k ] != source[ i + k ] ) break;
                }

                if ( k == len ) return i;
            }

            return -1;
        }

        /// <summary>檢查來源的 Byte 陣列，是否包含指定的目標 Byte 陣列 (效能最佳實作!)
        /// </summary>
        /// <param name="source">來源的 Bytes 陣列</param>
        /// <param name="target">目標的 Byte 陣列</param>
        /// <returns>是否包含指定的目標 Byte 陣列</returns>
        public static bool ContainBytes( this byte[] source, byte[] target ) => SearchBytes( source, target ) > 0;

        /// <summary>對來源的 Bytes 陣列，移除指定的 Byte (效能普通尚可!)
        /// </summary>
        /// <param name="source">來源的 Bytes 陣列</param>
        /// <param name="pattern">待移除的 Byte 陣列</param>
        /// <returns></returns>
        public static byte[] RemoveBytes( this byte[] source, byte[] pattern )
        {
            if ( null == pattern || 0 == pattern.Length )
            {
                return source;
            }

            var result = new List<byte>();

            int i;

            for ( i = 0; i <= source.Length - pattern.Length; i++ )
            {
                var foundMatch = !pattern.Where( ( t, j ) => source[ i + j ] != t ).Any();
                if ( foundMatch ) i += pattern.Length - 1;
                else result.Add( source[ i ] );
            }

            for ( ; i < source.Length; i++ )
            {
                result.Add( source[ i ] );
            }

            return result.ToArray();
        }

        #endregion 對 byte[] 的擴充方法
    }
}
