﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>Decimal 或 Decimal? 的擴充類別
    /// </summary>
    public static class DecimalExtension
    {
        /// <summary>檢查Decimal數字是否為 .NET CLR 的系統預設值 0.0M，如果是則將自動使用傳入的Decimal數字值取代，如果不是，則不會有取代的效果。
        /// </summary>
        /// <param name="self"></param>
        /// <param name="value">取代預設值 0.0M 的Decimal數字</param>
        /// <returns></returns>
        public static decimal IsDotNetDefault( this decimal self, decimal value ) => 0.0M == self ? value : self;

        /// <summary>檢查是否為一個真正有浮點數的小數值的 Decimal 數值
        /// </summary>
        /// <param name="self"></param>
        /// <returns>是否為一個真正的小數值的 Decimal 數值</returns>
        public static bool IsRealDecimal( this decimal self )
        {
            string target = self + "";
	
	        if ( !target.Contains( "." ) )
	        {
		        return false;
	        }
	
	        string[] tokens = target.Split( '.' );
	
	        if ( 2 != tokens.Length )
	        {
		        return false;
	        }
	
	        string decimalPart = "";
	
	        try
	        {	        
		        decimalPart = tokens[ 1 ];
	        }
	        catch ( Exception )
	        {
		        return false;
	        }
	
	        int i = int.Parse( decimalPart );
	        return 0 != i;
        }

        /// <summary>檢查是否為一個真正有浮點數的小數值的 Decimal 數值
        /// </summary>
        /// <param name="self"></param>
        /// <returns>是否為一個真正的小數值的 Decimal 數值</returns>
        public static bool IsRealDecimal( this decimal? self ) 
        {
            if ( self.IsNull() )
            {
                return false;
            }

            string target = self + "";
	
	        if ( target.IsNullOrEmpty() || !target.Contains( "." ) )
	        {
		        return false;
	        }
	
	        string[] tokens = target.Split( '.' );
	
	        if ( 2 != tokens.Length )
	        {
		        return false;
	        }
	
	        string decimalPart = "";
	
	        try
	        {	        
		        decimalPart = tokens[ 1 ];
	        }
	        catch ( Exception )
	        {
		        return false;
	        }
	
	        int i = int.Parse( decimalPart );
	        return 0 != i;
        }
    }
}
