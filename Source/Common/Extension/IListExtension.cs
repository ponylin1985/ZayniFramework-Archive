﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace ZayniFramework.Common
{
    /// <summary>IList 或 List 集合的擴充類別
    /// </summary>
    public static class IListExtension
    {
        /// <summary>從來源串列集合中隨便取出一個元素。範例:
        /// var source = new List&lt;string&gt;() { "A", "B", "C", "D", "E", "F" };
        /// source.TakeRandom&lt;string&gt;();
        /// </summary>
        /// <typeparam name="TSource">元素的泛型</typeparam>
        /// <example>
        /// <code>
        /// var source = new List&lt;string&gt;() { "A", "B", "C", "D", "E", "F" };
        /// source.TakeRandom&lt;string&gt;();
        /// </code>
        /// </example>
        /// <returns>元素物件</returns>
        public static TSource TakeRandom<TSource>( this IList source )
        {
            if ( source.IsNullOrEmptyList() )
            {
                return default ( TSource );
            }

            return (TSource)source[ new Random().Next( source.Count ) ];
        }

        /// <summary>轉換為物件的串列集合
        /// </summary>
        /// <param name="source">來源List串列集合</param>
        /// <returns>物件的串列集合</returns>
        public static List<object> ToObjectList( this IList source )
        {
            var result = new List<object>();

            foreach ( var item in source )
            {
                result.Add( item );
            }

            return result;
        }

        /// <summary>IList 串列是否為 Null 或是一個長度為 0 的空串列
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否為 Null 或是一個長度為 0 的空串列</returns>
        public static bool IsNullOrEmptyList( this IList source )
        {
            if ( null == source || 0 == source.Count )
            {
                return true;
            }

            return false;
        }

        /// <summary>IList 串列是否不為一個 Null 且不是一個長度為 0 的空串列
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否不為一個 Null 且不是一個長度為 0 的空串列</returns>
        public static bool IsNotNullOrEmptyList( this IList source )
        {
            if ( null != source && 0 != source.Count )
            {
                return true;
            }

            return false;
        }

        /// <summary>對目標泛型串列進行泛型的轉換
        /// </summary>
        /// <typeparam name="TModel">目標泛型型別</typeparam>
        /// <param name="self"></param>
        /// <returns>轉換後的目標串列泛型集合物件</returns>
        public static List<TModel> ConvertList<TModel>( this List<object> self )
        {
            var result = new List<TModel>();

            foreach ( var m in self )
            {
                try
                {
                    var model = (TModel)m;
                    result.Add( model );
                }
                catch ( Exception ex )
                {
                    throw ex;
                }
            }

            return result;
        }

        //  This function is adapated from: http://www.codeguru.com/forum/showthread.php?t=450171
        //  My thanks to Carl Quirion, for making it "nullable-friendly".
        /// <summary>將List資料集合轉換為DataTable
        /// </summary>
        /// <typeparam name="T">資料模型的泛型</typeparam>
        /// <param name="self"></param>
        /// <param name="tableName">資料表名稱</param>
        /// <returns>轉換完成的DataTable物件</returns>
        public static DataTable ConvertToDataTable<T>( this List<T> self, string tableName = "" )
        {
            var dt = new DataTable();

            var type       = typeof ( T );
            var properties = type.GetProperties().ToList();

            properties.ForEach( p => dt.Columns.Add( new DataColumn( p.Name, Reflector.GetNullableType( p.PropertyType ) ) ) );

            self.ForEach( t => 
            {
                DataRow row = dt.NewRow();
                properties.ForEach( p => row[ p.Name ] = !Reflector.IsNullableType( p.PropertyType ) ? p.GetValue( t, null ) : p.GetValue( t, null ) ?? DBNull.Value );
                dt.Rows.Add( row );
            } );

            tableName.IsNotNullOrEmpty( m => dt.TableName = tableName );
            return dt;
        }

        /// <summary>對字串串列中的所有元素進行Trim的動作
        /// </summary>
        /// <param name="self"></param>
        public static void TrimElements( this List<string> self )
        {
            for ( int i = 0, len = self.Count; i < len; i++ )
            {
                var token = self[ i ] + "";
                self[ i ] = token.Trim();
            }
        }

        /// <summary>物件串列中是否有包含任何Null的元素
        /// </summary>
        /// <param name="self"></param>
        /// <returns>是否有包含任何Null的元素</returns>
        public static bool HasNullElements( this List<object> self ) => self.Any( q => q.IsNull() );

        /// <summary>字串串列中是否包含任何Null或空字串的元素 (預設不會先對所有字串元素先進行Trim的動作)
        /// </summary>
        /// <param name="self"></param>
        /// <param name="trim">是否要先對所有字串元素先進行Trim的動作，預設是不進行</param>
        /// <returns>是否包含任何Null或空字串的元素</returns>
        public static bool HasNullOrEmptyElements( this List<string> self, bool trim = false )
        {
            if ( trim )
            {
                self.TrimElements();
            }

            return self.Any( r => r.IsNullOrEmpty() );
        }

        /// <summary>對 List 進行合併，預設會重複的元素進行過濾。<para/>
        /// 假若為 TData 泛型為自定義的 class，而且需要過濾重複的元素，則需要傳入 IEqualityComparer&lt;TData&gt; 物件，才會有效進行過濾。<para/>
        /// var aList  = new List&lt;strin&gt;() { "A", "B", "C", "D" };<para/>
        /// var bList  = new List&lt;strin&gt;() { "A", "B", "E", "F" };<para/>
        /// var result = aList.MergeList&lt;string&gt;( bList );<para/>
        /// result 內容為 "A", "B", "C", "D", "E", "F" 的 List 集合<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// var aList  = new List&lt;strin&gt;() { "A", "B", "C", "D" };
        /// var bList  = new List&lt;strin&gt;() { "A", "B", "E", "F" };
        /// var result = aList.MergeList&lt;string&gt;( bList );    // "A", "B", "C", "D", "E", "F"
        /// </code>
        /// </example>
        /// <typeparam name="TData">List 串列中的元素的泛型</typeparam>
        /// <param name="source">來源 List 集合</param>
        /// <param name="target">目標 List 集合</param>
        /// <param name="distinct">是否過慮重複的元素，預設會重複的元素進行過濾</param>
        /// <param name="equalityComparer">物件比較方式</param>
        /// <returns>合併後的 List 集合</returns>
        public static List<TData> MergeList<TData>( this List<TData> source, List<TData> target, bool distinct = true, IEqualityComparer<TData> equalityComparer = null )
        {
            var result = new List<TData>( source );

            if ( distinct )
            {
                result.AddRange( target.Except( source, equalityComparer ) );
            }
            else
            {
                result.AddRange( source );
            }
            
            return result;
        }
    }
}
