﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>TimeSpan 功能擴充類別
    /// </summary>
    public static class TimeSpanExtension
    {
        /// <summary>檢查 TimeSpan 是否為 .NET CLR 的系統預設值 00:00:00，如果是則將自動使用傳入的 TimeSpan 取代，如果不是，則不會有取代的效果。
        /// </summary>
        /// <param name="self"></param>
        /// <param name="value">取代預設值 00:00:00 的TimeSpan</param>
        /// <returns></returns>
        public static TimeSpan IsDotNetDefault( this TimeSpan self, TimeSpan value ) => new TimeSpan( 0, 0, 0 ) == self ? value : self;
    }
}
