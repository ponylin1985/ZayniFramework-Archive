﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace ZayniFramework.Common
{
    /// <summary>Object 物件的擴充類別
    /// </summary>
    public static class ObjectExtension
    {
        #region Public Object Extension Methods

        /// <summary>取得目標物件的屬性值，C# 的 dynamic 物件無法呼叫調用此擴充方法。
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="propertyName">屬性值名稱</param>
        /// <returns>屬性值</returns>
        public static object GetPropertyValue( this object source, string propertyName )
        {
            if ( !Reflector.TryGetPropertyValue( source, propertyName, out object value ) )
            {
                return null;
            }

            return value;
        }

        /// <summary>取得目標物件的屬性值
        /// </summary>
        /// <typeparam name="TResult">屬性的泛型</typeparam>
        /// <param name="source">目標物件</param>
        /// <param name="propertyName">屬性值名稱</param>
        /// <returns>屬性值</returns>
        public static TResult GetPropertyValue<TResult>( this object source, string propertyName ) =>
            (TResult)GetPropertyValue( source, propertyName );

        /// <summary>對目標物件設定屬性值<para/>
        /// 1. 支援各種 Property 的型別，因為使用 Convert.ChangeType() 進行 runtime 轉型。<para/>
        /// 2. 支援 Nullable 的 Value Type 屬性值設地。<para/>
        /// 3. 以 PropertyInfo.SetValue() 反射的機制實作屬性值的設定。
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <param name="value">屬性值 (資料值)</param>
        public static void SetPropertyValue( this object source, string propertyName, object value ) =>
            Reflector.SetPropertyValue( source, propertyName, value );

        /// <summary>嘗試對目標物件設定屬性值<para/>
        /// 1. 支援各種 Property 的型別，因為使用 Convert.ChangeType() 進行 runtime 轉型。<para/>
        /// 2. 支援 Nullable 的 Value Type 屬性值設地。<para/>
        /// 3. 以 PropertyInfo.SetValue() 反射的機制實作屬性值的設定。
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="propertyName">屬性名稱</param>
        /// <param name="value">屬性值 (資料值)</param>
        /// <param name="message">錯誤訊息</param>
        /// <returns>屬性值設定是否成功</returns>
        public static bool TrySetPropertyValue( this object source, string propertyName, object value, out string message ) => 
            Reflector.TrySetPropertyValue( source, propertyName, value, out message );

        /// <summary>反射取得物件的方法資訊
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <returns>方法資訊</returns>
        public static MethodInfo GetMethod( this object source, string methodName ) => Reflector.GetMethodInfo( source, methodName );

        /// <summary>以反射的方式呼叫物件的方法
        /// </summary>
        /// <param name="source">目標物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <param name="parameters">方法的參數陣列</param>
        /// <returns>呼叫方法後的回傳值</returns>
        public static object MethodInvoke( this object source, string methodName, params object[] parameters ) => 
            Reflector.InvokeMethod( source, methodName, parameters );

        /// <summary>以反射的方式呼叫物件的方法
        /// </summary>
        /// <typeparam name="TResult">回傳值的泛型</typeparam>
        /// <param name="source">目標物件</param>
        /// <param name="methodName">方法名稱</param>
        /// <param name="parameters">方法的參數陣列</param>
        /// <returns>呼叫方法後的回傳值</returns>
        public static TResult MethodInvoke<TResult>( this object source, string methodName, params object[] parameters ) =>
            (TResult)Reflector.InvokeMethod( source, methodName, parameters );

        // http://stackoverflow.com/questions/78536/deep-cloning-objects
        /// <summary>物件深度複製，物件本身必須要有標記為 [Serializable()] 可二進位序列化。<para/>
        /// Perform a deep Copy of the object.
        /// </summary>
        /// <typeparam name="TSource">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>The copied object.</returns>
        public static TSource Clone<TSource>( this object source )
        {
            if ( !typeof ( TSource ).IsSerializable )
            {
                throw new ArgumentException( $"The type must be serializable with [Serializable] attribute." );
            }

            if ( Object.ReferenceEquals( source, null ) )
            {
                return default( TSource );
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();

            using ( stream )
            {
                formatter.Serialize( stream, source );
                stream.Seek( 0, SeekOrigin.Begin );
                return (TSource)formatter.Deserialize( stream );
            }
        }

        /// <summary>物件深度複製，物件本身必須要有標記為 [Serializable()] 可二進位序列化。<para/>
        /// 參考: http://stackoverflow.com/questions/78536/deep-cloning-objects
        /// </summary>
        /// <param name="source">來源物件</param>
        /// <returns>複製出的物件</returns>
        public static object Clone( this object source )
        {
            if ( !source.GetType().IsSerializable )
            {
                throw new ArgumentException( $"The type must be serializable with [Serializable] attribute." );
            }

            if ( Object.ReferenceEquals( source, null ) )
            {
                return null;
            }

            IFormatter formatter = new BinaryFormatter();

            using ( var stream = new MemoryStream() )
            {
                formatter.Serialize( stream, source );
                stream.Seek( 0, SeekOrigin.Begin );
                return formatter.Deserialize( stream );
            }
        }

        /// <summary>物件深度複製。<para/>
        /// 1. 物件本身必須要可以支援被 Json.NET (Newtonsoft.Json.dll) 的 JSON 序列化與反序列化。<para/>
        /// 2. 傳入的來源物件，其中 Property 的實作方式，必須按照 C# Auto-Implemented Property 規範，否則複製出的物件屬性值可能會有不正確的情況。<para/>
        /// https://msdn.microsoft.com/zh-tw/library/bb384054.aspx
        /// </summary>
        /// <remarks>
        /// 1. 物件本身必須要可以支援被 Json.NET (Newtonsoft.Json.dll) 的 JSON 序列化與反序列化。
        /// 2. 傳入的來源物件，其中 Property 的實作方式，必須按照 C# Auto-Implemented Property 規範，否則複製出的物件屬性值可能會有不正確的情況。
        /// https://msdn.microsoft.com/zh-tw/library/bb384054.asp
        /// </remarks>
        /// <typeparam name="TSource">被深度複製的泛型，物件本身必須要可以支援被 Json.NET (Newtonsoft.Json) 序列化。</typeparam>
        /// <param name="source">來源物件</param>
        /// <returns>複製出的物件</returns>
        public static TSource CloneObject<TSource>( this TSource source ) 
            where TSource : class
        {
            if ( null == source )
            {
                return default( TSource );
            }

            return JsonConvert.DeserializeObject<TSource>( JsonConvert.SerializeObject( source ) );
        }

        /// <summary>物件深度複製，採用 System.Text.Json.JsonSerializer 進行序列化機制進行物件屬性深度複製。<para/>
        /// * 物件本身需要可以支援 System.Text.Json.JsonSerializer 進行序列化與反序列化。
        /// * 物件本身的 Property 宣告需要正確使用 System.Text.Json 的 [JsonPropertyName] 進行 JSON 屬性名稱標記，必且只宣告為 public property。
        /// </summary>
        /// <typeparam name="TSource">被深度複製的泛型，物件本身必須要可以支援被 System.Text.Json.JsonSerializer 序列化。</typeparam>
        /// <param name="source">來源物件</param>
        /// <returns>複製出的物件</returns>
        public static TSource CloneObjectJson<TSource>( this TSource source ) 
        {
            if ( null == source )
            {
                return default( TSource );
            }

            return System.Text.Json.JsonSerializer.Deserialize<TSource>( System.Text.Json.JsonSerializer.Serialize( source ) );
        }

        /// <summary>根據指定的泛型型別，將物件轉換成指定的型別
        /// </summary>
        /// <param name="source">來源物件</param>
        /// <typeparam name="TConvert">目標物件的泛型</typeparam>
        /// <returns>指定的型別的物件</returns>
        public static TConvert ConvertTo<TConvert>( this object source ) 
            where TConvert : new()
        {
            var convert     = new TConvert();
            var sourceProps = TypeDescriptor.GetProperties( source ).Cast<PropertyDescriptor>();

            var convertProps = 
                convert
                    .GetType()
                    .GetProperties( BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance );

            foreach ( var sourceProp in sourceProps )
            {
                var convertProp = convertProps.FirstOrDefault( m => m.Name == sourceProp.Name );

                if ( null == convertProp )
                {
                    continue;
                }

                try
                {
                    if ( convertProp.PropertyType == typeof ( DateTime? ) )
                    {
                        var dtValue = sourceProp.GetValue( source )?.ToString();
                        var dt = dtValue.IsNotNull() ? ( DateTime.TryParse( dtValue, out var d ) ? (DateTime?)d : null ) : null;
                        convertProp.SetValue( convert, dt, null );
                        continue;
                    }

                    if ( Reflector.IsNullableType( convertProp.PropertyType ) )
                    {
                        var type = Reflector.GetNullableType( convertProp.PropertyType );
                        var val  = Convert.ChangeType( sourceProp.GetValue( source ), type, CultureInfo.InvariantCulture );
                        convertProp.SetValue( convert, val, null );
                        continue;
                    }

                    object value = Convert.ChangeType( sourceProp.GetValue( source ), convertProp.PropertyType, CultureInfo.InvariantCulture );
                    convertProp.SetValue( convert, value, null );    
                }
                catch
                {
                    continue;
                }
            }

            return convert;
        }

        /// <summary>轉換成 IDictionary Key/Value Pair 的字典資料集合。<para/>
        /// var model = new UserModel<para/>
        /// {<para/>
        ///     Name = "Amber",<para/>
        ///     Age  = 23,<para/>
        ///     Sex  = "female"<para/>
        /// };<para/>
        /// IDictionary dict = model.ConvertToDictionary();<para/>
        /// string name = dict[ "Name" ] + "";<para/>
        /// string sex  = dict[ "Sex" ]  + "";<para/>
        /// int    age  = int.Parse( dict[ "Age" ] + "" );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// var model = new UserModel
        /// {
        ///     Name = "Amber",
        ///     Age  = 23,
        ///     Sex  = "female"
        /// };
        /// IDictionary dict = model.ConvertToDictionary();
        /// string name = dict[ "Name" ] + "";
        /// string sex  = dict[ "Sex" ]  + "";
        /// int    age  = int.Parse( dict[ "Age" ] + "" );
        /// var obj = new
        /// {
        ///     Message = "Hello World",
        ///     DoB     = DateTime.Now,
        ///     Money   = 324.52M,
        ///     Cash    = 76.123D,
        ///     Age     = 33,
        ///     User    = model
        /// };
        /// dict = obj.ConvertToDictionary();
        /// </code>
        /// </example>
        /// <param name="source">來源物件</param>
        /// <returns>IDictionary Key/Value Pair 的字典資料集合</returns>
        public static IDictionary ConvertToDictionary( this object source )
        {
            if ( null == source )
            {
                return null;
            }

            var result = new Dictionary<string, object>();

            foreach ( var property in source.GetType().GetProperties( BindingFlags.Public | BindingFlags.Instance ) )
            {
                string name  = property.Name;
                object value = property.GetValue( source );
                result.Add( name, value );
            }

            return result;
        }

        /// <summary>轉換成 IDictionary 字串 String Key/String Value Pair 的純字串字典資料集合。<para/>
        /// var source = new UserModel<para/>
        /// {<para/>
        ///     Name = "Sylvia",<para/>
        ///     Age  = 26,<para/>
        ///     Sex  = "F"<para/>
        /// };<para/>
        /// Dictionary&lt;string, string&gt; d = source.ConvertToStringDictionary();<para/>
        /// string name2 = d[ "Name" ];<para/>
        /// string sex2  = d[ "Sex" ];<para/>
        /// string age2  = d[ "Age" ];
        /// </summary>
        /// <example>
        /// <code>
        /// var source = new UserModel
        /// {
        ///     Name = "Sylvia",
        ///     Age  = 26,
        ///     Sex  = "F"
        /// };
        /// Dictionary&lt;string, string&gt; d = source.ConvertToStringDictionary();
        /// string name2 = d[ "Name" ];
        /// string sex2  = d[ "Sex" ];
        /// string age2  = d[ "Age" ];
        /// </code>
        /// </example>
        /// <param name="source">來源物件</param>
        /// <returns>IDictionary String Key/String Value Pair 的純字串字典資料集合</returns>
        public static Dictionary<string, string> ConvertToStringDictionary( this object source )
        {
            if ( null == source )
            {
                return null;
            }

            var result = new Dictionary<string, string>();

            foreach ( var property in source.GetType().GetProperties( BindingFlags.Public | BindingFlags.Instance ) )
            {
                string name  = property.Name;
                object value = property.GetValue( source );
                result.Add( name, value + "" );
            }

            return result;
        }

        /// <summary>檢查變數是否指向Null
        /// </summary>
        /// <param name="source"></param>
        /// <returns>變數是否指向Null</returns>
        public static bool IsNull( this object source ) => null == source;

        /// <summary>檢查變數使否指向Null，如果Null值，就回傳預設值，如果不是Null就回傳原本的物件
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value">預設值</param>
        /// <returns>如果Null值，就回傳預設值，如果不是Null就回傳原本的物件</returns>
        public static object IsNull( this object source, object value ) => source ?? value;

        /// <summary>檢查變數使否指向Null，如果指向Null就會執行回呼處理的委派
        /// </summary>
        /// <param name="source"></param>
        /// <param name="handler">回呼處理委派</param>
        /// <returns>變數使否指向Null</returns>
        public static bool IsNull( this object source, Action handler )
        {
            bool result = null == source;

            if ( result )
            {
                try
                {
                    handler();
                }
                catch ( Exception ex )
                {
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>變數是否沒有指向Null
        /// </summary>
        /// <param name="source"></param>
        /// <returns>變數是否沒有指向Null</returns>
        public static bool IsNotNull( this object source ) => null != source;

        /// <summary>變數是否沒有指向Null，如果沒有指向Null就會執行回呼處理的委派
        /// </summary>
        /// <param name="source"></param>
        /// <param name="handler">回呼處理委派</param>
        /// <returns>變數是否沒有指向Null</returns>
        public static bool IsNotNull( this object source, Action<dynamic> handler )
        {
            bool result = null != source;

            if ( result )
            {
                try
                {
                    handler( source );
                }
                catch ( Exception ex )
                {
                    throw ex;
                }
            }

            return result;
        }

        /// <summary>Zayni Framework框架的AOP切面攔截呼叫
        /// </summary>
        /// <param name="source"></param>
        /// <param name="methodName">目標方法名稱</param>
        /// <param name="methodParameters">方法的參數</param>
        /// <returns>執行回傳值</returns>
        public static object AspectInvoke( this object source, string methodName, params object[] methodParameters )
        {
            InterceptResult result = AspectInvoker.Invoke( methodName, source, methodParameters: methodParameters );
            return result.ReturnValue;
        }

        /// <summary>檢查是否為指定父型別的子型態物件
        /// </summary>
        /// <param name="source"></param>
        /// <param name="assemblyPath">來源組件路徑</param>
        /// <param name="typeFullName">目標型別完整名稱</param>
        /// <returns>是否為指定父型別的子型態物件</returns>
        public static bool IsTypeof( this object source, string assemblyPath, string typeFullName ) => Reflector.IsTypeof( source, assemblyPath, typeFullName );

        /// <summary>檢查是否為指定父型別的子型態物件
        /// </summary>
        /// <param name="source"></param>
        /// <param name="typeFullName">父型別Type完整名稱字串</param>
        /// <returns>是否為指定父型別的子型態物件</returns>
        public static bool IsTypeof( this object source, string typeFullName ) => Reflector.IsTypeof( source, typeFullName );

        /// <summary>檢查是否為指定父型別的子型態物件
        /// </summary>
        /// <param name="source"></param>
        /// <param name="type">父型別Type</param>
        /// <returns>是否為指定父型別的子型態物件</returns>
        public static bool IsTypeof( this object source, Type type ) => Reflector.IsTypeof( source, type );

        #endregion Public Object Extension Methods
        

        #region Public Object[] or List<object> Extension Methods

        /// <summary>物件陣列中是否有包含任何Null的元素
        /// </summary>
        /// <param name="source"></param>
        /// <returns>是否有包含任何Null的元素</returns>
        public static bool HasNullElements( this object[] source ) => source.Any( m => m.IsNull() );

        /// <summary>根據指定的泛型型別，將物件陣列轉換成指定型別的陣列
        /// </summary>
        /// <typeparam name="TSource">目標型別</typeparam>
        /// <param name="source">物件陣列</param>
        /// <returns>目標型別的陣列</returns>
        public static TSource[] ConvertTo<TSource>( this object[] source )
            where TSource : new()
        {
            var result = new TSource[ source.Length ];

            for ( int i = 0, len = source.Length; i < len; i++ )
            {
                result[ i ] = (TSource)source[ i ];
            }

            return result;
        }

        /// <summary>根據指定的泛型型別，將物件陣列轉換成指定型別的泛型 List
        /// </summary>
        /// <typeparam name="TSource">目標型別泛型</typeparam>
        /// <param name="source">物件陣列</param>
        /// <returns>目標型別的泛型List</returns>
        public static List<TSource> ToList<TSource>( this object[] source )
            where TSource : new()
        {
            var models = source.ConvertTo<TSource>();
            var result = new List<TSource>( models );
            return result;
        }

        /// <summary>根據指定的泛型型別，將物件陣列轉換成指定型別的泛型List
        /// </summary>
        /// <typeparam name="TSource">目標型別泛型</typeparam>
        /// <param name="source">物件串列集合</param>
        /// <returns>目標型別的泛型List集合</returns>
        public static List<TSource> ToList<TSource>( this List<object> source ) 
            where TSource : new()
        {
            return source.ToArray().ToList<TSource>();
        }
        
        /// <summary>轉換為動態物件的List串列集合
        /// </summary>
        /// <param name="source">物件串列集合</param>
        /// <returns>動態物件的List串列集合</returns>
        public static List<dynamic> ToDynamicList( this List<object> source )
        {
            var result = new List<dynamic>();
            source.ForEach( m => result.Add( m ) );
            return result;
        }

        #endregion Public Object[] or List<object> Extension Methods
    }
}
