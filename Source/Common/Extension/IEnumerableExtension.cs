﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ZayniFramework.Common
{
    /// <summary>對 IEnumerable 介面的擴充類別
    /// </summary>
    public static class IEnumerableExtension
    {
        /// <summary>檢查是否為一個 Null 或空的 IEnumerable 集合 (效能較佳!)
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>是否為一個 Null 或空的 IEnumerable 集合</returns>
        public static bool IsNullOrEmptyCollection<T>( this IEnumerable<T> source ) 
        {
            if ( null == source || !source.Any() )
            {
                return true;
            }

            return false;
        }

        /// <summary>檢查是否不為一個 Null 或空的 IEnumerable 集合 (效能較佳!)
        /// </summary>
        /// <param name="source"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns>是否不為一個 Null 或空的 IEnumerable 集合</returns>
        public static bool IsNotNullOrEmptyCollection<T>( this IEnumerable<T> source ) 
        {
            if ( null == source || !source.Any() )
            {
                return false;
            }

            return true;
        }   

        /// <summary>是否指向 Null 或一個長度為 0 的空集合 (效能較差!)
        /// </summary>
        /// <param name="source">物件集合</param>
        /// <returns>是否指向 Null 或一個長度為 0 的空集合</returns>
        public static bool IsNullOrEmpty( this IEnumerable source )
        {
            if ( null == source || 0 == source.Count() )
            {
                return true;
            }

            return false;
        }

        /// <summary>是否不指向 Null 而且長度不為 0 的集合 (效能較差!)
        /// </summary>
        /// <param name="source">物件集合</param>
        /// <returns>是否不指向 Null 而且長度不為 0 的集合</returns>
        public static bool IsNotNullOrEmpty( this IEnumerable source )
        {
            if ( null == source || 0 == source.Count() )
            {
                return false;
            }

            return true;
        }

        /// <summary>轉換目標集合為為動態物件的集合
        /// </summary>
        /// <param name="source">物件集合</param>
        /// <returns>動態物件的集合</returns>
        public static IEnumerable<dynamic> ToDynamics( this IEnumerable<object> source )
        {
            foreach ( var obj in source ) yield return obj;
        }

        /// <summary>取得集合中的元素數量
        /// </summary>
        /// <param name="source"></param>
        /// <returns>集合中的元素數量</returns>
        public static int Count( this IEnumerable source )
        {
            if ( null == source )
            {
                return 0;
            }

            int count = 0;

            foreach ( var item in source ) 
            {
                count++;
            }

            return count;
        }

        /// <summary>根據傳入的 Lambda 運算式 對指定的屬性去除重複的資料
        /// </summary>
        /// <typeparam name="TSource">資料集合泛型</typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <param name="source">資料集合</param>
        /// <param name="selector">資料欄位篩選 Lambda 運算式</param>
        /// <returns>去除重複資料的資料集合</returns>
        public static IEnumerable<TSource> Distinct<TSource, TKey>( this IEnumerable<TSource> source, Func<TSource, TKey> selector ) 
        {
            var keys = new HashSet<TKey>();
        
            foreach ( TSource m in source )
            {
                var value = selector( m );
            
                if ( keys.Add( value ) )
                {
                    yield return m;
                }
            }
        }

        /// <summary>物件集合中是否有任何Null的元素
        /// </summary>
        /// <param name="self"></param>
        /// <returns>是否有任何Null的元素</returns>
        public static bool HasNullElements( this IEnumerable self )
        {
            foreach ( var m in self )
            {
                if ( m.IsNull() )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>字串集合中是否有任何Null或空字串的元素 (預設不會先對所有字串元素先進行Trim的動作)
        /// </summary>
        /// <param name="self"></param>
        /// <param name="trim">是否要先對所有字串元素先進行Trim的動作，預設是不進行</param>
        /// <returns>是否包含任何Null或空字串的元素</returns>
        public static bool HasNullOrEmptyElements( this IEnumerable<string> self, bool trim = false )
        {
            foreach ( string s in self )
            {
                string m = trim ? s.Trim() : s;

                if ( m.IsNullOrEmpty() )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>串連字串集合的所有元素，並在每個元素之間使用指定的分隔符號
        /// </summary>
        /// <param name="source"></param>
        /// <param name="token">字串分隔符號</param>
        /// <returns>串連完成之後的字串</returns>
        public static string JoinToken( this IEnumerable<string> source, string token )
        {
            var sb = new StringBuilder();

            foreach ( var str in source )
            {
                sb.Append( $"{str}{token}" );
            }

            string result = sb.ToString().RemoveLastAppeared( token );
            return result;
        }

        /// <summary>檢查集合中是否有滿足條件的重覆的元素<para/>
        /// 範例:<para/>
        /// var source1 = new List&lt;string&gt;() { "A", "B", "C", "C" };<para/>
        /// var result1 = source1.HasDuplicates( q => q );<para/>
        /// Assert.IsTrue( result1 );<para/>
        /// var source2 = new List&lt;UserModel&gt;() { ... };<para/>
        /// var result2 = source2.HasDuplicates( q => q.Name );<para/>
        /// Assert.IsTrue( result2 );<para/>
        /// var source4 = new string[] { "1", "2", "A", "BBB" };<para/>
        /// var result4 = source4.HasDuplicates( s => s );<para/>
        /// Assert.IsFalse( result4 );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// var source1 = new List&lt;string&gt;() { "A", "B", "C", "C" };
        /// var result1 = source1.HasDuplicates( q => q );
        /// Assert.IsTrue( result1 );
        /// var source2 = new List&lt;UserModel&gt;() { ... };
        /// var result2 = source2.HasDuplicates( q => q.Name );
        /// Assert.IsTrue( result2 );
        /// var source4 = new string[] { "1", "2", "A", "BBB" };
        /// var result4 = source4.HasDuplicates( s => s );
        /// Assert.IsFalse( result4 );
        /// </code>
        /// </example>
        /// <param name="source"></param>
        /// <param name="selector">資料欄位篩選委派</param>
        /// <typeparam name="T">元素的泛型</typeparam>
        /// <typeparam name="TKey">資料欄位的泛型</typeparam>
        /// <returns>是否有滿足條件的重覆的元素</returns>
        public static bool HasDuplicates<T, TKey>( this IEnumerable<T> source, Func<T, TKey> selector )
        {
            var distinctCount = source.Distinct<T, TKey>( selector ).Count();

            if ( distinctCount < source.Count() )
            {
                return true;
            }

            return false;
        }

        /// <summary>對於參考到 IList 的 IEnumerable 集合進行資料新增的動作。<para/>
        /// * 執行運作為強制轉型成 IListIList&lt;T&gt; 型別之後，呼叫 Add() 方法，並且回傳原 IEnumerable&lt;T&gt; 資料集合。如下:<para/>
        /// * ((IList&lt;T&gt;)source).Add( item );<para/>
        /// </summary>
        /// <param name="source">IEnumerable 資料集合</param>
        /// <param name="item">資料物件</param>
        /// <typeparam name="T">資料泛型</typeparam>
        /// <returns>新增成功後的 IEnumerable&lt;T&gt; 資料集合</returns>
        public static IEnumerable<T> Insert<T>( this IEnumerable<T> source, T item ) 
        {
            ((IList<T>)source).Add( item );
            return source;
        }

        /// <summary>進行 foreach 迭代操作，「不支援」 LINQ 延遲執行 yield return 特性。<para/>
        /// * 迭代所有 IEnumerable 中所有元素，並且在迭代操作全部結束後回傳結果的 IEnumerable 集合。
        /// </summary>
        /// <param name="source">IEnumerable 資料集合</param>
        /// <param name="action">動作委派</param>
        /// <typeparam name="T">資料泛型</typeparam>
        public static IEnumerable<T> Foreach<T>( this IEnumerable<T> source, Action<T> action )
        {
            foreach ( T item in source ) 
            {
                action( item );
            }

            return source;
        }

        /// <summary>進行 foreach 迭代操作，支援 LINQ 延遲執行 yield return 特性。<para/>
        /// <para>* 迭代所有 IEnumerable 中所有元素，但以 yield return 方式進行回傳。</para>
        /// <para>* 適合使用在 IEnumerable 搭配其餘 System.Linq 的 lambda extension methods 進行 chaining call 的方式使用。</para>
        /// <para>* 範例如下:</para>
        /// <para>  * var query = source.Where( q => q.Age &lt; 18 &amp;&amp; q.Gender == "female" )?.ForeachYield( m => m.Commemt = "未成年少女" )?.Select( z => z.Ticket );</para>
        /// </summary>
        /// <example>
        /// <code>
        /// var query = source.Where( q => q.Age &lt; 18 &amp;&amp; q.Gender == "female" )?.ForeachYield( m => m.Commemt = "未成年少女" )?.Select( z => z.Ticket );
        /// </code>
        /// </example>
        /// <param name="source">IEnumerable 資料集合</param>
        /// <param name="action">動作委派</param>
        /// <typeparam name="T">資料泛型</typeparam>
        public static IEnumerable<T> ForeachYield<T>( this IEnumerable<T> source, Action<T> action )
        {
            foreach ( T item in source ) 
            {
                action( item );
                yield return item;
            }
        }
    }
}
