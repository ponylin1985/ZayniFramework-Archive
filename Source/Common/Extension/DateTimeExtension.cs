﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace ZayniFramework.Common
{
    /// <summary>DateTime 或 DateTime? 的擴充類別
    /// </summary>
    public static class DateTimeExtension
    {
        /// <summary>檢查是否為 .NET 預設的 DateTime 值
        /// </summary>
        /// <param name="self"></param>
        /// <returns>是否為 .NET 預設的DateTime值</returns>
        public static bool IsDotNetDefaultDateTime( this DateTime self ) => default ( DateTime ) == self;

        /// <summary>檢查是否為 .NET 框架預設的 DateTime 值:<para/>
        /// 1. 假若為 .NET 的預設值，則回傳 null。<para/>
        /// 2. 假若不為 .NET 的預設值，則回傳原本的日期時間。<para/>
        /// </summary>
        /// <param name="source">目標檢查日期時間</param>
        /// <returns>
        /// 1. 假若為 .NET 的預設值，則回傳 null。<para/>
        /// 2. 假若不為 .NET 的預設值，則回傳原本的日期時間。<para/>
        /// </returns>
        private static DateTime? IsDotNetDefault( this DateTime source )
        {
            if ( default ( DateTime ) == source )
            {
                return null;
            }

            return source;
        }

        /// <summary>轉換成 Unix 作業系統的 UTC 時間戳記
        /// </summary>
        /// <param name="self">來源日期時間</param>
        /// <param name="withMilliseconds">是否包含毫秒數</param>
        /// <returns>Unix 系統的 UTC 時間戳記</returns>
        public static long ToUnixUtcTimestamp( this DateTime self, bool withMilliseconds = false )
        {
            var tp = ( self - new DateTime( 1970, 1, 1, 0, 0, 0, 0 ) );

            if ( withMilliseconds )
            {
                return (long)tp.TotalMilliseconds;
            }

            return (long)tp.TotalSeconds;
        }

        /// <summary>將 Unix 系統的 UTC 時間戳記轉換為日期時間
        /// </summary>
        /// <param name="unixUtcTimestamp">Unix 系統的 UTC 時間戳記，不包括毫秒數</param>
        /// <param name="withMilliseconds">是否包含毫秒數</param>
        /// <returns>日期時間</returns>
        public static DateTime ToDateTimeFromUnixTimestamp( this long unixUtcTimestamp, bool withMilliseconds = false )
        {
            var dt = new DateTime( 1970, 1, 1, 0, 0, 0, 0 );

            if ( withMilliseconds )
            {
                return dt.AddMilliseconds( unixUtcTimestamp );
            }

            return dt.AddSeconds( unixUtcTimestamp );
        }   

        /// <summary>轉換成 Unix 系統的 UTC 時間戳記
        /// </summary>
        /// <param name="self">來源日期時間</param>
        /// <param name="withMilliseconds">是否包含毫秒數</param>
        /// <returns>Unix 系統的 UTC 時間戳記</returns>
        public static long ToUnixUtcTimestamp( this DateTime? self, bool withMilliseconds = false )
        {
            if ( null == self )
            {
                return 0;
            }

            return ((DateTime)self).ToUnixUtcTimestamp( withMilliseconds );
        }

        /// <summary>判斷日期是否在指定的開始日期與結束日期中間
        /// </summary>
        /// <param name="self"></param>
        /// <param name="beginDate">指定的開始日期</param>
        /// <param name="endDate">指定的結束日期</param>
        /// <param name="includeBeginDate">是否包括開始日期</param>
        /// <param name="includeEndDate">是否包括結束日期</param>
        /// <returns>是否在指定的開始日期與結束日期中間</returns>
        public static bool IsBetween( this DateTime self, DateTime beginDate, DateTime endDate, bool includeBeginDate = false, bool includeEndDate = false )
        {
            if ( beginDate > endDate )
            {
                throw new ArgumentException( "輸入的引數不合法: 起始時間不可以大於結束時間!" );
            }

            bool beginExpression = includeBeginDate ? self >= beginDate : self > beginDate;
            bool endExpression   = includeEndDate   ? self <= endDate   : self < endDate;

            return beginExpression && endExpression;
        }

        /// <summary>比較是否大於比較日期
        /// </summary>
        /// <param name="self"></param>
        /// <param name="target">比較日期</param>
        /// <param name="includeTarget">是否包含比較日期</param>
        /// <returns>回傳是否大於比較日期</returns>
        public static bool IsGreater( this DateTime self, DateTime target, bool includeTarget = false ) =>
            includeTarget ? self >= target : self > target;

        /// <summary>比較是否小於比較日期
        /// </summary>
        /// <param name="self"></param>
        /// <param name="target">比較日期</param>
        /// <param name="includeTarget">是否包含比較日期</param>
        /// <returns>回傳是否小於比較日期</returns>
        public static bool IsSmaller( this DateTime self, DateTime target, bool includeTarget = false ) =>
            includeTarget ? self <= target : self < target;

        // 20151030 Pony Says: Microsoft 支援的 Time Zone ID 索引值列表請參考 (這是 windows 作業系統的 TimeZoneId 索引!)
        // https://msdn.microsoft.com/en-us/library/ms912391(v=winembedded.11).aspx
        /// <summary>將日期時間轉換到指定目標時區的時間<para/>
        /// 1. 目標時區必須有安裝在作業系統中。<para/>
        /// 2. DateTime的Kind屬性必須為Local，轉換的基準時區以作業系統的預設時區為準。(預設new DateTime() 或DateTime.Now產生出的DateTime.Kind屬性就為Local。)<para/>
        /// </summary>
        /// <remarks>目標時區必須有安裝在作業系統中</remarks>
        /// <param name="self">DateTime的Kind屬性必須為Local，轉換的基準時區以作業系統的預設時區為準</param>
        /// <param name="timeZoneId">目標時區的ID值 (目標時區必須有安裝在作業系統中)</param>
        /// <returns>目標時區的時間</returns>
        public static DateTime ConvertTime( this DateTime self, string timeZoneId )
        {
            if ( RuntimeEnvironmentHelper.IsWindowsOS() ) 
            {
                return TimeZoneInfo.ConvertTimeBySystemTimeZoneId( self, timeZoneId );
            }

            var timeZone = ConvertToLinuxTimeZoneId( timeZoneId );
            timeZone.IsNotNullOrEmpty( tz => timeZoneId = TimeZoneInfo.FindSystemTimeZoneById( timeZone )?.Id );
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId( self, timeZoneId );
        }

        /// <summary>將 DateTime 日期時間值從指定的「來源時區」轉換到「目標時區」。<para/>
        /// 使用方式如下:<para/>
        /// * 時區都必須安裝在作業系統中。<para/>
        /// * 此方法已經動將 DateTime 的 Kind 屬性轉換為 Unspecified，再進行轉換。<para/>
        /// </summary>
        /// <param name="self">待轉換的日期時間</param>
        /// <param name="sourceTimeZoneId">來源時區的ID值 (來源時區必須有安裝在作業系統中)</param>
        /// <param name="targetTimeZoneId">目標時區的ID值 (目標時區必須有安裝在作業系統中)</param>
        /// <returns>目標時區的時間</returns>
        public static DateTime ConvertTimeFromSourceToTarget( this DateTime self, string sourceTimeZoneId, string targetTimeZoneId ) 
        {
            if ( RuntimeEnvironmentHelper.IsWindowsOS() ) 
            {
                return TimeZoneInfo.ConvertTimeBySystemTimeZoneId( new DateTime( self.Ticks, DateTimeKind.Unspecified ), sourceTimeZoneId, targetTimeZoneId );
            }

            var sourceTimeZone = ConvertToLinuxTimeZoneId( sourceTimeZoneId );
            sourceTimeZone.IsNotNullOrEmpty( tz => sourceTimeZoneId = TimeZoneInfo.FindSystemTimeZoneById( sourceTimeZone )?.Id );

            var targetTimeZone = ConvertToLinuxTimeZoneId( targetTimeZoneId );
            targetTimeZone.IsNotNullOrEmpty( tz => targetTimeZoneId = TimeZoneInfo.FindSystemTimeZoneById( targetTimeZone )?.Id );

            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId( new DateTime( self.Ticks, DateTimeKind.Unspecified ), sourceTimeZoneId, targetTimeZoneId );
        }

        #region Marked Obsoleted Methods

        // /// <summary>將日期時間從指定的來源時區轉換到目標時區，使用方式如下:<para/>
        // /// * 時區都必須安裝在作業系統中。<para/>
        // /// * DateTime 的 Kind 屬性，若設定為 Local，傳入的 sourceTimeZoneId 時區必須要跟 DateTime 本身建立起的時區一致，否則會有 Exception。<para/>
        // /// * DateTime 的 Kind 屬性，若設定為 Unspecified，則可以自行指定sourceTimeZoneId 的時區 ID 值。<para/>
        // /// * Kind 屬性是 ReadOnly 唯讀屬性，無法修改，只能在 new DateTime 時呼叫有 DateTimeKind 參數的建構子。<para/>
        // /// * 範例: <para/>
        // /// * new DateTime( 2015, 5, 8, 2, 0, 45, DateTimeKind.Unspecified );<para/>
        // /// * new DateTime( DateTime.Now.Ticks, DateTimeKind.Unspecified );<para/>
        // /// </summary>
        // /// <remarks>時區必須有安裝在作業系統中</remarks>
        // /// <param name="self">
        // /// 1. DateTime的Kind 屬性，若設定為 Local，傳入的 sourceTimeZoneId 時區必須要跟 DateTime 本身建立起的時區一致，否則會有 Exception。<para/>
        // /// 2. DateTime的Kind 屬性，若設定為 Unspecified，則可以自行指定 sourceTimeZoneId 的時區 ID 值。<para/>
        // /// </param>
        // /// <param name="sourceTimeZoneId">來源時區的ID值 (來源時區必須有安裝在作業系統中)</param>
        // /// <param name="targetTimeZoneId">目標時區的ID值 (目標時區必須有安裝在作業系統中)</param>
        // /// <returns>目標時區的時間</returns>
        // [Obsolete( "建議呼叫 ConvertTimeFromSourceToTarget() 擴充方法。", false )]
        // public static DateTime ConvertTimeFromSourceToTarget_Obsolete( this DateTime self, string sourceTimeZoneId, string targetTimeZoneId ) 
        // {
        //     if ( RuntimeEnvironmentHelper.IsWindowsOS() ) 
        //     {
        //         return TimeZoneInfo.ConvertTimeBySystemTimeZoneId( new DateTime( self.Ticks, DateTimeKind.Unspecified ), sourceTimeZoneId, targetTimeZoneId );
        //     }

        //     var sourceTimeZone = ConvertToLinuxTimeZoneId( sourceTimeZoneId );
        //     sourceTimeZone.IsNotNullOrEmpty( tz => sourceTimeZoneId = TimeZoneInfo.FindSystemTimeZoneById( sourceTimeZone )?.Id );

        //     var targetTimeZone = ConvertToLinuxTimeZoneId( targetTimeZoneId );
        //     targetTimeZone.IsNotNullOrEmpty( tz => targetTimeZoneId = TimeZoneInfo.FindSystemTimeZoneById( targetTimeZone )?.Id );

        //     return TimeZoneInfo.ConvertTimeBySystemTimeZoneId( self, sourceTimeZoneId, targetTimeZoneId );
        // }

        #endregion Marked Obsoleted Methods

        /// <summary>將日期時間從來源的時區時間轉換成 UTC 時間
        /// </summary>
        /// <param name="self"></param>
        /// <param name="sourceTimeZoneId">來源時區代碼ID (來源時區必須有安裝在作業系統中)</param>
        /// <returns>UTC 時間</returns>
        public static DateTime ConvertToUtcTime( this DateTime self, string sourceTimeZoneId ) 
        {
            if ( RuntimeEnvironmentHelper.IsWindowsOS() ) 
            {
                return TimeZoneInfo.ConvertTimeToUtc( self, TimeZoneInfo.FindSystemTimeZoneById( sourceTimeZoneId ) );
            }

            var sourceTimeZone = ConvertToLinuxTimeZoneId( sourceTimeZoneId );
            sourceTimeZone.IsNotNullOrEmpty( tz => sourceTimeZoneId = TimeZoneInfo.FindSystemTimeZoneById( sourceTimeZone )?.Id );
            return TimeZoneInfo.ConvertTimeToUtc( self, TimeZoneInfo.FindSystemTimeZoneById( sourceTimeZoneId ) );
        }

        /// <summary>將 UTC 日期時間轉換為目標時區的時間。<para/>
        /// DateTime 物件本身的 Kind 屬性必須為 Utc，才可以正確呼叫此擴充方法。
        /// </summary>
        /// <param name="self"></param>
        /// <param name="targetTimeZoneId">目標時區代碼ID (目標時區必須又安裝在作業系統中)</param>
        /// <returns>目標時區的時間</returns>
        public static DateTime ConvertFromUtcTime( this DateTime self, string targetTimeZoneId ) 
        {
            if ( RuntimeEnvironmentHelper.IsWindowsOS() ) 
            {
                return TimeZoneInfo.ConvertTimeFromUtc( self, TimeZoneInfo.FindSystemTimeZoneById( targetTimeZoneId ) );
            }

            var targetTimeZone = ConvertToLinuxTimeZoneId( targetTimeZoneId );
            targetTimeZone.IsNotNullOrEmpty( tz => targetTimeZoneId = TimeZoneInfo.FindSystemTimeZoneById( targetTimeZone )?.Id );
            return TimeZoneInfo.ConvertTimeFromUtc( self, TimeZoneInfo.FindSystemTimeZoneById( targetTimeZoneId ) );
        }

        /// <summary>將 DateTime 的 Kind 屬性轉換成 DateTimeKind.Utc 的 DateTime，此轉換不會影響原本 DateTime 的時間值。<para/>
        /// * <para>範例如下:</para>
        /// <para>
        /// ```
        ///     var dt     = new DateTime( 2018, 5, 12, 17, 22, 56 ).AddMilliseconds( 657 );<para/>
        ///     var result = dt.ToUtcKind();
        /// ```
        /// </para>
        /// </summary>
        /// <param name="self"></param>
        /// <returns>產生新的 DateTime 但日期時間的值與原本的完全相同，只是 Kind 屬性值調整為 DateTimeKine.Utc。</returns>
        public static DateTime ToUtcKind( this DateTime self ) => DateTime.SpecifyKind( self, DateTimeKind.Utc );

        /// <summary>將 DateTime? 的 Kind 屬性轉換成 DateTimeKind.Utc 的 DateTime，此轉換不會影響原本 DateTime? 的時間值。<para/>
        /// * 假若傳入 null 的 DateTime? 物件，則直接回傳 null。
        /// * <para>範例如下:</para>
        /// <para>
        /// ```
        /// DateTime? dt2     = new DateTime( 2018, 5, 12, 17, 22, 56 ).AddMilliseconds( 657 );
        /// DateTime? result2 = dt2.ToUtcKind();
        /// DateTime source = (DateTime)dt2;
        /// DateTime actual = (DateTime)result2;
        /// Assert.AreEqual( DateTimeKind.Utc, actual.Kind );
        /// Assert.AreEqual( source.Year, actual.Year );
        /// Assert.AreEqual( source.Month, actual.Month );
        /// Assert.AreEqual( source.Day, actual.Day );
        /// Assert.AreEqual( source.Hour, actual.Hour );
        /// Assert.AreEqual( source.Minute, actual.Minute );
        /// Assert.AreEqual( source.Second, actual.Second );
        /// Assert.AreEqual( source.Millisecond, actual.Millisecond );
        /// DateTime? dt3     = null;
        /// DateTime? result3 = dt3.ToUtcKind();
        /// Assert.IsNull( result3 );
        /// ```
        /// </para>
        /// </summary>
        /// <param name="self"></param>
        /// <returns>
        /// * 產生新的 DateTime? 但日期時間的值與原本的完全相同，只是 Kind 屬性值調整為 DateTimeKine.Utc。
        /// * 假若傳入 null 的 DateTime? 物件，則直接回傳 null。
        /// </returns>
        public static DateTime? ToUtcKind( this DateTime? self ) 
        {
            if ( null == self )
            {
                return null;
            }

            return DateTime.SpecifyKind( (DateTime)self, DateTimeKind.Utc );
        }

        /// <summary>轉換 Windows 作業系統的 TimeZone Id 成 Linux 或 macOS 作業系統的 TimeZone Id 時區代碼
        /// </summary>
        /// <reference>參考連結:<para/>
        /// https://stackoverflow.com/questions/31796391/system-timezonenotfoundexception-error-while-getting-datetime-for-particular-tim
        /// </reference>
        /// <param name="windowsTimeZoneId">Windows 作業系統的 TimeZone Id 時區代碼</param>
        /// <returns>Linux 或 macOS (Unix) 作業系統的 TimeZone Id 時區代碼</returns>
        public static string ConvertToLinuxTimeZoneId( string windowsTimeZoneId )
        {
            var timeZoneIdMappings = new Dictionary<string, string>()
            {
                { "Africa/Bangui", "W. Central Africa Standard Time" },
                { "Africa/Cairo", "Egypt Standard Time" },
                { "Africa/Casablanca", "Morocco Standard Time" },
                { "Africa/Harare", "South Africa Standard Time" },
                { "Africa/Johannesburg", "South Africa Standard Time" },
                { "Africa/Lagos", "W. Central Africa Standard Time" },
                { "Africa/Monrovia", "Greenwich Standard Time" },
                { "Africa/Nairobi", "E. Africa Standard Time" },
                { "Africa/Windhoek", "Namibia Standard Time" },
                { "America/Anchorage", "Alaskan Standard Time" },
                { "America/Argentina/San_Juan", "Argentina Standard Time" },
                { "America/Asuncion", "Paraguay Standard Time" },
                { "America/Bahia", "Bahia Standard Time" },
                { "America/Bogota", "SA Pacific Standard Time" },
                { "America/Buenos_Aires", "Argentina Standard Time" },
                { "America/Caracas", "Venezuela Standard Time" },
                { "America/Cayenne", "SA Eastern Standard Time" },
                { "America/Chicago", "Central Standard Time" },
                { "America/Chihuahua", "Mountain Standard Time (Mexico)" },
                { "America/Cuiaba", "Central Brazilian Standard Time" },
                { "America/Denver", "Mountain Standard Time" },
                { "America/Fortaleza", "SA Eastern Standard Time" },
                { "America/Godthab", "Greenland Standard Time" },
                { "America/Guatemala", "Central America Standard Time" },
                { "America/Halifax", "Atlantic Standard Time" },
                { "America/Indianapolis", "US Eastern Standard Time" },
                { "America/Indiana/Indianapolis", "US Eastern Standard Time" },
                { "America/La_Paz", "SA Western Standard Time" },
                { "America/Los_Angeles", "Pacific Standard Time" },
                { "America/Mexico_City", "Mexico Standard Time" },
                { "America/Montevideo", "Montevideo Standard Time" },
                { "America/New_York", "Eastern Standard Time" },
                { "America/Noronha", "UTC-02" },
                { "America/Phoenix", "US Mountain Standard Time" },
                { "America/Regina", "Canada Central Standard Time" },
                { "America/Santa_Isabel", "Pacific Standard Time (Mexico)" },
                { "America/Santiago", "Pacific SA Standard Time" },
                { "America/Sao_Paulo", "E. South America Standard Time" },
                { "America/St_Johns", "Newfoundland Standard Time" },
                { "America/Tijuana", "Pacific Standard Time" },
                { "Antarctica/McMurdo", "New Zealand Standard Time" },
                { "Atlantic/South_Georgia", "UTC-02" },
                { "Asia/Almaty", "Central Asia Standard Time" },
                { "Asia/Amman", "Jordan Standard Time" },
                { "Asia/Baghdad", "Arabic Standard Time" },
                { "Asia/Baku", "Azerbaijan Standard Time" },
                { "Asia/Bangkok", "SE Asia Standard Time" },
                { "Asia/Beirut", "Middle East Standard Time" },
                { "Asia/Calcutta", "India Standard Time" },
                { "Asia/Colombo", "Sri Lanka Standard Time" },
                { "Asia/Damascus", "Syria Standard Time" },
                { "Asia/Dhaka", "Bangladesh Standard Time" },
                { "Asia/Dubai", "Arabian Standard Time" },
                { "Asia/Irkutsk", "North Asia East Standard Time" },
                { "Asia/Jerusalem", "Israel Standard Time" },
                { "Asia/Kabul", "Afghanistan Standard Time" },
                { "Asia/Kamchatka", "Kamchatka Standard Time" },
                { "Asia/Karachi", "Pakistan Standard Time" },
                { "Asia/Katmandu", "Nepal Standard Time" },
                { "Asia/Kolkata", "India Standard Time" },
                { "Asia/Krasnoyarsk", "North Asia Standard Time" },
                { "Asia/Kuala_Lumpur", "Singapore Standard Time" },
                { "Asia/Kuwait", "Arab Standard Time" },
                { "Asia/Magadan", "Magadan Standard Time" },
                { "Asia/Muscat", "Arabian Standard Time" },
                { "Asia/Novosibirsk", "N. Central Asia Standard Time" },
                { "Asia/Oral", "West Asia Standard Time" },
                { "Asia/Rangoon", "Myanmar Standard Time" },
                { "Asia/Riyadh", "Arab Standard Time" },
                { "Asia/Seoul", "Korea Standard Time" },
                { "Asia/Shanghai", "China Standard Time" },
                { "Asia/Singapore", "Singapore Standard Time" },
                { "Asia/Taipei", "Taipei Standard Time" },
                { "Asia/Tashkent", "West Asia Standard Time" },
                { "Asia/Tbilisi", "Georgian Standard Time" },
                { "Asia/Tehran", "Iran Standard Time" },
                { "Asia/Tokyo", "Tokyo Standard Time" },
                { "Asia/Ulaanbaatar", "Ulaanbaatar Standard Time" },
                { "Asia/Vladivostok", "Vladivostok Standard Time" },
                { "Asia/Yakutsk", "Yakutsk Standard Time" },
                { "Asia/Yekaterinburg", "Ekaterinburg Standard Time" },
                { "Asia/Yerevan", "Armenian Standard Time" },
                { "Atlantic/Azores", "Azores Standard Time" },
                { "Atlantic/Cape_Verde", "Cape Verde Standard Time" },
                { "Atlantic/Reykjavik", "Greenwich Standard Time" },
                { "Australia/Adelaide", "Cen. Australia Standard Time" },
                { "Australia/Brisbane", "E. Australia Standard Time" },
                { "Australia/Darwin", "AUS Central Standard Time" },
                { "Australia/Hobart", "Tasmania Standard Time" },
                { "Australia/Perth", "W. Australia Standard Time" },
                { "Australia/Sydney", "AUS Eastern Standard Time" },
                { "Etc/GMT", "UTC" },
                { "Etc/GMT+11", "UTC-11" },
                { "Etc/GMT+12", "Dateline Standard Time" },
                { "Etc/GMT+2", "UTC-02" },
                { "Etc/GMT-12", "UTC+12" },
                { "Europe/Amsterdam", "W. Europe Standard Time" },
                { "Europe/Athens", "GTB Standard Time" },
                { "Europe/Belgrade", "Central Europe Standard Time" },
                { "Europe/Berlin", "W. Europe Standard Time" },
                { "Europe/Brussels", "Romance Standard Time" },
                { "Europe/Budapest", "Central Europe Standard Time" },
                { "Europe/Dublin", "GMT Standard Time" },
                { "Europe/Helsinki", "FLE Standard Time" },
                { "Europe/Istanbul", "GTB Standard Time" },
                { "Europe/Kiev", "FLE Standard Time" },
                { "Europe/London", "GMT Standard Time" },
                { "Europe/Minsk", "E. Europe Standard Time" },
                { "Europe/Moscow", "Russian Standard Time" },
                { "Europe/Paris", "Romance Standard Time" },
                { "Europe/Sarajevo", "Central European Standard Time" },
                { "Europe/Warsaw", "Central European Standard Time" },
                { "Indian/Mauritius", "Mauritius Standard Time" },
                { "Pacific/Apia", "Samoa Standard Time" },
                { "Pacific/Auckland", "New Zealand Standard Time" },
                { "Pacific/Fiji", "Fiji Standard Time" },
                { "Pacific/Guadalcanal", "Central Pacific Standard Time" },
                { "Pacific/Guam", "West Pacific Standard Time" },
                { "Pacific/Honolulu", "Hawaiian Standard Time" },
                { "Pacific/Pago_Pago", "UTC-11" },
                { "Pacific/Port_Moresby", "West Pacific Standard Time" },
                { "Pacific/Tongatapu", "Tonga Standard Time" }
            };

            string timeInfoKey = string.Empty;

            if ( timeZoneIdMappings.ContainsValue( windowsTimeZoneId ) ) 
            {
                timeInfoKey = timeZoneIdMappings.FirstOrDefault( x => x.Value == windowsTimeZoneId ).Key;
            }

            return timeInfoKey;
        }
    }
}
