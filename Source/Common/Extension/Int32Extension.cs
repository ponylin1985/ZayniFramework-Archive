﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>Int32 的擴充類別
    /// </summary>
    public static class Int32Extension
    {
        /// <summary>檢查 Int32 數字是否為 .NET CLR 的系統預設值 0，如果是則將自動使用傳入的 Int32 整數值取代，如果不是，則不會有取代的效果。
        /// </summary>
        /// <param name="self"></param>
        /// <param name="value">取代預設值 0 的 Int32 整數</param>
        /// <returns></returns>
        public static int IsDotNetDefault( this int self, int value ) => 0 == self ? value : self;
    }
}
