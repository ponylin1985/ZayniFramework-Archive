﻿namespace ZayniFramework.Common
{
    /// <summary>參考型別的 Long (Int64)長整數值
    /// </summary>
    public class LongR
    {
        /// <summary>預設建構子
        /// </summary>
        public LongR()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="value">Long (Int64)長整數值</param>
        public LongR( long value )
        {
            Value = value;
        }

        /// <summary>Long長整數值
        /// </summary>
        public long Value
        {
            get;
            set;
        }
    }
}
