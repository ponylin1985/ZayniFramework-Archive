﻿namespace ZayniFramework.Common
{
    /// <summary>參考型別的Double浮點數值
    /// </summary>
    public class DoubleR
    {
        /// <summary>預設建構子
        /// </summary>
        public DoubleR()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="value">Double浮點數值</param>
        public DoubleR( double value )
        {
            Value = value;
        }

        /// <summary>Double浮點數值
        /// </summary>
        public double Value
        {
            get;
            set;
        }
    }
}
