﻿namespace ZayniFramework.Common
{
    /// <summary>參考型別的Int32整數值
    /// </summary>
    public class Int
    {
        /// <summary>預設建構子
        /// </summary>
        public Int()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="value">Int32整數值</param>
        public Int( int value )
        {
            Value = value;
        }

        /// <summary>Int32整數值
        /// </summary>
        public int Value
        {
            get;
            set;
        }
    }
}
