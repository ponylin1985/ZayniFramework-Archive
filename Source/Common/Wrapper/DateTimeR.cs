﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>參考型別的DateTime日期
    /// </summary>
    public class DateTimeR
    {
        /// <summary>預設建構子
        /// </summary>
        public DateTimeR()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="value">DateTime日期</param>
        public DateTimeR( DateTime value )
        {
            Value = value;
        }

        /// <summary>DateTime日期
        /// </summary>
        public DateTime Value
        {
            get;
            set;
        }
    }
}
