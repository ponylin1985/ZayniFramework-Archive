﻿namespace ZayniFramework.Common
{
    /// <summary>參考型別的Byte整數值
    /// </summary>
    public class ByteR
    {
        /// <summary>預設建構子
        /// </summary>
        public ByteR()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="value">Byte整數值 (不帶正負號的 8 位元整數)</param>
        public ByteR( byte value )
        {
            Value = value;
        }

        /// <summary>Byte整數值 (不帶正負號的 8 位元整數)
        /// </summary>
        public byte Value
        {
            get;
            set;
        }
    }
}
