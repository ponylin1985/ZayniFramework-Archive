﻿namespace ZayniFramework.Common
{
    /// <summary>參考型別的Decimal浮點數值
    /// </summary>
    public class DecimalR
    {
        /// <summary>預設建構子
        /// </summary>
        public DecimalR()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="value">Decimal浮點數值</param>
        public DecimalR( decimal value )
        {
            Value = value;
        }

        /// <summary>Decimal浮點數值
        /// </summary>
        public decimal Value
        {
            get;
            set;
        }
    }
}
