﻿namespace ZayniFramework.Common
{
    /// <summary>參考型別的布林真假值
    /// </summary>
    public class Bool
    {
        /// <summary>預設建構子
        /// </summary>
        public Bool( bool value = false )
        {
            Value = value;
        }

        /// <summary>真假值
        /// </summary>
        public bool Value
        {
            get;
            set;
        }
    }
}
