﻿namespace ZayniFramework.Common
{
    /// <summary>參考型別的Float浮點數值
    /// </summary>
    public class FloatR
    {
        /// <summary>預設建構子
        /// </summary>
        public FloatR()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="value">Float浮點數值</param>
        public FloatR( float value )
        {
            Value = value;
        }

        /// <summary>Float浮點數值
        /// </summary>
        public float Value
        {
            get;
            set;
        }
    }
}
