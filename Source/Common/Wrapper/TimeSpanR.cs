﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>參考型別的TimeSpan
    /// </summary>
    public class TimeSpanR
    {
        /// <summary>預設建構子
        /// </summary>
        public TimeSpanR()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="value">TimeSpan浮點數值</param>
        public TimeSpanR( TimeSpan value )
        {
            Value = value;
        }

        /// <summary>TimeSpan浮點數值
        /// </summary>
        public TimeSpan Value
        {
            get;
            set;
        }
    }
}
