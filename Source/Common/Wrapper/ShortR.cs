﻿namespace ZayniFramework.Common
{
    /// <summary>參考型別的Short (Int16)短整數值
    /// </summary>
    public class ShortR
    {
        /// <summary>預設建構子
        /// </summary>
        public ShortR()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="value">Short (Int16)短整數值</param>
        public ShortR( short value )
        {
            Value = value;
        }

        /// <summary>Short (Int16)短整數值
        /// </summary>
        public short Value
        {
            get;
            set;
        }
    }
}
