﻿namespace ZayniFramework.Common
{
    /// <summary>執行引擎基底介面
    /// </summary>
    public interface IExecutable
    {
        /// <summary>執行動作
        /// </summary>
        void Execute();
    }

    /// <summary>執行引擎基底介面
    /// </summary>
    /// <typeparam name="TData">執行結果的資料泛型</typeparam>
    public interface IExecuteResult<TData>
    {
        /// <summary>執行動作
        /// </summary>
        /// <param name="args">執行參數</param>
        /// <returns>執行結果</returns>
        Result<TData> Execute( params object[] args );
    }

    /// <summary>執行引擎基底介面
    /// </summary>
    public interface IExecuteResult : IExecuteResult<object>
    {   
    }
}
