﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>字串壓縮器介面合約
    /// </summary>
    public interface ITextCompressor
    {
        /// <summary>壓縮目標字串
        /// </summary>
        /// <param name="target">目標字串</param>
        /// <returns>壓縮過後的字串</returns>
        string Compress( string target );

        /// <summary>解壓縮目標字串
        /// </summary>
        /// <param name="target">有壓縮過的目標字串</param>
        /// <returns>解壓縮過的字串</returns>
        string Decompress( string target );
    }
}
