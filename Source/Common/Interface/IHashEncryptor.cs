﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>單向雜湊加解密元件合約
    /// </summary>
    public interface IHashEncryptor
    {
        /// <summary>對來源明文字串進行雜湊加密 (如果雜湊過後的密文需要Salt，此參數一定要傳入，且不可以為空字串)
        /// </summary>
        /// <param name="source">來源明文字串</param>
        /// <param name="saltKey">加鹽的唯一字串 (如果雜湊過後的密文需要Salt，此參數一定要傳入，且不可以為空字串)</param>
        /// <returns>雜湊加密過後的密文字串</returns>
        string HashEncrypt( string source, string saltKey );
    }
}
