﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>資料存取元件異常例外處理介面
    /// </summary>
    public interface IDaoExceptionHandling
    {
        /// <summary>建立資料庫連線異常的回呼處理
        /// </summary>
        ExceptionHandler CreateConnectionExceptionCallback
        {
            get;
            set;
        }
        
        /// <summary>開啟資料庫連線異常的回呼處理
        /// </summary>
        ExceptionHandler OpenConnectionExceptionCallback 
        {
            get;
            set;
        }

        /// <summary>開啟資料庫交易異常的回呼處理
        /// </summary>
        ExceptionHandler BeginTransactionExceptionCallback
        {
            get;
            set;
        }

        /// <summary>確認資料庫交易異常的回呼處理
        /// </summary>
        ExceptionHandler CommitTransactionExceptionCallback
        {
            get;
            set;
        }

        /// <summary>退回資料庫交易異常的回呼處理
        /// </summary>
        ExceptionHandler RollbackTransactionExceptionCallback
        {
            get;
            set;
        }

        /// <summary>釋放資料庫連線資源異常的回呼處理
        /// </summary>
        ExceptionHandler DisposeExceptionCallback
        {
            get;
            set;
        }

        /// <summary>執行SQL指令異常的回呼處理
        /// </summary>
        ExceptionHandler ExecuteNonQueryExceptionCallback
        {
            get;
            set;
        }

        /// <summary>執行SQL資料讀取器異常的回呼處理
        /// </summary>
        ExceptionHandler ExecuteReaderExceptionCallback
        {
            get;
            set;
        }

        /// <summary>執行SQL單一純量查詢異常的回呼處理
        /// </summary>
        ExceptionHandler ExecuteScalarExceptionCallback
        {
            get;
            set;
        }

        /// <summary>執行SQL查詢異常的回呼處理
        /// </summary>
        ExceptionHandler LoadDataSetExceptionCallback
        {
            get;
            set;
        }

        /// <summary>執行ORM資料繫結異常的回呼處理
        /// </summary>
        ExceptionHandler OrmDataBindingExceptionCallback
        {
            get;
            set;
        }

        /// <summary>取得SQL敘述資料庫指令異常的回呼處理
        /// </summary>
        ExceptionHandler GetSqlStringExceptionCallback
        {
            get;
            set;
        }

        /// <summary>取得資料庫預存程序指令異常的回呼處理
        /// </summary>
        ExceptionHandler GetStoredProcExceptionCallback
        {
            get;
            set;
        }

        /// <summary>加入輸入參數異常的回呼處理
        /// </summary>
        ExceptionHandler AddInParameterExceptionCallback
        {
            get;
            set;
        }

        /// <summary>加入輸出參數異常的回呼處理
        /// </summary>
        ExceptionHandler AddOutParameterExceptionCallback
        {
            get;
            set;
        }

        /// <summary>取得參數值異常的回呼處理
        /// </summary>
        ExceptionHandler GetParameterValueExceptionCallback
        {
            get;
            set;
        }

        /// <summary>設定參數值異常的回呼處理
        /// </summary>
        ExceptionHandler SetParameterValueExceptionCallback
        {
            get;
            set;
        }

        /// <summary>清除所有參數值異常的回呼處理
        /// </summary>
        ExceptionHandler ClearAllParametersExceptionCallback
        {
            get;
            set;
        }

        /// <summary>其他異常的回呼處理
        /// </summary>
        ExceptionHandler OtherExceptionCallback
        {
            get;
            set;
        }
    }
}
