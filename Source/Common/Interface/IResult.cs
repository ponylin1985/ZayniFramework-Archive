﻿namespace ZayniFramework.Common
{
    /// <summary>基礎執行結果介面
    /// </summary>
    public interface IResult
    {
        /// <summary>執行結果是否成功
        /// </summary>
        bool Success { get; set; }

        /// <summary>執行結果訊息 (or 錯誤訊息)
        /// </summary>
        string Message { get; set; }

        /// <summary>執行結果回傳代碼
        /// </summary>
        string Code { get; set; }
    }

    /// <summary>基礎執行結果介面
    /// </summary>
    /// <typeparam name="TData">執行結果資料泛型</typeparam>
    public interface IResult<TData> : IResult 
    {
        /// <summary>執行結果的資料集合
        /// </summary>
        TData Data { get; set; }
    }
}
