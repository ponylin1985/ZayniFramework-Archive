﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>字串序列化元件合約基底
    /// </summary>
    public interface IStringSerializer
    {
        /// <summary>序列化目標物件
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <param name="type">目標物件的型別</param>
        /// <returns>序列化結果</returns>
        SerializeResult Serialize( object target, Type type = null );

        /// <summary>以非同步序列化目標物件
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <param name="type">目標物件的型別</param>
        /// <returns>序列化結果</returns>
        Task<SerializeResult> SerializeAsync( object target, Type type = null );

        /// <summary>序列化目標物件
        /// </summary>
        /// <typeparam name="TSource">目標物件的泛型</typeparam>
        /// <param name="target">目標物件</param>
        /// <returns>序列化結果</returns>
        SerializeResult Serialize<TSource>( object target );

        /// <summary>以非同步序列化目標物件
        /// </summary>
        /// <typeparam name="TSource">目標物件的泛型</typeparam>
        /// <param name="target">目標物件</param>
        /// <returns>序列化結果</returns>
        Task<SerializeResult> SerializeAsync<TSource>( object target );

        /// <summary>反序列化目標字串
        /// </summary>
        /// <param name="target">目標字串</param>
        /// <param name="type">結果物件的型別</param>
        /// <returns>反序列化結果</returns>
        DeserializeResult Deserialize( string target, Type type = null );

        /// <summary>以非同步反序列化目標字串
        /// </summary>
        /// <param name="target">目標字串</param>
        /// <param name="type">結果物件的型別</param>
        /// <returns>反序列化結果</returns>
        Task<DeserializeResult> DeserializeAsync( string target, Type type = null );

        /// <summary>反序列化目標字串
        /// </summary>
        /// <typeparam name="TSource">結果物件的泛型</typeparam>
        /// <param name="target">目標字串</param>
        /// <returns>反序列化結果</returns>
        DeserializeResult<TSource> Deserialize<TSource>( string target )
            where TSource : class, new();

        /// <summary>以非同步反序列化目標字串
        /// </summary>
        /// <typeparam name="TSource">結果物件的泛型</typeparam>
        /// <param name="target">目標字串</param>
        /// <returns>反序列化結果</returns>
        Task<DeserializeResult<TSource>> DeserializeAsync<TSource>( string target )
            where TSource : class, new();
    }
}
