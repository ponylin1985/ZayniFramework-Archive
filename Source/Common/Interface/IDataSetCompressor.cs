﻿using System;
using System.Data;


namespace ZayniFramework.Common
{
    /// <summary>DataSet 壓縮器介面
    /// </summary>
    public interface IDataSetCompressor
    {
        /// <summary>壓縮目標的DataSet物件
        /// </summary>
        /// <param name="target">目標DataSet</param>
        /// <returns>壓縮過的二進位資料</returns>
        byte[] Compress( DataSet target );

        /// <summary>解壓縮目標二進位資料
        /// </summary>
        /// <param name="target">目標二進位資料</param>
        /// <returns>解壓縮過後的DataSet物件</returns>
        DataSet Decompress( byte[] target );
    }
}
