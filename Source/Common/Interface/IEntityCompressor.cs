﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>參考型別資料模型壓縮器介面合約
    /// </summary>
    public interface IEntityCompressor
    {
        /// <summary>對目標物件進行二進位壓縮
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <returns>壓縮過後的二進位資料</returns>
        byte[] CompressObject( object target );

        /// <summary>對目標壓縮過後的二進位資料進行解壓縮，還原成資料模型物件
        /// </summary>
        /// <param name="target">壓縮過的二進位資料</param>
        /// <returns>資料模型物件</returns>
        object DecompressObject( byte[] target );

        /// <summary>對目標壓縮過後的二進位資料進行解壓縮，還原成資料模型物件
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="target">壓縮過的二進位資料</param>
        /// <returns>資料模型物件</returns>
        TModel DecompressObject<TModel>( byte[] target )
            where TModel : class;
    }
}
