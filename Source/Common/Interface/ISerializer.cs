namespace ZayniFramework.Common
{
    /// <summary>序列化處理介面
    /// </summary>
    public interface ISerializer
    {
        /// <summary>序列化物件
        /// </summary>
        /// <param name="obj">目標序列化物件</param>
        /// <returns>序列化結果</returns>
        object Serialize( object obj );

        /// <summary>反序列化物件
        /// </summary>
        /// <param name="obj">序列化後的原始資料格式</param>
        /// <typeparam name="TResult">反序列化的物件泛型</typeparam>
        /// <returns>反序列化後的物件</returns>
        TResult Deserialize<TResult>( object obj );
    }
}