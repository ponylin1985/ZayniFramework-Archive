using System;


namespace ZayniFramework.Common
{
    /// <summary>ICommand 命令儲存體
    /// </summary>
    public class CommandHolder
    {
        /// <summary>命令名稱
        /// </summary>
        /// <value></value>
        public string Name { get; set; }

        /// <summary>條件委派
        /// </summary>
        /// <value></value>
        public Func<string, bool> Condition { get; set; }

        /// <summary>命令的型別
        /// </summary>
        /// <value></value>
        public Type TypeOfCommand { get; set; }
    }
}