using System;


namespace ZayniFramework.Common
{
    /// <summary>主控台命令
    /// </summary>
    public abstract class ConsoleCommand : Command
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public ConsoleCommand() 
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="command">原始輸入的命令字串</param>
        public ConsoleCommand( string command )
        {
            CommandText                 = command;
            Parameters[ "CommandText" ] = command;
        }

        /// <summary>解構子
        /// </summary>
        ~ConsoleCommand()
        {
            CommandText                 = null;
            Parameters[ "CommandText" ] = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>原始輸入指令
        /// </summary>
        public string CommandText { get; set; }

        #endregion 宣告公開的屬性


        #region 宣告公開方法

        /// <summary>換行輸出
        /// </summary>
        public new virtual void StdoutLn() => Console.WriteLine();

        /// <summary>輸出錯誤訊息
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        public new virtual void StdoutErr( string message ) => Command.StdoutErr( message );

        /// <summary>輸出訊息
        /// </summary>
        /// <param name="message">訊息內容</param>
        /// <param name="color">訊息字體顏色</param>
        public virtual void Stdout( string message, ConsoleColor color = ConsoleColor.Gray ) => Command.Stdout( message, color );

        #endregion 宣告公開方法
    }
}