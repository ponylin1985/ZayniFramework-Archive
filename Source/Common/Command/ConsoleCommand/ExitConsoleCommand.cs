using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>Exit 主控台指令處理器 (原始指令: exit)
    /// </summary>
    public sealed class ExitConsoleCommand : ConsoleCommand
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters ) 
        {
            Shutdown();
            return Result;
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>終止 Process 處理程序
        /// </summary>
        private void Shutdown()
        {
            Stdout( "Are you sure to exit the application now? (Y/N)" );
            string command = ( Console.ReadLine() + "" ).ToLower()?.Trim();

            if ( !new string[] { "y", "n" }.Contains( command ) )
            {
                Shutdown();
                return;
            }

            if ( "n" == command )
            {
                Stdout( "Share Data Service is still working." );
                return;
            }

            SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 0.5 ) );
            Result.Success = true;
            Process.GetCurrentProcess().Kill();
        }

        #endregion 宣告私有的方法
    }

    /// <summary>Exit 主控台指令處理器 (原始指令: exit)
    /// </summary>
    public sealed class ExitConsoleCommandAsync : ConsoleCommandAsync
    {
        #region 宣告公開的方法

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters ) 
        {
            await ShutdownAsync();
            return Result;
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>終止 Process 處理程序
        /// </summary>
        private async Task ShutdownAsync()
        {
            await StdoutAsync( "Are you sure to exit the application now? (Y/N)" );
            string command = ( await Console.In.ReadLineAsync() + "" ).ToLower()?.Trim();

            if ( !new string[] { "y", "n" }.Contains( command ) )
            {
                await ShutdownAsync();
                return;
            }

            if ( "n" == command )
            {
                await StdoutAsync( "Share Data Service is still working." );
                return;
            }

            await Task.Delay( 500 );
            Result.Success = true;
            Process.GetCurrentProcess().Kill();
        }

        #endregion 宣告私有的方法
    }
}