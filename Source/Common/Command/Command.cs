using NeoSmart.AsyncLock;
using System;
using System.Globalization;


namespace ZayniFramework.Common
{
    /// <summary>抽象命令
    /// </summary>
    public abstract class Command : ICommand
    {
        #region Private Fields

        /// <summary>多執行緒鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>多執行緒鎖定物件，鎖定 _parameter 欄位
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncLockParameters = new AsyncLock();

        /// <summary>多執行緒鎖定物件，鎖定 _result 欄位
        /// </summary>
        /// <returns></returns>
        private readonly object _lockResult = new object();

        /// <summary>命令的參數集合
        /// </summary>
        /// <returns></returns>
        private readonly ParameterCollection _parameters = new ParameterCollection();

        /// <summary>執行結果
        /// </summary>
        /// <returns></returns>
        private IResult _result = Common.Result.Create();

        #endregion Private Fields


        #region Public Properties

        /// <summary>參數集合
        /// </summary>
        public ParameterCollection Parameters { get { using ( _asyncLockParameters.Lock() ) return _parameters; } }

        /// <summary>執行結果
        /// </summary>
        public IResult Result 
        { 
            get { lock ( _lockResult ) return _result; } 
            private set { lock ( _lockResult ) _result = value; }
        }

        #endregion Public Properties


        #region Public Virtual Methods

        /// <summary>執行命令處理
        /// </summary>
        public virtual void Execute() 
        {
            try
            {
                _result = Execute( Parameters );
            }
            catch ( Exception ex )
            {
                StdoutErr( $"Execute command occur exception. {Environment.NewLine}{ex.ToString()}" );
            }
        }

        #endregion Public Virtual Methods


        #region Public Abstract Methods

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public abstract IResult Execute( ParameterCollection parameters );

        #endregion Public Abstract Methods


        #region Public Static Methods

        /// <summary>換行輸出
        /// </summary>
        public static void StdoutLn() => Console.WriteLine();

        /// <summary>輸出錯誤訊息
        /// </summary>
        /// <param name="message">錯誤訊息</param>
        public static void StdoutErr( string message ) => WriteLog( message, ConsoleColor.Red );

        /// <summary>輸出訊息
        /// </summary>
        /// <param name="message">訊息內容</param>
        /// <param name="color">訊息字體顏色</param>
        /// <param name="withTimestamp">是否輸出日誌時間標頭</param>
        public static void Stdout( string message, ConsoleColor color = ConsoleColor.Gray, bool withTimestamp = true ) 
        {
            if ( withTimestamp )
            {
                WriteLog( message, color );
            }
            else
            {
                WriteLine( message, color );
            }
        }

        #endregion Public Static Methods


        #region Private Methods

        /// <summary>主控台日誌訊息輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// 3. 強制輸出訊息時，統一格式，包括輸出的時間，格式為 yyyy-MM-dd HH:mm:ss.fff。<para/>
        /// </summary>
        /// <param name="message">輸出訊息</param>
        /// <param name="color">訊息字體顏色</param>
        private static void WriteLog( string message, ConsoleColor color = ConsoleColor.Gray ) => 
            WriteLine( $"[{DateTime.UtcNow.ToString( "yyyy-MM-dd HH:mm:ss.fff", CultureInfo.InvariantCulture )}] {message}", color );

        /// <summary>主控台訊息輸出，均為對 Console.WriteLine 採用 AsyncWorker 進行非同步的標準輸出。<para/>
        /// 1. 預設輸出訊息會換行。<para/>
        /// 2. 預設字體顏色為灰色，可自行指定訊息的字體顏色。<para/>
        /// </summary>
        /// <param name="message">輸出訊息</param>
        /// <param name="color">訊息字體顏色</param>
        private static void WriteLine( string message, ConsoleColor color = ConsoleColor.Gray )
        {
            try
            {
                if ( message.IsNullOrEmpty() )
                {
                    return;
                }

                using ( _asyncLock.Lock() )
                {
                    ConsoleColor currentColor = Console.ForegroundColor;
                    Console.ForegroundColor   = color;
                    Console.WriteLine( message );
                    Console.ForegroundColor = currentColor;
                }
            }
            catch ( Exception )
            {
                Console.WriteLine( message );
                return;
            }
        }

        #endregion Private Methods
    }
}