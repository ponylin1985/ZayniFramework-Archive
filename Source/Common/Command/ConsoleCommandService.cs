using System;
using System.Threading;
using System.Threading.Tasks;

namespace ZayniFramework.Common
{
    /// <summary>Zayni Framework 框架內建的預設主控台 Console 命令處理服務
    /// </summary>
    public class ConsoleCommandService
    {
        /// <summary>啟動主控台命令處理服務<para/>
        /// (此為 Block 方法，代表呼叫此方法，後續的程式碼如果執行在同一條執行緒會被 Block 住!)
        /// </summary>
        /// <param name="container">命令容器</param>
        public static void StartDefaultConsoleService( CommandContainer container )
        {
            if ( container.IsNull() ) 
            {
                return;
            }

            Command.Stdout( $"Waitting for console command now...", ConsoleColor.Yellow );

            while ( true )
            {
                string commandText = null;

                try
                {
                    commandText = Console.ReadLine();

                    if ( commandText.IsNullOrEmpty() )
                    {
                        SpinWait.SpinUntil( () => false, 2 );
                        continue;
                    }

                    var command = container.Get<ConsoleCommand>( commandText );

                    if ( command.IsNull() )
                    {
                        SpinWait.SpinUntil( () => false, 2 );
                        continue;
                    }

                    command.Parameters[ "CommandText" ] = commandText;
                    command.CommandText = commandText;
                    command.Execute();    
                }
                catch ( Exception ex )
                {
                    Command.StdoutErr( $"Process console command occur exception. Console command: {commandText}. {Environment.NewLine}{ex.ToString()}" );
                    SpinWait.SpinUntil( () => false, 2 );
                    continue;
                }

                SpinWait.SpinUntil( () => false, 2 );
            }
        }

        /// <summary>啟動主控台命令處理服務<para/>
        /// (此為 Block 方法，代表呼叫此方法，後續的程式碼如果執行在同一條執行緒會被 Block 住!)
        /// </summary>
        /// <param name="container">命令容器</param>
        public static async Task StartDefaultConsoleServiceAsync( CommandContainer container )
        {
            if ( container.IsNull() ) 
            {
                return;
            }

            await CommandAsync.StdoutAsync( $"Waitting for console command now...", ConsoleColor.Yellow );

            while ( true )
            {
                string commandText = null;

                try
                {
                    commandText = await Console.In.ReadLineAsync();

                    if ( commandText.IsNullOrEmpty() )
                    {
                        await Task.Delay( 2 );
                        continue;
                    }

                    var command = await container.GetAsync<ConsoleCommandAsync>( commandText );

                    if ( command.IsNull() )
                    {
                        await Task.Delay( 2 );
                        continue;
                    }

                    command.Parameters[ "CommandText" ] = commandText;
                    command.CommandText = commandText;
                    await command.ExecuteAsync();
                }
                catch ( Exception ex )
                {
                    await CommandAsync.StdoutErrAsync( $"Process console command occur exception. Console command: {commandText}. {Environment.NewLine}{ex.ToString()}" );
                    await Task.Delay( 2 );
                    continue;
                }

                await Task.Delay( 2 );
            }
        }
    }
}