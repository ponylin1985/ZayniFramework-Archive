namespace ZayniFramework.Common
{
    /// <summary>物件轉換器
    /// </summary>
    public static class ObjectConverter
    {
        /// <summary>根據指定的泛型型別，將物件轉換成指定的型別
        /// </summary>
        /// <param name="source">來源物件</param>
        /// <typeparam name="T">目標物件的泛型</typeparam>
        /// <returns>指定的型別的物件</returns>
        public static T ConvertTo<T>( object source ) where T : new() => source.ConvertTo<T>();
    }
}