﻿using System;


namespace ZayniFramework.Common.ORM
{
    /// <summary>資料庫預存程序參數 ORM 對應中介資料
    /// </summary>
    [AttributeUsage( AttributeTargets.Property | AttributeTargets.Field )]
    public class ProcedureParameterAttribute : Attribute
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public ProcedureParameterAttribute()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="name">參數名稱</param>
        /// <param name="dbType">資料庫型別</param>
        /// <param name="isOutput">是否為輸出參數</param>
        public ProcedureParameterAttribute( string name, int dbType, bool isOutput )
        {
            Name     = name;
            DbType   = dbType;
            IsOutput = isOutput;
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="name">參數名稱</param>
        /// <param name="dbType">資料庫型別</param>
        public ProcedureParameterAttribute( string name, int dbType ) : this( name, dbType, false )
        {
            // pass
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>參數名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>資料庫型別 (DbTypeCode)
        /// </summary>
        public int DbType { get; set; }

        /// <summary>是否為輸出參數
        /// </summary>
        public bool IsOutput { get; set; }

        /// <summary>輸出參數的最大值
        /// </summary>
        public int MaxSize { get; set; }

        #endregion 宣告公開的屬性
    }
}
