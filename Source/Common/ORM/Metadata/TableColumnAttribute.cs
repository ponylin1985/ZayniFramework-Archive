﻿using System;


namespace ZayniFramework.Common.ORM
{
    /// <summary>資料表欄位對應 ORM 中介資料
    /// </summary>
    [AttributeUsage( AttributeTargets.Property | AttributeTargets.Field )]
    public class TableColumnAttribute : Attribute
    {
        /// <summary>是否為資料表主索引鍵
        /// </summary>
        public bool IsPrimaryKey { get; set; }

        /// <summary>屬性值是否允許為 Null 值，預設為false!<para/>
        /// 1. 當查詢出來的結果為 DBNull 時，而且屬性不是「Struct Nullable」型別時，會自動設定 DefaultValue 的屬性值當作預設值!<para/>
        /// </summary>
        public bool IsAllowNull { get; set; }

        /// <summary>此資料表欄位是否為「資料時間戳記」。一個資料表中，只應該有一個欄位為「資料時間戳記」。
        /// </summary>
        public bool IsTimeStamp { get; set; }

        /// <summary>屬性值的預設值，或是 Insert 寫入到資料庫中的預設值。<para/>
        /// 1. 當查詢出來的結果，如果屬性為「非 Struct Nullable」型別時，而且資料庫取得的資料值為 DBNull 時，會自動設定此預設值到此屬性!<para/>
        /// 框架內部預設為 .NET Framework 的 CLR 在各種型別在執行時期的預設值，譬如: default ( Int32 )、default ( DateTime )、defalut ( Decimal ) 或 Null 等等。<para/>
        /// 2. 當對資料庫進行 Insert 指令時，如果檢查到 Property 的資料值為 Null 時，預設寫入到資料庫中的預設值，如果沒有設定，則以 DBNull 值寫入到資料庫。<para/>
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>對應 DataTable 或資料表中的欄位名稱<para/>
        /// 1. 此屬性值必須設定的與資料庫中的 Table 欄位名稱一模一樣。<para/>
        /// 2. 自行在程式中撰寫的 SQL 查詢語法 SELECT 出來的欄位名稱一模一樣。<para/>
        /// 3. 自行在程式中撰寫的 SQL 查詢語法 SELECT 出來的欄位 AS 別名一模一樣。<para/>
        /// 4. 有區分大小寫!!!<para/>
        /// </summary>
        public string ColumnName { get; set; } = string.Empty;

        /// <summary>是否需要根據此欄位進行 SQL Order By 排序 (預設為遞增排序)
        /// </summary>
        public bool OrderBy { get; set; }

        /// <summary>是否根據欄位進行遞減排序 (OrderBy 屬性必須先設定為 true)
        /// </summary>
        public bool OrderByDesc { get; set; }

        /// <summary>SQL查詢條件運算子 (合法值為: = 或 LIKE)
        /// </summary>
        public string SqlOperator { get; set; } = "=";
    }
}
