﻿using System;


namespace ZayniFramework.Common.ORM
{
    /// <summary>資料表對應 ORM 中介資料
    /// </summary>
    [AttributeUsage( AttributeTargets.Class )]
    public class MappingTableAttribute : Attribute
    {
        /// <summary>資料表名稱。<para/>
        /// 1. 此屬性值必須與資料庫中的資料表名稱一模一樣。<para/>
        /// 2. 有區分大小寫。<para/>
        /// </summary>
        public string TableName { get; set; }
    }
}
