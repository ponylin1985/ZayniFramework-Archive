﻿using System;


namespace ZayniFramework.Common.ORM
{
    /// <summary>ORM 對應資料模型的中介資料
    /// </summary>
    [AttributeUsage( AttributeTargets.Property | AttributeTargets.Field )]
    public class OrmDataAttribute : Attribute
    {
        /// <summary>預設建構子
        /// </summary>
        public OrmDataAttribute()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="index"></param>
        public OrmDataAttribute( int index )
        {
            Index = index;
        }

        /// <summary>ORM對應索引 (相對應物件陣列的索引，Zero-base)
        /// </summary>
        public int Index { get; set; }
    }
}
