using System;
using System.Linq;
using System.Text;


namespace ZayniFramework.Common
{
    /// <summary>資料庫連線字串遮罩處理器
    /// </summary>
    public static class ConnectionStringMask
    {
        /// <summary>對傳入的資料庫連線字串進行敏感性資料遮罩。<para/>
        /// * 目前只針對資料庫帳號和密碼 (Uid、User Id、Pwd 和 Password) 進行遮罩。<para/>
        /// * 會忽略大小寫進行比對與遮罩。<para/>
        /// * 遮罩處理過程，假若有任何程式異常，則直接回傳空字串，永遠不會拋出 exception 程式異常物件。
        /// </summary>
        /// <param name="connString">資料庫連線字串</param>
        /// <returns>遮罩過後的資料庫連線字串</returns>
        public static string Mask( string connString ) 
        {
            try
            {
                if ( connString.IsNullOrEmpty() )
                {
                    return string.Empty;
                }

                var tokens = connString
                                .Split( ';' )?
                                .Select( v => v.Trim() )?
                                .Where( n => n.IsNotNullOrEmpty() )
                                .ToArray();

                var u = Array.FindIndex( tokens, t => t.StartsLike( "Uid" ) || t.StartsLike( "User Id" ) );
                var p = Array.FindIndex( tokens, t => t.StartsLike( "Pwd" ) || t.StartsLike( "Password" ) );

                var userPhrase = tokens[ u ];
                tokens[ u ] = $"{userPhrase.Split( '=' )?.FirstOrDefault()?.Trim()}=*****";

                var pwdPhrase = tokens[ p ];
                tokens[ p ] = $"{pwdPhrase.Split( '=' )?.FirstOrDefault()?.Trim()}=*****";

                var sbConnString   = new StringBuilder();
                var maskConnString = "";

                foreach ( var token in tokens )
                {
                    sbConnString.Append( $"{token};" );
                }

                maskConnString = sbConnString.ToString();
                return maskConnString;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}