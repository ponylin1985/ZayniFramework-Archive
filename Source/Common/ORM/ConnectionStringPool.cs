using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;


namespace ZayniFramework.Common
{
    /// <summary>資料庫連線字串常數池
    /// </summary>
    internal static class ConnectionStringPool
    {
        #region Private Fields

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>資料庫連線字串常數集合<para/>
        /// * Key: 資料庫連線設定名稱。<para/>
        /// * Value: 資料庫連線字串值。<para/>
        /// </summary>
        private static readonly Dictionary<string, string> _connectionStrings = new Dictionary<string, string>();

        #endregion Private Fields


        #region Internal Static Methods

        /// <summary>取得資料庫連線字串
        /// </summary>
        /// <param name="dbName">資料庫連線字串的名稱</param>
        /// <returns>取得資料庫連線字串結果</returns>
        internal static Result<string> Get( string dbName )
        {
            var result = Result.Create<string>();
            string connectionString = null;

            try
            {
                using ( _asyncLock.Lock() )
                {
                    if ( !_connectionStrings.ContainsKey( dbName ) )
                    {
                        result.Message = $"Config error. Can not found '{dbName}' db connection string from config file. DbName: {dbName}.";
                        return result;
                    }

                    connectionString = _connectionStrings[ dbName ];
                }
            }
            catch ( Exception ex )
            {
                result.HasException    = true;
                result.ExceptionObject = ex;
                result.Message         = $"Get db connection string from connection string pool occur exception. DbName: {dbName}. {ex.ToString()}";
                return result;
            }

            if ( connectionString.IsNullOrEmpty() )
            {
                result.Message = $"Retrive null or empty connection string from connection string pool. DbName: {dbName}.";
                return result;
            }

            result.Data    = connectionString;
            result.Success = true;
            return result;
        }

        /// <summary>加入資料庫連線字串
        /// </summary>
        /// <param name="dbName">資料庫連線字串的名稱</param>
        /// <param name="connectionString">資料庫連線字串</param>
        /// <returns>加入資料庫連線字串結果</returns>
        internal static Result Add( string dbName, string connectionString )
        {
            var result = Result.Create();

            try
            {
                using ( _asyncLock.Lock() )
                {
                    if ( _connectionStrings.ContainsKey( dbName ) )
                    {
                        result.Message = $"Can not add db connection string into connection string pool due to duplicated DbName. DbName: {dbName}.";
                        return result;
                    }

                    _connectionStrings.Add( dbName, connectionString );
                }
            }
            catch ( Exception ex )
            {
                result.HasException    = true;
                result.ExceptionObject = ex;
                result.Message         = $"Add db connection string into connection string pool occur exception. DbName: {dbName}. {ex.ToString()}";
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion Internal Static Methods
    }
}