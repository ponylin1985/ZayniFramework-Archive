﻿using System;
using System.Collections.Generic;
using System.Data;


namespace ZayniFramework.Common
{
    #region 宣告 ZayniFramework 框架公開的委派

    /// <summary>遞迴中止條件委派
    /// </summary>
    /// <param name="args">參數</param>
    /// <returns>是否可以中止遞迴</returns>
    public delegate bool BreakConitionHandler( params dynamic[] args );

    /// <summary>嘗試將字串轉型的委派
    /// </summary>
    /// <typeparam name="TResult">轉型的輸出泛型</typeparam>
    /// <param name="value">來源字串物件</param>
    /// <param name="output">轉型後輸出的物件</param>
    /// <returns>轉型是否成功</returns>
    public delegate bool TryParseHandler<TResult>( string value, out TResult output );

    /// <summary>ZayniFramework 框架的例外處理委派
    /// </summary>
    /// <param name="exception">例外物件</param>
    /// <param name="sender">處發例外事件的物件</param>
    /// <param name="message">事件訊息</param>
    public delegate void ExceptionHandler( Exception exception, object sender = null, string message = "" );

    /// <summary>ZayniFramework 框架的執行目的動作前的處理委派
    /// </summary>
    /// <param name="args">參數</param>
    public delegate void BeforeExecuteHandler( params object[] args );

    /// <summary>ZayniFramework 框架的執行動作成功的委派
    /// </summary>
    /// <param name="args">參數</param>
    public delegate void SuccessHandler( params object[] args );

    /// <summary>ZayniFramework 框架的執行動作失敗的委派
    /// </summary>
    /// <param name="args">參數</param>
    public delegate void FailureHandler( params object[] args );

    #endregion 宣告 ZayniFramework 框架公開的委派


    #region 宣告 ZayniFramework.DataAccess ORM 框架公開的委派

    /// <summary>ZayniFramework.DataAccess 框架執行 ORM 資料繫結失敗回呼處理委派
    /// </summary>
    /// <param name="ds">DataSet資料集合</param>
    /// <param name="message">失敗錯誤訊息</param>
    public delegate void DataSetOrmBindingFailureHandler( DataSet ds, string message );

    /// <summary>ZayniFramework.DataAccess 框架執行 ORM 資料繫結失敗回呼處理委派
    /// </summary>
    /// <param name="reader">資料讀取器</param>
    /// <param name="message">失敗錯誤訊息</param>
    public delegate void DataReaderOrmBindingFailureHanlder( IDataReader reader, string message );

    /// <summary>ZayniFramework.DataAccess 框架執行 ORM 資料繫結失敗回呼處理委派
    /// </summary>
    /// <param name="datas">資料集合</param>
    /// <param name="message">失敗錯誤訊息</param>
    public delegate void ModelSetOrmBindingFailureHanlder( List<object[]> datas, string message );

    #endregion 宣告ZayniFramework.DataAccess /ORM框架公開的委派
}
