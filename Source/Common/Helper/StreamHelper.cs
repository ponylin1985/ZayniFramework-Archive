﻿using System;
using System.IO;


namespace ZayniFramework.Common
{
    /// <summary>串流處理類別
    /// </summary>
    public static class StreamHelper
    {
        #region 宣告公開的靜態方法

        /// <summary>從來源的串流轉換成二進位位元陣列
        /// </summary>
        /// <param name="source">來源串流</param>
        /// <returns>二進位位元陣列</returns>
        public static byte[] ReadBytes( Stream source )
        {
            var ms = source as MemoryStream;

            if ( ms.IsNotNull() )
            {
                return ms.ToArray();
            }

            return ConvertToBytes( source );
        }

        /// <summary>從來源串流轉換成 Base64 編碼的字串
        /// </summary>
        /// <param name="source">來源串流</param>
        /// <returns>Base64 編碼的字串</returns>
        public static string ConvertToBase64String( Stream source ) 
        {
            var ms = new MemoryStream();
            source.CopyTo( ms );
            var bytes  = ms.ToArray();
            var base64 = Convert.ToBase64String( bytes );
            return base64;
        }

        /// <summary>從來源的串流轉換成二進位位元陣列
        /// </summary>
        /// <param name="source">來源串流</param>
        /// <returns>二進位位元陣列</returns>
        public static byte[] ConvertToBytes( Stream source )
        {
            byte[] buffer = new byte[ 16 * 1024 ];

            using ( var ms = new MemoryStream() )
            {
                int read;

                while ( ( read = source.Read( buffer, 0, buffer.Length ) ) > 0 )
                {
                    ms.Write( buffer, 0, read );
                }
                
                return ms.ToArray();
            }
        }

        #endregion 宣告公開的靜態方法
    }
}
