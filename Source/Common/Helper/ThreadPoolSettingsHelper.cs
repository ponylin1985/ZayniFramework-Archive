using System;
using System.Threading;


namespace ZayniFramework.Common
{
    /// <summary>.NET CLR 執行時期 Thread Pool 公用組態 Helper 
    /// </summary>
    public static class ThreadPoolSettingsHelper
    {
        /// <summary>依照 ZayniFramework config 設定檔中的 ThreadPoolSetting 組態，設定 .NET CLR 執行時期 Thread Pool 執行緒數量。
        /// </summary>
        public static IResult<( int minWorkerThreadsNumber, int minIOCPThreadsNumber, int maxWorkerThreadsNumber, int maxIOCPThreadsNumber )> ApplyConfig() 
        {
            var result = Result.Create<( int minWorkerThreadsNumber, int minIOCPThreadsNumber, int maxWorkerThreadsNumber, int maxIOCPThreadsNumber )>();

            try
            {
                var config = ConfigManagement.ZayniConfigs.ThreadPoolSettings;
                return SetThreadPool( config.MinWorkerThreads, config.MinIOCPThreads, config.MaxWorkerThreads, config.MaxIOCPThreads )    ;
            }
            catch ( Exception ex )
            {
                Command.StdoutErr( $"Apply the ThreadPoolSettings config to .NET thread pool occur exception. {ex.ToString()}" );
                return Result.Create<( int minWorkerThreadsNumber, int minIOCPThreadsNumber, int maxWorkerThreadsNumber, int maxIOCPThreadsNumber )>( false, 
                    data: ( minWorkerThreadsNumber: -1, minIOCPThreadsNumber: -1, maxWorkerThreadsNumber: -1, maxIOCPThreadsNumber: -1 ),
                    message: $"Apply the ThreadPoolSettings config to .NET thread pool occur exception. {ex.ToString()}" );
            }
        }

        /// <summary>設定 .NET CLR 執行時期 Thread Pool 執行緒數量
        /// </summary>
        /// <param name="minWorkerThreads">最小 Worker 執行緒數量</param>
        /// <param name="minIOCPThreads">最小 IOCP 執行緒數量</param>
        /// <param name="maxWorkerThreads">最大 Worker 執行緒數量</param>
        /// <param name="maxIOCPThreads">最大 IOCP 執行緒數量</param>
        public static IResult<( int minWorkerThreadsNumber, int minIOCPThreadsNumber, int maxWorkerThreadsNumber, int maxIOCPThreadsNumber )> SetThreadPool( 
            int minWorkerThreads, int minIOCPThreads, int maxWorkerThreads = 0, int maxIOCPThreads = 0 ) 
        {
            var result = Result.Create<( int minWorkerThreadsNumber, int minIOCPThreadsNumber, int maxWorkerThreadsNumber, int maxIOCPThreadsNumber )>();

            int minWorkerThreadsNumber = -1;
            int minIOCPThreadsNumber   = -1;
            int maxWorkerThreadsNumber = -1;
            int maxIOCPThreadsNumber   = -1;

            try
            {
                ThreadPool.GetMinThreads( out int minWorker, out int minIOC );
                Command.Stdout( $"Default .NET thread pool setting is --> minWorkerThread: {minWorker}, minIOPCThread: {minIOC}.", ConsoleColor.Yellow );

                if ( minWorkerThreads > 0 && minIOCPThreads > 0 )
                {
                    if ( ThreadPool.SetMinThreads( minWorkerThreads, minIOCPThreads ) )
                    {
                        minWorkerThreadsNumber = minWorkerThreads;
                        minIOCPThreadsNumber  = minIOCPThreads;
                        Command.Stdout( $"Set min numbers threads to thread pool successfully. Set minWorkerThreads: {minWorkerThreads}, minIOPCThreads: {minIOCPThreads}.", ConsoleColor.Yellow );
                    }
                    else
                    {
                        Command.StdoutErr( $"Set min numbers threads to thread pool fail." );
                    }
                }

                if ( maxWorkerThreads > 0 && maxIOCPThreads > 0 )
                {
                    if ( ThreadPool.SetMaxThreads( maxWorkerThreads, maxIOCPThreads ) )
                    {
                        maxWorkerThreadsNumber = maxWorkerThreads;
                        maxIOCPThreadsNumber   = maxIOCPThreads;
                        Command.Stdout( $"Set max numbers threads to thread pool successfully. Set maxWorkerThreads: {maxWorkerThreads}, maxIOPCThreads: {maxIOCPThreads}.", ConsoleColor.Yellow );
                    }
                    else
                    {
                        Command.StdoutErr( $"Set max numbers threads to thread pool fail." );
                    }
                }

                ThreadPool.GetMinThreads( out minWorkerThreadsNumber, out minIOCPThreadsNumber );
                ThreadPool.GetMaxThreads( out maxWorkerThreadsNumber, out maxIOCPThreadsNumber );
            }
            catch ( Exception ex )
            {
                Command.StdoutErr( $"Set thread number to .NET thread pool occur exception. {ex.ToString()}" );
                return Result.Create<( int minWorkerThreadsNumber, int minIOCPThreadsNumber, int maxWorkerThreadsNumber, int maxIOCPThreadsNumber )>( false, 
                    data: ( minWorkerThreadsNumber: minWorkerThreadsNumber, minIOCPThreadsNumber: minIOCPThreadsNumber, maxWorkerThreadsNumber: maxWorkerThreadsNumber, maxIOCPThreadsNumber: maxIOCPThreadsNumber ),
                    message: $"Set thread number to .NET thread pool occur exception. {ex.ToString()}" );
            }

            return Result.Create<( int minWorkerThreadsNumber, int minIOCPThreadsNumber, int maxWorkerThreadsNumber, int maxIOCPThreadsNumber )>( true, 
                data: ( minWorkerThreadsNumber: minWorkerThreadsNumber, minIOCPThreadsNumber: minIOCPThreadsNumber, maxWorkerThreadsNumber: maxWorkerThreadsNumber, maxIOCPThreadsNumber: maxIOCPThreadsNumber ) );
        }
    }
}