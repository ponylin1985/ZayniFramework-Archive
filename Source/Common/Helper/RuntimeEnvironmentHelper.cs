using System.Runtime.InteropServices;


namespace ZayniFramework.Common
{
    /// <summary>執行階段作業系統環境 Helper 類別
    /// </summary>
    public static class RuntimeEnvironmentHelper
    {
        /// <summary>檢查執行環境是否為 Windows 作業系統
        /// </summary>
        /// <returns>執行環境是否為 Windows 作業系統</returns>
        public static bool IsWindowsOS() 
        {
            return RuntimeInformation.IsOSPlatform( OSPlatform.Windows );

            #region 另一種判斷方式 (暫時註解掉)

            // var os = Environment.OSVersion.Platform;

            // switch ( os )
            // {
            //     case PlatformID.Win32S:
            //     case PlatformID.Win32Windows:
            //     case PlatformID.Win32NT:
            //     case PlatformID.WinCE:
            //         return true;
                
            //     default:
            //         return false;
            // }

            #endregion 另一種判斷方式 (暫時註解掉)
        }

        /// <summary>檢查執行環境是否為 Unix-Like (譬如: Linux 或 macOS) 的作業系統
        /// </summary>
        /// <returns>執行環境是否為 Unix-Like (譬如: Linux 或 macOS) 的作業系統</returns>
        public static bool IsUnixLikeOS() 
        {
            return RuntimeInformation.IsOSPlatform( OSPlatform.Linux ) || RuntimeInformation.IsOSPlatform( OSPlatform.OSX );

            #region 另一種判斷方式 (暫時註解掉)

            // var os = Environment.OSVersion.Platform;

            // switch ( os )
            // {
            //     case PlatformID.Unix:
            //     case PlatformID.MacOSX:
            //         return true;
                
            //     default:
            //         return false;
            // }

            #endregion 另一種判斷方式 (暫時註解掉)
        }
    }
}