﻿using System;


namespace ZayniFramework.Common
{
    /// <summary>真假值條件檢查類別
    /// </summary>
    public static class Check
    {
        /// <summary>當為真值時，自動執行指定的回呼委派。呼叫範例:<para/>
        /// int  j = 9;<para/>
        /// bool b = 9 == j;<para/>
        /// Check.IsTrue( b, () => Console.WriteLine( "This is TRUE." ) );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// int  j = 9;
        /// bool b = 9 == j;
        /// Check.IsTrue( b, () => Console.WriteLine( "This is TRUE." ) );
        /// </code>
        /// </example>
        /// <param name="isTrue">布林真假值</param>
        /// <param name="action">為真時執行的回呼</param>
        public static void IsTrue( bool isTrue, Action action ) => IsTrue( () => isTrue, action );

        /// <summary>當條件為真時，自動執行指定的回呼委派。呼叫範例:<para/>
        /// int j = 9;<para/>
        /// Check.IsTrue( () => 9 == j, () => Console.WriteLine( $"I am {j}." ) );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// int j = 9;
        /// Check.IsTrue( () => 9 == j, () => Console.WriteLine( $"I am {j}." ) );
        /// </code>
        /// </example>
        /// <param name="condition">真假值條件</param>
        /// <param name="action">條件為真時執行的回呼</param>
        public static void IsTrue( Func<bool> condition, Action action )
        {
            if ( condition() )
            {
                action();
            }
        }

        /// <summary>當為假值時，自動執行指定的回呼委派。呼叫範例:<para/>
        /// int  j = 9;<para/>
        /// bool b = 123 = j;<para/>
        /// Check.IsFalse( b, () => Console.WriteLine( "j is 9." ) );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// int  j = 9;
        /// bool b = 123 = j;
        /// Check.IsFalse( b, () => Console.WriteLine( "j is 9." ) );
        /// </code>
        /// </example>
        /// <param name="isFalse">布林真假值</param>
        /// <param name="action">為假時執行的回呼</param>
        public static void IsFalse( bool isFalse, Action action ) => IsFalse( () => isFalse, action );

        /// <summary>當條件為假時，自動執行指定的回呼委派。呼叫範例:<para/>
        /// int j = 9;<para/>
        /// Check.IsFalse( () => 123 == j, () => {<para/>
        ///     j = 9;<para/>
        ///     Console.WriteLine( $"Hey hey, I am still {j}." );<para/>
        /// } );<para/>
        /// </summary>
        /// <example>
        /// <code>
        /// int j = 9;
        /// Check.IsFalse( () => 123 == j, () => {
        ///     j = 9;
        ///     Console.WriteLine( $"Hey hey, I am still {j}." );
        /// } );
        /// </code>
        /// </example>
        /// <param name="condition">真假值條件</param>
        /// <param name="action">條件為假時執行的回呼</param>
        public static void IsFalse( Func<bool> condition, Action action )
        {
            if ( !condition() )
            {
                action();
            }
        }
    }
}
