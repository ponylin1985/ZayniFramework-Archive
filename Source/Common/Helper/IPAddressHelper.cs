﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;


namespace ZayniFramework.Common
{
    /// <summary>IP 位址的 Helper 公用類別
    /// </summary>
    public static class IPAddressHelper
    {
        // Pony Says: 這個方式，在 .NET Core 執行在 macOS 上居然會抓不到 IP address，還會爆 exception... what the fuck...
        /// <summary>取得本地端在區域網路的 Local IP 位址
        /// </summary>
        /// <returns>本地端在區域網路的 Local IP 位址</returns>
        public static string GetLocalIPAddress()
        {
            try
            {
                if ( RuntimeInformation.IsOSPlatform( OSPlatform.OSX ) )
                {
                    return "127.0.0.1";
                }

                // macOS issue:
                // https://github.com/dotnet/corefx/issues/13309
                // Fuck you macOS... what the fuck... 
                string hostName  = Dns.GetHostName();
                string ipAddress = Dns.GetHostEntry( hostName )?.AddressList?.Where( m => AddressFamily.InterNetwork == m.AddressFamily )?.FirstOrDefault()?.ToString();
                return ipAddress.IsNullOrEmptyString( string.Empty );
            }
            catch ( Exception )
            {
                return "";
            }
        }

        /// <summary>取得本地端在公有網際網路的 IP 位址，假若回傳 127.0.0.1，代表嘗試取得網際網路的 IP 位址失敗。
        /// </summary>
        /// <returns>地端在公有網際網路的 IP 位址，假若回傳 127.0.0.1，代表嘗試取得網際網路的 IP 位址失敗。</returns>
        public static string GetExternalIPAddress()
        {
            string publicIPAddress;

            try
            {
                var request       = (HttpWebRequest)WebRequest.Create( "http://ifconfig.me" );
                request.Method    = "GET";
                request.UserAgent = "curl";

                using ( WebResponse response = request.GetResponse() )
                using ( var reader = new StreamReader( response.GetResponseStream() ) )
                {
                    publicIPAddress = reader.ReadToEnd();
                }

                publicIPAddress = publicIPAddress.Replace( "\n", string.Empty );
            }
            catch
            {
                return "127.0.0.1";
            }

            return publicIPAddress.IsNullOrEmptyString( "127.0.0.1" );
        }
    }
}
