﻿using System;
using System.Collections;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>對集合的所有元素執行動作
    /// </summary>
    public sealed class ForEach
    {
        #region 宣告私有的欄位

        /// <summary>目標陣列
        /// </summary>
        private Array _array;

        /// <summary>目標串列
        /// </summary>
        private IList _list;
    
        /// <summary>計數
        /// </summary>
        private int _i = 0;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="array">目標陣列</param>
        public ForEach( Array array ) 
        {
            _array = array;
        }

        /// <summary>目標串列
        /// </summary>
        /// <param name="list">目標串列</param>
        public ForEach( IList list )
        {
            _list = list;
        }

        #endregion 宣告建構子

        
        #region 宣告公開的屬性

        /// <summary>目前執行次數
        /// </summary>
        public int Count => _i;

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>執行動作
        /// </summary>
        /// <param name="callback">動作委派</param>
        public void Do( Action callback ) 
        {
            int count = 0;

            _array.IsNotNull( a => count = _array.Length );
            _list.IsNotNull(  y => count = _list.Count );

            if ( 0 == count )
            {
                return;
            }

            if ( _i >= count ) 
            {
                return;
            }
        
            try 
            {
                callback();
            }	
            catch ( Exception ex ) 
            {
                throw ex;
            }
                
            _i++;
            Do( callback );
        }

        /// <summary>以非同步執行動作
        /// </summary>
        /// <param name="callback">動作委派</param>
        /// <returns></returns>
        public async Task DoAsync( Action callback )
        {
            int count = 0;

            _array.IsNotNull( a => count = _array.Length );
            _list.IsNotNull(  y => count = _list.Count );

            if ( 0 == count )
            {
                return;
            }

            if ( _i >= count ) 
            {
                return;
            }
        
            await Task.Factory.StartNew( () => 
            {
                try 
                {
                    callback();
                }	
                catch ( Exception ex ) 
                {
                    throw ex;
                }
            } );
                
            _i++;
            Do( callback );
        }

        #endregion 宣告公開的方法
    }
}
