﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>重覆執行類別
    /// </summary>
    public sealed class For
    {
        #region 宣告私有的欄位

        /// <summary>計數
        /// </summary>
        private int _i = 0;

        #endregion 宣告私有的欄位


        #region 宣告公開的屬性

        /// <summary>目前執行次數
        /// </summary>
        public int Count => _i;

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>重置計數
        /// </summary>
        public void Reset() => _i = 0;

        /// <summary>重覆執行動作
        /// </summary>
        /// <param name="times">執行次數</param>
        /// <param name="callback">動作委派</param>
        public void Do( int times, Action callback ) 
        {
            if ( _i >= times ) 
            {
                return;
            }
        
            try 
            {
                callback();
            }	
            catch ( Exception ex ) 
            {
                throw ex;
            }
                
            _i++;
            Do( times, callback );
        }

        /// <summary>以非同步重覆執行動作
        /// </summary>
        /// <param name="times">執行次數</param>
        /// <param name="callback">動作委派</param>
        /// <returns></returns>
        public async Task DoAsync( int times, Action callback )
        {
            if ( _i >= times ) 
            {
                return;
            }
        
            await Task.Factory.StartNew( () => 
            {
                try 
                {
                    callback();
                }	
                catch ( Exception ex ) 
                {
                    throw ex;
                }
            } );
                
            _i++;
            Do( times, callback );
        }

        #endregion 宣告公開的方法
    }
}
