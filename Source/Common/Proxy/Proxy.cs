using System;


namespace ZayniFramework.Common
{
    /// <summary>動作代理人，代理呼叫沒有回傳值的動作
    /// </summary>
    public class Proxy 
    {
        #region Public Properties

        /// <summary>目標動作委派
        /// </summary>
        /// <value></value>
        public Delegate Action { get; set; }

        /// <summary>呼叫前的處理委派
        /// </summary>
        /// <value></value>
        public Func<object[], object[]> BeforeInvokeHandler { get; set; }

        #endregion Public Properties


        #region Public Static Methdos

        /// <summary>建立代理人物件
        /// </summary>
        /// <returns>代理人物件</returns>
        public static Proxy Create() => new Proxy();

        /// <summary>建立代理人物件
        /// </summary>
        /// <param name="action">目標動作委派</param>
        /// <param name="beforeInvoke">呼叫前的處理委派</param>
        /// <returns>代理人物件</returns>
        public static Proxy Create( Delegate action, Func<object[], object[]> beforeInvoke = null ) => 
            new Proxy() { Action = action, BeforeInvokeHandler = beforeInvoke };

        /// <summary>建立代理人物件
        /// </summary>
        /// <typeparam name="TReturn">目標動作回傳值的泛型</typeparam>
        /// <returns>代理人物件</returns>
        public static Proxy<TReturn> Create<TReturn>() => new Proxy<TReturn>();

        /// <summary>建立代理人物件
        /// </summary>
        /// <param name="action">目標動作委派</param>
        /// <param name="beforeInvoke">呼叫前的處理委派</param>
        /// <param name="afterInvoke">呼叫後的處理委派</param>
        /// <typeparam name="TReturn">目標動作回傳值的泛型</typeparam>
        /// <returns>代理人物件</returns>
        public static Proxy<TReturn> Create<TReturn>( Delegate action, Func<object[], object[]> beforeInvoke = null, Func<object[], TReturn, TReturn> afterInvoke = null ) => 
            new Proxy<TReturn>() { Action = action, BeforeInvokeHandler = beforeInvoke, AfterInvokeHandler = afterInvoke };

        #endregion Public Static Methdos


        #region Public Methods

        /// <summary>呼叫執行目標動作
        /// </summary>
        /// <param name="parameters">目標動作委派的執行時期參數</param>
        /// <returns>目標動作的回傳值</returns>
        public void Invoke( params object[] parameters ) 
        {
            BeforeInvokeHandler.IsNotNull( b => parameters = BeforeInvokeHandler.Invoke( parameters ) );
            DelegateInvoker.Exec( Action, parameters );
        }

        #endregion Public Methods
    }

    /// <summary>動作代理人，代理呼叫有回傳值的動作
    /// </summary>
    /// <typeparam name="TResult">目標動作回傳值的泛型</typeparam>
    public class Proxy<TResult> : Proxy
    {
        #region Public Properties

        /// <summary>呼叫後的處理委派
        /// </summary>
        /// <value></value>
        public Func<object[], TResult, TResult> AfterInvokeHandler { get; set; }

        #endregion Public Properties


        #region Public Methdos

        /// <summary>呼叫執行目標動作
        /// </summary>
        /// <param name="parameters">目標動作委派的執行時期參數</param>
        /// <returns>目標動作的回傳值</returns>
        public new TResult Invoke( params object[] parameters ) 
        {
            BeforeInvokeHandler.IsNotNull( b => parameters = BeforeInvokeHandler.Invoke( parameters ) );
            var result = DelegateInvoker.ExecResult<TResult>( Action, parameters );
            AfterInvokeHandler.IsNotNull( a => result = AfterInvokeHandler.Invoke( parameters, result ) );
            return result;
        }

        #endregion Public Methdos
    }
}