using System;
using System.Collections.Concurrent;
using System.Threading;


namespace ZayniFramework.Common.Tasks
{
    /// <summary>非同步工作佇列處理器
    /// </summary>
    public class AsyncWorker
    {
        #region Private Fields

        /// <summary>Enable 的多執行緒鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly object _lockThiz = new object();

        /// <summary>Enqueue 的多執行緒鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly object _lockEnqueue = new object();

        /// <summary>Dequeue 的多執行緒鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly object _lockDequeue = new object();

        /// <summary>工作佇列集合
        /// </summary>
        /// <returns></returns>
        private readonly BlockingCollection<Action> _queue = new BlockingCollection<Action>();

        /// <summary>工作處理器執行緒
        /// </summary>
        /// <returns></returns>
        private Thread _consumer;

        /// <summary>是否持續處理工作
        /// </summary>
        private bool _enable = true;

        /// <summary>Runner 執行時間間隔，單位為 ms 毫秒數，不可小於或等於 0。
        /// </summary>
        private int _interval = 3;

        /// <summary>工作處理器 Worker 名稱
        /// </summary>
        private string _name;

        #endregion Private Fields


        #region Constructors

        /// <summary>建構子
        /// </summary>
        /// <param name="interval">Runner 執行時間間隔，單位為 ms 毫秒數，不可小於或等於 0</param>
        /// <param name="name">工作處理器 Worker 名稱</param>
        public AsyncWorker( int interval = 3, string name = null )
        {
            if ( interval <= 0 )
            {
                throw new ArgumentOutOfRangeException();
            }

            _interval = interval;
            _name     = name;
        }

        /// <summary>解構子
        /// </summary>
        ~AsyncWorker() 
        {
            _consumer = null;
            _name   = null;
        }

        #endregion Constructors


        #region Public Methods

        /// <summary>加入工作任務到 AsyncWorker 的工作佇列中
        /// </summary>
        /// <param name="action">工作內容委派</param>
        /// <returns>執行結果</returns>
        public IResult<int> Enqueue( Action action ) 
        {
            lock ( _lockEnqueue )
            {
                try
                {
                    bool success = _queue.TryAdd( action, 2 );
                    return Result.Create<int>( success, _queue.Count );
                }
                catch ( Exception ex )
                {
                    return Result.Create<int>( false, message: $"Start {nameof ( AsyncWorker )} occur exception. {ex.ToString()}" );
                }
            }
        }

        /// <summary>啟動 AsyncWorker 非同步工作處理器
        /// </summary>
        /// <returns>啟動結果</returns>
        public IResult Start() 
        {
            lock ( _lockThiz )
            {
                try
                {
                    if ( _enable && _consumer.IsNotNull() )
                    {
                        return Result.Create( true );
                    }

                    _enable   = true;
                    _consumer = new Thread( () => Run() );
                    _consumer.Start();
                }
                catch ( Exception ex )
                {
                    return Result.Create( false, message: $"Start {nameof ( AsyncWorker )} occur exception. {ex.ToString()}" );
                }

                return Result.Create( true );
            }
        }

        /// <summary>關閉 AsyncWorker 非同步工作處理器
        /// </summary>
        /// <returns>關閉結果</returns>
        public IResult Stop() 
        {
            lock ( _lockThiz )
            {
                try
                {
                    _enable   = false;
                    _consumer = null;
                }
                catch ( Exception ex )
                {
                    return Result.Create( false, message: $"Stop {nameof ( AsyncWorker )} occur exception. {ex.ToString()}" );
                }

                return Result.Create( true );
            }
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>執行工作內容
        /// </summary>
        private void Run() 
        {
            while ( true )
            {
                if ( !_enable )
                {
                    break;
                }

                lock ( _lockDequeue )
                {
                    try
                    {
                        // BlockingCollection.Take 方法會自動 Block 住目前的 Thread，因此不需要特別 Sleep，反而更有效率並且節省 CPU 使用量。
                        _queue.Take()?.Invoke();
                    }
                    catch ( Exception ex )
                    {
                        var errorMsg = $"{nameof ( AsyncWorker )} execute action occur exception. AsyncWorker Name: {_name}. {Environment.NewLine}{ex.ToString()}";
                        Command.StdoutErr( errorMsg );
                        SpinWait.SpinUntil( () => false, 3 );
                        continue;
                    }
                }
            }
        }

        #endregion Private Methods
    }
}