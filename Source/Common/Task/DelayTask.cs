﻿using System;
using System.Threading.Tasks;


namespace ZayniFramework.Common
{
    /// <summary>一次性的延遲性作業
    /// </summary>
    public sealed class DelayTask
    {
        #region 宣告公開的屬性

        /// <summary>一次性動作
        /// </summary>
        public Action TargetJob { get; set; }

        /// <summary>延遲幾秒鐘
        /// </summary>
        public int DelaySeconds { get; set; }

        #endregion 宣告公開的屬性


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="action">一次性動作</param>
        /// <param name="delaySeconds">延遲幾秒鐘</param>
        public DelayTask( Action action, int delaySeconds )
        {
            TargetJob    = action;
            DelaySeconds = delaySeconds;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>於設定延遲的秒數之後，開始執行一次性動作，動作只會執行一次
        /// </summary>
        public async Task Execute()
        {
            await Task.Delay( 1000 * DelaySeconds );

            if ( TargetJob.IsNull() )
            {
                return;
            }

            try
            {
                TargetJob();
            }
            catch ( Exception ex )
            {
                throw ex;
            }
        }

        #endregion 宣告公開的方法
    }
}
