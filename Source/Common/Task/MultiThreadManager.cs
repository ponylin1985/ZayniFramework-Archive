﻿using System;
using System.Collections.Generic;
using System.Threading;


namespace ZayniFramework.Common.Tasks
{
    /// <summary>多執行緒任務的控制器
    /// </summary>
    public class MultiThreadManager
    {
        #region 宣告私有的欄位

        /// <summary>目前跑到第幾筆
        /// </summary>
        private int _index = 0;

        #endregion 宣告私有的欄位


        #region 宣告公開的唯讀屬性

        /// <summary>總共要重覆執行任務的次數
        /// </summary>
        public int TotalCount { get; private set; }

        /// <summary>每個執行階段(Iteration)要開啟的子執行緒數量
        /// </summary>
        public int IterationThreadCount { get; private set; }

        /// <summary>目標工作委派
        /// </summary>
        public Action TargetAction { get; private set; }

        /// <summary>子執行緒是否為背景執行緒
        /// </summary>
        public bool IsBackgroundThread { get; private set; }

        #endregion 宣告公開的唯讀屬性


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="totalCount">總共要重覆執行任務的次數</param>
        /// <param name="iterationThreadCount">每個執行階段 (Iteration) 要開啟的子執行緒數量</param>
        /// <param name="action">目標工作委派</param>
        /// <param name="isBackgroundThread">子執行緒是否為背景執行緒，預設為 false </param>
        public MultiThreadManager( int totalCount, int iterationThreadCount, Action action, bool isBackgroundThread = false )
        {
            if ( totalCount <= 0 )
            {
                throw new ArgumentException( $"Invalid argument '{nameof ( totalCount )}'. Must greater than 0 integer." );
            }

            if ( iterationThreadCount <= 0 )
            {
                throw new ArgumentException( $"Invalid argument '{nameof ( iterationThreadCount )}'. Must greater than 0 integer." );
            }

            if ( action.IsNull() )
            {
                throw new ArgumentNullException( $"Invalid argument '{nameof ( action )}'. Can not be null." );
            }

            TotalCount           = totalCount;
            IterationThreadCount = iterationThreadCount;
            TargetAction         = action;
            IsBackgroundThread   = isBackgroundThread;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>開始執行任務
        /// </summary>
        /// <returns>執行任務結果</returns>
        public IResult Execute()
        {
            var result = Result.Create();

            try
            {
                Run();
            }
            catch ( Exception ex )
            {
                result.HasException    = true;
                result.ExceptionObject = ex;
                result.Message         = $"Execute task occur exception. {ex.ToString()}";
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>遞迴執行任務
        /// </summary>
        private void Run()
        {
            if ( _index >= TotalCount )
            {
                _index = 0;
                return;
            }

            _index = ExecuteMultiThreadIteration( IterationThreadCount, _index );
            Run();
        }

        /// <summary>執行每個階段的多執行緒作業
        /// </summary>
        /// <param name="threadCount">每個階段預設要開啟幾條執行緒進行作業</param>
        /// <param name="start">從第幾次開始</param>
        /// <returns>目前跑到了第幾次</returns>
        private int ExecuteMultiThreadIteration( int threadCount, int start )
        {
            var threads = new List<Thread>();

            int j              = TotalCount - start;
            int howManyThreads = j < threadCount ? j : threadCount;
            
            new For().Do( howManyThreads, () => 
            {
                var t = new Thread( () => TargetAction() ) 
                { 
                    IsBackground = IsBackgroundThread 
                };
                
                threads.Add( t );
                t.Start();
            } );

            threads.ForEach( t => t.Join() );
            threads.ForEach( d => d.Abort() );
            return start + threadCount;
        }

        #endregion 宣告私有的方法
    }
}
