﻿using ZayniFramework.Common.Dynamic;
using System;
using System.Threading;


namespace ZayniFramework.Common.Tasks
{
    /// <summary>佇列工作
    /// </summary>
    public class QueueAction : BaseDynamicObject
    {
        #region 宣告私有的欄位

        /// <summary>多執行緒鎖定物件
        /// </summary>
        private readonly object _lockResult = new object();

        /// <summary>多執行緒鎖定物件
        /// </summary>
        private readonly object _lockCancel = new object();

        /// <summary>工作執行結果
        /// </summary>
        private IResult _result;

        /// <summary>工作是否被取消
        /// </summary>
        private bool _isCancel = false;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public QueueAction()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="action">工作內容委派</param>
        public QueueAction( Action<QueueAction> action )
        {
            Action = action;
        }

        /// <summary>解構子
        /// </summary>
        ~QueueAction()
        {
            Action = null;
        }

        #endregion 宣告建構子


        #region 宣告內部的屬性

        /// <summary>工作完成事件
        /// </summary>
        internal AutoResetEvent FinishEvent { get; set; }

        #endregion 宣告內部的屬性


        #region 宣告公開的屬性

        /// <summary>工作內容委派
        /// </summary>
        public Action<QueueAction> Action { get; set; }

        /// <summary>工作執行結果
        /// </summary>
        public IResult Result 
        {
            get
            {
                lock ( _lockResult )
                {
                    return _result;
                }
            }
            set
            {
                lock ( _lockResult )
                {
                    _result = value;
                }
            }
        }

        /// <summary>工作是否被取消。<para/>
        /// 在QueueAction中指定的工作內容，需要在適當的程是流程檢查 IsCancel 屬性，
        /// 如果作業已經執行過久而逾時，IsCancel會被設定為 true，後續的作業流程則應該要中止。
        /// </summary>
        /// <remarks>在QueueAction中指定的工作內容，需要在適當的程是流程檢查 IsCancel 屬性，
        /// 如果作業已經執行過久而逾時，IsCancel會被設定為 true，後續的作業流程則應該要中止。
        /// </remarks>
        public bool IsCancel 
        {
            get
            {
                lock ( _lockCancel )
                {
                    return _isCancel;
                }
            }
            set
            {
                lock ( _lockCancel )
                {
                    _isCancel = value;
                }
            }
        }

        #endregion 宣告公開的屬性
    }
}
