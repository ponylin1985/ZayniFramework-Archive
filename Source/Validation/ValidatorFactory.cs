﻿namespace ZayniFramework.Validation
{
    /// <summary>資料驗證器工廠
    /// </summary>
    public static class ValidatorFactory
    {
        /// <summary>建立資料驗證器
        /// </summary>
        /// <returns>資料驗證器</returns>
        public static Validator Create() => Validator.Instance;
    }
}
