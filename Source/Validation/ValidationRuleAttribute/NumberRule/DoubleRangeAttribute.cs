﻿using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>Double 數值範圍檢查
    /// </summary>
    public class DoubleRangeAttribute : NumberRangeAttribute
    {
        /// <summary>實作驗證機制
        /// </summary>
        /// <param name="field">需驗證物件</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate( object field )
        {
            var strValue = field + "";
            var result   = base.CheckTargetType<double>( strValue, out double target, "Invalid Double value.", double.TryParse );
            
            if ( !result.IsValid )
            {
                return result;
            }

            result = base.Validate( strValue, "Double value out of range." );
            return result;
        }
    }
}
