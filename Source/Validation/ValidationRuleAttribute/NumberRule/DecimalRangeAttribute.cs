﻿using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>Decimal大小範圍檢查
    /// </summary>
    public class DecimalRangeAttribute : NumberRangeAttribute
    {
        /// <summary>實作驗證機制
        /// </summary>
        /// <param name="field">需驗證物件</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate( object field )
        {
            var strValue = field + "";
            var result   = base.CheckTargetType<decimal>( strValue, out decimal target, "Invalid decimal value.", decimal.TryParse );
            
            if ( !result.IsValid )
            {
                return result;
            }

            result = base.Validate( strValue, "Decimal value out of range." );
            return result;
        }
    }
}
