﻿using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>Int32 整數範圍檢查
    /// </summary>
    public class IntRangeAttribute : NumberRangeAttribute
    {
        /// <summary>實作驗證機制
        /// </summary>
        /// <param name="field">需驗證物件</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate( object field )
        {
            var strValue = field + "";
            var result   = base.CheckTargetType<int>( strValue, out int target, "Invalid Int32 value.", int.TryParse );
            
            if( !result.IsValid )
            {
                return result;
            }

            result = base.Validate( strValue, "Int32 value out of range." );
            return result;
        }
    }
}
