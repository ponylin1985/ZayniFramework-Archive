﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>基底資料驗證中介資料
    /// </summary>
    [AttributeUsage( AttributeTargets.Property | AttributeTargets.Field )]
    public abstract class ValidationAttribute : Attribute, IValidate
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public ValidationAttribute()
        {
            Message = "";
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="message">資料驗證失敗訊息</param>
        public ValidationAttribute( string message )
        {
            Message = message;
        }

        /// <summary>多載建構子
        /// 1. 如果訊息需要支援多國語系，訊息資源檔必須要根據語系開出不同的.resx資源檔。
        /// </summary>
        /// <param name="resourceType">訊息資源檔的型別</param>
        /// <param name="resourceKey">訊息資源檔的Key值 (當資料驗證失敗時，會根據此key值從指定的訊息資源檔中取得「資料驗證失敗訊息」。)</param>
        public ValidationAttribute( Type resourceType, string resourceKey )
        {
            ResourceType = resourceType;
            ResourceKey  = resourceKey;
        }

        /// <summary>解構子
        /// </summary>
        ~ValidationAttribute()
        {
            Message      = null;
            ResourceType = null;
            ResourceKey  = null;
        }

        #endregion 宣告建構子


        #region 宣告公開屬性

        /// <summary>資料驗證失敗時回傳的訊息。<para/>
        /// 如果直接設定此屬性，Validation 模組就不支援多國語系。
        /// </summary>
        public string Message { get; set; }

        /// <summary>訊息資源檔的型別
        /// </summary>
        public Type ResourceType { get; set; }

        /// <summary>訊息資源檔的Key值 (當資料驗證失敗時，會根據此key值從指定的訊息資源檔中取得「資料驗證失敗訊息」。)<para/>
        /// 1. 如果訊息需要支援多國語系，訊息資源檔必須要根據語系開出不同的.resx資源檔。
        /// </summary>
        public string ResourceKey { get; set; }

        #endregion 宣告公開屬性


        #region 宣告公開的抽象

        /// <summary>執行資料驗證
        /// </summary>
        public abstract ValidationResult DoValidate( object target );

        #endregion 宣告公開的抽象


        #region 宣告保護的方法

        /// <summary>驗證後失敗的訊息的處理
        /// </summary>
        /// <param name="validationResult">驗證的結果</param>
        /// <param name="message">失敗的訊息</param>
        protected void PrepareFailure( ValidationResult validationResult, string message ) 
        {
            validationResult.IsValid = false;
            validationResult.Message = Message.IsNullOrEmpty() ? message : Message;
        }

        #endregion 宣告保護的方法
    }
}
