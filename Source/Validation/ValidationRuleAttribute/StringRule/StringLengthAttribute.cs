﻿using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>字串長度檢查
    /// </summary>
    public class StringLengthAttribute : NumberRangeAttribute
    {
        /// <summary>實作驗證機制
        /// </summary>
        /// <param name="field">需驗證物件</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate( object field )
        {
            var    result = new ValidationResult();
            string value  = field + "";

            int max;
            int min;

            bool minExpression = true;
            bool maxExpression = true;
            
            if ( int.TryParse( Min, out min ) )
            {
                minExpression = IncludeMin ? min <= value.Length : min < value.Length; 
            }

            if ( int.TryParse( Max, out max ) )
            {
                maxExpression = IncludeMax ? value.Length <= max : value.Length < max; 
            }

            if ( minExpression && maxExpression )
            {
                result.IsValid = true;
                result.Message = "";
                return result;
            }

            result.IsValid = false;
            result.Message = string.IsNullOrWhiteSpace( Message ) ? "字串長度不在設定的範圍內" : Message;
            return result;
        }
    }
}
