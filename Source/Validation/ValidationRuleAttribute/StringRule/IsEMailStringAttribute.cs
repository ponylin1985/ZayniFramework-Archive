﻿using System;
using System.Text.RegularExpressions;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>檢查是否為合法的EMail
    /// </summary>
    public class IsEMailStringAttribute : ValidationAttribute
    {
        /// <summary>檢查目標字串是否為合法的EMail格式
        /// </summary>
        /// <param name="target">受檢的目標字串</param>
        /// <returns>回傳驗證結果</returns>
        public override ValidationResult DoValidate( object target )
        {
            ValidationResult result = new ValidationResult() 
            {
                IsValid = false
            };
            
            string input   =  target + "" ;
            string pattern = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex reg = new Regex( pattern );

            try
            {
                if ( !reg.IsMatch( input ) )
                {
                    base.PrepareFailure( result, "EMail格式不正確" );
                    return result;
                }
            }
            catch ( Exception ex )
            {
                result.Message = "EMail驗證過程中發生錯誤，錯誤訊息: {0}".FormatTo( ex.Message );
                return result;
            }
            
            result.IsValid = true;
            result.Message = "";
            return result;
        }
    }
}
