﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>參考型別的資料驗證
    /// </summary>
    public abstract class ObjectPropertyValidatorAttribute : ValidationAttribute
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public ObjectPropertyValidatorAttribute()
        {
            Language = "zh-TW";
        }

        /// <summary>解構子
        /// </summary>
        ~ObjectPropertyValidatorAttribute()
        {
            Language = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>語系代碼
        /// </summary>
        public string Language { get; set; }

        #endregion 宣告公開的屬性
    }
}
