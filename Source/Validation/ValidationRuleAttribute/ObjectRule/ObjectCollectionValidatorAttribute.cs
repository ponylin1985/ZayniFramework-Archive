﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;


namespace ZayniFramework.Validation
{
    /// <summary>複雜型別集合屬性的資料驗證 (目前主要支援C#的List型別)
    /// </summary>
    public class ObjectCollectionValidatorAttribute : ObjectPropertyValidatorAttribute
    {
        /// <summary>進行資料驗證
        /// </summary>
        /// <param name="target">目標資料</param>
        /// <returns>資料驗證結果</returns>
        public override ValidationResult DoValidate( object target )
        {
            var result = new ValidationResult() 
            {
                IsValid = false
            };

            if ( target.IsNull() )
            {
                result.Message = Message.IsNullOrEmptyString( "資料不可以為Null值。", true );
                return result;
            }

            IEnumerable models = target as IEnumerable;

            if ( models.IsNull() )
            {
                result.Message = "資料必須為集合型別。";
                return result;
            }

            if ( 0 == models.Count() )
            {
                result.Message = Message.IsNullOrEmptyString( "資料集合的筆數為 0 筆。", true );
                return result;
            }

            var results   = new List<ValidationResult>();
            var validator = ValidatorFactory.Create();

            foreach ( var model in models )
            {
                var r = validator.Validate( new ValidationEntry() 
                {
                    TargetModel = model,
                    Language    = base.Language
                } );

                results.Add( r );
            }

            if ( results.Any( r => !r.IsValid ) )
	        {
                result.Message = Message.IsNullOrEmptyString( "資料集合驗證失敗。", true );
                return result;
	        }

            result.IsValid = true;
            return result;
        }
    }
}
