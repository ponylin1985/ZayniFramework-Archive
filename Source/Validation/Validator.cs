﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Validation
{
    /// <summary>資料驗證器
    /// </summary>
    public sealed class Validator
    {
        #region 宣告私有的常數

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>錯誤訊息的標題
        /// </summary>
        private static readonly string _logTitle = nameof ( Validator );

        /// <summary>作業系統的換行符號
        /// </summary>
        private static readonly string _newline = Environment.NewLine;

        #endregion 宣告私有的常數


        #region 宣告靜態成員

        /// <summary>資料驗證器實體參考
        /// </summary>
        private static Validator _instance;

        /// <summary>資料驗證器獨體
        /// </summary>
        public static Validator Instance
        {
            get
            {
                using ( _asyncLock.Lock() )
                {
                    if ( _instance.IsNull() )
                    {
                        _instance = new Validator();
                        return _instance;
                    }
                }

                return _instance;
            }
        }

        #endregion 宣告靜態成員


        #region 宣告私有的建構子

        /// <summary>私有的建構子
        /// </summary>
        private Validator()
        {
            // pass
        }

        #endregion 宣告私有的建構子


        #region 宣告公開的屬性

        /// <summary>驗證失敗的回呼委派
        /// </summary>
        public FailureHandler FailCallback { get; set; }

        /// <summary>程式異常的回呼委派
        /// </summary>
        public ExceptionHandler ExceptionCallback { get; set; }

        #endregion 宣告公開的屬性


        #region 宣告公開方法

        /// <summary>執行資料模型驗證
        /// </summary>
        /// <param name="entry">資料驗證請求</param>
        /// <returns>資料驗證結果</returns>
        public ValidationResult Validate( ValidationEntry entry )
        {
            var result = new ValidationResult() 
            {
                IsValid = false
            };
            
            string[] ignoreProperies = null;

            if ( entry.IgnoreProperies.IsNotNull() )
            {
                ignoreProperies = entry.IgnoreProperies.ToArray();
            }

            bool isValid = Validate( entry.TargetModel, entry.Language, entry.IgnoreValitions, out List<Dictionary<string, List<string>>> invalidMessages, out string message, ignoreProperies );

            result.IsValid            = isValid;
            result.ValidationMessages = invalidMessages;
            result.Message            = message;
            return result;
        }

        #endregion 宣告公開方法


        #region 宣告私有方法

        /// <summary>執行驗證並回傳是否成功
        /// </summary>
        /// <param name="model">目標資料模型</param>
        /// <param name="language">語系代碼</param>
        /// <param name="ignoreValitions">此次資料驗證要忽略的驗證規則</param>
        /// <param name="invalidMessages">驗證失敗的資訊集合</param>
        /// <param name="message">錯誤訊息</param>
        /// <param name="ignoreProperies">此次資料驗證可以忽略檢查的屬性名稱，傳入後將不會對這些屬性進行資料驗證</param>
        /// <returns>目標資料模型是否通過驗證</returns>
        private bool Validate( object model, string language, Dictionary<string, List<string>> ignoreValitions, out List<Dictionary<string, List<string>>> invalidMessages, out string message, params string[] ignoreProperies )
        {
            message         = string.Empty;
            invalidMessages = new List<Dictionary<string, List<string>>>();

            if ( model.IsNull() )
            {
                message = "Target model object is null.";
                Logger.WriteErrorLog( this, message, _logTitle );
                HandlerExecuter.DoFailureHandler( FailCallback );
                return false;
            }
            
            Type type = model.GetType();

            PropertyInfo[] properties = type.GetProperties( BindingFlags.Public    |
                                                            BindingFlags.NonPublic |
                                                            BindingFlags.Instance );

            if ( properties.IsNullOrEmptyArray() )
            {
                return true;
            }
            
            bool result = true;
            string logMessage = string.Empty;
            
            ValidationResult validateResult = null;

            foreach ( PropertyInfo propertyInfo in properties )
            {
                // 20140813 Added by Pony: 外界可以決定每次的資料模行驗證忽略掉哪些屬性不進行檢查
                if ( ignoreProperies.IsNotNullOrEmptyArray() && ignoreProperies.Contains( propertyInfo.Name ) )
                {
                    continue;
                }

                List<string> ignoreValidationRules = null;

                try
                {
                    if ( null != ignoreValitions && 0 != ignoreValitions.Count )
                    {
                        ignoreValidationRules = ignoreValitions.Where( m => m.Key == propertyInfo.Name ).FirstOrDefault().Value;
                    }
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( this, ex, _logTitle );
                    ignoreValidationRules = null;
                }

                object obj     = propertyInfo.GetValue( model );
                validateResult = DoValidate( propertyInfo, language, obj, ignoreValidationRules );

                if ( !validateResult.IsValid )
                {
                    result        = false;
                    string line   = message.IsNullOrEmpty() ? string.Empty : _newline;
                    message      += "{0}{1}".FormatTo( line, validateResult.Message );
                    logMessage   += "{0}{1}Property : {2}{1}Message : {3}".FormatTo( line, _newline, propertyInfo.Name, validateResult.Message );
                    invalidMessages.Add( validateResult.InvalidMessages );
                }
            }

            if ( !result )
            {   
                Logger.WriteErrorLog( this, logMessage, _logTitle );
            }

            return result;
        }

        /// <summary>對所有有標記資料驗證 ValidationAttribute 的 Property 進形資料驗證。<para/>
        /// 反射 Property 屬性，有標示 BaseAttribute 就做資料驗證
        /// </summary>
        /// <param name="property">屬性資訊</param>
        /// <param name="language">語系代碼</param>
        /// <param name="obj">需驗證資料</param>
        /// <param name="ignoreValitionRules">此次資料驗證要忽略掉的驗證規則</param>
        /// <returns>回傳驗證結果</returns>
        private ValidationResult DoValidate( PropertyInfo property, string language, object obj, List<string> ignoreValitionRules = null )
        {
            var result = new ValidationResult() 
            {
                IsValid = true,
                Message = null
            };

            var map = new Dictionary<string ,List<string>>();

            foreach ( Attribute attribute in Attribute.GetCustomAttributes( property ) )
            {
                ValidationAttribute validationAttribute = attribute as ValidationAttribute;

                if ( validationAttribute.IsNull() )
                {
                    continue;
                }

                string validationRuleName = GetValidationAttributeName( validationAttribute.GetType().Name );

                // 20140821 Added by Pony: 讓外界可以決定每一次資料驗證時，可以決定該屬性哪些驗證規則可以忽略!
                if ( ignoreValitionRules.IsNotNullOrEmptyList() && ignoreValitionRules.Contains( validationRuleName ) )
                {
                    continue;
                }

                Type            type        = attribute.GetType();
                ConstructorInfo constructor = type.GetConstructor( Type.EmptyTypes );
  
                if ( constructor.IsNull() )
                {
                    result.IsValid = false;
                    result.Message = $"Validation fail due to '{type.Name}' does not contain default no args constructor.";
                    Logger.WriteErrorLog( this, result.Message, _logTitle ); 
                    continue;
                }

                object classObject;

                try
                {
                    classObject = constructor.Invoke( new object[] {} );
                }
                catch ( Exception ex )
                {
                    result.IsValid = false;
                    result.Message = "{0} : 呼叫預設建構子出現Exception{2} Exception : {1}".FormatTo( type.Name, ex.ToString(), _newline );
                    Logger.WriteErrorLog( this, result.Message, _logTitle );
                    HandlerExecuter.DoExceptionHandler( ExceptionCallback, ex );
                    continue;
                }
                    
                PropertyInfo[] properties = type.GetProperties( BindingFlags.Public     |
                                                                BindingFlags.NonPublic  |
                                                                BindingFlags.Instance);
                    
                if ( properties.IsNullOrEmptyArray() )
                {
                    continue;
                }

                foreach ( PropertyInfo propertyInfo in properties )
                {
                    if ( "TypeId" == propertyInfo.Name )
                    {
                        continue;
                    }

                    object prop = propertyInfo.GetValue( attribute );
                    classObject.GetType().GetProperty( propertyInfo.Name ).SetValue( classObject, prop );
                }
                
                ValidationResult validationResult = null;

                try
                {
                    MethodInfo methodInfo = type.GetMethod( "DoValidate" );
                    validationResult = methodInfo.Invoke( classObject, new object[] { obj } ) as ValidationResult;
                }
                catch ( Exception ex )
                {
                    result.IsValid = false;
                    result.Message = "{0} : DoValidate方法出現 Exception{2} Exception : {1}".FormatTo( type.Name, ex.ToString(), _newline );
                    Logger.WriteErrorLog( this, result.Message, _logTitle );
                    HandlerExecuter.DoExceptionHandler( ExceptionCallback, ex );
                    continue;
                }
                    
                if ( validationResult.IsNull() )
                {
                    result.IsValid = false;
                    result.Message = "{0} : DoValidate方法沒有正確回傳ValidationResult型別物件，資料驗證失敗。".FormatTo( type.Name );
                    Logger.WriteErrorLog( this, result.Message, _logTitle );
                    HandlerExecuter.DoFailureHandler( FailCallback );
                    continue;
                }

                if ( !validationResult.IsValid )
                {
                    result.IsValid = false;

                    if ( validationAttribute.ResourceType.IsNotNull() && validationAttribute.ResourceKey.IsNotNullOrEmpty() )
                    {
                        ResourceManager rm       = new ResourceManager( validationAttribute.ResourceType );
                        validationResult.Message = rm.GetString( validationAttribute.ResourceKey, CultureInfo.CreateSpecificCulture( language ) );
                    }

                    if ( validationResult.Message.IsNotNullOrEmpty() )
                    {
                        string line     = result.Message.IsNullOrEmpty() ? string.Empty : _newline;
                        result.Message += "{0}{1}".FormatTo( line, validationResult.Message );
                    }
                    
                    if ( !result.InvalidMessages.ContainsKey( property.Name ) )
                    {
                        var info = new List<string>
                        {
                            validationResult.Message
                        };

                        result.InvalidMessages.Add( property.Name, info );
                    }
                    else 
                    {
                        List<string> info = result.InvalidMessages[ property.Name ];
                        info.Add( validationResult.Message );
                    }
                }
            }

            return result;
        }

        /// <summary>取得資料驗證規則 Attribute 的名稱 (去除掉後綴字Attribute)
        /// </summary>
        /// <param name="attributeName">Attribute的類別名稱</param>
        /// <returns>資料驗證規則Attribute的名稱 (去除掉後綴字Attribute)</returns>
        private string GetValidationAttributeName( string attributeName )
        {
            try
            {
                return attributeName.Remove( attributeName.IndexOf( "Attribute" ) );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, _logTitle );
                return attributeName;                
            }
        }

        #endregion 宣告私有方法
    }
}
