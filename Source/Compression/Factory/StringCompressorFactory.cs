﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Compression
{
    /// <summary>字串壓縮器工廠
    /// </summary>
    public class StringCompressorFactory
    {
        /// <summary>建立字串壓縮器
        /// </summary>
        /// <param name="compressionType">壓縮格式(GZIP、LZW)</param>
        /// <returns>字串壓縮器</returns>
        public static ITextCompressor Create( string compressionType = "GZIP" )
        {
            ITextCompressor result = null;

            switch ( compressionType.ToUpper() )
            {
                case "GZIP":
                    result = new GZipCompressor();
                    break;

                case "LZW":
                    result = new LzwCompressor();
                    break;

                default:
                    result = new GZipCompressor();
                    break;
            }

            return result;
        }
    }
}
