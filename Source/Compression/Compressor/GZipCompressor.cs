﻿using System;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Compression
{
    /// <summary>GZip格式字串壓縮器
    /// </summary>
    public class GZipCompressor : ITextCompressor, IDataSetCompressor, IEntityCompressor
    {
        #region 實作ITextCompressor介面方法

        /// <summary>壓縮目標字串
        /// </summary>
        /// <param name="target">目標字串</param>
        /// <returns>壓縮過後的字串</returns>
        public string Compress( string target )
        {
            if ( target.IsNullOrEmpty() )
            {
                Logger.WriteErrorLog( this, "目標字串為Null或是空字串，無法進行字串壓縮。", "GZipCompressor.Compress" );
                return string.Empty;
            }

            string result = string.Empty;

            try
            {
                result = target.GZipCompress();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "GZipCompressor.Compress" );
                return string.Empty;
            }

            return result;
        }

        /// <summary>解壓縮目標字串
        /// </summary>
        /// <param name="target">有壓縮過的目標字串</param>
        /// <returns>解壓縮過的字串</returns>
        public string Decompress( string target )
        {
            if ( target.IsNullOrEmpty() )
            {
                Logger.WriteErrorLog( this, "目標字串為Null或是空字串，無法進行字串解壓縮。", "GZipCompressor.Decompress" );
                return string.Empty;
            }

            string result = string.Empty;

            try
            {
                result = target.GZipDecompress();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "GZipCompressor.Decompress" );
                return string.Empty;
            }

            return result;
        }

        #endregion 實作ITextCompressor介面方法


        #region 實作IDataSetCompressor介面方法

        /// <summary>壓縮目標的DataSet物件
        /// </summary>
        /// <param name="target">目標DataSet</param>
        /// <returns>壓縮過的二進位資料</returns>
        public byte[] Compress( DataSet target ) 
        {
            byte[] result = null;

            using ( var stream = new MemoryStream() )
            using ( var gzip   = new GZipStream( stream, CompressionMode.Compress ) )
            {
                try
                {
                    target.WriteXml( gzip, XmlWriteMode.WriteSchema );
                    gzip.Close();

                    result = stream.ToArray();
                    stream.Close(); 
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( "GZipCompressor", ex, "將目標DataSet物件壓縮成GZip二進位資料發生異常: {0}".FormatTo( ex.ToString() ) );
                    return null;
                }
            }
            
            return result; 
        }

        /// <summary>解壓縮目標二進位資料
        /// </summary>
        /// <param name="target">目標二進位資料</param>
        /// <returns>解壓縮過後的DataSet物件</returns>
        public DataSet Decompress( byte[] target )
        {
            if ( target.IsNull() )
            {
                Logger.WriteErrorLog( this, "目標二進位資料為Null值，無法進行二進位解壓縮。", "GZipCompressor.Decompress" );
                return null;
            }

            DataSet result = new DataSet();

            using ( var stream = new MemoryStream( target ) )
            using ( var gzip   = new GZipStream( stream, CompressionMode.Decompress ) )
            {
                try
                {
                    result.ReadXml( gzip, XmlReadMode.ReadSchema );
                    gzip.Close();
                    stream.Close();
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( "GZipCompressor", ex, "將二進位資料解壓縮成DataSet物件發生異常: {0}".FormatTo( ex.ToString() ) );
                    return null;
                }
            }
            
            return result;
        }

        #endregion 實作IDataSetCompressor介面方法


        #region 實作IEntityCompressor介面方法

        /// <summary>對目標物件進行二進位壓縮
        /// </summary>
        /// <param name="target">目標物件</param>
        /// <returns>壓縮過後的二進位資料</returns>
        public byte[] CompressObject( object target ) 
        {
            byte[] result = null;
            byte[] binary = null;

            using ( var mStream = new MemoryStream() )
            {
                var formatter = new BinaryFormatter();

                try
                {
                    formatter.Serialize( mStream, target );
                    binary = mStream.ToArray();
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( this, ex, "對目標物件進行GZip資料壓縮時發生異常: {0}".FormatTo( ex.ToString() ) );
                    return null;
                }
            }

            using ( var stream = new MemoryStream() )
            {
                try
                {
                    using ( var gzip     = new GZipStream( stream, CompressionMode.Compress ) )
                    using ( var myStream = new MemoryStream( binary ) )
                    {
                        myStream.CopyTo( gzip );
                    }

                    result = stream.ToArray();
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( this, ex, "對目標物件進行GZip資料壓縮時發生異常: {0}".FormatTo( ex.ToString() ) );
                    return null;
                }
            }

            return result;
        }

        /// <summary>對目標壓縮過後的二進位資料進行解壓縮，還原成資料模型物件
        /// </summary>
        /// <param name="binary">壓縮過的二進位資料</param>
        /// <returns>資料模型物件</returns>
        public object DecompressObject( byte[] binary ) 
        {
            object result    = null;
            var    formatter = new BinaryFormatter();

            try
            {
                using ( var stream  = new MemoryStream( binary ) )
                using ( var gzip    = new GZipStream( stream, CompressionMode.Decompress ) )
                using ( var mStream = new MemoryStream() )
                {
                    gzip.CopyTo( mStream );
                    mStream.Position = 0;
                    result = formatter.Deserialize( mStream );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "對目標二進位資料進行GZip解壓縮時發生異常: {0}".FormatTo( ex.ToString() ) );
                return null;
            }

            return result;
        }

        /// <summary>對目標壓縮過後的二進位資料進行解壓縮，還原成資料模型物件
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="target">壓縮過的二進位資料</param>
        /// <returns>資料模型物件</returns>
        public TModel DecompressObject<TModel>( byte[] target )
            where TModel : class
        {
            object obj    = this.DecompressObject( target );
            TModel result = obj as TModel;

            if ( result.IsNull() )
            {
                Logger.WriteErrorLog( this, "解壓縮後的物件無法正常轉型成指定的泛型", "GZipCompressor.DecompressObject<TModel>" );
            }

            return result;
        }

        #endregion 實作IEntityCompressor介面方法


        #region 宣告公開的靜態字串壓縮/解壓縮方法

        /// <summary>將字串壓縮成GZip壓縮格式Base64編碼的字串
        /// </summary>
        /// <param name="target">要進行GZip壓縮的目標字串</param>
        /// <returns>GZip壓縮格式的二進位元陣列</returns>
        public static byte[] GZipCompress( string target )
        {
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes( target );

                using ( var msi = new MemoryStream( bytes ) )
                using ( var mso = new MemoryStream() )
                {
                    using ( var gs = new GZipStream( mso, CompressionMode.Compress ) )
                    {
                        msi.CopyTo( gs );
                    }

                    byte[] result = mso.ToArray();
                    return result;
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( "GZipCompressor", ex, "將目標字串壓縮成GZip字串發生異常: {0}".FormatTo( ex.ToString() ) );
                return null;
            }
        }

        /// <summary>將GZip壓縮格式的二位元陣列解壓縮成一般字串
        /// </summary>
        /// <param name="uncompressed">壓縮過的二位元陣列</param>
        /// <returns>一般字串</returns>
        public static string GZipDecompress( byte[] uncompressed )
        {
            try
            {
                using ( var msi = new MemoryStream( uncompressed ) )
                using ( var mso = new MemoryStream() )
                {
                    using ( var gs = new GZipStream( msi, CompressionMode.Decompress ) )
                    {
                        gs.CopyTo( mso );
                    }

                    string result = Encoding.UTF8.GetString( mso.ToArray() );
                    return result;
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( "GZipCompressor", ex, "將目標GZip資料解壓縮成一般字串發生異常: {0}".FormatTo( ex.ToString() ) );
                return null;
            }
        }

        #endregion 宣告公開的靜態字串壓縮/解壓縮方法
    }
}
