﻿using System.Collections.Generic;
using System.Text;
using ZayniFramework.Common;


namespace ZayniFramework.Compression
{
    /// <summary>LZW演算法字串壓縮器
    /// </summary>
    public class LzwCompressor : ITextCompressor
    {
        /// <summary>壓縮目標字串
        /// </summary>
        /// <param name="target">目標字串</param>
        /// <returns>壓縮過後的字串</returns>
        public string Compress( string target )
        {
            List<int> compressed = LzwCompress( target );
            string result        = string.Join( ",", compressed );
            return result;
        }

        /// <summary>解壓縮目標字串
        /// </summary>
        /// <param name="target">有壓縮過的目標字串</param>
        /// <returns>解壓縮過的字串</returns>
        public string Decompress( string target )
        {
            string[] tokens = target.Split( ',' );

            List<int> compressed = new List<int>();

            for ( int i = 0, len = tokens.Length; i < len; i++ )
            {
                string token = tokens[ i ];
                int    item  = int.Parse( token );
                compressed.Add( item );
            }
            
            string result = LzwDecompress( compressed );
            return result;
        }

        /// <summary>對目標字串進行LZW演算法壓縮
        /// </summary>
        /// <param name="uncompressed">目標字串</param>
        /// <returns>LZW壓縮過後的數字串列</returns>
        public static List<int> LzwCompress( string uncompressed )
        {
            uncompressed   = uncompressed.Base64Encode();     // 為了解決中文字編碼的問題
            var dictionary = new Dictionary<string, int>();
            
            for ( int i = 0; i < 256; i++ )
            {
                var key = ((char)i).ToString();
                dictionary.Add( key, i );
            }
 
            string w = string.Empty;
            List<int> compressed = new List<int>();
 
            foreach ( char c in uncompressed )
            {
                string wc = w + c;

                if ( dictionary.ContainsKey( wc ) )
                {
                    w = wc;
                }
                else
                {
                    compressed.Add( dictionary[ w ] );
                    dictionary.Add( wc, dictionary.Count );
                    w = c.ToString();
                }
            }
            
            if ( w.IsNotNullOrEmpty() ) 
            {
                compressed.Add( dictionary[ w ] );
            }
 
            return compressed;
        }

        /// <summary>對目標LZW數字串列進行解壓縮
        /// </summary>
        /// <param name="compressed">目標數字串列</param>
        /// <returns>解壓縮後的字串</returns>
        public static string LzwDecompress( List<int> compressed )
        {
            var dictionary = new Dictionary<int, string>();
            
            for ( int i = 0; i < 256; i++ ) 
            {
                var value = ((char)i).ToString();
                dictionary.Add( i, value );
            }
 
            string w = dictionary[ compressed[ 0 ] ];
            compressed.RemoveAt( 0 );
            
            StringBuilder decompressed = new StringBuilder( w );
 
            foreach ( int k in compressed )
            {
                string entry = null;

                if ( dictionary.ContainsKey( k ) ) 
                {
                    entry = dictionary[ k ];
                }
                else if ( k == dictionary.Count ) 
                {
                    entry = w + w[ 0 ];
                }
                    
 
                decompressed.Append( entry );
                dictionary.Add( dictionary.Count, w + entry[ 0 ] );
                w = entry;
            }

            string result = decompressed.ToString().Base64Decode();     // 為了解決中文字編碼的問題
            return result;
        }
    }
}
