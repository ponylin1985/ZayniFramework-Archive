﻿using RabbitMQ.Client.Events;
using System.Collections.Generic;
using System.Text;


namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// RabbitMQ Message回覆資料實作
    /// </summary>
    public class RabbitMQEventMessage : IEventMessage
    {
        #region 屬性、欄位

        /// <summary>
        /// MQ的訊息
        /// </summary>
        BasicDeliverEventArgs _ea;

        /// <summary>
        /// 該訊息的Consumer
        /// </summary>
        EventingBasicConsumer _consumer;

        /// <summary>
        /// 拒絕時是否丟回Queue中(預設true)
        /// </summary>
        bool _requeue;

        /// <summary>
        /// consum是否為自動ACK，若TRUE的話，ACT會造成Channel斷線，因此要避免
        /// </summary>
        bool _autoAct;

        #endregion

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="ch">該訊息的Consumer</param>
        /// <param name="ea">MQ的訊息</param>
        /// <param name="isRequeue">拒絕時，是否返回訊息至佇列中</param>
        /// <param name="autoAct">是否自動 Ack 認可訊息</param>
        public RabbitMQEventMessage(object ch, BasicDeliverEventArgs ea, bool isRequeue, bool autoAct)
        {
            _consumer = ch as EventingBasicConsumer;
            _ea = ea;
            _requeue = isRequeue;
            _autoAct = autoAct;
        }

        /// <summary>
        /// 建構子
        /// </summary>
        /// <param name="ea">MQ的訊息</param>
        public RabbitMQEventMessage(BasicDeliverEventArgs ea)
        {
            _consumer = null;
            _ea = ea;
            _requeue = false;
        }

        /// <summary>
        /// 認可/處理結束此訊息
        /// </summary>
        public void Act()
        {
            if (_autoAct)
                return;
            if (_consumer != null)
                _consumer.Model.BasicAck(_ea.DeliveryTag, false);
        }

        /// <summary>
        /// 丟棄/返回此訊息
        /// </summary>
        public void NAck()
        {
            if (_autoAct)
                return;
            if (_consumer != null)
                _consumer.Model.BasicReject(_ea.DeliveryTag, _requeue);
        }

        /// <summary>
        /// 取得訊息格式
        /// </summary>
        public string GetContentType()
        {
            return _ea.BasicProperties.ContentType;
        }

        /// <summary>
        /// 取得訊息的關連ID
        /// </summary>
        public string GetCorrelationID()
        {
            return _ea.BasicProperties.CorrelationId;
        }

        /// <summary>
        /// 取得主要訊息內容
        /// </summary>
        public string GetBody()
        {
            return Encoding.UTF8.GetString(_ea.Body);
        }

        /// <summary>
        /// 取得事件名稱
        /// </summary>
        public string GetEvent()
        {
            return _ea.RoutingKey;
        }

        /// <summary>
        /// 取得訊息的ID
        /// </summary>
        public string GetMessageID()
        {
            return _ea.BasicProperties.MessageId;
        }

        /// <summary>
        /// 取得訊息應該要回覆的地址
        /// </summary>
        public string GetReplyTo()
        {
            return _ea.BasicProperties.ReplyTo;
        }

        /// <summary>
        /// 取得訊息的時間戳記(毫秒)
        /// </summary>
        public long GetTimestamp()
        {
            return _ea.BasicProperties.Timestamp.UnixTime;
        }

        /// <summary>
        /// 取得BasicProperties中的Header資訊
        /// </summary>
        public object GetHeader(string key)
        {
            if (_ea.BasicProperties.Headers == null)
                return null;

            if (!_ea.BasicProperties.Headers.ContainsKey(key))
                return null;

            return _ea.BasicProperties.Headers[key];
        }

        /// <summary>
        /// 取得BasicProperties中的Header資訊
        /// </summary>
        public IDictionary<string, object> GetHeaders()
        {
            if (_ea.BasicProperties.Headers == null)
                return null;

            return _ea.BasicProperties.Headers;
        }
    }
}
