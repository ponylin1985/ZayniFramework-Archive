﻿using System;
using System.Collections.Generic;

namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// RabbitMQClient資料.
    /// </summary>
    public class RabbitMQModule
    {
        #region 建構子

        /// <summary>
        /// 建構子
        /// </summary>
        public RabbitMQModule()
        {
            ConnectionPolicy = new RabbitMQConnectionPolicy();
            PublishPolicy = new RabbitMQPublishPolicy();
        }

        #endregion

        #region Propertys

        /// <summary>
        /// 連線資訊
        /// </summary>
        public RabbitMQConnectionPolicy ConnectionPolicy { get; set; }

        /// <summary>
        /// Publish原則
        /// </summary>
        public RabbitMQPublishPolicy PublishPolicy { get; set; }

        /// <summary>
        /// Key: RoutingKey
        /// value: RoutingKey相對應publish時要使用的exchange
        /// </summary>
        public Dictionary<string, string> RoutingExchangeMapper { get; private set; } = new Dictionary<string, string>();

        /// <summary>
        /// 預設publish時要使用的Exchange名稱
        /// </summary>
        public string DefaultRoutingExchange { get; private set; } = "";

        /// <summary>
        /// Key: Routekey
        /// value: 相對應要接收RPC請求時要使用的QueueName
        /// </summary>
        public Dictionary<string, string> RPCReplyQueueMapper { get; private set; } = new Dictionary<string, string>();

        /// <summary>
        /// RPC發出請求後，預設要在哪個Queue等候RPC回覆的Queue名稱
        /// </summary>
        public string DefaultRPCReplyQueueName { get; private set; }

        /// <summary>
        /// 請求連線伺服器心跳的時間(秒數, 預設10)
        /// </summary>
        public ushort RequestHeartBeat { get; private set; } = 10;

        /// <summary>
        /// 連線關閉的匿名委派
        /// </summary>
        public Action<ConnectionShutdownEventArgs> _connShutdownEvent { get; private set; }

        #endregion

        #region Public Method

        /// <summary>
        /// 加入連線資訊
        /// </summary>
        /// <param name="connectionPolicy">連線資料模型</param>
        public void SetConnectionPolicy(RabbitMQConnectionPolicy connectionPolicy)
        {
            if (connectionPolicy == null)
                throw new Exception("RabbitMQConnectionPolicy sould not null.");

            ConnectionPolicy = connectionPolicy;
        }

        /// <summary>
        /// 將routingKey和exchange的關連加入Mapper表
        /// 當publish訊息時的routingKey，若在Mapper中有找到ExchangeName,
        /// 將會以找到的Exchange送至該Exchange
        /// 此優先權大於預設Exchange
        /// </summary>
        /// <param name="routingKey">事件名稱</param>
        /// <param name="exchangeName">Exchange</param>
        public void SetRoutingExchangeMapper(string routingKey, string exchangeName)
        {
            if (string.IsNullOrEmpty(routingKey) || string.IsNullOrEmpty(exchangeName))
                throw new Exception("routingKey or exchangeName was null.");

            if (RoutingExchangeMapper.ContainsKey(routingKey))
                RoutingExchangeMapper[routingKey] = exchangeName;
            else
                RoutingExchangeMapper.Add(routingKey, exchangeName);
        }

        /// <summary>
        /// 預設的Exchange名稱
        /// 使用於Publish or RPC呼叫時
        /// </summary>
        public void SetDefaultExchange(string defaultName)
        {
            //為避免不要null
            DefaultRoutingExchange = defaultName + "";
        }

        /// <summary>
        /// 將RPC送出routingKey和回覆的ReplyQueue的關連加入Mapper表
        /// </summary>
        /// <param name="routingKey">事件名稱</param>
        /// <param name="replyQueueName">RPC請求回覆時的QueueName</param>
        public void SetRPCReplyQueueMapper(string routingKey, string replyQueueName)
        {
            if (string.IsNullOrEmpty(routingKey))
                throw new Exception("routingKey was null.");

            if (RPCReplyQueueMapper.ContainsKey(routingKey))
                RPCReplyQueueMapper[routingKey] = replyQueueName + "";
            else
                RPCReplyQueueMapper.Add(routingKey, replyQueueName + "");
        }

        /// <summary>
        /// 預設RPC請求要回覆的QueueName
        /// </summary>
        public void SetDefaultRPCReplyQueue(string defaultReplyName)
        {
            DefaultRPCReplyQueueName = defaultReplyName + "";
        }

        /// <summary>
        /// 設定連線從MQServer收到的心跳請求秒數間隔，若大於這個間隔沒有，則會發出例外
        /// </summary>
        /// <param name="seconds">非負秒數</param>
        public void SetConnectionHeartBeat(ushort seconds)
        {
            RequestHeartBeat = seconds;
        }

        /// <summary>
        /// 連線關閉的匿名委派，只允許存在一個
        /// </summary>
        public void SetConnectionShutdownEvent(Action<ConnectionShutdownEventArgs> action)
        {
            if (action == null)
                throw new Exception("action null.");

            _connShutdownEvent = action;
        }

        #endregion
    }
}
