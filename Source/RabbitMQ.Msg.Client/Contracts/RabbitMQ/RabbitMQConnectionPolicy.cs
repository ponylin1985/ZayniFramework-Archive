﻿namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// 連線資訊資料模型
    /// </summary>
    public class RabbitMQConnectionPolicy
    {
        /// <summary>
        /// 靜態方法，建立連線資料模型
        /// </summary>
        /// <param name="host">Host</param>
        /// <param name="port">Port</param>
        /// <param name="user">User</param>
        /// <param name="password">Password</param>
        /// <param name="vhost">Virtual Host</param>
        /// <returns></returns>
        public static RabbitMQConnectionPolicy CreatePolicy(string host, int port, string user, string password, string vhost)
        {
            return new RabbitMQConnectionPolicy()
            {
                HostName = host,
                Port = port,
                UserName = user,
                Password = password,
                VirtualHost = vhost,
                AutoReConnect = true
            };
        }

        /// <summary>
        /// RabbitMQ server Host
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// RabbitMQ server port
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Login Account
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Login password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// RabbitMQ server virtual host
        /// </summary>
        public string VirtualHost { get; set; }

        /// <summary>
        /// ReConnect when disconnect;
        /// </summary>
        public bool AutoReConnect { get; set; }
    }
}
