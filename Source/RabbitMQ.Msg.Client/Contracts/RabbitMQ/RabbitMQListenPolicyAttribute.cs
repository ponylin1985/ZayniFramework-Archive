﻿using System;

namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// 方法函式的標籤，用於Rabbit MQ Listen委派
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class RabbitMQListenPolicyAttribute : Attribute
    {
        /// <summary>
        /// 拿取Message的大小(預設0)
        /// </summary>
        public uint PrefetchSize { get; set; } = 0;

        /// <summary>
        /// 拿取Message的數量(預設1)
        /// </summary>
        public ushort PrefetchCount { get; set; } = 1;

        /// <summary>
        /// 拒絕(NAck)時是否丟回Queue中(預設true)
        /// </summary>
        public bool ReQueue { get; set; } = true;

        /// <summary>
        /// 從Queue拿取訊息的同時，Act這個訊息(預設false)
        /// </summary>
        public bool AutoAct { get; set; } = false;
    }
}
