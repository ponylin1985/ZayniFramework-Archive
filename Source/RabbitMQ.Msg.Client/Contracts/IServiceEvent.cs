﻿using System;
using System.Collections.Generic;

namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// Service Event Interface
    /// </summary>
    public interface IServiceEvent : IDisposable
    {
        /// <summary>
        /// 監聽ServiceEvent訊息
        /// </summary>
        /// <param name="eventName">監聽的ServiceEvent事件名稱</param>
        /// <param name="listenFunc">監聽的委派事件。必須包含Attribute RabbitMQListenPolicyAttribute。若Return True則停止監控，若Return False則繼續監聽</param>
        /// <param name="closeNotifyAction">監聽事件結束的委派事件。</param>
        void Listen(string eventName, Func<IEventMessage, bool> listenFunc, Action<ListenShutdownEventArgs> closeNotifyAction = null);

        /// <summary>
        /// 監聽ServiceEvent訊息, 若事件發生例外, 則關閉Channel停止該Queue的監聽事件
        /// </summary>
        /// <param name="queueNamePrefix">監聽的 Queue 名稱前綴字</param>
        /// <param name="exchangeName">監聽的 Exchange 名稱</param>
        /// <param name="routingKey">監聽的 RoutingKey 名稱</param>
        /// <param name="listenFunc">監聽的委派事件。必須包含Attribute RabbitMQListenPolicyAttribute。若Return True則停止監控，若Return False則繼續監聽</param>
        /// <param name="closeNotifyAction">監聽事件結束的委派事件。</param>
        void ListenExchange( string queueNamePrefix, string exchangeName, string routingKey, Func<IEventMessage, bool> listenFunc, Action<ListenShutdownEventArgs> closeNotifyAction = null );

        /// <summary>
        /// 發送訊息
        /// </summary>
        /// <param name="eventName">ServiceEvent事件名稱</param>
        /// <param name="contentType">內容型式</param>
        /// <param name="body">主要訊息</param>
        /// <param name="messageID">訊息識別碼</param>
        /// <param name="timestamp">時間戳紀(毫秒)</param>
        void Emit(string eventName, string contentType, string body, string messageID, long timestamp);

        /// <summary>
        /// 發送RPC訊息
        /// </summary>
        /// <param name="eventName">ServiceEvent事件名稱</param>
        /// <param name="contentType">內容型式</param>
        /// <param name="body">主要訊息</param>
        /// <param name="messageID">訊息識別碼</param>
        /// <param name="timestamp">時間戳紀(毫秒)</param>
        /// <param name="timeout">等待時間(毫秒)</param>
        /// <param name="onlyWaitOne">有一個Message回覆，則立即返回</param>
        /// <returns></returns>
        List<IEventMessage> RPC(string eventName, string contentType, string body, string messageID, long timestamp, ushort timeout, bool onlyWaitOne);

        /// <summary>
        /// 回覆RPC訊息
        /// </summary>
        /// <param name="eventMessage">收到來自於RPC的請求Message</param>
        /// <param name="contentType">回覆的內容格式</param>
        /// <param name="body">回覆的主要訊息</param>
        /// <param name="messageID">回覆的訊息識別碼</param>
        /// <param name="timestamp">回覆時間戳紀(毫秒)</param>
        void ResponseRPC(IEventMessage eventMessage, string contentType, string body, string messageID, long timestamp);
    }
}
