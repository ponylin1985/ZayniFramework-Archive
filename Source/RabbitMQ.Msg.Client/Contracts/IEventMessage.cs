﻿using System.Collections.Generic;

namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// Message回覆資料的介面
    /// </summary>
    public interface IEventMessage
    {
        /// <summary>
        /// 取得事件名稱
        /// </summary>
        string GetEvent();

        /// <summary>
        /// 取得訊息的ID
        /// </summary>
        string GetMessageID();

        /// <summary>
        /// 取得訊息的時間戳記(毫秒)
        /// </summary>
        long GetTimestamp();

        /// <summary>
        /// 取得訊息格式
        /// </summary>
        string GetContentType();

        /// <summary>
        /// 取得訊息應該要回覆的地址
        /// </summary>
        string GetReplyTo();

        /// <summary>
        /// 取得訊息的關連ID
        /// </summary>
        string GetCorrelationID();

        /// <summary>
        /// 取得主要訊息內容
        /// </summary>
        string GetBody();

        /// <summary>
        /// 認可/處理結束此訊息
        /// </summary>
        void Act();

        /// <summary>
        /// 丟棄/返回此訊息
        /// 請參考RabbitMQListenPolicyAttribute的ReQueue屬性
        /// </summary>
        void NAck();

        /// <summary>
        /// 取得BasicProperties中的Header資訊
        /// </summary>
        object GetHeader(string key);

        /// <summary>
        /// 取得BasicProperties中的Header資訊(全部)
        /// </summary>
        IDictionary<string, object> GetHeaders();
    }
}
