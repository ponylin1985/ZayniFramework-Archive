﻿namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// 監聽結束事件資訊
    /// </summary>
    public class ListenShutdownEventArgs
    {
        /// <summary>
        /// 建構子
        /// </summary>
        public ListenShutdownEventArgs(ListenerCloseCode code, string msg)
        {
            Code = code;
            Message = msg;
        }

        /// <summary>
        /// 該服務回傳的錯誤代碼
        /// </summary>
        public ListenerCloseCode Code { get; set; }

        /// <summary>
        /// 該服務回傳的錯誤訊息
        /// </summary>
        public string Message { get; set; }
    }
}
