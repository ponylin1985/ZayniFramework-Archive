﻿using System;


namespace RabbitMQ.Message.Client
{
    /// <summary>
    /// 產生IServiceEvent實作物件的工廠
    /// </summary>
    public class ServiceEventFactory
    {
        /// <summary>
        /// 透過傳入的型別，建立相對應的IServiceEvent實作物件
        /// </summary>
        public static IServiceEvent Create<T>(T module)
        {
            var type = module.GetType();

            if (type == typeof(RabbitMQModule))
                return new RabbitMQServiceClient(module as RabbitMQModule);

            throw new Exception($"unknow type: {type}.");
        }
    }
}
