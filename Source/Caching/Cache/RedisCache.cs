﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;
using ST = System.Timers;


namespace ZayniFramework.Caching
{
    /// <summary>Redis 遠端資料快取<para/>
    /// <para>* 在 macOS 本地端的 redis docker container 進行測試，對於 syncTimeout 參數值的設置，目前大致上初步結論如下: </para>
    /// <para>  1. 在有執行到 GetCacheKeys 方法，因為有 redis key 值 scan 比對的情況下，syncTimeout 保守至少需要設定到 25 ms 以上。</para>
    /// <para>  2. 如果只有對 redis 進行 set 或 get 的處理，本地端 redis docker container 大約可以在 10 ~ 15 ms 內完成動作而不會有 timeout 問題。</para>
    /// <para>  3. 但在 runtime 環境還需要考量網路 IO 與 redis server 本身 key 數量以及 redis 內 value 的資料內容大小等因素，因此，以上測試結論，只是一個基本的參考。</para>
    /// </summary>
    public sealed class RedisCache
    {
        #region 宣告私有的欄位

        /// <summary>RedisCache 資料快取的 Config 設定
        /// </summary>
        private RedisCacheElement _config;

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>非同步作業鎖定物件: 重新連線的鎖
        /// </summary>
        private readonly AsyncLock _asyncLockConnect = new AsyncLock();

        /// <summary>非同步作業鎖定物件: 鎖定 _message 欄位
        /// </summary>
        private readonly AsyncLock _asyncLockMessage = new AsyncLock();

        /// <summary>訊息
        /// </summary>
        private string _message;

        /// <summary>Redis 快取服務的指令引擎
        /// </summary>
        private RedisEngine _redisEngine;

        /// <summary>Redis Server 服務的連線
        /// </summary>
        private ConnectionMultiplexer _connection;

        /// <summary>Redis Server 服務的資料庫
        /// </summary>
        private IDatabase _database;

        /// <summary>自動重新連線的定時器
        /// </summary>
        private ST.Timer _reconnectTimer;

        /// <summary>資料自動更新的參數集合
        /// </summary>
        private readonly Dictionary<string, DataRefreshProperty> _refreshProperties = new Dictionary<string, DataRefreshProperty>();

        /// <summary>執行 CacheManager.ConnectionBroken 委派的次數<para/>
        /// * 會需要這的欄位的原因是因為，StackExchange.Redis 套件有個 bug，會重覆觸發 ConnectionFailed 兩次... 蠻智障的...
        /// * https://github.com/StackExchange/StackExchange.Redis/issues/794
        /// </summary>
        private int _connectBroken = 0;

        /// <summary>執行 CacheManager.ConnectionRestored 委派的次數<para/>
        /// * 會需要這的欄位的原因是因為，StackExchange.Redis 套件有個 bug，會重覆觸發 ConnectionRestored 兩次... 蠻智障的...
        /// * https://github.com/StackExchange/StackExchange.Redis/issues/794
        /// </summary>
        private int _restoreHandler = 0;

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static RedisCache() => ConfigReader.LoadRedisCacheSettings();

        #endregion 宣告靜態建構子


        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public RedisCache() : this( ConfigReader.GetDefaultRedisCacheConfig() )
        {
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="config">Redis 服務快取的 Config 設定</param>
        public RedisCache( RedisCacheElement config )
        {
            if ( config.IsNull() )
            {
                throw new ArgumentNullException( $"Argument '{nameof ( config )}' can not be null." );
            }

            _config = config;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>訊息
        /// </summary>
        public string Message
        {
            get
            {
                using ( _asyncLockMessage.Lock() )
                {
                    return _message;
                }
            }
            set
            {
                using ( _asyncLockMessage.Lock() )
                {
                    _message = value;
                }
            }
        }

        /// <summary>快取池中目前存放的資料個數
        /// </summary>
        public int Count
        {
            get
            {
                try
                {
                    return _connection.GetServer( $"{_config.Host}:{_config.Port}" ).Keys( _config.DatabaseNo ).Count();
                }
                catch ( Exception ex )
                {
                    Logger.WriteErrorLog( nameof ( RedisCache ), $"Get cache data count from redis server occur exception. {ex.ToString()}", $"{GetLogTraceName( nameof ( Count ) )}" );
                    return -1;
                }
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告內部的方法

        /// <summary>初始化 Redis Server 連線
        /// </summary>
        internal void Initailize() => Connect();

        /// <summary>取得訊息
        /// </summary>
        /// <returns>失敗/錯誤訊息</returns>
        internal string GetMessage() => Message;

        /// <summary>設定訊息
        /// </summary>
        /// <param name="value">失敗/錯誤訊息</param>
        internal void SetMessage( string value ) => Message = value;

        /// <summary>取得快取池中目前存放的資料個數
        /// </summary>
        /// <returns>快取池中目前存放的資料個數</returns>
        internal int GetCount() => Count;

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        // Pony TODO: 如果在 Redis 架構為 master/slave 的模式下，以下建立連線的寫法可能需要做一些調整
        // 參考資料: https://blog.yowko.com/stackexchange-redis-sentinel-cluster/
        /// <summary>建立 Redis Server 服務的連線
        /// </summary>
        private void Connect()
        {
            using ( _asyncLockConnect.Lock() )
            {
                try
                {
                    if ( _connection.IsNotNull() || _database.IsNotNull() )
                    {
                        return;
                    }

                    int keepAliveSeconds = 60 * 30;

                    if (  _config.Password.IsNullOrEmpty() )
                    {
                        _connection = ConnectionMultiplexer.Connect( new ConfigurationOptions()
                        {
                            EndPoints          = { { _config.Host, _config.Port } },
                            KeepAlive          = keepAliveSeconds,
                            DefaultDatabase    = _config.DatabaseNo,
                            AllowAdmin         = true,
                            ConnectTimeout     = _config.ConnectTimeout,
                            SyncTimeout        = _config.SyncTimeout,
                            AbortOnConnectFail = false
                        } );

                        _connection.ConnectionFailed += ( sender, args ) => 
                        {
                            if ( _connectBroken > 0 )
                            {
                                _connectBroken = 0;
                                return;
                            }

                            _connectBroken++;

                            ConsoleLogger.LogError( $"RedisCache connection is broken. RedisCacheName: {_config.Name}, Redis Server: {_config.Host}, Redis Server Port: {_config.Port}." );
                            CacheManager.ConnectionBroken?.Invoke();
                        };

                        _connection.ConnectionRestored += ( sender, args ) => 
                        {
                            if ( _restoreHandler > 0 )
                            {
                                _restoreHandler = 0;
                                return;
                            }

                            _restoreHandler++;

                            SpinWait.SpinUntil( () => false, 350 );
                            ConsoleLogger.Log( $"RedisCache connection is restored. RedisCacheName: {_config.Name}, Redis Server: {_config.Host}, Redis Server Port: {_config.Port}.", ConsoleColor.Magenta );
                            CacheManager.ConnectionRestored?.Invoke();
                        };

                        _database    = _connection.GetDatabase( _config.DatabaseNo );
                        _redisEngine = new RedisEngine( _config.Name );
                        return;
                    }

                    _connection = ConnectionMultiplexer.Connect( new ConfigurationOptions()
                    {
                        EndPoints          = { { _config.Host, _config.Port } },
                        Password           = _config.Password,
                        KeepAlive          = keepAliveSeconds,
                        DefaultDatabase    = _config.DatabaseNo,
                        AllowAdmin         = true,
                        ConnectTimeout     = _config.ConnectTimeout,
                        SyncTimeout        = _config.SyncTimeout,
                        AbortOnConnectFail = false
                    } );

                    _database    = _connection.GetDatabase( _config.DatabaseNo );
                    _redisEngine = new RedisEngine( _config.Name );

                    _reconnectTimer.IsNotNull( t => StopReconnectTimer() );

                    _connection.ConnectionFailed += ( sender, args ) => 
                    {
                        if ( _connectBroken > 0 )
                        {
                            _connectBroken = 0;
                            return;
                        }

                        _connectBroken++;

                        ConsoleLogger.LogError( $"RedisCache connection is broken. RedisCacheName: {_config.Name}, Redis Server: {_config.Host}, Redis Server Port: {_config.Port}." );
                        CacheManager.ConnectionBroken?.Invoke();
                    };

                    _connection.ConnectionRestored += ( sender, args ) => 
                    {
                        if ( _restoreHandler > 0 )
                        {
                            _restoreHandler = 0;
                            return;
                        }

                        _restoreHandler++;

                        SpinWait.SpinUntil( () => false, 350 );
                        ConsoleLogger.Log( $"RedisCache connection is restored. RedisCacheName: {_config.Name}, Redis Server: {_config.Host}, Redis Server Port: {_config.Port}.", ConsoleColor.Magenta );
                        CacheManager.ConnectionRestored?.Invoke();
                    };

                    Logger.WriteInformationLog( this, $"RedisCache '{_config.Name}' establish connection with redis server ({_config.Host}:{_config.Port}) successfully.", GetLogTraceName( nameof ( Connect ) ) );
                }
                catch ( Exception ex )
                {
                    _connection  = null;
                    _database    = null;
                    _redisEngine = null;
                    Logger.WriteErrorLog( this, $"RedisCache '{_config.Name}' connect to redis server occur exception. {ex.ToString()}", $"{GetLogTraceName( nameof ( Connect ) )}" );

                    // 我發現: 好像只要在對 Redis Server 進行連線時，ConfigurationOptions.AbortOnConnectFail 設定為 false
                    // ConnectionMultiplexer 連線物件就還是可以正常建立，而且 StackExchange.Redis 內部還會自行 Auto-Reconnect
                    // 連線的只是 IsConnected 會是 false，因此就不太需要自行在實作重新連線了...
                    // StartReconnectTimer();
                }
            }
        }

        /// <summary>啟動重新連線的定時器
        /// </summary>
        private void StartReconnectTimer()
        {
            _reconnectTimer          = new ST.Timer();
            _reconnectTimer.Interval = TimeSpan.FromSeconds( 3 ).TotalSeconds;
            _reconnectTimer.Elapsed += Reconnect;
            _reconnectTimer.Start();
            Logger.WriteWarningLog( this, $"RedisCache '{_config.Name}' is starting retry connect to redis server ({_config.Host}:{_config.Port}).", GetLogTraceName( nameof ( StartReconnectTimer ) ) );
        }

        /// <summary>關閉重新連線的定時器
        /// </summary>
        private void StopReconnectTimer()
        {
            _reconnectTimer.Stop();
            _reconnectTimer.Elapsed -= Reconnect;
            _reconnectTimer.Dispose();
            _reconnectTimer = null;
        }

        /// <summary>重新對 Redis Server 進行連線
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reconnect( object sender, ST.ElapsedEventArgs e ) => Connect();

        /// <summary>檢查 Redis Server 的連線狀態
        /// </summary>
        /// <returns>目前的連線是否正常</returns>
        private bool CheckConnectionStatus()
        {
            if ( _connection.IsNull() || _database.IsNull() )
            {
                return false;
            }

            return _connection.IsConnected;
        }

        /// <summary>執行快取資料更新
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        private async void ExecuteRefreshDataAsync( string cacheId )
        {
            if ( !_refreshProperties.TryGetValue( cacheId, out DataRefreshProperty refreshProperty ) || refreshProperty.IsNull() )
            {
                return;
            }

            using ( await refreshProperty.InternalAsyncLock.LockAsync() )
            {
                #region 從 Redis Server 取得快取資料

                CacheProperty cacheProperty;

                var reqStringGet = new RedisCommandArgs()
                {
                    CacheKey = cacheId,
                    Action   = $"{nameof ( _database.StringGetAsync )} RefreshTimer Get",
                    Data     = cacheId
                };

                var g = await _redisEngine.ExecuteAsync<CacheProperty>( reqStringGet, async args => 
                {
                    string json = await _database.StringGetAsync( args.CacheKey );
                    return JsonConvertUtil.DeserializeFromCamelCase<CacheProperty>( json );
                } );

                if ( !g.Success )
                {
                    Logger.WriteErrorLog( this, $"RedisCache '{_config.Name}' timer get data from redis server fail. CacheId: {cacheId}.", $"{GetLogTraceName( nameof ( ExecuteRefreshDataAsync ))}" );
                    return;
                }

                cacheProperty = g.Data;

                if ( cacheProperty.IsNull() )
                {
                    Logger.WriteErrorLog( this, $"RedisCache '{_config.Name}' timer retrive null value from redis server. CacheId: {cacheId}.", $"{GetLogTraceName( nameof ( ExecuteRefreshDataAsync ))}" );
                    return;
                }

                #endregion 從 Redis Server 取得快取資料

                #region 執行資料更新委派

                object data = null;

                try
                {
                    data = refreshProperty.RefreshHandler();
                }
                catch ( Exception ex )
                {
                    Logger.WriteErrorLog( this, $"RedisCache '{_config.Name}' timer execute refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}", $"{GetLogTraceName( nameof ( ExecuteRefreshDataAsync ))}" );
                    return;
                }

                if ( data.IsNull() )
                {
                    Logger.WriteWarningLog( this, $"RedisCache '{_config.Name}' timer retrive null from refresh handler result. No update to redis server. CacheId: {cacheId}.", $"{GetLogTraceName( nameof ( ExecuteRefreshDataAsync ))}" );
                    return;
                }

                cacheProperty.Data        = data;
                cacheProperty.DataId      = RandomTextHelper.Create( 36 );
                cacheProperty.RefreshTime = DateTime.Now;

                #endregion 執行資料更新委派

                #region 執行對 Redis Server 進行資料更新

                try
                {
                    string cacheJson = JsonConvertUtil.SerializeInCamelCaseNoFormatting( cacheProperty );

                    if ( cacheJson.IsNullOrEmpty() )
                    {
                        Logger.WriteErrorLog( this, $"RedisCache '{_config.Name}' timer serialize {nameof ( CacheProperty )} fail. Can not put to redis server. CacheId: {cacheId}.", $"{GetLogTraceName( nameof ( ExecuteRefreshDataAsync ))}" );
                        return;
                    }

                    var reqStringSet = new RedisCommandArgs()
                    {
                        CacheKey = cacheId,
                        Action   = $"{nameof ( _database.StringSetAsync )} RefreshTimer Update",
                        Data     = cacheJson
                    };

                    var r = await _redisEngine.ExecuteAsync<bool>( reqStringSet, async args => await _database.StringSetAsync( cacheId, cacheJson, when: When.Exists ) );

                    if ( !r.Success )
                    {
                        Logger.WriteErrorLog( this, $"RedisCache '{_config.Name}' timer try update data to redis server fail. CacheId: {cacheId}.", $"{GetLogTraceName( nameof ( ExecuteRefreshDataAsync ))}" );
                        return;
                    }

                    bool success = r.Data;

                    if ( !success )
                    {
                        Logger.WriteErrorLog( this, $"RedisCache '{_config.Name}' timer try update data to redis server fail. CacheId: {cacheId}.", $"{GetLogTraceName( nameof ( ExecuteRefreshDataAsync ))}" );
                        return;
                    }
                }
                catch ( Exception ex )
                {
                    Logger.WriteErrorLog( this, $"RedisCache '{_config.Name}' timer update data to redis server occur exception. CacheId: {cacheId}. {ex.ToString()}", $"{GetLogTraceName( nameof ( ExecuteRefreshDataAsync ))}" );
                    return;
                }

                #endregion 執行對 Redis Server 進行資料更新
            }
        }

        #endregion 宣告私有的方法


        #region 宣告公開的方法

        /// <summary>取得快取倉儲中的 Key 值集合
        /// </summary>
        /// <param name="pattern">Key 值的規則</param>
        /// <returns>取得結果</returns>
        public Result<List<string>> GetCacheKeys( string pattern = null )
        {
            #region 初始化回傳值

            var result = Result.Create<List<string>>();
            List<string> cacheKeys = null;

            #endregion 初始化回傳值

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                result.Message = $"The Redis connection is broken. Redis server --> ({_config.Host}:{_config.Port}).";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( GetCacheKeys ) ) );
                return result;
            }

            #endregion 檢查連線狀態

            #region 從 Redis Server 取得快取的 Keys 值

            try
            {
                var server = _connection.GetServer( $"{_config.Host}:{_config.Port}" );

                if ( pattern.IsNullOrEmpty() )
                {
                    cacheKeys = server.Keys( _config.DatabaseNo ).Select( k => k.ToString() ).ToList();
                }
                else
                {
                    cacheKeys = server.Keys( _config.DatabaseNo, pattern: $"{pattern}*" ).Select( k => k.ToString() ).ToList();
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Get match cache keys from redis server occur exception. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, "Get match cache keys from redis server occur exception." );
                return result;
            }

            #endregion 從 Redis Server 取得快取的 Keys 值

            #region 設定回傳值

            result.Data    = cacheKeys;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>取得指定 CacheId 識別碼的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <param name="handler">重新從資料來源取得資料的委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間 (單位為分鐘)</param>
        /// <returns>取得資料結果</returns>
        public GetCacheResult Get( string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult()
            {
                Success   = false,
                CacheData = null
            };

            #endregion 初始化回傳值

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                result.Message = $"The Redis connection is broken. Redis server --> ({_config.Host}:{_config.Port}).";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Get ) ) );
                return result;
            }

            #endregion 檢查連線狀態

            #region 檢查傳入的快取 ID 參數

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"{nameof ( Get )}, argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( RedisCache ), result.Message, GetLogTraceName( nameof ( Get ) ) );
                return result;
            }

            #endregion 檢查傳入的快取 ID 參數

            #region 從 Redis Server 快取中取得目標資料

            CacheProperty cacheProperty = null;

            try
            {
                var req = new RedisCommandArgs()
                {
                    CacheKey = cacheId,
                    Action   = nameof ( _database.StringGet ),
                    Data     = cacheId
                };

                var r = _redisEngine.Execute<CacheProperty>( req, args => 
                {
                    var json = _database.StringGet( args.CacheKey );
                    return json.HasValue && json.IsNotNull() ? JsonConvertUtil.DeserializeFromCamelCase<CacheProperty>( json ) : null;
                } );

                if ( !r.Success )
                {
                    result.Message = r.Message;
                    return result;
                }

                cacheProperty = r.Data;

                #region 重新檢查從 Redis 取得到的 CacheProperty 資料是否為 Null

                if ( cacheProperty.IsNull() )
                {
                    if ( handler.IsNotNull() )
                    {
                        #region 執行資料來源取得資料的回呼函式

                        if ( !CachingHandlerExecutor.ExecuteGetSomethingCallback( handler, out object rawData, out string message ) )
                        {
                            result.Message = $"Get data from redis cache occur error. No data found. CacheId: {cacheId}. Execute GetSomethingFromDataSource callback occur error.";
                            Logger.WriteErrorLog( this, $"{result.Message} {message}", $"{GetLogTraceName( nameof ( Get ) )}" );
                            return result;
                        }

                        if ( rawData.IsNull() )
                        {
                            result.Message = $"Get data from redis cache occur error. No data found. CacheId: {cacheId}. Execute GetSomethingFromDataSource callback success but retrive no raw data.";
                            Logger.WriteErrorLog( this, $"{result.Message} {message}", $"{GetLogTraceName( nameof ( Get ) )}" );
                            return result;
                        }

                        var cacheProp = new CacheProperty()
                        {
                            CacheId        = cacheId,
                            Data           = rawData,
                            ExpireInterval = timeoutMinuntes
                        };

                        // 如果重新向資料來源取得資料成功，就自動把資料存放到記憶體快取池中
                        if ( Put( cacheProp, out string dataId, out DateTime refreshTime ) )
                        {
                            result.DataId      = dataId;
                            result.CacheData   = rawData;
                            result.RefreshTime = refreshTime;
                            result.Success     = true;
                            return result;
                        }

                        result.Message = $"No data found from redis server but get from the data source success. Put raw data into redis redis occur error. CacheId: {cacheId}.";
                        Logger.WriteWarningLog( this, result.Message, $"{GetLogTraceName( nameof ( Get ) )}" );

                        result.CacheData = rawData;
                        result.Success   = true;
                        return result;

                        #endregion 執行資料來源取得資料的回呼函式
                    }
                    else
                    {
                        #region 回傳從 Redis 快取中取得資料失敗

                        result.Message = $"Get data from redis cache occur error. No data found from redis cache. CacheId: {cacheId}.";
                        Logger.WriteWarningLog( this, result.Message, $"{GetLogTraceName( nameof ( Get ) )}" );
                        return result;

                        #endregion 回傳從 Redis 快取中取得資料失敗
                    }
                }

                #endregion 重新檢查從 Redis 取得到的 CacheProperty 資料是否為 Null
            }
            catch ( Exception ex )
            {
                result.Message = $"Get data from redis server occur exception. CacheId: {cacheId}. {ex.ToString()}";
                Logger.WriteErrorLog( this, $"{result.Message} {ex.ToString()}", $"{GetLogTraceName( nameof ( Get ) )}" );
                return result;
            }

            #endregion 從 Redis Server 快取中取得目標資料

            #region 設定回傳值屬性

            result.DataId      = cacheProperty.DataId;
            result.CacheData   = cacheProperty.Data;
            result.RefreshTime = cacheProperty.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值屬性
            
            return result;
        }

        /// <summary>取得指定 CacheId 識別碼的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <param name="handler">重新從資料來源取得資料的委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間 (單位為分鐘)</param>
        /// <returns>取得資料結果</returns>
        public async Task<GetCacheResult> GetAsync( string cacheId, GetSomethingFromDataSourceAsync handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult()
            {
                Success   = false,
                CacheData = null
            };

            #endregion 初始化回傳值

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                result.Message = $"The Redis connection is broken. Redis server --> ({_config.Host}:{_config.Port}).";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( GetAsync ) ) );
                return result;
            }

            #endregion 檢查連線狀態

            #region 檢查傳入的快取 ID 參數

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"{nameof ( GetAsync )}, argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( RedisCache ), result.Message, GetLogTraceName( nameof ( GetAsync ) ) );
                return result;
            }

            #endregion 檢查傳入的快取 ID 參數

            #region 從 Redis Server 快取中取得目標資料

            CacheProperty cacheProperty = null;

            try
            {
                var req = new RedisCommandArgs()
                {
                    CacheKey = cacheId,
                    Action   = nameof ( _database.StringGetAsync ),
                    Data     = cacheId
                };

                var r = await _redisEngine.ExecuteAsync<CacheProperty>( req, async args => 
                {
                    var json = await _database.StringGetAsync( args.CacheKey );
                    return json.HasValue && json.IsNotNull() ? JsonConvertUtil.DeserializeFromCamelCase<CacheProperty>( json ) : null;
                } );

                if ( !r.Success )
                {
                    result.Message = r.Message;
                    return result;
                }

                cacheProperty = r.Data;

                #region 重新檢查從 Redis 取得到的 CacheProperty 資料是否為 Null

                if ( cacheProperty.IsNull() )
                {
                    if ( handler.IsNotNull() )
                    {
                        #region 執行資料來源取得資料的回呼函式

                        var g = await CachingHandlerExecutor.ExecuteGetSomethingCallbackAsync( handler );

                        if ( !g.Success )
                        {
                            result.Message = $"Get data from redis cache occur error. No data found. CacheId: {cacheId}. Execute GetSomethingFromDataSourceAsync callback occur error.";
                            Logger.WriteErrorLog( this, $"{result.Message} {g.Message}", $"{GetLogTraceName( nameof ( GetAsync ) )}" );
                            return result;
                        }

                        var rawData = g.Data;

                        if ( rawData.IsNull() )
                        {
                            result.Message = $"Get data from redis cache occur error. No data found. CacheId: {cacheId}. Execute GetSomethingFromDataSourceAsync callback success but retrive no raw data.";
                            Logger.WriteErrorLog( this, $"{result.Message} {g.Message}", $"{GetLogTraceName( nameof ( GetAsync ) )}" );
                            return result;
                        }

                        var cacheProp = new CacheProperty()
                        {
                            CacheId        = cacheId,
                            Data           = rawData,
                            ExpireInterval = timeoutMinuntes
                        };

                        // 如果重新向資料來源取得資料成功，就自動把資料存放到記憶體快取池中
                        var p = await PutDataAsync( cacheProp );

                        if ( p.success )
                        {
                            result.DataId      = p.dataId;
                            result.CacheData   = rawData;
                            result.RefreshTime = p.refreshTime;
                            result.Success     = true;
                            return result;
                        }

                        result.Message = $"No data found from redis server but get from the data source success. Put raw data into redis redis occur error. CacheId: {cacheId}.";
                        Logger.WriteWarningLog( this, result.Message, $"{GetLogTraceName( nameof ( GetAsync ) )}" );

                        result.CacheData = rawData;
                        result.Success   = true;
                        return result;

                        #endregion 執行資料來源取得資料的回呼函式
                    }
                    else
                    {
                        #region 回傳從 Redis 快取中取得資料失敗

                        result.Message = $"Get data from redis cache occur error. No data found from redis cache. CacheId: {cacheId}.";
                        Logger.WriteWarningLog( this, result.Message, $"{GetLogTraceName( nameof ( GetAsync ) )}" );
                        return result;

                        #endregion 回傳從 Redis 快取中取得資料失敗
                    }
                }

                #endregion 重新檢查從 Redis 取得到的 CacheProperty 資料是否為 Null
            }
            catch ( Exception ex )
            {
                result.Message = $"Get data from redis server occur exception. CacheId: {cacheId}. {ex.ToString()}";
                Logger.WriteErrorLog( this, $"{result.Message} {ex.ToString()}", $"{GetLogTraceName( nameof ( GetAsync ) )}" );
                return result;
            }

            #endregion 從 Redis Server 快取中取得目標資料

            #region 設定回傳值屬性

            result.DataId      = cacheProperty.DataId;
            result.CacheData   = cacheProperty.Data;
            result.RefreshTime = cacheProperty.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值屬性
            
            return result;
        }

        /// <summary>取得指定 CacheId 識別碼的快取資料
        /// </summary>
        /// <typeparam name="TData">資料載體的泛型</typeparam>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <param name="handler">重新從資料來源取得資料的委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間 (單位為分鐘)</param>
        /// <returns>取得資料結果</returns>
        public GetCacheResult<TData> Get<TData>( string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult<TData>() 
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                result.Message = $"The Redis connection is broken. Redis server --> ({_config.Host}:{_config.Port}).";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( Get ) ) );
                return result;
            }

            #endregion 檢查連線狀態

            #region 取得 Redis Server 中的目標資料

            GetCacheResult g = Get( cacheId, handler, timeoutMinuntes );

            if ( !g.Success )
            {
                Message        = g.Message;
                result.Message = g.Message;
                return result;
            }

            #endregion 取得 Redis Server 中的目標資料

            #region 對目標資料進行轉型處理

            TData data = default ( TData );

            try
            {
                switch ( g.CacheData )
                {
                    case JToken json:
                        data = json.ToObject<TData>();
                        break;

                    default:
                        data = (TData)g.CacheData;
                        break;
                }
            }
            catch ( JsonSerializationException jsonException )
            {
                try
                {
                    JToken jToken = g.CacheData as JToken;

                    if ( jToken.IsNull() )
                    {
                        result.Success   = false;
                        result.CacheData = default ( TData );
                        result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                        Logger.WriteExceptionLog( this, jsonException, result.Message );
                        return result;
                    }

                    data = JsonConvert.DeserializeObject<TData>( jToken.ToString() );
                }
                catch ( Exception ex )
                {
                    result.Success   = false;
                    result.CacheData = default ( TData );
                    result.Message   = $"Deserialize Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}{Environment.NewLine}{g.CacheData.ToString()}";
                    Logger.WriteExceptionLog( this, ex, result.Message );
                    return result;
                }

                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                Logger.WriteExceptionLog( this, jsonException, result.Message );
                return result;
            }
            catch ( InvalidCastException castException ) 
            {
                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting data object occur exception. CacheId: {cacheId}. {castException.ToString()}";
                Logger.WriteExceptionLog( this, castException, result.Message );
                return result;
            }
            catch ( Exception ex )
            {
                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Get cache data occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, result.Message );
                return result;
            }

            #endregion 對目標資料進行轉型處理

            #region 設定回傳值屬性

            result.CacheData   = data;
            result.DataId      = g.DataId;
            result.RefreshTime = g.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>取得指定 CacheId 識別碼的快取資料
        /// </summary>
        /// <typeparam name="TData">資料載體的泛型</typeparam>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <param name="handler">重新從資料來源取得資料的委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間 (單位為分鐘)</param>
        /// <returns>取得資料結果</returns>
        public async Task<GetCacheResult<TData>> GetAsync<TData>( string cacheId, GetSomethingFromDataSourceAsync handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult<TData>() 
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                result.Message = $"The Redis connection is broken. Redis server --> ({_config.Host}:{_config.Port}).";
                Logger.WriteErrorLog( this, result.Message, GetLogTraceName( nameof ( GetAsync ) ) );
                return result;
            }

            #endregion 檢查連線狀態

            #region 取得 Redis Server 中的目標資料

            GetCacheResult g = await GetAsync( cacheId, handler, timeoutMinuntes );

            if ( !g.Success )
            {
                Message        = g.Message;
                result.Message = g.Message;
                return result;
            }

            #endregion 取得 Redis Server 中的目標資料

            #region 對目標資料進行轉型處理

            TData data = default ( TData );

            try
            {
                switch ( g.CacheData )
                {
                    case JToken json:
                        data = json.ToObject<TData>();
                        break;

                    default:
                        data = (TData)g.CacheData;
                        break;
                }
            }
            catch ( JsonSerializationException jsonException )
            {
                try
                {
                    JToken jToken = g.CacheData as JToken;

                    if ( jToken.IsNull() )
                    {
                        result.Success   = false;
                        result.CacheData = default ( TData );
                        result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                        Logger.WriteExceptionLog( this, jsonException, result.Message );
                        return result;
                    }

                    data = JsonConvert.DeserializeObject<TData>( jToken.ToString() );
                }
                catch ( Exception ex )
                {
                    result.Success   = false;
                    result.CacheData = default ( TData );
                    result.Message   = $"Deserialize Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}{Environment.NewLine}{g.CacheData.ToString()}";
                    Logger.WriteExceptionLog( this, ex, result.Message );
                    return result;
                }

                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                Logger.WriteExceptionLog( this, jsonException, result.Message );
                return result;
            }
            catch ( InvalidCastException castException ) 
            {
                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting data object occur exception. CacheId: {cacheId}. {castException.ToString()}";
                Logger.WriteExceptionLog( this, castException, result.Message );
                return result;
            }
            catch ( Exception ex )
            {
                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Get cache data occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, result.Message );
                return result;
            }

            #endregion 對目標資料進行轉型處理

            #region 設定回傳值屬性

            result.CacheData   = data;
            result.DataId      = g.DataId;
            result.RefreshTime = g.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>將資料放入 Redis Server 快取中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入 Redis Server 快取中</returns>
        public bool Put( CacheProperty cacheProperty ) => Put( cacheProperty, out string dataId, out DateTime refreshTime );

        /// <summary>將資料放入 Redis Server 快取中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入 Redis Server 快取中</returns>
        public async Task<bool> PutAsync( CacheProperty cacheProperty ) => ( await PutDataAsync( cacheProperty ) ).success;

        /// <summary>將資料放入 Redis Server 快取中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <param name="dataId">資料異動識別 ID 代碼，每次更新後資料後改變。</param>
        /// <param name="refreshTime">資料最後更新時間 (資料新增或更新至快取倉儲時的時間戳記)</param>
        /// <returns>是否成功將資料放入 Redis Server 快取中</returns>
        public bool Put( CacheProperty cacheProperty, out string dataId, out DateTime refreshTime )
        {
            dataId      = null;
            refreshTime = default ( DateTime );

            if ( cacheProperty.IsNull() )
            {
                Message = $"Augument '{nameof ( cacheProperty )}' can not be null.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( Put ) ) );
                return false;
            }

            return SetDataToRemote( nameof ( Put ), cacheProperty, out dataId, out refreshTime );
        }

        /// <summary>將資料放入 Redis Server 快取中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入 Redis Server 快取中</returns>
        public async Task<( bool success, string dataId, DateTime refreshTime )> PutDataAsync( CacheProperty cacheProperty )
        {
            if ( cacheProperty.IsNull() )
            {
                Message = $"Augument '{nameof ( cacheProperty )}' can not be null.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( PutAsync ) ) );
                return ( false, null, default( DateTime ) );
            }

            return await SetDataToRemoteAsync( nameof ( PutAsync ), cacheProperty );
        }

        /// <summary>更新 Redis Server 中的快取資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料更新至 Redis Server 中</returns>
        public bool Update( CacheProperty cacheProperty ) => Update( cacheProperty, out string dataId, out DateTime refreshTime );

        /// <summary>更新 Redis Server 中的快取資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料更新至 Redis Server 中</returns>
        public async Task<bool> UpdateAsync( CacheProperty cacheProperty ) => ( await UpdateDataAsync( cacheProperty ) ).success;

        /// <summary>更新 Redis Server 中的快取資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <param name="dataId">資料異動識別 ID 代碼，每次更新後資料後改變。</param>
        /// <param name="refreshTime">資料最後更新時間 (資料新增或更新至快取倉儲時的時間戳記)</param>
        /// <returns>是否成功將資料更新至 Redis Server 中</returns>
        public bool Update( CacheProperty cacheProperty, out string dataId, out DateTime refreshTime )
        {
            dataId      = null;
            refreshTime = default ( DateTime );

            if ( cacheProperty.IsNull() )
            {
                Message = $"Augument '{nameof ( cacheProperty )}' can not be null.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( Update ) ) );
                return false;
            }

            return SetDataToRemote( nameof ( Update ), cacheProperty, out dataId, out refreshTime );
        }

        /// <summary>更新 Redis Server 中的快取資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料更新至 Redis Server 中</returns>
        public async Task<( bool success, string dataId, DateTime refreshTime )> UpdateDataAsync( CacheProperty cacheProperty )
        {
            var dataId      = default ( String );
            var refreshTime = default ( DateTime );

            if ( cacheProperty.IsNull() )
            {
                Message = $"Augument '{nameof ( cacheProperty )}' can not be null.";
                Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( UpdateAsync ) ) );
                return ( false, dataId, refreshTime );
            }

            return await SetDataToRemoteAsync( nameof ( UpdateAsync ), cacheProperty );
        }

        /// <summary>移除 Redis Server 中的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <returns>移除資料是否成功</returns>
        public bool Remove( string cacheId )
        {
            #region 檢查傳入的快取 ID 值

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Augument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( Remove ) )}" );
                return false;
            }

            #endregion 檢查傳入的快取 ID 值

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                Message = $"The Redis connection is broken, can not remove cache data. Redis server --> ({_config.Host}:{_config.Port}).";
                Logger.WriteErrorLog( this, $"The Redis connection is broken.", GetLogTraceName( nameof ( Remove ) ) );
                return false;
            }

            #endregion 檢查連線狀態

            #region 將目標資料從 Redis 快取服務中移除

            using ( _asyncLock.Lock() ) 
            {
                bool success = false;
                
                try
                {
                    var req = new RedisCommandArgs()
                    {
                        CacheKey = cacheId,
                        Action   = $"{nameof ( _database.KeyDeleteAsync )}",
                        Data     = cacheId
                    };

                    var r = _redisEngine.Execute<bool>( req, args => _database.KeyDelete( cacheId ) );

                    if ( !r.Success )
                    {
                        Message = r.Message;
                        return false;
                    }

                    success = r.Data;
                }
                catch ( Exception ex )
                {
                    Message = $"Remove cache data from redis server occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( Remove ) )}" );
                    return false;
                }

                if ( !success )
                {
                    Message = $"Remove data to redis server fail. CacheId: {cacheId}.";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( Remove ) )}" );
                    return false;
                }

                #endregion 將目標資料從 Redis 快取服務中移除

                #region 移除 RefreshCacheDataHandler 處理委派

                try
                {
                    // 並不是所有的快取資料都有要自動更新機制!
                    if ( !_refreshProperties.TryGetValue( cacheId, out var refreshProperty ) )
                    {
                        return true;
                    }

                    if ( refreshProperty.IsNull() )
                    {
                        return true;
                    }

                    refreshProperty.RefreshTimer.Stop();
                    refreshProperty.RefreshTimer.Dispose();

                    refreshProperty.CacheId        = null;
                    refreshProperty.RefreshHandler = null;
                    refreshProperty.RefreshTimer   = null;
                }
                catch ( Exception ex )
                {
                    Message = $"Remove and dispose refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( Remove ) )}" );
                    return true;
                }

                #endregion 移除 RefreshCacheDataHandler 處理委派
            }

            return true;
        }

        /// <summary>移除 Redis Server 中的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <returns>移除資料是否成功</returns>
        public async Task<bool> RemoveAsync( string cacheId )
        {
            #region 檢查傳入的快取 ID 值

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Augument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( RemoveAsync ) )}" );
                return false;
            }

            #endregion 檢查傳入的快取 ID 值

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                Message = $"The Redis connection is broken, can not remove cache data. Redis server --> ({_config.Host}:{_config.Port}).";
                Logger.WriteErrorLog( this, $"The Redis connection is broken.", GetLogTraceName( nameof ( RemoveAsync ) ) );
                return false;
            }

            #endregion 檢查連線狀態

            #region 將目標資料從 Redis 快取服務中移除

            using ( await _asyncLock.LockAsync() )
            {
                bool success = false;
            
                try
                {
                    var req = new RedisCommandArgs()
                    {
                        CacheKey = cacheId,
                        Action   = $"{nameof ( _database.KeyDeleteAsync )}",
                        Data     = cacheId
                    };

                    var r = await _redisEngine.ExecuteAsync<bool>( req, async args => await _database.KeyDeleteAsync( cacheId ) );

                    if ( !r.Success )
                    {
                        Message = r.Message;
                        return false;
                    }

                    success = r.Data;
                }
                catch ( Exception ex )
                {
                    Message = $"Remove cache data from redis server occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( RemoveAsync ) )}" );
                    return false;
                }

                if ( !success )
                {
                    Message = $"Remove data to redis server fail. CacheId: {cacheId}.";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( RemoveAsync ) )}" );
                    return false;
                }

                #endregion 將目標資料從 Redis 快取服務中移除

                #region 移除 RefreshCacheDataHandler 處理委派

                try
                {
                    // 並不是所有的快取資料都有要自動更新機制!
                    if ( !_refreshProperties.TryGetValue( cacheId, out var refreshProperty ) )
                    {
                        return true;
                    }

                    if ( refreshProperty.IsNull() )
                    {
                        return true;
                    }

                    refreshProperty.RefreshTimer.Stop();
                    refreshProperty.RefreshTimer.Dispose();

                    refreshProperty.CacheId        = null;
                    refreshProperty.RefreshHandler = null;
                    refreshProperty.RefreshTimer   = null;
                }
                catch ( Exception ex )
                {
                    Message = $"Remove and dispose refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( RemoveAsync ) )}" );
                    return true;
                }

                #endregion 移除 RefreshCacheDataHandler 處理委派
            }

            return true;
        }

        /// <summary>檢查是否存在指定資料識別碼的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <returns>資料是否存在</returns>
        public bool Contains( string cacheId )
        {
            #region 檢查傳入的快取 ID 值

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Augument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( Contains ) )}" );
                return false;
            }

            #endregion 檢查傳入的快取 ID 值

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                Message = $"The Redis connection is broken, can not check cache key dose exist. Redis server --> ({_config.Host}:{_config.Port}).";
                Logger.WriteErrorLog( this, $"The Redis connection is broken, can not check cache key dose exist.", GetLogTraceName( nameof ( Contains ) ) );
                return false;
            }

            #endregion 檢查連線狀態

            #region 檢查 Redis 快取服務中使否包含指定 CacheId 的目標資料

            bool contains = false;

            try
            {
                var req = new RedisCommandArgs()
                {
                    CacheKey = cacheId,
                    Action   = $"{nameof ( _database.KeyExists )}",
                    Data     = cacheId
                };

                var r = _redisEngine.Execute<bool>( req, args => _database.KeyExists( cacheId ) );

                if ( !r.Success )
                {
                    Message = r.Message;
                    return false;
                }

                contains = r.Data;
            }
            catch ( Exception ex )
            {
                Message = $"Check exist cache data from redis server occur exception. CacheId: {cacheId}. {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( Contains ) )}" );
                return false;
            }

            #endregion 檢查 Redis 快取服務中使否包含指定 CacheId 的目標資料

            return contains;
        }

        /// <summary>檢查是否存在指定資料識別碼的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        /// <returns>資料是否存在</returns>
        public async Task<bool> ContainsAsync( string cacheId )
        {
            #region 檢查傳入的快取 ID 值

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Augument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( ContainsAsync ) )}" );
                return false;
            }

            #endregion 檢查傳入的快取 ID 值

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                Message = $"The Redis connection is broken, can not check cache key dose exist. Redis server --> ({_config.Host}:{_config.Port}).";
                Logger.WriteErrorLog( this, $"The Redis connection is broken, can not check cache key dose exist.", GetLogTraceName( nameof ( ContainsAsync ) ) );
                return false;
            }

            #endregion 檢查連線狀態

            #region 檢查 Redis 快取服務中使否包含指定 CacheId 的目標資料

            bool contains = false;

            try
            {
                var req = new RedisCommandArgs()
                {
                    CacheKey = cacheId,
                    Action   = $"{nameof ( _database.KeyExistsAsync )}",
                    Data     = cacheId
                };

                var r = await _redisEngine.ExecuteAsync<bool>( req, async args => await _database.KeyExistsAsync( cacheId ) );

                if ( !r.Success )
                {
                    Message = r.Message;
                    return false;
                }

                contains = r.Data;
            }
            catch ( Exception ex )
            {
                Message = $"Check exist cache data from redis server occur exception. CacheId: {cacheId}. {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( ContainsAsync ) )}" );
                return false;
            }

            #endregion 檢查 Redis 快取服務中使否包含指定 CacheId 的目標資料

            return contains;
        }

        /// <summary>清空 Redis Server 上所有的快取資料
        /// </summary>
        /// <returns>是否成功清除所有資料</returns>
        public bool Clear()
        {
            using ( _asyncLock.Lock() )
            {
                if ( !CheckConnectionStatus() )
                {
                    Message = $"The Redis connection is broken, can not clear cache data. Redis server --> ({_config.Host}:{_config.Port}).";
                    Logger.WriteErrorLog( this, $"The Redis connection is broken, can not clear cache data.", GetLogTraceName( nameof ( Clear ) ) );
                    return false;
                }

                var req = new RedisCommandArgs()
                {
                    CacheKey = "ClearDatabase",
                    Action   = "FlushDatabase"
                };

                var r = _redisEngine.Execute<bool>( req, args => 
                {
                    try
                    {
                        _connection.GetServer( $"{_config.Host}:{_config.Port}" ).FlushDatabase( _config.DatabaseNo );
                    }
                    catch ( Exception ex )
                    {
                        Logger.WriteErrorLog( this, $"Clear all data from redis server occur exception. {ex.ToString()}", $"{GetLogTraceName( nameof ( Clear ) )}" );
                        return false;
                    }

                    return true;
                } );
            }
            
            return true;
        }

        /// <summary>清空 Redis Server 上所有的快取資料
        /// </summary>
        /// <returns>是否成功清除所有資料</returns>
        public async Task<bool> ClearAsync()
        {
            using ( await _asyncLock.LockAsync() )
            {
                if ( !CheckConnectionStatus() )
                {
                    Message = $"The Redis connection is broken, can not clear cache data. Redis server --> ({_config.Host}:{_config.Port}).";
                    Logger.WriteErrorLog( this, $"The Redis connection is broken, can not clear cache data.", GetLogTraceName( nameof ( ClearAsync ) ) );
                    return false;
                }

                var req = new RedisCommandArgs()
                {
                    CacheKey = "ClearDatabase",
                    Action   = "FlushDatabase"
                };

                var r = await _redisEngine.ExecuteAsync<bool>( req, async args => 
                {
                    try
                    {
                        await _connection.GetServer( $"{_config.Host}:{_config.Port}" ).FlushDatabaseAsync( _config.DatabaseNo );
                    }
                    catch ( Exception ex )
                    {
                        Logger.WriteErrorLog( this, $"Clear all data from redis server occur exception. {ex.ToString()}", $"{GetLogTraceName( nameof ( ClearAsync ) )}" );
                        return false;
                    }

                    return true;
                } );
            }
            
            return true;
        }

        /// <summary>取得 HashProperty 資料結構的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>取得資料結果</returns>
        public GetHashCacheResult GetHashProperty( string cacheId, string subkey )
        {
            #region 初始化回傳值

            var result = new GetHashCacheResult()
            {
                Success = false,
                Data    = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的參數

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashProperty ) ) );
                return result;
            }

            if ( subkey.IsNullOrEmpty() )
            {
                result.Message = $"Argument '{nameof ( subkey )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashProperty ) ) );
                return result;
            }

            #endregion 檢查傳入的參數

            #region 從 Redis Server 上取得 Hash 資料

            RedisValue redisValue;

            try
            {
                if ( !_database.HashExists( cacheId, subkey ) )
                {
                    result.Message = $"Get hash property data from redis server fail due to does not exist specific cacheId. CacheId: {cacheId}, Subkey: {subkey}.";
                    Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashProperty ) ) );
                    return result;
                }

                var req = new RedisHashCommandArgs()
                {
                    CacheKey = cacheId,
                    Subkey   = subkey,
                    Action   = $"{nameof ( _database.HashGet )} {nameof ( GetHashProperty )}"
                };

                var r = _redisEngine.Execute<RedisValue>( req, args => _database.HashGet( args.CacheKey, args.Subkey ) );

                if ( !r.Success )
                {
                    result.Message = r.Message;
                    return result;
                }

                redisValue = r.Data;
            }
            catch ( Exception ex )
            {
                result.Message = $"Get hash data from redis server fail. CacheId: {cacheId}, Subkey: {subkey}.";
                Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( GetHashProperty ) ), result.Message );
                return result;
            }

            #endregion 從 Redis Server 上取得 Hash 資料

            #region 設定回傳值

            result.Data    = redisValue;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>取得 HashProperty 資料結構的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>取得資料結果</returns>
        public async Task<GetHashCacheResult> GetHashPropertyAsync( string cacheId, string subkey )
        {
            #region 初始化回傳值

            var result = new GetHashCacheResult()
            {
                Success = false,
                Data    = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的參數

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashPropertyAsync ) ) );
                return result;
            }

            if ( subkey.IsNullOrEmpty() )
            {
                result.Message = $"Argument '{nameof ( subkey )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashPropertyAsync ) ) );
                return result;
            }

            #endregion 檢查傳入的參數

            #region 從 Redis Server 上取得 Hash 資料

            RedisValue redisValue;

            try
            {
                if ( !await _database.HashExistsAsync( cacheId, subkey ) )
                {
                    result.Message = $"Get hash property data from redis server fail due to does not exist specific cacheId. CacheId: {cacheId}, Subkey: {subkey}.";
                    Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashPropertyAsync ) ) );
                    return result;
                }

                var req = new RedisHashCommandArgs()
                {
                    CacheKey = cacheId,
                    Subkey   = subkey,
                    Action   = $"{nameof ( _database.HashGetAsync )} {nameof ( GetHashPropertyAsync )}"
                };

                var r = await _redisEngine.ExecuteAsync<RedisValue>( req, async args => await _database.HashGetAsync( args.CacheKey, args.Subkey ) );

                if ( !r.Success )
                {
                    result.Message = r.Message;
                    return result;
                }

                redisValue = r.Data;
            }
            catch ( Exception ex )
            {
                result.Message = $"Get hash data from redis server fail. CacheId: {cacheId}, Subkey: {subkey}.";
                Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( GetHashPropertyAsync ) ), result.Message );
                return result;
            }

            #endregion 從 Redis Server 上取得 Hash 資料

            #region 設定回傳值

            result.Data    = redisValue;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>取得 HashProperty 資料集合的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得資料結果</returns>
        public GetHashPropertiesCacheResult GetHashProperties( string cacheId )
        {
            #region 初始化回傳值

            var result = new GetHashPropertiesCacheResult()
            {
                Success = false,
                Data    = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的參數

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashProperties ) ) );
                return result;
            }

            #endregion 檢查傳入的參數
            
            #region 從 Redis Server 上取得 Hash 資料

            HashEntry[] hashEntries;

            try
            {
                if ( !_database.KeyExists( cacheId ) )
                {
                    result.Message = $"Get hash property data from redis server fail due to does not exist specific cacheId. CacheId: {cacheId}.";
                    Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashProperties ) ) );
                    return result;
                }

                var req = new RedisHashCommandArgs()
                {
                    CacheKey = cacheId,
                    Action   = $"{nameof ( _database.HashGetAll )} {nameof ( GetHashProperties )}"
                };

                var r = _redisEngine.Execute<HashEntry[]>( req, args => _database.HashGetAll( args.CacheKey ) );

                if ( !r.Success )
                {
                    result.Message = r.Message;
                    return result;
                }

                hashEntries = r.Data;
            }
            catch ( Exception ex )
            {
                result.Message = $"Get hash data from redis server fail. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( GetHashProperties ) ), result.Message );
                return result;
            }

            #endregion 從 Redis Server 上取得 Hash 資料

            #region 轉換 HashProperty 資料集合

            HashProperty[] hashProperties;

            try
            {
                hashProperties = HashPropertyConverter.ConvertToHashProperty( hashEntries );
            }
            catch ( Exception ex )
            {
                result.Message = $"Convert HashEntry array to HashProperty array occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( GetHashProperties ) ), result.Message );
                return result;
            }

            if ( hashProperties.IsNullOrEmptyArray() )
            {
                result.Message = $"Get hash data from redis server fail. Retrive null or empty array. CacheId: {cacheId}.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashProperties ) ) );
                return result;
            }

            #endregion 轉換 HashProperty 資料集合

            #region 設定回傳值

            result.Data    = hashProperties;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>取得 HashProperty 資料集合的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得資料結果</returns>
        public async Task<GetHashPropertiesCacheResult> GetHashPropertiesAsync( string cacheId )
        {
            #region 初始化回傳值

            var result = new GetHashPropertiesCacheResult()
            {
                Success = false,
                Data    = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的參數

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashPropertiesAsync ) ) );
                return result;
            }

            #endregion 檢查傳入的參數
            
            #region 從 Redis Server 上取得 Hash 資料

            HashEntry[] hashEntries;

            try
            {
                if ( !await _database.KeyExistsAsync( cacheId ) )
                {
                    result.Message = $"Get hash property data from redis server fail due to does not exist specific cacheId. CacheId: {cacheId}.";
                    Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashPropertiesAsync ) ) );
                    return result;
                }

                var req = new RedisHashCommandArgs()
                {
                    CacheKey = cacheId,
                    Action   = $"{nameof ( _database.HashGetAllAsync )} {nameof ( GetHashPropertiesAsync )}"
                };

                var r = await _redisEngine.ExecuteAsync<HashEntry[]>( req, async args => await _database.HashGetAllAsync( args.CacheKey ) );

                if ( !r.Success )
                {
                    result.Message = r.Message;
                    return result;
                }

                hashEntries = r.Data;
            }
            catch ( Exception ex )
            {
                result.Message = $"Get hash data from redis server fail. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( GetHashPropertiesAsync ) ), result.Message );
                return result;
            }

            #endregion 從 Redis Server 上取得 Hash 資料

            #region 轉換 HashProperty 資料集合

            HashProperty[] hashProperties;

            try
            {
                hashProperties = HashPropertyConverter.ConvertToHashProperty( hashEntries );
            }
            catch ( Exception ex )
            {
                result.Message = $"Convert HashEntry array to HashProperty array occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( GetHashPropertiesAsync ) ), result.Message );
                return result;
            }

            if ( hashProperties.IsNullOrEmptyArray() )
            {
                result.Message = $"Get hash data from redis server fail. Retrive null or empty array. CacheId: {cacheId}.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( GetHashPropertiesAsync ) ) );
                return result;
            }

            #endregion 轉換 HashProperty 資料集合

            #region 設定回傳值

            result.Data    = hashProperties;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>存放 HashProperty 資料<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">Hash 快取資料結構集合</param>
        /// <returns>存放是否成功</returns>
        public bool PutHashProperty( string cacheId, HashProperty[] hashProperties )
        {
            using ( _asyncLock.Lock() )
            {
                #region 檢查傳入的參數

                if ( cacheId.IsNullOrEmpty() )
                {
                    Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                    Logger.WriteErrorLog( this, Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( PutHashProperty ) ) );
                    return false;
                }

                if ( hashProperties.IsNullOrEmptyArray() )
                {
                    Message = $"Argument '{nameof ( hashProperties )}' can not be null or empty array.";
                    Logger.WriteErrorLog( this, Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( PutHashProperty ) ) );
                    return false;
                }

                #endregion 檢查傳入的參數

                #region 轉換 HashEntry 資料集合

                HashEntry[] hashEntries;

                try
                {
                    hashEntries = HashPropertyConverter.ConvertToHashEntry( hashProperties );
                }
                catch ( Exception ex )
                {
                    Message = $"Convert HashProperty array to Redis HashEntry array occur exception. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( PutHashProperty ) ), Message );
                    return false;
                }

                if ( hashEntries.IsNullOrEmptyArray() )
                {
                    Message = $"Convert HashProperty array to Redis HashEntry array occur error. Retrive null or empty array. CacheId: {cacheId}.";
                    Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( PutHashProperty ) ) );
                    return false;
                }

                #endregion 轉換 HashEntry 資料集合

                #region 將目標資料存放到 Redis 快取服務中

                try
                {
                    var req = new RedisHashCommandArgs()
                    {
                        CacheKey    = cacheId,
                        Action      = $"{nameof ( _database.HashSet )} {nameof ( PutHashProperty )}",
                        HashEntries = hashEntries
                    };

                    var r = _redisEngine.Execute<bool>( req, args => 
                    {
                        try
                        {
                            _database.HashSet( args.CacheKey, args.HashEntries );
                        }
                        catch ( Exception ex )
                        {
                            Message = $"Execute redis {nameof ( _database.HashSet )} occur exception. CacheId: {cacheId}.";
                            Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( PutHashProperty ) ), Message );
                            return false;
                        }

                        return true;
                    } );

                    if ( !r.Success )
                    {
                        Message = r.Message;
                        return false;
                    }

                    bool success = r.Data;

                    if ( !success )
                    {
                        Message = $"Put hash data to redis server fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( PutHashProperty ) )}" );
                        return false;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Put hash data to redis server fail. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( PutHashProperty ) ), Message );
                    return false;
                }

                #endregion 將目標資料存放到 Redis 快取服務中

                return true;
            }
        }

        /// <summary>存放 HashProperty 資料<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">Hash 快取資料結構集合</param>
        /// <returns>存放是否成功</returns>
        public async Task<bool> PutHashPropertyAsync( string cacheId, HashProperty[] hashProperties )
        {
            using ( await _asyncLock.LockAsync() )
            {
                #region 檢查傳入的參數

                if ( cacheId.IsNullOrEmpty() )
                {
                    Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                    Logger.WriteErrorLog( this, Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( PutHashPropertyAsync ) ) );
                    return false;
                }

                if ( hashProperties.IsNullOrEmptyArray() )
                {
                    Message = $"Argument '{nameof ( hashProperties )}' can not be null or empty array.";
                    Logger.WriteErrorLog( this, Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( PutHashPropertyAsync ) ) );
                    return false;
                }

                #endregion 檢查傳入的參數

                #region 轉換 HashEntry 資料集合

                HashEntry[] hashEntries;

                try
                {
                    hashEntries = HashPropertyConverter.ConvertToHashEntry( hashProperties );
                }
                catch ( Exception ex )
                {
                    Message = $"Convert HashProperty array to Redis HashEntry array occur exception. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( PutHashPropertyAsync ) ), Message );
                    return false;
                }

                if ( hashEntries.IsNullOrEmptyArray() )
                {
                    Message = $"Convert HashProperty array to Redis HashEntry array occur error. Retrive null or empty array. CacheId: {cacheId}.";
                    Logger.WriteErrorLog( this, Message, GetLogTraceName( nameof ( PutHashPropertyAsync ) ) );
                    return false;
                }

                #endregion 轉換 HashEntry 資料集合

                #region 將目標資料存放到 Redis 快取服務中

                try
                {
                    var req = new RedisHashCommandArgs()
                    {
                        CacheKey    = cacheId,
                        Action      = $"{nameof ( _database.HashSetAsync )} {nameof ( PutHashPropertyAsync )}",
                        HashEntries = hashEntries
                    };

                    var r = await _redisEngine.ExecuteAsync<bool>( req, async args => 
                    {
                        try
                        {
                            await _database.HashSetAsync( args.CacheKey, args.HashEntries );
                        }
                        catch ( Exception ex )
                        {
                            Message = $"Execute redis {nameof ( _database.HashSetAsync )} occur exception. CacheId: {cacheId}.";
                            Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( PutHashPropertyAsync ) ), Message );
                            return false;
                        }

                        return true;
                    } );

                    if ( !r.Success )
                    {
                        Message = r.Message;
                        return false;
                    }

                    bool success = r.Data;

                    if ( !success )
                    {
                        Message = $"Put hash data to redis server fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( PutHashPropertyAsync ) )}" );
                        return false;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Put hash data to redis server fail. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( PutHashPropertyAsync ) ), Message );
                    return false;
                }

                #endregion 將目標資料存放到 Redis 快取服務中

                return true;
            }
        }

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料，CacheId 不會被整個移除掉。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>移除是否成功</returns>
        public bool RemoveHashProperty( string cacheId, string subkey )
        {
            using ( _asyncLock.Lock() )
            {
                #region 檢查傳入的參數

                if ( cacheId.IsNullOrEmpty() )
                {
                    Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                    Logger.WriteErrorLog( this, Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( RemoveHashProperty ) ) );
                    return false;
                }

                if ( subkey.IsNullOrEmpty() )
                {
                    Message = $"Argument '{nameof ( subkey )}' can not be null or empty string.";
                    Logger.WriteErrorLog( this, Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( RemoveHashProperty ) ) );
                    return false;
                }

                #endregion 檢查傳入的參數

                #region 移除 Redis Server 上目標的 Hash 資料

                try
                {
                    var req = new RedisHashCommandArgs()
                    {
                        CacheKey = cacheId,
                        Subkey   = subkey,
                        Action   = $"{nameof ( _database.HashDelete )} {nameof ( RemoveHashProperty )}"
                    };

                    var r = _redisEngine.Execute<bool>( req, args => _database.HashDelete( args.CacheKey, args.Subkey ) );

                    if ( !r.Success )
                    {
                        Message = r.Message;
                        return false;
                    }

                    bool success = r.Data;

                    if ( !success )
                    {
                        Message = $"Remove hash data from redis server fail. CacheId: {cacheId}, Subkey: {subkey}.";
                        Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( RemoveHashProperty ) )}" );
                        return false;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Remove hash data from redis server fail. CacheId: {cacheId}, Subkey: {subkey}.";
                    Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( RemoveHashProperty ) ), Message );
                    return false;
                }

                #endregion 移除 Redis Server 上目標的 Hash 資料

                return true;
            }
        }

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料，CacheId 不會被整個移除掉。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>移除是否成功</returns>
        public async Task<bool> RemoveHashPropertyAsync( string cacheId, string subkey )
        {
            using ( await _asyncLock.LockAsync() )
            {
                #region 檢查傳入的參數

                if ( cacheId.IsNullOrEmpty() )
                {
                    Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                    Logger.WriteErrorLog( this, Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( RemoveHashPropertyAsync ) ) );
                    return false;
                }

                if ( subkey.IsNullOrEmpty() )
                {
                    Message = $"Argument '{nameof ( subkey )}' can not be null or empty string.";
                    Logger.WriteErrorLog( this, Message, Logger.GetTraceLogTitle( nameof ( RedisCache ), nameof ( RemoveHashPropertyAsync ) ) );
                    return false;
                }

                #endregion 檢查傳入的參數

                #region 移除 Redis Server 上目標的 Hash 資料

                try
                {
                    var req = new RedisHashCommandArgs()
                    {
                        CacheKey = cacheId,
                        Subkey   = subkey,
                        Action   = $"{nameof ( _database.HashDeleteAsync )} {nameof ( RemoveHashPropertyAsync )}"
                    };

                    var r = await _redisEngine.ExecuteAsync<bool>( req, async args => await _database.HashDeleteAsync( args.CacheKey, args.Subkey ) );

                    if ( !r.Success )
                    {
                        Message = r.Message;
                        return false;
                    }

                    bool success = r.Data;

                    if ( !success )
                    {
                        Message = $"Remove hash data from redis server fail. CacheId: {cacheId}, Subkey: {subkey}.";
                        Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( RemoveHashPropertyAsync ) )}" );
                        return false;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Remove hash data from redis server fail. CacheId: {cacheId}, Subkey: {subkey}.";
                    Logger.WriteExceptionLog( this, ex, GetLogTraceName( nameof ( RemoveHashPropertyAsync ) ), Message );
                    return false;
                }

                #endregion 移除 Redis Server 上目標的 Hash 資料

                return true;
            }
        }

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件的資料。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="data">Entity 資料物件</param>
        /// <returns>存放資料結果</returns>
        public Result PutHashObject( string cacheId, object data )
        {
            using ( _asyncLock.Lock() )
            {
                var result = Result.Create();

                HashProperty[] hashProperties;

                try
                {
                    hashProperties = HashPropertyConverter.ConvertTo( data );
                }
                catch ( Exception ex )
                {
                    result.Message = $"Convert data entity to HashProperty array occur exception. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( PutHashObject ) ), result.Message );
                    return result;
                }

                if ( !PutHashProperty( cacheId, hashProperties ) )
                {
                    result.Message = Message;
                    Logger.WriteErrorLog( this, $"Put hash property object into redis server fail. CacheId: {cacheId}. {result.Message}", Logger.GetTraceLogTitle( this, nameof ( PutHashObject ) ) );
                    return result;
                }

                result.Success = true;
                return result;
            }
        }

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件的資料。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="data">Entity 資料物件</param>
        /// <returns>存放資料結果</returns>
        public async Task<Result> PutHashObjectAsync( string cacheId, object data )
        {
            using ( await _asyncLock.LockAsync() )
            {
                var result = Result.Create();

                HashProperty[] hashProperties;

                try
                {
                    hashProperties = HashPropertyConverter.ConvertTo( data );
                }
                catch ( Exception ex )
                {
                    result.Message = $"Convert data entity to HashProperty array occur exception. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( PutHashObjectAsync ) ), result.Message );
                    return result;
                }

                if ( !await PutHashPropertyAsync( cacheId, hashProperties ) )
                {
                    result.Message = Message;
                    Logger.WriteErrorLog( this, $"Put hash property object into redis server fail. CacheId: {cacheId}. {result.Message}", Logger.GetTraceLogTitle( this, nameof ( PutHashObjectAsync ) ) );
                    return result;
                }

                result.Success = true;
                return result;
            }
        }

        /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得結果</returns>
        public Result<TData> GetHashObject<TData>( string cacheId )
        {
            var result = Result.Create<TData>();
            var g = GetHashProperties( cacheId );

            if ( !g.Success )
            {
                result.Code    = g.Code;
                result.Message = g.Message;
                return result;
            }

            HashProperty[] hashProperties = g.Data;

            if ( hashProperties.IsNullOrEmptyArray() )
            {
                result.Message = $"No data found from redis server. Key: {cacheId}.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( GetHashObject ) ) );
                return result;
            }

            TData data = default ( TData );

            try
            {
                data = HashPropertyConverter.ConvertTo<TData>( hashProperties );
            }
            catch ( Exception ex )
            {
                result.Message = $"Convert hash property array to entity object occur exception. Key: {cacheId}.";
                Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( GetHashObject ) ), result.Message );
                return result;
            }

            result.Data    = data;
            result.Success = true;
            return result;
        }

        /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得結果</returns>
        public async Task<Result<TData>> GetHashObjectAsync<TData>( string cacheId )
        {
            var result = Result.Create<TData>();
            var g = await GetHashPropertiesAsync( cacheId );

            if ( !g.Success )
            {
                result.Code    = g.Code;
                result.Message = g.Message;
                return result;
            }

            HashProperty[] hashProperties = g.Data;

            if ( hashProperties.IsNullOrEmptyArray() )
            {
                result.Message = $"No data found from redis server. Key: {cacheId}.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( GetHashObjectAsync ) ) );
                return result;
            }

            TData data = default ( TData );

            try
            {
                data = HashPropertyConverter.ConvertTo<TData>( hashProperties );
            }
            catch ( Exception ex )
            {
                result.Message = $"Convert hash property array to entity object occur exception. Key: {cacheId}.";
                Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( GetHashObjectAsync ) ), result.Message );
                return result;
            }

            result.Data    = data;
            result.Success = true;
            return result;
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>對 Redis 快取服務進行資料更新
        /// </summary>
        /// <param name="action">新增或更新動作</param>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <param name="dataId">資料異動識別 ID 代碼，每次更新後資料後改變。</param>
        /// <param name="refreshTime">資料最後更新時間 (資料新增或更新至快取倉儲時的時間戳記)</param>
        /// <returns>是否成功對 Redis 快取服務更新資料</returns>
        private bool SetDataToRemote( string action, CacheProperty cacheProperty, out string dataId, out DateTime refreshTime )
        {
            #region 初始化輸出參數

            dataId      = null;
            refreshTime = default ( DateTime );

            #endregion 初始化輸出參數

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                Logger.WriteErrorLog( this, $"The Redis connection is broken. Redis server --> ({_config.Host}:{_config.Port}).", GetLogTraceName( nameof ( SetDataToRemote ) ) );
                return false;
            }

            #endregion 檢查連線狀態

            #region 檢查快取資料儲存屬性值

            string cacheId = cacheProperty.CacheId;

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Augument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemote ) )}" );
                return false;
            }

            if ( new int?[] { cacheProperty.ExpireInterval, cacheProperty.RefreshInterval }.All( j => j.IsNotNull() && j > 0 ) )
            {
                Message = $"Augument '{nameof ( cacheProperty.ExpireInterval )}' or '{nameof ( cacheProperty.RefreshInterval )}' is invalid.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemote ) )}" );
                return false;
            }

            #endregion 檢查快取資料儲存屬性值

            #region 設定快取儲存的屬性

            DateTime nowTime          = DateTime.Now;
            cacheProperty.RefreshTime = nowTime;
            cacheProperty.DataId      = RandomTextHelper.Create( 36 );

            int expireInterval = CacheDataSettings.TimeoutInterval;

            if ( cacheProperty.ExpireInterval.IsNotNull() && cacheProperty.ExpireInterval > 0 )
            {
                expireInterval = (int)cacheProperty.ExpireInterval;
            }

            if ( expireInterval > 0 )
            {
                cacheProperty.ExpireInterval = expireInterval;
                cacheProperty.ExpireTime     = nowTime.AddMinutes( expireInterval );
            }

            int refreshInterval = CachePoolSettings.RefreshInterval;

            if ( cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0 )
            {
                refreshInterval = (int)cacheProperty.RefreshInterval;
            }

            bool needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

            if ( needRefresh )
            {
                cacheProperty.RefreshInterval = refreshInterval;
                cacheProperty.ExpireInterval  = null;
                cacheProperty.ExpireTime      = null;
            }

            #endregion 設定快取儲存的屬性

            #region 序列化快取儲存載體

            string cacheJson = null;

            try
            {
                cacheJson = JsonConvertUtil.SerializeInCamelCaseNoFormatting( cacheProperty );
            }
            catch ( Exception ex )
            {
                Message = $"Serialize {nameof ( CacheProperty )} occur exception. CacheId: {cacheId}. {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemote ) )}" );
                return false;
            }

            if ( cacheJson.IsNullOrEmpty() )
            {
                Message = $"Serialize {nameof ( CacheProperty )} fail. CacheId: {cacheId}.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemote ) )}" );
                return false;
            }

            #endregion 序列化快取儲存載體

            #region 將目標資料存放到 Redis 快取服務中

            using ( _asyncLock.Lock() ) 
            {
                try
                {
                    var req = new RedisCommandArgs()
                    {
                        CacheKey = cacheId,
                        Action   = $"{nameof ( _database.StringSet )} {action}",
                        Data     = cacheJson
                    };

                    When when = nameof ( Put ) == action ? When.NotExists : When.Exists;

                    var r = _redisEngine.Execute<bool>( req, args => 
                        expireInterval > 0 ?
                            _database.StringSet( cacheId, cacheJson, TimeSpan.FromMinutes( expireInterval ), when: when ) :
                            _database.StringSet( cacheId, cacheJson, when: when )
                    );

                    if ( !r.Success )
                    {
                        Message = r.Message;
                        return false;
                    }

                    bool success = r.Data;

                    if ( !success )
                    {
                        Message = $"Put data to redis server fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemote ) )}" );
                        return false;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Put data to redis server occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemote ) )}" );
                    return false;
                }

                #endregion 將目標資料存放到 Redis 快取服務中

                #region 註冊快取資料的 RefreshCacheDataHandler 處理委派

                if ( !needRefresh )
                {
                    dataId      = cacheProperty.DataId;
                    refreshTime = cacheProperty.RefreshTime;
                    return true;
                }

                var timer      = new ST.Timer( TimeSpan.FromMinutes( refreshInterval ).TotalMilliseconds );
                timer.Elapsed += ( sender, e ) => ExecuteRefreshDataAsync( cacheId );

                var refreshProperty = new DataRefreshProperty()
                {
                    CacheId        = cacheId,
                    RefreshHandler = cacheProperty.RefreshCallback,
                    RefreshTimer   = timer
                };

                try
                {
                    if ( !_refreshProperties.ContainsKey( cacheId ) )
                    {
                        _refreshProperties.Add( cacheId, refreshProperty );
                    }
                    else
                    {
                        var refreshProp = _refreshProperties[ cacheId ];

                        refreshProp.IsNotNull( p => 
                        {
                            refreshProp.RefreshTimer?.Stop();
                            refreshProp.RefreshTimer?.Dispose();
                            refreshProp.RefreshTimer   = null;
                            refreshProp.RefreshHandler = null;
                            refreshProp.CacheId        = null;
                        } );

                        _refreshProperties[ cacheId ] = refreshProperty;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Add refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemote ) )}" );
                    return false;
                }

                timer.Start();

                #endregion 註冊快取資料的 RefreshCacheDataHandler 處理委派

                #region 設定輸出參數

                dataId      = cacheProperty.DataId;
                refreshTime = cacheProperty.RefreshTime;

                #endregion 設定輸出參數
            }

            return true;
        }

        /// <summary>對 Redis 快取服務進行資料更新
        /// </summary>
        /// <param name="action">新增或更新動作</param>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功對 Redis 快取服務更新資料</returns>
        private async Task<( bool success, string dataId, DateTime refreshTime)> SetDataToRemoteAsync( string action, CacheProperty cacheProperty )
        {
            #region 初始化輸出參數

            var dataId      = default ( String );
            var refreshTime = default ( DateTime );

            #endregion 初始化輸出參數

            #region 檢查連線狀態

            if ( !CheckConnectionStatus() )
            {
                Logger.WriteErrorLog( this, $"The Redis connection is broken. Redis server --> ({_config.Host}:{_config.Port}).", GetLogTraceName( nameof ( SetDataToRemoteAsync ) ) );
                return ( false, dataId, refreshTime );
            }

            #endregion 檢查連線狀態

            #region 檢查快取資料儲存屬性值

            string cacheId = cacheProperty.CacheId;

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Augument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemoteAsync ) )}" );
                return ( false, dataId, refreshTime );
            }

            if ( new int?[] { cacheProperty.ExpireInterval, cacheProperty.RefreshInterval }.All( j => j.IsNotNull() && j > 0 ) )
            {
                Message = $"Augument '{nameof ( cacheProperty.ExpireInterval )}' or '{nameof ( cacheProperty.RefreshInterval )}' is invalid.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemoteAsync ) )}" );
                return ( false, dataId, refreshTime );
            }

            #endregion 檢查快取資料儲存屬性值

            #region 設定快取儲存的屬性

            DateTime nowTime          = DateTime.Now;
            cacheProperty.RefreshTime = nowTime;
            cacheProperty.DataId      = RandomTextHelper.Create( 36 );

            int expireInterval = CacheDataSettings.TimeoutInterval;

            if ( cacheProperty.ExpireInterval.IsNotNull() && cacheProperty.ExpireInterval > 0 )
            {
                expireInterval = (int)cacheProperty.ExpireInterval;
            }

            if ( expireInterval > 0 )
            {
                cacheProperty.ExpireInterval = expireInterval;
                cacheProperty.ExpireTime     = nowTime.AddMinutes( expireInterval );
            }

            int refreshInterval = CachePoolSettings.RefreshInterval;

            if ( cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0 )
            {
                refreshInterval = (int)cacheProperty.RefreshInterval;
            }

            bool needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

            if ( needRefresh )
            {
                cacheProperty.RefreshInterval = refreshInterval;
                cacheProperty.ExpireInterval  = null;
                cacheProperty.ExpireTime      = null;
            }

            #endregion 設定快取儲存的屬性

            #region 序列化快取儲存載體

            string cacheJson = null;

            try
            {
                cacheJson = JsonConvertUtil.SerializeInCamelCaseNoFormatting( cacheProperty );
            }
            catch ( Exception ex )
            {
                Message = $"Serialize {nameof ( CacheProperty )} occur exception. CacheId: {cacheId}. {ex.ToString()}";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemoteAsync ) )}" );
                return ( false, dataId, refreshTime );
            }

            if ( cacheJson.IsNullOrEmpty() )
            {
                Message = $"Serialize {nameof ( CacheProperty )} fail. CacheId: {cacheId}.";
                Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemoteAsync ) )}" );
                return ( false, dataId, refreshTime );
            }

            #endregion 序列化快取儲存載體
            
            using ( await _asyncLock.LockAsync() )
            {
                #region 將目標資料存放到 Redis 快取服務中

                try
                {
                    var req = new RedisCommandArgs()
                    {
                        CacheKey = cacheId,
                        Action   = $"{nameof ( _database.StringSetAsync )} {action}",
                        Data     = cacheJson
                    };

                    When when = nameof ( PutAsync ) == action ? When.NotExists : When.Exists;

                    var r = await _redisEngine.ExecuteAsync<bool>( req, async args => 
                        expireInterval > 0 ?
                            await _database.StringSetAsync( cacheId, cacheJson, TimeSpan.FromMinutes( expireInterval ), when: when ) :
                            await _database.StringSetAsync( cacheId, cacheJson, when: when )
                    );

                    if ( !r.Success )
                    {
                        Message = r.Message;
                        return ( false, dataId, refreshTime );
                    }

                    bool success = r.Data;

                    if ( !success )
                    {
                        Message = $"Put data to redis server fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemoteAsync ) )}" );
                        return ( false, dataId, refreshTime );
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Put data to redis server occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemoteAsync ) )}" );
                    return ( false, dataId, refreshTime );
                }

                #endregion 將目標資料存放到 Redis 快取服務中

                #region 註冊快取資料的 RefreshCacheDataHandler 處理委派

                if ( !needRefresh )
                {
                    dataId      = cacheProperty.DataId;
                    refreshTime = cacheProperty.RefreshTime;
                    return ( success: true, dataId: dataId, refreshTime: refreshTime );
                }

                var timer      = new ST.Timer( TimeSpan.FromMinutes( refreshInterval ).TotalMilliseconds );
                timer.Elapsed += ( sender, e ) => ExecuteRefreshDataAsync( cacheId );

                var refreshProperty = new DataRefreshProperty()
                {
                    CacheId        = cacheId,
                    RefreshHandler = cacheProperty.RefreshCallback,
                    RefreshTimer   = timer
                };

                try
                {
                    if ( !_refreshProperties.ContainsKey( cacheId ) )
                    {
                        _refreshProperties.Add( cacheId, refreshProperty );
                    }
                    else
                    {
                        var refreshProp = _refreshProperties[ cacheId ];

                        refreshProp.IsNotNull( p => 
                        {
                            refreshProp.RefreshTimer?.Stop();
                            refreshProp.RefreshTimer?.Dispose();
                            refreshProp.RefreshTimer   = null;
                            refreshProp.RefreshHandler = null;
                            refreshProp.CacheId        = null;
                        } );

                        _refreshProperties[ cacheId ] = refreshProperty;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Add refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"{GetLogTraceName( nameof ( SetDataToRemoteAsync ) )}" );
                    return ( false, dataId, refreshTime );
                }

                timer.Start();

                #endregion 註冊快取資料的 RefreshCacheDataHandler 處理委派
            }

            #region 設定輸出參數

            dataId      = cacheProperty.DataId;
            refreshTime = cacheProperty.RefreshTime;

            #endregion 設定輸出參數

            return ( success: true, dataId: dataId, refreshTime: refreshTime );
        }

        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( RedisCache )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
