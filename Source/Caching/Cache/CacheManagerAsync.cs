﻿using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Caching
{
    /// <summary>快取管理員
    /// </summary>
    public static partial class CacheManager
    {
        #region 宣告內部的靜態委派屬性

        /// <summary>存放快取資料的委派
        /// </summary>
        internal static PutCacheDataAsyncHandler PutAsyncCallback { get; set; }

        /// <summary>更新快取資料的委派
        /// </summary>
        internal static UpdateCacheDataAsyncHandler UpdateAsyncCallback { get; set; }

        /// <summary>移除快取資料的委派
        /// </summary>
        internal static RemoveCacheDataAsyncHandler RemoveAsyncCallback { get; set; }

        /// <summary>清空快取資料的委派
        /// </summary>
        internal static ClearCacheDataAsyncHandler ClearAsyncCallback { get; set; }

        /// <summary>檢查快取是否包含有目標資料的委派
        /// </summary>
        internal static ContainsCacheDataAsyncHandler ContainsAsyncCallback { get; set; }

        /// <summary>檢查 Hash 資料快取池中是否包含指定識別碼的資料的委派
        /// </summary>
        internal static ContainsHashAsyncHandler ContainsHashAsyncCallback { get; set; }

        /// <summary>取得 Hash 資料結構的 Hash Field 資料值的委派
        /// </summary>
        internal static GetHashPropertyAsyncHandler GetHashPropertyAsyncCallback { get; set; }

        /// <summary>取得 HashProperty 資料集合的快取資料的委派
        /// </summary>
        internal static GetHashPropertiesAsyncHandler GetHashPropertiesAsyncCallback { get; set; }

        /// <summary>以 Hash 資料結構存放至快取池中的委派。<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        internal static PutHashPropertyAsyncHandler PutHashPropertyAsyncCallback { get; set; }

        /// <summary>將指定的 Hash 快取資料從快取池中移除的委派。
        /// </summary>
        internal static RemoveHashPropertyAsyncHandler RemoveHashPropertyAsyncCallback { get; set; }

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料的委派，CacheId 不會被整個移除掉。
        /// </summary>
        internal static RemoveHashPropertyFieldAsyncHandler RemoveHashPropertyFieldAsyncCallback { get; set; }

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件資料的委派
        /// </summary>
        internal static PutHashObjectAsyncHandler PutHashObjectAsyncCallback { get; set; }

        #endregion 宣告內部的靜態委派屬性


        #region 宣告公開的靜態方法

        /// <summary>根據指定的快取ID從快取池中取出資料
        /// </summary>
        /// <param name="cacheId">快取ID值</param>
        /// <param name="getSomethingCallback">重新從資料來源取得資料的回呼委派</param>
        /// <returns>從快取池中取得資料的結果</returns>
        public static async Task<GetCacheResult> GetAsync( string cacheId, GetSomethingFromDataSourceAsync getSomethingCallback = null ) =>
            await HandlerBroker.GetCacheAsyncHandler( _cacheRepoType )?.Invoke( cacheId, getSomethingCallback );

        /// <summary>根據指定的快取ID從快取池中取出資料
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="cacheId">快取ID值</param>
        /// <param name="getSomethingCallback">重新從資料來源取得資料的回呼委派</param>
        /// <returns>從快取池中取得資料的結果</returns>
        public static async Task<GetCacheResult<TModel>> GetAsync<TModel>( string cacheId, GetSomethingFromDataSourceAsync getSomethingCallback = null ) =>
            await HandlerBroker.GetCacheAsyncHandler<TModel>( _cacheRepoType )?.Invoke( cacheId, getSomethingCallback );

        /// <summary>將指定的資料放入快取池中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入快取池</returns>
        public static async Task<bool> PutAsync( CacheProperty cacheProperty ) => await PutAsyncCallback?.Invoke( cacheProperty );

        /// <summary>更新快取池中指定的資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否更新成功</returns>
        public static async Task<bool> UpdateAsync( CacheProperty cacheProperty ) => await UpdateAsyncCallback?.Invoke( cacheProperty );

        /// <summary>將指定的資料從快取池中移除
        /// </summary>
        /// <param name="cacheId">快取ID值</param>
        /// <returns>是否成功移除資料</returns>
        public static async Task<bool> RemoveAsync( string cacheId ) => await RemoveAsyncCallback?.Invoke( cacheId );

        /// <summary>清空快取池中所有的資料
        /// </summary>
        /// <returns>是否成功清空</returns>
        public static async Task<bool> ClearAsync() => await ClearAsyncCallback?.Invoke();

        /// <summary>檢查快取池中是否包含指定的快取ID的資料
        /// </summary>
        /// <param name="cacheId">快取ID值</param>
        /// <returns>快取池中是否包含指定的快取ID的資料</returns>
        public static async Task<bool> ContainsAsync( string cacheId ) => await ContainsAsyncCallback?.Invoke( cacheId );

        /// <summary>檢查 Hash 資料快取池中是否包含指定識別碼的資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>是否包含指定識別碼的資料</returns>
        public static async Task<bool> ContainsHashAsync( string cacheId ) => await ContainsHashAsyncCallback?.Invoke( cacheId );

        /// <summary>取得 Hash 資料結構的 Hash Field 資料值
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>取得資料結果</returns>
        public static async Task<GetHashCacheResult> GetHashPropertyAsync( string cacheId, string subkey ) => 
            await GetHashPropertyAsyncCallback?.Invoke( cacheId, subkey );

        /// <summary>取得 HashProperty 資料集合的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得資料結果</returns>
        public static async Task<GetHashPropertiesCacheResult> GetHashPropertiesAsync( string cacheId ) => 
            await GetHashPropertiesAsyncCallback?.Invoke( cacheId );

        /// <summary>以 Hash 資料結構存放至快取池中。<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">HashProperty 的資料識別 Key 值</param>
        /// <returns>存放是否成功</returns>
        public static async Task<bool> PutHashPropertyAsync( string cacheId, HashProperty[] hashProperties ) =>
            await PutHashPropertyAsyncCallback?.Invoke( cacheId, hashProperties );

        /// <summary>將指定的 Hash 快取資料從快取池中移除。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>移除是否成功</returns>
        public static async Task<bool> RemoveHashPropertyAsync( string cacheId ) => 
            await RemoveHashPropertyAsyncCallback?.Invoke( cacheId );

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料，CacheId 不會被整個移除掉。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>移除是否成功</returns>
        public static async Task<bool> RemoveHashPropertyAsync( string cacheId, string subkey ) => 
            await RemoveHashPropertyFieldAsyncCallback?.Invoke( cacheId, subkey );

        /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得結果</returns>
        public static async Task<Result<TData>> GetHashObjectAsync<TData>( string cacheId ) => 
            await HandlerBroker.GetHashObjectAsyncHandler<TData>( _cacheRepoType )?.Invoke( cacheId );

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件資料的委派
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="data">Entity 資料物件</param>
        /// <returns>存放資料結果</returns>
        public static async Task<Result> PutHashObjectAsync( string cacheId, object data ) =>
            await PutHashObjectAsyncCallback?.Invoke( cacheId, data );

        #endregion 宣告公開的靜態方法
    }
}
