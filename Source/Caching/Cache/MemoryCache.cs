﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>In-process 記憶體快取池
    /// </summary>
    public static class MemoryCache
    {
        #region 宣告私有的靜態成員

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>非同步作業鎖定物件: 鎖定 _hashData 欄位
        /// </summary>
        private static readonly AsyncLock _asyncLockHash = new AsyncLock();

        /// <summary>非同步作業鎖定物件: 鎖定 _message 欄位
        /// </summary>
        private static readonly AsyncLock _asyncLockMessage = new AsyncLock();

        /// <summary>訊息
        /// </summary>
        private static string _message;

        /// <summary>資料快取池
        /// </summary>
        private static readonly Dictionary<string, CacheProperty> _data = new Dictionary<string, CacheProperty>();

        /// <summary>Hash 資料結構的快取池
        /// </summary>
        private static readonly Dictionary<string, Dictionary<string, object>> _hashData = new Dictionary<string, Dictionary<string, object>>();

        /// <summary>處理器記時器池
        /// </summary>
        private static readonly Dictionary<string, Timer> _handlerTimers = new Dictionary<string, Timer>();

        /// <summary>快取自動回收計時器
        /// </summary>
        private static Timer _cleanerTimer = null;

        #endregion 宣告私有的靜態成員
        

        #region 宣告靜態建構子

        /// <summary>靜態建構子，註冊Timer的處理常式
        /// </summary>
        static MemoryCache()
        {
            ConfigReader.LoadMemoryCacheSettings();
            Initailize();
        }

        #endregion 宣告靜態建構子


        #region 宣告私有的靜態方法

        /// <summary>初始化
        /// </summary>
        private static void Initailize()
        {
            InitializeResetTimer();
            InializeHandlerTimers();
        }

        /// <summary>初始化快取自動回收計時器，並且註冊事件處理常式
        /// </summary>
        private static void InitializeResetTimer()
        {
            _cleanerTimer          = new Timer( TimeSpan.FromSeconds( CachePoolSettings.ResetInterval ).TotalMilliseconds );
            _cleanerTimer.Elapsed += ( sender, e ) => 
            {
                var cacheProperties = _data.Values;
                
                lock ( _data )
                {
                    var expireCaches = new List<string>();

                    foreach ( var cacheProp in cacheProperties )
                    {
                        try
                        {
                            if ( cacheProp.IsNull() )
                            {
                                continue;
                            }

                            if ( cacheProp.RefreshInterval.IsNotNull() || cacheProp.RefreshCallback.IsNotNull() )
                            {
                                continue;
                            }

                            if ( DateTime.Now < cacheProp.ExpireTime )
                            {
                                continue;
                            }

                            expireCaches.Add( cacheProp.CacheId );
                        }
                        catch ( Exception ex )
                        {
                            Logger.WriteErrorLog( nameof ( MemoryCache ), $"{nameof ( MemoryCache )} CleanerTimer remove expire data from in-process memory cache occur exception. CacheId: {cacheProp.CacheId}. {ex.ToString()}", GetLogTraceName( "CleanerTimer" ) );
                            continue;
                        }
                    }

                    if ( expireCaches.IsNullOrEmptyList() )
                    {
                        return;
                    }

                    foreach ( string cacheId in expireCaches )
                    {
                        try
                        {
                            if ( !Remove( cacheId ) )
                            {
                                Logger.WriteErrorLog( nameof ( MemoryCache ), $"{nameof ( MemoryCache )} CleanerTimer remove expire data from in-process memory cache fail. CacheId: {cacheId}. {Message}", GetLogTraceName( "CleanerTimer" ) );
                                continue;
                            }
                        }
                        catch ( Exception ex )
                        {
                            Logger.WriteErrorLog( nameof ( MemoryCache ), $"{nameof ( MemoryCache )} CleanerTimer remove expire data from in-process memory cache occur exception. CacheId: {cacheId}. {ex.ToString()}", GetLogTraceName( "CleanerTimer" ) );
                            continue;
                        }
                    }
                }
            };

            _cleanerTimer.Enabled = CachePoolSettings.IsResetEnable;
        }

        /// <summary>執行快取資料更新
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        private static void ExecuteRefreshData( string cacheId )
        {
            #region 從記憶體快取中取得資料

            CacheProperty cacheProperty = null;

            try
            {
                cacheProperty = _data[ cacheId ];
            }
            catch ( Exception ex )
            {
                Logger.WriteErrorLog( nameof ( MemoryCache ), $"{nameof ( MemoryCache )} RefreshTimer get data from in-process memory cache occur exception. CacheId: {cacheId}. {ex.ToString()}", $"{GetLogTraceName( nameof ( ExecuteRefreshData ))}" );
                return;
            }

            if ( cacheProperty.IsNull() )
            {
                Logger.WriteErrorLog( nameof ( MemoryCache ), $"{nameof ( MemoryCache )} RefreshTimer retrive null value of CacheProperty from in-process memory cache. CacheId: {cacheId}.", $"{GetLogTraceName( nameof ( ExecuteRefreshData ))}" );
                return;
            }

            DataRefreshProperty refreshProperty = cacheProperty.RefreshProperty;

            if ( refreshProperty.IsNull() )
            {
                return;
            }

            if ( refreshProperty.RefreshHandler.IsNull() )
            {
                return;
            }

            #endregion 從記憶體快取中取得資料

            lock ( cacheProperty )
            {
                #region 執行資料更新委派

                object data = null;

                try
                {
                    data = refreshProperty.RefreshHandler();
                }
                catch ( Exception ex )
                {
                    Logger.WriteErrorLog( nameof ( MemoryCache ), $"{nameof ( MemoryCache )} RefreshTimer execute refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}", $"{GetLogTraceName( nameof ( ExecuteRefreshData ))}" );
                    return;
                }

                if ( data.IsNull() )
                {
                    Logger.WriteErrorLog( nameof ( MemoryCache ), $"{nameof ( MemoryCache )} RefreshTimer retrive null from refresh handler result. No update date to in-process memory cache. CacheId: {cacheId}.", $"{GetLogTraceName( nameof ( ExecuteRefreshData ))}" );
                    return;
                }

                cacheProperty.Data        = data;
                cacheProperty.DataId      = RandomTextHelper.Create( 36 );
                cacheProperty.RefreshTime = DateTime.Now;

                #endregion 執行資料更新委派

                #region 對記憶體快取池進行資料更新

                try
                {
                    _data[ cacheId ] = cacheProperty;
                }
                catch ( Exception ex )
                {
                    Logger.WriteErrorLog( nameof ( MemoryCache ), $"{nameof ( MemoryCache )} RefreshTimer update date to in-process memory cache occur exception. CacheId: {cacheId}. {ex.ToString()}", $"{GetLogTraceName( nameof ( ExecuteRefreshData ))}" );
                    return;
                }

                #endregion 對記憶體快取池進行資料更新
            }
        }

        /// <summary>初始化快取處理器記時器池
        /// </summary>
        private static void InializeHandlerTimers()
        {
            var handlerConfigCollection = CachePoolSettings.CachingHandlers;

            if ( handlerConfigCollection.IsNull() || 0 == handlerConfigCollection.Count )
            {
                return;
            }

            foreach ( var m in CachePoolSettings.CachingHandlers )
            {
                var handlerConfig = (CachingHandlerElement)m;

                string handlerName    = handlerConfig.HandlerName + "";
                int    intervalMinute = handlerConfig.IntervalMinute;

                Timer handlerTimer = new Timer( intervalMinute * 1000 * 60 );  // 每多少分鐘自動執行
                //Timer handlerTimer = new Timer( intervalMinute * 1000 );       // Unit Test測試時，每多少秒鐘自動執行

                try
                {
                    _handlerTimers.Add( handlerName, handlerTimer );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, "Zayni.Caching.MemoryCache 初始化快取處理器發生異常: {0}".FormatTo( ex.ToString() ) );
                }
            }
        }

        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( MemoryCache )}.{methodName}";

        #endregion 宣告私有的靜態方法


        #region 宣告公開的靜態屬性

        /// <summary>訊息
        /// </summary>
        public static string Message
        {
            get
            {
                using ( _asyncLockMessage.Lock() )
                {
                    return _message;
                }
            }
            set
            {
                using ( _asyncLockMessage.Lock() )
                {
                    _message = value;
                }
            }
        }

        /// <summary>快取池中目前存放的資料個數
        /// </summary>
        public static int Count => CacheKeyContainer.Count;

        #endregion 宣告公開的靜態屬性


        #region 宣告內部的靜態方法

        /// <summary>取得訊息
        /// </summary>
        /// <returns>失敗/錯誤訊息</returns>
        internal static string GetMessage() => Message;

        /// <summary>設定失敗/錯誤訊息
        /// </summary>
        /// <param name="value">失敗/錯誤訊息</param>
        internal static void SetMessage( string value ) => Message = value;

        /// <summary>取得快取池中目前存放的資料個數
        /// </summary>
        /// <returns>快取池中目前存放的資料個數</returns>
        internal static int GetCount() => Count;

        #endregion 宣告內部的靜態方法


        #region 公開的靜態方法

        /// <summary>取得快取倉儲中的 Key 值集合
        /// </summary>
        /// <param name="pattern">Key 值的規則</param>
        /// <returns>取得結果</returns>
        public static Result<List<string>> GetCacheKeys( string pattern = null )
        {
            #region 初始化回傳值

            var result = Result.Create<List<string>>( false );
            List<string> cacheKeys = null;

            #endregion 初始化回傳值

            #region 從記憶體快取池中取得 Keys

            try
            {
                if ( pattern.IsNullOrEmpty() )
                {
                    cacheKeys = _data.Keys.Select( k => k.ToString() ).ToList();
                }
                else
                {
                    cacheKeys = _data.Keys.Select( k => k.ToString() ).Where( q => q.StartsWith( pattern ) ).ToList();
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Get match cache keys from in-process memory cache occur exception. {ex.ToString()}";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, "Get match cache keys from in-process memory occur exception." );
                return result;
            }

            #endregion 從記憶體快取池中取得 Keys

            #region 設定回傳值

            result.Data      = cacheKeys;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }
        
        /// <summary>根據指定的快取ID從快取池中取出資料
        /// </summary>
        /// <param name="cacheId">快取ID值</param>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
        /// <returns>從快取池中取得資料的結果</returns>
        public static GetCacheResult Get( string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult()
            {
                Success   = false,
                CacheData = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的快取ID參數

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = "cacheId為Null或空字串不合法，無法從快取池中取得資料。";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, "MemoryCache.Get" );
                return result;
            }

            #endregion 檢查傳入的快取ID參數

            #region 檢查記憶體快取池中是否存在目標資料

            if ( !_data.ContainsKey( cacheId ) && !CacheKeyContainer.Exists( cacheId ) )
            {
                if ( handler.IsNotNull() )
                {
                    #region 執行資料來源取得資料的回呼函式

                    bool isSuccess = CachingHandlerExecutor.ExecuteGetSomethingCallback( handler, out object resultData, out string message );

                    if ( !isSuccess )
                    {
                        result.Message = $"Can not found cacheId: '{cacheId}' from in-process memory cache pool. Try to execute {nameof ( GetSomethingFromDataSource )} handler occur exception. {message}";
                        Logger.WriteWarningLog( nameof ( MemoryCache ), result.Message, "MemoryCache.Get" );
                        return result;
                    }

                    var cacheProp = new CacheProperty()
                    {
                        CacheId        = cacheId,
                        Data           = resultData,
                        ExpireInterval = timeoutMinuntes
                    };

                    // 如果重新向資料來源取得資料成功，就自動把資料存放到記憶體快取池中
                    if ( !Put( cacheProp, out string dataId, out DateTime refreshTime ) )
                    {
                        result.Message = $"Execute {nameof ( GetSomethingFromDataSource )} handler success, but put data to in-process memory cache pool fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, "MemoryCache.Get" );
                    }
                    
                    result.DataId      = dataId;
                    result.CacheData   = resultData;
                    result.RefreshTime = refreshTime;
                    result.Success   = true;
                    return result;

                    #endregion 執行資料來源取得資料的回呼函式
                }
                else
                {
                    #region 回傳從記憶體快取中取得資料失敗

                    result.Message = $"No cacheId: '{cacheId}' in in-process memory cache pool.";
                    Logger.WriteWarningLog( nameof ( MemoryCache ), result.Message, "MemoryCache.Get" );
                    return result;

                    #endregion 回傳從記憶體快取中取得資料失敗
                }
            }

            #endregion 檢查記憶體快取池中是否存在目標資料

            #region 從記憶體快取中取得目標資料

            CacheProperty cacheProperty = null;

            try
            {
                cacheProperty = _data[ cacheId ];
            }
            catch ( Exception ex )
            {
                result.Message = $"Get data from in-process memory cache occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( Get ) ), message: result.Message );
                return result;
            }

            #endregion 從記憶體快取中取得目標資料

            #region 設定回傳值屬性

            result.DataId      = cacheProperty.DataId;
            result.CacheData   = cacheProperty.Data;
            result.RefreshTime = cacheProperty.RefreshTime;
            result.Success   = true;

            #endregion 設定回傳值屬性
            
            return result;
        }

        /// <summary>根據指定的快取ID從快取池中取出資料
        /// </summary>
        /// <param name="cacheId">快取ID值</param>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
        /// <returns>從快取池中取得資料的結果</returns>
        public static async Task<GetCacheResult> GetAsync( string cacheId, GetSomethingFromDataSourceAsync handler = null, int? timeoutMinuntes = null ) 
        {
            #region 初始化回傳值

            var result = new GetCacheResult()
            {
                Success   = false,
                CacheData = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的快取ID參數

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = "cacheId為Null或空字串不合法，無法從快取池中取得資料。";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, "MemoryCache.GetAsync" );
                return result;
            }

            #endregion 檢查傳入的快取ID參數

            #region 檢查記憶體快取池中是否存在目標資料

            if ( !_data.ContainsKey( cacheId ) && !CacheKeyContainer.Exists( cacheId ) )
            {
                if ( handler.IsNotNull() )
                {
                    #region 執行資料來源取得資料的回呼函式

                    var g = await CachingHandlerExecutor.ExecuteGetSomethingCallbackAsync( handler );

                    if ( !g.Success )
                    {
                        result.Message = $"Can not found cacheId: '{cacheId}' from in-process memory cache pool. Try to execute {nameof ( GetSomethingFromDataSourceAsync )} handler occur exception. {g.Message}";
                        Logger.WriteWarningLog( nameof ( MemoryCache ), result.Message, "MemoryCache.GetAsync" );
                        return result;
                    }

                    var resultData = g.Data;

                    var cacheProp = new CacheProperty()
                    {
                        CacheId        = cacheId,
                        Data           = resultData,
                        ExpireInterval = timeoutMinuntes
                    };

                    // 如果重新向資料來源取得資料成功，就自動把資料存放到記憶體快取池中
                    if ( !Put( cacheProp, out string dataId, out DateTime refreshTime ) )
                    {
                        result.Message = $"Execute {nameof ( GetSomethingFromDataSourceAsync )} handler success, but put data to in-process memory cache pool fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, "MemoryCache.GetAsync" );
                    }
                    
                    result.DataId      = dataId;
                    result.CacheData   = resultData;
                    result.RefreshTime = refreshTime;
                    result.Success   = true;
                    return result;

                    #endregion 執行資料來源取得資料的回呼函式
                }
                else
                {
                    #region 回傳從記憶體快取中取得資料失敗

                    result.Message = $"No cacheId: '{cacheId}' in in-process memory cache pool.";
                    Logger.WriteWarningLog( nameof ( MemoryCache ), result.Message, "MemoryCache.GetAsync" );
                    return result;

                    #endregion 回傳從記憶體快取中取得資料失敗
                }
            }

            #endregion 檢查記憶體快取池中是否存在目標資料

            #region 從記憶體快取中取得目標資料

            CacheProperty cacheProperty = null;

            try
            {
                cacheProperty = _data[ cacheId ];
            }
            catch ( Exception ex )
            {
                result.Message = $"Get data from in-process memory cache occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( GetAsync ) ), message: result.Message );
                return result;
            }

            #endregion 從記憶體快取中取得目標資料

            #region 設定回傳值屬性

            result.DataId      = cacheProperty.DataId;
            result.CacheData   = cacheProperty.Data;
            result.RefreshTime = cacheProperty.RefreshTime;
            result.Success   = true;

            #endregion 設定回傳值屬性
            
            return result;
        }

        /// <summary>根據指定的快取ID從快取池中取出資料
        /// </summary>
        /// <typeparam name="TData">資料模型泛型</typeparam>
        /// <param name="cacheId">快取ID值</param>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
        /// <returns>從快取池中取得資料的結果</returns>
        public static GetCacheResult<TData> Get<TData>( string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult<TData>() 
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 取得快取池中的目標資料

            GetCacheResult g = Get( cacheId, handler, timeoutMinuntes );

            if ( !g.Success )
            {
                Message        = g.Message;
                result.Message = g.Message;
                return result;
            }

            result.Message = g.Message;
            result.Success = g.Success;

            #endregion 取得快取池中的目標資料

            #region 對目標資料進行強制轉型

            TData data = default ( TData );

            try
            {
                switch ( g.CacheData )
                {
                    case JToken json:
                        data = json.ToObject<TData>();
                        break;

                    default:
                        data = (TData)g.CacheData;
                        break;
                }
            }
            catch ( JsonSerializationException jsonException )
            {
                try
                {
                    JToken jToken = g.CacheData as JToken;

                    if ( jToken.IsNull() )
                    {
                        result.Success = false;
                        result.CacheData = default ( TData );
                        result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                        Logger.WriteExceptionLog( nameof ( MemoryCache ), jsonException, result.Message );
                        return result;
                    }

                    data = JsonConvert.DeserializeObject<TData>( jToken.ToString() );
                }
                catch ( Exception ex )
                {
                    result.Success = false;
                    result.CacheData = default ( TData );
                    result.Message   = $"Deserialize Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}{Environment.NewLine}{g.CacheData.ToString()}";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, result.Message );
                    return result;
                }

                result.Success = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), jsonException, result.Message );
                return result;
            }
            catch ( InvalidCastException castException ) 
            {
                result.Success = false;
                result.CacheData = default ( TData );
                result.Message   = $"Get data from in-process memory cache occur exception due to casting error. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), castException, eventTitle: GetLogTraceName( nameof ( Get ) ), message: result.Message );
                return result;
            }
            catch ( Exception ex )
            {
                result.Success = false;
                result.CacheData = default ( TData );
                result.Message   = $"Get data from in-process memory cache occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( Get ) ), message: result.Message );
                return result;
            }

            #endregion 對目標資料進行強制轉型

            #region 設定回傳值屬性

            result.CacheData   = data;
            result.DataId      = g.DataId;
            result.RefreshTime = g.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>根據指定的快取ID從快取池中取出資料
        /// </summary>
        /// <typeparam name="TData">資料模型泛型</typeparam>
        /// <param name="cacheId">快取ID值</param>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
        /// <returns>從快取池中取得資料的結果</returns>
        public static async Task<GetCacheResult<TData>> GetAsync<TData>( string cacheId, GetSomethingFromDataSourceAsync handler = null, int? timeoutMinuntes = null ) 
        {
            #region 初始化回傳值

            var result = new GetCacheResult<TData>() 
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 取得快取池中的目標資料

            GetCacheResult g = await GetAsync( cacheId, handler, timeoutMinuntes );

            if ( !g.Success )
            {
                Message        = g.Message;
                result.Message = g.Message;
                return result;
            }

            result.Message = g.Message;
            result.Success = g.Success;

            #endregion 取得快取池中的目標資料

            #region 對目標資料進行強制轉型

            TData data = default ( TData );

            try
            {
                switch ( g.CacheData )
                {
                    case JToken json:
                        data = json.ToObject<TData>();
                        break;

                    default:
                        data = (TData)g.CacheData;
                        break;
                }
            }
            catch ( JsonSerializationException jsonException )
            {
                try
                {
                    JToken jToken = g.CacheData as JToken;

                    if ( jToken.IsNull() )
                    {
                        result.Success = false;
                        result.CacheData = default ( TData );
                        result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                        Logger.WriteExceptionLog( nameof ( MemoryCache ), jsonException, result.Message );
                        return result;
                    }

                    data = JsonConvert.DeserializeObject<TData>( jToken.ToString() );
                }
                catch ( Exception ex )
                {
                    result.Success = false;
                    result.CacheData = default ( TData );
                    result.Message   = $"Deserialize Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}{Environment.NewLine}{g.CacheData.ToString()}";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, result.Message );
                    return result;
                }

                result.Success = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), jsonException, result.Message );
                return result;
            }
            catch ( InvalidCastException castException ) 
            {
                result.Success = false;
                result.CacheData = default ( TData );
                result.Message   = $"Get data from in-process memory cache occur exception due to casting error. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), castException, eventTitle: GetLogTraceName( nameof ( Get ) ), message: result.Message );
                return result;
            }
            catch ( Exception ex )
            {
                result.Success = false;
                result.CacheData = default ( TData );
                result.Message   = $"Get data from in-process memory cache occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( Get ) ), message: result.Message );
                return result;
            }

            #endregion 對目標資料進行強制轉型

            #region 設定回傳值屬性

            result.CacheData   = data;
            result.DataId      = g.DataId;
            result.RefreshTime = g.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值屬性

            return result;
        }
            

        /// <summary>將指定的資料放入快取池中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入快取池</returns>
        public static bool Put( CacheProperty cacheProperty ) => Put( cacheProperty, out string dataId, out DateTime refreshTime );

        /// <summary>將指定的資料放入快取池中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入快取池</returns>
        public static Task<bool> PutAsync( CacheProperty cacheProperty ) => Task.FromResult( Put( cacheProperty, out string dataId, out DateTime refreshTime ) );

        /// <summary>將指定的資料放入快取池中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <param name="dataId">資料異動識別 ID 代碼，每次更新後資料後改變。</param>
        /// <param name="refreshTime">資料最後更新時間 (資料新增或更新至快取倉儲時的時間戳記)</param>
        /// <returns>是否成功將資料放入快取池</returns>
        public static bool Put( CacheProperty cacheProperty, out string dataId, out DateTime refreshTime )
        {
            #region 初始化輸出參數

            dataId      = null;
            refreshTime = default ( DateTime );

            #endregion 初始化輸出參數

            #region 檢查快取資料儲存屬性值

            if ( cacheProperty.IsNull() )
            {
                Message = $"Augument '{nameof ( cacheProperty )}' can not be null.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Put ) ) );
                return false;
            }

            string cacheId = cacheProperty.CacheId;

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Augument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Put ) ) );
                return false;
            }

            if ( new int?[] { cacheProperty.ExpireInterval, cacheProperty.RefreshInterval }.All( j => j.IsNotNull() && j > 0 ) )
            {
                Message = $"Augument '{nameof ( cacheProperty.ExpireInterval )}' or '{nameof ( cacheProperty.RefreshInterval )}' is invalid.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Put ) ) );
                return false;
            }

            #endregion 檢查快取資料儲存屬性值

            #region 檢查資料快取識別碼

            if ( Contains( cacheId ) )
            {
                Message = $"Duplicate cacheId. Can not put data to in-process memory cache. CacheId: {cacheProperty.CacheId}.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Put ) ) );
                return false;
            }

            #endregion 檢查資料快取識別碼

            #region 設定快取儲存的屬性

            DateTime nowTime          = DateTime.Now;
            cacheProperty.DataId      = RandomTextHelper.Create( 36 );
            cacheProperty.RefreshTime = nowTime;

            int expireInterval = CacheDataSettings.TimeoutInterval;

            if ( cacheProperty.ExpireInterval.IsNotNull() && cacheProperty.ExpireInterval > 0 )
            {
                expireInterval = (int)cacheProperty.ExpireInterval;
            }

            if ( expireInterval > 0 )
            {
                cacheProperty.ExpireInterval = expireInterval;
                cacheProperty.ExpireTime     = nowTime.AddMinutes( expireInterval );
            }

            int refreshInterval = CachePoolSettings.RefreshInterval;

            if ( cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0 )
            {
                refreshInterval = (int)cacheProperty.RefreshInterval;
            }

            bool needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

            if ( needRefresh )
            {
                cacheProperty.RefreshInterval = refreshInterval;
                cacheProperty.ExpireInterval  = null;
                cacheProperty.ExpireTime      = null;

                var timer      = new Timer( TimeSpan.FromMinutes( refreshInterval ).TotalMilliseconds );
                timer.Elapsed += ( sender, e ) => ExecuteRefreshData( cacheId );

                var refreshProperty = new DataRefreshProperty()
                {
                    CacheId        = cacheId,
                    RefreshHandler = cacheProperty.RefreshCallback,
                    RefreshTimer   = timer
                };

                cacheProperty.RefreshProperty = refreshProperty;
            }

            #endregion 設定快取儲存的屬性

            #region 將目標資料存放到快取池中

            using ( _asyncLock.Lock() )
            {
                try
                {
                    _data.Add( cacheId, cacheProperty );

                    if ( !CacheKeyContainer.Add( cacheId ) )
                    {
                        Message = $"Put cacheId into {nameof ( CacheKeyContainer )} fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( Put ) ) );
                        return false;
                    }

                    Check.IsTrue( needRefresh, () => cacheProperty?.RefreshProperty?.RefreshTimer?.Start() );
                }
                catch ( Exception ex )
                {
                    Message = $"Put data to in-process memory cache occur exception: {ex.ToString()}";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( Put ) ), message: Message );
                    return false;
                }
            }

            #endregion 將目標資料存放到快取池中

            #region 設定輸出參數

            dataId      = cacheProperty.DataId;
            refreshTime = cacheProperty.RefreshTime;

            #endregion 設定輸出參數

            return true;
        }

        // 20170630 Pony Says: 暫時保留此方法的原因是: 因為框架內部有其他 module 直接使用...
        /// <summary>將指定的資料放入快取池中
        /// </summary>
        /// <param name="cacheId">快取ID值</param>
        /// <param name="cacheData">目標資料</param>
        /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔 (單位為分鐘)</param>
        /// <param name="refreshHandler">更新記憶體快取池中常駐資料的處理委派 (如果有傳入此委派，資料就會常駐在快取池中)</param>
        /// <returns>是否成功將資料放入快取池</returns>
        public static bool Put( string cacheId, object cacheData, int? timeoutMinuntes = null, RefreshCacheDataHandler refreshHandler = null )
        {
            #region 檢查是否可以將目標資料存放到快取池中

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Put ) ) );
                return false;
            }
            
            if ( Count >= CachePoolSettings.MaxCapcaity )
            {
                Message = "快取池容量已到達 {0} 設定值上限，無法加入資料到快取池中。".FormatTo( CachePoolSettings.MaxCapcaity );
                Logger.WriteWarningLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Put ) ) );
                return false;
            }

            if ( Contains( cacheId ) )
            {
                Message = $"Duplicate cacheId. Can not put data to in-process memory cache. CacheId: {cacheId}.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Put ) ) );
                return false;
            }

            #endregion 檢查是否可以將目標資料存放到快取池中

            #region 初始化快取儲存資訊

            int timeout = timeoutMinuntes.IsNotNull() ? (int)timeoutMinuntes : CacheDataSettings.TimeoutInterval;
            
            var cacheProperty = new CacheProperty() 
            {
                CacheId         = cacheId,
                Data            = cacheData,
                RefreshTime     = DateTime.Now,
                ExpireTime      = DateTime.Now.AddMinutes( timeout ),
                //PullOutTime   = DateTime.Now.AddSeconds( timeout ),    // 要執行Caching的Unit Test要用這一行
                RefreshCallback = refreshHandler,
                RefreshInterval = timeoutMinuntes.IsNull() ? null : timeoutMinuntes
            };

            #endregion 初始化快取儲存資訊

            #region 將目標資料存放到快取池中

            using ( _asyncLock.Lock() )
            {
                try
                {
                    _data.Add( cacheId, cacheProperty );

                    if ( !CacheKeyContainer.Add( cacheId ) )
                    {
                        Message = $"Put cacheId into {nameof ( CacheKeyContainer )} fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( Put ) ) );
                        return false;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Put data to in-process memory cache occur exception: {ex.ToString()}";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( Put ) ), message: Message );
                    return false;
                }
            }

            #endregion 將目標資料存放到快取池中

            return true;
        }

        /// <summary>更新快取池中指定的資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否更新成功</returns>
        public static bool Update( CacheProperty cacheProperty ) => Update( cacheProperty, out string dataId, out DateTime refreshTime );

        /// <summary>更新快取池中指定的資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否更新成功</returns>
        public static Task<bool> UpdateAsync( CacheProperty cacheProperty ) => Task.FromResult( Update( cacheProperty, out string dataId, out DateTime refreshTime ) );

        /// <summary>更新快取池中指定的資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <param name="dataId">資料異動識別 ID 代碼，每次更新後資料後改變。</param>
        /// <param name="refreshTime">資料最後更新時間 (資料新增或更新至快取倉儲時的時間戳記)</param>
        /// <returns>是否更新成功</returns>
        public static bool Update( CacheProperty cacheProperty, out string dataId, out DateTime refreshTime )
        {
            #region 初始化輸出參數

            dataId      = null;
            refreshTime = default ( DateTime );

            #endregion 初始化輸出參數

            #region 檢查快取資料儲存屬性值

            if ( cacheProperty.IsNull() )
            {
                Message = $"Augument '{nameof ( cacheProperty )}' can not be null.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Update ) ) );
                return false;
            }

            string cacheId = cacheProperty.CacheId;

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Augument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Update ) ) );
                return false;
            }

            if ( new int?[] { cacheProperty.ExpireInterval, cacheProperty.RefreshInterval }.All( j => j.IsNotNull() && j > 0 ) )
            {
                Message = $"Augument '{nameof ( cacheProperty.ExpireInterval )}' or '{nameof ( cacheProperty.RefreshInterval )}' is invalid.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Update ) ) );
                return false;
            }

            #endregion 檢查快取資料儲存屬性值

            #region 檢查資料快取識別碼

            if ( !Contains( cacheId ) )
            {
                Message = $"No cache data in in-process memory cache. Can not update data to in-process memory cache. CacheId: {cacheProperty.CacheId}.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( Update ) ) );
                return false;
            }

            #endregion 檢查資料快取識別碼

            #region 設定快取儲存的屬性

            DateTime now              = DateTime.Now;
            cacheProperty.DataId      = RandomTextHelper.Create( 36 );
            cacheProperty.RefreshTime = now;

            int expireInterval = CacheDataSettings.TimeoutInterval;

            if ( cacheProperty.ExpireInterval.IsNotNull() && cacheProperty.ExpireInterval > 0 )
            {
                expireInterval = (int)cacheProperty.ExpireInterval;
            }

            if ( expireInterval > 0 )
            {
                cacheProperty.ExpireInterval = expireInterval;
                cacheProperty.ExpireTime     = now.AddMinutes( expireInterval );
            }

            int refreshInterval = CachePoolSettings.RefreshInterval;

            if ( cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0 )
            {
                refreshInterval = (int)cacheProperty.RefreshInterval;
            }

            bool needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

            if ( needRefresh )
            {
                cacheProperty.RefreshInterval = refreshInterval;
                cacheProperty.ExpireInterval  = null;
                cacheProperty.ExpireTime      = null;

                var timer      = new Timer( TimeSpan.FromMinutes( refreshInterval ).TotalMilliseconds );
                timer.Elapsed += ( sender, e ) => ExecuteRefreshData( cacheId );

                var refreshProperty = new DataRefreshProperty()
                {
                    CacheId        = cacheId,
                    RefreshHandler = cacheProperty.RefreshCallback,
                    RefreshTimer   = timer
                };

                cacheProperty.RefreshProperty = refreshProperty;
            }

            #endregion 設定快取儲存的屬性

            #region 將資料更新至快取池中

            using ( _asyncLock.Lock() )
            {
                try
                {
                    var cacheProp = _data[ cacheId ];

                    cacheProp.RefreshProperty.IsNotNull( p => 
                    {
                        var refreshProp = cacheProp.RefreshProperty;
                        refreshProp.RefreshTimer?.Stop();
                        refreshProp.RefreshTimer?.Dispose();
                        refreshProp.RefreshTimer   = null;
                        refreshProp.RefreshHandler = null;
                        refreshProp.CacheId        = null;
                    } );

                    _data[ cacheId ] = cacheProperty;
                    Check.IsTrue( needRefresh, () => cacheProperty?.RefreshProperty?.RefreshTimer?.Start() );
                }
                catch ( Exception ex )
                {
                    Message = $"Update data to in-process memory cache occur exception. CacheId: {cacheProperty.CacheId}. {ex.ToString()}";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( Update ) ), message: Message );
                    return false;
                }
            }

            #endregion 將資料更新至快取池中

            #region 設定輸出參數

            dataId      = cacheProperty.DataId;
            refreshTime = cacheProperty.RefreshTime;

            #endregion 設定輸出參數

            return true;
        }

        /// <summary>將指定的資料從快取池中移除
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>是否成功移除資料</returns>
        public static bool RemoveData( string cacheId )
        {
            #region 檢查傳入的快取 ID 值

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, "MemoryCache.RemoveData" );
                return false;
            }

            #endregion 檢查傳入的快取 ID 值

            #region 將目標資料從快取池中移除

            using ( _asyncLock.Lock() )
            {
                try
                {
                    if ( !_data.ContainsKey( cacheId ) && !CacheKeyContainer.Exists( cacheId ) )
                    {
                        return true;
                    }

                    var refreshProp = _data[ cacheId ]?.RefreshProperty;

                    refreshProp.IsNotNull( p => 
                    {
                        refreshProp.RefreshTimer?.Stop();
                        refreshProp.RefreshTimer?.Dispose();
                        refreshProp.RefreshTimer   = null;
                        refreshProp.RefreshHandler = null;
                        refreshProp.CacheId        = null;
                    } );

                    if ( !_data.Remove( cacheId ) )
                    {
                        Message = $"Remove data from in-process memory cache fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( RemoveData ) ) );
                        return false;
                    }

                    if ( !CacheKeyContainer.Remove( cacheId ) )
                    {
                        Message = $"Remove cacheId from {nameof ( CacheKeyContainer )} fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), Message, GetLogTraceName( nameof ( RemoveData ) ) );
                        return false;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Remove data from in-process memory cache occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( RemoveData ) ), message: Message );
                    return false;
                }
            }

            #endregion 將目標資料從快取池中移除

            return true;
        }

        /// <summary>將指定的資料從快取池中移除
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>是否成功移除資料</returns>
        public static Task<bool> RemoveDataAsync( string cacheId ) => Task.FromResult( RemoveData( cacheId ) );

        /// <summary>將指定的資料從快取池中移除
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>是否成功移除資料</returns>
        public static bool Remove( string cacheId )
        {
            try
            {
                if ( _data.ContainsKey( cacheId ) )
                {
                    return RemoveData( cacheId );
                }

                if ( _hashData.ContainsKey( cacheId ) )
                {
                    return RemoveHashProperty( cacheId );
                }
            }
            catch ( Exception ex )
            {
                Message = $"Remove data from in-process memory cache occur exception. CacheId: {cacheId}. {ex.ToString()}";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( Remove ) ), message: Message );
                return false;
            }

            return true;
        }

        /// <summary>將指定的資料從快取池中移除
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>是否成功移除資料</returns>
        public static Task<bool> RemoveAsync( string cacheId ) => Task.FromResult( Remove( cacheId ) );

        /// <summary>清空快取池中所有 TokenService 服務中的服務憑證 (Token) 以外的資料
        /// </summary> 
        /// <returns>是否成功清空</returns>
        public static bool Clear()
        {
            using ( _asyncLock.Lock() )
            {
                try
                {
                    _data.Clear();
                }
                catch ( Exception ex )
                {
                    Message = $"Clear all data from in-process memory cache occur exception.";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( Clear ) ), message: Message );
                    return false;
                }

                try
                {
                    _hashData.Clear();
                }
                catch ( Exception ex )
                {
                    Message = $"Clear all data from in-process memory cache occur exception. Clear HashProperty data occur error.";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: GetLogTraceName( nameof ( Clear ) ), message: Message );
                    return false;
                }

                return CacheKeyContainer.Clear();
            }
        }
        
        /// <summary>清空快取池中所有 TokenService 服務中的服務憑證 (Token) 以外的資料
        /// </summary> 
        /// <returns>是否成功清空</returns>
        public static Task<bool> ClearAsync() => Task.FromResult( Clear() );

        /// <summary>檢查快取池中是否包含指定的快取ID的資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>快取池中是否包含指定的快取ID的資料</returns>
        public static bool Contains( string cacheId )
        {
            #region 檢查傳入的快取ID值

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( Contains ) ) );
                return false;
            }

            #endregion 檢查傳入的快取ID值

            #region 檢查快取池中使否包含指定 CacheId 的目標資料

            bool result = false;

            try
            {
                result = CacheKeyContainer.Exists( cacheId );
            }
            catch ( Exception ex )
            {
                Message = $"Check cacheId exists occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( Contains ) ), message: Message );
                return false;
            }

            #endregion 檢查快取池中使否包含指定 CacheId 的目標資料

            return result;
        }

        /// <summary>檢查快取池中是否包含指定的快取ID的資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>快取池中是否包含指定的快取ID的資料</returns>
        public static Task<bool> ContainsAsync( string cacheId ) => Task.FromResult( Contains( cacheId ) );

        /// <summary>檢查 Hash 資料快取池中是否包含指定識別碼的資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>是否包含指定識別碼的資料</returns>
        public static bool ContainsHash( string cacheId )
        {
            #region 檢查傳入的快取資料識別碼

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( ContainsHash ) ) );
                return false;
            }

            #endregion 檢查傳入的快取資料識別碼

            #region 檢查快取池中使否存在指定 CacheId 的目標資料

            bool result = false;

            try
            {
                result = _hashData.ContainsKey( cacheId );
            }
            catch ( Exception ex )
            {
                Message = $"Check HashProperty cacheId exists occur exception: {ex.ToString()}";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( ContainsHash ) ), message: Message );
                return false;
            }

            #endregion 檢查快取池中使否存在指定 CacheId 的目標資料

            return result;
        }

        /// <summary>檢查 Hash 資料快取池中是否包含指定識別碼的資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>是否包含指定識別碼的資料</returns>
        public static Task<bool> ContainsHashAsync( string cacheId ) => Task.FromResult( ContainsHash( cacheId ) );

        /// <summary>取得 HashProperty 資料結構的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>取得資料結果</returns>
        public static GetHashCacheResult GetHashProperty( string cacheId, string subkey )
        {
            #region 初始化回傳值

            var result = new GetHashCacheResult()
            {
                Success = false,
                Data    = null
            };

            object value = null;

            #endregion 初始化回傳值

            #region 檢查傳入的快取資料識別碼

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperty ) ) );
                return result;
            }

            if ( subkey.IsNullOrEmpty() )
            {
                result.Message = $"Argument '{nameof ( subkey )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperty ) ) );
                return result;
            }

            #endregion 檢查傳入的快取資料識別碼

            #region 從記憶體快取池中取得資料

            try
            {
                using ( _asyncLockHash.Lock() )
                {
                    if ( !ContainsHash( cacheId ) && !CacheKeyContainer.Exists( cacheId ) )
                    {
                        result.Message = $"Get hash property data from MemoryCache fail due to does not exist specific cacheId. CacheId: {cacheId}, Subkey: {subkey}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperty ) ) );
                        return result;
                    }

                    Dictionary<string, object> hashFields = _hashData[ cacheId ];

                    if ( hashFields.IsNullOrEmptyCollection() )
                    {
                        result.Message = $"Get hash property data from MemoryCache fail. HashProperty is null or empty. CacheId: {cacheId}, Subkey: {subkey}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperty ) ) );
                        return result;
                    }

                    lock ( hashFields )
                    {
                        if ( !hashFields.ContainsKey( subkey ) )
                        {
                            result.Message = $"Get hash property data from MemoryCache fail due to does not exist specific subkey. CacheId: {cacheId}, Subkey: {subkey}.";
                            Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperty ) ) );
                            return result;
                        }

                        value = hashFields[ subkey ];
                    }

                    result.Data      = value;
                    result.Success = true;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Get HashProperty occur exception. CacheId: {cacheId}, Subkey: {subkey}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperty ) ), message: result.Message );
                return result;
            }

            #endregion 從記憶體快取池中取得資料

            return result;
        }

        /// <summary>取得 HashProperty 資料結構的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>取得資料結果</returns>
        public static Task<GetHashCacheResult> GetHashPropertyAsync( string cacheId, string subkey ) => Task.FromResult( GetHashProperty( cacheId, subkey ) );

        /// <summary>取得 HashProperty 資料集合的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得資料結果</returns>
        public static GetHashPropertiesCacheResult GetHashProperties( string cacheId )
        {
            #region 初始化回傳值

            var result = new GetHashPropertiesCacheResult()
            {
                Success = false,
                Data    = null
            };

            #endregion 初始化回傳值

            #region 檢查傳入的快取資料識別碼

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperties ) ) );
                return result;
            }

            #endregion 檢查傳入的快取資料識別碼

            #region 從記憶體快取池中取得資料

            var hashProperties = new List<HashProperty>();

            try
            {
                using ( _asyncLockHash.Lock() )
                {
                    if ( !ContainsHash( cacheId ) && !CacheKeyContainer.Exists( cacheId ) )
                    {
                        result.Message = $"Get hash properties data from MemoryCache fail due to does not exist specific cacheId. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperties ) ) );
                        return result;
                    }

                    Dictionary<string, object> hashFields = _hashData[ cacheId ];

                    if ( hashFields.IsNullOrEmptyCollection() )
                    {
                        result.Message = $"Get hash properties data from MemoryCache fail. HashProperty is null or empty. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperties ) ) );
                        return result;
                    }

                    foreach ( var hashField in hashFields )
                    {
                        var hashProperty = new HashProperty( hashField.Key, hashField.Value );
                        hashProperties.Add( hashProperty );
                    }
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Get HashProperty occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashProperties ) ), message: result.Message );
                return result;
            }

            #endregion 從記憶體快取池中取得資料

            #region 設定回傳值

            result.Data    = hashProperties.ToArray();
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>取得 HashProperty 資料集合的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得資料結果</returns>
        public static Task<GetHashPropertiesCacheResult> GetHashPropertiesAsync( string cacheId ) => Task.FromResult( GetHashProperties( cacheId ) );

        /// <summary>以 HashProperty 資料結構存放至快取池中。<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">Hash 快取資料結構集合</param>
        /// <returns>存放是否成功</returns>
        public static bool PutHashProperty( string cacheId, HashProperty[] hashProperties )
        {
            #region 檢查傳入的快取資料識別碼

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( PutHashProperty ) ) );
                return false;
            }

            #endregion 檢查傳入的快取資料識別碼

            #region 將資料存放進快取池中

            try
            {
                using ( _asyncLockHash.Lock() )
                {
                    bool exists = _hashData.TryGetValue( cacheId, out Dictionary<string, object> hashFields );

                    if ( !exists )
                    {
                        hashFields = new Dictionary<string, object>();
                    }

                    lock ( hashFields )
                    {
                        foreach ( var hashProperty in hashProperties )
                        {
                            hashFields[ hashProperty.Subkey ] = hashProperty.Value;
                        }
                    }

                    if ( !exists )
                    {
                        _hashData[ cacheId ] = hashFields;
                    }

                    CacheKeyContainer.Add( cacheId );
                }
            }
            catch ( Exception ex )
            {
                Message = $"Put hash property to {nameof ( MemoryCache )} occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( PutHashProperty ) ), message: Message );
                return false;
            }

            #endregion 將資料存放進快取池中

            return true;
        }

        /// <summary>以 HashProperty 資料結構存放至快取池中。<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">Hash 快取資料結構集合</param>
        /// <returns>存放是否成功</returns>
        public static Task<bool> PutHashPropertyAsync( string cacheId, HashProperty[] hashProperties ) => Task.FromResult( PutHashProperty( cacheId, hashProperties ) );

        /// <summary>將指定的 HashProperty 快取資料從快取池中移除。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>移除是否成功</returns>
        public static bool RemoveHashProperty( string cacheId )
        {
            #region 檢查傳入的快取資料識別碼

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( RemoveHashProperty ) ) );
                return false;
            }

            #endregion 檢查傳入的快取資料識別碼

            #region 移除快取資料

            using ( _asyncLockHash.Lock() )
            {
                try
                {
                    #region 檢查資料是否存在

                    if ( !_hashData.ContainsKey( cacheId ) && !CacheKeyContainer.Exists( cacheId ) )
                    {
                        return true;
                    }

                    #endregion 檢查資料是否存在

                    #region 資料移除

                    if ( !_hashData.Remove( cacheId ) )
                    {
                        Message = $"Remove hash property data from in-process memory cache fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( RemoveHashProperty ) ) );
                        return false;
                    }

                    if ( !CacheKeyContainer.Remove( cacheId ) )
                    {
                        Message = $"Remove cachdId from {nameof ( CacheKeyContainer )} fail. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( RemoveHashProperty ) ) );
                        return false;
                    }

                    #endregion 資料移除
                }
                catch ( Exception ex )
                {
                    Message = $"Remove hash property data from in-process memory cache occur exception. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( RemoveHashProperty ) ), message: Message );
                    return false;
                }
            }

            #endregion 移除快取資料

            return true;
        }

        /// <summary>將指定的 HashProperty 快取資料從快取池中移除。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>移除是否成功</returns>
        public static Task<bool> RemoveHashPropertyAsync( string cacheId ) => Task.FromResult( RemoveHashProperty( cacheId ) );

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料，CacheId 不會被整個移除掉。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>移除是否成功</returns>
        public static bool RemoveHashProperty( string cacheId, string subkey )
        {
            #region 檢查傳入的快取資料識別碼

            if ( cacheId.IsNullOrEmpty() )
            {
                Message = $"Argument '{nameof ( cacheId )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( RemoveHashProperty ) ) );
                return false;
            }

            if ( subkey.IsNullOrEmpty() )
            {
                Message = $"Argument '{nameof ( subkey )}' can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( RemoveHashProperty ) ) );
                return false;
            }

            #endregion 檢查傳入的快取資料識別碼

            #region 移除快取資料

            using ( _asyncLockHash.Lock() )
            {
                try
                {
                    #region 檢查資料是否存在

                    if ( !_hashData.ContainsKey( cacheId ) && !CacheKeyContainer.Exists( cacheId ) )
                    {
                        return true;
                    }

                    var hashFields = _hashData[ cacheId ];

                    if ( hashFields.IsNullOrEmptyCollection() || !hashFields.ContainsKey( subkey ) )
                    {
                        return true;
                    }

                    #endregion 檢查資料是否存在

                    #region 資料移除

                    if ( !hashFields.Remove( subkey ) )
                    {
                        Message = $"Remove hash property field from in-process memory cache fail. CacheId: {cacheId}, Subkey: {subkey}.";
                        Logger.WriteErrorLog( nameof ( MemoryCache ), Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( RemoveHashProperty ) ) );
                        return false;
                    }

                    #endregion 資料移除
                }
                catch ( Exception ex )
                {
                    Message = $"Remove hash property data from in-process memory cache occur exception. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, eventTitle: Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( RemoveHashProperty ) ), message: Message );
                    return false;
                }
            }

            #endregion 移除快取資料

            return true;
        }

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料，CacheId 不會被整個移除掉。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>移除是否成功</returns>
        public static Task<bool> RemoveHashPropertyAsync( string cacheId, string subkey ) => Task.FromResult( RemoveHashProperty( cacheId, subkey ) );

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件的資料。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="data">Entity 資料物件</param>
        /// <returns>存放資料結果</returns>
        public static Result PutHashObject( string cacheId, object data )
        {
            var result = Result.Create();

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"Invalid '{nameof ( cacheId )}' argument. Can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( PutHashObject ) ) );
                return result;
            }

            if ( data.IsNull() )
            {
                result.Message = $"Invalid '{nameof ( data )}' argument. Not allowed put null value to in-process memory cache.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( PutHashObject ) ) );
                return result;
            }

            if ( data is string d && d.IsNullOrEmpty() )
            {
                result.Message = $"Invalid '{nameof ( data )}' argument. Not allowed put null empty string to in-process memory cache.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( PutHashObject ) ) );
                return result;
            }

            HashProperty[] hashProperties;

            try
            {
                hashProperties = HashPropertyConverter.ConvertTo( data );
            }
            catch ( Exception ex )
            {
                result.Message = $"Put hash data to in-process memory cache occur exception. CacheId: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( PutHashObject ) ), result.Message );
                return result;
            }

            if ( !PutHashProperty( cacheId, hashProperties ) )
            {
                result.Message = $"Put hash data to in-process memory occur error. CacheId: {cacheId}. {Message}";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( PutHashObject ) ) );
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件的資料。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="data">Entity 資料物件</param>
        /// <returns>存放資料結果</returns>
        public static Task<Result> PutHashObjectAsync( string cacheId, object data ) => Task.FromResult( PutHashObject( cacheId, data ) );

        /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得結果</returns>
        public static Result<TData> GetHashObject<TData>( string cacheId )
        {
            var result = Result.Create<TData>();

            if ( cacheId.IsNullOrEmpty() )
            {
                result.Message = $"Invalid '{nameof ( cacheId )}' argument. Can not be null or empty string.";
                Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashObject ) ) );
                return result;
            }

            TData data = default ( TData );

            try
            {
                if ( !ContainsHash( cacheId ) )
                {
                    return result;
                }

                var hashProperties = _hashData[ cacheId ];

                if ( hashProperties.IsNullOrEmptyCollection() )
                {
                    result.Message = $"No data found from in-process memory cache. Key: {cacheId}.";
                    Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashObject ) ) );
                    return result;
                }

                data = HashPropertyConverter.ConvertTo<TData>( hashProperties );

                if ( data.IsNull() )
                {
                    result.Message = $"Convert HashProperty dictionary retrive from in-process memory cache to '{typeof ( TData ).FullName}' object occur error. Key: {cacheId}.";
                    Logger.WriteErrorLog( nameof ( MemoryCache ), result.Message, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashObject ) ) );
                    return result;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Get hash data from in-process memory cache occur exception. Key: {cacheId}.";
                Logger.WriteExceptionLog( nameof ( MemoryCache ), ex, Logger.GetTraceLogTitle( nameof ( MemoryCache ), nameof ( GetHashObject ) ), result.Message );
                return result;
            }

            result.Data    = data;
            result.Success = true;
            return result;
        }

        /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得結果</returns>
        public static Task<Result<TData>> GetHashObjectAsync<TData>( string cacheId ) => Task.FromResult( GetHashObject<TData>( cacheId ) );

        #endregion 公開的靜態方法
    }
}
