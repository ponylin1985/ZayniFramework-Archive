﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Client;
using ZayniFramework.Middle.Service.Entity;
using ST = System.Timers;


namespace ZayniFramework.Caching
{
    /// <summary>遠端資料快取
    /// </summary>
    public sealed class RemoteCache : RemoteProxy
    {
        #region Private Fields

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>非同步作業鎖定物件: 鎖定 _message 欄位
        /// </summary>
        private readonly AsyncLock _asyncLockMessage = new AsyncLock();

        /// <summary>訊息
        /// </summary>
        private string _message;

        /// <summary>遠端快取服務的客戶端代理設定名稱
        /// </summary>
        private string _serviceClientName;

        /// <summary>資料自動更新的參數集合
        /// </summary>
        private readonly Dictionary<string, DataRefreshProperty> _refreshProperties = new Dictionary<string, DataRefreshProperty>();

        #endregion Private Fields


        #region Constructors

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">遠端快取服務的客戶端代理設定名稱</param>
        /// <param name="serviceClientConfigPath">RemoteCache 的服務客戶端 serviceClientConfig.json 設定檔的相對路徑</param>
        public RemoteCache( string serviceClientName, string serviceClientConfigPath = null ) : base( serviceClientName, serviceClientConfigPath )
        {
            _serviceClientName = serviceClientName;
            ConnectionBroken   = OnConnectionBroken;
            ConnectionRestored = OnConnectionRestored;
        }

        /// <summary>解構子
        /// </summary>
        ~RemoteCache()
        {
            _serviceClientName = null;
            ConnectionBroken   = null;
            ConnectionRestored = null;
        }

        #endregion Constructors


        #region Properties

        /// <summary>訊息
        /// </summary>
        public string Message
        {
            get
            {
                using ( _asyncLockMessage.Lock() )
                {
                    return _message;
                }
            }
            set
            {
                using ( _asyncLockMessage.Lock() )
                {
                    _message = value;
                }
            }
        }

        /// <summary>快取池中目前存放的資料個數
        /// </summary>
        public int Count => GetCount();

        #endregion Properties


        #region Internal Methods

        /// <summary>取得訊息
        /// </summary>
        /// <returns>失敗/錯誤訊息</returns>
        internal string GetMessage() => Message;

        /// <summary>設定訊息
        /// </summary>
        /// <param name="value">失敗/錯誤訊息</param>
        internal void SetMessage( string value ) => Message = value;

        #endregion Internal Methods


        #region Public Methdos

        /// <summary>取得快取料 Key 值的目前筆數
        /// </summary>
        /// <returns>快取料 Key 值的目前筆數</returns>
        public int GetCount()
        {
            var r = Execute<FooDTO, int>( "Count", new FooDTO() );

            if ( !r.Success )
            {
                Logger.WriteErrorLog( this, $"Get cache key count from remote cache fail. {r.Message}", "Get cache key count from remote cache fail." );
                return -1;
            }

            int count = r.Data;
            return count;
        }
        
        /// <summary>取得快取倉儲中的 Key 值集合
        /// </summary>
        /// <param name="pattern">Key 值的規則</param>
        /// <returns>取得結果</returns>
        public Result<List<string>> GetCacheKeys( string pattern = null )
        {
            var result = Result.Create<List<string>>();
            var reqDTO = new GetCacheKeysReqDTO() { Pattern = pattern };

            var r = Execute<GetCacheKeysReqDTO, GetCacheKeysResDTO>( "GetCacheKeys", reqDTO );

            if ( !r.Success )
            {
                result.Message = $"Get all cache keys from remote cache repository fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, "Get cache keys from remote cache fail." );
                return result;
            }

            GetCacheKeysResDTO resDTO = r.Data;

            if ( resDTO.IsNull() )
            {
                result.Message = $"Get all cache keys from remote cache repository fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, "Get cache keys from remote cache fail." );
                return result;
            }

            List<string> cacheKeys = resDTO.CacheKeys;

            result.Data    = cacheKeys;
            result.Success = true;
            return result;
        }

        /// <summary>取得指定資料識別代碼的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別代碼</param>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
        /// <returns>資料快取的取得結果</returns>
        public GetCacheResult Get( string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult()
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 嘗試從 RemoteCache 取得資料

            var reqDTO = new CacheReqDTO()
            {
                CacheId = cacheId
            };

            var r = Execute<CacheReqDTO, GetCacheResult>( "Get", reqDTO );

            if ( r.Success && r.Data.Success )
            {
                result = r.Data;
                return result;
            }

            if ( handler.IsNull() )
            {
                result.Message = $"No data from remote cache service. CacheId: {cacheId}. RemoteCache ServiceClient: {_serviceClientName}.";
                Logger.WriteErrorLog( this, result.Message, "Get cache data from remote cache fail." );
                return result;
            }

            #endregion 嘗試從 RemoteCache 取得資料

            #region 執行取得資料的委派

            object data = null;

            try
            {
                data = handler?.Invoke();
            }
            catch ( Exception ex )
            {
                result.Message = $"Get cache data from GetSomethingFromDataSource occur exception. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, "Get cache data from remote cache fail. Get cache data from GetSomethingFromDataSource occur exception." );
                return result;
            }

            if ( data.IsNull() )
            {
                result.Message = $"Get cache data from GetSomethingFromDataSource fail Retrive null data.";
                Logger.WriteErrorLog( this, result.Message, "Get cache data from remote cache fail." );
                return result;
            }

            #endregion 執行取得資料的委派

            #region 重新將資料放入 RemoteCache 服務中

            var cacheProperty = new CacheProperty()
            {
                CacheId     = cacheId,
                Data        = data,
                DataId      = RandomTextHelper.Create( 36 ),
                RefreshTime = DateTime.Now
            };

            var p = Execute<CacheProperty>( "Put", cacheProperty );

            if ( !r.Success )
            {
                Message = p.Message;
                Logger.WriteErrorLog( this, $"Put data to remote cache repository fail. {p.Message}", "Get data from remote cache fail. Put data to remote cache fail." );
                return result;
            }

            #endregion 重新將資料放入 RemoteCache 服務中

            #region 設定回傳值

            result.CacheData   = data;
            result.DataId      = cacheProperty.DataId;
            result.RefreshTime = cacheProperty.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值
            
            return result;
        }

        /// <summary>取得指定資料識別代碼的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別代碼</param>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
        /// <returns>資料快取的取得結果</returns>
        public async Task<GetCacheResult> GetAsync( string cacheId, GetSomethingFromDataSourceAsync handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult()
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 嘗試從 RemoteCache 取得資料

            var reqDTO = new CacheReqDTO()
            {
                CacheId = cacheId
            };

            var r = await ExecuteAsync<CacheReqDTO, GetCacheResult>( "GetAsync", reqDTO );

            if ( r.Success && r.Data.Success )
            {
                result = r.Data;
                return result;
            }

            if ( handler.IsNull() )
            {
                result.Message = $"No data from remote cache service. CacheId: {cacheId}. RemoteCache ServiceClient: {_serviceClientName}.";
                Logger.WriteErrorLog( this, result.Message, "Get cache data from remote cache fail." );
                return result;
            }

            #endregion 嘗試從 RemoteCache 取得資料

            #region 執行取得資料的委派

            object data = null;

            try
            {
                data = await handler?.Invoke();
            }
            catch ( Exception ex )
            {
                result.Message = $"Get cache data from GetSomethingFromDataSource occur exception. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, "Get cache data from remote cache fail. Get cache data from GetSomethingFromDataSource occur exception." );
                return result;
            }

            if ( data.IsNull() )
            {
                result.Message = $"Get cache data from GetSomethingFromDataSource fail Retrive null data.";
                Logger.WriteErrorLog( this, result.Message, "Get cache data from remote cache fail." );
                return result;
            }

            #endregion 執行取得資料的委派

            #region 重新將資料放入 RemoteCache 服務中

            var cacheProperty = new CacheProperty()
            {
                CacheId     = cacheId,
                Data        = data,
                DataId      = RandomTextHelper.Create( 36 ),
                RefreshTime = DateTime.Now
            };

            var p = await ExecuteAsync<CacheProperty>( "PutAsync", cacheProperty );

            if ( !r.Success )
            {
                Message = p.Message;
                Logger.WriteErrorLog( this, $"Put data to remote cache repository fail. {p.Message}", "Get data from remote cache fail. Put data to remote cache fail." );
                return result;
            }

            #endregion 重新將資料放入 RemoteCache 服務中

            #region 設定回傳值

            result.CacheData   = data;
            result.DataId      = cacheProperty.DataId;
            result.RefreshTime = cacheProperty.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值
            
            return result;
        }

        /// <summary>取得指定資料識別代碼的快取資料
        /// </summary>
        /// <typeparam name="TData">資料內容的泛型</typeparam>
        /// <param name="cacheId">快取資料識別代碼</param>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
        /// <returns>資料快取的取得結果</returns>
        public GetCacheResult<TData> Get<TData>( string cacheId, GetSomethingFromDataSource handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult<TData>()
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 取得快取池中的目標資料

            GetCacheResult g = Get( cacheId, handler, timeoutMinuntes );

            if ( !g.Success )
            {
                Message        = g.Message;
                result.Message = g.Message;
                return result;
            }

            result.Message = g.Message;
            result.Success = g.Success;

            #endregion 取得快取池中的目標資料

            #region 對目標資料進行強制轉型

            TData data = default ( TData );

            try
            {
                switch ( g.CacheData )
                {
                    case JToken json:
                        data = json.ToObject<TData>();
                        break;

                    default:
                        data = (TData)g.CacheData;
                        break;
                }
            }
            catch ( JsonSerializationException jsonException )
            {
                try
                {
                    JToken jToken = g.CacheData as JToken;

                    if ( jToken.IsNull() )
                    {
                        result.Success   = false;
                        result.CacheData = default ( TData );
                        result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                        Logger.WriteExceptionLog( this, jsonException, result.Message );
                        return result;
                    }

                    data = JsonConvert.DeserializeObject<TData>( jToken.ToString() );
                }
                catch ( Exception ex )
                {
                    result.Success   = false;
                    result.CacheData = default ( TData );
                    result.Message   = $"Deserialize Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}{Environment.NewLine}{g.CacheData.ToString()}";
                    Logger.WriteExceptionLog( this, ex, result.Message );
                    return result;
                }

                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                Logger.WriteExceptionLog( this, jsonException, result.Message );
                return result;
            }
            catch ( InvalidCastException castException ) 
            {
                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting data object occur exception. CacheId: {cacheId}. {castException.ToString()}";
                Logger.WriteExceptionLog( this, castException, result.Message );
                return result;
            }
            catch ( Exception ex )
            {
                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Get cache data occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, result.Message );
                return result;
            }

            #endregion 對目標資料進行強制轉型

            #region 設定回傳值屬性

            result.CacheData   = data;
            result.DataId      = g.DataId;
            result.RefreshTime = g.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>取得指定資料識別代碼的快取資料
        /// </summary>
        /// <typeparam name="TData">資料內容的泛型</typeparam>
        /// <param name="cacheId">快取資料識別代碼</param>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <param name="timeoutMinuntes">資料逾時時間間隔 或 更新資料時間間隔(單位為分鐘)</param>
        /// <returns>資料快取的取得結果</returns>
        public async Task<GetCacheResult<TData>> GetAsync<TData>( string cacheId, GetSomethingFromDataSourceAsync handler = null, int? timeoutMinuntes = null )
        {
            #region 初始化回傳值

            var result = new GetCacheResult<TData>()
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 取得快取池中的目標資料

            GetCacheResult g = await GetAsync( cacheId, handler, timeoutMinuntes );

            if ( !g.Success )
            {
                Message        = g.Message;
                result.Message = g.Message;
                return result;
            }

            result.Message = g.Message;
            result.Success = g.Success;

            #endregion 取得快取池中的目標資料

            #region 對目標資料進行強制轉型

            TData data = default ( TData );

            try
            {
                switch ( g.CacheData )
                {
                    case JToken json:
                        data = json.ToObject<TData>();
                        break;

                    default:
                        data = (TData)g.CacheData;
                        break;
                }
            }
            catch ( JsonSerializationException jsonException )
            {
                try
                {
                    JToken jToken = g.CacheData as JToken;

                    if ( jToken.IsNull() )
                    {
                        result.Success   = false;
                        result.CacheData = default ( TData );
                        result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                        Logger.WriteExceptionLog( this, jsonException, result.Message );
                        return result;
                    }

                    data = JsonConvert.DeserializeObject<TData>( jToken.ToString() );
                }
                catch ( Exception ex )
                {
                    result.Success   = false;
                    result.CacheData = default ( TData );
                    result.Message   = $"Deserialize Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}{Environment.NewLine}{g.CacheData.ToString()}";
                    Logger.WriteExceptionLog( this, ex, result.Message );
                    return result;
                }

                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting Json.NET object data to {nameof ( TData )} occur exception. CacheId: {cacheId}. {Environment.NewLine}{jsonException.ToString()}";
                Logger.WriteExceptionLog( this, jsonException, result.Message );
                return result;
            }
            catch ( InvalidCastException castException ) 
            {
                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Casting data object occur exception. CacheId: {cacheId}. {castException.ToString()}";
                Logger.WriteExceptionLog( this, castException, result.Message );
                return result;
            }
            catch ( Exception ex )
            {
                result.Success   = false;
                result.CacheData = default ( TData );
                result.Message   = $"Get cache data occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, result.Message );
                return result;
            }

            #endregion 對目標資料進行強制轉型

            #region 設定回傳值屬性

            result.CacheData   = data;
            result.DataId      = g.DataId;
            result.RefreshTime = g.RefreshTime;
            result.Success     = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>將指定的資料放入快取倉儲中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入快取倉儲中</returns>
        public bool Put( CacheProperty cacheProperty )
        {
            using ( _asyncLock.Lock() )
            {
                #region 設定快取儲存的屬性

                string cacheId = cacheProperty.CacheId;

                DateTime nowTime          = DateTime.Now;
                cacheProperty.RefreshTime = nowTime;
                cacheProperty.DataId      = RandomTextHelper.Create( 36 );

                int refreshInterval = CachePoolSettings.RefreshInterval;

                if ( cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0 )
                {
                    refreshInterval = (int)cacheProperty.RefreshInterval;
                }

                bool needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

                if ( needRefresh )
                {
                    cacheProperty.RefreshInterval = refreshInterval;
                    cacheProperty.ExpireInterval  = null;
                    cacheProperty.ExpireTime      = null;
                }

                #endregion 設定快取儲存的屬性

                #region 存放資料到 RemoteCache 服務

                var r = Execute<CacheProperty>( "Put", cacheProperty );

                if ( !r.Success )
                {
                    Message = r.Message;
                    Logger.WriteErrorLog( this, $"Put data to remote cache repository fail. {r.Message}", "Put data to remote cache fail." );
                    return false;
                }

                #endregion 存放資料到 RemoteCache 服務

                #region 註冊快取資料的 RefreshCacheDataHandler 處理委派

                if ( !needRefresh )
                {
                    return true;
                }

                var timer      = new ST.Timer( TimeSpan.FromMinutes( refreshInterval ).TotalMilliseconds );
                timer.Elapsed += ( sender, e ) => ExecuteRefreshData( cacheId );

                var refreshProperty = new DataRefreshProperty()
                {
                    CacheId        = cacheId,
                    RefreshHandler = cacheProperty.RefreshCallback,
                    RefreshTimer   = timer
                };

                try
                {
                    if ( !_refreshProperties.ContainsKey( cacheId ) )
                    {
                        _refreshProperties.Add( cacheId, refreshProperty );
                    }
                    else
                    {
                        var refreshProp = _refreshProperties[ cacheId ];

                        refreshProp.IsNotNull( p => 
                        {
                            refreshProp.RefreshTimer?.Stop();
                            refreshProp.RefreshTimer?.Dispose();
                            refreshProp.RefreshTimer   = null;
                            refreshProp.RefreshHandler = null;
                            refreshProp.CacheId        = null;
                        } );

                        _refreshProperties[ cacheId ] = refreshProperty;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Add refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteExceptionLog( this, ex, Message, "Put data to remote cache fail." );
                    return false;
                }

                timer.Start();

                #endregion 註冊快取資料的 RefreshCacheDataHandler 處理委派

                return true;
            }
        }

        /// <summary>將指定的資料放入快取倉儲中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入快取倉儲中</returns>
        public async Task<bool> PutAsync( CacheProperty cacheProperty )
        {
            using ( await _asyncLock.LockAsync() )
            {
                #region 設定快取儲存的屬性

                string cacheId = cacheProperty.CacheId;

                DateTime nowTime          = DateTime.Now;
                cacheProperty.RefreshTime = nowTime;
                cacheProperty.DataId      = RandomTextHelper.Create( 36 );

                int refreshInterval = CachePoolSettings.RefreshInterval;

                if ( cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0 )
                {
                    refreshInterval = (int)cacheProperty.RefreshInterval;
                }

                bool needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

                if ( needRefresh )
                {
                    cacheProperty.RefreshInterval = refreshInterval;
                    cacheProperty.ExpireInterval  = null;
                    cacheProperty.ExpireTime      = null;
                }

                #endregion 設定快取儲存的屬性

                #region 存放資料到 RemoteCache 服務

                var r = await ExecuteAsync<CacheProperty>( "PutAsync", cacheProperty );

                if ( !r.Success )
                {
                    Message = r.Message;
                    Logger.WriteErrorLog( this, $"Put data to remote cache repository fail. {r.Message}", "Put data to remote cache fail." );
                    return false;
                }

                #endregion 存放資料到 RemoteCache 服務

                #region 註冊快取資料的 RefreshCacheDataHandler 處理委派

                if ( !needRefresh )
                {
                    return true;
                }

                var timer      = new ST.Timer( TimeSpan.FromMinutes( refreshInterval ).TotalMilliseconds );
                timer.Elapsed += ( sender, e ) => ExecuteRefreshData( cacheId );

                var refreshProperty = new DataRefreshProperty()
                {
                    CacheId        = cacheId,
                    RefreshHandler = cacheProperty.RefreshCallback,
                    RefreshTimer   = timer
                };

                try
                {
                    if ( !_refreshProperties.ContainsKey( cacheId ) )
                    {
                        _refreshProperties.Add( cacheId, refreshProperty );
                    }
                    else
                    {
                        var refreshProp = _refreshProperties[ cacheId ];

                        refreshProp.IsNotNull( p => 
                        {
                            refreshProp.RefreshTimer?.Stop();
                            refreshProp.RefreshTimer?.Dispose();
                            refreshProp.RefreshTimer   = null;
                            refreshProp.RefreshHandler = null;
                            refreshProp.CacheId        = null;
                        } );

                        _refreshProperties[ cacheId ] = refreshProperty;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Add refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteExceptionLog( this, ex, Message, "Put data to remote cache fail." );
                    return false;
                }

                timer.Start();

                #endregion 註冊快取資料的 RefreshCacheDataHandler 處理委派

                return true;
            }
        }

        /// <summary>更新指定的資料到快取倉儲中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料更新至快取倉儲中</returns>
        public bool Update( CacheProperty cacheProperty )
        {
            using ( _asyncLock.Lock() )
            {
                #region 設定快取儲存的屬性

                string cacheId = cacheProperty.CacheId;

                DateTime nowTime          = DateTime.Now;
                cacheProperty.RefreshTime = nowTime;
                cacheProperty.DataId      = RandomTextHelper.Create( 36 );

                int refreshInterval = CachePoolSettings.RefreshInterval;

                if ( cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0 )
                {
                    refreshInterval = (int)cacheProperty.RefreshInterval;
                }

                bool needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

                if ( needRefresh )
                {
                    cacheProperty.RefreshInterval = refreshInterval;
                    cacheProperty.ExpireInterval  = null;
                    cacheProperty.ExpireTime      = null;
                }

                #endregion 設定快取儲存的屬性

                #region 存放資料到 RemoteCache 服務

                var r = Execute<CacheProperty>( "Update", cacheProperty );

                if ( !r.Success )
                {
                    Message = r.Message;
                    Logger.WriteErrorLog( this, $"Update data to remote cache repository fail. {r.Message}", "Update data to remote cache fail." );
                    return false;
                }

                #endregion 存放資料到 RemoteCache 服務

                #region 註冊快取資料的 RefreshCacheDataHandler 處理委派

                if ( !needRefresh )
                {
                    return true;
                }

                var timer      = new ST.Timer( TimeSpan.FromMinutes( refreshInterval ).TotalMilliseconds );
                timer.Elapsed += ( sender, e ) => ExecuteRefreshData( cacheId );

                var refreshProperty = new DataRefreshProperty()
                {
                    CacheId        = cacheId,
                    RefreshHandler = cacheProperty.RefreshCallback,
                    RefreshTimer   = timer
                };

                try
                {
                    if ( !_refreshProperties.ContainsKey( cacheId ) )
                    {
                        _refreshProperties.Add( cacheId, refreshProperty );
                    }
                    else
                    {
                        var refreshProp = _refreshProperties[ cacheId ];

                        refreshProp.IsNotNull( p => 
                        {
                            refreshProp.RefreshTimer?.Stop();
                            refreshProp.RefreshTimer?.Dispose();
                            refreshProp.RefreshTimer   = null;
                            refreshProp.RefreshHandler = null;
                            refreshProp.CacheId        = null;
                        } );

                        _refreshProperties[ cacheId ] = refreshProperty;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Add refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteExceptionLog( this, ex, Message, "Update remote cache fail." );
                    return false;
                }

                timer.Start();

                #endregion 註冊快取資料的 RefreshCacheDataHandler 處理委派

                return true;
            }
        }

        /// <summary>更新指定的資料到快取倉儲中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料更新至快取倉儲中</returns>
        public async Task<bool> UpdateAsync( CacheProperty cacheProperty )
        {
            using ( await _asyncLock.LockAsync() )
            {
                #region 設定快取儲存的屬性

                string cacheId = cacheProperty.CacheId;

                DateTime nowTime          = DateTime.Now;
                cacheProperty.RefreshTime = nowTime;
                cacheProperty.DataId      = RandomTextHelper.Create( 36 );

                int refreshInterval = CachePoolSettings.RefreshInterval;

                if ( cacheProperty.RefreshInterval.IsNotNull() && cacheProperty.RefreshInterval > 0 )
                {
                    refreshInterval = (int)cacheProperty.RefreshInterval;
                }

                bool needRefresh = refreshInterval > 0 && cacheProperty.RefreshCallback.IsNotNull();

                if ( needRefresh )
                {
                    cacheProperty.RefreshInterval = refreshInterval;
                    cacheProperty.ExpireInterval  = null;
                    cacheProperty.ExpireTime      = null;
                }

                #endregion 設定快取儲存的屬性

                #region 存放資料到 RemoteCache 服務

                var r = await ExecuteAsync<CacheProperty>( "UpdateAsync", cacheProperty );

                if ( !r.Success )
                {
                    Message = r.Message;
                    Logger.WriteErrorLog( this, $"Update data to remote cache repository fail. {r.Message}", "Update data to remote cache fail." );
                    return false;
                }

                #endregion 存放資料到 RemoteCache 服務

                #region 註冊快取資料的 RefreshCacheDataHandler 處理委派

                if ( !needRefresh )
                {
                    return true;
                }

                var timer      = new ST.Timer( TimeSpan.FromMinutes( refreshInterval ).TotalMilliseconds );
                timer.Elapsed += ( sender, e ) => ExecuteRefreshData( cacheId );

                var refreshProperty = new DataRefreshProperty()
                {
                    CacheId        = cacheId,
                    RefreshHandler = cacheProperty.RefreshCallback,
                    RefreshTimer   = timer
                };

                try
                {
                    if ( !_refreshProperties.ContainsKey( cacheId ) )
                    {
                        _refreshProperties.Add( cacheId, refreshProperty );
                    }
                    else
                    {
                        var refreshProp = _refreshProperties[ cacheId ];

                        refreshProp.IsNotNull( p => 
                        {
                            refreshProp.RefreshTimer?.Stop();
                            refreshProp.RefreshTimer?.Dispose();
                            refreshProp.RefreshTimer   = null;
                            refreshProp.RefreshHandler = null;
                            refreshProp.CacheId        = null;
                        } );

                        _refreshProperties[ cacheId ] = refreshProperty;
                    }
                }
                catch ( Exception ex )
                {
                    Message = $"Add refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteExceptionLog( this, ex, Message, "Update remote cache fail." );
                    return false;
                }

                timer.Start();

                #endregion 註冊快取資料的 RefreshCacheDataHandler 處理委派

                return true;
            }
        }

        /// <summary>將指定的資料從快取池中移除
        /// </summary>
        /// <param name="cacheId">快取資料識別代碼</param>
        /// <returns>是否成功移除資料</returns>
        public bool Remove( string cacheId )
        {
            using ( _asyncLock.Lock() )
            {
                #region 將目標資料從 RemoteCache 移除

                var reqDTO = new CacheReqDTO() { CacheId = cacheId };

                var r = Execute<CacheReqDTO>( "Remove", reqDTO );

                if ( !r.Success )
                {
                    Logger.WriteErrorLog( this, $"Remove cache data from remote cache repository fail. {r.Message}", "Remove cache data from remote cache fail." );
                    return false;
                }

                #endregion 將目標資料從 RemoteCache 移除

                #region 移除 RefreshCacheDataHandler 處理委派

                try
                {
                    // 並不是所有的快取資料都有要自動更新機制!
                    if ( !_refreshProperties.TryGetValue( cacheId, out var refreshProperty ) )
                    {
                        return true;
                    }

                    if ( refreshProperty.IsNull() )
                    {
                        return true;
                    }

                    refreshProperty.RefreshTimer.Stop();
                    refreshProperty.RefreshTimer.Dispose();

                    refreshProperty.CacheId        = null;
                    refreshProperty.RefreshHandler = null;
                    refreshProperty.RefreshTimer   = null;
                }
                catch ( Exception ex )
                {
                    Message = $"Remove and dispose refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"Remove data from remote cache occur error." );
                    return true;
                }

                #endregion 移除 RefreshCacheDataHandler 處理委派

                return true;
            }
        }

        /// <summary>將指定的資料從快取池中移除
        /// </summary>
        /// <param name="cacheId">快取資料識別代碼</param>
        /// <returns>是否成功移除資料</returns>
        public async Task<bool> RemoveAsync( string cacheId )
        {
            using ( await _asyncLock.LockAsync() )
            {
                #region 將目標資料從 RemoteCache 移除

                var reqDTO = new CacheReqDTO() { CacheId = cacheId };

                var r = await ExecuteAsync<CacheReqDTO>( "RemoveAsync", reqDTO );

                if ( !r.Success )
                {
                    Logger.WriteErrorLog( this, $"Remove cache data from remote cache repository fail. {r.Message}", "Remove cache data from remote cache fail." );
                    return false;
                }

                #endregion 將目標資料從 RemoteCache 移除

                #region 移除 RefreshCacheDataHandler 處理委派

                try
                {
                    // 並不是所有的快取資料都有要自動更新機制!
                    if ( !_refreshProperties.TryGetValue( cacheId, out var refreshProperty ) )
                    {
                        return true;
                    }

                    if ( refreshProperty.IsNull() )
                    {
                        return true;
                    }

                    refreshProperty.RefreshTimer.Stop();
                    refreshProperty.RefreshTimer.Dispose();

                    refreshProperty.CacheId        = null;
                    refreshProperty.RefreshHandler = null;
                    refreshProperty.RefreshTimer   = null;
                }
                catch ( Exception ex )
                {
                    Message = $"Remove and dispose refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}";
                    Logger.WriteErrorLog( this, Message, $"Remove data from remote cache occur error." );
                    return true;
                }

                #endregion 移除 RefreshCacheDataHandler 處理委派

                return true;
            }
        }

        /// <summary>檢查是否包含指定 CacheId 的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別代碼</param>
        /// <returns>是否包含指定的資料</returns>
        public bool Contains( string cacheId )
        {
            var reqDTO = new CacheReqDTO() { CacheId = cacheId };

            var r = Execute<CacheReqDTO, bool>( "Contains", reqDTO );

            if ( !r.Success )
            {
                Logger.WriteErrorLog( this, $"Check contains cache data from remote cache repository fail. {r.Message}", "Check cache data from remote cache fail." );
                return false;
            }

            bool exists = r.Data;
            return exists;
        }

        /// <summary>檢查是否包含指定 CacheId 的快取資料
        /// </summary>
        /// <param name="cacheId">快取資料識別代碼</param>
        /// <returns>是否包含指定的資料</returns>
        public async Task<bool> ContainsAsync( string cacheId )
        {
            var reqDTO = new CacheReqDTO() { CacheId = cacheId };

            var r = await ExecuteAsync<CacheReqDTO, bool>( "ContainsAsync", reqDTO );

            if ( !r.Success )
            {
                Logger.WriteErrorLog( this, $"Check contains cache data from remote cache repository fail. {r.Message}", "Check cache data from remote cache fail." );
                return false;
            }

            bool exists = r.Data;
            return exists;
        }

        /// <summary>清空所有的快取資料
        /// </summary>
        /// <returns>是否成功的清空所有資料</returns>
        public bool Clear()
        {
            var r = Execute<FooDTO>( "Clear", new FooDTO() );

            if ( !r.Success )
            {
                Logger.WriteErrorLog( this, $"Clear all cache data from remote cache repository fail. {r.Message}", "Clear data from remote cache fail." );
                return false;
            }

            return true;
        }

        /// <summary>清空所有的快取資料
        /// </summary>
        /// <returns>是否成功的清空所有資料</returns>
        public async Task<bool> ClearAsync()
        {
            var r = await ExecuteAsync<FooDTO>( "ClearAsync", new FooDTO() );

            if ( !r.Success )
            {
                Logger.WriteErrorLog( this, $"Clear all cache data from remote cache repository fail. {r.Message}", "Clear data from remote cache fail." );
                return false;
            }

            return true;
        }

        /// <summary>取得 HashProperty 資料結構的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>取得資料結果</returns>
        public GetHashCacheResult GetHashProperty( string cacheId, string subkey )
        {
            var result = new GetHashCacheResult()
            {
                Success = false,
                Data    = null
            };

            var reqDTO = new HashPropertyReqDTO()
            {
                CacheId = cacheId,
                Subkey  = subkey
            };

            var r = Execute<HashPropertyReqDTO, object>( "GetHashProperty", reqDTO );

            if ( !r.Success )
            {
                result.Code    = r.Code;
                result.Message = r.Message;
                Logger.WriteErrorLog( this, $"Get hash property data from remote cache repository fail. CacheId: {cacheId}, Subkey: {subkey}. {r.Message}", Logger.GetTraceLogTitle( this, nameof ( GetHashProperty ) ) );
                return result;
            }

            object data = r.Data;

            result.Data    = data;
            result.Success = true;
            return result;
        }

        /// <summary>取得 HashProperty 資料結構的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>取得資料結果</returns>
        public async Task<GetHashCacheResult> GetHashPropertyAsync( string cacheId, string subkey )
        {
            var result = new GetHashCacheResult()
            {
                Success = false,
                Data    = null
            };

            var reqDTO = new HashPropertyReqDTO()
            {
                CacheId = cacheId,
                Subkey  = subkey
            };

            var r = await ExecuteAsync<HashPropertyReqDTO, object>( "GetHashPropertyAsync", reqDTO );

            if ( !r.Success )
            {
                result.Code    = r.Code;
                result.Message = r.Message;
                Logger.WriteErrorLog( this, $"Get hash property data from remote cache repository fail. CacheId: {cacheId}, Subkey: {subkey}. {r.Message}", Logger.GetTraceLogTitle( this, nameof ( GetHashPropertyAsync ) ) );
                return result;
            }

            object data = r.Data;

            result.Data    = data;
            result.Success = true;
            return result;
        }

        /// <summary>取得 HashProperty 資料集合的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得資料結果</returns>
        public GetHashPropertiesCacheResult GetHashProperties( string cacheId )
        {
            var result = new GetHashPropertiesCacheResult()
            {
                Success = false,
                Data    = null
            };

            var reqDTO = new CacheReqDTO()
            {
                CacheId = cacheId
            };

            var r = Execute<CacheReqDTO, HashProperty[]>( "GetHashProperties", reqDTO );

            if ( !r.Success )
            {
                result.Code    = r.Code;
                result.Message = r.Message;
                Logger.WriteErrorLog( this, $"Get hash properties data from remote cache repository fail. CacheId: {cacheId}. {r.Message}", Logger.GetTraceLogTitle( this, nameof ( GetHashProperties ) ) );
                return result;
            }

            HashProperty[] hashProperties = r.Data;

            if ( hashProperties.IsNullOrEmptyArray() )
            {
                result.Message = $"Get hash properties data from remote cache fail. Retirve HashProperty array is null or empty. CacheId: {cacheId}.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( GetHashProperties ) ) );
                return result;
            }

            result.Data    = hashProperties;
            result.Success = true;
            return result;
        }

        /// <summary>取得 HashProperty 資料集合的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得資料結果</returns>
        public async Task<GetHashPropertiesCacheResult> GetHashPropertiesAsync( string cacheId )
        {
            var result = new GetHashPropertiesCacheResult()
            {
                Success = false,
                Data    = null
            };

            var reqDTO = new CacheReqDTO()
            {
                CacheId = cacheId
            };

            var r = await ExecuteAsync<CacheReqDTO, HashProperty[]>( "GetHashPropertiesAsync", reqDTO );

            if ( !r.Success )
            {
                result.Code    = r.Code;
                result.Message = r.Message;
                Logger.WriteErrorLog( this, $"Get hash properties data from remote cache repository fail. CacheId: {cacheId}. {r.Message}", Logger.GetTraceLogTitle( this, nameof ( GetHashPropertiesAsync ) ) );
                return result;
            }

            HashProperty[] hashProperties = r.Data;

            if ( hashProperties.IsNullOrEmptyArray() )
            {
                result.Message = $"Get hash properties data from remote cache fail. Retirve HashProperty array is null or empty. CacheId: {cacheId}.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( GetHashPropertiesAsync ) ) );
                return result;
            }

            result.Data    = hashProperties;
            result.Success = true;
            return result;
        }

        /// <summary>存放 HashProperty 資料<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">Hash 快取資料結構集合</param>
        /// <returns>存放是否成功</returns>
        public bool PutHashProperty( string cacheId, HashProperty[] hashProperties )
        {
            using ( _asyncLock.Lock() )
            {
                var reqDTO = new PutHashPropertyReqDTO()
                {
                    CacheId        = cacheId,
                    HashProperties = hashProperties
                };

                var r = Execute<PutHashPropertyReqDTO, Result>( "PutHashProperty", reqDTO );

                if ( !r.Success )
                {
                    Message = r.Message;
                    Logger.WriteErrorLog( this, $"Put hash property data to remote cache repository fail. CacheId: {cacheId}. {r.Message}", Logger.GetTraceLogTitle( this, nameof ( PutHashProperty ) ) );
                    return false;
                }

                return true;
            }
        }

        /// <summary>存放 HashProperty 資料<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">Hash 快取資料結構集合</param>
        /// <returns>存放是否成功</returns>
        public async Task<bool> PutHashPropertyAsync( string cacheId, HashProperty[] hashProperties )
        {
            using ( await _asyncLock.LockAsync() )
            {
                var reqDTO = new PutHashPropertyReqDTO()
                {
                    CacheId        = cacheId,
                    HashProperties = hashProperties
                };

                var r = await ExecuteAsync<PutHashPropertyReqDTO, Result>( "PutHashPropertyAsync", reqDTO );

                if ( !r.Success )
                {
                    Message = r.Message;
                    Logger.WriteErrorLog( this, $"Put hash property data to remote cache repository fail. CacheId: {cacheId}. {r.Message}", Logger.GetTraceLogTitle( this, nameof ( PutHashPropertyAsync ) ) );
                    return false;
                }

                return true;
            }
        }

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料，CacheId 不會被整個移除掉。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>移除是否成功</returns>
        public bool RemoveHashProperty( string cacheId, string subkey )
        {
            using ( _asyncLock.Lock() )
            {
                var reqDTO = new HashPropertyReqDTO()
                {
                    CacheId = cacheId,
                    Subkey  = subkey
                };

                var r = Execute<HashPropertyReqDTO, Result>( "RemoveHashProperty", reqDTO );

                if ( !r.Success )
                {
                    Message = r.Message;
                    Logger.WriteErrorLog( this, $"Remove hash property data from remote cache repository fail. CacheId: {cacheId}, Subkey: {subkey}. {r.Message}", Logger.GetTraceLogTitle( this, nameof ( RemoveHashProperty ) ) );
                    return false;
                }

                return true;
            }
        }

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料，CacheId 不會被整個移除掉。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>移除是否成功</returns>
        public async Task<bool> RemoveHashPropertyAsync( string cacheId, string subkey )
        {
            using ( await _asyncLock.LockAsync() )
            {
                var reqDTO = new HashPropertyReqDTO()
                {
                    CacheId = cacheId,
                    Subkey  = subkey
                };

                var r = await ExecuteAsync<HashPropertyReqDTO, Result>( "RemoveHashPropertyAsync", reqDTO );

                if ( !r.Success )
                {
                    Message = r.Message;
                    Logger.WriteErrorLog( this, $"Remove hash property data from remote cache repository fail. CacheId: {cacheId}, Subkey: {subkey}. {r.Message}", Logger.GetTraceLogTitle( this, nameof ( RemoveHashPropertyAsync ) ) );
                    return false;
                }

                return true;
            }
        }

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件的資料。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="data">Entity 資料物件</param>
        /// <returns>存放資料結果</returns>
        public Result PutHashObject( string cacheId, object data )
        {
            using ( _asyncLock.Lock() )
            {
                var result = Result.Create();

                HashProperty[] hashProperties;

                try
                {
                    hashProperties = HashPropertyConverter.ConvertTo( data );
                }
                catch ( Exception ex )
                {
                    result.Message = $"Convert data entity to HashProperty array occur exception. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( PutHashObject ) ), result.Message );
                    return result;
                }

                if ( !PutHashProperty( cacheId, hashProperties ) )
                {
                    result.Message = Message;
                    Logger.WriteErrorLog( this, $"Put hash property object into remote cache repository fail. CacheId: {cacheId}. {result.Message}", Logger.GetTraceLogTitle( this, nameof ( PutHashObject ) ) );
                    return result;
                }

                result.Success = true;
                return result;
            }
        }

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件的資料。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="data">Entity 資料物件</param>
        /// <returns>存放資料結果</returns>
        public async Task<Result> PutHashObjectAsync( string cacheId, object data )
        {
            using ( await _asyncLock.LockAsync() )
            {
                var result = Result.Create();

                HashProperty[] hashProperties;

                try
                {
                    hashProperties = HashPropertyConverter.ConvertTo( data );
                }
                catch ( Exception ex )
                {
                    result.Message = $"Convert data entity to HashProperty array occur exception. CacheId: {cacheId}.";
                    Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( PutHashObjectAsync ) ), result.Message );
                    return result;
                }

                if ( !await PutHashPropertyAsync( cacheId, hashProperties ) )
                {
                    result.Message = Message;
                    Logger.WriteErrorLog( this, $"Put hash property object into remote cache repository fail. CacheId: {cacheId}. {result.Message}", Logger.GetTraceLogTitle( this, nameof ( PutHashObjectAsync ) ) );
                    return result;
                }

                result.Success = true;
                return result;
            }
        }

        /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得結果</returns>
        public Result<TData> GetHashObject<TData>( string cacheId )
        {
            var result = Result.Create<TData>();
            var g = GetHashProperties( cacheId );

            if ( !g.Success )
            {
                result.Code    = g.Code;
                result.Message = g.Message;
                return result;
            }

            HashProperty[] hashProperties = g.Data;

            if ( hashProperties.IsNullOrEmptyArray() )
            {
                result.Message = $"No data found from remote cache. Key: {cacheId}.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( GetHashObject ) ) );
                return result;
            }

            TData data = default ( TData );

            try
            {
                data = HashPropertyConverter.ConvertTo<TData>( hashProperties );
            }
            catch ( Exception ex )
            {
                result.Message = $"Convert hash property array to entity object occur exception. Key: {cacheId}.";
                Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( GetHashObject ) ), result.Message );
                return result;
            }

            result.Data    = data;
            result.Success = true;
            return result;
        }

        /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得結果</returns>
        public async Task<Result<TData>> GetHashObjectAsync<TData>( string cacheId )
        {
            var result = Result.Create<TData>();
            var g = await GetHashPropertiesAsync( cacheId );

            if ( !g.Success )
            {
                result.Code    = g.Code;
                result.Message = g.Message;
                return result;
            }

            HashProperty[] hashProperties = g.Data;

            if ( hashProperties.IsNullOrEmptyArray() )
            {
                result.Message = $"No data found from remote cache. Key: {cacheId}.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( GetHashObjectAsync ) ) );
                return result;
            }

            TData data = default ( TData );

            try
            {
                data = HashPropertyConverter.ConvertTo<TData>( hashProperties );
            }
            catch ( Exception ex )
            {
                result.Message = $"Convert hash property array to entity object occur exception. Key: {cacheId}.";
                Logger.WriteExceptionLog( this, ex, Logger.GetTraceLogTitle( this, nameof ( GetHashObjectAsync ) ), result.Message );
                return result;
            }

            result.Data    = data;
            result.Success = true;
            return result;
        }

        #endregion Public Methdos


        #region Private Methods

        /// <summary>遠端快取連線中斷的事件觸發方法
        /// </summary>
        private void OnConnectionBroken() => CacheManager.ConnectionBroken?.Invoke();

        /// <summary>遠端快取連線恢復的事件觸發方法
        /// </summary>
        private void OnConnectionRestored() => CacheManager.ConnectionRestored?.Invoke();

        /// <summary>執行快取資料更新
        /// </summary>
        /// <param name="cacheId">快取資料識別碼</param>
        private void ExecuteRefreshData( string cacheId )
        {
            if ( !_refreshProperties.TryGetValue( cacheId, out DataRefreshProperty refreshProperty ) )
            {
                return;
            }

            lock ( refreshProperty )
            {
                #region 執行資料更新委派

                object data = null;

                try
                {
                    data = refreshProperty.RefreshHandler();
                }
                catch ( Exception ex )
                {
                    Logger.WriteErrorLog( this, $"RemoteCache '{_serviceClientName}' timer execute refresh data handler occur exception. CacheId: {cacheId}. {ex.ToString()}", "Execute remote cache refresh handler occur error." );
                    return;
                }

                if ( data.IsNull() )
                {
                    Logger.WriteWarningLog( this, $"RemoteCache '{_serviceClientName}' timer retrive null from refresh handler result. No update to remote cache. CacheId: {cacheId}.", "Execute remote cache refresh handler occur error." );
                    return;
                }

                var cacheProperty = new CacheProperty()
                {
                    CacheId     = cacheId,
                    Data        = data,
                    DataId      = RandomTextHelper.Create( 36 ),
                    RefreshTime = DateTime.Now
                };

                #endregion 執行資料更新委派

                #region 執行對 RemoteCache 進行資料更新

                var r = Execute<CacheProperty>( "Update", cacheProperty );

                if ( !r.Success )
                {
                    Message = r.Message;
                    Logger.WriteErrorLog( this, $"Refresh timer update data to remote cache repository fail. {r.Message}", "Update data to remote cache fail. Execute remote cache refresh handler occur error." );
                    return;
                }

                #endregion 執行對 RemoteCache 進行資料更新
            }
        }

        #endregion Private Methods
    }
}
