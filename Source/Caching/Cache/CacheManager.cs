﻿using System.Collections.Generic;
using ZayniFramework.Common;


namespace ZayniFramework.Caching
{
    /// <summary>快取管理員
    /// </summary>
    public static partial class CacheManager
    {
        #region 宣告內部的靜態委派屬性

        /// <summary>取得快取倉儲中的 Key 值集合的委派
        /// </summary>
        internal static GetCacheKeysHandler GetCacheKeysCallback { get; set; }

        /// <summary>存放快取資料的委派
        /// </summary>
        internal static PutCacheDataHandler PutCallback { get; set; }

        /// <summary>更新快取資料的委派
        /// </summary>
        internal static UpdateCacheDataHandler UpdateCallback { get; set; }

        /// <summary>移除快取資料的委派
        /// </summary>
        internal static RemoveCacheDataHandler RemoveCallback { get; set; }

        /// <summary>清空快取資料的委派
        /// </summary>
        internal static ClearCacheDataHandler ClearCallback { get; set; }

        /// <summary>檢查快取是否包含有目標資料的委派
        /// </summary>
        internal static ContainsCacheDataHandler ContainsCallback { get; set; }

        /// <summary>檢查 Hash 資料快取池中是否包含指定識別碼的資料的委派
        /// </summary>
        internal static ContainsHashHandler ContainsHashCallback { get; set; }

        /// <summary>取得 Hash 資料結構的 Hash Field 資料值的委派
        /// </summary>
        internal static GetHashPropertyHandler GetHashPropertyCallback { get; set; }

        /// <summary>取得 HashProperty 資料集合的快取資料的委派
        /// </summary>
        internal static GetHashPropertiesHandler GetHashPropertiesCallback { get; set; }

        /// <summary>以 Hash 資料結構存放至快取池中的委派。<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        internal static PutHashPropertyHandler PutHashPropertyCallback { get; set; }

        /// <summary>將指定的 Hash 快取資料從快取池中移除的委派。
        /// </summary>
        internal static RemoveHashPropertyHandler RemoveHashPropertyCallback { get; set; }

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料的委派，CacheId 不會被整個移除掉。
        /// </summary>
        internal static RemoveHashPropertyFieldHandler RemoveHashPropertyFieldCallback { get; set; }

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件資料的委派
        /// </summary>
        internal static PutHashObjectHandler PutHashObjectCallback { get; set; }

        /// <summary>取得錯誤訊息的委派
        /// </summary>
        internal static GetMessageHandler GetMessageCallback { get; set; }

        /// <summary>設定錯誤訊息的委派
        /// </summary>
        internal static SetMessageHandler SetMessageCallback { get; set; }

        /// <summary>取得快取資料數量的委派
        /// </summary>
        internal static GetCountHandler GetCountCallback { get; set; }

        #endregion 宣告內部的靜態委派屬性


        #region 宣告私有的靜態欄位

        /// <summary>快取倉儲的模式<para/>
        /// Memory: InProcess Memory 內部程序記憶體快取 (效能最快)<para/>
        /// Redis: Redis 遠端快取服務 (效能很不錯)<para/>
        /// MySQLMemory: MySQL Memory Engine 遠端快取服務<para/>
        /// RemoteCache: Zayni Framework 框架內建的遠端快取服務<para/>
        /// </summary>
        private static string _cacheRepoType = "memory";

        #endregion 宣告私有的靜態欄位


        #region 宣告靜態建構子

        /// <summary>宣告靜態建構子
        /// </summary>
        static CacheManager() 
        {
            _cacheRepoType = ConfigReader.LoadCacheManagerMode().IsNullOrEmptyString( "memory" );
            HandlerBroker.SetCacheHandlers( _cacheRepoType );
        }

        #endregion 宣告靜態建構子
        

        #region 宣告公開的靜態屬性

        /// <summary>訊息
        /// </summary>
        public static string Message
        {
            get
            {
                return GetMessageCallback();
            }
            set
            {
                SetMessageCallback( value );
            }
        }

        /// <summary>快取池中目前存放的資料個數
        /// </summary>
        public static int Count => GetCountCallback();

        /// <summary>遠端快取倉儲的連線中斷的處理委派
        /// </summary>
        /// <value></value>
        public static CacheRepositoryConnectionBrokenHandler ConnectionBroken { get; set; }

        /// <summary>遠端快取倉儲的連線重新恢復的處理委派
        /// </summary>
        /// <value></value>
        public static CacheRepositoryConnectionRestoreHandler ConnectionRestored { get; set; }

        #endregion 宣告公開的靜態屬性


        #region 宣告公開的靜態方法

        /// <summary>取得快取倉儲中的 Key 值集合
        /// </summary>
        /// <param name="pattern">Key 值的規則</param>
        /// <returns>取得結果</returns>
        public static Result<List<string>> GetCacheKeys( string pattern = null ) => 
            GetCacheKeysCallback?.Invoke( pattern ) ?? new Result<List<string>>() { Code = "", Message = "Config error. Should check the cache 'managerMode' again." };

        /// <summary>根據指定的快取ID從快取池中取出資料
        /// </summary>
        /// <param name="cacheId">快取ID值</param>
        /// <param name="getSomethingCallback">重新從資料來源取得資料的回呼委派</param>
        /// <returns>從快取池中取得資料的結果</returns>
        public static GetCacheResult Get( string cacheId, GetSomethingFromDataSource getSomethingCallback = null ) =>
            HandlerBroker.GetCacheHandler( _cacheRepoType )?.Invoke( cacheId, getSomethingCallback );

        /// <summary>根據指定的快取ID從快取池中取出資料
        /// </summary>
        /// <typeparam name="TModel">資料模型泛型</typeparam>
        /// <param name="cacheId">快取ID值</param>
        /// <param name="getSomethingCallback">重新從資料來源取得資料的回呼委派</param>
        /// <returns>從快取池中取得資料的結果</returns>
        public static GetCacheResult<TModel> Get<TModel>( string cacheId, GetSomethingFromDataSource getSomethingCallback = null ) =>
            HandlerBroker.GetCacheHandler<TModel>( _cacheRepoType )?.Invoke( cacheId, getSomethingCallback );

        /// <summary>將指定的資料放入快取池中
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否成功將資料放入快取池</returns>
        public static bool Put( CacheProperty cacheProperty ) => PutCallback?.Invoke( cacheProperty ) ?? false;

        /// <summary>更新快取池中指定的資料
        /// </summary>
        /// <param name="cacheProperty">快取資料儲存屬性</param>
        /// <returns>是否更新成功</returns>
        public static bool Update( CacheProperty cacheProperty ) => UpdateCallback?.Invoke( cacheProperty ) ?? false;

        /// <summary>將指定的資料從快取池中移除
        /// </summary>
        /// <param name="cacheId">快取ID值</param>
        /// <returns>是否成功移除資料</returns>
        public static bool Remove( string cacheId ) => RemoveCallback?.Invoke( cacheId ) ?? false;

        /// <summary>清空快取池中所有的資料
        /// </summary>
        /// <returns>是否成功清空</returns>
        public static bool Clear() => ClearCallback?.Invoke() ?? false;

        /// <summary>檢查快取池中是否包含指定的快取ID的資料
        /// </summary>
        /// <param name="cacheId">快取ID值</param>
        /// <returns>快取池中是否包含指定的快取ID的資料</returns>
        public static bool Contains( string cacheId ) => ContainsCallback?.Invoke( cacheId ) ?? false;

        /// <summary>檢查 Hash 資料快取池中是否包含指定識別碼的資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>是否包含指定識別碼的資料</returns>
        public static bool ContainsHash( string cacheId ) => ContainsHashCallback?.Invoke( cacheId ) ?? false;

        /// <summary>取得 Hash 資料結構的 Hash Field 資料值
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>取得資料結果</returns>
        public static GetHashCacheResult GetHashProperty( string cacheId, string subkey ) => 
            GetHashPropertyCallback?.Invoke( cacheId, subkey );

        /// <summary>取得 HashProperty 資料集合的快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得資料結果</returns>
        public static GetHashPropertiesCacheResult GetHashProperties( string cacheId ) => 
            GetHashPropertiesCallback?.Invoke( cacheId );

        /// <summary>以 Hash 資料結構存放至快取池中。<para/>
        /// 此功能有以下限制:<para/>
        /// 1. 暫時不支援快取資料 TTL Expiry 過其自動回收的功能，代表以 HashProperty 資料結構儲存在 in-process memory cache 的資料都是常駐型快取資料。<para/>
        /// 2. 暫時不支援快取資料定時自動重新更新的功能。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">HashProperty 的資料識別 Key 值</param>
        /// <returns>存放是否成功</returns>
        public static bool PutHashProperty( string cacheId, HashProperty[] hashProperties ) =>
            PutHashPropertyCallback?.Invoke( cacheId, hashProperties ) ?? false;

        /// <summary>將指定的 Hash 快取資料從快取池中移除。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>移除是否成功</returns>
        public static bool RemoveHashProperty( string cacheId ) => 
            RemoveHashPropertyCallback?.Invoke( cacheId ) ?? false;

        /// <summary>移除指定的 CacheId 中特定 Subkey 的 HashProperty 欄位快取資料，CacheId 不會被整個移除掉。
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="subkey">HashProperty 的資料識別 Key 值</param>
        /// <returns>移除是否成功</returns>
        public static bool RemoveHashProperty( string cacheId, string subkey ) => 
            RemoveHashPropertyFieldCallback?.Invoke( cacheId, subkey ) ?? false;

        /// <summary>依照指定的 CacheId 取得完整的 HashProperty 資料結構的快取資料，並依照指定的泛型 Entity 回傳。
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <returns>取得結果</returns>
        public static Result<TData> GetHashObject<TData>( string cacheId ) => 
            HandlerBroker.GetHashObjectHandler<TData>( _cacheRepoType )?.Invoke( cacheId );

        /// <summary>以 HashProperty 存放整個完整的 Entity 物件資料的委派
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="data">Entity 資料物件</param>
        /// <returns>存放資料結果</returns>
        public static Result PutHashObject( string cacheId, object data ) =>
            PutHashObjectCallback?.Invoke( cacheId, data );

        #endregion 宣告公開的靜態方法
    }
}
