﻿using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>ZayniFramework.Caching 模組的委派執行器
    /// </summary>
    internal static class CachingHandlerExecutor
    {
        /// <summary>執行對資料來源重新取得資料的回呼
        /// </summary>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <param name="result">目標資料</param>
        /// <param name="message">執行訊息(錯誤訊息)</param>
        /// <returns>執行是否成功</returns>
        internal static bool ExecuteGetSomethingCallback( GetSomethingFromDataSource handler, out object result, out string message )
        {
            result  = null;
            message = null;

            var logTitle = GetTraceLogTitle( nameof ( ExecuteGetSomethingCallback ) );

            try
            {
                result = handler?.Invoke();
            }
            catch ( Exception ex )
            {
                message = $"{logTitle}, execute delegate callback occur exception. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteErrorLog( nameof ( CachingHandlerExecutor ), message, logTitle );
                return false;
            }

            return true;
        }

        /// <summary>執行對資料來源重新取得資料的回呼
        /// </summary>
        /// <param name="handler">重新從資料來源取得資料的回呼委派</param>
        /// <returns>執行是否成功</returns>
        internal static async Task<Result<object>> ExecuteGetSomethingCallbackAsync( GetSomethingFromDataSourceAsync handler )
        {
            var logTitle = GetTraceLogTitle( nameof ( ExecuteGetSomethingCallbackAsync ) );

            try
            {
                var rawData = await handler?.Invoke();
                return Result.Create<object>( true, data: rawData );
            }
            catch ( Exception ex )
            {
                string message = $"{logTitle}, execute delegate callback occur exception. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteErrorLog( nameof ( CachingHandlerExecutor ), message, logTitle );
                return Result.Create<object>( false, message: message );
            }
        }

        /// <summary>取得日誌動作的標題名稱
        /// </summary>
        /// <param name="methodName"></param>
        /// <returns></returns>
        private static string GetTraceLogTitle( string methodName ) => Logger.GetTraceLogTitle( nameof ( CachingHandlerExecutor ), methodName );
    }
}
