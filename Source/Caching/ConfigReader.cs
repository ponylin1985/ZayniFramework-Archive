﻿using System;
using System.Collections.Generic;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>Caching 模組的 Config 設定值讀取器
    /// </summary>
    public static class ConfigReader
    {
        #region Private Fields

        /// <summary>ZayniFramework 框架設定區段　
        /// </summary>
        private static ZayniConfigSection _zayniConfigSection;

        #endregion Private Fields


        #region Static Constructor

        /// <summary>靜態建構子
        /// </summary>
        static ConfigReader()
        {
            try
            {
                _zayniConfigSection = ConfigManagement.ZayniConfigs;
            }
            catch ( Exception ex )
            {
                ConsoleLogger.LogError( $"{GetLogTitle( "static.ctor" )}. Read ZayniFramework config section occur exception. {ex.ToString()}" );
                Logger.WriteExceptionLog( nameof ( ConfigReader ) , ex, message: $"Read ZayniFramework config section occur exception.", eventTitle: GetLogTitle( "static.ctor" ) );
                throw ex;
            }
        }

        #endregion Static Constructor


        #region Public Methods

        /// <summary>讀取快取類型設定值
        /// </summary>
        /// <returns>取快取類型設定值</returns>
        public static string LoadCacheManagerMode()
        {
            try
            {
                return _zayniConfigSection.CachingSettings.CacheManagerMode;
            }
            catch ( Exception ex )
            {
                ConsoleLogger.LogError( $"Load ZayniFramework/CachingSettings/managerMode occur exception. {ex.ToString()}" );
                Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, GetLogTitle( nameof ( LoadCacheManagerMode ) ), $"Load ZayniFramework/CachingSettings/managerMode occur exception." );
                throw ex;
            }
        }

        /// <summary>讀取記憶體快取機制的所有的相關Config設定值
        /// </summary>
        public static void LoadMemoryCacheSettings()
        {
            try
            {
                CachingSettingsConfigCollection cacheSettings = _zayniConfigSection.CachingSettings;

                CachePoolSettings.IsResetEnable   = cacheSettings.CachePool.IsResetEnable;
                CachePoolSettings.ResetInterval   = cacheSettings.CachePool.CleanerIntervalSecond.IsDotNetDefault( 60 );
                CachePoolSettings.MaxCapcaity     = cacheSettings.CachePool.MaxCapcaity.IsDotNetDefault( 5000 );
                CachePoolSettings.RefreshInterval = cacheSettings.CachePool.RefreshInterval;
                CachePoolSettings.CachingHandlers = cacheSettings.CachePool.CachingHandlers;

                CacheDataSettings.TimeoutInterval = cacheSettings.CacheData.TimeoutInterval;
            }
            catch ( Exception ex )
            {
                ConsoleLogger.LogError( $"Load ZayniFramework/CachingSettings/CachePool occur exception. {ex.ToString()}" );
                Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, GetLogTitle( nameof ( LoadMemoryCacheSettings ) ), $"Load ZayniFramework/CachingSettings/CachePool occur exception." );
                throw ex;
            }
        }

        /// <summary>讀取 Redis 服務快取的 Config 設定值
        /// </summary>
        public static void LoadRedisCacheSettings()
        {
            try
            {
                CachingSettingsConfigCollection cacheSettings = _zayniConfigSection.CachingSettings;
                RedisCacheElement defaultCacheConfig = GetDefaultRedisCacheConfig();

                RedisCacheConfigs.DefaultHost       = defaultCacheConfig.Host;
                RedisCacheConfigs.DefaultPort       = defaultCacheConfig.Port;
                RedisCacheConfigs.DefaultPassword   = defaultCacheConfig.Password;
                RedisCacheConfigs.DefaultDatabaseNo = defaultCacheConfig.DatabaseNo;

                CachePoolSettings.MaxCapcaity       = cacheSettings.CachePool.MaxCapcaity.IsDotNetDefault( 5000 );
                CachePoolSettings.RefreshInterval   = cacheSettings.CachePool.RefreshInterval;
                CacheDataSettings.TimeoutInterval   = cacheSettings.CacheData.TimeoutInterval;
            }
            catch ( Exception ex )
            {
                ConsoleLogger.LogError( $"Load ZayniFramework/CachingSettings/RedisCache config section occur exception. {ex.ToString()}" );
                Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, GetLogTitle( nameof ( LoadRedisCacheSettings ) ), $"Load ZayniFramework/CachingSettings/RedisCache config section occur exception." );
                RedisCacheConfigs.DefaultPort = 6379;
                throw ex;
            }
        }

        /// <summary>取得預設的 Redis 快取連線設定
        /// </summary>
        /// <returns>預設的 Redis 快取連線設定</returns>
        public static RedisCacheElement GetDefaultRedisCacheConfig()
        {
            CachingSettingsConfigCollection cacheSettings = _zayniConfigSection.CachingSettings;

            if ( cacheSettings.IsNull() || cacheSettings.RedisCache.IsNullOrEmpty() )
            {
                return null;
            }

            foreach ( var config in cacheSettings.RedisCache )
            {
                RedisCacheElement cfg = (RedisCacheElement)config;

                if ( cfg.IsDefault )
                {
                    return cfg;
                }
            }

            return cacheSettings.RedisCache[ 0 ];
        }

        /// <summary>取得指定組態名稱的 Redis 連線設定
        /// </summary>
        /// <param name="name">Redis 快取的 Config 組態名稱</param>
        /// <returns>Redis 服務連線設定</returns>
        public static RedisCacheElement GetCacheConfig( string name )
        {
            CachingSettingsConfigCollection cacheSettings = _zayniConfigSection.CachingSettings;

            foreach ( var config in cacheSettings.RedisCache )
            {
                RedisCacheElement cfg = (RedisCacheElement)config;

                if ( cfg.Name == name )
                {
                    return cfg;
                }
            }

            return null;
        }

        /// <summary>取得所有 Redis 快取連線的設定集合
        /// </summary>
        /// <returns>所有 Redis 快取連線的設定集合</returns>
        public static List<RedisCacheElement> GetCacheConfigs()
        {
            var configs = new List<RedisCacheElement>();

            CachingSettingsConfigCollection cacheSettings = _zayniConfigSection.CachingSettings;

            foreach ( var config in cacheSettings.RedisCache )
            {
                RedisCacheElement cfg = (RedisCacheElement)config;
                configs.Add( cfg );
            }

            return configs;
        }

        /// <summary>取得預設的 RemoteCache 客戶端設定值集合
        /// </summary>
        /// <returns>預設的 RemoteCache 客戶端設定值集合</returns>
        public static RemoteCacheElement GetRemoteCacheConfigs() 
        {
            try
            {
                var cacheSettings                 = _zayniConfigSection.CachingSettings;
                CachePoolSettings.MaxCapcaity     = cacheSettings.CachePool.MaxCapcaity.IsDotNetDefault( 5000 );
                CachePoolSettings.RefreshInterval = cacheSettings.CachePool.RefreshInterval;
                CacheDataSettings.TimeoutInterval = cacheSettings.CacheData.TimeoutInterval;
                return cacheSettings.RemoteCache;
            }
            catch ( Exception ex )
            {
                ConsoleLogger.LogError( $"Load RemoteCache config occur exception. {ex.ToString()}" );
                Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, GetLogTitle( nameof ( GetRemoteCacheConfigs ) ), "Load RemoteCache config occur exception." );
                throw ex;
            }
        }

        /// <summary>取得預設的遠端快取客戶端設定名稱
        /// </summary>
        /// <returns>預設的遠端快取客戶端設定名稱</returns>
        public static string GetDefaultRemoteCacheClient()
        {
            try
            {
                var cacheSettings                 = _zayniConfigSection.CachingSettings;
                CachePoolSettings.MaxCapcaity     = cacheSettings.CachePool.MaxCapcaity.IsDotNetDefault( 5000 );
                CachePoolSettings.RefreshInterval = cacheSettings.CachePool.RefreshInterval;
                CacheDataSettings.TimeoutInterval = cacheSettings.CacheData.TimeoutInterval;
                return cacheSettings.RemoteCache.DefaultCacheClientName;
            }
            catch ( Exception ex )
            {
                ConsoleLogger.LogError( $"Load RemoteCache/defaultCacheClientName config occur exception. {ex.ToString()}" );
                Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, GetLogTitle( nameof ( GetDefaultRemoteCacheClient ) ), "Load RemoteCache/defaultCacheClientName config occur exception." );
                throw ex;
            }
        }

        /// <summary>取得預設的 MySQL Memory 快取資料庫連線名稱
        /// </summary>
        /// <returns>預設的 MySQL Memory 快取資料庫連線名稱</returns>
        public static string GetDefaultMySqlMemoryCache()
        {
            try
            {
                var cacheSettings                 = _zayniConfigSection.CachingSettings;
                CachePoolSettings.MaxCapcaity     = cacheSettings.CachePool.MaxCapcaity.IsDotNetDefault( 5000 );
                CachePoolSettings.RefreshInterval = cacheSettings.CachePool.RefreshInterval;
                CacheDataSettings.TimeoutInterval = cacheSettings.CacheData.TimeoutInterval;
                return cacheSettings.MySQLMemoryCache.DefaultConnectionName;
            }
            catch ( Exception ex )
            {
                ConsoleLogger.LogError( $"Load MySQLMemoryCache/defaultConnectionName config occur exception. {ex.ToString()}" );
                Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, GetLogTitle( nameof ( GetDefaultMySqlMemoryCache ) ), "Load MySQLMemoryCache/defaultConnectionName config occur exception." );
                throw ex;
            }
        }

        #endregion Public Methods


        #region Private Methods

        /// <summary>取得日誌的標題
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的標題</returns>
        private static string GetLogTitle( string methodName ) => $"{nameof ( ConfigReader )}.{methodName}";

        #endregion Private Methods
    }
}
