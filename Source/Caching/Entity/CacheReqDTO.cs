﻿using Newtonsoft.Json;
using System;
using ZayniFramework.Validation;


namespace ZayniFramework.Caching
{
    /// <summary>遠端資料快取請求的資料載體
    /// </summary>
    [Serializable()]
    public class CacheReqDTO
    {
        /// <summary>快取資料的識別代碼
        /// </summary>
        [JsonProperty( PropertyName = "cacheId" )]
        [NotNullOrEmpty( Message = "The 'cacheId' can not be null or empty string." )]
        public string CacheId { get; set; }
    }
}
