﻿using Newtonsoft.Json;
using System;


namespace ZayniFramework.Caching
{
    /// <summary>取得快取倉儲中的 Key 值集合的請求資料載體
    /// </summary>
    [Serializable()]
    public sealed class GetCacheKeysReqDTO
    {
        /// <summary>資料快取 Key 值的規則 (選擇性參數)
        /// </summary>
        [JsonProperty( "pattern", NullValueHandling = NullValueHandling.Ignore )]
        public string Pattern { get; set; }
    }
}
