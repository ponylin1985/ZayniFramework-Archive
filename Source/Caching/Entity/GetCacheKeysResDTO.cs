﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace ZayniFramework.Caching
{
    /// <summary>取得快取倉儲中的 Key 值集合的回應資料載體
    /// </summary>
    [Serializable()]
    public sealed class GetCacheKeysResDTO
    {
        /// <summary>資料快取 Key 值的集合
        /// </summary>
        [JsonProperty( "cacheKeys" )]
        public List<string> CacheKeys { get; set; }
    }
}
