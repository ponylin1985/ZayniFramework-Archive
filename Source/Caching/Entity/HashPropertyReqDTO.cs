﻿using Newtonsoft.Json;
using System;
using ZayniFramework.Validation;


namespace ZayniFramework.Caching
{
    /// <summary>遠端 HashProperty 的資料快取的請求
    /// </summary>
    [Serializable()]
    public sealed class HashPropertyReqDTO : CacheReqDTO
    {
        /// <summary>HashProperty 的資料識別 Key 值
        /// </summary>
        [JsonProperty( PropertyName = "subkey" )]
        [NotNullOrEmpty( Message = "The 'subkey' can not be null or empty string." )]
        public string Subkey { get; set; }
    }
}
