﻿using Newtonsoft.Json;
using System;
using ZayniFramework.Validation;


namespace ZayniFramework.Caching
{
    /// <summary>存放 HashProperty 資料的請求資料載體
    /// </summary>
    [Serializable()]
    public class PutHashPropertyReqDTO : CacheReqDTO
    {
        /// <summary>HashProperty 資料集合
        /// </summary>
        [JsonProperty( PropertyName = "hashProperties" )]
        [NotNullOrEmpty( Message = "The 'hashProperties' can not be null or empty collection." )]
        public HashProperty[] HashProperties { get; set; }
    }
}
