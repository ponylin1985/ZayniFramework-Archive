﻿using Newtonsoft.Json;
using System;
using ZayniFramework.Validation;


namespace ZayniFramework.Caching
{
    /// <summary>以 HashProperty 存放整個資料物件的請求載體
    /// </summary>
    [Serializable()]
    public class PutHashObjectReqDTO : CacheReqDTO
    {
        /// <summary>資料物件內容
        /// </summary>
        [JsonProperty( "dataContent" )]
        [NotNull( Message = "The 'dataContent' can not be null." )]
        public object DataContent { get; set; }
    }
}
