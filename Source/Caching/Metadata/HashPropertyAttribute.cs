﻿using System;
using ZayniFramework.Common;


namespace ZayniFramework.Caching
{
    /// <summary>HashProperty 的資料型別 Attribute
    /// </summary>
    [AttributeUsage( AttributeTargets.Property | AttributeTargets.Field )]
    public class HashPropertyAttribute : Attribute
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public HashPropertyAttribute()
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="subkey">HashProperty 資料結構的 SubKey 名稱</param>
        /// <param name="defaultValue">資料預設值</param>
        public HashPropertyAttribute( string subkey, string defaultValue = null )
        {
            if ( subkey.IsNullOrEmpty() )
            {
                throw new ArgumentException( $"The argument '{nameof ( subkey )}' is not allowed null or empty string." );
            }

            Subkey       = subkey;
            DefaultValue = defaultValue;
        }

        /// <summary>解構子
        /// </summary>
        ~HashPropertyAttribute()
        {
            Subkey       = null;
            DefaultValue = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>HashProperty 資料結構的 SubKey 名稱
        /// </summary>
        public string Subkey { get; set; }

        /// <summary>資料預設值<para/>
        /// 1. 當進行資料存放時，如果檢查到資料為 Null 值，則以此預設值進行儲存。<para/>
        /// 2. 當進行查詢時，如果檢查到資料為 Null 值，則以此預設值設定屬性值。<para/>
        /// </summary>
        public object DefaultValue { get; set; }

        #endregion 宣告公開的屬性
    }
}
