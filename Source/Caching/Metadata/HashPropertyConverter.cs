﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>HashProperty 資料轉換器
    /// </summary>
    internal static class HashPropertyConverter
    {
        /// <summary>轉換 HashProperty 資料字典成強型別 Entity 資料物件
        /// </summary>
        /// <typeparam name="TData">Entity 資料物件的泛型</typeparam>
        /// <param name="hashProperties">HashProperty 資料字典</param>
        /// <returns>強型別 Entity 資料物件</returns>
        internal static TData ConvertTo<TData>( Dictionary<string, object> hashProperties )
        {
            if ( hashProperties.IsNullOrEmptyCollection() )
            {
                return default ( TData );
            }

            TData obj = default ( TData );

            try
            {
                obj = Activator.CreateInstance<TData>();

                if ( obj.IsNull() )
                {
                    return default ( TData );
                }

                var properties = typeof ( TData ).GetProperties();

                if ( properties.IsNullOrEmptyArray() )
                {
                    return default ( TData );
                }

                foreach ( var property in properties )
                {
                    HashPropertyAttribute hashMetadata = Reflector.GetCustomAttribute<HashPropertyAttribute>( property );

                    if ( hashMetadata.IsNull() )
                    {
                        continue;
                    }

                    if ( hashMetadata.Subkey.IsNullOrEmpty() )
                    {
                        continue;
                    }

                    if ( !hashProperties.ContainsKey( hashMetadata.Subkey ) )
                    {
                        continue;
                    }

                    object value = hashProperties[ hashMetadata.Subkey ];

                    if ( value.IsNull() && hashMetadata.DefaultValue.IsNotNull() )
                    {
                        value = hashMetadata.DefaultValue;
                    }

                    Reflector.SetPropertyValue( obj, property, value );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( HashPropertyConverter ), ex, Logger.GetTraceLogTitle( nameof ( HashPropertyConverter ), nameof ( ConvertTo ) ), $"Convert HashProperty dictionary to entity object occur exception." );
                throw ex;
            }

            return obj;
        }

        /// <summary>轉換 HashProperty 資料集合成強型別 Entity 資料物件
        /// </summary>
        /// <typeparam name="TData">強型別 Entity 資料物件的泛型</typeparam>
        /// <param name="hashProperties">HashProperty 資料陣列</param>
        /// <returns>強型別 Entity 資料物件</returns>
        internal static TData ConvertTo<TData>( HashProperty[] hashProperties )
        {
            if ( hashProperties.IsNullOrEmptyArray() )
            {
                return default ( TData );
            }

            TData obj = default ( TData );

            try
            {
                obj = Activator.CreateInstance<TData>();

                if ( obj.IsNull() )
                {
                    return default ( TData );
                }

                var properties = typeof ( TData ).GetProperties();

                if ( properties.IsNullOrEmptyArray() )
                {
                    return default ( TData );
                }

                foreach ( var property in properties )
                {
                    HashPropertyAttribute hashMetadata = Reflector.GetCustomAttribute<HashPropertyAttribute>( property );

                    if ( hashMetadata.IsNull() )
                    {
                        continue;
                    }

                    if ( hashMetadata.Subkey.IsNullOrEmpty() )
                    {
                        continue;
                    }

                    var where = hashProperties.Where( h => h.Subkey == hashMetadata.Subkey );

                    if ( where.Count() != 1 )
                    {
                        continue;
                    }

                    var value = where.SingleOrDefault()?.Value;

                    if ( value is RedisValue redisValue )
                    {
                        if ( !redisValue.HasValue )
                        {
                            value = hashMetadata.DefaultValue.IsNotNull() ? hashMetadata.DefaultValue : null;
                        }
                    }
                    else
                    {
                        if ( value.IsNull() && hashMetadata.DefaultValue.IsNotNull() )
                        {
                            value = hashMetadata.DefaultValue;
                        }

                        if ( value is string val && val.IsNullOrEmpty() && hashMetadata.DefaultValue.IsNotNull() )
                        {
                            value = hashMetadata.DefaultValue;
                        }
                    }

                    Reflector.SetPropertyValue( obj, property, value );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( HashPropertyConverter ), ex, Logger.GetTraceLogTitle( nameof ( HashPropertyConverter ), nameof ( ConvertTo ) ), $"Convert HashProperty dictionary to entity object occur exception." );
                throw ex;
            }

            return obj;
        }

        /// <summary>轉換強型別 Entity 資料物件成 HashProperty 資料陣列
        /// </summary>
        /// <param name="data">強型別的 Entity 資料物件</param>
        /// <returns>HashProperty 資料陣列</returns>
        internal static HashProperty[] ConvertTo( object data )
        {
            if ( data.IsNull() )
            {
                return null;
            }

            var dataType   = data.GetType();
            var properties = dataType.GetProperties();

            if ( properties.IsNullOrEmptyArray() )
            {
                return null;
            }

            var hashProperties = new List<HashProperty>();

            try
            {
                foreach ( var property in properties )
                {
                    HashPropertyAttribute hashMetadata = Reflector.GetCustomAttribute<HashPropertyAttribute>( property );

                    if ( hashMetadata.IsNull() )
                    {
                        continue;
                    }

                    if ( hashMetadata.Subkey.IsNullOrEmpty() )
                    {
                        continue;
                    }

                    var hashProperty = new HashProperty( hashMetadata.Subkey, property.GetValue( data ) );
                    hashProperties.Add( hashProperty );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( HashPropertyConverter ), ex, Logger.GetTraceLogTitle( nameof ( HashPropertyConverter ), nameof ( ConvertTo ) ), $"Convert entity object to HashProperty array occur exception." );
                throw ex;
            }

            if ( hashProperties.IsNullOrEmptyList() )
            {
                return null;
            }

            return hashProperties.ToArray();
        }

        /// <summary>轉換 HashProperty 資料集合成 Redis 的 HashEntry 陣列
        /// </summary>
        /// <param name="hashProperties">HashProperty 資料集合</param>
        /// <returns>Redis 的 HashEntry 陣列</returns>
        internal static HashEntry[] ConvertToHashEntry( HashProperty[] hashProperties )
        {
            if ( hashProperties.IsNullOrEmptyArray() )
            {
                return null;
            }

            var hashEntries = new List<HashEntry>();

            foreach ( var hashProperty in hashProperties )
            {
                var hashEntry = new HashEntry( hashProperty.Subkey, hashProperty.Value + "" );
                hashEntries.Add( hashEntry );
            }

            return hashEntries.ToArray();
        }

        /// <summary>轉換 HashEntry 資料陣列成 HashProperty 資料陣列
        /// </summary>
        /// <param name="hashEntries">Redis 的 HashEntry 陣列</param>
        /// <returns>HashProperty 資料集合</returns>
        internal static HashProperty[] ConvertToHashProperty( HashEntry[] hashEntries )
        {
            if ( hashEntries.IsNullOrEmptyArray() )
            {
                return null;
            }

            var hashProperties = new List<HashProperty>();

            foreach ( var hashEntry in hashEntries )
            {
                var hashProperty = new HashProperty( hashEntry.Name, hashEntry.Value );
                hashProperties.Add( hashProperty );
            }

            return hashProperties.ToArray();
        }

        /// <summary>轉換 HashProperty 資料陣列成 MySQLMemoryHashModel 資料陣列
        /// </summary>
        /// <param name="cacheId">資料快取識別碼</param>
        /// <param name="hashProperties">HashProperty 資料陣列</param>
        /// <returns>MySQLMemoryHashModel 資料陣列</returns>
        internal static MySQLMemoryHashModel[] ConvertToHashModel( string cacheId, HashProperty[] hashProperties )
        {
            if ( hashProperties.IsNullOrEmptyArray() )
            {
                return null;
            }

            var models = new MySQLMemoryHashModel[ hashProperties.Length ];

            for ( int i = 0, len = hashProperties.Length; i< len; i++ )
            {
                var hashProperty = hashProperties[ i ];

                models[ i ] = new MySQLMemoryHashModel()
                {
                    CacheId    = cacheId,
                    Subkey     = hashProperty.Subkey,
                    CacheValue = hashProperty.Value + ""
                };
            }

            return models;
        }

        /// <summary>轉換 MySQLMemoryHashModel 資料集合成 HashProperty 資料陣列
        /// </summary>
        /// <param name="hashModels">MySQLMemoryHashModel 資料集合</param>
        /// <returns>HashProperty 資料陣列</returns>
        internal static HashProperty[] ConvertToHashProperty( IEnumerable<MySQLMemoryHashModel> hashModels )
        {
            if ( hashModels.IsNullOrEmptyCollection() )
            {
                return null;
            }

            var hashProperties = new List<HashProperty>();

            foreach ( var hashModel in hashModels )
            {
                var hashProperty = new HashProperty()
                {
                    Subkey = hashModel.Subkey,
                    Value  = hashModel.CacheValue
                };

                hashProperties.Add( hashProperty );
            }

            return hashProperties.ToArray();
        }
    }
}
