﻿using StackExchange.Redis;
using System;
using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Caching
{
    /// <summary>Redis 服務 Hash 資料結構的指令參數
    /// </summary>
    [Serializable()]
    internal sealed class RedisHashCommandArgs : BaseDynamicObject
    {
        /// <summary>資料快取 Key 值
        /// </summary>
        internal string CacheKey { get; set; }

        /// <summary>Redis Hash 子結構的識別名稱
        /// </summary>
        internal string Subkey { get; set; }

        /// <summary>Redis 指令的動作名稱
        /// </summary>
        internal string Action { get; set; }

        /// <summary>Hash 資料集合陣列
        /// </summary>
        internal HashEntry[] HashEntries { get; set; }
    }
}
