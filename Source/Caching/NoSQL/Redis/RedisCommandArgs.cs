﻿using StackExchange.Redis;
using System;
using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Caching
{
    /// <summary>Redis 服務的指令參數
    /// </summary>
    [Serializable()]
    internal sealed class RedisCommandArgs : BaseDynamicObject
    {
        /// <summary>資料快取 Key 值
        /// </summary>
        internal string CacheKey { get; set; }

        /// <summary>Redis 指令的動作名稱
        /// </summary>
        internal string Action { get; set; }

        /// <summary>資料內容
        /// </summary>
        internal string Data { get; set; }
    }
}
