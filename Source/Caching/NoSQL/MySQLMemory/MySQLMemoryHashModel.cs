﻿using Newtonsoft.Json;
using System;


namespace ZayniFramework.Caching
{
    /// <summary>遠端 MySQL 記憶體快取 HashProperty 的資料模型
    /// </summary>
    [Serializable()]
    internal sealed class MySQLMemoryHashModel
    {
        /// <summary>資料快取流水號
        /// </summary>
        [JsonProperty( "SRNO" )]
        public long SrNo { get; set; }

        /// <summary>資料快取 Key 值
        /// </summary>
        [JsonProperty( "CACHE_ID" )]
        public string CacheId { get; set; }

        /// <summary>資料快取 Subkey 值
        /// </summary>
        [JsonProperty( "SUB_KEY" )]
        public string Subkey { get; set; }
        
        /// <summary>資料快取內容 (字串最大長度為20000)
        /// </summary>
        [JsonProperty( "CACHE_VALUE" )]
        public string CacheValue { get; set; }

        /// <summary>快取資料異動時間
        /// </summary>
        [JsonProperty( "DATA_FLAG" )]
        public DateTime DataFlag { get; set; }
    }
}
