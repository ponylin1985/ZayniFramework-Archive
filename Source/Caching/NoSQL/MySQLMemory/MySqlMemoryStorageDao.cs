﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace ZayniFramework.Caching
{
    // ===========================================
    // Pony Says: 經過我簡單的測試後發現如下:
    // 在這邊最好能盡量控制讓 Dao 都使用同一個 MySqlConnection，並且盡量讓資料庫連線是開啟的!
    // 雖然說 ADO.NET 底層確實有 Connection Pool 機制，但是，經過我測下來的結果發現
    // 如果這邊依賴 Connection Pool 機制，每次存取 MySQL Memeory Table，還是要花個 100 多毫秒，這樣太慢了!
    // 但如果這邊我自己成是可以控制都使用同一個開啟的 MySQL 資料庫連線
    // 這樣存取 MySQL Memory engine 的 table 都在 10 毫秒內... (後來測發現，網路正常情況下，通常不超過 5 毫秒...)
    // 這樣才有真的 In-memory cache 的效能啊!
    // ===========================================

    /// <summary>遠端 MySQL 記憶體快取倉儲的資料存取類別
    /// </summary>
    internal sealed class MySqlMemoryStorageDao : BaseMySqlDal
    {
        #region 宣告私有的欄位

        /// <summary>MySQL 資料庫連線名稱
        /// </summary>
        private string _connectionName;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="name">MySQL 資料庫連線名稱</param>
        internal MySqlMemoryStorageDao( string name ) : base( name )
        {
            _connectionName = name;

            if ( MySqlConnectionPool.Contains( name ) )
            {
                return;
            }

            MySqlConnection connection = base.CreateOpenConnection();

            if ( !MySqlConnectionPool.Set( name, connection ) )
            {
                Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( MySqlMemoryStorageDao ) )}, Set MySQL db connection to pool occur error.", nameof ( MySqlMemoryStorageDao ) );
                return;
            }
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>取得快取資料
        /// </summary>
        /// <returns>快取資料內容字串</returns>
        internal Result<long> Count()
        {
            #region 初始化回傳值

            var result = new Result<long>()
            {
                Success = false,
                Data      = -1
            };

            long count = -1;

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sql = @" SELECT COUNT(*)
                                FROM `FS_CACHING_STORAGE`; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( Count ) )}, {result.Message}", GetLogActionName( nameof ( Count ) ) );
                    return result;   
                }

                using ( MySqlCommand cmdSelect = base.GetSqlStringCommand( connection, sql ) )
                {
                    int j = base.ExecuteScalar( cmdSelect );

                    if ( j < 0 )
                    {
                        result.Message = "No cache data in MySQL fs_caching_storage memory table.";
                        return result;
                    }

                    count = j;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute SQL command occur exception. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( Count ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令

            #region 設定回傳值屬性

            result.Data      = count;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>取得快取倉儲中的 Key 值集合
        /// </summary>
        /// <param name="pattern">Key 值的規則</param>
        /// <returns>取得結果</returns>
        internal Result<List<string>> SelectKeys( string pattern = null )
        {
            #region 初始化回傳值

            var result = new Result<List<string>>()
            {
                Success = false,
                Data      = null
            };

            List<string> keys = new List<string>();

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string where = pattern.IsNotNullOrEmpty() ? $@"WHERE CACHE_ID LIKE '{pattern}%'; " : ";";

            string sql = $@" SELECT CACHE_ID
                                FROM `FS_CACHING_STORAGE`
                                  {where}; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            List<MySQLMemoryModel> models;

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( SelectKeys ) ) );
                    return result;   
                }

                using ( MySqlCommand cmdSelect = base.GetSqlStringCommand( connection, sql ) )
                {
                    if ( !base.LoadData( cmdSelect, out models ) )
                    {
                        result.Message = $"Execute query SQL command occur error. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( SelectKeys ) )}, {result.Message}", GetLogActionName( nameof ( SelectKeys ) ) );
                        return result;
                    }
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute query SQL command occur exception. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( SelectKeys ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令

            #region 檢查資料筆數

            if ( models.IsNullOrEmptyList() )
            {
                result.Success = true;
                result.Message   = $"";
                return result;
            }

            #endregion 檢查資料筆數

            #region 設定回傳值屬性

            result.Data      = models.Select( m => m.CacheId )?.ToList();
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>取得快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別代碼</param>
        /// <returns>快取資料內容字串</returns>
        internal Result<string> Select( string cacheId )
        {
            #region 初始化回傳值

            var result = new Result<string>()
            {
                Success = false,
                Data      = null
            };

            string cacheValue = null;

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sql = @" SELECT CACHE_VALUE
                                FROM `FS_CACHING_STORAGE`
                                  WHERE CACHE_ID = ?CacheId; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( Select ) ) );
                    return result;   
                }

                using ( MySqlCommand cmdSelect = base.GetSqlStringCommand( connection, sql ) )
                {
                    base.AddInParameter( cmdSelect, "?CacheId", MySqlDbType.VarChar, cacheId );

                    if ( !base.ExecuteScalar<string>( cmdSelect, out string cacheContent ) )
                    {
                        result.Message = $"Execute query SQL command occur error. CacheId: {cacheId}. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( Select ) )}, {result.Message}", GetLogActionName( nameof ( Select ) ) );
                        return result;
                    }
                    
                    cacheValue = cacheContent;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute query SQL command occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( Select ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令

            #region 設定回傳值屬性

            result.Data      = cacheValue;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>檢查指定的資料快取識別代碼是否存在
        /// </summary>
        /// <param name="cacheId">資料快取識別代碼</param>
        /// <returns>指定的資料快取識別代碼是否存在</returns>
        internal Result<bool> Contains( string cacheId )
        {
            #region 初始化回傳值

            var result = new Result<bool>()
            {
                Success = false,
                Data      = false
            };

            bool exits = false;

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sql = @" SELECT EXISTS ( 
                                SELECT 1 FROM `FS_CACHING_STORAGE` WHERE CACHE_ID = ?CacheId ) AS `Contains`; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( Contains ) ) );
                    return result;   
                }

                using ( MySqlCommand cmdSelect = base.GetSqlStringCommand( connection, sql ) )
                {
                    base.AddInParameter( cmdSelect, "?CacheId", MySqlDbType.VarChar, cacheId );

                    int j = base.ExecuteScalar( cmdSelect );

                    if ( j < 0 )
                    {
                        result.Message = $"Execute query SQL command occur error. CacheId: {cacheId}. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( Contains ) )}, {result.Message}", GetLogActionName( nameof ( Contains ) ) );
                        return result;
                    }
                    
                    exits = 1 == j;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute query SQL command occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( Contains ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令

            #region 設定回傳值屬性

            result.Data      = exits;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>新增快取資料
        /// </summary>
        /// <param name="model">遠端 MySQL 記憶體快取的資料模型</param>
        /// <returns>新增結果</returns>
        internal Result<long> Insert( MySQLMemoryModel model )
        {
            #region 初始化回傳值

            var result = new Result<long>()
            {
                Success = false,
                Data      = -1
            };

            long srNo = -1;

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sql = @" INSERT INTO `FS_CACHING_STORAGE` (
                                CACHE_ID
                              , CACHE_VALUE
                            ) VALUES (
                                ?CacheId
                              , ?CacheValue
                            ); ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( Insert ) ) );
                    return result;   
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( connection, sql ) )
                {
                    base.AddInParameter( cmd, "?CacheId",    MySqlDbType.VarChar, model.CacheId );
                    base.AddInParameter( cmd, "?CacheValue", MySqlDbType.VarChar, model.CacheValue );

                    if ( !base.ExecuteNonQuery( cmd, out int j ) )
                    {
                        result.Message = $"Execute insert SQL command occur error. {base.Message}{Environment.NewLine}{JsonConvertUtil.Serialize( model )}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( Insert ) )}, {result.Message}", GetLogActionName( nameof ( Insert ) ) );
                        return result;
                    }

                    if ( 1 != j )
                    {
                        result.Message = $"Execute insert SQL command fail. {base.Message}{Environment.NewLine}{JsonConvertUtil.Serialize( model )}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( Insert ) )}, {result.Message}", GetLogActionName( nameof ( Insert ) ) );
                        return result;
                    }

                    srNo = cmd.LastInsertedId;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute insert SQL command occur exception. CacheId: {model?.CacheId}, CacheValue: {model?.CacheValue}{Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( Insert ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令

            #region 設定回傳值屬性

            result.Data      = srNo;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>更新快取資料
        /// </summary>
        /// <param name="model">遠端 MySQL 記憶體快取的資料模型</param>
        /// <returns>更新結果</returns>
        internal Result Update( MySQLMemoryModel model )
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false,
                Message   = null
            };

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sql = @" UPDATE `FS_CACHING_STORAGE`
                                SET CACHE_VALUE = ?CacheValue
                                WHERE CACHE_ID  = ?CacheId; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( Update ) ) );
                    return result;   
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( connection, sql ) )
                {
                    base.AddInParameter( cmd, "?CacheValue", MySqlDbType.VarChar, model.CacheValue );
                    base.AddInParameter( cmd, "?CacheId",    MySqlDbType.VarChar, model.CacheId );

                    if ( !base.ExecuteNonQuery( cmd, out int j ) )
                    {
                        result.Message = $"Execute update SQL command occur error. {base.Message}{Environment.NewLine}{JsonConvertUtil.Serialize( model )}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( Update ) )}, {result.Message}", GetLogActionName( nameof ( Update ) ) );
                        return result;
                    }

                    if ( 1 != j )
                    {
                        result.Message = $"Execute insert SQL command fail. {base.Message}{Environment.NewLine}{JsonConvertUtil.Serialize( model )}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( Update ) )}, {result.Message}", GetLogActionName( nameof ( Update ) ) );
                        return result;
                    }
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute update SQL command occur exception. CacheId: {model?.CacheId}, CacheValue: {model?.CacheValue}{Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( Update ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令
            
            result.Success = true;
            return result;
        }

        /// <summary>刪除快取資料
        /// </summary>
        /// <param name="cacheId">資料快取識別代碼</param>
        /// <returns>刪除結果</returns>
        internal Result Delete( string cacheId )
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false,
                Message   = null
            };

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sql = 
                @" DELETE FROM `FS_CACHING_STORAGE`
                       WHERE CACHE_ID = ?CacheId; 

                   DELETE FROM `FS_CACHING_STORAGE_HASH`
                       WHERE CACHE_ID = ?CacheId; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( Delete ) ) );
                    return result;   
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( connection, sql ) )
                {
                    base.AddInParameter( cmd, "?CacheId", MySqlDbType.VarChar, cacheId );

                    if ( !base.ExecuteNonQuery( cmd, out int j ) )
                    {
                        result.Message = $"Execute delete SQL command occur error. CacheId: {cacheId}. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( Delete ) )}, {result.Message}", GetLogActionName( nameof ( Delete ) ) );
                        return result;
                    }
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute delete SQL command occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( Delete ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令
            
            result.Success = true;
            return result;
        }

        /// <summary>清空快取資料
        /// </summary>
        /// <returns>清空結果</returns>
        internal Result Clear()
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false,
                Message   = null
            };

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sql = 
                @" TRUNCATE TABLE `FS_CACHING_STORAGE`;
                   TRUNCATE TABLE `FS_CACHING_STORAGE_HASH`; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( Clear ) ) );
                    return result;   
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( connection, sql ) )
                {
                    if ( !base.ExecuteNonQuery( cmd, out int j ) )
                    {
                        result.Message = $"Execute truncate table SQL command occur error. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( Clear ) )}, {result.Message}", GetLogActionName( nameof ( Clear ) ) );
                        return result;
                    }
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute truncate table SQL command occur exception. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( Clear ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令
            
            result.Success = true;
            return result;
        }

        /// <summary>新增或更新 HashProperty 的快取資料
        /// </summary>
        /// <param name="models">HashProperty 的快取資料集合</param>
        /// <returns>執行結果</returns>
        internal Result InsertOrUpdateHash( MySQLMemoryHashModel[] models )
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false,
                Message   = null
            };

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sqlInsertMaster = 
                @" INSERT IGNORE INTO `FS_CACHING_STORAGE` (
                         DATA_TYPE
                       , CACHE_ID
                   ) VALUES (
                         ?DataType
                       , ?CacheId
                   ); ";

            string sqlInsertUpdate = 
                @" INSERT INTO `FS_CACHING_STORAGE_HASH` (
                        CACHE_ID
                      , SUB_KEY
                      , CACHE_VALUE
                   ) VALUES {columns}
                    ON DUPLICATE KEY UPDATE CACHE_VALUE = VALUES( CACHE_VALUE ); ";

            string sqlParameters = "( '{id}', '{skey}', '{val}' ),";

            var sbParameters = new StringBuilder();

            foreach ( var model in models )
            {
                sbParameters.AppendLine( sqlParameters.Format( id => model.CacheId, skey => model.Subkey, val => model.CacheValue ) );
            }

            string sqlInsertColumns = sbParameters.ToString().RemoveLastAppeared( "," );
            string sql = $"{sqlInsertMaster}{Environment.NewLine}{sqlInsertUpdate.Format( columns => sqlInsertColumns )}";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool. ConnectionName: {_connectionName}.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( InsertOrUpdateHash ) ) );
                    return result;   
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( connection, sql ) )
                {
                    var model = models.FirstOrDefault();
                    base.AddInParameter( cmd, "?DataType", MySqlDbType.VarChar, "HashProperty" );
                    base.AddInParameter( cmd, "?CacheId",  MySqlDbType.VarChar, model.CacheId );

                    if ( !base.ExecuteNonQuery( cmd, out int j ) )
                    {
                        result.Message = $"Execute insert-update table SQL command occur error. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( InsertOrUpdateHash ) )}, {result.Message}", GetLogActionName( nameof ( InsertOrUpdateHash ) ) );
                        return result;
                    }
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute insert-update table SQL command occur exception. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( InsertOrUpdateHash ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令

            result.Success = true;
            return result;
        }

        /// <summary>查詢 HashProperty 資料集合
        /// </summary>
        /// <param name="cacheId">資料快取識別代碼</param>
        /// <returns>查詢結果</returns>
        internal Result<List<MySQLMemoryHashModel>> SelectHash( string cacheId )
        {
            #region 初始化回傳值

            var result = new Result<List<MySQLMemoryHashModel>>()
            {
                Success = false,
                Data      = null
            };

            List<MySQLMemoryHashModel> models;

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sqlSelect = 
                @" SELECT EXISTS ( 
                       SELECT 1 FROM `FS_CACHING_STORAGE_HASH` WHERE CACHE_ID = ?CacheId ) AS `Contains`; ";

            string sql = 
                @" SELECT CACHE_ID
                        , SUB_KEY
                        , CACHE_VALUE
                    FROM `FS_CACHING_STORAGE_HASH`
                      WHERE CACHE_ID = ?CacheId; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( SelectHash ) ) );
                    return result;   
                }

                using ( MySqlCommand cmdSelect = base.GetSqlStringCommand( connection, sqlSelect ) )
                {
                    base.AddInParameter( cmdSelect, "?CacheId", MySqlDbType.VarChar, cacheId );

                    if ( !base.ExecuteScalar( cmdSelect, out object obj ) )
                    {
                        result.Message = $"Execute query SQL command occur error. CacheId: {cacheId}. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( SelectHashValue ) )}, {result.Message}", GetLogActionName( nameof ( SelectHashValue ) ) );
                        return result;
                    }

                    if ( 1 != Convert.ToInt32( obj ) )
                    {
                        result.Message = $"No hash data found from MySQL memory fs_caching_storage_hash. CacheId: {cacheId}.";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( SelectHashValue ) )}, {result.Message}", GetLogActionName( nameof ( SelectHashValue ) ) );
                        return result;
                    }
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( connection, sql ) )
                {
                    base.AddInParameter( cmd, "?CacheId", MySqlDbType.VarChar, cacheId );

                    if ( !base.LoadData( cmd, out models ) )
                    {
                        result.Message = $"Execute query SQL command occur error. CacheId: {cacheId}. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( SelectHash ) )}, {result.Message}", GetLogActionName( nameof ( SelectHashValue ) ) );
                        return result;
                    }
                }

                if ( models.IsNullOrEmptyList() )
                {
                    result.Message = $"No cache data found from MySQL memory storage. CacheId: {cacheId}.";
                    Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( SelectHash ) )}, {result.Message}", GetLogActionName( nameof ( SelectHashValue ) ) );
                    return result;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute query SQL command occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( SelectHash ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令

            #region 設定回傳值屬性

            result.Data      = models;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>查詢 HashProperty 資料的資料值
        /// </summary>
        /// <param name="cacheId">資料快取識別代碼</param>
        /// <param name="subkey">資料快取 Subkey 值</param>
        /// <returns>查詢結果</returns>
        internal Result<string> SelectHashValue( string cacheId, string subkey )
        {
            #region 初始化回傳值

            var result = new Result<string>()
            {
                Success = false,
                Data      = null
            };

            string cacheValue = null;

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sqlSelect = 
                @" SELECT EXISTS ( 
                       SELECT 1 FROM `FS_CACHING_STORAGE_HASH` 
                           WHERE CACHE_ID = ?CacheId AND SUB_KEY = ?Subkey ) AS `Contains`; ";

            string sql = 
                @" SELECT CACHE_VALUE
                       FROM `FS_CACHING_STORAGE_HASH`
                       WHERE CACHE_ID = ?CacheId
                         AND SUB_KEY  = ?Subkey; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( SelectHashValue ) ) );
                    return result;   
                }

                using ( MySqlCommand cmdSelect = base.GetSqlStringCommand( connection, sqlSelect ) )
                {
                    base.AddInParameter( cmdSelect, "?CacheId", MySqlDbType.VarChar, cacheId );
                    base.AddInParameter( cmdSelect, "?Subkey",  MySqlDbType.VarChar, subkey );

                    if ( !base.ExecuteScalar( cmdSelect, out object obj ) )
                    {
                        result.Message = $"Execute query SQL command occur error. CacheId: {cacheId}, Subkey: {subkey}. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( SelectHashValue ) )}, {result.Message}", GetLogActionName( nameof ( SelectHashValue ) ) );
                        return result;
                    }

                    if ( 1 != Convert.ToInt32( obj ) )
                    {
                        result.Message = $"No hash data found from MySQL memory fs_caching_storage_hash. CacheId: {cacheId}, Subkey: {subkey}.";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( SelectHashValue ) )}, {result.Message}", GetLogActionName( nameof ( SelectHashValue ) ) );
                        return result;
                    }
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( connection, sql ) )
                {
                    base.AddInParameter( cmd, "?CacheId", MySqlDbType.VarChar, cacheId );
                    base.AddInParameter( cmd, "?Subkey",  MySqlDbType.VarChar, subkey );

                    if ( !base.ExecuteScalar<string>( cmd, out string cacheContent ) )
                    {
                        result.Message = $"Execute query SQL command occur error. CacheId: {cacheId}, Subkey: {subkey}. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( SelectHashValue ) )}, {result.Message}", GetLogActionName( nameof ( SelectHashValue ) ) );
                        return result;
                    }
                    
                    cacheValue = cacheContent;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute query SQL command occur exception. CacheId: {cacheId}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( SelectHashValue ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令

            #region 設定回傳值屬性

            result.Data      = cacheValue;
            result.Success = true;

            #endregion 設定回傳值屬性

            return result;
        }

        /// <summary>刪除 HashProperty 資料
        /// </summary>
        /// <param name="cacheId">資料快取識別代碼</param>
        /// <param name="subkey">資料快取 Subkey 值</param>
        /// <returns>刪除結果</returns>
        internal Result DeleteHash( string cacheId, string subkey )
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false,
                Message   = null
            };

            #endregion 初始化回傳值

            #region 宣告 SQL 字串

            string sql = 
                @" DELETE FROM `FS_CACHING_STORAGE_HASH`
                       WHERE CACHE_ID = ?CacheId
                         AND SUB_KEY  = ?Subkey; ";

            #endregion 宣告 SQL 字串

            #region 執行 SQL 指令

            try
            {
                MySqlConnection connection = MySqlConnectionPool.Get( _connectionName );

                if ( connection.IsNull() )
                {
                    result.Message = $"MySQL connection error. Can not get active MySQL connection from connection pool.";
                    Logger.WriteErrorLog( this, result.Message, GetLogActionName( nameof ( DeleteHash ) ) );
                    return result;   
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( connection, sql ) )
                {
                    base.AddInParameter( cmd, "?CacheId", MySqlDbType.VarChar, cacheId );
                    base.AddInParameter( cmd, "?Subkey",  MySqlDbType.VarChar, subkey );

                    if ( !base.ExecuteNonQuery( cmd, out int j ) )
                    {
                        result.Message = $"Execute delete SQL command occur error. CacheId: {cacheId}, Subkey: {subkey}. {base.Message}";
                        Logger.WriteErrorLog( this, $"{GetLogActionName( nameof ( DeleteHash ) )}, {result.Message}", GetLogActionName( nameof ( Delete ) ) );
                        return result;
                    }
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute delete SQL command occur exception. CacheId: {cacheId}, Subkey: {subkey}. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"{GetLogActionName( nameof ( DeleteHash ) )}, {result.Message}" );
                return result;
            }

            #endregion 執行 SQL 指令
            
            result.Success = true;
            return result;
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>取得方法名稱的日誌訊息
        /// </summary>
        /// <param name="methodName">目標方法名稱</param>
        /// <returns>方法名稱的日誌訊息</returns>
        private static string GetLogActionName( string methodName ) => $"{nameof ( MySqlMemoryStorageDao )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
