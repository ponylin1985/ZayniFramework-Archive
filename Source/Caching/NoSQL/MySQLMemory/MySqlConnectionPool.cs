﻿using MySql.Data.MySqlClient;
using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Data;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>有效的 MySQL 資料庫連線池
    /// </summary>
    internal static class MySqlConnectionPool
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>MySQL 資料庫的有效連線池<para/>
        /// Key值: 資料庫連線名稱。<para/>
        /// Value值: MySQL 資料庫連線。
        /// </summary>
        private static readonly Dictionary<string, MySqlConnection> _connections = new Dictionary<string, MySqlConnection>();

        #endregion 宣告私有的欄位


        #region 宣告內部的屬性

        /// <summary>訊息
        /// </summary>
        internal static string Message { get; private set; }

        #endregion 宣告內部的屬性


        #region 宣告內部的方法

        /// <summary>檢查是否存在指定名稱的 MySQL 資料庫連線
        /// </summary>
        /// <param name="name">資料庫連線名稱</param>
        /// <returns>是否存在指定名稱的 MySQL 資料庫連線</returns>
        internal static bool Contains( string name ) => _connections.ContainsKey( name );

        /// <summary>取得有效的 MySQL 資料庫連線
        /// </summary>
        /// <param name="name">資料庫連線名稱</param>
        /// <returns>有效的 MySQL 資料庫連線</returns>
        internal static MySqlConnection Get( string name ) => 
            _connections.TryGetValue( name, out MySqlConnection connection ) ? connection : null;

        /// <summary>新增有效的 MySQL 資料庫連線
        /// </summary>
        /// <param name="name">資料庫連線名稱</param>
        /// <param name="connection">有效的 MySQL 資料庫連線</param>
        /// <returns>新增結果</returns>
        internal static bool Set( string name, MySqlConnection connection )
        {
            using ( _asyncLock.Lock() )
            {
                if ( _connections.ContainsKey( name ) )
                {
                    return true;
                }

                try
                {
                    if ( connection.IsNull() )
                    {
                        Message = $"{GetLogTraceName( nameof ( Set ) )}, Add MySQL connection to pool fail due to connection is null.";
                        Logger.WriteErrorLog( nameof ( MySqlConnectionPool ), Message, nameof ( MySqlConnectionPool ) );
                        return false;
                    }

                    if ( connection.State != ConnectionState.Open )
                    {
                        Message = $"{GetLogTraceName( nameof ( Set ) )}, Add MySQL connection to pool fail due to the connection state is not 'Open'. ConnectionState: {connection.State.ToString()}";
                        Logger.WriteErrorLog( nameof ( MySqlConnectionPool ), Message, nameof ( MySqlConnectionPool ) );
                        return false;
                    }

                    _connections.Add( name, connection );
                }
                catch ( Exception ex )
                {
                    Message = $"{GetLogTraceName( nameof ( Set ) )}, Add MySQL connection to pool occur exception. {ex.ToString()}";
                    Logger.WriteExceptionLog( nameof ( MySqlConnectionPool ), ex, Message );
                    return false;
                }

                return true;
            }
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( MySqlConnectionPool )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
