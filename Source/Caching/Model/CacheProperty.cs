﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;


namespace ZayniFramework.Caching
{
    /// <summary>快取資料儲存屬性
    /// </summary>
    [Serializable()]
    public sealed class CacheProperty
    {
        #region 宣告公開的屬性

        /// <summary>資料快取 ID 識別碼
        /// </summary>
        [JsonPropertyName( "cacheId" )]
        [JsonProperty( PropertyName = "cacheId" )]
        public string CacheId { get; set; }

        /// <summary>資料內容
        /// </summary>
        [JsonPropertyName( "data" )]
        [JsonProperty( PropertyName = "data" )]
        public object Data { get; set; }

        /// <summary>資料逾時的時間參數<para/>
        /// * 單位為分鐘。
        /// </summary>
        [JsonPropertyName( "expireInterval" )]
        [JsonProperty( PropertyName = "expireInterval" )]
        public int? ExpireInterval { get; set; }

        /// <summary>常駐資料更新時間間隔 (單位為分鐘)<para/>
        /// 當設定此參數時，ExpireInterval 參數必須為 Null 或 0，否則視為不合法的參數動作。
        /// </summary>
        [JsonPropertyName( "refreshInterval" )]
        [JsonProperty( PropertyName = "refreshInterval" )]
        public int? RefreshInterval { get; set; }

        /// <summary>常駐資料的更新委派
        /// </summary>
        [System.Text.Json.Serialization.JsonIgnore()]
        [Newtonsoft.Json.JsonIgnore()]
        public RefreshCacheDataHandler RefreshCallback { get; set; }

        #endregion 宣告公開的屬性


        #region 宣告內部的屬性

        /// <summary>資料異動識別 ID 代碼，每次更新後資料後改變。
        /// </summary>
        [JsonPropertyName( "dataId" )]
        [JsonProperty( PropertyName = "dataId" )]
        internal string DataId { get; set; }

        /// <summary>資料最後更新時間 (資料新增或更新至快取倉儲時的時間戳記)
        /// </summary>
        [JsonPropertyName( "refreshTime" )]
        [JsonProperty( PropertyName = "refreshTime" )]
        internal DateTime RefreshTime { get; set; }

        /// <summary>資料逾時過期的時間
        /// </summary>
        [JsonPropertyName( "expireTime" )]
        [JsonProperty( PropertyName = "expireTime" )]
        internal DateTime? ExpireTime { get; set; }

        /// <summary>資料自動更新參數
        /// </summary>
        [System.Text.Json.Serialization.JsonIgnore()]
        [Newtonsoft.Json.JsonIgnore()]
        internal DataRefreshProperty RefreshProperty { get; set; }

        #endregion 宣告內部的屬性
    }
}
