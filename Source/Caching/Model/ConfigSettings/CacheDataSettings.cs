﻿using System;


namespace ZayniFramework.Caching
{
    /// <summary>快取資料設定值
    /// </summary>
    internal static class CacheDataSettings
    {
        /// <summary>快取資料的逾時時間間隔設定 (單位為分鐘)
        /// </summary>
        internal static int TimeoutInterval { get; set; }
    }
}
