﻿namespace ZayniFramework.Caching
{
    /// <summary>Redis 快取服務的預設 Config 設定
    /// </summary>
    internal static class RedisCacheConfigs
    {
        /// <summary>預設 Redis Server 服務的位址
        /// </summary>
        internal static string DefaultHost { get; set; }

        /// <summary>預設 Redis Server 服務的連接阜號
        /// </summary>
        internal static int DefaultPort { get; set; } = 6379;

        /// <summary>預設 Redis Server 服務的連線密碼
        /// </summary>
        internal static string DefaultPassword { get; set; }

        /// <summary>預設 Redis Server 服務的資料庫
        /// </summary>
        internal static int DefaultDatabaseNo { get; set; } = 0;
    }
}
