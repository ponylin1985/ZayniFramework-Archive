﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>Redis 服務快取的管理容器
    /// </summary>
    public static class RedisCacheContainer
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>Redis 資料快取的字典容器<para/>
        /// Key值: RedisCache 在 Config 設定檔中的連線組態名稱。<para/>
        /// Value: RedisCache 服務快取實體。
        /// </summary>
        private static readonly Dictionary<string, RedisCache> _container = new Dictionary<string, RedisCache>();

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static RedisCacheContainer()
        {
            using ( _asyncLock.Lock() )
            {
                try
                {
                    var configs = ConfigReader.GetCacheConfigs();

                    if ( configs.IsNullOrEmptyList() )
                    {
                        Logger.WriteWarningLog( nameof ( RedisCacheContainer ), "No ZayniFramework/CachingSettings/RedisCache in config section. Can not initialize RedisCacheContainer.", $"{nameof ( RedisCacheContainer )} static ctor" );
                        return;
                    }

                    foreach ( var config in configs )
                    {
                        if ( _container.ContainsKey( config.Name ) )
                        {
                            Logger.WriteWarningLog( nameof ( RedisCacheContainer ), "Duplicate 'name' in ZayniFramework/CachingSettings/RedisCache in config section.", $"{nameof ( RedisCacheContainer )} static ctor" );
                            continue;
                        }

                        var redisCache = new RedisCache( config );
                        redisCache.Initailize();
                        _container.Add( config.Name, redisCache );
                    }
                }
                catch ( Exception ex )
                {
                    Logger.WriteErrorLog( nameof ( RedisCacheContainer ), $"RedisCacheContainer initialize process occur exception. {ex.ToString()}", $"{nameof ( RedisCacheContainer )} static ctor" );
                }
            }
        }

        #endregion 宣告靜態建構子


        #region 宣告公開的靜態方法

        /// <summary>取得預設的 RedisCache 快取服務
        /// </summary>
        /// <returns>取得結果: RedisCache 快取服務</returns>
        public static Result<RedisCache> GetDefault() => Get( ConfigReader.GetDefaultRedisCacheConfig()?.Name );

        /// <summary>取得指定組態名稱的 RedisCache 快取服務
        /// </summary>
        /// <param name="redisCacheName">RedisCache 於 Config 檔設定中的組態名稱: ZayniFramework/CachingSettings/RedisCache name屬性</param>
        /// <returns>取得結果: RedisCache 快取服務</returns>
        public static Result<RedisCache> Get( string redisCacheName )
        {
            var result = Result.Create<RedisCache>();

            if ( !_container.ContainsKey( redisCacheName ) )
            {
                result.Message = $"Can not get '{redisCacheName}' from RedisCacheContainer due to key not found.";
                Logger.WriteErrorLog( nameof ( RedisCacheContainer ), result.Message, Logger.GetTraceLogTitle( nameof ( RedisCacheContainer ), nameof ( Get ) ) );
                return result;
            }

            RedisCache redisCache = null;

            try
            {
                redisCache = _container[ redisCacheName ];
            }
            catch ( Exception ex )
            {
                result.Message = $"Get RedisCache instance from RedisCacheContainer occur exception. RedisCacheName: {redisCacheName}.";
                Logger.WriteExceptionLog( nameof ( RedisCacheContainer ), ex, Logger.GetTraceLogTitle( nameof ( RedisCacheContainer ), nameof ( Get ) ), result.Message );
                return result;
            }

            if ( redisCache.IsNull() )
            {
                result.Message = $"Retrive RedisCache instance from RedisCacheContainer is null. RedisCacheName: {redisCacheName}.";
                Logger.WriteErrorLog( nameof ( RedisCacheContainer ), result.Message, Logger.GetTraceLogTitle( nameof ( RedisCacheContainer ), nameof ( Get ) ) );
                return result;
            }

            result.Data    = redisCache;
            result.Success = true;
            return result;
        }

        #endregion 宣告公開的靜態方法
    }
}
