﻿using System;
using System.Text;
using ZayniFramework.Common.Tasks;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace ZayniFramework.Caching
{
    /// <summary>Redis 服務動作日誌器
    /// </summary>
    internal static class RedisActionLogger
    {
        #region 宣告私有的欄位

        /// <summary>Redis 服務動作日誌的非同步工作佇列處理器
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncWorker _worker = new AsyncWorker( 3, "Redis Action Database Logger AsyncWorker" );

        /// <summary>資料存取物件
        /// </summary>
        private static readonly RedisActionLogDao _dao = new RedisActionLogDao();

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        /// <returns></returns>
        static RedisActionLogger() => _worker.Start();

        #endregion 宣告靜態建構子


        #region 宣告內部的方法

        /// <summary>動作日誌紀錄
        /// </summary>
        /// <param name="redisHost">Redis服務位址</param>
        /// <param name="redisPort">Redis服務連線連接阜</param>
        /// <param name="requestId">原始請求代碼</param>
        /// <param name="direction">動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。</param>
        /// <param name="action">動作名稱</param>
        /// <param name="data">資料內容</param>
        /// <param name="message">框架內部訊息</param>
        internal static void Log( string redisHost, int redisPort, string requestId, int direction, string action, object data, string message = null ) => 
            _worker.Enqueue( () => WriteLog( redisHost, redisPort, requestId, direction, action, data ) );

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>動作日誌紀錄
        /// </summary>
        /// <param name="redisHost">Redis服務位址</param>
        /// <param name="redisPort">Redis服務連線連接阜</param>
        /// <param name="requestId">原始請求代碼</param>
        /// <param name="direction">動作方向，1: 對 Redis 發送指令請求，2: Redis 執行指令的回應。</param>
        /// <param name="action">動作名稱</param>
        /// <param name="data">資料內容</param>
        /// <param name="message">框架內部訊息</param>
        private static void WriteLog( string redisHost, int redisPort, string requestId, int direction, string action, object data, string message = null )
        {
            string stringContent = null;

            switch ( data )
            {
                case string jsonContent:
                    stringContent = JsonConvertUtil.FormatToCamelCase( jsonContent );
                    break;

                default:
                    if ( data is null )
                    {
                        stringContent = null;
                        break;
                    }

                    stringContent = JsonConvertUtil.SerializeInCamelCase( data );
                    break;
            }

            var model = new RedisActionLogModel()
            {
                RedisHost   = redisHost,
                RedisPort   = redisPort,
                RequestId   = requestId,
                Direction   = direction,
                Action      = action,
                DataContent = stringContent,
                LogTime     = DateTime.UtcNow
            };

            _dao.InsertLog( model );

            var sb = new StringBuilder();
            sb.AppendLine( $"RequestId: {requestId}" );
            sb.AppendLine( $"Direction: {direction}" );
            sb.AppendLine( $"Action: {action}" );
            sb.AppendLine( $"DataContent: {Environment.NewLine}{stringContent}" );

            Logger.WriteInformationLog( nameof ( RedisActionLogger ), sb.ToString(), $"Redis Action Trace. {redisHost}:{redisPort}", "RedisCacheTrace" );
        }

        #endregion 宣告私有的方法
    }
}
