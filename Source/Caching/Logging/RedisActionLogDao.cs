﻿using System;
using System.Data.Common;
using ZayniFramework.DataAccess;
using ZayniFramework.Logging;


namespace ZayniFramework.Caching
{
    /// <summary>Redis 服務動作日誌紀錄的資料存取類別
    /// </summary>
    internal sealed class RedisActionLogDao : BaseDataAccess
    {
        #region 宣告私有的欄位

        /// <summary>資料庫連線名稱
        /// </summary>
        private static readonly string _dbName = "Zayni";

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        internal RedisActionLogDao() : base( _dbName )
        {
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>寫入動作日誌
        /// </summary>
        /// <param name="model">日誌紀錄資料模型</param>
        internal void InsertLog( RedisActionLogModel model )
        {
            try
            {
                string sql = @" 
                    -- Insert FS_CACHING_REDIS_ACTION_LOG
                    INSERT INTO `FS_CACHING_REDIS_ACTION_LOG` (
                          `REDIS_HOST`
                        , `REDIS_PORT`
                        , `REQUEST_ID`
                        , `DIRECTION`
                        , `ACTION`

                        , `DATA_CONTENT`
                        , `LOG_TIME`
                    ) VALUES (
                          @REDIS_HOST
                        , @REDIS_PORT
                        , @REQUEST_ID
                        , @DIRECTION
                        , @ACTION

                        , @DATA_CONTENT
                        , @LOG_TIME
                    ) ";

                using ( var conn = base.CreateConnection() )
                {
                    var cmd = GetSqlStringCommand( sql, conn );
                    cmd.CommandTimeout = 60;
                    
                    base.AddInParameter( cmd, "@REDIS_HOST",   DbTypeCode.String,   model.RedisHost );
                    base.AddInParameter( cmd, "@REDIS_PORT",   DbTypeCode.Int32,    model.RedisPort );
                    base.AddInParameter( cmd, "@REQUEST_ID",   DbTypeCode.String,   model.RequestId );
                    base.AddInParameter( cmd, "@DIRECTION",    DbTypeCode.Int32,    model.Direction );
                    base.AddInParameter( cmd, "@ACTION",       DbTypeCode.String,   model.Action );

                    base.AddInParameter( cmd, "@DATA_CONTENT", DbTypeCode.String,   model.DataContent );
                    base.AddInParameter( cmd, "@LOG_TIME",     DbTypeCode.DateTime, model.LogTime );

                    if ( !base.ExecuteNonQuery( cmd ) )
                    {
                        Logger.WriteErrorLog( this, $"{nameof ( RedisActionLogDao )}, insert log data to FS_CACHING_REDIS_ACTION_LOG fail. {base.Message}", $"{GetLogTraceName( nameof ( InsertLog ) )}" );
                        conn.Close();
                        return;
                    }

                    conn.Close();
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteErrorLog( this, $"Write redis cache action log into FS_CACHING_REDIS_ACTION_LOG occur exception. {ex.ToString()}", Logger.GetTraceLogTitle( this, nameof ( InsertLog ) ) );
            }
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( RedisActionLogDao )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
