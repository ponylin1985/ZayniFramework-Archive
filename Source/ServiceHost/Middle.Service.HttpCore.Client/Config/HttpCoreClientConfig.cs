using Newtonsoft.Json;
using System;
using ZayniFramework.Middle.Service.Client;


namespace ZayniFramework.Middle.Service.HttpCore.Client
{
    /// <summary>HttpCore 通訊客戶端的設定組態
    /// </summary>
    [Serializable()]

    public sealed class HttpCoreClientConfig : RemoteClientConfig
    {
        /// <summary>HttpCore Host Web API 服務的 URL 服務位址。<para/>
        /// * httpHostUrl 設定值結尾必須要包含 `/` 字元，否則可能回報 HTTP 404 Not Found 錯誤。<para/>
        /// * httpHostUrl 設定值假若為 `/action/` 結尾，代表呼叫一般 Non-async 的 ServiceAction，譬如: `http://localhost:5110/action/`
        /// * httpHostUrl 設定值假若為 `/action/async/` 結尾，代表呼叫非同步作業 Async 的 ServiceActionAsync，譬如: `http://localhost:5100/action/async/`。
        /// </summary>
        [JsonProperty( PropertyName = "httpHostUrl" )]
        public string HttpHostUrl { get; internal set; }

        /// <summary>HttpCore 請求後等待 Http Response 回應的逾時秒數
        /// </summary>
        [JsonProperty( PropertyName = "httpResponseTimeout" )]
        public int HttpResponseTimeout { get; internal set; }
    }
}