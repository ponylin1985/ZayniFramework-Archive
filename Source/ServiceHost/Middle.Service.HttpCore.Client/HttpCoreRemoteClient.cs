﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Client;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Middle.Service.HttpCore.Entity;
using ZayniFramework.Serialization;


namespace ZayniFramework.Middle.Service.HttpCore.Client
{
    /// <summary>Http 客戶端類別
    /// </summary>
    public class HttpCoreRemoteClient : RemoteClient
    {
        #region 宣告私有的欄位

        /// <summary>Http 通訊客戶端獨體物件
        /// </summary>
        private static readonly HttpClient _httpClient = new HttpClient();

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly static AsyncLock _asyncLock = new AsyncLock();

        /// <summary>服務客戶端個體的設定組態
        /// </summary>
        private ServiceClientConfig _serviceClientConfig;

        /// <summary>HttpCore 通訊客戶端的設定組態
        /// </summary>
        private HttpCoreClientConfig _httpCoreClientConfig;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體設定名稱</param>
        public HttpCoreRemoteClient( string serviceClientName ) : base( serviceClientName )
        {
            ExecuteHandler      = Execute;
            ExecuteAsyncHandler = ExecuteAsync;
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>初始化遠端服務連線
        /// </summary>
        /// <returns>連線是否正常建立</returns>
        public override bool Initialize()
        {
            using ( _asyncLock.Lock() )
            {
                try
                {
                    _serviceClientConfig  = ClientConfigReader.GetServiceClientConfig( ServiceClientName );

                    dynamic config        = GlobalClientContext.JsonConfig;
                    JToken rClientCfg     = config.httpCoreProtocol.remoteClients;

                    var remoteClients     = rClientCfg.ToObject<List<HttpCoreClientConfig>>();
                    var remoteClientCfg   = remoteClients.Where( n => n.Name == _serviceClientConfig.RemoteClientName )?.FirstOrDefault();
                    _httpCoreClientConfig = remoteClientCfg;

                    var serivceHostUri      = new Uri( _httpCoreClientConfig.HttpHostUrl );
                    _httpClient.BaseAddress = serivceHostUri;
                    _httpClient.Timeout     = TimeSpan.FromSeconds( _httpCoreClientConfig.HttpResponseTimeout );

                    var sp = ServicePointManager.FindServicePoint( serivceHostUri );
                    sp.ConnectionLeaseTimeout = (int)TimeSpan.FromMinutes( 30 ).TotalMilliseconds;
                    ServicePointManager.DnsRefreshTimeout = (int)TimeSpan.FromMinutes( 30 ).TotalMilliseconds;
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( this, ex, nameof ( Initialize ), "Load HttpCoreClient config occur exception.", "ServiceHostTrace" );
                    return false;
                }

                return true;
            }
        }

        /// <summary>發送 Http RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestData">請求資料內容</param>
        /// <returns>執行結果</returns>
        public Result<ResponsePackage> Execute( string requestId, string actionName, object requestData = null ) 
        {
            var result = Result.Create<ResponsePackage>();

            #region 設定請求包裹

            var utcNow = DateTime.UtcNow;

            var httpRequestPackage = new HttpCoreRequestPackage()
            {
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId   = requestId,
                ActionName  = actionName,
                Data        = requestData,
                RequestTime = utcNow
            };

            var requestPackage = new RequestPackage()
            {
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId   = requestId,
                ActionName  = actionName,
                Data        = JsonConvertUtil.Serialize( requestData ),
                RequestTime = utcNow
            };

            ServiceClientActionLogger.Log( ServiceClientName, requestPackage );

            #endregion 設定請求包裹

            #region 執行遠端服務動作

            string resContent = null;

            try
            {
                _httpClient.DefaultRequestHeaders.Clear();
                
                var reqString  = JsonSerializer.Serialize( httpRequestPackage );
                var reqContent = new StringContent( reqString, Encoding.UTF8, "application/json" );

                // 由於在 serviceClientConfig.json 中設定的 httpHostUrl 參數會包含 API function path，也就是 POST /action/，因此，這邊傳入的 api path 參數必須給定為空字串。
                var httpResponse = _httpClient.PostAsync( string.Empty, reqContent ).ConfigureAwait( false ).GetAwaiter().GetResult();

                if ( httpResponse.IsNull() || httpResponse.StatusCode != HttpStatusCode.OK )
                {
                    result.Code    = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Retrive service host error http response. The http response status is not OK. RequestId: {requestId}, ActionName: {actionName}, HttpStatusCode: {httpResponse?.StatusCode}.";
                    Logger.WriteErrorLog( this, $"{result.Message}", nameof ( Execute ), "ServiceHostTrace" );
                    return result;
                }

                resContent = httpResponse.Content.ReadAsStringAsync().ConfigureAwait( false ).GetAwaiter().GetResult();
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteExceptionLog( this, ex, $"Execute remote action occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 執行遠端服務動作

            #region 檢查動作執行結果

            if ( resContent.IsNullOrEmpty() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteErrorLog( this, $"No RPC response from remote service. RequestId: {requestId}, ActionName: {actionName}.", nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            #endregion 檢查動作執行結果

            #region 取得遠端回應包裹

            ResponsePackage responsePackage;

            try
            {
                var resPackage = JsonSerializer.Deserialize<HttpResponsePackage>( resContent );

                responsePackage = new ResponsePackage() 
                {
                    PackageType = resPackage.PackageType,
                    RequestId   = resPackage.RequestId,
                    ActionName  = resPackage.ActionName,
                    Success     = resPackage.Success,
                    Code        = resPackage.Code,
                    Message     = resPackage.Message
                };

                var dataContent = (JsonElement)resPackage.Data;
                responsePackage.Data = dataContent.ToString();
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteExceptionLog( this, ex, $"Deserialize response data content from JSON occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            if ( responsePackage.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteErrorLog( this, $"Retrive null ResponsePackage from remote service. RequestId: {requestId}, ActionName: {actionName}.", nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            #endregion 取得遠端回應包裹

            #region 設定回傳值

            if ( !responsePackage.Success )
            {
                result.Code    = responsePackage.Code;
                result.Message = responsePackage.Message;
                return result;
            }

            result.Data    = responsePackage;
            result.Message = responsePackage.Message;
            result.Success = responsePackage.Success;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>發送 Http RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestData">請求資料內容</param>
        /// <returns>執行結果</returns>
        public async Task<Result<ResponsePackage>> ExecuteAsync( string requestId, string actionName, object requestData = null ) 
        {
            var result = Result.Create<ResponsePackage>();

            #region 設定請求包裹

            var utcNow = DateTime.UtcNow;

            var httpRequestPackage = new HttpCoreRequestPackage()
            {
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId   = requestId,
                ActionName  = actionName,
                Data        = requestData,
                RequestTime = utcNow
            };

            var requestPackage = new RequestPackage()
            {
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId   = requestId,
                ActionName  = actionName,
                Data        = JsonConvertUtil.Serialize( requestData ),
                RequestTime = utcNow
            };

            ServiceClientActionLogger.Log( ServiceClientName, requestPackage );

            #endregion 設定請求包裹

            #region 執行遠端服務動作

            string resContent = null;

            try
            {
                _httpClient.DefaultRequestHeaders.Clear();
                
                var reqString  = JsonSerializer.Serialize( httpRequestPackage );
                var reqContent = new StringContent( reqString, Encoding.UTF8, "application/json" );

                // 由於在 serviceClientConfig.json 中設定的 httpHostUrl 參數會包含 API function path，也就是 POST /action/async/。
                var httpResponse = await _httpClient.PostAsync( "", reqContent ).ConfigureAwait( false );

                if ( httpResponse.IsNull() || httpResponse.StatusCode != HttpStatusCode.OK )
                {
                    result.Code    = StatusCode.INTERNAL_ERROR;
                    result.Message = $"Retrive service host error http response. The http response status is not OK. RequestId: {requestId}, ActionName: {actionName}, HttpStatusCode: {httpResponse?.StatusCode}.";
                    Logger.WriteErrorLog( this, $"{result.Message}", nameof ( ExecuteAsync ), "ServiceHostTrace" );
                    return result;
                }

                resContent = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait( false );
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteExceptionLog( this, ex, $"Execute remote action occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 執行遠端服務動作

            #region 檢查動作執行結果

            if ( resContent.IsNullOrEmpty() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteErrorLog( this, $"No RPC response from remote service. RequestId: {requestId}, ActionName: {actionName}.", nameof ( ExecuteAsync ), "ServiceHostTrace" );
                return result;
            }

            #endregion 檢查動作執行結果

            #region 取得遠端回應包裹

            ResponsePackage responsePackage;

            try
            {
                var resPackage = JsonSerializer.Deserialize<HttpResponsePackage>( resContent );

                responsePackage = new ResponsePackage() 
                {
                    PackageType = resPackage.PackageType,
                    RequestId   = resPackage.RequestId,
                    ActionName  = resPackage.ActionName,
                    Success     = resPackage.Success,
                    Code        = resPackage.Code,
                    Message     = resPackage.Message
                };

                var dataContent = (JsonElement)resPackage.Data;
                responsePackage.Data = dataContent.ToString();
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteExceptionLog( this, ex, $"Deserialize response data content from JSON occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            if ( responsePackage.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteErrorLog( this, $"Retrive null ResponsePackage from remote service. RequestId: {requestId}, ActionName: {actionName}.", nameof ( ExecuteAsync ), "ServiceHostTrace" );
                return result;
            }

            #endregion 取得遠端回應包裹

            #region 設定回傳值

            if ( !responsePackage.Success )
            {
                result.Code    = responsePackage.Code;
                result.Message = responsePackage.Message;
                return result;
            }

            result.Data    = responsePackage;
            result.Message = responsePackage.Message;
            result.Success = responsePackage.Success;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>發送 Http RPC 遠端請求訊息 (此方法實作在 runtime 不會被呼叫到!)
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Result<ResponsePackage> Execute( string requestId, string actionName, string requestDataContent = null ) =>
            throw new NoNeedToImplementException( $"No need to implement this method." );

        /// <summary>發送 Http RPC 遠端請求訊息 (此方法實作在 runtime 不會被呼叫到!)
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Task<Result<ResponsePackage>> ExecuteAsync( string requestId, string actionName, string requestDataContent = null ) => 
            throw new NoNeedToImplementException( $"No need to implement this method." );

        #endregion 宣告公開的方法
    }
}
