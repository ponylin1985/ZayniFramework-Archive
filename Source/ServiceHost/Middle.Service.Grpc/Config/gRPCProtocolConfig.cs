using Newtonsoft.Json;
using System;


namespace ZayniFramework.Middle.Service.Grpc
{
    /// <summary>gRPC 通訊連線組態設定
    /// </summary>
    [Serializable()]
    public class gRPCProtocolConfig : ProtocolConfig
    {
        /// <summary>gRPC Server 監聽的 domain 或 IP 位址
        /// </summary>
        /// <value></value>
        [JsonProperty( PropertyName = "gRPCHostServer" )]
        public string gRPCHostServer { get; set; }

        /// <summary>gRPC Server 監聽的 Port
        /// </summary>
        /// <value></value>
        [JsonProperty( PropertyName = "port" )]
        public int Port { get; set; }
    }
}