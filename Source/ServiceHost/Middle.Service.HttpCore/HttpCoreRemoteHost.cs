using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using NeoSmart.AsyncLock;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;



namespace ZayniFramework.Middle.Service.HttpCore
{
    /// <summary>ASP.NET Core Http 遠端自我裝載服務
    /// </summary>
    public class HttpCoreRemoteHost : IRemoteHost
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>IsHostReady 屬性的非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLockService = new AsyncLock();

        /// <summary>遠端服務是否註冊與啟動完成
        /// </summary>
        private bool _isHostReady;

        /// <summary>遠端服務
        /// </summary>
        private RemoteService _remoteService;

        /// <summary>ASP.NET Core Self-Hosted 通訊組態設定
        /// </summary>
        private HttpCoreProtocolConfig _config;

        /// <summary>ASP.NET Core 3.0 的自我裝載服務
        /// </summary>
        private IHost _host;


        #endregion 宣告私有的欄位


        #region 宣告公開的屬性

        /// <summary>服務設定名稱
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>服務託管 Host 的設定名稱
        /// </summary>
        public string HostName { get; set; }

        /// <summary>遠端服務是否註冊與啟動完成。
        /// </summary>
        public bool IsHostReady
        {
            get
            {
                using ( _asyncLockService.Lock() )
                {
                    return _isHostReady;
                }
            }
            private set
            {
                using ( _asyncLockService.Lock() )
                {
                    _isHostReady = value;
                }
            }
        }

        /// <summary>控制 HttpCore Remote Host 的全域性 System.Text.Json.JsonSerializer 的序列化處理參數委派。
        /// <para>* 此參數設定會影響所有 HttpCore Remote Host 的 JSON 序列化機制。</para>
        /// <para>* 如果未設定此 JsonSerializerOptions 委派，HttpCore remote host 啟動後，JsonSerializer 依照 ASP.NET Core Kestrel 預設設定進行序列化。</para>
        /// <para>* 當有確實指定此 JsonSerializerOptions 委派，HttpCore remote host 啟動後，JsonSerializer 則完全依照外部委派的參數進行 JSON 序列化處理，效果等同在 ASP.NET Core 的 `Startup.ConfigureServices( IServiceCollection services )` 進行以下 DI JSON 服務注射。</para><br/>
        /// <para>  `services.AddRazorPages().AddJsonOptions( o => o.JsonSerializerOptions.WriteIndented = true )`</para>
        /// </summary>
        /// <value></value>
        public static Action<JsonOptions> JsonSerializerOptions { get; set; }

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>初始化
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        public void Initialize( string serviceName = null )
        {
            ServiceName = serviceName.IsNullOrEmptyString( GlobalContext.Config.ServiceHosts?.FirstOrDefault()?.ServiceName );
            HostName    = ConfigReader.GetHostName( ServiceName );   // 這邊 serviceHostConfig.json 設定上的限制為: 不能使用 defaultHost，一定要明確指定 hostName 屬性!
            
            _remoteService = new RemoteService( ServiceName );
            RemoteServiceContainer.Add( ServiceName, _remoteService );

            dynamic config      = GlobalContext.JsonConfig;
            JToken  hostConfigs = config.httpCoreSettings.hosts;

            var hosts   = hostConfigs.ToObject<List<HttpCoreProtocolConfig>>();
            var hostCfg = hosts.Where( h => h.HostName == HostName )?.FirstOrDefault();
            _config     = hostCfg;
        }

        /// <summary>啟動自我裝載服務
        /// </summary>
        /// <returns>啟動結果</returns>
        public Result StartHost()
        {
            using ( _asyncLock.Lock() )
            {
                ConsoleOutputLogger.Log( $"Starting HttpCore remote host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"Starting HttpCore remote host service.", nameof ( StartHost ), "ServiceHostTrace" );

                var result = Result.Create();

                #region 檢查 ServiceHost 的啟動狀態

                if ( IsHostReady )
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查 ServiceHost 的啟動狀態

                #region 啟動 ServiceHost 服務

                try
                {
                    var keepAliveTimeout = _config.KeepAliveTimeout.IsNullOrEmpty() ? TimeSpan.FromMinutes( 15 ) : TimeSpan.Parse( _config.KeepAliveTimeout );

                    // ASP.NET Core 3.0
                    _host = 
                        Host.CreateDefaultBuilder()
                            .ConfigureWebHostDefaults( webBuilder => 
                            {
                                webBuilder
                                    .UseKestrel( options => 
                                    {
                                        options.Limits.KeepAliveTimeout = keepAliveTimeout;
                                    } )
                                    .UseContentRoot( Directory.GetCurrentDirectory() )
                                    .UseUrls( _config.HostBaseUrl )
                                    .UseStartup<Startup>();
                            } ).Build();
                        

                    // ASP.NET Core 2.2
                    // _host = new WebHostBuilder()
                    //     .UseKestrel( options => 
                    //     {
                    //         options.Limits.KeepAliveTimeout = keepAliveTimeout;
                    //     } )
                    //     .UseContentRoot( Directory.GetCurrentDirectory() )
                    //     .UseUrls( _config.HostBaseUrl )
                    //     .UseStartup<Startup>()
                    //     .Build();

                    new Thread( () => _host.Run() ).Start();

                    ConsoleOutputLogger.Log( $"Start HttpCore remote host service success. Listening http on {_config.HostBaseUrl}." );
                }
                catch ( Exception ex )
                {
                    result.Message = $"Start HttpCore {nameof ( IWebHost )} service occur exception: {ex.ToString()}";
                    Logger.WriteExceptionLog( this, ex, result.Message, "ServiceHostTrace" );
                    return result;
                }

                #endregion 啟動 ServiceHost 服務

                #region 設定回傳值

                IsHostReady    = true;
                result.Success = true;

                #endregion 設定回傳值

                ConsoleOutputLogger.Log( $"End of starting HttpCore remote host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"End of starting HttpCore remote host service.", nameof ( StartHost ), "ServiceHostTrace" );
                return result;
            }
        }

        /// <summary>關閉自我裝載服務
        /// </summary>
        /// <returns>關閉結果</returns>
        public Result StopHost()
        {
            using ( _asyncLock.Lock() )
            {
                ConsoleOutputLogger.Log( $"Stoping HttpCore remote host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"Stoping HttpCore remote host service.", nameof ( StartHost ), "ServiceHostTrace" );

                var result = Result.Create();

                #region 檢查 ServiceHost 的啟動狀態

                if ( !IsHostReady )
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查 ServiceHost 的啟動狀態

                #region 關閉 ServiceHost 服務

                try
                {
                    _host.StopAsync().ConfigureAwait( false );
                }
                catch ( Exception ex )
                {
                    result.Message = $"Stop HttpCore {nameof ( IWebHost )} service occur exception: {ex.ToString()}";
                    Logger.WriteExceptionLog( this, ex, result.Message, "ServiceHostTrace" );
                    return result;
                }

                #endregion 關閉 ServiceHost 服務

                #region 設定回傳值

                IsHostReady    = false;
                result.Success = true;

                #endregion 設定回傳值

                ConsoleOutputLogger.Log( $"End of stopping HttpCore remote host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"End of stopping HttpCore remote host service.", nameof ( StartHost ), "ServiceHostTrace" );
                return result;
            }
        }

        /// <summary>執行遠端服務動作
        /// </summary>
        /// <param name="request">遠端服務請求包裹</param>
        /// <returns>遠端服務回應包裹</returns>
        public ResponsePackage Execute( RequestPackage request ) => 
            RemoteServiceContainer.Get( ServiceName ).Execute( request );
        
        /// <summary>發佈事件訊息
        /// </summary>
        /// <param name="messagePackage">訊息資料包裹</param>
        /// <returns>訊息發佈結果</returns>
        public Result Publish( MessagePackage messagePackage ) => 
            throw new NotSupportedException( "HttpCore does not support message publish now..." );

        #endregion 宣告公開的方法
    }
}