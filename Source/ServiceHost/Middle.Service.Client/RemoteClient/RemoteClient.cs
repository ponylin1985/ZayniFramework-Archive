﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Serialization;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>服務客戶端基底元件
    /// </summary>
    public abstract class RemoteClient : IRemoteClient, IRemoteClientAsync
    {
        #region Private Fields

        /// <summary>非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>遠端請求的識別代碼池
        /// </summary>
        private readonly List<string> _requests = new List<string>();

        #endregion Private Fields


        #region Properties

        /// <summary>服務客戶端個體的設定名稱
        /// </summary>
        protected string ServiceClientName { get; private set; }

        /// <summary>連線是否正常建立
        /// </summary>
        protected bool IsReady { get; set; }

        /// <summary>執行 RPC 遠端呼叫的處理委派
        /// </summary>
        /// <value></value>
        protected ExecuteRpcHandler ExecuteHandler { get; set; }

        /// <summary>非同步作業執行 RPC 遠端呼叫的處理委派
        /// </summary>
        /// <value></value>
        protected ExecuteRpcAsyncHandler ExecuteAsyncHandler { get; set; }

        /// <summary>遠端服務的連線中斷的處理委派
        /// </summary>
        /// <value></value>
        public ConnectionBrokenHandler ConnectionBroken { get; set; }

        /// <summary>遠端服務的連線重新恢復的處理委派
        /// </summary>
        /// <value></value>
        public ConnectionRestoreHandler ConnectionRestored { get; set; }

        #endregion Properties


        #region Internal Property

        /// <summary>事件處理器容器
        /// </summary>
        internal EventProcessorContainer EventHandlers { get; private set; } = new EventProcessorContainer();

        #endregion Internal Property


        #region Constructor

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        /// <remarks>在我目前設計的架構中，這邊無論是哪一種 RemoteClient 物件備實體化，
        /// 執行到 Initialize() 初始化方法在 runtime 只會被執行一次，沒有重覆初始化的問題。
        /// </remarks>
        public RemoteClient( string serviceClientName )
        {
            try
            {
                ServiceClientName = serviceClientName;
                IsReady           = Initialize();
            }
            catch ( Exception ex )
            {
                IsReady = false;
                Logger.WriteExceptionLog( this, ex, $"Initialize connection occur exception.", "ServiceHostTrace" );
            }
        }

        #endregion Constructor


        #region Public Abstract

        /// <summary>初始化遠端服務連線
        /// </summary>
        /// <returns>連線是否正常建立</returns>
        public abstract bool Initialize();

        /// <summary>發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public abstract Result<ResponsePackage> Execute( string requestId, string actionName, string requestDataContent = null );

        /// <summary>非同步發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public abstract Task<Result<ResponsePackage>> ExecuteAsync( string requestId, string actionName, string requestDataContent = null );

        /// <summary>發佈事件訊息到遠端服務
        /// </summary>
        /// <param name="messageId">訊息識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="messageContent">訊息資料內容集合</param>
        /// <returns>發佈結果</returns>
        public virtual Result Publish( string messageId, string actionName, string messageContent = null ) => new Result() { Code = StatusCode.NOT_SUPPORT, Message = "This is kind of remote host type does not support 'Publish Message Package' process currently." };

        #endregion Public Abstract


        #region Method Implementation

        /// <summary>執行 RPC 遠端請求訊息呼叫
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容集合</param>
        /// <returns>執行結果</returns>
        public Result<TResponseDTO> Execute<TResponseDTO>( string actionName, object requestDataContent = null )
        {
            var result = Result.Create<TResponseDTO>();

            #region 請求參數資料驗證

            var v = Validator.Validate( requestDataContent );

            if ( !v.Success )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid client-side request. {v.Message}";
                Logger.WriteErrorLog( this, result.Message, nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            #endregion 請求參數資料驗證

            #region 新增 RPC 請求訊息

            if ( !AddRequest( out string requestId ) )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error. Create requestId occur error.";
                Logger.WriteErrorLog( this, $"Can not execute RPC request due to add RPC request fail. Create requestId occur error. RequestId: {requestId}.", nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            #endregion 新增 RPC 請求訊息

            #region 處理 RPC 請求參數資料內容

            string reqDataContent = null;

            try
            {
                if ( requestDataContent.IsNotNull() && ExecuteHandler.IsNull() )
                {
                    reqDataContent = System.Text.Json.JsonSerializer.Serialize( requestDataContent );
                }
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteExceptionLog( this, ex, $"Can not execute RPC request due to serialize request data content to JSON occur exception. RequestId: {requestId}. ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 處理 RPC 請求參數資料內容

            #region 執行 RPC 遠端動作

            Result<ResponsePackage> r;

            try
            {
                if ( ExecuteHandler.IsNull() ) 
                {
                    r = Execute( requestId, actionName, reqDataContent );
                }
                else 
                {
                    r = ExecuteHandler?.Invoke( requestId, actionName, requestDataContent );
                }
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.CLIENT_ERROR;
                result.Message         = $"Execute remote action occur error. RequestId: {requestId}. ActionName: {actionName}.";
                RemoveRequest( requestId );
                Logger.WriteExceptionLog( this, ex, $"Execute RPC remote service occur exception. RequestId: {requestId}. ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 執行 RPC 遠端動作

            #region 檢查遠端動作結果

            if ( !r.Success )
            {
                result.Code    = r.Code;
                result.Message = r.Message;
                RemoveRequest( requestId );
                Logger.WriteErrorLog( this, $"Execute RPC remote service fail. RequestId: {requestId}. ActionName: {actionName}.", nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            ResponsePackage responsePackage = r.Data;

            if ( responsePackage.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"No ResponsePackage retrive from remote service. RequestId: {requestId}. ActionName: {actionName}.";
                RemoveRequest( requestId );
                Logger.WriteErrorLog( this, result.Message, nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            ServiceClientActionLogger.Log( ServiceClientName, responsePackage );

            Check.IsFalse( responsePackage.Success, () => 
            {
                Logger.WriteErrorLog( this, $"Retrive fail status from remote service. RequestId: {requestId}. ActionName: {actionName}. Result RawData: {Environment.NewLine}{JsonConvertUtil.SerializeInCamelCase( r )}", nameof ( Execute ), "ServiceHostTrace" );
            } );

            #endregion 檢查遠端動作結果

            #region 處理 RPC 回應資料內容

            TResponseDTO responseDataContent = default ( TResponseDTO );
            string resDataContent = responsePackage.Data;

            try
            {
                if ( resDataContent.IsNotNullOrEmpty() )
                {
                    responseDataContent = System.Text.Json.JsonSerializer.Deserialize<TResponseDTO>( resDataContent );
                }
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.CLIENT_ERROR;
                result.Message         = $"Internal zayni framework service client error.";
                RemoveRequest( requestId );
                Logger.WriteExceptionLog( this, ex, $"Deserialize response data content from JSON occur exception. RequestId: {requestId}. ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 處理 RPC 回應資料內容

            #region 設定回傳值

            Check.IsFalse( RemoveRequest( requestId ), () => Logger.WriteErrorLog( this, $"Remove RPC request fail. RequestId: {requestId}. ActionName: {actionName}.", nameof ( Execute ), "ServiceHostTrace" ) );

            result.Data      = responseDataContent;
            result.Code      = responsePackage.Code;
            result.Message   = responsePackage.Message;
            result.Success   = responsePackage.Success;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>非同步作業執行 RPC 遠端請求訊息呼叫
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容集合</param>
        /// <returns>執行結果</returns>
        public async Task<Result<TResponseDTO>> ExecuteAsync<TResponseDTO>( string actionName, object requestDataContent = null ) 
        {
            var result = Result.Create<TResponseDTO>();

            #region 請求參數資料驗證

            var v = Validator.Validate( requestDataContent );

            if ( !v.Success )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid client-side request. {v.Message}";
                Logger.WriteErrorLog( this, result.Message, nameof ( ExecuteAsync ), "ServiceHostTrace" );
                return result;
            }

            #endregion 請求參數資料驗證

            #region 新增 RPC 請求訊息

            var a = await AddRequestAsync();

            if ( !a.Success || a.Data.IsNullOrEmpty() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error. Create requestId occur error.";
                Logger.WriteErrorLog( this, $"Can not execute RPC request due to add RPC request fail. Create requestId occur error. RequestId: {a.Data}.", nameof ( ExecuteAsync ), "ServiceHostTrace" );
                return result;
            }

            var requestId = a.Data;

            #endregion 新增 RPC 請求訊息

            #region 處理 RPC 請求參數資料內容

            string reqDataContent = null;

            try
            {
                if ( requestDataContent.IsNotNull() && ExecuteAsyncHandler.IsNull() )
                {
                    reqDataContent = JsonSerializer.Serialize( requestDataContent );
                }
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteExceptionLog( this, ex, $"Can not execute RPC request due to serialize request data content to JSON occur exception. RequestId: {requestId}. ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 處理 RPC 請求參數資料內容

            #region 執行 RPC 遠端動作

            Result<ResponsePackage> r;

            try
            {
                if ( ExecuteAsyncHandler.IsNull() ) 
                {
                    r = await ExecuteAsync( requestId, actionName, reqDataContent );
                }
                else 
                {
                    r = await ExecuteAsyncHandler?.Invoke( requestId, actionName, requestDataContent );
                }
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.CLIENT_ERROR;
                result.Message         = $"Execute remote action occur error. RequestId: {requestId}. ActionName: {actionName}.";
                await RemoveRequestAsync( requestId );
                Logger.WriteExceptionLog( this, ex, $"Execute RPC remote service occur exception. RequestId: {requestId}. ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 執行 RPC 遠端動作

            #region 檢查遠端動作結果

            if ( !r.Success )
            {
                result.Code    = r.Code;
                result.Message = r.Message;
                await RemoveRequestAsync( requestId );
                Logger.WriteErrorLog( this, $"Execute RPC remote service fail. RequestId: {requestId}. ActionName: {actionName}.", nameof ( ExecuteAsync ), "ServiceHostTrace" );
                return result;
            }

            ResponsePackage responsePackage = r.Data;

            if ( responsePackage.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"No ResponsePackage retrive from remote service. RequestId: {requestId}. ActionName: {actionName}.";
                await RemoveRequestAsync( requestId );
                Logger.WriteErrorLog( this, result.Message, nameof ( ExecuteAsync ), "ServiceHostTrace" );
                return result;
            }

            ServiceClientActionLogger.Log( ServiceClientName, responsePackage );

            Check.IsFalse( responsePackage.Success, () => 
            {
                Logger.WriteErrorLog( this, $"Retrive fail status from remote service. RequestId: {requestId}. ActionName: {actionName}. Result RawData: {Environment.NewLine}{JsonConvertUtil.SerializeInCamelCase( r )}", nameof ( ExecuteAsync ), "ServiceHostTrace" );
            } );

            #endregion 檢查遠端動作結果

            #region 處理 RPC 回應資料內容

            TResponseDTO responseDataContent = default ( TResponseDTO );
            string resDataContent = responsePackage.Data;

            try
            {
                if ( resDataContent.IsNotNullOrEmpty() )
                {
                    responseDataContent = JsonSerializer.Deserialize<TResponseDTO>( resDataContent );
                }
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.CLIENT_ERROR;
                result.Message         = $"Internal zayni framework service client error.";
                await RemoveRequestAsync( requestId );
                Logger.WriteExceptionLog( this, ex, $"Deserialize response data content from JSON occur exception. RequestId: {requestId}. ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 處理 RPC 回應資料內容

            #region 設定回傳值

            Check.IsFalse( await RemoveRequestAsync( requestId ), () => Logger.WriteErrorLog( this, $"Remove RPC request fail. RequestId: {requestId}. ActionName: {actionName}.", nameof ( ExecuteAsync ), "ServiceHostTrace" ) );

            result.Data      = responseDataContent;
            result.Code      = responsePackage.Code;
            result.Message   = responsePackage.Message;
            result.Success   = responsePackage.Success;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>發佈事件訊息至遠端服務
        /// </summary>
        /// <param name="action">動作名稱</param>
        /// <param name="messageContent">訊息資料內容集合</param>
        /// <returns>發佈結果</returns>
        public Result Publish( string action, object messageContent = null )
        {
            #region 初始化回傳值

            dynamic result   = Result.CreateDynamicResult();
            result.MessageId = null;

            #endregion 初始化回傳值

            #region 請求參數資料驗證

            var v = Validator.Validate( messageContent );

            if ( !v.Success )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid client-side request. {v.Message}";
                Logger.WriteErrorLog( this, result.Message, nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            #endregion 請求參數資料驗證

            #region 新增請求訊息

            if ( !AddRequest( out string messageId ) )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteErrorLog( this, $"Can not publish message package due to create messageId fail. MessageId: {messageId}.", nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            #endregion 新增請求訊息

            #region 序列化訊息包裹

            string dataContent = null;

            try
            {
                if ( messageContent.IsNotNull() )
                {
                    dataContent = JsonSerializer.Serialize( messageContent );
                }
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error. Serialize to JSON occur exception.";
                Logger.WriteExceptionLog( this, ex, $"Can not publish message package due to serialize request data content to JSON occur exception. MessageId: {messageId}. ActionName: {action}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 序列化訊息包裹

            #region 發佈事件訊息

            Result r;

            try
            {
                r = Publish( messageId, action, dataContent );
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.CLIENT_ERROR;
                result.Message         = $"Publish message package occur error. MessageId: {messageId}. ActionName: {action}.";
                RemoveRequest( messageId );
                Logger.WriteExceptionLog( this, ex, $"Publish message package to remote service occur exception. MessageId: {messageId}. ActionName: {action}.", "ServiceHostTrace" );
                return result;
            }

            Check.IsFalse( RemoveRequest( messageId ), () => Logger.WriteErrorLog( this, $"Remove MessageId of MessagePackage from message pool error. MessageId: {messageId}, ActionName: {action}.", nameof ( Publish ), "ServiceHostTrace" ) );

            #endregion 發佈事件訊息

            #region 設定回傳值

            result.MessageId = messageId;
            result.Code      = r.Code;
            result.Message   = r.Message;
            result.Success   = true;
            result.IsSuccess = true;

            #endregion 設定回傳值

            return result;
        }

        #endregion Method Implementation


        #region Private Method

        /// <summary>建立 RPC 請求識別代碼，長度為 15 碼。
        /// </summary>
        /// <returns>RPC 請求識別代碼</returns>
        private string CreateRequestId()
        {
            string requestId = RandomTextHelper.Create( 15 );

            if ( _requests.Contains( requestId ) )
            {
                requestId = CreateRequestId();
            }

            return requestId;
        }

        /// <summary>新增 RPC 請求識別代碼
        /// </summary>
        /// <param name="requestId">RPC 請求識別代碼</param>
        /// <returns>新增結果</returns>
        private bool AddRequest( out string requestId )
        {
            requestId = null;

            try
            {
                requestId = CreateRequestId();

                using ( _asyncLock.Lock() )
                {
                    _requests.Add( requestId );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Add request occur exception.", "ServiceHostTrace" );
                return false;
            }

            return true;
        }

        /// <summary>新增 RPC 請求識別代碼
        /// </summary>
        /// <returns>新增結果</returns>
        private async Task<Result<string>> AddRequestAsync()
        {
            try
            {
                string requestId = CreateRequestId();

                using ( await _asyncLock.LockAsync() )
                {
                    _requests.Add( requestId );
                }

                return Result.Create<string>( true, data: requestId );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Add request async occur exception.", "ServiceHostTrace" );
                return Result.Create<string>();
            }
        }

        /// <summary>移除 RPC 請求識別代碼
        /// </summary>
        /// <param name="requestId">RPC 請求識別代碼</param>
        /// <returns>移除結果</returns>
        private bool RemoveRequest( string requestId )
        {
            try
            {
                using ( _asyncLock.Lock() )
                {
                    _requests.Remove( requestId );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Remove request occur exception.", "ServiceHostTrace" );
                return false;
            }

            return true;
        }

        /// <summary>移除 RPC 請求識別代碼
        /// </summary>
        /// <param name="requestId">RPC 請求識別代碼</param>
        /// <returns>移除結果</returns>
        private async Task<bool> RemoveRequestAsync( string requestId )
        {
            try
            {
                using ( await _asyncLock.LockAsync() )
                {
                    _requests.Remove( requestId );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Remove request async occur exception.", "ServiceHostTrace" );
                return false;
            }

            return true;
        }

        #endregion Private Method
    }
}
