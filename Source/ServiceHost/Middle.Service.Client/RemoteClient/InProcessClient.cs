using NeoSmart.AsyncLock;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Serialization;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>InProcess Middle Serive Host 的服務客戶端
    /// </summary>
    public class InProcessClient : RemoteClient
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>服務客戶端個體的設定組態
        /// </summary>
        private ServiceClientConfig _serviceClientConfig;

        /// <summary>遠端服務 (在此情境下遠端服務即為 In Process 的程式內部的服務)
        /// </summary>
        private RemoteService _service;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        public InProcessClient( string serviceClientName ) : base( serviceClientName )
        {
        }

        #endregion 宣告建構子


        #region 宣告公開的方法

        /// <summary>初始化遠端服務連線
        /// </summary>
        /// <returns>初始化是否正常</returns>
        public override bool Initialize()
        {
            using ( _asyncLock.Lock() )
            {
                try
                {
                    _serviceClientConfig = ClientConfigReader.GetServiceClientConfig( ServiceClientName );
                    _service             = RemoteServiceContainer.Get( _serviceClientConfig.RemoteServiceName );

                    if ( _service.IsNull() )
                    {
                        return false;
                    }
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( this, ex, $"Initialize {nameof ( InProcessClient )} occur exception. ServiceClientName: {ServiceClientName}", "ServiceHostTrace" );
                    return false;
                }

                return true;
            }
        }

        // Pony Says: 此方法在 runtime 不應該被呼叫到，算是一個在 InProcess Invoke 的情境下特殊的情況。
        /// <summary>呼叫 InProcess 的 Middle Service Host 服務動作
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Result<ResponsePackage> Execute( string requestId, string actionName, string requestDataContent = null ) => 
            Result.Create<ResponsePackage>( false, message: "The method is obsolete." );

        // Pony Says: 此方法在 runtime 不應該被呼叫到，算是一個在 InProcess Invoke 的情境下特殊的情況。
        /// <summary>呼叫 InProcess 的 Middle Service Host 服務動作
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        public override Task<Result<ResponsePackage>> ExecuteAsync( string requestId, string actionName, string requestDataContent = null ) => 
            Task.FromResult( Result.Create<ResponsePackage>( false, message: "The method is obsolete." ) );

        #endregion 宣告公開的方法


        #region 宣告內部的方法

        /// <summary>內部程序的 ServiceAction Invoke 呼叫
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="reqDTO">請求資料載體</param>
        /// <typeparam name="TResDTO">回應資料載體的泛型</typeparam>
        /// <returns>服務動作呼叫結果</returns>
        internal Result<TResDTO> Invoke<TResDTO>( string actionName, object reqDTO ) 
        {
            var result = Result.Create<TResDTO>();

            #region 請求參數資料驗證

            var v = Validator.Validate( reqDTO );

            if ( !v.Success )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid client-side request. {v.Message}";
                Logger.WriteErrorLog( this, result.Message, nameof ( Invoke ), "ServiceHostTrace" );
                return result;
            }

            #endregion 請求參數資料驗證

            #region 執行服務動作呼叫

            string requestId = RandomTextHelper.Create( 15 );

            var requestPackage = new RequestPackage() 
            {
                PackageType = PackageType.RPCRequest,
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId   = requestId,
                ActionName  = actionName,
                RequestTime = DateTime.UtcNow
            };

            ServiceClientActionLogger.Log( ServiceClientName, requestPackage, reqDTO );
            var ( responsePackage, resData ) = _service.ExecuteServiceAction( requestPackage, reqDTO );

            #endregion 執行服務動作呼叫

            #region 檢查遠端動作結果

            if ( responsePackage.IsNull() || !responsePackage.Success )
            {
                result.Code    = responsePackage.Code;
                result.Message = responsePackage.Message;
                Logger.WriteErrorLog( this, $"Execute in-process service call fail. RequestId: {requestId}. ActionName: {actionName}.", nameof ( Invoke ), "ServiceHostTrace" );
                return result;
            }

            ServiceClientActionLogger.Log( ServiceClientName, responsePackage, resData );

            Check.IsFalse( responsePackage.Success, () => 
            {
                Logger.WriteErrorLog( this, $"Retrive fail status from remote service. RequestId: {requestId}. ActionName: {actionName}. Result RawData: {Environment.NewLine}{JsonConvertUtil.SerializeInCamelCase( responsePackage )}", nameof ( Execute ), "ServiceHostTrace" );
            } );

            try
            {
                if ( !( resData is Result<TResDTO> ) ) 
                {
                    result.Code    = StatusCode.CLIENT_ERROR;
                    result.Message = $"Can not convert response DTO type. RequestId: {requestId}. ActionName: {actionName}.";
                    Logger.WriteErrorLog( this, result.Message, nameof ( Invoke ), "ServiceHostTrace" );
                    return result;
                }

                result = (Result<TResDTO>)resData;
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.CLIENT_ERROR;
                result.Message         = $"Convert to ResDTO occur exception. RequestId: {requestId}. ActionName: {actionName}.";
                Logger.WriteExceptionLog( this, ex, nameof ( Invoke ), result.Message, "ServiceHostTrace" );
                return result;
            }

            #endregion 檢查遠端動作結果

            return result;
        }

        /// <summary>內部程序的 ServiceAction Invoke 呼叫
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="reqDTO">請求資料載體</param>
        /// <typeparam name="TResDTO">回應資料載體的泛型</typeparam>
        /// <returns>服務動作呼叫結果</returns>
        internal async Task<Result<TResDTO>> InvokeAsync<TResDTO>( string actionName, object reqDTO ) 
        {
            var result = Result.Create<TResDTO>();

            #region 請求參數資料驗證

            var v = Validator.Validate( reqDTO );

            if ( !v.Success )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid client-side request. {v.Message}";
                Logger.WriteErrorLog( this, result.Message, nameof ( Invoke ), "ServiceHostTrace" );
                return result;
            }

            #endregion 請求參數資料驗證

            #region 執行服務動作呼叫

            string requestId = RandomTextHelper.Create( 15 );

            var requestPackage = new RequestPackage() 
            {
                PackageType = PackageType.RPCRequest,
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId   = requestId,
                ActionName  = actionName,
                RequestTime = DateTime.UtcNow
            };

            ServiceClientActionLogger.Log( ServiceClientName, requestPackage, reqDTO );
            var ( responsePackage, resData ) = await _service.ExecuteServiceActionAsync( requestPackage, reqDTO );

            #endregion 執行服務動作呼叫

            #region 檢查遠端動作結果

            if ( responsePackage.IsNull() || !responsePackage.Success )
            {
                result.Code    = responsePackage.Code;
                result.Message = responsePackage.Message;
                Logger.WriteErrorLog( this, $"Execute in-process service call fail. RequestId: {requestId}. ActionName: {actionName}.", nameof ( Invoke ), "ServiceHostTrace" );
                return result;
            }

            ServiceClientActionLogger.Log( ServiceClientName, responsePackage, resData );

            Check.IsFalse( responsePackage.Success, () => 
            {
                Logger.WriteErrorLog( this, $"Retrive fail status from remote service. RequestId: {requestId}. ActionName: {actionName}. Result RawData: {Environment.NewLine}{JsonConvertUtil.SerializeInCamelCase( responsePackage )}", nameof ( Execute ), "ServiceHostTrace" );
            } );

            try
            {
                if ( !( resData is Result<TResDTO> ) ) 
                {
                    result.Code    = StatusCode.CLIENT_ERROR;
                    result.Message = $"Can not convert response DTO type. RequestId: {requestId}. ActionName: {actionName}.";
                    Logger.WriteErrorLog( this, result.Message, nameof ( Invoke ), "ServiceHostTrace" );
                    return result;
                }

                result = (Result<TResDTO>)resData;
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.CLIENT_ERROR;
                result.Message         = $"Convert to ResDTO occur exception. RequestId: {requestId}. ActionName: {actionName}.";
                Logger.WriteExceptionLog( this, ex, nameof ( Invoke ), result.Message, "ServiceHostTrace" );
                return result;
            }

            #endregion 檢查遠端動作結果

            return result;
        }

        /// <summary>內部程序的 ServiceAction Invoke 呼叫
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="reqDTO">請求資料載體</param>
        /// <returns>服務動作呼叫結果</returns>
        internal Result Invoke( string actionName, object reqDTO ) 
        {
            var result = Result.Create();

            #region 請求參數資料驗證

            var v = Validator.Validate( reqDTO );

            if ( !v.Success )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid client-side request. {v.Message}";
                Logger.WriteErrorLog( this, result.Message, nameof ( Invoke ), "ServiceHostTrace" );
                return result;
            }

            #endregion 請求參數資料驗證

            #region 執行服務動作呼叫

            string requestId = RandomTextHelper.Create( 15 );

            var requestPackage = new RequestPackage() 
            {
                PackageType = PackageType.RPCRequest,
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId   = requestId,
                ActionName  = actionName,
                RequestTime = DateTime.UtcNow
            };

            ServiceClientActionLogger.Log( ServiceClientName, requestPackage, reqDTO );
            var ( responsePackage, resData ) = _service.ExecuteServiceAction( requestPackage, reqDTO );

            #endregion 執行服務動作呼叫

            #region 檢查遠端動作結果

            if ( responsePackage.IsNull() || !responsePackage.Success )
            {
                result.Code    = responsePackage.Code;
                result.Message = responsePackage.Message;
                Logger.WriteErrorLog( this, $"Execute in-process service call fail. RequestId: {requestId}. ActionName: {actionName}.", nameof ( Invoke ), "ServiceHostTrace" );
                return result;
            }

            ServiceClientActionLogger.Log( ServiceClientName, responsePackage, resData );

            Check.IsFalse( responsePackage.Success, () => 
            {
                Logger.WriteErrorLog( this, $"Retrive fail status from remote service. RequestId: {requestId}. ActionName: {actionName}. Result RawData: {Environment.NewLine}{JsonConvertUtil.SerializeInCamelCase( responsePackage )}", nameof ( Execute ), "ServiceHostTrace" );
            } );

            try
            {
                if ( !( resData is Result ) ) 
                {
                    result.Code    = StatusCode.CLIENT_ERROR;
                    result.Message = $"Can not convert response DTO type. RequestId: {requestId}. ActionName: {actionName}.";
                    Logger.WriteErrorLog( this, result.Message, nameof ( Invoke ), "ServiceHostTrace" );
                    return result;
                }

                result = (Result)resData;
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.CLIENT_ERROR;
                result.Message         = $"Convert to ResDTO occur exception. RequestId: {requestId}. ActionName: {actionName}.";
                Logger.WriteExceptionLog( this, ex, nameof ( Invoke ), result.Message, "ServiceHostTrace" );
                return result;
            }

            #endregion 檢查遠端動作結果

            return result;
        }

        /// <summary>內部程序的 ServiceAction Invoke 呼叫
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="reqDTO">請求資料載體</param>
        /// <returns>服務動作呼叫結果</returns>
        internal async Task<Result> InvokeAsync( string actionName, object reqDTO ) 
        {
            var result = Result.Create();

            #region 請求參數資料驗證

            var v = Validator.Validate( reqDTO );

            if ( !v.Success )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid client-side request. {v.Message}";
                Logger.WriteErrorLog( this, result.Message, nameof ( Invoke ), "ServiceHostTrace" );
                return result;
            }

            #endregion 請求參數資料驗證

            #region 執行服務動作呼叫

            string requestId = RandomTextHelper.Create( 15 );

            var requestPackage = new RequestPackage() 
            {
                PackageType = PackageType.RPCRequest,
                ServiceName = _serviceClientConfig.RemoteServiceName,
                RequestId   = requestId,
                ActionName  = actionName,
                RequestTime = DateTime.UtcNow
            };

            ServiceClientActionLogger.Log( ServiceClientName, requestPackage, reqDTO );
            var ( responsePackage, resData ) = await _service.ExecuteServiceActionAsync( requestPackage, reqDTO );

            #endregion 執行服務動作呼叫

            #region 檢查遠端動作結果

            if ( responsePackage.IsNull() || !responsePackage.Success )
            {
                result.Code    = responsePackage.Code;
                result.Message = responsePackage.Message;
                Logger.WriteErrorLog( this, $"Execute in-process service call fail. RequestId: {requestId}. ActionName: {actionName}.", nameof ( Invoke ), "ServiceHostTrace" );
                return result;
            }

            ServiceClientActionLogger.Log( ServiceClientName, responsePackage, resData );

            Check.IsFalse( responsePackage.Success, () => 
            {
                Logger.WriteErrorLog( this, $"Retrive fail status from remote service. RequestId: {requestId}. ActionName: {actionName}. Result RawData: {Environment.NewLine}{JsonConvertUtil.SerializeInCamelCase( responsePackage )}", nameof ( Execute ), "ServiceHostTrace" );
            } );

            try
            {
                if ( !( resData is Result ) ) 
                {
                    result.Code    = StatusCode.CLIENT_ERROR;
                    result.Message = $"Can not convert response DTO type. RequestId: {requestId}. ActionName: {actionName}.";
                    Logger.WriteErrorLog( this, result.Message, nameof ( Invoke ), "ServiceHostTrace" );
                    return result;
                }

                result = (Result)resData;
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.CLIENT_ERROR;
                result.Message         = $"Convert to ResDTO occur exception. RequestId: {requestId}. ActionName: {actionName}.";
                Logger.WriteExceptionLog( this, ex, nameof ( Invoke ), result.Message, "ServiceHostTrace" );
                return result;
            }

            #endregion 檢查遠端動作結果

            return result;
        }

        #endregion 宣告內部的方法
    }
}