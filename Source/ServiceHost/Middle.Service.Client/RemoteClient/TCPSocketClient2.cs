﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ZayniFramework.Serialization;
using ZeroFormatter;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>TCP Socket 客戶端類別
    /// </summary>
    internal sealed class TCPSocketClient2 : RemoteClient
    {
        #region 宣告私有的欄位

        /// <summary>多執行緒鎖定物件
        /// </summary>
        private readonly object _lockThiz = new object();

        /// <summary>多執行緒鎖定物件
        /// </summary>
        private readonly object _lockMsgBuffers = new object();

        /// <summary>等待 TCP Socket 服務 RPC 回應訊息的逾時秒數
        /// </summary>
        private int _tcpReponseTimeout;

        /// <summary>TCP Socket 訊息的結束標記字串
        /// </summary>
        private string _tcpMsgEndingToken;

        /// <summary>TCP Socket 網路連線客戶端物件
        /// </summary>
        private TcpClient _tcpClient = null;

        /// <summary>TCP Socket 網路串流物件
        /// </summary>
        private NetworkStream _networkStream = null;

        /// <summary>TCP Socket 訊息的緩存 StringBuilder
        /// </summary>
        /// <remarks>注意，這個緩存的型別不要使用 String，因為字串 Contract 的效能遠遠比 StringBuilder 還慢很多...</remarks>
        private StringBuilder _msgBuffers = new StringBuilder( "" );

        /// <summary>RPC 請求管理容器
        /// </summary>
        private RPCRequestContainer _requestContainer = new RPCRequestContainer();

        #endregion 宣告私有的欄位


        #region 宣告私有屬性

        /// <summary>TCP Socket 訊息緩存
        /// </summary>
        /// <remarks>注意，這個緩存的型別不要使用 String，因為字串 Contract 的效能遠遠比 StringBuilder 還慢很多...</remarks>
        private StringBuilder MsgBuffers
        {
            get
            {
                lock ( _lockMsgBuffers )
                {
                    return _msgBuffers;
                }
            }
            set
            {
                lock ( _lockMsgBuffers )
                {
                    _msgBuffers = value;
                }
            }
        }

        #endregion 宣告私有屬性


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        internal TCPSocketClient2( string serviceClientName ) : base( serviceClientName )
        {
            // pass
        }

        #endregion 宣告建構子


        #region 實作抽象

        /// <summary>初始化遠端服務連線
        /// </summary>
        /// <returns>連線是否正常建立</returns>
        internal override bool Initialize()
        {
            TCPSocketClientConfig tcpConfig = null;

            try
            {
                var svrConfig = ConfigReader.GetServiceClientConfig( ServiceClientName );
                tcpConfig     = ConfigReader.GetTCPSocketClientConfig( svrConfig );

                _tcpClient = new TcpClient();
                _tcpClient.Client.Connect( tcpConfig.TCPServerHost, tcpConfig.TCPServerPort );

                _networkStream     = _tcpClient.GetStream();
                _tcpReponseTimeout = tcpConfig.TCPResponseTimeout <= 0 ? 60 : tcpConfig.TCPResponseTimeout;
                _tcpMsgEndingToken = tcpConfig.TCPMsgEndToken.IsNullOrEmptyString( "<EOF>" );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "Establish TCP socket connection occur exception.", "ServiceHostTrace" );
                return false;
            }

            try
            {
                Read( _networkStream );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, "Establish TCP socket connection occur exception.", "ServiceHostTrace" );
                return false;
            }

            Logger.WriteInformationLog( this, $"Establish TCP socket connection to remote host successfully. RemoteHost: {tcpConfig.TCPServerHost}:{tcpConfig.TCPServerPort}.", nameof ( Initialize ), "ServiceHostTrace" );
            Console.WriteLine( $"Establish TCP socket connection to remote host successfully. RemoteHost: {tcpConfig.TCPServerHost}:{tcpConfig.TCPServerPort}." );
            return true;
        }

        /// <summary>發送 RPC 遠端請求訊息
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容 JSON 集合</param>
        /// <returns>執行結果</returns>
        internal override Result<ResponsePackage> Execute( string requestId, string actionName, string requestDataContent = null )
        {
            #region 初始化回傳值

            var result = new Result<ResponsePackage>()
            {
                IsSuccess = false,
                Data      = null
            };

            #endregion 初始化回傳值

            #region 設定請求包裹

            var requestPackage = new RequestPackage()
            {
                RequestId   = requestId,
                ActionName  = actionName,
                Data        = requestDataContent,
                RequestTime = DateTime.Now
            };

            #endregion 設定請求包裹

            #region 執行遠端服務動作

            ResponsePackage responsePackage;

            try
            {
                responsePackage = Send( requestPackage );
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteExceptionLog( this, ex, $"Execute remote action occur exception. RequestId: {requestId}, ActionName: {actionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 執行遠端服務動作

            #region 檢查回應包裹

            if ( responsePackage.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Internal zayni framework service client error.";
                Logger.WriteErrorLog( this, $"No response package from remote service. RequestId: {requestId}, ActionName: {actionName}.", nameof ( Execute ), "ServiceHostTrace" );
                return result;
            }

            #endregion 檢查回應包裹

            #region 設定回傳值

            result.Data      = responsePackage;
            result.IsSuccess = responsePackage.IsSuccess;

            #endregion 設定回傳值

            return result;
        }

        #endregion 實作抽象


        #region 宣告私有的方法

        /// <summary>讀取遠端服務的 TCP Socket 網路串流訊息
        /// </summary>
        /// <param name="networkStream">TCP Socket 網路串流</param>
        private void Read( NetworkStream networkStream )
        {
            if ( new object[] { _tcpClient, networkStream }.HasNullElements() )
            {
                Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message fail due to TCP socket network stream is null.", nameof ( Read ), "ServiceHostTrace" );
                return;
            }

            var thread = new Thread( () => 
            {
                while ( true )
                {
                    Thread.Sleep( 1 );

                    try
                    {
                        if ( !networkStream.CanRead )
                        {
                            continue;
                            //Logger.WriteWarningLog( this, $"Read ResponsePackage from TCP socket message fail due to TCP socket network stream is not readable.", nameof ( Read ), "ServiceHostTrace" );
                            //Read( networkStream );
                            //return;
                        }

                        if ( !networkStream.DataAvailable )
                        {
                            continue;
                            //Logger.WriteWarningLog( this, $"Read ResponsePackage from TCP socket message fail due to data from TCP socket network stream is unavailable.", nameof ( Read ), "ServiceHostTrace" );
                            //Read( networkStream );
                            //return;
                        }

                        byte[] receiveBytes = new byte[ _tcpClient.ReceiveBufferSize ];
                        networkStream.Read( receiveBytes, 0, (int)_tcpClient.ReceiveBufferSize );

                        string data = Encoding.UTF8.GetString( receiveBytes )?.Replace( "\0", string.Empty );
                        MsgBuffers.Append( data );

                        if ( !data.Contains( _tcpMsgEndingToken ) )
                        {
                            continue;
                            //Read( networkStream );
                            //return;
                        }

                        string json = MsgBuffers.ToString().RemoveLastAppeared( _tcpMsgEndingToken );

                        if ( json.IsNullOrEmpty() )
                        {
                            Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message fail due to invalid socket message. Receive TCP socket message:{Environment.NewLine}{MsgBuffers.ToString()}", nameof ( Read ), "ServiceHostTrace" );
                            MsgBuffers = new StringBuilder( "" );
                            continue;
                            //Read( networkStream );
                            //return;
                        }

                        MsgBuffers = new StringBuilder( "" );
                        ResponsePackage responsePackage = null;

                        try
                        {
                            responsePackage = JsonConvert.DeserializeObject<ResponsePackage>( json );
                        }
                        catch ( Exception ex )
                        {
                            Logger.WriteExceptionLog( this, ex, $"Read ResponsePackage from TCP socket message occur exception due to deserialize to ResponsePackage fail. Receive TCP socket message:{Environment.NewLine}{MsgBuffers.ToString()}", "ServiceHostTrace" );
                            continue;
                            //Read( networkStream );
                            //return;
                        }

                        if ( responsePackage.IsNull() )
                        {
                            Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message fail due to retrive null value of ResponsePackage.", nameof ( Read ), "ServiceHostTrace" );
                            continue;
                            //Read( networkStream );
                            //return;
                        }

                        string requestId = responsePackage.RequestId;

                        if ( requestId.IsNullOrEmpty() )
                        {
                            Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message fail due to invalid ResponsePackage. RequestId is null or empty string. Receive ResponsePackage:{Environment.NewLine}{JsonConvertUtil.Serialize( responsePackage )}", nameof ( Read ), "ServiceHostTrace" );
                            Read( networkStream );
                            return;
                        }

                        if ( !_requestContainer.Contains( requestId ) )
                        {
                            Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message fail due to invalid ResponsePackage. Can not found RequestId: {requestId}.", nameof ( Read ), "ServiceHostTrace" );
                            continue;
                            //Read( networkStream );
                            //return;
                        }
                    
                        if ( !_requestContainer.Get( requestId, out RPCRequest rpcRequest ) )
                        {
                            Logger.WriteErrorLog( this, $"Get RPC request from container fail. Retrive ResponsePackage is:{Environment.NewLine}{JsonConvertUtil.Serialize( responsePackage )}", nameof ( Read ), "ServiceHostTrace" );
                            continue;
                            //Read( networkStream );
                            //return;
                        }

                        if ( rpcRequest.IsNull() )
                        {
                            Logger.WriteErrorLog( this, $"Read ResponsePackage from TCP socket message occur exception due get TcpRequestPackage from request pool fail. Receive null TcpRequestPackage. Retrive ResponsePackage is:{Environment.NewLine}{JsonConvertUtil.Serialize( responsePackage )}", nameof ( Read ), "ServiceHostTrace" );
                            continue;
                            //Read( networkStream );
                            //return;
                        }

                        lock ( _lockThiz )
                        {
                            rpcRequest.Result = responsePackage;
                            rpcRequest.ReceiveEvent?.Set();
                        }
                    }
                    catch ( Exception ex )
                    {
                        Logger.WriteExceptionLog( this, ex, $"Read ResponsePackage message from TCP socket network stream occur exception.", "ServiceHostTrace" );
                        continue;
                    }
                }
            } );

            thread.Start();
        }

        /// <summary>發送請求包裹的 TCP Socket 網路串流訊息
        /// </summary>
        /// <param name="requestPackage">遠端服務請求包裹</param>
        private ResponsePackage Send( RequestPackage requestPackage )
        {
            if ( new object[] { _tcpClient, _networkStream }.HasNullElements() )
            {
                Logger.WriteErrorLog( this, $"Read RequestPackage to TCP socket message fail due to TCP socket network stream is null.", nameof ( Send ), "ServiceHostTrace" );
                return null;
            }

            //if ( !IsConnectedWhenSend() )
            //{
            //    return null;
            //}

            RPCRequest request;

            try
            {
                request = requestPackage.ConvertTo<RPCRequest>();
                request.ReceiveEvent = new AutoResetEvent( false );

                if ( !_requestContainer.Add( requestPackage.RequestId, request ) )
                {
                    Logger.WriteErrorLog( this, $"Add RPC request to container fail. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Execute ), "ServiceHostTrace" );
                    return null;
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Send RequestPackage to from TCP socket network stream occur exception due to add TcpRequestPackage to request pool occur exception.", "ServiceHostTrace" );
                return null;
            }

            byte[] messageBytes = null;

            try
            {
                //string json  = JsonConvert.SerializeObject( requestPackage ) + _tcpMsgEndingToken;
                //messageBytes = Encoding.UTF8.GetBytes( json );

                var reqBytes   = ZeroFormatterSerializer.Serialize( requestPackage );
                var tokenBytes = ZeroFormatterSerializer.Serialize( "[$]" );
                messageBytes   = reqBytes.Combine( tokenBytes );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Send RequestPackage to TCP socket network stream occur exception due to serialize RequestPackage object occur exception.", "ServiceHostTrace" );
                return null;
            }

            try
            {
                // 第一種寫法:
                _tcpClient.Client.Send( messageBytes );

                // 第二種寫法:
                //_networkStream.Write( data, 0, data.Length );
                //_networkStream.Flush();
            }
            catch ( Exception ex )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteExceptionLog( this, ex, $"Send RequestPackage to TCP socket network stream occur exception. Write TCP socket message to network stream occur exception.", "ServiceHostTrace" );
                return null;
            }

            request.ReceiveEvent?.Reset();
            bool isTimeout = !request.ReceiveEvent.WaitOne( TimeSpan.FromSeconds( _tcpReponseTimeout ) );

            if ( isTimeout )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteErrorLog( this, $"Waiting for remote host reply ResponsePackage TCP socket timeout. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Send ), "ServiceHostTrace" );
                return null;
            }

            if ( !_requestContainer.Get( request.RequestId, out RPCRequest rpcRequest ) )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteErrorLog( this, $"Get RPC request from container fail. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Execute ), "ServiceHostTrace" );
                return null;
            }

            if ( rpcRequest.IsNull() )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteErrorLog( this, $"TcpRequestPackage from request pool is null. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Send ), "ServiceHostTrace" );
                return null;
            }

            ResponsePackage responsePackage = rpcRequest.Result;

            if ( responsePackage.IsNull() )
            {
                _requestContainer.Remove( request.RequestId );
                Logger.WriteErrorLog( this, $"Retrive ResponsePackage object is null from remote host. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", nameof ( Send ), "ServiceHostTrace" );
                return null;
            }

            _requestContainer.Remove( request.RequestId );
            return responsePackage;
        }

        #endregion 宣告私有的方法
    }
}
