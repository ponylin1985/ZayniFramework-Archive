using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>服務客戶端介面
    /// </summary>
    public interface IRemoteClientAsync
    {
        #region Public Methods

        /// <summary>執行 RPC 遠端請求訊息呼叫
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容集合</param>
        /// <returns>執行結果</returns>
        Task<Result<TResponseDTO>> ExecuteAsync<TResponseDTO>( string actionName, object requestDataContent = null );

        #endregion Public Methods
    }
}
