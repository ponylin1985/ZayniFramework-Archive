﻿using ZayniFramework.Common;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>服務客戶端介面
    /// </summary>
    public interface IRemoteClient
    {
        #region Public Properties

        /// <summary>遠端服務的連線中斷的處理委派
        /// </summary>
        /// <value></value>
        ConnectionBrokenHandler ConnectionBroken { get; set; }

        /// <summary>遠端服務的連線重新恢復的處理委派
        /// </summary>
        /// <value></value>
        ConnectionRestoreHandler ConnectionRestored { get; set; }

        #endregion Public Properties


        #region Public Methods

        /// <summary>執行 RPC 遠端請求訊息呼叫
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <param name="requestDataContent">請求資料內容集合</param>
        /// <returns>執行結果</returns>
        Result<TResponseDTO> Execute<TResponseDTO>( string actionName, object requestDataContent = null );

        /// <summary>發佈事件訊息到遠端服務
        /// </summary>
        /// <param name="action">動作名稱</param>
        /// <param name="messageContent">訊息資料內容集合</param>
        /// <returns>發佈結果</returns>
        Result Publish( string action, object messageContent = null );

        #endregion Public Methods
    }
}
