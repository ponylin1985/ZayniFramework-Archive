﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using ZayniFramework.Logging;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>RPC 遠端請求的管理容器
    /// </summary>
    internal sealed class RPCRequestContainer
    {
        #region 宣告私以的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>RPC 請求集合池<para/>
        /// Key值: RPC 請求識別代碼<para/>
        /// Value值: RPC 遠端呼叫請求物件
        /// </summary>
        private readonly Dictionary<string, RPCRequest> _requests = new Dictionary<string, RPCRequest>();

        #endregion 宣告私以的欄位


        #region 宣告內部的方法

        /// <summary>檢查是否存在指定請求識別代碼的 RPC 請求
        /// </summary>
        /// <param name="requsetId">請求識別代碼</param>
        /// <returns>是否存在指定請求識別代碼的 RPC 請求</returns>
        internal bool Contains( string requsetId ) => _requests.ContainsKey( requsetId );

        /// <summary>取得指定請求識別代碼的 RPC 請求
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="request">RPC 遠端請求物件</param>
        /// <returns>是否重功取得 RPC 遠端請求物件</returns>
        internal bool Get( string requestId, out RPCRequest request ) => _requests.TryGetValue( requestId, out request );

        /// <summary>新增 RPC 遠端請求到管理容器
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <param name="request">RPC 遠端請求物件</param>
        /// <returns>新增是否成功</returns>
        internal bool Add( string requestId, RPCRequest request )
        {
            using ( _asyncLock.Lock() )
            {
                if ( Contains( requestId ) )
                {
                    return false;
                }

                try
                {
                    _requests.Add( requestId, request );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( this, ex, $"Add RPC request to container occur exception. RequestId: {requestId}.", "ServiceHostTrace" );
                    return false;
                }

                return true;
            }
        }

        /// <summary>移除 RPC 遠端請求到管理容器
        /// </summary>
        /// <param name="requestId">請求識別代碼</param>
        /// <returns>移除是否成功</returns>
        internal bool Remove( string requestId )
        {
            using ( _asyncLock.Lock() )
            {
                if ( !Contains( requestId ) )
                {
                    return true;
                }

                try
                {
                    return _requests.Remove( requestId );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( this, ex, $"Remove RPC request to container occur exception. RequestId: {requestId}.", "ServiceHostTrace" );
                    return false;
                }
            }
        }

        #endregion 宣告內部的方法
    }
}
