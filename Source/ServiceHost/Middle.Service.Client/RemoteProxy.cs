﻿using NeoSmart.AsyncLock;
using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>遠端服務代理人
    /// </summary>
    public class RemoteProxy
    {
        #region Private Fields

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>遠端服務的客戶端物件
        /// </summary>
        private RemoteClient _remoteClient;

        #endregion Private Fields


        #region Constructors & Initialize

        /// <summary>預設建構子
        /// </summary>
        public RemoteProxy()
        {
            InitializeConfiguration();

            ServiceClientName = GlobalClientContext.Config.ServiceClients?.FirstOrDefault()?.ServiceClientName;

            if ( ServiceClientName.IsNullOrEmpty() )
            {
                ServiceClientName = null;
                var errorMsg = $"{Logger.GetTraceLogTitle( this, nameof ( RemoteProxy ) )}.cotr No serviceClients in serviceClientConfig.json config.";
                ConsoleLogger.LogError( errorMsg );
                throw new ConfigurationErrorsException( errorMsg );
            }

            Initialize();
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        /// <param name="configPath">服務客戶端 serviceClientConfig.json 設定檔的相對路徑</param>
        public RemoteProxy( string serviceClientName, string configPath = null )
        {
            InitializeConfiguration( configPath );

            if ( serviceClientName.IsNullOrEmpty() )
            {
                throw new ArgumentNullException( $"Argument '{nameof ( serviceClientName )}' can not be null or empty string." );
            }

            ServiceClientName = serviceClientName;

            if ( ServiceClientName.IsNullOrEmpty() )
            {
                ServiceClientName = null;
                var errorMsg = $"{Logger.GetTraceLogTitle( this, nameof ( RemoteProxy ) )}.cotr No '{serviceClientName}' serviceClients in serviceClientConfig.json config.";
                ConsoleLogger.LogError( errorMsg );
                throw new ConfigurationErrorsException( errorMsg );
            }

            Initialize();
        }

        /// <summary>初始化與讀取客戶端服務組態 serviceClientConfig.json 設定檔
        /// </summary>
        private void InitializeConfiguration( string path = "./serviceClientConfig.json" ) 
        {
            using ( _asyncLock.Lock() )
            {
                if ( GlobalClientContext.Config.IsNotNull() )
                {
                    return;
                }

                dynamic cr = ClientConfigReader.GetConfig( path );

                if ( null == cr || !cr.Success )
                {
                    Logger.WriteErrorLog( this, $"Load serviceClientConfig.json file settings fail.", $"{nameof ( RemoteProxy )} static ctor", "ServiceHostTrace" );
                    ConsoleLogger.LogError( $"{Logger.GetTraceLogTitle( this, nameof ( InitializeConfiguration ) )}, Load serviceClientConfig.json file settings fail. {cr.Message}" );
                    return;
                }

                GlobalClientContext.Config     = cr.Data;
                GlobalClientContext.JsonConfig = cr.JConfig;
            }
        }

        /// <summary>初始化 RemoteProxy 客戶端物件
        /// </summary>
        private void Initialize()
        {
            var r = RemoteClientContainer.Get( ServiceClientName );

            if ( !r.Success )
            {
                var errorMsg = $"Create remote host client component object fail. {r.Message}";
                Logger.WriteErrorLog( this, errorMsg, $"{nameof ( Initialize )}", "ServiceHostTrace" );
                ConsoleLogger.LogError( $"{Logger.GetTraceLogTitle( this, nameof ( Initialize ) )}, {errorMsg}" );
                throw new Exception( $"{Logger.GetTraceLogTitle( this, nameof ( Initialize ) )}, {errorMsg}" );
            }

            _remoteClient = (RemoteClient)r.Data;
        }

        #endregion Constructors & Initialize


        #region Properties

        /// <summary>服務客戶端個體的設定名稱
        /// </summary>
        public string ServiceClientName { get; set; }

        /// <summary>遠端服務的連線中斷的處理委派
        /// </summary>
        /// <value></value>
        public ConnectionBrokenHandler ConnectionBroken 
        {
            get => _remoteClient.ConnectionBroken;
            set => _remoteClient.ConnectionBroken = value;
        }

        /// <summary>遠端服務的連線重新恢復的處理委派
        /// </summary>
        /// <value></value>
        public ConnectionRestoreHandler ConnectionRestored 
        {
            get => _remoteClient.ConnectionRestored;
            set => _remoteClient.ConnectionRestored = value;
        }

        #endregion Properties


        #region Protected Methods

        /// <summary>執行動作呼叫
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體泛型</typeparam>
        /// <typeparam name="TResDTO">回應資料載體泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料集合</param>
        /// <returns>執行結果</returns>
        protected Result<TResDTO> Execute<TReqDTO, TResDTO>( string actionName, TReqDTO request )
        {
            if ( _remoteClient is InProcessClient inProcessClient ) 
            {
               return inProcessClient.Invoke<TResDTO>( actionName, request );
            }

            var result = Result.Create<TResDTO>();

            var z = _remoteClient.Execute<Result<TResDTO>>( actionName, request );

            if ( !z.Success )
            {
                result.Code    = z.Code.IsNullOrEmptyString( StatusCode.CLIENT_ERROR );
                result.Message = z.Message;
                Logger.WriteErrorLog( this, $"Execute remote invoke fail. ActionName: {actionName}. {Environment.NewLine}{z.Message}", "Execute", "ServiceHostTrace" );
                return result;
            }

            Result<TResDTO> r = z.Data;

            if ( r.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Retrive response DTO is null. ActionName: {actionName}.";
                Logger.WriteErrorLog( this, r.Message, "Execute", "ServiceHostTrace" );
                return result;
            }

            TResDTO resDTO = r.Data;

            result.Data      = resDTO;
            result.Message   = r.Message;
            result.Code      = r.Code;
            result.Success   = r.Success;

            Check.IsFalse( r.Success, () => Logger.WriteErrorLog( this, $"Execute remote action retrive fail status. ActionName: {actionName}.", "Execute", "ServiceHostTrace" ) );
            return result;
        }

        /// <summary>執行動作呼叫
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體泛型</typeparam>
        /// <typeparam name="TResDTO">回應資料載體泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料集合</param>
        /// <returns>執行結果</returns>
        protected async Task<Result<TResDTO>> ExecuteAsync<TReqDTO, TResDTO>( string actionName, TReqDTO request )
        {
            if ( _remoteClient is InProcessClient inProcessClient ) 
            {
               return await inProcessClient.InvokeAsync<TResDTO>( actionName, request );
            }

            var result = Result.Create<TResDTO>();

            var z = await _remoteClient.ExecuteAsync<Result<TResDTO>>( actionName, request );

            if ( !z.Success )
            {
                result.Code    = z.Code.IsNullOrEmptyString( StatusCode.CLIENT_ERROR );
                result.Message = z.Message;
                Logger.WriteErrorLog( this, $"Execute remote invoke fail. ActionName: {actionName}. {Environment.NewLine}{z.Message}", "ExecuteAsync", "ServiceHostTrace" );
                return result;
            }

            Result<TResDTO> r = z.Data;

            if ( r.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Retrive response DTO is null. ActionName: {actionName}.";
                Logger.WriteErrorLog( this, r.Message, "ExecuteAsync", "ServiceHostTrace" );
                return result;
            }

            TResDTO resDTO = r.Data;

            result.Data      = resDTO;
            result.Message   = r.Message;
            result.Code      = r.Code;
            result.Success   = r.Success;

            Check.IsFalse( r.Success, () => Logger.WriteErrorLog( this, $"Execute remote action retrive fail status. ActionName: {actionName}.", "ExecuteAsync", "ServiceHostTrace" ) );
            return result;
        }

        /// <summary>執行動作呼叫
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料集合</param>
        /// <returns>執行結果</returns>
        protected Result Execute<TReqDTO>( string actionName, TReqDTO request )
        {
            if ( _remoteClient is InProcessClient inProcessClient ) 
            {
                return inProcessClient.Invoke( actionName, request );
            }

            var result = Result.Create();

            var z = _remoteClient.Execute<Result>( actionName, request );

            if ( !z.Success )
            {
                result.Code    = z.Code.IsNullOrEmptyString( StatusCode.CLIENT_ERROR );
                result.Message = z.Message;
                Logger.WriteErrorLog( this, $"Execute remote invoke fail. ActionName: {actionName}. {Environment.NewLine}{z.Message}", "Execute", "ServiceHostTrace" );
                return result;
            }

            Result r = z.Data;

            if ( r.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Retrive response DTO is null. ActionName: {actionName}.";
                Logger.WriteErrorLog( this, r.Message, "Execute", "ServiceHostTrace" );
                return result;
            }
            
            result.Message   = r.Message;
            result.Code      = r.Code;
            result.Success   = r.Success;

            Check.IsFalse( r.Success, () => Logger.WriteErrorLog( this, $"Execute remote action retrive fail status. ActionName: {actionName}.", "Execute", "ServiceHostTrace" ) );
            return result;
        }

        /// <summary>執行動作呼叫
        /// </summary>
        /// <typeparam name="TReqDTO">請求資料載體泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="request">請求資料集合</param>
        /// <returns>執行結果</returns>
        protected async Task<Result> ExecuteAsync<TReqDTO>( string actionName, TReqDTO request )
        {
            if ( _remoteClient is InProcessClient inProcessClient ) 
            {
                return await inProcessClient.InvokeAsync( actionName, request );
            }

            var result = Result.Create();

            var z = await _remoteClient.ExecuteAsync<Result>( actionName, request );

            if ( !z.Success )
            {
                result.Code    = z.Code.IsNullOrEmptyString( StatusCode.CLIENT_ERROR );
                result.Message = z.Message;
                Logger.WriteErrorLog( this, $"Execute remote invoke fail. ActionName: {actionName}. {Environment.NewLine}{z.Message}", "ExecuteAsync", "ServiceHostTrace" );
                return result;
            }

            Result r = z.Data;

            if ( r.IsNull() )
            {
                result.Code    = StatusCode.CLIENT_ERROR;
                result.Message = $"Retrive response DTO is null. ActionName: {actionName}.";
                Logger.WriteErrorLog( this, r.Message, "ExecuteAsync", "ServiceHostTrace" );
                return result;
            }
            
            result.Message   = r.Message;
            result.Code      = r.Code;
            result.Success   = r.Success;

            Check.IsFalse( r.Success, () => Logger.WriteErrorLog( this, $"Execute remote action retrive fail status. ActionName: {actionName}.", "ExecuteAsync", "ServiceHostTrace" ) );
            return result;
        }

        /// <summary>發佈事件訊息
        /// </summary>
        /// <typeparam name="TMessageDTO">事件訊息資料載體的泛型</typeparam>
        /// <param name="actionName">動作名稱</param>
        /// <param name="messageDTO">事件訊息資料載體</param>
        /// <returns>發佈的結果</returns>
        protected Result Publish<TMessageDTO>( string actionName, TMessageDTO messageDTO )
        {
            var result = Result.Create();

            var r = _remoteClient.Publish( actionName, messageDTO );

            if ( !r.Success )
            {
                result.Code    = r.Code.IsNullOrEmptyString( StatusCode.CLIENT_ERROR );
                result.Message = r.Message;
                Logger.WriteErrorLog( this, $"Execute remote invoke fail. ActionName: {actionName}. {Environment.NewLine}{r.Message}", "Execute", "ServiceHostTrace" );
                return result;
            }

            result.Code      = r.Code;
            result.Message   = r.Message;
            result.Success   = r.Success;
            return result;
        }

        /// <summary>註冊事件訊息處理器
        /// </summary>
        /// <param name="actionName">動作名稱 (事件名稱)</param>
        /// <param name="type">事件訊息處理器的型別</param>
        /// <returns>註冊結果</returns>
        protected Result AddEventProcessor( string actionName, Type type )
        {
            var result = Result.Create();

            RemoteClient remoteClient = _remoteClient as RemoteClient;

            if ( remoteClient.IsNull() )
            {
                result.Code    = StatusCode.INTERNAL_ERROR;
                result.Message = $"";
                return result;
            }

            return remoteClient.EventHandlers.Add( actionName, type );
        }

        #endregion Protected Methods
    }
}
