﻿using System;
using System.Threading.Tasks;
using ZayniFramework.DataAccess;
using ZayniFramework.Logging;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>客戶端動作日誌紀錄的資料存取類別
    /// </summary>
    internal sealed class ServiceClientActionLogDao : BaseDataAccess
    {
        #region 宣告私有的欄位

        /// <summary>資料庫連線名稱
        /// </summary>
        private static readonly string _dbName = "Zayni";

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        internal ServiceClientActionLogDao() : base( _dbName )
        {
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>寫入客戶端請求動作日誌
        /// </summary>
        /// <param name="model">日誌資料模型</param>
        internal async Task InsertLogAsync( ServiceClientActionLogModel model )
        {
            try
            {
                #region 宣告 SQL 字串

                string sql = @" 
                    -- Insert FS_SERVICE_CLIENT_ACTION_LOG
                    INSERT INTO `FS_SERVICE_CLIENT_ACTION_LOG` (
                          `REQUEST_ID`
                        , `CLIENT_NAME`
                        , `CLIENT_HOST`
                        , `REMOTE_NAME`
                        , `REMOTE_HOST`

                        , `DIRECTION`
                        , `ACTION_NAME`
                        , `DATA_CONTENT`
                        , `REQUEST_TIME`
                        , `RESPONSE_TIME`

                        , `IS_SUCCESS`
                        , `CODE`
                        , `MESSAGE`
                        , `LOG_TIME`
                    ) VALUES (
                          @RequestId
                        , @ServiceClientName
                        , @ServiceClientHost
                        , @RemoteServiceName
                        , @RemoteServiceHost

                        , @Direction
                        , @ActionName
                        , @DataContent
                        , @RequestTime
                        , @ResponseTime

                        , @IsSuccess
                        , @Code
                        , @Message
                        , @LogTime
                    ) ";

                #endregion 宣告 SQL 字串

                #region 執行資料新增

                using ( var conn = await base.CreateConnectionAsync() )
                {
                    var cmd = base.GetSqlStringCommand( sql, conn );
                    cmd.CommandTimeout = 60;

                    base.AddInParameter( cmd, "@RequestId",         DbTypeCode.String,   model.RequestId );
                    base.AddInParameter( cmd, "@ServiceClientName", DbTypeCode.String,   model.ServiceClientName );
                    base.AddInParameter( cmd, "@ServiceClientHost", DbTypeCode.String,   model.ServiceClientHost );
                    base.AddInParameter( cmd, "@RemoteServiceName", DbTypeCode.String,   model.RemoteServiceName );
                    base.AddInParameter( cmd, "@RemoteServiceHost", DbTypeCode.String,   model.RemoteServiceHost );

                    base.AddInParameter( cmd, "@Direction",         DbTypeCode.Byte,     model.Direction );
                    base.AddInParameter( cmd, "@ActionName",        DbTypeCode.String,   model.ActionName );
                    base.AddInParameter( cmd, "@DataContent",       DbTypeCode.String,   model.DataContent );
                    base.AddInParameter( cmd, "@RequestTime",       DbTypeCode.DateTime, model.RequestTime );
                    base.AddInParameter( cmd, "@ResponseTime",      DbTypeCode.DateTime, model.ResponseTime );

                    base.AddInParameter( cmd, "@IsSuccess",         DbTypeCode.Boolean,  model.IsSuccess );
                    base.AddInParameter( cmd, "@Code",              DbTypeCode.String,   model.Code );
                    base.AddInParameter( cmd, "@Message",           DbTypeCode.String,   model.Message );
                    base.AddInParameter( cmd, "@LogTime",           DbTypeCode.DateTime, model.LogTime );

                    if ( ! await base.ExecuteNonQueryAsync( cmd ) )
                    {
                        Logger.WriteErrorLog( this, $"Execute SQL fail: {Environment.NewLine}{base.Message}{Environment.NewLine}{sql}", GetLogTraceName( nameof ( InsertLogAsync ) ), "ServiceHostTrace" );
                        conn.Close();
                        return;
                    }

                    conn.Close();
                }

                #endregion 執行資料新增
            }
            catch ( Exception ex )
            {
                Logger.WriteErrorLog( this, $"Write service client action log into FS_SERVICE_CLIENT_ACTION_LOG occur exception. {ex.ToString()}", Logger.GetTraceLogTitle( this, nameof ( InsertLogAsync ) ) );
            }
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>取得日誌的追蹤動作名稱
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的追蹤動作名稱</returns>
        private static string GetLogTraceName( string methodName ) => $"{nameof ( ServiceClientActionLogDao )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
