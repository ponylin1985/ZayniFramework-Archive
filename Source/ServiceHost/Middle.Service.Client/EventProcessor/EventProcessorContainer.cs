﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>事件訊息處理器容器
    /// </summary>
    internal sealed class EventProcessorContainer
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>事件訊息處理器池<para/>
        /// Key: 動作名稱 (遠端觸發的事件名稱)<para/>
        /// Value: 事件訊息處理器的型別
        /// </summary>
        private readonly Dictionary<string, Type> _container = new Dictionary<string, Type>();

        #endregion 宣告私有的欄位


        #region 宣告內部的方法

        /// <summary>加入事件處理器
        /// </summary>
        /// <param name="action">動作名稱 (事件名稱)</param>
        /// <param name="typeOfHandler">事件訊息處理器的型別</param>
        /// <returns>執行結果</returns>
        internal Result Add( string action, Type typeOfHandler )
        {
            using ( _asyncLock.Lock() )
            {
                var result = new Result()
                {
                    Success = false,
                    Message   = null
                };

                try
                {
                    if ( _container.ContainsKey( action ) )
                    {
                        result.Success = true;
                        return result;
                    }

                    if ( action.IsNullOrEmpty() )
                    {
                        result.Code    = StatusCode.CLIENT_ERROR;
                        result.Message = $"Add event processor fail due to 'ActionName' is null or empty.";
                        Logger.WriteErrorLog( nameof ( EventProcessorContainer ), result.Message, nameof ( Add ), "ServiceHostTrace" );
                        return result;
                    }

                    if ( typeOfHandler.IsNull() )
                    {
                        result.Code    = StatusCode.INTERNAL_ERROR;
                        result.Message = $"Add event processor fail due to 'typeOfHandler' is null.";
                        Logger.WriteErrorLog( nameof ( EventProcessorContainer ), result.Message, nameof ( Add ), "ServiceHostTrace" );
                        return result;
                    }

                    if ( !typeOfHandler.IsSubclassOf( typeof ( EventProcessor ) ) )
                    {
                        result.Code    = StatusCode.INTERNAL_ERROR;
                        result.Message = $"Add event processor fail due to 'typeOfHandler' is not a subtype of {nameof ( EventProcessor )}.";
                        Logger.WriteErrorLog( nameof ( EventProcessorContainer ), result.Message, nameof ( Add ), "ServiceHostTrace" );
                        return result;
                    }

                    _container.Add( action, typeOfHandler );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( nameof ( EventProcessorContainer ), ex, "Add event processor to container occur exception.", "ServiceHostTrace" );
                    return result;
                }

                result.Success = true;
                return result;
            }
        }

        /// <summary>取得事件處理器
        /// </summary>
        /// <param name="action">動作名稱 (事件名稱)</param>
        /// <returns>取得結果</returns>
        internal EventProcessor Get( string action )
        {
            using ( _asyncLock.Lock() )
            {
                try
                {
                    if ( !_container.ContainsKey( action ) )
                    {
                        Logger.WriteErrorLog( this, $"Get '{action}' event processor from container fail. No such action. Action: {action}.", nameof ( Get ), "ServiceHostTrace" );
                        return null;
                    }

                    var type = _container[ action ];

                    if ( type.IsNull() )
                    {
                        Logger.WriteErrorLog( this, $"Get '{action}' event processor from container fail. Type of event processor from is null. Action: {action}.", nameof ( Get ), "ServiceHostTrace" );
                        return null;
                    }

                    if ( !type.IsSubclassOf( typeof ( EventProcessor ) ) )
                    {
                        Logger.WriteErrorLog( this, $"Get '{action}' event processor from container fail. Type of event processor from is not a subtype of {nameof ( EventProcessor )}. Action: {action}.", nameof ( Get ), "ServiceHostTrace" );
                        return null;
                    }

                    return (EventProcessor)Activator.CreateInstance( type );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( nameof ( EventProcessorContainer ), ex, $"Get '{action}' event processor from container occur exception.", "ServiceHostTrace" );
                    return null;
                }
            }
        }

        #endregion 宣告內部的方法
    }
}
