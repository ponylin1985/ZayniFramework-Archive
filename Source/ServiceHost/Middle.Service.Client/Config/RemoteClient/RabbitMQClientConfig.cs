﻿using Newtonsoft.Json;
using ZayniFramework.Middle.Service.Client.RabbitMQ;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>RabbitMQ 通訊客戶端的設定組態
    /// </summary>
    public sealed class RabbitMQClientConfig : RemoteClientConfig
    {
        /// <summary>RabbitMQ 服務主機位址
        /// </summary>
        [JsonProperty( PropertyName = "mbHost" )]
        public string MbHost { get; internal set; }

        /// <summary>RabbitMQ 服務連接阜號
        /// </summary>
        [JsonProperty( PropertyName = "mbPort" )]
        public int MbPort { get; internal set; }

        /// <summary>RabbitMQ 服務連接帳號
        /// </summary>
        [JsonProperty( PropertyName = "mbUserId" )]
        public string MbUserId { get; internal set; }

        /// <summary>RabbitMQ 服務連接密碼
        /// </summary>
        [JsonProperty( PropertyName = "mbPassword" )]
        public string MbPassword { get; internal set; }

        /// <summary>RabbitMQ 服務的 VHost
        /// </summary>
        [JsonProperty( PropertyName = "mbVHost" )]
        public string MbVHost { get; internal set; }

        /// <summary>預設發送訊息的 Exchange 名稱
        /// </summary>
        [JsonProperty( PropertyName = "defaultExchange" )]
        public string DefaultExchange { get; internal set; }

        /// <summary>RPC 設定集合
        /// </summary>
        [JsonProperty( PropertyName = "rpc" )]
        public RPCConfig RpcConfig { get; internal set; }

        /// <summary>訊息發佈設定集合
        /// </summary>
        [JsonProperty( PropertyName = "publish" )]
        public PublishConfig PublishConfig { get; internal set; }

        /// <summary>監聽訊息組態設定
        /// </summary>
        [JsonProperty( PropertyName = "listen" )]
        public ListenConfig ListenConfig { get; internal set; }
    }
}
