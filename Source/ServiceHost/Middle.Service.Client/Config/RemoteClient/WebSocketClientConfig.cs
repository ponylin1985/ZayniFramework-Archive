﻿using Newtonsoft.Json;
using System;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>WebSocket 通訊客戶端的設定組態
    /// </summary>
    [Serializable()]
    public sealed class WebSocketClientConfig : RemoteClientConfig
    {
        /// <summary>WebSocket Host 服務端的連線 Base URL 位址
        /// </summary>
        [JsonProperty( PropertyName = "webSocketHostBaseUrl" )]
        public string WebSocketHostBaseUrl { get; internal set; }

        /// <summary>等待 WebSocket 服務 RPC 回應訊息的逾時秒數
        /// </summary>
        [JsonProperty( PropertyName = "wsResponseTimeout" )]
        public int ResponseTimeout { get; internal set; }
    }
}
