﻿using Newtonsoft.Json;


namespace ZayniFramework.Middle.Service.Client.RabbitMQ
{
    /// <summary>RPC 遠端呼叫組態設定
    /// </summary>
    public sealed class RPCConfig
    {
        /// <summary>預設發送 RPC 請求的 Exchange 名稱
        /// </summary>
        [JsonProperty( PropertyName = "exchange" )]
        public string Exchange { get; internal set; }

        /// <summary>請求訊息的 RoutingKey 前綴字串
        /// </summary>
        [JsonProperty( PropertyName = "routingKey" )]
        public string RoutingKey { get; internal set; }

        /// <summary>接收 RPC 回覆訊息的 Queue 名稱
        /// </summary>
        [JsonProperty( PropertyName = "rpcReplyQueue" )]
        public string RpcReplyQueue { get; internal set; }

        /// <summary>等待 RPC 回應訊息的逾時秒數
        /// </summary>
        [JsonProperty( PropertyName = "rcpResponseTimeout" )]
        public int RpcResponseTimeout { get; internal set; }
    }
}
