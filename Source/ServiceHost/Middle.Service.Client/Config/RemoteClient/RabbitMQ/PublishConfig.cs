﻿using Newtonsoft.Json;


namespace ZayniFramework.Middle.Service.Client.RabbitMQ
{
    /// <summary>訊息發佈組態設定
    /// </summary>
    public sealed class PublishConfig
    {
        /// <summary>預設發送 RPC 請求的 Exchange 名稱
        /// </summary>
        [JsonProperty( PropertyName = "exchange" )]
        public string Exchange { get; internal set; }

        /// <summary>訊息的 RoutingKey 前綴字串
        /// </summary>
        [JsonProperty( PropertyName = "routingKey" )]
        public string RoutingKey { get; internal set; }
    }
}
