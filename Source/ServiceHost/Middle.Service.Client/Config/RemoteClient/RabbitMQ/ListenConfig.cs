﻿using Newtonsoft.Json;


namespace ZayniFramework.Middle.Service.Client.RabbitMQ
{
    /// <summary>監聽訊息組態設定
    /// </summary>
    public class ListenConfig
    {
        #region Listen Exchange Routing: Auto-scale runtime queue consume

        /// <summary>監聽的 Exchange 名稱
        /// </summary>
        /// <value></value>
        [JsonProperty( PropertyName = "exchangeName" )]
        public string ListenExchange { get; internal set; }

        /// <summary>在 runtime 動態產生接收 ListenExchange 的 Queue 住列名稱前綴字。<para/>
        /// * 注意: 不需要在 RabbitMQ web management 上事前建立此 Queue 住列。<para/>
        /// * 此在 runtime 動態建立的 Queue 為 RabbitMQ auto-delete 的 Queue，在連線關閉後會自動刪除。<para/>
        /// </summary>
        /// <value></value>
        [JsonProperty( PropertyName = "queueNamePrefix" )]
        public string ListenQueuePrefix { get; set; }

        /// <summary>監聽 ListenExchange 訊息的 RoutingKey 字串前綴字
        /// </summary>
        [JsonProperty( PropertyName = "exchangeRoutingKey" )]
        public string ListenExchangeRoutingKey { get; internal set; }

        #endregion Listen Exchange Routing: Auto-scale runtime queue consume


        #region Listen Queue: No Auto-scale queue consume

        /// <summary>監聽的住列名稱。<para/>
        /// * 注意: 需要在 RabbitMQ web management 上事前建立此 Queue 住列，否則初始化 RabbitMQ 連線失敗。<para/>
        /// </summary>
        [JsonProperty( PropertyName = "msgQueue" )]
        public string ListenQueue { get; internal set; }

        /// <summary>訊息的 RoutingKey 字串前綴字
        /// </summary>
        [JsonProperty( PropertyName = "msgRoutingKey" )]
        public string ListenRoutingKey { get; internal set; }

        #endregion Listen Queue: No Auto-scale queue consume
    }
}
