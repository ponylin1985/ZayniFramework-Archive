﻿using Newtonsoft.Json;
using System;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>TCP Socket 通訊客戶端的設定組態
    /// </summary>
    [Serializable()]
    public sealed class TCPSocketClientConfig : RemoteClientConfig
    {
        /// <summary>TCP Socket 訊息的結束標記字串
        /// </summary>
        [JsonProperty( PropertyName = "tcpMsgEndsWith" )]
        public string TCPMsgEndToken { get; internal set; } = "u0003";

        /// <summary>TCP Socket 服務主機的 IP 位址
        /// </summary>
        [JsonProperty( PropertyName = "tcpServerHost" )]
        public string TCPServerHost { get; internal set; }

        /// <summary>TCP Socket 服務連接阜號
        /// </summary>
        [JsonProperty( PropertyName = "tcpServerPort" )]
        public int TCPServerPort { get; internal set; }

        /// <summary>TCP Socket 寫入網路串流的 Buffer 緩衝的大小 (KB)
        /// </summary>
        [JsonProperty( PropertyName = "tcpSendBufferSize" )]
        public int TCPSendBufferSize { get; internal set; }

        /// <summary>TCP Socket 讀取網路串流的 Buffer 緩衝的大小 (KB)
        /// </summary>
        [JsonProperty( PropertyName = "tcpReceiveBufferSize" )]
        public int TCPReceiveBufferSize { get; internal set; }

        /// <summary>TCP Socket 建立連線的逾時秒數
        /// </summary>
        [JsonProperty( PropertyName = "connectionTimeout" )]
        public int ConnecionTimeout { get; internal set; }

        /// <summary>等待 TCP Socket 服務 RPC 回應訊息的逾時秒數
        /// </summary>
        [JsonProperty( PropertyName = "tcpResponseTimeout" )]
        public int TCPResponseTimeout { get; internal set; }
    }
}
