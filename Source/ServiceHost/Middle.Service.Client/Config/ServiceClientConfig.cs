﻿using Newtonsoft.Json;
using System;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>服務客戶端個體的設定組態
    /// </summary>
    [Serializable()]
    public sealed class ServiceClientConfig
    {
        /// <summary>服務客戶端個體的設定名稱
        /// </summary>
        [JsonProperty( PropertyName = "serviceClientName" )]
        public string ServiceClientName { get; internal set; }

        /// <summary>對應的遠端服務的設定名稱 (serviceHostConfig.js 中的 serviceHosts/serviceName 設定值)
        /// </summary>
        [JsonProperty( PropertyName = "remoteServiceName" )]
        public string RemoteServiceName { get; internal set; }

        /// <summary>服務自我託管的模式，必須與對應的遠端服務 HostType 設定一致。<para/>
        /// .NET Remoting: 採用 .NET Framework 的 Remoting 服務。<para/>
        /// RabbitMQ: 採用 RabbitMQ AMQP 的事件監聽。<para/>
        /// TCPSocket: 採用 TCP Socket 通訊協定的網路串流的服務。<para/>
        /// WebSocekt: 採用 WebSocket 通訊協定的網路串流的服務。<para/>
        /// Http: 採用 Http 通訊協定的網路串流的服務。<para/>
        /// </summary>
        [JsonProperty( PropertyName = "remoteHostType" )]
        public string RemoteHostType { get; internal set; }

        /// <summary>通訊客戶端設定名稱
        /// </summary>
        [JsonProperty( PropertyName = "remoteClient" )]
        public string RemoteClientName { get; internal set; }

        /// <summary>是否啟用客戶端動作的資料庫日誌紀錄功能
        /// </summary>
        [JsonProperty( PropertyName = "enableActionLogToDB" )]
        public bool EnableDbLogging { get; internal set; }

        /// <summary>動作日誌紀錄在 ElasticSearch 服務的 ESLogger 設定名稱
        /// * 當設定值不為 Null 或空字串的情況下，會自動啟動指定的 ESLogger 日誌器進行紀錄。
        /// </summary>
        [JsonProperty( PropertyName = "esLoggerName" )]
        public string ESLoggerName { get; internal set; }
    }
}
