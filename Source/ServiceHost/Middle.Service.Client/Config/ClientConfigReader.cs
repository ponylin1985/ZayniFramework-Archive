﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>組態設定讀取器
    /// </summary>
    public class ClientConfigReader
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>組態設定值物件
        /// </summary>
        private static Config _configs;

        /// <summary>JSON 組態設定值物件
        /// </summary>
        private static dynamic _jsonConfigs;

        #endregion 宣告私有的欄位


        #region 宣告公開的方法

        /// <summary>取得組態設定集合
        /// </summary>
        /// <param name="path">服務客戶端組態 serviceClientConfig.json 設定檔的相對路徑</param>
        /// <returns>組態設定值</returns>
        public static Result<Config> GetConfig( string path = "./serviceClientConfig.json" )
        {
            using ( _asyncLock.Lock() )
            {
                dynamic result = Result.CreateDynamicResult<Config>();
                var logTitle = GetLogTitle( nameof ( GetConfig ) );

                if ( _configs.IsNotNull() )
                {
                    result.Data      = _configs.CloneObject();
                    result.JConfig   = ((object)_jsonConfigs).CloneObject();
                    result.Success   = true;
                    result.IsSuccess = true;
                    return result;
                }

                path = path.IsNullOrEmptyString( "./serviceClientConfig.json" );

                Config config;
                dynamic jconfig;

                try
                {
                    string execPath   = AppDomain.CurrentDomain.BaseDirectory;
                    string configPath = path.Contains( execPath ) ? Path.Combine( execPath, path ) : path;

                    _jsonConfigs = ConfigManager.GetConfig( configPath );
                    _configs     = ((JToken)_jsonConfigs).ToObject<Config>();

                    config  = _configs;
                    jconfig = _jsonConfigs;
                }
                catch ( Exception ex )
                {
                    result.Message = $"Load serviceClientConfig.json file settings occur exception. Config path: {path}.";
                    Logger.WriteExceptionLog( nameof ( ClientConfigReader ), ex, eventTitle: logTitle, message: result.Message, loggerName: "ServiceHostTrace" );
                    ConsoleLogger.LogError( $"{logTitle} {result.Message}" );
                    return result;
                }

                if ( _configs.IsNull() )
                {
                    result.Message = $"Load serviceClientConfig.json file settings fail: Configs are null.";
                    Logger.WriteErrorLog( nameof ( ClientConfigReader ), result.Message, logTitle, "ServiceHostTrace" );
                    ConsoleLogger.LogError( $"{logTitle} {result.Message}" );
                    return result;
                }
            
                result.Data      = config;
                result.JConfig   = jconfig;
                result.Success   = true;
                result.IsSuccess = true;
                return result;
            }
        }

        /// <summary>取得服務客戶端設定組態
        /// </summary>
        /// <param name="serviceClientName">服務客戶端個體的設定名稱</param>
        /// <returns>服務客戶端設定組態</returns>
        public static ServiceClientConfig GetServiceClientConfig( string serviceClientName )
        {
            try
            {
                return GlobalClientContext.Config.ServiceClients.Where( g => g.ServiceClientName == serviceClientName ).FirstOrDefault();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ClientConfigReader ), ex, $"Load service client config setting occur exception. serviceClientName: {serviceClientName}. {Environment.NewLine}{ex.ToString()}" );
                ConsoleLogger.LogError( $"{GetLogTitle( nameof ( GetServiceClientConfig ) )}, Load service client config setting occur exception. serviceClientName: {serviceClientName}. {Environment.NewLine}{ex.ToString()}" );
                throw ex;
            }
        }

        #endregion 宣告公開的方法


        #region 宣告內部的方法

        /// <summary>取得 RabbitMQ 客戶端連線設定組態
        /// </summary>
        /// <param name="config">ServiceClientConfig 服務客戶端設定組態物件</param>
        /// <returns>RabbitMQ 客戶端連線設定組態</returns>
        internal static RabbitMQClientConfig GetRabbitMQClientConfig( ServiceClientConfig config )
        {
            string name = null;

            try
            {
                // Pony TODO: 這邊幹嘛去抓 TCPSocketProtocol 的 DefaultRemoteClient
                name = config.RemoteClientName.IsNullOrEmpty() ? GlobalClientContext.Config.TCPSocketProtocol.DefaultRemoteClient : config.RemoteClientName;
                return GlobalClientContext.Config.RabbitMQProtocol.RemoteClients.Where( q => q.Name == name ).FirstOrDefault();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ClientConfigReader ), ex, $"Load RabbitMQ client config setting occur exception. Name: {name}. {Environment.NewLine}{ex.ToString()}" );
                ConsoleLogger.LogError( $"{GetLogTitle( nameof ( GetRabbitMQClientConfig ) )}, Load RabbitMQ client config setting occur exception. Name: {name}. {Environment.NewLine}{ex.ToString()}" );
                throw ex;
            }
        }

        /// <summary>取得 TCP Socket 客戶端連線設定組態
        /// </summary>
        /// <param name="config">ServiceClientConfig 服務客戶端設定組態物件</param>
        /// <returns>TCP Socket 客戶端連線設定組態</returns>
        internal static TCPSocketClientConfig GetTCPSocketClientConfig( ServiceClientConfig config )
        {
            string name = null;

            try
            {
                name = config.RemoteClientName.IsNullOrEmpty() ? GlobalClientContext.Config.TCPSocketProtocol.DefaultRemoteClient : config.RemoteClientName;
                return GlobalClientContext.Config.TCPSocketProtocol.RemoteClients.Where( q => q.Name == name ).FirstOrDefault();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ClientConfigReader ), ex, $"Load TCP socket client config setting occur exception. Name: {name}. {Environment.NewLine}{ex.ToString()}" );
                ConsoleLogger.LogError( $"{GetLogTitle( nameof ( GetTCPSocketClientConfig ) )}, Load TCP socket client config setting occur exception. Name: {name}. {Environment.NewLine}{ex.ToString()}" );
                throw ex;
            }
        }

        /// <summary>取得 Web Socket 客戶端連線設定組態
        /// </summary>
        /// <param name="config">ServiceClientConfig 服務客戶端設定組態物件</param>
        /// <returns>Web Socket 客戶端連線設定組態</returns>
        internal static WebSocketClientConfig GetWebSocketClientConfig( ServiceClientConfig config )
        {
            string name = null;

            try
            {
                // Pony TODO: 這邊幹嘛去抓 TCPSocketProtocol 的 DefaultRemoteClient
                name = config.RemoteClientName.IsNullOrEmpty() ? GlobalClientContext.Config.TCPSocketProtocol.DefaultRemoteClient : config.RemoteClientName;
                return GlobalClientContext.Config.WebSocketProtocol.RemoteClients.Where( q => q.Name == name ).FirstOrDefault();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ClientConfigReader ), ex, $"Load web socket client config setting occur exception. Name: {name}. {Environment.NewLine}{ex.ToString()}" );
                ConsoleLogger.LogError( $"{GetLogTitle( nameof ( GetWebSocketClientConfig ) )}, Load web socket client config setting occur exception. Name: {name}. {Environment.NewLine}{ex.ToString()}" );
                throw ex;
            }
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>取得日誌的標題
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的標題</returns>
        private static string GetLogTitle( string methodName ) => $"{nameof ( ClientConfigReader )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
