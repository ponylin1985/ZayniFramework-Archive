using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service.Client
{
    /// <summary>執行 RPC 遠端呼叫的處理委派
    /// </summary>
    /// <param name="requestId">原始請求識別碼</param>
    /// <param name="actionName">請求的動做名稱</param>
    /// <param name="requestData">請求資料內容</param>
    /// <returns>RPC 執行結果</returns>
    public delegate Result<ResponsePackage> ExecuteRpcHandler( string requestId, string actionName, object requestData = null );

    /// <summary>非同步執行 RPC 遠端呼叫的處理委派
    /// </summary>
    /// <param name="requestId">原始請求識別碼</param>
    /// <param name="actionName">請求的動做名稱</param>
    /// <param name="requestData">請求資料內容</param>
    /// <returns>RPC 執行結果</returns>
    public delegate Task<Result<ResponsePackage>> ExecuteRpcAsyncHandler( string requestId, string actionName, object requestData = null );

    /// <summary>遠端服務的連線中斷的處理委派
    /// </summary>
    public delegate void ConnectionBrokenHandler();

    /// <summary>遠端服務的連線重新恢復的處理委派
    /// </summary>
    public delegate void ConnectionRestoreHandler();
}