using Newtonsoft.Json;
using System;
using ZayniFramework.Middle.Service.Client;


namespace ZayniFramework.Middle.Service.Grpc.Client
{
    /// <summary>gRPC 客戶端連線組態設定
    /// </summary>
    [Serializable()]
    public class gRPCClientConfig : RemoteClientConfig
    {
        /// <summary>gRPC 遠端服務的 domain 或 IP 位址
        /// </summary>
        [JsonProperty( PropertyName = "serverHost" )]
        public string ServerHost { get; set; }

        /// <summary>gRPC 遠端服務的 Port
        /// </summary>
        [JsonProperty( PropertyName = "serverPort" )]
        public int ServerPort { get; set; }

        /// <summary>gRPC 遠端呼叫的逾時秒數
        /// </summary>
        /// <value></value>
        [JsonProperty( PropertyName = "rpcTimeout" )]
        public int? RpcTimeout { get; set; }
    }
}