﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace ZayniFramework.Middle.Service
{
    /// <summary>WebSocket 通訊託管組態設定集合
    /// </summary>
    [Serializable()]
    public sealed class WebSocketHostSettingConfig : HostSettingConfig
    {
        /// <summary>WebSocket 連線通訊設定集合
        /// </summary>
        [JsonProperty( PropertyName = "hosts" )]
        public List<WebSocketProtocolConfig> Hosts { get; internal set; }
    }
}
