﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace ZayniFramework.Middle.Service
{
    /// <summary>RabbitMQ AMQP 通訊託管組態設定集合
    /// </summary>
    [Serializable()]
    public sealed class RabbitMQHostSettingConfig : HostSettingConfig
    {
        /// <summary>RabbitMQ 訊息傳送連線JSON設定集合
        /// </summary>
        [JsonProperty( PropertyName = "hosts" )]
        public List<RabbitMQProtocolConfig> Hosts { get; internal set; }
    }
}
