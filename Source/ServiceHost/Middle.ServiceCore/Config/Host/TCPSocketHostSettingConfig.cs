﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace ZayniFramework.Middle.Service
{
    /// <summary>TCP Socket 通訊託管組態設定集合
    /// </summary>
    [Serializable()]
    public sealed class TCPSocketHostSettingConfig : HostSettingConfig
    {
        /// <summary>TCP Socket 連線通訊設定集合
        /// </summary>
        [JsonProperty( PropertyName = "hosts" )]
        public List<TCPSocketProtocolConfig> Hosts { get; set; }
    }
}
