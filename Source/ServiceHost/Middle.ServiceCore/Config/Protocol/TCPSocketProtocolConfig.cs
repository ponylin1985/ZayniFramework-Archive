﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace ZayniFramework.Middle.Service
{
    /// <summary>TCP Socket 連線通訊組態設定
    /// </summary>
    [Serializable()]
    public sealed class TCPSocketProtocolConfig : ProtocolConfig
    {
        /// <summary>TCP Socket 訊息的結束標記字串
        /// </summary>
        [JsonProperty( PropertyName = "tcpMsgEndsWith" )]
        public string TCPMsgEndToken { get; internal set; }

        /// <summary>TCP Socket 的連接阜號
        /// </summary>
        [JsonProperty( PropertyName = "tcpPort" )]
        public int TCPPort { get; internal set; }

        /// <summary>TCP Socket 客戶端的 IP 白名單 (如過設定為空陣列或 Null 則代表不限制任何 IP 都可以建立連線)
        /// </summary>
        [JsonProperty( PropertyName = "whitelist" )]
        public List<string> Whitelist { get; internal set; }

        /// <summary>讀取 TCP Socket 訊息的緩存大小 (KB)
        /// </summary>
        [JsonProperty( PropertyName = "tcpBufferSizeKB" )]
        public int TCPBufferSize { get; internal set; }
    }
}
