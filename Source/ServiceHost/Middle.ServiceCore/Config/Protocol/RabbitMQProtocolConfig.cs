﻿using Newtonsoft.Json;
using System;


namespace ZayniFramework.Middle.Service
{
    /// <summary>RabbitMQ 連線設定組態
    /// </summary>
    [Serializable()]
    public sealed class RabbitMQProtocolConfig : ProtocolConfig
    {
        /// <summary>RabbitMQ 服務主機位址
        /// </summary>
        [JsonProperty( PropertyName = "mbHost" )]
        public string MbHost { get; internal set; }

        /// <summary>RabbitMQ 服務連接阜號
        /// </summary>
        [JsonProperty( PropertyName = "mbPort" )]
        public int MbPort { get; internal set; }

        /// <summary>RabbitMQ 服務連接帳號
        /// </summary>
        [JsonProperty( PropertyName = "mbUserId" )]
        public string MbUserId { get; internal set; }

        /// <summary>RabbitMQ 服務連接密碼
        /// </summary>
        [JsonProperty( PropertyName = "mbPassword" )]
        public string MbPassword { get; internal set; }

        /// <summary>RabbitMQ 服務的VHost
        /// </summary>
        [JsonProperty( PropertyName = "mbVHost" )]
        public string MbVHost { get; internal set; }

        /// <summary>監聽訊息佇列的 RoutingKey 前綴字串
        /// </summary>
        [JsonProperty( PropertyName = "msgRoutingKey" )]
        public string MsgRoutingKeyPrefix { get; internal set; }

        /// <summary>監聽的訊息佇列的 Queue 名稱
        /// </summary>
        [JsonProperty( PropertyName = "msgQueue" )]
        public string MsgQueue { get; internal set; }

        /// <summary>監聽的 Exchange 名稱
        /// </summary>
        /// <value></value>
        [JsonProperty( PropertyName = "listenExchange" )]
        public string ListenExchange { get; internal set; }

        /// <summary>在 runtime 動態產生接收 ListenExchange 的 Queue 住列名稱前綴字。<para/>
        /// * 注意: 不需要在 RabbitMQ web management 上事前建立此 Queue 住列。<para/>
        /// * 此在 runtime 動態建立的 Queue 為 RabbitMQ auto-delete 的 Queue，在連線關閉後會自動刪除。<para/>
        /// </summary>
        /// <value></value>
        [JsonProperty( PropertyName = "listenQueuePrefix" )]
        public string ListenQueuePrefix { get; internal set; }

        /// <summary>監聽 ListenExchange 訊息的 RoutingKey 字串前綴字
        /// </summary>
        /// <value></value>
        [JsonProperty( PropertyName = "listenRoutingKey" )]
        public string ListenRoutingKey { get; internal set; }

        /// <summary>RPC 請求佇列的 RoutingKey 前綴字串
        /// </summary>
        [JsonProperty( PropertyName = "rpcRoutingKey" )]
        public string RpcRoutingKeyPrefix { get; internal set; }

        /// <summary>接收 RPC 請求訊息的 Queue 名稱
        /// </summary>
        [JsonProperty( PropertyName = "rpcQueue" )]
        public string RpcQueue { get; internal set; }

        /// <summary>事件訊息發佈時，預設的 Exchange 名稱 (不需要設定對應的 RoutingKey 規則)
        /// </summary>
        [JsonProperty( PropertyName = "publishDefaultExchange" )]
        public string PublishDefaultExchange { get; internal set; }

        /// <summary>事件訊息發佈的目標 Exchange 名稱
        /// </summary>
        [JsonProperty( PropertyName = "publishExchange" )]
        public string PublishExchange { get; internal set; }

        /// <summary>事件訊息發佈的 RoutingKey 前綴字串
        /// </summary>
        [JsonProperty( PropertyName = "publishRoutingKey" )]
        public string PublishRoutingKey { get; internal set; }
    }
}
