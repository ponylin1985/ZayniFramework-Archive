﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Middle.Service
{
    /// <summary>組態設定讀取器
    /// </summary>
    public static class ConfigReader
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>組態設定值物件
        /// </summary>
        private static Config _configs;

        /// <summary>JSON 組態設定值物件
        /// </summary>
        private static dynamic _jsonConfigs;

        #endregion 宣告私有的欄位


        #region 宣告公開的方法

        /// <summary>取得組態設定集合
        /// </summary>
        /// <param name="path">服務組態 serviceHostConfig.json 設定檔的相對路徑</param>
        /// <returns>組態設定值</returns>
        public static Result<Config> GetConfig( string path = "./serviceHostConfig.json" )
        {
            using ( _asyncLock.Lock() )
            {
                dynamic result = Result.CreateDynamicResult<Config>();
                var logTitle = GetLogTitle( nameof ( GetConfig ) );

                if ( _configs.IsNotNull() )
                {
                    result.Data      = _configs.CloneObject();
                    result.JConfig   = ((object)_jsonConfigs).CloneObject();
                    result.IsSuccess = true;
                    return result;
                }

                path = path.IsNullOrEmptyString( "./serviceHostConfig.json" );

                Config config;
                dynamic jconfig;

                try
                {
                    string execPath   = AppDomain.CurrentDomain.BaseDirectory;
                    string configPath = path.Contains( execPath ) ? Path.Combine( execPath, path ) : path;
                   
                    _jsonConfigs = ConfigManager.GetConfig( configPath );
                    _configs     = ((JToken)_jsonConfigs).ToObject<Config>();
                    
                    config  = _configs;
                    jconfig = _jsonConfigs;
                }
                catch ( Exception ex )
                {
                    result.Message = $"Load serviceHostConfig.json file settings occur exception. Config path: {path}. {ex.ToString()}";
                    Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, eventTitle: logTitle, message: result.Message, loggerName: "ServiceHostTrace" );
                    ConsoleLogger.LogError( $"{logTitle} {result.Message}" );
                    return result;
                }

                if ( _configs.IsNull() )
                {
                    result.Message = $"Load serviceHostConfig.json file settings fail. Configs are null.";
                    Logger.WriteErrorLog( nameof ( ConfigReader ), result.Message, logTitle, "ServiceHostTrace" );
                    ConsoleLogger.LogError( $"{logTitle} {result.Message}" );
                    return result;
                }

                result.Data      = config;
                result.JConfig   = jconfig;
                result.Success   = true;
                result.IsSuccess = true;
                return result;
            }
        }

        /// <summary>取得服務託管 Host 的設定名稱
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <returns>服務託管 Host 的設定名稱</returns>
        public static string GetHostName( string serviceName )
        {
            var logTitle = GetLogTitle( nameof ( GetHostName ) );
            ServiceHostConfig config = null;


            try
            {
                config = GlobalContext.Config.ServiceHosts.Where( s => s.ServiceName == serviceName )?.FirstOrDefault();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, logTitle, $"Get remote service host name occur excepion. serviceName: {serviceName}.", "ServiceHostTrace" );
                ConsoleLogger.LogError( $"{logTitle} Get remote service host name occur excepion. serviceName: {serviceName}. {ex.ToString()}" );
                return null;
            }

            if ( config.IsNull() )
            {
                Logger.WriteErrorLog( nameof ( ConfigReader ), $"Get remote service host name error. Can not found '{serviceName}' service host in serviceHostConfig.json.", logTitle, "ServiceHostTrace" );
                ConsoleLogger.LogError( $"{logTitle} Get remote service host name error. Can not found '{serviceName}' service host in serviceHostConfig.json." );
                return null;
            }

            if ( config.HostName.IsNullOrEmpty() )
            {
                switch ( config.RemoteHostType )
                {
                    case "TCPSocket":
                        return GlobalContext.Config.TCPSocketSettings.DefaultHost;

                    case "WebSocket":
                        return GlobalContext.Config.WebSocketSettings.DefaultHost;

                    case "RabbitMQ":
                        return GlobalContext.Config.RabbitMQSettings.DefaultHost;

                    default:
                        return null;
                }
            }

            return config.HostName;
        }

        /// <summary>取得 WebSocket 服務 Host 的組態設定
        /// </summary>
        /// <param name="hostName">連線通訊 Host 設定名稱</param>
        /// <returns>WebSocket 服務 Host 的組態設定</returns>
        public static WebSocketProtocolConfig GetWebSocketHostConfig( string hostName )
        {
            var logTitle = GetLogTitle( nameof ( GetWebSocketHostConfig ) );

            try
            {
                return GlobalContext.Config.WebSocketSettings.Hosts.Where( q => q.HostName == hostName ).FirstOrDefault();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ConfigReader ), ex, logTitle, $"Get websocket host config in serviceHostConfig.json occur excepion. HostName: {hostName}.", "ServiceHostTrace" );
                ConsoleLogger.LogError( $"{logTitle} Get websocket host config in serviceHostConfig.json occur excepion. HostName: {hostName}. {ex.ToString()}" );
                return null;
            }
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>取得日誌的標題
        /// </summary>
        /// <param name="methodName">方法名稱</param>
        /// <returns>日誌的標題</returns>
        private static string GetLogTitle( string methodName ) => $"{nameof ( ConfigReader )}.{methodName}";

        #endregion 宣告私有的方法
    }
}
