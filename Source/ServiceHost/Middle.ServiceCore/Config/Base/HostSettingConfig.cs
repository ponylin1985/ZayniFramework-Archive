﻿using Newtonsoft.Json;


namespace ZayniFramework.Middle.Service
{
    /// <summary>通訊託管組態設定基底
    /// </summary>
    public abstract class HostSettingConfig
    {
        /// <summary>預設採用的 Host 設定名稱
        /// </summary>
        [JsonProperty( PropertyName = "defaultHost" )]
        public string DefaultHost { get; internal set; }
    }
}
