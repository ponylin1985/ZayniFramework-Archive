﻿using Newtonsoft.Json;
using System;


namespace ZayniFramework.Middle.Service
{
    /// <summary>通訊協定設定組態基底
    /// </summary>
    [Serializable()]
    public abstract class ProtocolConfig
    {
        /// <summary>連線通訊設定名稱
        /// </summary>
        [JsonProperty( PropertyName = "hostName" )]
        public string HostName { get; internal set; }
    }
}
