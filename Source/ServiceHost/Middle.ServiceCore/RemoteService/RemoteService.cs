﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>遠端服務類別
    /// </summary>
    public sealed partial class RemoteService : IRemoteService
    {
        #region 宣告私有的欄位

        /// <summary>服務動作日誌紀錄員
        /// </summary>
        private ServiceActionLogger _actionLog;

        #endregion 宣告私有的欄位


        #region 宣告內部的屬性

        /// <summary>服務的設定名稱
        /// </summary>
        internal string ServiceName { get; private set; }

        #endregion 宣告內部的屬性


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        public RemoteService( string serviceName )
        {
            ServiceName = serviceName;
            _actionLog  = new ServiceActionLogger( serviceName );
        }

        /// <summary>解構子
        /// </summary>
        ~RemoteService()
        {
            ServiceName = null;
            _actionLog  = null;
        }

        #endregion 宣告建構子


        #region 實作 IRemoteService 介面

        /// <summary>執行遠端服務動作
        /// </summary>
        /// <param name="request">遠端服務請求包裹</param>
        /// <returns>遠端服務回應包裹</returns>
        public ResponsePackage Execute( RequestPackage request )
        {
            #region 初始化回應包裹

            var result = new ResponsePackage()
            {
                RequestId  = request?.RequestId,
                ActionName = request?.ActionName,
                Success    = false,
                Data       = null
            };

            #endregion 初始化回應包裹

            #region 驗證請求包裹

            var v = Validator.Validate( request );

            if ( !v.Success )
            {
                request.RequestId  = request?.RequestId?.IsNullOrEmptyString( "_InvalidReqId" );
                request.ActionName = request?.ActionName.IsNullOrEmptyString( "_InvalidActionName" );
                result.RequestId   = request?.RequestId.IsNullOrEmptyString( "_InvalidReqId" );
                result.ActionName  = request?.ActionName.IsNullOrEmptyString( "_InvalidActionName" );
                result.Code        = StatusCode.INVALID_REQUEST;
                result.Message     = $"Invalid remote request. {v.Message}";
                _actionLog.Log( request );
                _actionLog.Log( result );
                return result;
            }

            #endregion 驗證請求包裹

            #region 取得服務動作

            var action = ServiceActionContainerManager.GetServiceAction( ServiceName, request.ActionName );

            if ( action.IsNull() )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid remote request. Service name: {ServiceName}. Unknow action: {request.ActionName}.";
                _actionLog.Log( request );
                _actionLog.Log( result );
                Logger.WriteErrorLog( this, result.Message, nameof ( Execute ), loggerName: "ServiceHostTrace" );
                return result;
            }

            #endregion 取得服務動作

            #region 設定服務動作的請求參數

            try
            {
                string requestData = request.Data;
                requestData.IsNotNullOrEmpty( d => action.RequestData = JsonSerializer.Deserialize( request.Data, action.RequestType ) );
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, "Set request data to service action occur exception.", "ServiceHostTrace" );
                _actionLog.Log( request );
                _actionLog.Log( result );
                return result;
            }

            #endregion 設定服務動作的請求參數

            #region 執行請求參數的資料驗證

            if ( action.RequestData.IsNotNull() && action.RequestValidation )
            {
                var d = Validator.Validate( action.RequestData );

                if ( !d.Success )
                {
                    result.Code    = StatusCode.INVALID_REQUEST;
                    result.Message = $"Invalid remote request. {d.Message}";
                    _actionLog.Log( request );
                    _actionLog.Log( result );
                    Logger.WriteErrorLog( this, result.Message, nameof ( Execute ), loggerName: "ServiceHostTrace" );
                    return result;
                }
            }

            #endregion 執行請求參數的資料驗證

            #region 執行服務動作

            _actionLog.Log( request );

            try
            {
                action.Execute();
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"Execute service action occur exception. ActionName: {request.ActionName}, RequestId: {request.RequestId}.", "ServiceHostTrace" );
                _actionLog.Log( result );
                return result;
            }

            #endregion 執行服務動作

            #region 設定動作執行後的回傳值

            string json = null;

            try
            {
                var response = action.ResponseData;
                response.IsNotNull( r => json = JsonSerializer.Serialize( response ) );
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"Set response data of service action occur exception.", "ServiceHostTrace" );
                _actionLog.Log( result );
                return result;
            }

            #endregion 設定動作執行後的回傳值

            #region 設定回應包裹

            result.Data    = json;
            result.Code    = StatusCode.ACTION_SUCCESS;
            result.Success = true;

            _actionLog.Log( result );

            #endregion 設定回應包裹

            return result;
        }

        /// <summary>接收到服務事件訊息的處理動作
        /// </summary>
        /// <param name="messagePackage">事件的訊息資料包裹</param>
        /// <returns>執行結果</returns>
        public Result Execute( MessagePackage messagePackage )
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 驗證請求包裹

            messagePackage.ServiceName = messagePackage.ServiceName.IsNullOrEmptyString( ServiceName );

            var v = Validator.Validate( messagePackage );

            if ( !v.Success )
            {
                messagePackage.MessageId  = messagePackage?.MessageId?.IsNullOrEmptyString( "_InvalidMessageId" );
                messagePackage.ActionName = messagePackage?.ActionName.IsNullOrEmptyString( "_InvalidActionName" );
                result.Code               = StatusCode.INVALID_REQUEST;
                result.Message            = $"Invalid remote message publish. {v.Message}";
                ReceiveMessagePackageLog( messagePackage );
                return result;
            }

            #endregion 驗證請求包裹

            #region 取得服務動作

            var action = ServiceActionContainerManager.GetServiceAction( ServiceName, messagePackage.ActionName );

            if ( action.IsNull() )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid remote message publish. Service name: {ServiceName}. Unknow action: {messagePackage.ActionName}.";
                ReceiveMessagePackageLog( messagePackage );
                Logger.WriteErrorLog( this, result.Message, nameof ( Execute ), loggerName: "ServiceHostTrace" );
                return result;
            }

            #endregion 取得服務動作

            #region 設定服務動作的請求參數

            try
            {
                string requestData = messagePackage.Data;
                messagePackage.IsNotNull( d => action.RequestData = JsonSerializer.Deserialize( messagePackage.Data, action.RequestType ) );
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, "Set message content data to service action occur exception.", "ServiceHostTrace" );
                ReceiveMessagePackageLog( messagePackage );
                return result;
            }

            #endregion 設定服務動作的請求參數

            #region 訊息內容資料驗證

            if ( action.RequestData.IsNotNull() && action.RequestValidation )
            {
                var d = Validator.Validate( action.RequestData );

                if ( !d.Success )
                {
                    result.Code    = StatusCode.INVALID_REQUEST;
                    result.Message = $"Invalid remote message publish. {d.Message}";
                    ReceiveMessagePackageLog( messagePackage );
                    Logger.WriteErrorLog( this, result.Message, nameof ( Execute ), loggerName: "ServiceHostTrace" );
                    return result;
                }
            }

            #endregion 訊息內容資料驗證

            #region 執行服務動作

            ReceiveMessagePackageLog( messagePackage );

            try
            {
                action.Execute();
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"Execute service action occur exception. ActionName: {messagePackage.ActionName}, MessageId: {messagePackage.MessageId}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 執行服務動作

            #region 設定動作執行後的回傳值

            Result r;

            try
            {
                r = action.ResponseData as Result;
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"Set response data of service action occur exception.", "ServiceHostTrace" );
                return result;
            }

            #endregion 設定動作執行後的回傳值

            #region 設定回傳值

            if ( r is null )
            {
                result.Code      = StatusCode.ACTION_SUCCESS;
                result.Success = true;
                return result;
            }

            result.Code      = r.Code.IsNullOrEmptyString( StatusCode.ACTION_SUCCESS );
            result.Message   = r.Message;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        /// <summary>發佈事件訊息
        /// </summary>
        /// <param name="messagePackage">訊息資料包裹</param>
        /// <returns>訊息發佈結果</returns>
        public Result Publish( MessagePackage messagePackage ) => Result.Create( false, code: StatusCode.INTERNAL_ERROR, message: "NotImplementation" );

        #endregion 實作 IRemoteService 介面


        #region 宣告公開的方法

        /// <summary>執行服務動作
        /// </summary>
        /// <param name="request">服務請求包裹</param>
        /// <param name="reqDTO">請求資料載體</param>
        /// <returns>回應資料載體</returns>
        public ( ResponsePackage responsePackage, object resDTO ) ExecuteServiceAction( RequestPackage request, object reqDTO ) 
        {
            #region 初始化回應包裹

            var responsePackage = new ResponsePackage()
            {
                RequestId  = request?.RequestId,
                ActionName = request?.ActionName
            };

            object resDTO = null;

            #endregion 初始化回應包裹

            #region 取得服務動作

            var action = ServiceActionContainerManager.GetServiceAction( ServiceName, request.ActionName );

            if ( action.IsNull() )
            {
                responsePackage.Code    = StatusCode.INVALID_REQUEST;
                responsePackage.Message = $"Invalid remote request. Service name: {ServiceName}. Unknow action: {request.ActionName}.";
                Logger.WriteErrorLog( this, responsePackage.Message, nameof ( Execute ), loggerName: "ServiceHostTrace" );
                return ( responsePackage: responsePackage, resDTO: resDTO );
            }

            #endregion 取得服務動作

            #region 設定服務動作的請求參數

            try
            {
                action.RequestData = reqDTO;
            }
            catch ( Exception ex )
            {
                responsePackage.Code    = StatusCode.INTERNAL_ERROR;
                responsePackage.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, "Set request data to service action occur exception.", "ServiceHostTrace" );
                return ( responsePackage: responsePackage, resDTO: resDTO );
            }

            #endregion 設定服務動作的請求參數

            #region 執行服務動作

            try
            {
                action.Execute();
            }
            catch ( Exception ex )
            {
                responsePackage.Code    = StatusCode.INTERNAL_ERROR;
                responsePackage.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"Execute service action occur exception. ActionName: {request.ActionName}, RequestId: {request.RequestId}.", "ServiceHostTrace" );
                return ( responsePackage: responsePackage, resDTO: resDTO );
            }

            #endregion 執行服務動作

            #region 設定動作執行後的回傳值

            try
            {
                if ( action.ResponseData.IsNotNull() ) 
                {
                    resDTO = action.ResponseData;
                }
            }
            catch ( Exception ex )
            {
                responsePackage.Code    = StatusCode.INTERNAL_ERROR;
                responsePackage.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"Set response data of service action occur exception.", "ServiceHostTrace" );
                return ( responsePackage: responsePackage, resDTO: resDTO );
            }

            #endregion 設定動作執行後的回傳值

            #region 設定回應包裹

            responsePackage.Code    = StatusCode.ACTION_SUCCESS;
            responsePackage.Success = true;

            #endregion 設定回應包裹

            return ( responsePackage: responsePackage, resDTO: resDTO );
        }

        /// <summary>執行服務動作
        /// </summary>
        /// <param name="request">服務請求包裹</param>
        /// <param name="reqDTO">請求資料載體</param>
        /// <returns>回應資料載體</returns>
        public async Task<( ResponsePackage responsePackage, object resDTO )> ExecuteServiceActionAsync( RequestPackage request, object reqDTO ) 
        {
            #region 初始化回應包裹

            var responsePackage = new ResponsePackage()
            {
                RequestId  = request?.RequestId,
                ActionName = request?.ActionName
            };

            object resDTO = null;

            #endregion 初始化回應包裹

            #region 取得服務動作

            var action = await ServiceActionContainerManager.GetServiceActionAsync( ServiceName, request.ActionName );

            if ( action.IsNull() )
            {
                responsePackage.Code    = StatusCode.INVALID_REQUEST;
                responsePackage.Message = $"Invalid remote request. Service name: {ServiceName}. Unknow action: {request.ActionName}.";
                Logger.WriteErrorLog( this, responsePackage.Message, nameof ( Execute ), loggerName: "ServiceHostTrace" );
                return ( responsePackage: responsePackage, resDTO: resDTO );
            }

            #endregion 取得服務動作

            #region 設定服務動作的請求參數

            try
            {
                action.RequestData = reqDTO;
            }
            catch ( Exception ex )
            {
                responsePackage.Code    = StatusCode.INTERNAL_ERROR;
                responsePackage.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, "Set request data to service action occur exception.", "ServiceHostTrace" );
                return ( responsePackage: responsePackage, resDTO: resDTO );
            }

            #endregion 設定服務動作的請求參數

            #region 執行服務動作

            try
            {
                await action.ExecuteAsync();
            }
            catch ( Exception ex )
            {
                responsePackage.Code    = StatusCode.INTERNAL_ERROR;
                responsePackage.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"Execute service action occur exception. ActionName: {request.ActionName}, RequestId: {request.RequestId}.", "ServiceHostTrace" );
                return ( responsePackage: responsePackage, resDTO: resDTO );
            }

            #endregion 執行服務動作

            #region 設定動作執行後的回傳值

            try
            {
                if ( action.ResponseData.IsNotNull() ) 
                {
                    resDTO = action.ResponseData;
                }
            }
            catch ( Exception ex )
            {
                responsePackage.Code    = StatusCode.INTERNAL_ERROR;
                responsePackage.Message = $"Internal zayni framework service error. {ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"Set response data of service action occur exception.", "ServiceHostTrace" );
                return ( responsePackage: responsePackage, resDTO: resDTO );
            }

            #endregion 設定動作執行後的回傳值

            #region 設定回應包裹

            responsePackage.Code    = StatusCode.ACTION_SUCCESS;
            responsePackage.Success = true;

            #endregion 設定回應包裹

            return ( responsePackage: responsePackage, resDTO: resDTO );
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>紀錄發佈出的訊息資料包裹
        /// </summary>
        /// <param name="messagePackage">訊息資料包裹</param>
        private void PublishMessagePackageLog( MessagePackage messagePackage ) => _actionLog.Log( 4, messagePackage );

        /// <summary>紀錄接收到的訊息資料包裹
        /// </summary>
        /// <param name="messagePackage">訊息資料包裹</param>
        private void ReceiveMessagePackageLog( MessagePackage messagePackage ) => _actionLog.Log( 3, messagePackage );

        #endregion 宣告私有的方法
    }
}
