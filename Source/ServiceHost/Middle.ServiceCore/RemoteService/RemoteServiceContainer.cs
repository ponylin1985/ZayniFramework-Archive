﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using ZayniFramework.Logging;


namespace ZayniFramework.Middle.Service
{
    /// <summary>遠端服務的管理容器
    /// </summary>
    public static class RemoteServiceContainer
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>遠端服務字典容器<para/>
        /// Key值: 服務設定名稱<para/>
        /// Value值: RemoteService 遠端服務物件個體
        /// </summary>
        private static Dictionary<string, RemoteService> _services = new Dictionary<string, RemoteService>();

        #endregion 宣告私有的欄位


        #region 宣告內部的方法

        /// <summary>檢查是否包含指定名稱的服務
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        /// <returns>是否包含指定名稱的服務</returns>
        internal static bool Contains( string serviceName ) => _services.ContainsKey( serviceName );
        
        /// <summary>取得指定名稱的遠端服務個體
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        /// <returns>遠端服務個體</returns>
        public static RemoteService Get( string serviceName )
        {
            try
            {
                if ( !Contains( serviceName ) )
                {
                    Logger.WriteErrorLog( nameof ( RemoteServiceContainer ), $"Can not get RemoteService object from container. ServiceName: {serviceName}.", nameof ( Get ), "ServiceHostTrace" );
                    return null;
                }

                return _services[ serviceName ];
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( RemoteServiceContainer ), ex, $"Get RemoteService object from container occur exception. ServiceName: {serviceName}.", "ServiceHostTrace" );
                return null;
            }
        }

        /// <summary>註冊遠端服務個體
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        /// <param name="service">遠端服務個體</param>
        /// <returns>註冊是否成功</returns>
        public static bool Add( string serviceName, RemoteService service )
        {
            try
            {
                using ( _asyncLock.Lock() )
                {
                    if ( Contains( serviceName ) )
                    {
                        return true;
                    }

                    _services.Add( serviceName, service );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( RemoteServiceContainer ), ex, $"Add RemoteService object to container occur exception. ServiceName: {serviceName}.", "ServiceHostTrace" );
                return false;
            }

            return true;
        }

        #endregion 宣告內部的方法
    }
}
