﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Text;
using ZayniFramework.Common;
using ZayniFramework.Common.Tasks;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務動作日誌紀錄員
    /// </summary>
    internal sealed class ServiceActionLogger
    {
        #region 宣告私有的欄位

        /// <summary>非同步日誌工作佇列處理器
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncWorker _worker = new AsyncWorker( 2, $"Middle.Service {nameof ( ServiceActionLogger )} AsyncWorker" );

        /// <summary>服務的組態設定
        /// </summary>
        private static readonly Config _config = GlobalContext.Config;

        /// <summary>服務設定名稱
        /// </summary>
        private string _serviceName;

        /// <summary>是否啟用服務動作日誌紀錄 (資料庫日誌紀錄)
        /// </summary>
        private bool _enableLogToDb;

        /// <summary>是否啟用服務動作日誌紀錄 (文字日誌紀錄)
        /// </summary>
        private bool _enableLogToFile;

        /// <summary>動作日誌紀錄在 ElasticSearch 服務的 ESLogger 設定名稱
        /// * 當設定值不為 Null 或空字串的情況下，會自動啟動指定的 ESLogger 日誌器進行紀錄。
        /// </summary>
        private string _esLoggerName;

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        /// <returns></returns>
        static ServiceActionLogger() => _worker.Start();

        #endregion 宣告靜態建構子


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="servcieName">服務設定名稱</param>
        internal ServiceActionLogger( string servcieName )
        {
            var cfg = GlobalContext.Config.ServiceHosts?.Where( s => s.ServiceName == servcieName )?.FirstOrDefault();

            if ( cfg.IsNull() )
            {
                cfg = GlobalContext.Config.ServiceHosts?.FirstOrDefault();
            }

            if ( cfg.IsNull() )
            {
                _enableLogToDb   = false;
                _enableLogToFile = false;
                return;
            }

            _serviceName     = servcieName;
            _enableLogToDb   = cfg.EnableActionLogToDB;
            _enableLogToFile = cfg.EnableActionLogToFile;
            _esLoggerName    = cfg.ESLoggerName;
        }

        #endregion 宣告建構子


        #region 宣告內部的方法

        /// <summary>紀錄請求動作日誌
        /// </summary>
        /// <param name="requestPackage">服務請求包裹</param>
        internal void Log( RequestPackage requestPackage )
        {
            string data   = requestPackage.Data.IsNotNullOrEmpty() ? JValue.Parse( requestPackage.Data ).ToString( Formatting.Indented ) : string.Empty;
            var sbMessage = new StringBuilder();
            sbMessage.AppendLine( $"Service host receive remote request." );
            sbMessage.AppendLine( $"  - RequestId: {requestPackage.RequestId}" );
            sbMessage.AppendLine( $"  - ActionName: {requestPackage.ActionName}" );
            sbMessage.AppendLine( $"  - Data: {data}" );
            sbMessage.AppendLine( $"  - RequestTime: {requestPackage.RequestTime}" );
            requestPackage.ServiceName.IsNullOrEmpty( () => requestPackage.ServiceName = _serviceName );
            Log( sbMessage.ToString(), ConsoleColor.Green, async () => await new ServiceActionLogRepo().LogAsync( requestPackage ), async () => await new ServiceActionESLogger( _esLoggerName ).LogAsync( requestPackage ) );
        } 

        /// <summary>紀錄回應動作日誌
        /// </summary>
        /// <param name="responsePackage">服務回應包裹</param>
        internal void Log( ResponsePackage responsePackage )
        {
            var sbMessage = new StringBuilder();
            sbMessage.AppendLine( $"Service host return remote response." );
            sbMessage.AppendLine( $"  - RequestId: {responsePackage.RequestId}" );
            sbMessage.AppendLine( $"  - ActionName: {responsePackage.ActionName}" );
            sbMessage.AppendLine( $"  - Success: {responsePackage.Success}" );
            sbMessage.AppendLine( $"  - Code: {responsePackage.Code}" );
            sbMessage.AppendLine( $"  - Data: {responsePackage.Data}" );
            sbMessage.AppendLine( $"  - Message: {responsePackage.Message}" );
            var resPackage = responsePackage.ConvertTo<ResponsePackageEntry>();
            resPackage.ServiceName = _serviceName;
            Log( sbMessage.ToString(), ConsoleColor.Cyan, async () => await new ServiceActionLogRepo().LogAsync( resPackage ), async () => await new ServiceActionESLogger( _esLoggerName ).LogAsync( resPackage ) );
        }

        /// <summary>紀錄事件訊息動作的日誌
        /// </summary>
        /// <param name="direction">通訊的方向類型，3: ReceiveMessage，4: PublishMessage。</param>
        /// <param name="messagePackage">訊息資料包裹</param>
        internal void Log( int direction, MessagePackage messagePackage )
        {
            string data   = messagePackage.Data.IsNotNullOrEmpty() ? JValue.Parse( messagePackage.Data ).ToString( Formatting.Indented ) : string.Empty;
            var sbMessage = new StringBuilder();
            sbMessage.AppendLine( $"Service host message package trace." );
            sbMessage.AppendLine( $"  - MessageId: {messagePackage.MessageId}" );
            sbMessage.AppendLine( $"  - Direction: {direction}" );
            sbMessage.AppendLine( $"  - ActionName: {messagePackage.ActionName}" );
            sbMessage.AppendLine( $"  - Data: {data}" );
            sbMessage.AppendLine( $"  - PublishTime: {messagePackage.PublishTime}" );
            Log( sbMessage.ToString(), ConsoleColor.Green, async () => await  new ServiceActionLogRepo().Log( direction, messagePackage ), async () => await new ServiceActionESLogger( _esLoggerName ).LogAsync( direction, messagePackage ) );
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>日誌紀錄
        /// </summary>
        /// <param name="message">日誌訊息</param>
        /// <param name="color">主控台日誌字體顏色</param>
        /// <param name="dbLogAction">資料庫日誌紀錄的動作委派</param>
        /// <param name="esLogAction">ElasticSearch 日誌紀錄的動作委派</param>
        private void Log( string message, ConsoleColor color, Action dbLogAction, Action esLogAction )
        {
            Check.IsTrue( _enableLogToDb, () => _worker.Enqueue( dbLogAction ) );
            Check.IsTrue( _enableLogToFile, () => Logger.WriteInformationLog( nameof ( ServiceActionLogger ), message, "Remote Service Action Trace", "ServiceHostActionLog" ) );
            _esLoggerName.IsNotNullOrEmpty( i => _worker.Enqueue( esLogAction ) );
        }

        #endregion 宣告私有的方法
    }
}
