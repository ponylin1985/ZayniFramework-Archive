﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;
using ZayniFramework.Common.ORM;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務動作日誌紀錄的資料模型 (ORM對應: FS_SERVICE_HOST_ACTION_LOG)
    /// </summary>
    [Serializable()]
    [MappingTable( TableName = "FS_SERVICE_HOST_ACTION_LOG" )]
    internal sealed class ServiceActionLogModel
    {
        /// <summary>日誌紀錄流水號
        /// </summary>
        [JsonProperty( PropertyName = "LOG_SRNO" )]
        [TableColumn( ColumnName = "LOG_SRNO", IsPrimaryKey = true, OrderBy = true, OrderByDesc = true )]
        public long LogSrNo { get; set; }

        /// <summary>動作請求代碼
        /// </summary>
        [JsonProperty( PropertyName = "REQUEST_ID" )]
        [TableColumn( ColumnName = "REQUEST_ID", IsAllowNull = true, DefaultValue = null )]
        public string RequestId { get; set; }

        /// <summary>服務設定名稱
        /// </summary>
        [JsonProperty( PropertyName = "SERVICE_NAME" )]
        [TableColumn( ColumnName = "SERVICE_NAME", IsAllowNull = true, DefaultValue = null )]
        public string ServiceName { get; set; }

        /// <summary>服務主機的位址
        /// </summary>
        [JsonProperty( PropertyName = "SERVICE_HOST" )]
        [TableColumn( ColumnName = "SERVICE_HOST", IsAllowNull = true, DefaultValue = null )]
        public string ServiceHost { get; set; }

        /// <summary>動作方向<para/>
        /// 1: RPC 請求訊息。<para/>
        /// 2: RPC 回應訊息。
        /// </summary>
        [JsonProperty( PropertyName = "DIRECTION" )]
        [TableColumn( ColumnName = "DIRECTION" )]
        public int Direction { get; set; }

        /// <summary>動作名稱
        /// </summary>
        [JsonProperty( PropertyName = "ACTION_NAME" )]
        [TableColumn( ColumnName = "ACTION_NAME" )]
        public string ActionName { get; set; }

        /// <summary>資料集合，資料內容格式為 JSON 字串。
        /// </summary>
        [JsonProperty( PropertyName = "DATA_CONTENT" )]
        [TableColumn( ColumnName = "DATA_CONTENT", IsAllowNull = true, DefaultValue = null )]
        public string DataContent { get; set; }

        /// <summary>原始請求時間
        /// </summary>
        [JsonProperty( PropertyName = "REQUEST_TIME" )]
        [TableColumn( ColumnName = "REQUEST_TIME", IsAllowNull = true, DefaultValue = null )]
        public DateTime? RequestTime { get; set; }

        /// <summary>服務回應的時間
        /// </summary>
        [JsonProperty( PropertyName = "RESPONSE_TIME" )]
        [TableColumn( ColumnName = "RESPONSE_TIME", IsAllowNull = true, DefaultValue = null )]
        public DateTime? ResponseTime { get; set; }

        /// <summary>服務執行動作是否成功
        /// </summary>
        [JsonProperty( PropertyName = "IS_SUCCESS" )]
        [TableColumn( ColumnName = "IS_SUCCESS", IsAllowNull = true, DefaultValue = null )]
        public bool? IsSuccess { get; set; }

        /// <summary>服務回應的代碼
        /// </summary>
        [JsonProperty( PropertyName = "CODE" )]
        [TableColumn( ColumnName = "CODE", IsAllowNull = true, DefaultValue = null )]
        public string Code { get; set; }

        /// <summary>服務回應的訊息
        /// </summary>
        [JsonProperty( PropertyName = "MESSAGE" )]
        [TableColumn( ColumnName = "MESSAGE", IsAllowNull = true, DefaultValue = null )]
        public string Message { get; set; }

        /// <summary>日誌時間
        /// </summary>
        [JsonProperty( PropertyName = "LOG_TIME" )]
        [TableColumn( ColumnName = "LOG_TIME" )]
        public DateTime LogTime { get; set; }
    }

    /// <summary>服務動作日誌紀錄的資料模型 (ElasticSearch 日誌紀錄資料模型)
    /// </summary>
    internal sealed class service_host_action_log 
    {
        /// <summary>日誌紀錄流水號
        /// </summary>
        [JsonPropertyName( "log_srno" )]
        [JsonProperty( PropertyName = "log_srno" )]
        public string LogSrNo { get; set; }

        /// <summary>動作請求代碼
        /// </summary>
        [JsonPropertyName( "request_id" )]
        [JsonProperty( PropertyName = "request_id" )]
        public string RequestId { get; set; }

        /// <summary>服務設定名稱
        /// </summary>
        [JsonPropertyName( "service_name" )]
        [JsonProperty( PropertyName = "service_name" )]
        public string ServiceName { get; set; }

        /// <summary>服務主機的位址
        /// </summary>
        [JsonPropertyName( "service_host" )]
        [JsonProperty( PropertyName = "service_host" )]
        public string ServiceHost { get; set; }

        /// <summary>動作方向<para/>
        /// 1: RPC 請求訊息。<para/>
        /// 2: RPC 回應訊息。
        /// </summary>
        [JsonPropertyName( "direction" )]
        [JsonProperty( PropertyName = "direction" )]
        public int Direction { get; set; }

        /// <summary>動作名稱
        /// </summary>
        [JsonPropertyName( "action_name" )]
        [JsonProperty( PropertyName = "action_name" )]
        public string ActionName { get; set; }

        /// <summary>資料集合，資料內容格式為 JSON 字串。
        /// </summary>
        [JsonPropertyName( "data_content" )]
        [JsonProperty( PropertyName = "data_content" )]
        public object DataContent { get; set; }

        /// <summary>原始請求時間
        /// </summary>
        [JsonPropertyName( "request_time" )]
        [JsonProperty( PropertyName = "request_time" )]
        public DateTime? RequestTime { get; set; }

        /// <summary>服務回應的時間
        /// </summary>
        [JsonPropertyName( "response_time" )]
        [JsonProperty( PropertyName = "response_time" )]
        public DateTime? ResponseTime { get; set; }

        /// <summary>服務執行動作是否成功
        /// </summary>
        [JsonPropertyName( "is_success" )]
        [JsonProperty( PropertyName = "is_success" )]
        public bool? IsSuccess { get; set; }

        /// <summary>服務回應的代碼
        /// </summary>
        [JsonPropertyName( "code" )]
        [JsonProperty( PropertyName = "code" )]
        public string Code { get; set; }

        /// <summary>服務回應的訊息
        /// </summary>
        [JsonPropertyName( "message" )]
        [JsonProperty( PropertyName = "message" )]
        public string Message { get; set; }

        /// <summary>日誌時間
        /// </summary>
        [JsonPropertyName( "log_time" )]
        [JsonProperty( PropertyName = "log_time" )]
        public DateTime LogTime { get; set; }
    }
}
