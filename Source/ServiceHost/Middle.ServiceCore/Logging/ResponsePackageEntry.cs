using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>遠端服務回應包裹資訊
    /// </summary>
    internal class ResponsePackageEntry : ResponsePackage
    {
        /// <summary>服務的設定名稱
        /// </summary>
        /// <value></value>
        internal string ServiceName { get; set; }
    }
}