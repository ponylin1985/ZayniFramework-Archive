﻿using NeoSmart.AsyncLock;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務動作日誌資料倉儲類別
    /// </summary>
    internal sealed class ServiceActionLogRepo
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>資料存取物件
        /// </summary>
        private static readonly ServiceActionLogDao _dao = new ServiceActionLogDao();

        #endregion 宣告私有的欄位


        #region 宣告內部的方法

        /// <summary>紀錄服務動作日誌
        /// </summary>
        /// <param name="requestPackage">服務請求包裹</param>
        internal async Task LogAsync( RequestPackage requestPackage )
        {
            using ( await _asyncLock.LockAsync() )
            {
                try
                {
                    var model = ConvertTo( requestPackage );
                    await _dao.InsertLogAsync( model );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( nameof ( ServiceActionLogRepo ), ex, "Write request FS_SERVICE_HOST_ACTION_LOG to database occur exception.", "ServiceHostTrace" );
                }
            }
        }

        /// <summary>紀錄服務動作日誌
        /// </summary>
        /// <param name="responsePackage">服務回應包裹</param>
        internal async Task LogAsync( ResponsePackageEntry responsePackage )
        {
            using ( await _asyncLock.LockAsync() )
            {
                try
                {
                    var model = ConvertTo( responsePackage );
                    await _dao.InsertLogAsync( model );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( nameof ( ServiceActionLogRepo ), ex, "Write response FS_SERVICE_HOST_ACTION_LOG to database occur exception.", "ServiceHostTrace" );
                }
            }
        }

        /// <summary>紀錄服務動作日誌
        /// </summary>
        /// <param name="direction">通訊的方向類型，3: ReceiveMessage，4: PublishMessage。</param>
        /// <param name="messagePackage">訊息資料包裹</param>
        internal async Task Log( int direction, MessagePackage messagePackage )
        {
            using ( await _asyncLock.LockAsync() )
            {
                try
                {
                    var model = ConvertTo( direction, messagePackage );
                    await _dao.InsertLogAsync( model );
                }
                catch ( Exception ex )
                {
                    Logger.WriteExceptionLog( nameof ( ServiceActionLogRepo ), ex, "Write response FS_SERVICE_HOST_ACTION_LOG to database occur exception.", "ServiceHostTrace" );
                }
            }
        }

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>轉換服務動作日誌紀錄的資料模型
        /// </summary>
        /// <param name="requestPackage">服務請求包裹</param>
        /// <returns>服務動作日誌紀錄的資料模型</returns>
        private ServiceActionLogModel ConvertTo( RequestPackage requestPackage )
        {
            var model = new ServiceActionLogModel()
            {
                RequestId   = requestPackage.RequestId,
                ServiceName = requestPackage.ServiceName,
                ServiceHost = IPAddressHelper.GetLocalIPAddress(),
                Direction   = 1,
                ActionName  = requestPackage.ActionName,

                RequestTime = requestPackage.RequestTime,
                LogTime     = DateTime.UtcNow
            };

            requestPackage.Data.IsNotNullOrEmpty( d => model.DataContent = JValue.Parse( d ).ToString( Formatting.Indented ) );
            return model;
        }

        /// <summary>轉換服務動作日誌紀錄的資料模型
        /// </summary>
        /// <param name="responsePackage">服務回應包裹</param>
        /// <returns>服務動作日誌紀錄的資料模型</returns>
        private ServiceActionLogModel ConvertTo( ResponsePackageEntry responsePackage )
        {
            var nowTime = DateTime.UtcNow;

            var model = new ServiceActionLogModel()
            {
                RequestId    = responsePackage.RequestId,
                ServiceName  = responsePackage.ServiceName,
                ServiceHost  = IPAddressHelper.GetLocalIPAddress(),
                Direction    = 2,
                ActionName   = responsePackage.ActionName,

                IsSuccess    = responsePackage.Success,
                Code         = responsePackage.Code,
                Message      = responsePackage.Message,
                ResponseTime = nowTime,
                LogTime      = nowTime
            };

            int maxLengthOfMessage = 500;

            if ( responsePackage.Message.IsNotNullOrEmpty() && responsePackage.Message.Length > maxLengthOfMessage )
            {
                model.Message = responsePackage.Message.Substring( 0, maxLengthOfMessage );
            }

            responsePackage.Data.IsNotNullOrEmpty( d => model.DataContent = JValue.Parse( d ).ToString( Formatting.Indented ) );
            return model;
        }

        /// <summary>轉換服務動作日誌紀錄的資料模型
        /// </summary>
        /// <param name="direction">通訊的方向類型，3: ReceiveMessage，4: PublishMessage。</param>
        /// <param name="messagePackage"></param>
        /// <returns>服務動作日誌紀錄的資料模型</returns>
        private ServiceActionLogModel ConvertTo( int direction, MessagePackage messagePackage )
        {
            var model = new ServiceActionLogModel()
            {
                RequestId   = messagePackage.MessageId,
                ServiceName = messagePackage.ServiceName,
                ServiceHost = IPAddressHelper.GetLocalIPAddress(),
                Direction   = direction,
                ActionName  = messagePackage.ActionName,

                LogTime     = DateTime.UtcNow
            };

            switch ( direction )
            {
                case 3:
                    model.RequestTime = messagePackage.PublishTime;
                    break;

                case 4:
                    model.ResponseTime = messagePackage.PublishTime;
                    break;
            }

            messagePackage.Data.IsNotNullOrEmpty( d => model.DataContent = JValue.Parse( d ).ToString( Formatting.Indented ) );
            return model;
        }

        #endregion 宣告私有的方法
    }
}
