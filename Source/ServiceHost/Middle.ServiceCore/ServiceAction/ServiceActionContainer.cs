﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Common.Dynamic;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務動作容器
    /// </summary>
    internal sealed class ServiceActionContainer
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// * 只鎖定處理 _asyncActions 與 _asyncActionCreators 的集合的方法，即為鎖定 ServiceActionAsync 的動作處理。
        /// * 使用此 _asyncLock 的鎖定處理段落，不影響 ServiceAction 集合的操作。
        /// </summary>
        /// <returns></returns>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>多執行緒鎖定物件
        /// * 只鎖定處理 _actions 與 _actionCreators 的集合的方法，即為鎖定 ServiceAction 的動作處理。
        /// * 使用此 _lockThiz 的鎖定處理段落，不影響 ServiceActionAsync 集合的操作。
        /// </summary>
        private readonly object _lockThiz = new object();

        /// <summary>服務動作池。<para/>
        /// * Key 值: 動作名稱字串。<para/>
        /// * Value 值: 服務動作的 Runtime 型別，必須為 ServiceAction 的一種子型別。<para/>
        /// </summary>
        private readonly Dictionary<string, Type> _actions = new Dictionary<string, Type>();

        /// <summary>非同步服務動作池。<para/>
        /// * Key 值: 動作名稱字串。<para/>
        /// * Value 值: 服務動作的 Runtime 型別，必須為 ServiceActionAsync 的一種子型別。<para/>
        /// </summary>
        private readonly Dictionary<string, Type> _asyncActions = new Dictionary<string, Type>();

        /// <summary>服務動作 Creator 集合。
        /// * Key 值: 動作名稱字串。<para/>
        /// * Value 值: ServiceAction 物件對應的 ObjectCreator 物件。
        /// </summary>
        /// <returns></returns>
        private readonly Dictionary<string, ObjectCreator> _actionCreators = new Dictionary<string, ObjectCreator>();

        /// <summary>服務動作 Creator 集合。
        /// * Key 值: 動作名稱字串。<para/>
        /// * Value 值: ServiceActionAsync 物件對應的 ObjectCreator 物件。
        /// </summary>
        /// <returns></returns>
        private readonly Dictionary<string, ObjectCreator> _asyncActionCreators = new Dictionary<string, ObjectCreator>();

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceName">服務的設定名稱</param>
        internal ServiceActionContainer( string serviceName )
        {
            ServiceName = serviceName;
        }

        /// <summary>解構子
        /// </summary>
        ~ServiceActionContainer()
        {
            ServiceName = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>服務的設定名稱
        /// </summary>
        internal string ServiceName { get; private set; }

        #endregion 宣告公開的屬性


        #region 宣告內部的方法

        /// <summary>取得服務動作容器的狀態資訊
        /// </summary>
        /// <returns>取得結果</returns>
        internal Result<dynamic> GetServiceActionsInfo()
        {
            var result = Result.Create<dynamic>();

            try
            {
                var count   = _actions.Count + _asyncActions.Count;
                var actions = new List<object>();

                actions.AddRange( _actions.Values.Select( q => new { ActionName = q.Name, TypeName = q.FullName } ) );
                actions.AddRange( _asyncActions.Values.Select( q => new { ActionName = q.Name, TypeName = q.FullName } ) );

                dynamic actionsInfo = DynamicHelper.CreateDynamicObject( new
                {
                    Count   = count,
                    Actions = actions.ToList()
                } );

                result.Data    = actionsInfo;
                result.Success = true;
                return result;
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.HasException    = true;
                result.Code            = StatusCode.INTERNAL_ERROR;
                result.Message         = $"{nameof ( ServiceActionContainer )}.{nameof( GetServiceActionsInfo )} occur exception: {ex.ToString()}";
                return result;
            }
        }

        /// <summary>新增服務動作
        /// </summary>
        /// <param name="actionName">服務動作名稱</param>
        /// <param name="typeOfAction">服務動作的型別</param>
        /// <returns>新增動作結果</returns>
        internal Result Add( string actionName, Type typeOfAction )
        {
            var result = Result.Create();

            try
            {
                lock ( _lockThiz )
                {
                    if ( _actions.ContainsKey( actionName ) )
                    {
                        result.Success = true;
                        return result;
                    }

                    if ( actionName.IsNullOrEmpty() )
                    {
                        result.Code    = StatusCode.INTERNAL_ERROR;
                        result.Message = $"Add service action fail due to 'ActionName' is null or empty.";
                        Logger.WriteErrorLog( nameof ( ServiceActionContainer ), result.Message, nameof ( Add ), "ServiceHostTrace" );
                        return result;
                    }

                    if ( typeOfAction.IsNull() )
                    {
                        result.Code    = StatusCode.INTERNAL_ERROR;
                        result.Message = $"Add service action fail due to 'ActionType' is null.";
                        Logger.WriteErrorLog( nameof ( ServiceActionContainer ), result.Message, nameof ( Add ), "ServiceHostTrace" );
                        return result;
                    }

                    if ( !typeOfAction.IsSubclassOf( typeof ( ServiceAction ) ) )
                    {
                        result.Code    = StatusCode.INTERNAL_ERROR;
                        result.Message = $"Add service action fail due to 'ActionType' is not a subtype of {nameof ( ServiceAction )}.";
                        Logger.WriteErrorLog( nameof ( ServiceActionContainer ), result.Message, nameof ( Add ), "ServiceHostTrace" );
                        return result;
                    }

                    _actions.Add( actionName, typeOfAction );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ServiceActionContainer ), ex, "Add service action to container occur exception.", "ServiceHostTrace" );
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>新增服務動作
        /// </summary>
        /// <param name="actionName">服務動作名稱</param>
        /// <param name="typeOfAction">服務動作的型別</param>
        /// <returns>新增動作結果</returns>
        internal async Task<Result> AddAsync( string actionName, Type typeOfAction )
        {
            var result = Result.Create();

            try
            {
                using ( await _asyncLock.LockAsync() )
                {
                    if ( _asyncActions.ContainsKey( actionName ) )
                    {
                        result.Success = true;
                        return result;
                    }

                    if ( actionName.IsNullOrEmpty() )
                    {
                        result.Code    = StatusCode.INTERNAL_ERROR;
                        result.Message = $"Add service action fail due to 'ActionName' is null or empty.";
                        Logger.WriteErrorLog( nameof ( ServiceActionContainer ), result.Message, nameof ( AddAsync ), "ServiceHostTrace" );
                        return result;
                    }

                    if ( typeOfAction.IsNull() )
                    {
                        result.Code    = StatusCode.INTERNAL_ERROR;
                        result.Message = $"Add service action fail due to 'ActionType' is null.";
                        Logger.WriteErrorLog( nameof ( ServiceActionContainer ), result.Message, nameof ( AddAsync ), "ServiceHostTrace" );
                        return result;
                    }

                    if ( !typeOfAction.IsSubclassOf( typeof ( ServiceActionAsync ) ) )
                    {
                        result.Code    = StatusCode.INTERNAL_ERROR;
                        result.Message = $"Add service action fail due to 'ActionType' is not a subtype of {nameof ( ServiceActionAsync )}.";
                        Logger.WriteErrorLog( nameof ( ServiceActionContainer ), result.Message, nameof ( AddAsync ), "ServiceHostTrace" );
                        return result;
                    }

                    _asyncActions.Add( actionName, typeOfAction );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ServiceActionContainer ), ex, "Add async service action to container occur exception.", "ServiceHostTrace" );
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>取得服務動作。<para/>
        /// * 假若無法依照傳入的動作名稱，取得服務動作，則回傳 Null 值。<para/>
        /// * 每次從容器中取得的服務動作，皆為新的 ServiceAction instance 實體。
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <returns>服務動作</returns>
        internal ServiceAction Get( string actionName )
        {
            try
            {
                if ( !_actions.ContainsKey( actionName ) )
                {
                    return null;
                }

                var type = _actions[ actionName ];

                if ( type.IsNull() )
                {
                    return null;
                }

                if ( !type.IsSubclassOf( typeof ( ServiceAction ) ) )
                {
                    return null;
                }

                if ( _actionCreators.TryGetValue( actionName, out ObjectCreator creator ) )
                {
                    return (ServiceAction)creator.CreateInstance();
                }

                creator = new ObjectCreator( type );
                _actionCreators[ actionName ] = creator;
                return (ServiceAction)creator.CreateInstance();
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ServiceActionContainer ), ex, $"Get {actionName} service action from container occur exception.", "ServiceHostTrace" );
                return null;
            }
        }

        /// <summary>取得服務動作。<para/>
        /// * 假若無法依照傳入的動作名稱，取得服務動作，則回傳 Null 值。<para/>
        /// * 每次從容器中取得的服務動作，皆為新的 ServiceActionAsync instance 實體。
        /// </summary>
        /// <param name="actionName">動作名稱</param>
        /// <returns>服務動作</returns>
        internal Task<ServiceActionAsync> GetAsync( string actionName )
        {
            try
            {
                if ( !_asyncActions.ContainsKey( actionName ) )
                {
                    return Task.FromResult<ServiceActionAsync>( null );
                }

                var type = _asyncActions[ actionName ];

                if ( type.IsNull() )
                {
                    return Task.FromResult<ServiceActionAsync>( null );
                }

                if ( !type.IsSubclassOf( typeof ( ServiceActionAsync ) ) )
                {
                    return Task.FromResult<ServiceActionAsync>( null );
                }

                if ( _asyncActionCreators.TryGetValue( actionName, out ObjectCreator creator ) )
                {
                    return Task.FromResult( (ServiceActionAsync)creator.CreateInstance() );
                }

                creator = new ObjectCreator( type );
                _asyncActionCreators[ actionName ] = creator;
                return Task.FromResult( (ServiceActionAsync)creator.CreateInstance() );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( nameof ( ServiceActionContainer ), ex, $"Get {actionName} async service action from container occur exception.", "ServiceHostTrace" );
                return Task.FromResult<ServiceActionAsync>( null );
            }
        }

        #endregion 宣告內部的方法
    }
}
