﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>服務事件基底
    /// </summary>
    public abstract class ServiceEvent
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>非同步作業鎖定物件: Result 屬性鎖定
        /// </summary>
        private readonly AsyncLock _asyncLockResult = new AsyncLock();

        /// <summary>事件觸發 (發佈) 結果
        /// </summary>
        private Result _result = new Result();

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <param name="eventName">事件名稱</param>
        public ServiceEvent( string serviceName, string eventName ) : this( serviceName: serviceName, eventName: eventName, messageContent: null )
        {
            // pass
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <param name="eventName">事件名稱</param>
        /// <param name="messageContent">訊息資料內容</param>
        public ServiceEvent( string serviceName, string eventName, object messageContent )
        {
            EventName      = eventName;
            MessageContent = messageContent;
            ServiceNames.Add( serviceName );
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceNames">服務設定名稱集合</param>
        /// <param name="eventName">事件名稱</param>
        /// <param name="messageContent">訊息資料內容</param>
        public ServiceEvent( List<string> serviceNames, string eventName, object messageContent )
        {
            if ( serviceNames.IsNullOrEmptyList() )
            {
                throw new ArgumentNullException( $"The '{nameof ( serviceNames )}' argument can not be null or empty list." );
            }

            EventName      = eventName;
            MessageContent = messageContent;
            ServiceNames.AddRange( serviceNames );
        }

        /// <summary>多載建構子
        /// </summary>
        /// <param name="eventName">事件名稱</param>
        /// <param name="messageContent">訊息資料內容</param>
        /// <param name="serviceNames">服務設定名稱集合</param>
        public ServiceEvent( string eventName, object messageContent, params string[] serviceNames ) 
        {
            if ( serviceNames.IsNullOrEmptyArray() )
            {
                throw new ArgumentNullException( $"The '{nameof ( serviceNames )}' argument can not be null or empty list." );
            }

            EventName      = eventName;
            MessageContent = messageContent;
            ServiceNames.AddRange( serviceNames );
        }

        /// <summary>解構子
        /// </summary>
        ~ServiceEvent()
        {
            ServiceNames.Clear();
            ServiceNames   = null;
            EventName      = null;
            MessageContent = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>服務設定名稱集合複本
        /// </summary>
        public List<string> ServiceNames { get; private set; } = new List<string>();

        /// <summary>動作名稱 (事件名稱)
        /// </summary>
        public string EventName { get; set; }

        /// <summary>訊息資料內容
        /// </summary>
        public object MessageContent { get; set; }

        /// <summary>事件觸發 (發佈) 結果
        /// </summary>
        public Result Result
        {
            get
            {
                using ( _asyncLockResult.Lock() )
                {
                    return _result;
                }
            }
            set
            {
                using ( _asyncLockResult.Lock() )
                {
                    _result = value;
                }
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告保護的方法

        /// <summary>事件觸發前的處理
        /// </summary>
        protected virtual void DoExecuting()
        {
            Result = Result ?? Result.Create( success: true );
            Result.Success = true;
        }

        /// <summary>事件觸發後的處理
        /// </summary>
        protected virtual void DoExecuted()
        {
            Result = Result ?? Result.Create( success: true );
            Result.Success = true;
        }

        #endregion 宣告保護的方法


        #region 宣告公開的方法

        /// <summary>觸發事件 (非同步觸發)
        /// </summary>
        public void Fire() => Task.Factory.StartNew( FireEvent );

        /// <summary>觸發事件 (同步觸發)
        /// </summary>
        public void FireEvent()
        {
            using ( _asyncLock.Lock() )
            {
                Result = Result.Create();

                if ( ServiceNames.IsNullOrEmptyList() )
                {
                    Result.Code    = StatusCode.INTERNAL_ERROR;
                    Result.Message = $"Fire service event fail due to {nameof ( ServiceNames )} is a empty list. EventName: {EventName}.";
                    Logger.WriteErrorLog( nameof ( ServiceEvent ), Result.Message, nameof ( FireEvent ), "ServiceHostTrace" );
                    return;
                }

                try
                {
                    DoExecuting();
                }
                catch ( Exception ex )
                {
                    Result.Code    = StatusCode.INTERNAL_ERROR;
                    Result.Message = $"Fire service event fail due to {nameof ( DoExecuting )} handler occur exception. {Environment.NewLine}{ex.ToString()}";
                    Logger.WriteErrorLog( nameof ( ServiceEvent ), Result.Message, nameof ( FireEvent ), "ServiceHostTrace" );
                    return;
                }

                if ( !Result.Success )
                {
                    Logger.WriteErrorLog( nameof ( ServiceEvent ), $"Fire service event fail due to {nameof ( DoExecuting )} handler return fail. {Environment.NewLine}{Result.Message}", nameof ( FireEvent ), "ServiceHostTrace" );
                    return;
                }

                string messageId = RandomTextHelper.Create( 15 );

                foreach ( string serviceName in ServiceNames.CloneObject() )
                {
                    var messagePackage = new MessagePackage()
                    {
                        PackageType = PackageType.Publish,
                        ServiceName = serviceName,
                        MessageId   = messageId,
                        ActionName  = EventName,
                        PublishTime = DateTime.UtcNow
                    };

                    if ( MessageContent.IsNotNull() )
                    {
                        string json;

                        try
                        {
                            json = JsonSerializer.Serialize( MessageContent );
                        }
                        catch ( Exception ex )
                        {
                            Logger.WriteErrorLog( nameof ( ServiceEvent ), $"Serialize data content of message package occur exception. {Environment.NewLine}{ex.ToString()}", nameof ( FireEvent ), "ServiceHostTrace" );
                            continue;
                        }

                        messagePackage.Data = json;
                    }
            
                    var r = RemoteHostContainer.Get( serviceName );

                    if ( !r.Success )
                    {
                        Logger.WriteErrorLog( nameof ( ServiceEvent ), $"Publish service event message fail due to service name not found. ServiceName: {serviceName}.", nameof ( FireEvent ), "ServiceHostTrace" );
                        continue;
                    }

                    IRemoteHost remoteHost = r.Data;
                    Result = remoteHost.Publish( messagePackage );
                    Check.IsTrue( Result.Success, () => new ServiceActionLogger( serviceName ).Log( 4, messagePackage ) );
                }

                try
                {
                    DoExecuted();
                }
                catch ( Exception ex )
                {
                    Result.Code    = StatusCode.INTERNAL_ERROR;
                    Result.Message = $"Service event {nameof ( DoExecuted )} handler occur exception. {Environment.NewLine}{ex.ToString()}";
                    Logger.WriteErrorLog( nameof ( ServiceEvent ), Result.Message, nameof ( FireEvent ), "ServiceHostTrace" );
                    return;
                }
            }
        }

        #endregion 宣告公開的方法
    }
}
