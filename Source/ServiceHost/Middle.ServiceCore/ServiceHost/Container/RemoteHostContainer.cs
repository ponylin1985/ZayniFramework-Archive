﻿using System;
using System.Collections.Generic;
using NeoSmart.AsyncLock;
using ZayniFramework.Common;



namespace ZayniFramework.Middle.Service
{
    /// <summary>服務託管容器
    /// </summary>
    internal static class RemoteHostContainer
    {
        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>服務託管容器池
        /// </summary>
        private static readonly Dictionary<string, IRemoteHost> _hosts = new Dictionary<string, IRemoteHost>();

        /// <summary>取得指定的服務託管物件
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <returns>服務託管物件</returns>
        internal static Result<IRemoteHost> Get( string serviceName )
        {
            using ( _asyncLock.Lock() )
            {
                var result = Result.Create<IRemoteHost>();

                if ( _hosts.TryGetValue( serviceName, out IRemoteHost remoteHost ) && remoteHost.IsNotNull() )
                {
                    result.Data      = remoteHost;
                    result.Success = true;
                    return result;
                }

                var r = RemoteHostFactory.Create( serviceName );

                if ( !r.Success )
                {
                    result.Code    = r.Code;
                    result.Message = r.Message;
                    return result;
                }

                remoteHost = r.Data;

                try
                {
                    _hosts[ serviceName ] = remoteHost;
                }
                catch ( Exception ex )
                {
                    result.Message = $"Add '{serviceName}' remote host to container occur exception. {Environment.NewLine}{ex.ToString()}";
                    return result;
                }

                result.Data    = remoteHost;
                result.Success = true;
                return result;
            }
        }
    }
}
