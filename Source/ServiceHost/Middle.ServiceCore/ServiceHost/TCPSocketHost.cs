﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;
using ST = System.Timers;


namespace ZayniFramework.Middle.Service
{
    /// <summary>TCP Socket 的遠端服務託管
    /// </summary>
    [Serializable()]
    internal sealed class TCPSocketHost : IRemoteHost
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>IsHostReady 屬性的非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLockService = new AsyncLock();

        /// <summary>連線定時器的非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLockTimer = new AsyncLock();

        /// <summary>TCP Socket 連線狀態定時檢查器
        /// </summary>
        private ST.Timer _timer = new ST.Timer();

        /// <summary>遠端服務是否註冊與啟動完成
        /// </summary>
        private bool _isHostReady;

        /// <summary>TCP Socket 客戶端的 IP 白名單 (如過設定為空陣列或 Null 則代表不限制任何 IP 都可以建立連線)
        /// </summary>
        private List<string> _whitelist;

        /// <summary>TCP Socket 訊息的結束標記字串
        /// </summary>
        private string _tcpMsgEndingToken = "u0003";

        /// <summary>TCP Socket 訊息的結束標記
        /// </summary>
        private byte[] _tcpMsgEndingBytes = new byte[ 3 ];

        /// <summary>讀取 TCP Socket 訊息的緩存大小 (Byte)
        /// </summary>
        private int _bufferSize = 1024 * 5;

        /// <summary>TCP 通訊協定網路串流的監聽器
        /// </summary>
        private TcpListener _tcpListener;

        /// <summary>TCP Socket 連線客戶端連線池
        /// </summary>
        private readonly TCPSocketClientPool _clients = new TCPSocketClientPool();

        /// <summary>遠端服務
        /// </summary>
        private RemoteService _remoteService;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>解構子
        /// </summary>
        ~TCPSocketHost()
        {
            ServiceName     = null;
            HostName        = null;
            _remoteService  = null;

            _timer.Elapsed -= CheckTcpSocketConnections;
            _timer          = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>服務設定名稱
        /// </summary>
        public string ServiceName { get; internal set; }

        /// <summary>服務託管 Host 的設定名稱
        /// </summary>
        public string HostName { get; internal set; }

        /// <summary>遠端服務是否註冊與啟動完成。
        /// </summary>
        public bool IsHostReady
        {
            get
            {
                using ( _asyncLockService.Lock() )
                {
                    return _isHostReady;
                }
            }
            private set
            {
                using ( _asyncLockService.Lock() )
                {
                    _isHostReady = value;
                }
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>初始化
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        public void Initialize( string serviceName = null ) 
        {
            ServiceName     = serviceName;
            HostName        = ConfigReader.GetHostName( serviceName );
            
            _remoteService  = new RemoteService( serviceName );
            RemoteServiceContainer.Add( ServiceName, _remoteService );

            _timer.Interval = TimeSpan.FromSeconds( 5 ).TotalMilliseconds;
            _timer.Elapsed += CheckTcpSocketConnections;
        }

        /// <summary>啟動服務託管
        /// </summary>
        /// <returns>啟動結果</returns>
        public Result StartHost()
        {
            using ( _asyncLock.Lock() )
            {
                ConsoleOutputLogger.Log( $"Starting TCP socket host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"Starting TCP socket host service.", nameof ( StartHost ), "ServiceHostTrace" );

                #region 初始化回傳值

                var result = new Result()
                {
                    Success = false,
                    Message = null
                };

                #endregion 初始化回傳值

                #region 檢查 ServiceHost 的啟動狀態

                if ( IsHostReady )
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查 ServiceHost 的啟動狀態

                #region 取得 TCP Socket Host 連線設定

                var config = GlobalContext.Config.TCPSocketSettings?.Hosts?.Where( h => h.HostName == HostName )?.FirstOrDefault();

                if ( config.IsNull() )
                {
                    result.Message = $"Get serviceHostConfig.json config fail. Read TCP socket host config fail. Can not start TCP socket port listen.";
                    Logger.WriteErrorLog( this, result.Message, nameof ( StartHost ), "ServiceHostTrace" );
                    return result;
                }

                _whitelist = config.Whitelist;

                if ( config.TCPBufferSize > 0 )
                {
                    _bufferSize = 1024 * config.TCPBufferSize;
                }

                if ( config.TCPMsgEndToken.IsNotNullOrEmpty() )
                {
                    _tcpMsgEndingToken = GetEndTokenString( config.TCPMsgEndToken );
                    _tcpMsgEndingBytes = Encoding.UTF8.GetBytes( _tcpMsgEndingToken );
                }
                else
                {
                    _tcpMsgEndingToken = GetEndTokenString( "u0003" );
                }

                #endregion 取得 TCP Socket Host 連線設定

                #region 啟動 ServiceHost 服務

                try
                {
                    _tcpListener = new TcpListener( IPAddress.Any, config.TCPPort );
                    _tcpListener.Start();

                    var thread = new Thread( () => StartTcpServer() );
                    thread.Start();
                    _timer.Start();
                }
                catch ( Exception ex )
                {
                    result.Message = $"Start TCP socket host service occur exception: {ex.ToString()}";
                    Logger.WriteExceptionLog( this, ex, result.Message, "ServiceHostTrace" );
                    return result;
                }

                #endregion 啟動 ServiceHost 服務

                #region 設定回傳值

                IsHostReady      = true;
                result.Success = true;
                ConsoleOutputLogger.Log( $"Start TCP socket host service success. Listening TCP socket on localhost:{config.TCPPort}." );

                #endregion 設定回傳值

                ConsoleOutputLogger.Log( $"End of starting TCP socket host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"End of starting TCP socket host service.", nameof ( StartHost ), "ServiceHostTrace" );
                return result;
            }
        }

        /// <summary>關閉服務託管
        /// </summary>
        /// <returns>關閉結果</returns>
        public Result StopHost()
        {
            using ( _asyncLock.Lock() )
            {
                ConsoleOutputLogger.Log( $"Stoping TCP socket host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"Stoping TCP socket host service.", nameof ( StartHost ), "ServiceHostTrace" );

                #region 初始化回傳值

                var result = new Result()
                {
                    Success = false,
                    Message = null
                };

                #endregion 初始化回傳值

                #region 檢查 ServiceHost 的啟動狀態

                if ( !IsHostReady )
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查 ServiceHost 的啟動狀態

                #region 關閉 ServiceHost 服務

                try
                {
                    _timer.Stop();
                    _tcpListener.Stop();

                    _clients.Foreach( ( connectionId, client ) => 
                    {
                        TcpClient tcpClient = client.TcpClient;

                        if ( tcpClient.Connected )
                        {
                            tcpClient.Client?.Close();
                            tcpClient.Client?.Dispose();
                            client.NetworkStream?.Close();
                            client.NetworkStream.Dispose();

                            client.TcpClient     = null;
                            client.NetworkStream = null;
                        }
                    } );

                    _clients.Clear();
                }
                catch ( Exception ex )
                {
                    result.Message = $"Stop TCP socket host service occur exception: {ex.ToString()}";
                    Logger.WriteExceptionLog( this, ex, result.Message, "ServiceHostTrace" );
                    return result;
                }

                #endregion 關閉 ServiceHost 服務

                #region 設定回傳值

                IsHostReady      = false;
                result.Success = true;

                #endregion 設定回傳值

                ConsoleOutputLogger.Log( $"End of stoping TCP socket host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"End of stoping TCP socket host service.", nameof ( StartHost ), "ServiceHostTrace" );
                return result;
            }
        }

        /// <summary>執行遠端服務動作
        /// </summary>
        /// <param name="request">遠端服務請求包裹</param>
        /// <returns>遠端服務回應包裹</returns>
        public ResponsePackage Execute( RequestPackage request ) => _remoteService.Execute( request );

        /// <summary>發佈事件訊息
        /// </summary>
        /// <param name="messagePackage">訊息資料包裹</param>
        /// <returns>訊息發佈結果</returns>
        public Result Publish( MessagePackage messagePackage )
        {
            var result = Result.Create();

            if ( 0 == _clients.Count )
            {
                result.Message = $"No TcpClient connected now. No service event message was published! MessageId: {messagePackage.MessageId}, ActionName: {messagePackage.ActionName}.";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( Publish ) ), "ServiceHostTrace" );
                return result;
            }

            var success = _clients.Foreach( ( connectionId, client ) => 
            {
                var tcpClient     = client.TcpClient;
                var networkStream = client.NetworkStream;
                Send( tcpClient, networkStream, messagePackage );
            } );

            result.Success = success;
            return result;
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>取得正確的訊息結束字元字符
        /// </summary>
        /// <param name="endToken">原始 Config 組態的結束字元字符</param>
        /// <returns>結束字元字符</returns>
        private static string GetEndTokenString( string endToken )
        {
            if ( endToken.StartsWith( "u" ) )
            {
                return Convert.ToChar( int.Parse( endToken.RemoveFirstAppeared( "u" ).TrimStart( '0' ) ) ).ToString();
            }

            return endToken;
        }

        /// <summary>啟動 TCP Socket 服務的網路訊息監聽作業
        /// </summary>
        private void StartTcpServer()
        {
            while ( true )
            {
                TcpClient tcpClient = _tcpListener.AcceptTcpClient();
                var thread = new Thread( () => HandleTcpClient( tcpClient ) );
                thread.Start();
            }
        }

        /// <summary>處理 TCP Socket 客戶端連線
        /// </summary>
        /// <param name="tcpClient">TCP Socket 網路連線客戶端物件</param>
        private void HandleTcpClient( TcpClient tcpClient )
        {
            IPEndPoint remoteEndpoint = tcpClient.Client.RemoteEndPoint as IPEndPoint;

            if ( _whitelist.IsNotNullOrEmptyList() && !_whitelist.Contains( remoteEndpoint.Address.ToString() ) )
            {
                tcpClient.Client.Dispose();
                tcpClient.Close();
                Logger.WriteWarningLog( this, $"Denied remote TCP client endpoint address due to not in whitelist IP. Remote address: {remoteEndpoint.Address}:{remoteEndpoint.Port}.", nameof ( HandleTcpClient ), "ServiceHostTrace" );
                return;
            }

            Logger.WriteInformationLog( this, $"TCP client endpoint address {remoteEndpoint.Address}, port {remoteEndpoint.Port} connect successfully.", nameof ( HandleTcpClient ), "ServiceHostTrace" );
            ConsoleOutputLogger.Log( $"TCP client endpoint address {remoteEndpoint.Address}, port {remoteEndpoint.Port} connect successfully.", ConsoleColor.Green );

            tcpClient.SendBufferSize    = 1024 * 5;
            NetworkStream networkStream = tcpClient.GetStream();
            string connectionId         = Guid.NewGuid().ToString();

            var clientEndpoint = new TCPSocketClientEndpoint()
            {
                ConnectionId  = connectionId,
                ClientId      = connectionId,
                IpAddress     = remoteEndpoint.Address.ToString(),
                Port          = remoteEndpoint.Port + "",
                TcpClient     = tcpClient,
                NetworkStream = networkStream
            };

            if ( !_clients.Add( clientEndpoint.ConnectionId, clientEndpoint ) )
            {
                tcpClient.Client?.Close();
                tcpClient.Client?.Dispose();
                Logger.WriteErrorLog( this, $"Create TCP connection error. {_clients.Message}", "HandleTcpClient", "ServiceHostTrace" );
                return;
            }

            ProcessTcpSocketMessage( clientEndpoint );
        }

        /// <summary>處理 TCP Socket 網路串流訊息
        /// </summary>
        /// <param name="clientEnpoint">TCP Socket 客戶端連線端點資訊</param>
        private void ProcessTcpSocketMessage( TCPSocketClientEndpoint clientEnpoint )
        {
            while ( true )
            {
                // 改成用 Thread.Sleep() 方法之後，效能很好多，而且讀取 Socket 訊息時也比較不會有問題
                // 但是不知道為什麼，使用 SpinWait.SpintUntil() 資料量大的時候，問題一大堆...
                // Thread.Sleep( TimeSpan.FromMilliseconds( 0.5 ) );
                SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 0.5 ) );

                var tcpClient     = clientEnpoint.TcpClient;
                var networkStream = clientEnpoint.NetworkStream;

                if ( !networkStream.CanRead )
                {
                    continue;
                }

                if ( !networkStream.DataAvailable )
                {
                    continue;
                }

                try
                {
                    ProcessStreamingPackage( clientEnpoint, tcpClient, networkStream );
                }
                catch ( Exception ex )
                {
                    Logger.WriteErrorLog( this, $"TCP scoket host receive and process incoming socket message occur exception. {ex.ToString()}", "TCP Socket Host occur exception.", "ServiceHostTrace" );
                    continue;
                }
            }
        }

        /// <summary>處理接收自網路串流的通訊資料包裹
        /// </summary>
        /// <param name="clientEndpoint">TCP Socket 客戶端連線端點資訊</param>
        /// <param name="tcpClient">TCP Socket 網路連線客戶端物件</param>
        /// <param name="networkStream">TCP 網路串流</param>
        private void ProcessStreamingPackage( TCPSocketClientEndpoint clientEndpoint, TcpClient tcpClient, NetworkStream networkStream )
        {
            List<IPackage> packages = null;

            try
            {
                DateTime readBegin = DateTime.UtcNow;
                var r = Read( clientEndpoint, tcpClient, networkStream );
                DateTime readEnd = DateTime.UtcNow;
                TimeSpan spRead  = readEnd - readBegin;

                if ( spRead.Ticks > 0 )
                {
                    Logger.WriteInformationLog( this, $"TCPSocketHost Read RequestPackage: {spRead}", "TCPSocketHost Read TimeSpan", "ServiceHostTrace:TCPSocketHost" );
                }

                if ( !r.Success )
                {
                    string errorCode = r.Code.IsNullOrEmptyString( StatusCode.INTERNAL_ERROR );
                    string errorMsg  = r.Message.IsNullOrEmptyString( $"Internal runtime error." );
                    SendErrorResponsePackage( tcpClient, networkStream, errorCode, errorMsg );
                    return;
                }

                packages = r.Data;

                if ( packages.IsNullOrEmptyList() )
                {
                    return;
                }

                foreach ( var package in packages )
                {
                    Task.Factory.StartNew( () => 
                    {
                        var p = Process( tcpClient, networkStream, package );

                        if ( !p.Success )
                        {
                            Logger.WriteErrorLog( this, $"Process incoming message package of TCP socket network occur error. {p.Message}", nameof ( ProcessStreamingPackage ), "ServiceHostTrace" );
                        }
                    } );
                }
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Process message of TCP socket network stream occur exception. TCP socket client: {clientEndpoint.IpAddress}:{clientEndpoint.Port}.", "ServiceHostTrace" );
                ConsoleOutputLogger.Log( $"Process message of TCP socket network stream occur exception. TCP socket client: {clientEndpoint.IpAddress}:{clientEndpoint.Port}.", ConsoleColor.Red );
                return;
            }
        }

        /// <summary>從 TCP Socket 網路串流訊息中讀取遠端服務請求包裹
        /// </summary>
        /// <param name="clientEndpoint">TCP Socket 客戶端連線端點資訊</param>
        /// <param name="tcpClient">TCP Socket 網路連線客戶端物件</param>
        /// <param name="networkStream">TCP 網路串流</param>
        /// <returns>訊息包裹物件</returns>
        private Result<List<IPackage>> Read( TCPSocketClientEndpoint clientEndpoint, TcpClient tcpClient, NetworkStream networkStream )
        {
            #region 初始化回傳值

            var result = Result.Create<List<IPackage>>();
            var msgPackages = new List<IPackage>();

            #endregion 初始化回傳值

            lock ( clientEndpoint.ReadLock )
            {
                #region 讀取 Socket 網路串流資料

                MemoryStream msgBuffers = clientEndpoint.MsgBuffers;

                // Pony Says: 目前測下來的結果，這邊每次讀取資料的 BufferSize 如果大一點，效能確時會比較好!
                byte[] receiveBytes = null;
                int receiveBytesLength = 0;

                try
                {
                    DateTime readBegin = DateTime.UtcNow;

                    // 動態的檢查接收到的 Byte 訊息長度，決定 BufferSize 的大小!
                    int bufferSize     = tcpClient.Client.Available >= 8192 ? _bufferSize : tcpClient.Client.Available + 2;
                    receiveBytes       = new byte[ bufferSize ];
                    receiveBytesLength = networkStream.Read( receiveBytes, 0, bufferSize );

                    DateTime readEnd   = DateTime.UtcNow;
                    TimeSpan spRead    = readEnd - readBegin;

                    if ( spRead.TotalMilliseconds > 0 )
                    {
                        Logger.WriteInformationLog( this, $"TCP socket network stream read: {spRead}", "TCP Socket Read", "ServiceHostTrace:TCPSocketHost" );
                    }
                }
                catch ( IOException ioEx )
                {
                    msgBuffers?.Close();
                    msgBuffers?.Dispose();
                    clientEndpoint.MsgBuffers = new MemoryStream();
                    Logger.WriteExceptionLog( this, ioEx, $"Read RequestPackage from TCP socket network stream occur IOException.", "ServiceHostTrace" );
                    ConsoleOutputLogger.Log( $"Read RequestPackage from TCP socket network stream occur IOException.", ConsoleColor.Red );
                    return result;
                }
                catch ( SocketException socketEx )
                {
                    msgBuffers?.Close();
                    msgBuffers?.Dispose();
                    clientEndpoint.MsgBuffers = new MemoryStream();
                    Logger.WriteExceptionLog( this, socketEx, $"Read RequestPackage from TCP socket network stream occur SocketException.", "ServiceHostTrace" );
                    ConsoleOutputLogger.Log( $"Read RequestPackage from TCP socket network stream occur SocketException.", ConsoleColor.Red );
                    return result;
                }
                catch ( Exception ex )
                {
                    msgBuffers?.Close();
                    msgBuffers?.Dispose();
                    clientEndpoint.MsgBuffers = new MemoryStream();
                    Logger.WriteExceptionLog( this, ex, $"Read RequestPackage from TCP socket network stream occur exception.", "ServiceHostTrace" );
                    ConsoleOutputLogger.Log( $"Read RequestPackage from TCP socket network stream occur exception.", ConsoleColor.Red );
                    return result;
                }

                #endregion 讀取 Socket 網路串流資料

                #region 資料緩存處理

                string receiveMsg = null;
                byte[] receives   = null;

                try
                {
                    #region 寫入資料到緩存串流

                    DateTime buffersBegin = DateTime.UtcNow;
                    msgBuffers.Write( receiveBytes, 0, receiveBytesLength );

                    byte lastByte = receiveBytes[ receiveBytes.Length - 1 ];

                    if ( 0 != lastByte )
                    {
                        result.Success = true;
                        return result;
                    }

                    #endregion 寫入資料到緩存串流

                    #region 讀取緩存中所有資料

                    msgBuffers.Seek( 0, SeekOrigin.Begin );
                    receives = new byte[ msgBuffers.Length ];
                    msgBuffers.Read( receives, 0, (int)msgBuffers.Length );
                    receiveMsg = Encoding.UTF8.GetString( receives )?.Replace( "\0", string.Empty );

                    DateTime buffersEnd = DateTime.UtcNow;
                    TimeSpan spBuffer   = buffersEnd - buffersBegin;

                    if ( spBuffer.TotalMilliseconds > 0 )
                    {
                        Logger.WriteInformationLog( this, $"TCP socket process socket message buffer: {spBuffer}", "TCP Socket Read Process Buffer", "ServiceHostTrace:TCPSocketHost" );
                    }

                    #endregion 讀取緩存中所有資料
                }
                catch ( Exception ex )
                {
                    msgBuffers?.Close();
                    msgBuffers?.Dispose();
                    clientEndpoint.MsgBuffers = new MemoryStream();
                    Logger.WriteExceptionLog( this, ex, $"Read RequestPackage from TCP socket network stream occur exception. {ex.ToString()}", "ServiceHostTrace" );
                    ConsoleOutputLogger.Log( $"Read RequestPackage from TCP socket network stream occur exception.", ConsoleColor.Red );
                    return result;
                }

                #endregion 資料緩存處理

                #region 檢查訊息是否包含「結束字元」

                if ( !receiveMsg.Contains( _tcpMsgEndingToken ) )
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查訊息是否包含「結束字元」

                #region 處理訊息

                DateTime messageBegin = DateTime.UtcNow;
                bool endsWith         = receiveMsg.EndsWith( _tcpMsgEndingToken );
                string[] messages     = receiveMsg.SplitString( _tcpMsgEndingToken );
                int messagesLength    = endsWith ? messages.Length : messages.Length - 1;

                for ( int i = 0; i < messagesLength; i++ )
                {
                    string message = messages[ i ];
                    string json    = message?.Trim();

                    var r = GetMessagePackage( json );

                    if ( !r.Success )
                    {
                        Logger.WriteErrorLog( this, $"TCP socket host process incoming message fail. {r.Message}", nameof ( Read ), "ServiceHostTrace" );
                        continue;
                    }

                    IPackage package = r.Data;
                    msgPackages.Add( package );
                }

                msgBuffers?.Close();
                msgBuffers?.Dispose();
                clientEndpoint.MsgBuffers = endsWith ? new MemoryStream() : new MemoryStream( Encoding.UTF8.GetBytes( messages.LastOrDefault() ) );

                DateTime messageEnd = DateTime.UtcNow;
                TimeSpan spMessage  = messageEnd - messageBegin;

                if ( spMessage.TotalMilliseconds > 0 )
                {
                    Logger.WriteInformationLog( this, $"TCP socket process message: {spMessage}", "TCP Socket Read Process Message", "ServiceHostTrace:TCPSocketHost" );
                }

                #endregion 處理訊息

                #region 設定回傳值

                result.Data    = msgPackages;
                result.Success = true;

                #endregion 設定回傳值
            }

            return result;
        }

        /// <summary>傳送遠端服務回應包裹到 TCP Socket 網路串流
        /// </summary>
        /// <param name="tcpClient">TCP Socket 網路連線客戶端物件</param>
        /// <param name="networkStream">TCP 網路串流</param>
        /// <param name="responsePackage">遠端服務回應包裹</param>
        /// <returns>訊息是否成功寫入 TCP Socket 網路串流</returns>
        private bool Send( TcpClient tcpClient, NetworkStream networkStream, ResponsePackage responsePackage )
        {
            if ( tcpClient.IsNull() )
            {
                Logger.WriteWarningLog( this, $"Can not send ResponsePackage to TCP socket stream due to TcpClient object is null.", nameof ( Send ), "ServiceHostTrace" );
                return false;
            }

            if ( networkStream.IsNull() )
            {
                Logger.WriteWarningLog( this, $"Can not send ResponsePackage to TCP socket stream due to NetworkStream is null.", nameof ( Send ), "ServiceHostTrace" );
                return false;
            }

            if ( !networkStream.CanWrite )
            {
                Logger.WriteWarningLog( this, $"Can not send ResponsePackage to TCP socket stream due to NetworkStream is not writable.", nameof ( Send ), "ServiceHostTrace" );
                return false;
            }

            byte[] messageBytes = null;

            try
            {
                string json  = $"{JsonSerializer.Serialize( responsePackage )}{_tcpMsgEndingToken}";
                messageBytes = Encoding.UTF8.GetBytes( json );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Serialize the ResponsePackage object to byte[] occur excpetion.", "ServiceHostTrace" );
                ConsoleOutputLogger.Log( $"Serialize the ResponsePackage object to byte[] occur excpetion.", ConsoleColor.Red );
                return false;
            }

            try
            {
                networkStream.Write( messageBytes, 0, messageBytes.Length );
                networkStream.Flush();
            }
            catch ( IOException ioEx )
            {
                Logger.WriteExceptionLog( this, ioEx, $"Write ResponsePackage binary to TCP socket network stream occur IOExcpetion.", "ServiceHostTrace" );
                ConsoleOutputLogger.Log( $"Write ResponsePackage binary to TCP socket network stream occur IOExcpetion.", ConsoleColor.Red );
                return false;
            }
            catch ( SocketException socketEx )
            {
                Logger.WriteExceptionLog( this, socketEx, $"Write ResponsePackage binary to TCP socket network stream occur SocketException.", "ServiceHostTrace" );
                ConsoleOutputLogger.Log( $"Write ResponsePackage binary to TCP socket network stream occur SocketException.", ConsoleColor.Red );
                return false;
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Write ResponsePackage binary to TCP socket network stream occur exception.", "ServiceHostTrace" );
                ConsoleOutputLogger.Log( $"Write ResponsePackage binary to TCP socket network stream occur exception.", ConsoleColor.Red );
                return false;
            }

            return true;
        }

        /// <summary>傳送事件訊息至 TCP 客戶端
        /// </summary>
        /// <param name="tcpClient">TCP Socket 網路連線客戶端物件</param>
        /// <param name="networkStream">TCP 網路串流</param>
        /// <param name="messagePackage">訊息資料包裹</param>
        /// <returns>訊息包裹是否成功寫入 TCP Socket 網路串流</returns>
        private bool Send( TcpClient tcpClient, NetworkStream networkStream, MessagePackage messagePackage )
        {
            if ( tcpClient.IsNull() )
            {
                Logger.WriteWarningLog( this, $"Can not send MessagePackage to TCP socket stream due to TcpClient object is null.", nameof ( Send ), "ServiceHostTrace" );
                return false;
            }

            if ( networkStream.IsNull() )
            {
                Logger.WriteWarningLog( this, $"Can not send MessagePackage to TCP socket stream due to NetworkStream is null.", nameof ( Send ), "ServiceHostTrace" );
                return false;
            }

            if ( !networkStream.CanWrite )
            {
                Logger.WriteWarningLog( this, $"Can not send MessagePackage to TCP socket stream due to NetworkStream is not writable.", nameof ( Send ), "ServiceHostTrace" );
                return false;
            }

            byte[] messageBytes = null;

            try
            {
                string json  = $"{JsonSerializer.Serialize( messagePackage )}{_tcpMsgEndingToken}";
                messageBytes = Encoding.UTF8.GetBytes( json );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Serialize the MessagePackage object to byte[] occur excpetion.", "ServiceHostTrace" );
                ConsoleOutputLogger.Log( $"Serialize the MessagePackage object to byte[] occur excpetion.", ConsoleColor.Red );
                return false;
            }

            try
            {
                networkStream.Write( messageBytes, 0, messageBytes.Length );
                networkStream.Flush();
            }
            catch ( IOException ioEx )
            {
                Logger.WriteExceptionLog( this, ioEx, $"Write MessagePackage binary to TCP socket network stream occur IOExcpetion.", "ServiceHostTrace" );
                ConsoleOutputLogger.Log( $"Write MessagePackage binary to TCP socket network stream occur IOExcpetion.", ConsoleColor.Red );
                return false;
            }
            catch ( SocketException socketEx )
            {
                Logger.WriteExceptionLog( this, socketEx, $"Write MessagePackage binary to TCP socket network stream occur SocketException.", "ServiceHostTrace" );
                ConsoleOutputLogger.Log( $"Write ResponsePackage binary to TCP socket network stream occur SocketException.", ConsoleColor.Red );
                return false;
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Write MessagePackage binary to TCP socket network stream occur exception.", "ServiceHostTrace" );
                ConsoleOutputLogger.Log( $"Write MessagePackage binary to TCP socket network stream occur exception.", ConsoleColor.Red );
                return false;
            }

            return true;
        }

        /// <summary>發送錯誤回應包裹
        /// </summary>
        /// <param name="tcpClient">TCP Socket 網路連線客戶端物件</param>
        /// <param name="networkStream">TCP 網路串流</param>
        /// <param name="errorCode">錯誤代碼</param>
        /// <param name="errorMsg">錯誤訊息</param>
        /// <param name="requestPackage">原始請求包裹</param>
        private void SendErrorResponsePackage( TcpClient tcpClient, NetworkStream networkStream, string errorCode, string errorMsg, RequestPackage requestPackage = null )
        {
            var responsePackage = new ResponsePackage()
            {
                Code      = errorCode,
                Message   = errorMsg,
                Success = false
            };

            requestPackage.IsNotNull( req => 
            {
                responsePackage.RequestId  = requestPackage?.RequestId;
                responsePackage.ActionName = requestPackage?.ActionName;
            } );

            Send( tcpClient, networkStream, responsePackage );
        }

        /// <summary>取得通訊資料包裹
        /// </summary>
        /// <param name="json">通訊 JSON 資料字串</param>
        /// <returns>取得結果</returns>
        private Result<IPackage> GetMessagePackage( string json )
        {
            var result = Result.Create<IPackage>();

            IPackage messagePackage;

            try
            {
                var package     = (JsonElement)JsonSerializer.Deserialize( json, typeof ( object ) );
                var packageType = package.GetProperty( "packageType" ).GetString();

                switch ( packageType )
                {
                    case PackageType.Publish:
                        messagePackage = JsonSerializer.Deserialize<MessagePackage>( json );
                        break;

                    default:
                        messagePackage = JsonSerializer.Deserialize<RequestPackage>( json );
                        break;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Deserialize receive JSON data to {nameof ( IPackage )} object occur exception. {Environment.NewLine}{ex.ToString()}";
                return result;
            }

            if ( messagePackage.IsNull() )
            {
                result.Message = $"Deserialize receive JSON data fail due to result object is null.";
                return result;
            }

            result.Data    = messagePackage;
            result.Success = true;
            return result;
        }

        /// <summary>處理訊息包裹
        /// </summary>
        /// <param name="tcpClient">TCP Socket 網路連線客戶端物件</param>
        /// <param name="networkStream">TCP 網路串流</param>
        /// <param name="package">通訊資料包裹</param>
        /// <returns>處理結果</returns>
        private Result Process( TcpClient tcpClient, NetworkStream networkStream, IPackage package )
        {
            var result = Result.Create();

            if ( package.IsNull() )
            {
                result.Success = true;
                return result;
            }

            switch ( package )
            {
                case RequestPackage requestPackage:
                    return Process( tcpClient, networkStream, requestPackage );

                case MessagePackage messagePackage:
                    return Process( messagePackage );

                default:
                    return Process( tcpClient, networkStream, (RequestPackage)package );
            }
        }

        /// <summary>處理客戶端的請求包裹
        /// </summary>
        /// <param name="tcpClient">TCP Socket 網路連線客戶端物件</param>
        /// <param name="networkStream">TCP 網路串流</param>
        /// <param name="requestPackage">服務請求包裹</param>
        /// <returns>處理結果</returns>
        private Result Process( TcpClient tcpClient, NetworkStream networkStream, RequestPackage requestPackage )
        {
            var result = Result.Create();

            ResponsePackage responsePackage = Execute( requestPackage );

            if ( responsePackage.IsNull() )
            {
                result.Message = $"Execute remote request package retrive empty ResponsePackage. RequestId: {requestPackage?.RequestId}. ActionName: {requestPackage?.ActionName}.";
                SendErrorResponsePackage( tcpClient, networkStream, StatusCode.INTERNAL_ERROR, $"Execute remote request package retrive empty ResponsePackage. RequestId: {requestPackage?.RequestId}. ActionName: {requestPackage?.ActionName}.", requestPackage );
                return result;
            }
            
            if ( !Send( tcpClient, networkStream, responsePackage ) )
            {
                result.Message = $"Send TCP socket message of ResponsePackage fail. RequestId: {requestPackage?.RequestId}. ActionName: {requestPackage?.ActionName}.";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>處理客戶端觸發的事件訊息包裹
        /// </summary>
        /// <param name="messagePackage">通訊訊息資料包裹</param>
        /// <returns>處理結果</returns>
        private Result Process( MessagePackage messagePackage ) => _remoteService.Execute( messagePackage );

        /// <summary>定時檢查客戶端 TCP Socket 連線
        /// </summary>
        /// <param name="sender">事件觸發物件</param>
        /// <param name="e">定時器事件參數</param>
        private void CheckTcpSocketConnections( object sender, ST.ElapsedEventArgs e )
        {
            using ( _asyncLockTimer.Lock() )
            {
                if ( 0 == _clients.Count )
                {
                    return;
                }

                var connections = _clients.Where( q => !IsConnectionAlive( q.Value.TcpClient ) );

                foreach ( var connection in connections )
                {
                    string connectionId = connection.Key;
                    var tcpClient = connection.Value.TcpClient;

                    if ( !_clients.Remove( connectionId ) )
                    {
                        Logger.WriteErrorLog( this, $"Remove broken connectionId from TCP connecion pool fail. {_clients.Message}", "TCP socket connection check timer.", "ServiceHostTrace" );
                        return;
                    }

                    IPEndPoint remoteEndpoint = tcpClient.Client.RemoteEndPoint as IPEndPoint;
                    ConsoleOutputLogger.Log( $"TCP socket connection is broken. Client endpoint host: {remoteEndpoint.Address}, client endpoint port: {remoteEndpoint.Port}.", ConsoleColor.Green );
                    Logger.WriteInformationLog( this, $"TCP socket connection is broken. Client endpoint host: {remoteEndpoint.Address}, client endpoint port: {remoteEndpoint.Port}.", "TCP socket connection check timer.", "ServiceHostTrace" );
                }
            }
        }

        /// <summary>檢查目前的 TCP Socket 連線是否正常連線中
        /// </summary>
        /// <returns>TCP Socket 連線是否正常</returns>
        private bool IsConnectionAlive( TcpClient tcpClient )
        {
            try
            {
                if ( tcpClient.IsNull() )
                {
                    return false;
                }

                if ( !tcpClient.Connected )
                {
                    return false;
                }

                if ( tcpClient.Client.Poll( 0, SelectMode.SelectRead ) )
                {
                    if ( 0 == tcpClient.Client.Receive( new byte[ 1 ], SocketFlags.Peek ) )
                    {
                        return false;
                    }
                }
            }
            catch ( Exception ex )
            {
                IPEndPoint remoteEndpoint = tcpClient.Client.RemoteEndPoint as IPEndPoint;
                Logger.WriteWarningLog( this, $"Check TCP socket connection occur exception. Remote client host IP address: {remoteEndpoint?.Address}:{remoteEndpoint?.Port}. {Environment.NewLine}{ex.ToString()}", nameof ( IsConnectionAlive ), "ServiceHostTrace" );
                return false;
            }

            return true;
        }

        #endregion 宣告私有的方法
    }
}
