﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace ZayniFramework.Middle.Service
{
    /// <summary>遠端服務託管物件的工廠
    /// </summary>
    internal static class RemoteHostFactory
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>遠端自我裝載服務種類的容器池<para/>
        /// Key 值: IRemoteHost 的種類名稱 (RemoteHostType)<para/>
        /// Value 值: IRemoteHost 的型別
        /// </summary>
        /// <returns></returns>
        private static readonly Dictionary<string, Type> _remoteHostTypes = new Dictionary<string, Type>();

        #endregion 宣告私有的欄位


        #region 宣告靜態建構子

        /// <summary>靜態建構子
        /// </summary>
        static RemoteHostFactory() 
        {
            using ( _asyncLock.Lock() )
            {
                _remoteHostTypes.Add( "TCPSocket", typeof ( TCPSocketHost ) );
                _remoteHostTypes.Add( "WebSocket", typeof ( WebSocketHost ) );
                _remoteHostTypes.Add( "RabbitMQ", typeof ( RabbitMQHost ) );
            }
        }

        #endregion 宣告靜態建構子


        #region 宣告內部的方法

        /// <summary>註冊 IRemoteHost 型別到容器中
        /// </summary>
        /// <param name="remoteHostTypeName">自我裝載服務的種類名稱</param>
        /// <typeparam name="TRemoteHost">IRemoteHost 自我裝載服務的泛型</typeparam>
        /// <returns>註冊結果</returns>
        internal static IResult RegisterRemoteHostType<TRemoteHost>( string remoteHostTypeName ) 
            where TRemoteHost : IRemoteHost
        {
            var result = Result.Create();

            try
            {
                using ( _asyncLock.Lock() )
                {
                    if ( remoteHostTypeName.IsNullOrEmpty() )
                    {
                        result.Message = $"The {nameof ( remoteHostTypeName )} can not be null.";
                        Logger.WriteErrorLog( nameof ( RemoteHostFactory ), result.Message, nameof ( RegisterRemoteHostType ), "ServiceHostTrace" );
                        return result;
                    }

                    if ( new string[] { "TCPSocket", "WebSocket", "RabbitMQ" }.Contains( remoteHostTypeName ) )
                    {
                        result.Success = true;
                        return result;
                    }

                    if ( _remoteHostTypes.ContainsKey( remoteHostTypeName ) )
                    {
                        result.Message = $"Duplicate {nameof ( remoteHostTypeName )} in container. remoteHostTypeName: {remoteHostTypeName}.";
                        Logger.WriteErrorLog( nameof ( RemoteHostFactory ), result.Message, nameof ( RegisterRemoteHostType ), "ServiceHostTrace" );
                        return result;
                    }

                    _remoteHostTypes.Add( remoteHostTypeName, typeof ( TRemoteHost ) );
                }    
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.Message = $"Register remote host type into container occur exception. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( nameof ( RemoteHostFactory ), ex, result.Message, "ServiceHostTrace" );
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>建立遠端自我裝載服務物件
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <returns>建立遠端自我裝載服務物件的結果</returns>
        internal static Result<IRemoteHost> Create( string serviceName = "" )
        {
            var result = Result.Create<IRemoteHost>();
            IRemoteHost remoteHost = null;

            try
            {
                using ( _asyncLock.Lock() )
                {
                    Config config = GlobalContext.Config;

                    if ( config.IsNull() )
                    {
                        Logger.WriteErrorLog( nameof ( RemoteHostFactory ), $"Create remote host fail due to config error.", nameof ( Create ), "ServiceHostTrace" );
                        return result;
                    }

                    var svrConfig = config.ServiceHosts?.Where( s => s.ServiceName == serviceName )?.FirstOrDefault() ?? config.ServiceHosts?.FirstOrDefault();

                    if ( svrConfig.IsNull() )
                    {
                        Logger.WriteErrorLog( nameof ( RemoteHostFactory ), $"Create remote host fail due to config error.", nameof ( Create ), "ServiceHostTrace" );
                        return result;
                    }

                    if ( _remoteHostTypes.TryGetValue( svrConfig.RemoteHostType, out Type remoteHostType ) )
                    {
                        remoteHost = (IRemoteHost)Activator.CreateInstance( remoteHostType );
                        remoteHost.Initialize( serviceName );    
                    }
                    else
                    {
                        remoteHost = new TCPSocketHost();
                        remoteHost.Initialize( serviceName );
                    }
                }
            }
            catch ( Exception ex )
            {
                result.ExceptionObject = ex;
                result.Message = $"Create RemoteHost object occur exception. ServiceName: {serviceName}.";
                Logger.WriteExceptionLog( nameof ( RemoteHostFactory ), ex, result.Message, "ServiceHostTrace" );
                return result;
            }

            result.Data    = remoteHost;
            result.Success = true;
            return result;
        }

        #endregion 宣告內部的方法
    }
}
