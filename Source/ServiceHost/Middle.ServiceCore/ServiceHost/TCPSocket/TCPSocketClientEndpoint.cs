﻿using NeoSmart.AsyncLock;
using System;
using System.IO;
using System.Net.Sockets;


namespace ZayniFramework.Middle.Service
{
    /// <summary>TCP Socket 客戶端連線端點資訊
    /// </summary>
    [Serializable()]
    internal sealed class TCPSocketClientEndpoint
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private AsyncLock _asyncLock = new AsyncLock();

        /// <summary>TCP Socket 訊息緩存
        /// </summary>
        private byte[] _msgBuffer = new byte[ 0 ];

        /// <summary>TCP Socket 訊息緩存
        /// </summary>
        private MemoryStream _msgMemoryStreamBuffer = new MemoryStream();

        #endregion 宣告私有的欄位


        #region 宣告內部的屬性

        /// <summary>TCP Socket 的連線代碼
        /// </summary>
        internal string ConnectionId { get; set; }

        /// <summary>TCP Socket 客戶端識別代碼
        /// </summary>
        internal string ClientId { get; set; }

        /// <summary>TCP Socket 客戶端的 IP 位址
        /// </summary>
        internal string IpAddress { get; set; }

        /// <summary>TCP Socket 客戶端的連接阜號
        /// </summary>
        internal string Port { get; set; }

        /// <summary>TCP 連線客戶端物件
        /// </summary>
        internal TcpClient TcpClient { get; set; }

        /// <summary>TCP 連線網路串流
        /// </summary>
        internal NetworkStream NetworkStream { get; set; }

        /// <summary>TCP Socket 訊息緩存，預設 1024 Bytes
        /// </summary>
        internal byte[] BytesBuffer
        {
            get
            {
                using ( _asyncLock.Lock() )
                {
                    return _msgBuffer;
                }
            }
            set
            {
                using ( _asyncLock.Lock() )
                {
                    _msgBuffer = value;
                }
            }
        }

        /// <summary>TCP Socket 訊息緩存
        /// </summary>
        internal MemoryStream MsgBuffers
        {
            get
            {
                using ( _asyncLock.Lock() )
                {
                    return _msgMemoryStreamBuffer;
                }
            }
            set
            {
                using ( _asyncLock.Lock() )
                {
                    _msgMemoryStreamBuffer = value;
                }
            }
        }

        /// <summary>寫入網路串流時多執行緒的鎖定物件
        /// </summary>
        internal object WriteLock { get; private set; } = new object();

        /// <summary>讀取網路串流時多執行緒的鎖定物件
        /// </summary>
        internal object ReadLock { get; private set; } = new object();

        #endregion 宣告內部的屬性
    }
}
