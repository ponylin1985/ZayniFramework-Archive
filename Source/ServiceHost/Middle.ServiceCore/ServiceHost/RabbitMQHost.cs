﻿using NeoSmart.AsyncLock;
using RabbitMQ.Message.Client;
using System;
using System.Linq;
using System.Text.Json;
using ZayniFramework.Common;
using ZayniFramework.Common.Tasks;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Middle.Service
{
    /// <summary>MessageBroker RabbitMQ 的遠端服務託管
    /// </summary>
    [Serializable()]
    internal sealed class RabbitMQHost : IRemoteHost
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>IsHostReady 屬性的非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLockService = new AsyncLock();

        /// <summary>遠端服務是否註冊與啟動完成
        /// </summary>
        private bool _isHostReady;

        /// <summary>MessageBroker 的客戶端代理人
        /// </summary>
        private IServiceEvent _messageClient;

        /// <summary>RabbitMQ RPC 請求的 RoutingKey 前綴字
        /// </summary>
        private string _rpcReqRoutingKeyPrefix;

        /// <summary>RabbitMQ 事件訊息的 RoutingKey 前綴字
        /// </summary>
        private string _msgRoutingKeyPrefix;

        /// <summary>RabbitMQ 事件訊息發佈的 RoutingKey 前綴字串
        /// </summary>
        private string _publishRoutingKeyPrefix;

        /// <summary>RPC 訊息處理佇列
        /// </summary>
        private readonly TaskQueue _rpcMessageQueue = new TaskQueue( false );

        /// <summary>事件訊息處理佇列
        /// </summary>
        private readonly TaskQueue _messageQueue = new TaskQueue( false );

        /// <summary>遠端服務
        /// </summary>
        private RemoteService _remoteService;

        #endregion 宣告私有的欄位


        #region 宣告建構子

        /// <summary>解構子
        /// </summary>
        ~RabbitMQHost()
        {
            ServiceName    = null;
            HostName       = null;
            _remoteService = null;
        }

        #endregion 宣告建構子


        #region 宣告公開的屬性

        /// <summary>服務設定名稱
        /// </summary>
        public string ServiceName { get; internal set; }

        /// <summary>服務託管 Host 的設定名稱
        /// </summary>
        public string HostName { get; set; }

        /// <summary>遠端服務是否註冊與啟動完成。
        /// </summary>
        public bool IsHostReady
        {
            get
            {
                using ( _asyncLockService.Lock() )
                {
                    return _isHostReady;
                }
            }
            private set
            {
                using ( _asyncLockService.Lock() )
                {
                    _isHostReady = value;
                }
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>初始化
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        public void Initialize( string serviceName = null )
        {
            ServiceName    = serviceName.IsNullOrEmptyString( GlobalContext.Config.ServiceHosts?.FirstOrDefault()?.ServiceName );
            HostName       = ConfigReader.GetHostName( serviceName );
            _remoteService = new RemoteService( serviceName );
            RemoteServiceContainer.Add( ServiceName, _remoteService );
        }

        /// <summary>啟動服務託管
        /// </summary>
        /// <returns>啟動結果</returns>
        public Result StartHost()
        {
            using ( _asyncLock.Lock() )
            {
                ConsoleOutputLogger.Log( $"Starting message broker (amqp RabbitMQ) host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"Starting message broker (amqp RabbitMQ) host service.", nameof ( StartHost ), "ServiceHostTrace" );

                #region 初始化回傳值

                var result = new Result()
                {
                    Success = false,
                    Message = null
                };

                #endregion 初始化回傳值

                #region 檢查 ServiceHost 的啟動狀態

                if ( IsHostReady )
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查 ServiceHost 的啟動狀態

                #region 取得 RabbitMQ Host 連線設定

                var config = GlobalContext.Config.RabbitMQSettings?.Hosts?.Where( h => h.HostName == HostName )?.FirstOrDefault();

                if ( config.IsNull() )
                {
                    result.Message = $"Get serviceHostConfig.json config fail. Can not establish connection with MessageBroker server.";
                    Logger.WriteErrorLog( this, result.Message, nameof ( StartHost ), "ServiceHostTrace" );
                    return result;
                }

                _rpcReqRoutingKeyPrefix  = config.RpcRoutingKeyPrefix;
                _publishRoutingKeyPrefix = config.PublishRoutingKey;

                #endregion 取得 RabbitMQ Host 連線設定

                #region 啟動 ServiceHost 服務

                try
                {
                    var module = new RabbitMQModule();

                    // 作為主動訊息發佈時，預設使用的目標 Exchange，此設定不需要 RoutingKey 路由規則。
                    if ( config.PublishDefaultExchange.IsNotNullOrEmpty() )
                    {
                        module.SetDefaultExchange( config.PublishExchange );
                    }

                    // 作為主動訊息發佈時，使用設定的 Exchange 與 RoutingKey 規則進行訊息發佈。
                    if ( config.PublishExchange.IsNotNullOrEmpty() && config.PublishRoutingKey.IsNotNullOrEmpty() )
                    {
                        module.SetRoutingExchangeMapper( config.PublishRoutingKey, config.PublishExchange );
                    }
                    
                    module.SetConnectionPolicy( RabbitMQConnectionPolicy.CreatePolicy( config.MbHost, config.MbPort, config.MbUserId, config.MbPassword, config.MbVHost ) );
                    _messageClient = ServiceEventFactory.Create( module );

                    config.RpcQueue.IsNotNullOrEmpty( r => _messageClient.Listen( config.RpcQueue, ProcessRequest ) );

                    // 支援 auto-scaling 機制: 監聽指定的 Exchange 訊息，動態產生 Consumer 的 Queue 接收 Exchange 的訊息。
                    if ( config.ListenExchange.IsNotNullOrEmpty() && config.MsgQueue.IsNullOrEmpty() )
                    {
                        _messageClient.ListenExchange( config.ListenQueuePrefix, config.ListenExchange, $"{config.ListenRoutingKey}*", ProcessMessage );
                        _msgRoutingKeyPrefix = config.ListenRoutingKey;
                    }
                    // 不支援 auto-scling 機制: 直接指定監聽 ListenQueue 的訊息。
                    else if ( config.MsgQueue.IsNotNullOrEmpty() && config.ListenExchange.IsNullOrEmpty() ) 
                    {
                        _messageClient.Listen( config.MsgQueue, ProcessMessage );
                        _msgRoutingKeyPrefix = config.MsgRoutingKeyPrefix;
                    }
                }
                catch ( Exception ex )
                {
                    result.Message = $"Start RabbitMQ host service occur exception: {ex.ToString()}";
                    Logger.WriteExceptionLog( this, ex, result.Message, "ServiceHostTrace" );
                    return result;
                }

                #endregion 啟動 ServiceHost 服務

                #region 設定回傳值

                IsHostReady      = true;
                result.Success = true;

                #endregion 設定回傳值

                ConsoleOutputLogger.Log( $"End of starting message broker (amqp RabbitMQ) host service.", ConsoleColor.Yellow, true, true );
                Logger.WriteInformationLog( this, $"End of starting message broker (amqp RabbitMQ) host service.", nameof ( StartHost ), "ServiceHostTrace" );
                return result;
            }
        }

        /// <summary>關閉服務託管
        /// </summary>
        /// <returns>關閉結果</returns>
        public Result StopHost() => Result.Create( true, message: "Unable to close message broker RabbitMQ host service." );

        /// <summary>執行遠端服務動作
        /// </summary>
        /// <param name="request">遠端服務請求包裹</param>
        /// <returns>遠端服務回應包裹</returns>
        public ResponsePackage Execute( RequestPackage request ) => _remoteService.Execute( request );

        /// <summary>發佈事件訊息
        /// </summary>
        /// <param name="messagePackage">訊息資料包裹</param>
        /// <returns>訊息發佈結果</returns>
        public Result Publish( MessagePackage messagePackage )
        {
            #region 初始化回傳值

            var result = new Result()
            {
                Success = false,
                Message = null
            };

            #endregion 初始化回傳值

            #region 執行發佈事件訊息

            try
            {
                string routingKey = $"{_publishRoutingKeyPrefix}{messagePackage.ActionName}";
                _messageClient.Emit( routingKey, "application/json", messagePackage.Data, messagePackage.MessageId, messagePackage.PublishTime.ToUnixUtcTimestamp() );
            }
            catch ( Exception ex )
            {
                result.Code    = StatusCode.INTERNAL_ERROR;
                result.Message = $"Internal zayni framework service error. Publish message package occur exception. {Environment.NewLine}{ex.ToString()}";
                Logger.WriteExceptionLog( this, ex, $"Publish message package occur exception. MessageId: {messagePackage.MessageId}, ActionName: {messagePackage.ActionName}.", "ServiceHostTrace" );
                return result;
            }

            #endregion 執行發佈事件訊息

            #region 設定回傳值

            result.Code      = StatusCode.ACTION_SUCCESS;
            result.Success = true;

            #endregion 設定回傳值

            return result;
        }

        #endregion 宣告公開的方法


        #region 宣告私有的方法

        /// <summary>處理接收到的 RPC 請求訊息
        /// </summary>
        /// <param name="eventMessage">RPC 請求訊息</param>
        /// <returns></returns>
        [RabbitMQListenPolicy( ReQueue = false )]
        private bool ProcessRequest( IEventMessage eventMessage )
        {
            dynamic action    = new QueueAction( ProcessRequest );
            action.RpcMessage = eventMessage;
            _rpcMessageQueue.EnQueue( action );
            eventMessage.Act();
            return false;
        }

        /// <summary>處理接收到的 RPC 請求訊息
        /// </summary>
        /// <param name="args">參數集合</param>
        private void ProcessRequest( dynamic args )
        {
            IEventMessage rpcReqMessage = args.RpcMessage;

            string messageId = rpcReqMessage.GetMessageID();
            string eventName = rpcReqMessage.GetEvent();
            string reqData   = rpcReqMessage.GetBody();
            long   reqTime   = rpcReqMessage.GetTimestamp();

            var requestPackage = new RequestPackage()
            {
                RequestId   = messageId,
                Data        = reqData,
                RequestTime = reqTime.ToDateTimeFromUnixTimestamp()
            };

            _rpcReqRoutingKeyPrefix.IsNotNullOrEmpty( k => requestPackage.ActionName = eventName.RemoveFirstAppeared( _rpcReqRoutingKeyPrefix ) );

            ResponsePackage responsePackage;

            try
            {
                responsePackage = Execute( requestPackage );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Execute remote request occur exception. MessageId: {messageId}. EventName: {eventName}.", "ServiceHostTrace" );
                ExecuteRpcResponse( rpcReqMessage, new ResponsePackage() { ActionName = eventName, RequestId = messageId, Code = StatusCode.INTERNAL_ERROR, Message = $"Execute remote request occur exception. {ex.Message}" } );
                return;
            }

            ExecuteRpcResponse( rpcReqMessage, responsePackage );
        }

        /// <summary>執行 RPC 訊息的回應
        /// </summary>
        /// <param name="rpcReqMessage">原始 RPC 請求訊息</param>
        /// <param name="responsePackage">遠端服務回應包裹</param>
        private void ExecuteRpcResponse( IEventMessage rpcReqMessage, ResponsePackage responsePackage )
        {
            string messageId = rpcReqMessage.GetMessageID();
            string eventName = rpcReqMessage.GetEvent();

            try
            {
                string rpcResMessage = JsonSerializer.Serialize( responsePackage );
                _messageClient.ResponseRPC( rpcReqMessage, "application/json", rpcResMessage, messageId, DateTime.UtcNow.ToUnixUtcTimestamp() );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Response rpc message occur exception. MessageId: {messageId}. EventName: {eventName}.", "ServiceHostTrace" );
                return;
            }
        }

        /// <summary>處理接收到的事件訊息
        /// </summary>
        /// <param name="eventMessage">事件訊息</param>
        /// <returns></returns>
        [RabbitMQListenPolicy( ReQueue = false )]
        private bool ProcessMessage( IEventMessage eventMessage )
        {
            dynamic action  = new QueueAction( ProcessMessage );
            action.EventMsg = eventMessage;
            _messageQueue.EnQueue( action );
            eventMessage.Act();
            return false;
        }

        /// <summary>處理接收到的事件訊息
        /// </summary>
        /// <param name="args">參數集合</param>
        private void ProcessMessage( dynamic args )
        {
            IEventMessage eventMessage = args.EventMsg;

            string messageId = eventMessage.GetMessageID();
            string eventName = eventMessage.GetEvent();
            string data      = eventMessage.GetBody();
            long   pubTime   = eventMessage.GetTimestamp();

            var messagePackage = new MessagePackage()
            {
                MessageId   = messageId,
                Data        = data,
                PublishTime = pubTime.ToDateTimeFromUnixTimestamp()
            };

            _msgRoutingKeyPrefix.IsNotNullOrEmpty( k => messagePackage.ActionName = eventName.RemoveFirstAppeared( _msgRoutingKeyPrefix ) );

            try
            {
                _remoteService.Execute( messagePackage );
            }
            catch ( Exception ex )
            {
                Logger.WriteExceptionLog( this, ex, $"Execute message package action occur exception. MessageId: {messageId}. EventName: {eventName}.", "ServiceHostTrace" );
                return;
            }
        }

        #endregion 宣告私有的方法
    }
}
