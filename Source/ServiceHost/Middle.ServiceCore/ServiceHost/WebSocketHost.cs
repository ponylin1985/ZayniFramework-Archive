﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using WebSocketSharp;
using WebSocketSharp.Server;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service.Entity;
using ZLog = ZayniFramework.Logging.Logger;


namespace ZayniFramework.Middle.Service
{
    /// <summary>WebSocket 的遠端服務託管
    /// </summary>
    [Serializable()]
    internal sealed class WebSocketHost : WebSocketBehavior, IRemoteHost
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLock = new AsyncLock();

        /// <summary>IsHostReady屬性的非同步作業鎖定物件
        /// </summary>
        private readonly AsyncLock _asyncLockService = new AsyncLock();

        /// <summary>遠端服務是否註冊與啟動完成
        /// </summary>
        private bool _isHostReady;

        /// <summary>WebSocket 通訊協定服務端物件
        /// </summary>
        private WebSocketServer _websocketServer;

        /// <summary>遠端服務
        /// </summary>
        private RemoteService _remoteService;

        /// <summary>WebSocket 客戶端 IP 白名單
        /// </summary>
        private List<string> _whitelist;

        #endregion 宣告私有的欄位


        #region 宣告公開的屬性

        /// <summary>服務設定名稱
        /// </summary>
        public string ServiceName { get; internal set; }

        /// <summary>服務託管 Host 的設定名稱
        /// </summary>
        public string HostName { get; private set; }

        /// <summary>遠端服務是否註冊與啟動完成。
        /// </summary>
        public bool IsHostReady
        {
            get
            {
                using ( _asyncLockService.Lock() )
                {
                    return _isHostReady;
                }
            }
            private set
            {
                using ( _asyncLockService.Lock() )
                {
                    _isHostReady = value;
                }
            }
        }

        #endregion 宣告公開的屬性


        #region 宣告公開的方法

        /// <summary>初始化
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        public void Initialize( string serviceName = null ) 
        {
            ServiceName    = serviceName.IsNullOrEmptyString( GlobalContext.Config.ServiceHosts?.FirstOrDefault()?.ServiceName );
            HostName       = ConfigReader.GetHostName( ServiceName );
            
            _remoteService = new RemoteService( ServiceName );
            RemoteServiceContainer.Add( ServiceName, _remoteService );
        }

        /// <summary>啟動服務託管
        /// </summary>
        /// <returns>啟動結果</returns>
        public Result StartHost()
        {
            using ( _asyncLock.Lock() )
            {
                ConsoleOutputLogger.Log( $"Starting WebSocket host service.", ConsoleColor.Yellow, true, true );
                ZLog.WriteInformationLog( this, $"Starting WebSocket host service.", nameof ( StartHost ), "ServiceHostTrace" );

                #region 初始化回傳值

                var result = new Result()
                {
                    Success = false,
                    Message = null
                };

                #endregion 初始化回傳值

                #region 檢查 ServiceHost 的啟動狀態

                if ( IsHostReady )
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查 ServiceHost 的啟動狀態

                #region 取得 WebSocket Host 連線設定

                var config = GlobalContext.Config.WebSocketSettings?.Hosts?.Where( h => h.HostName == HostName )?.FirstOrDefault();

                if ( config.IsNull() )
                {
                    result.Message = $"Get serviceHostConfig.json config fail. Read WebSocket host config fail.";
                    ZLog.WriteErrorLog( this, result.Message, nameof ( StartHost ), "ServiceHostTrace" );
                    return result;
                }

                if ( config.HostBaseUrl.IsNullOrEmpty() )
                {
                    result.Message = $"Get serviceHostConfig.json config fail. webSocketHostSettings/hostBaseUrl from config is null or empty string.";
                    ZLog.WriteErrorLog( this, result.Message, nameof ( StartHost ), "ServiceHostTrace" );
                    return result;
                }

                _whitelist = config.Whitelist;

                #endregion 取得 WebSocket Host 連線設定

                #region 啟動 ServiceHost 服務

                try
                {
                    _websocketServer = new WebSocketServer( config.HostBaseUrl );
                    _websocketServer.AddWebSocketService<WebSocketHost>( "/service" );
                    _websocketServer.Start();
                }
                catch ( Exception ex )
                {
                    result.Message = $"Start WebSocket host service occur exception: {ex.ToString()}";
                    ZLog.WriteExceptionLog( this, ex, result.Message, "ServiceHostTrace" );
                    return result;
                }

                #endregion 啟動 ServiceHost 服務

                #region 設定回傳值

                IsHostReady      = true;
                result.Success = true;
                ConsoleOutputLogger.Log( $"Start WebSocket host service success. Listening WebSocket message on {config.HostBaseUrl}." );

                #endregion 設定回傳值

                ConsoleOutputLogger.Log( $"End of starting WebSocket host service.", ConsoleColor.Yellow, true, true );
                ZLog.WriteInformationLog( this, $"End of starting WebSocket host service.", nameof ( StartHost ), "ServiceHostTrace" );
                return result;
            }
        }

        /// <summary>關閉服務託管
        /// </summary>
        /// <returns>關閉結果</returns>
        public Result StopHost()
        {
            using ( _asyncLock.Lock() )
            {
                ConsoleOutputLogger.Log( $"Stoping WebSocket host service.", ConsoleColor.Yellow, true, true );
                ZLog.WriteInformationLog( this, $"Stoping WebSocket host service.", nameof ( StartHost ), "ServiceHostTrace" );

                #region 初始化回傳值

                var result = new Result()
                {
                    Success = false,
                    Message = null
                };

                #endregion 初始化回傳值

                #region 檢查 ServiceHost 的啟動狀態

                if ( !IsHostReady )
                {
                    result.Success = true;
                    return result;
                }

                #endregion 檢查 ServiceHost 的啟動狀態

                #region 關閉 ServiceHost 服務

                try
                {
                    _websocketServer.Stop();
                    _websocketServer = null;
                }
                catch ( Exception ex )
                {
                    result.Message = $"Stop WebSocket host service occur exception: {ex.ToString()}";
                    ZLog.WriteExceptionLog( this, ex, result.Message, "ServiceHostTrace" );
                    return result;
                }

                #endregion 關閉 ServiceHost 服務

                #region 設定回傳值

                IsHostReady      = false;
                result.Success = true;

                #endregion 設定回傳值

                ConsoleOutputLogger.Log( $"End of stoping WebSocket host service.", ConsoleColor.Yellow, true, true );
                ZLog.WriteInformationLog( this, $"End of stoping WebSocket host service.", nameof ( StartHost ), "ServiceHostTrace" );
                return result;
            }
        }

        /// <summary>執行遠端服務動作
        /// </summary>
        /// <param name="request">遠端服務請求包裹</param>
        /// <returns>遠端服務回應包裹</returns>
        public ResponsePackage Execute( RequestPackage request ) => RemoteServiceContainer.Get( ServiceName ).Execute( request );

        /// <summary>發佈事件訊息
        /// </summary>
        /// <param name="messagePackage">訊息資料包裹</param>
        /// <returns>訊息發佈結果</returns>
        public Result Publish( MessagePackage messagePackage )
        {
            var result = new Result()
            {
                Success = false,
                Message = null
            };

            try
            {
                string json = JsonSerializer.Serialize( messagePackage );
                _websocketServer.WebSocketServices.Broadcast( json );
            }
            catch ( Exception ex )
            {
                result.Message = $"Broadcast message package to remote clients occur exception. MessageId: {messagePackage.MessageId}, ActionName: {messagePackage.ActionName}. {Environment.NewLine}{ex.ToString()}";
                ZLog.WriteErrorLog( this, result.Message, nameof ( Publish ), "ServiceHostTrace" );
                return result;
            }

            result.Success = true;
            return result;
        }

        #endregion 宣告公開的方法


        #region 宣告保護的方法，實作 WebSocketBehavior 樣版

        /// <summary>當 WebSocket 連線成功建立的處理
        /// </summary>
        protected override void OnOpen() => base.OnOpen();

        /// <summary>當 WebSocket 連線關閉的處理
        /// </summary>
        /// <param name="e">事件參數</param>
        protected override void OnClose( CloseEventArgs e ) => base.OnClose( e );

        /// <summary>接收到 WebSocket 通訊的訊息的處理
        /// </summary>
        /// <param name="e">訊息事件參數</param>
        protected override void OnMessage( MessageEventArgs e )
        {
            string clientAddress = base.Context.UserEndPoint.Address.ToString();

            if ( !e.IsText )
            {
                return;
            }

            string json = e.Data;

            if ( json.IsNullOrEmpty() )
            {
                ZLog.WriteErrorLog( this, $"Read RequestPackage from WebSocket message fail due to retrive empty raw data.", nameof ( OnMessage ), "ServiceHostTrace" );
                return;
            }

            var g = GetMessagePackage( json );

            if ( !g.Success )
            {
                ZLog.WriteErrorLog( this, $"Get IPackage object from WebSocket message fail. {Environment.NewLine}{g.Message}", nameof ( OnMessage ), "ServiceHostTrace" );
                return;
            }

            IPackage package = g.Data;

            var r = Process( package, clientAddress );

            if ( !r.Success )
            {
                ZLog.WriteErrorLog( this, $"Process message package data from WebSocket message fail. {Environment.NewLine}{r.Message}", nameof ( OnMessage ), "ServiceHostTrace" );
                return;
            }
        }

        /// <summary>WebSocket 通訊發生異常的處理
        /// </summary>
        /// <param name="e">異常事件參數</param>
        protected override void OnError( ErrorEventArgs e )
        {
            ZLog.WriteErrorLog( this, $"{nameof ( WebSocketHost )} occur error. {Environment.NewLine}{e.Message}", nameof ( OnError ), "ServiceHostTrace" );
            e.Exception.IsNotNull( ex => ZLog.WriteExceptionLog( this, e.Exception, $"{nameof ( WebSocketHost )} occur exception. {Environment.NewLine}{e.Message}", "ServiceHostTrace" ) );
            base.OnError( e );
        }

        #endregion 宣告保護的方法，實作 WebSocketBehavior 樣版


        #region 宣告私有的方法

        /// <summary>取得通訊資料包裹
        /// </summary>
        /// <param name="json">通訊 JSON 資料字串</param>
        /// <returns>取得結果</returns>
        private Result<IPackage> GetMessagePackage( string json )
        {
            var result = Result.Create<IPackage>();

            IPackage messagePackage;

            try
            {
                var package     = (JsonElement)JsonSerializer.Deserialize( json, typeof ( object ) );
                var packageType = package.GetProperty( "packageType" ).GetString();

                switch ( packageType )
                {
                    case PackageType.Publish:
                        messagePackage = JsonSerializer.Deserialize<MessagePackage>( json );
                        break;

                    default:
                        messagePackage = JsonSerializer.Deserialize<RequestPackage>( json );
                        break;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"Deserialize receive JSON data to {nameof ( IPackage )} object occur exception. {Environment.NewLine}{ex.ToString()}";
                return result;
            }

            if ( messagePackage.IsNull() )
            {
                result.Message = $"Deserialize receive JSON data fail due to result object is null.";
                return result;
            }

            result.Data      = messagePackage;
            result.Success = true;
            return result;
        }

        /// <summary>處理訊息包裹
        /// </summary>
        /// <param name="package">通訊資料包裹</param>
        /// <param name="clientAddress">客戶端的 IP 位址</param>
        /// <returns>處理結果</returns>
        private Result Process( IPackage package, string clientAddress )
        {
            var result = Result.Create();

            if ( package.IsNull() )
            {
                result.Success = true;
                return result;
            }

            switch ( package )
            {
                case RequestPackage requestPackage:
                    return Process( requestPackage, clientAddress );

                case MessagePackage messagePackage:
                    return Process( messagePackage, clientAddress );

                default:
                    return Process( (RequestPackage)package, clientAddress );
            }
        }

        /// <summary>處理客戶端的請求包裹
        /// </summary>
        /// <param name="requestPackage">服務請求包裹</param>
        /// <param name="clientAddress">客戶端的 IP 位址</param>
        /// <returns>處理結果</returns>
        private Result Process( RequestPackage requestPackage, string clientAddress )
        {
            var result = Result.Create();

            if ( requestPackage.IsNull() )
            {
                result.Message = $"Read RequestPackage from WebSocket message fail due to RequestPackage is null.";
                ProcessInvalidRequest( requestPackage, $"Invalid remote request. Retrive RequestPackage is null." );
                return result;
            }

            if ( requestPackage.ServiceName.IsNullOrEmpty() )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid remote request. Retrive 'serviceName' is null or empty string.";
                ProcessInvalidRequest( requestPackage, $"Invalid remote request. In WebSocket remote host type protocol, the 'servcieName' can not be null or empty string. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}.", StatusCode.INVALID_REQUEST );
                return result;
            }

            ServiceName = requestPackage.ServiceName;
            HostName    = HostName.IsNullOrEmptyString( ConfigReader.GetHostName( ServiceName ), trim: true );

            var d = CheckClientAddress( clientAddress );

            if ( !d.Success )
            {
                result.Code    = d.Code;
                result.Message = d.Message;
                ProcessInvalidRequest( requestPackage, result.Message, result.Code );
                return result;
            }

            ResponsePackage responsePackage = null;

            try
            {
                responsePackage = Execute( requestPackage );
                responsePackage.PackageType = PackageType.RPCResponse;
            }
            catch ( Exception ex )
            {
                result.Message = $"Execute service action occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}. {Environment.NewLine}{ex.ToString()}";
                ProcessInvalidRequest( requestPackage, $"Execute service action occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}." );
                return result;
            }

            string json = null;

            try
            {
                json = JsonSerializer.Serialize( responsePackage );
            }
            catch ( Exception ex )
            {
                result.Message = $"Serialize ResponsePackage to JSON occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}. {Environment.NewLine}{ex.ToString()}";
                ProcessInvalidRequest( requestPackage, $"Internal exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}." );
                return result;
            }

            try
            {
                base.Send( json );
            }
            catch ( Exception ex )
            {
                result.Message = $"Write ResponsePackage to WebSocket message stream occur exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}. {Environment.NewLine}{ex.ToString()}";
                ProcessInvalidRequest( requestPackage, $"Internal exception. RequestId: {requestPackage.RequestId}, ActionName: {requestPackage.ActionName}." );
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>處理客戶端觸發的事件訊息包裹
        /// </summary>
        /// <param name="messagePackage">通訊訊息資料包裹</param>
        /// <param name="clientAddress">客戶端的 IP 位址</param>
        /// <returns>處理結果</returns>
        private Result Process( MessagePackage messagePackage, string clientAddress )
        {
            var result = Result.Create();

            if ( messagePackage.ServiceName.IsNullOrEmpty() )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid remote message publish. Retrive 'serviceName' is null or empty string.";
                return result;
            }

            ServiceName = messagePackage.ServiceName;
            HostName    = HostName.IsNullOrEmptyString( ConfigReader.GetHostName( ServiceName ), trim: true );

            var d = CheckClientAddress( clientAddress );

            if ( !d.Success )
            {
                result.Code    = d.Code;
                result.Message = d.Message;
                return result;
            }

            return _remoteService.Execute( messagePackage );
        } 

        /// <summary>檢查 WebSocket 客戶端的 IP 位址是否在白名單中
        /// </summary>
        /// <param name="clientAddress">客戶端 IP 位址字串</param>
        /// <returns>客戶端 IP 位址是否在白名單授權中</returns>
        private Result CheckClientAddress( string clientAddress )
        {
            var result = Result.Create();

            var cfg = ConfigReader.GetWebSocketHostConfig( HostName );

            if ( cfg.IsNull() )
            {
                result.Code    = StatusCode.INVALID_REQUEST;
                result.Message = $"Invalid remote request. Can not found '{HostName}' service host config.";
                return result;
            }

            if ( cfg.Whitelist.IsNotNullOrEmptyList() && !cfg.Whitelist.Contains( clientAddress ) )
            {
                result.Code    = StatusCode.UNAUTHORIZED;
                result.Message = $"Invalid remote request. Unauthorized websocket client.";
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>處理不合法的 WebSocket 遠端動作請求
        /// </summary>
        /// <param name="requestPackage">原始請求包裹</param>
        /// <param name="errorMsg">錯誤訊息</param>
        /// <param name="statusCode">錯誤狀態碼</param>
        private void ProcessInvalidRequest( RequestPackage requestPackage, string errorMsg, string statusCode = null )
        {
            statusCode = statusCode.IsNullOrEmptyString( StatusCode.INVALID_REQUEST );

            var responsePackage = new ResponsePackage()
            {
                RequestId  = requestPackage.RequestId,
                ActionName = requestPackage.ActionName,
                Code       = statusCode,
                Message    = errorMsg,
                Success  = false
            };

            string json = JsonSerializer.Serialize( responsePackage );
            base.Send( json );
        }

        #endregion 宣告私有的方法
    }
}
