﻿using System.Threading.Tasks;
using ZayniFramework.Common;


namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>遠端服務介面
    /// </summary>
    public interface IRemoteService
    {
        /// <summary>執行遠端服務動作
        /// </summary>
        /// <param name="request">遠端服務請求包裹</param>
        /// <returns>遠端服務回應包裹</returns>
        ResponsePackage Execute( RequestPackage request );

        /// <summary>發佈事件訊息
        /// </summary>
        /// <param name="messagePackage">訊息資料包裹</param>
        /// <returns>訊息發佈結果</returns>
        Result Publish( MessagePackage messagePackage );
    }
}
