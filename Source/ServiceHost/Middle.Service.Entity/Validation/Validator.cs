﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using ZayniFramework.Common;
using ZayniFramework.Validation;


namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>資料驗證器
    /// </summary>
    public static class Validator
    {
        /// <summary>執行資料驗證
        /// </summary>
        /// <param name="request">目標資料載體物件</param>
        /// <returns>資料驗證結果</returns>
        public static Result<string> Validate( object request )
        {
            #region 初始化回傳值

            var result = new Result<string>()
            {
                Success = false,
                Message   = null
            };

            #endregion 初始化回傳值

            #region 檢查目標載體是否為 Null 值

            if ( request.IsNull() )
            {
                result.Message = $"Target object is null.";
                return result;
            }

            #endregion 檢查目標載體是否為 Null 值

            #region 檢查是否略過資料驗證

            if ( request is JObject )
            {
                result.Success = true;
                return result;
            }

            Type type = request.GetType();

            if ( type.Name.Contains( "AnonymousType" ) && type.Namespace.IsNull() )
            {
                result.Success = true;
                return result;
            }

            #endregion 檢查是否略過資料驗證

            #region 設定目標物件

            IEnumerable targets = null;

            if ( request is IList )
            {
                targets = request as IEnumerable; 
            }
            else
            {
                targets = new List<object>() { request };
            }

            #endregion 設定目標物件

            #region 執行資料驗證

            foreach ( var target in targets )
            {
                var validateEntry = new ValidationEntry()
                {
                    TargetModel = target,
                    Language    = "en-US"
                };

                ValidationResult r = ValidatorFactory.Create().Validate( validateEntry );

                if ( !r.IsValid )
                {
                    result.Message = r.Message.Replace( "\r\n", " " );
                    return result;
                }
            }

            #endregion 執行資料驗證

            #region 設定回傳值

            result.Success = true;
            result.Message   = null;

            #endregion 設定回傳值

            return result;
        }
    }
}
