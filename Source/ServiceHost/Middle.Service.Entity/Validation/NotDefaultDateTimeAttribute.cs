﻿using System;
using ZayniFramework.Common;
using ZayniFramework.Validation;


namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>檢查是否為.NET預設的日期時間值，假若為預設日期時間，則認定驗證失敗。
    /// </summary>
    /// <remarks>
    /// 1. 此為 Zayni Framework，原前身為 Zayni Framework 框架的 Validation Module 的客製化資料驗證 Attribute。<para/>
    /// 2. 採用 Zayni Framework 框架，類似這些東西都是可擴充、可抽換的 Component。
    /// </remarks>
    public class NotDefaultDateTimeAttribute : ValidationAttribute
    {
        /// <summary>執行資料驗證
        /// </summary>
        /// <param name="obj">目標資料</param>
        /// <returns>資料驗證結果</returns>
        public override ValidationResult DoValidate( object obj )
        {
            var result = new ValidationResult()
            {
                IsValid = false,
                Message = null
            };

            try
            {
                DateTime target = (DateTime)obj;

                if ( target.IsDotNetDefaultDateTime() )
                {
                    result.Message = Message.IsNullOrEmptyString( "Can not be .NET framework default value of DateTime." );
                    return result;
                }
            }
            catch ( Exception ex )
            {
                result.Message = $"{nameof ( NotDefaultDateTimeAttribute )}: {nameof( DoValidate )} occur exception: {ex.ToString()}";
                return result;
            }

            result.IsValid = true;
            return result;
        }
    }
}
