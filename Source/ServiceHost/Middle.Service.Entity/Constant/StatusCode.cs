﻿namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>系統代碼常數值
    /// </summary>
    public sealed class StatusCode
    {
        /// <summary>服務動作執行成功
        /// </summary>
        public static readonly string ACTION_SUCCESS = "SUCCESS";

        /// <summary>不合法的遠端服務請求<para/>
        /// * 代表請求的 RequestPackage 參數不合法。<para/>
        /// * 或是請求資料載體的參數格式不合法。<para/>
        /// </summary>
        public static readonly string INVALID_REQUEST = "INVALID_REQUEST";

        /// <summary>未授權的請求或連線
        /// </summary>
        public static readonly string UNAUTHORIZED = "UNAUTHORIZED";

        /// <summary>不支援的請求或訊息
        /// </summary>
        public static readonly string NOT_SUPPORT = "NOT_SUPPORT";

        /// <summary>網路通訊連線異常
        /// </summary>
        public static readonly string CONNECTION_ERROR = "CONNECTION_ERROR";

        /// <summary>呼叫端內部程式異常
        /// </summary>
        public static readonly string CLIENT_ERROR = "CLIENT_ERROR";

        /// <summary>呼叫端 RPC 等待回應逾時
        /// </summary>
        public static readonly string CLIENT_RPC_TIMEOUT = "CLIENT_RPC_TIMEOUT";

        /// <summary>呼叫端 RPC 回應接收到不合法的訊息包裹
        /// </summary>
        public static readonly string CLIENT_RPC_INVALID_RES = "CLIENT_RPC_INVALID_RES";

        /// <summary>服務內部程式異常
        /// </summary>
        public static readonly string INTERNAL_ERROR = "INTERNAL_ERROR";
    }
}
