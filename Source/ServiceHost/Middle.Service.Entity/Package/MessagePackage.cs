﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;
using ZayniFramework.Validation;


namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>通訊訊息資料包裹
    /// </summary>
    [Serializable()]
    public class MessagePackage : IPackage
    {
        /// <summary>包裹種類
        /// </summary>
        [JsonPropertyName( "packageType" )]
        [JsonProperty( PropertyName = "packageType" )]
        public virtual string PackageType { get; set; }

        /// <summary>服務的設定名稱 (在 WebSocket 與 HttpCore 通訊技術下，必須要要傳入此欄位)
        /// </summary>
        [JsonPropertyName( "serviceName" )]
        [JsonProperty( PropertyName = "serviceName" )]
        public virtual string ServiceName { get; set; }

        /// <summary>訊息識別代碼
        /// </summary>
        [JsonPropertyName( "msgId" )]
        [JsonProperty( PropertyName = "msgId" )]
        [NotNullOrEmpty( Message = "'msgId' is null or empty string." )]
        public virtual string MessageId { get; set; }

        /// <summary>動作名稱
        /// </summary>
        [JsonPropertyName( "action" )]
        [JsonProperty( PropertyName = "action" )]
        [NotNullOrEmpty( Message = "'action' is null or empty string." )]
        public virtual string ActionName { get; set; }

        /// <summary>資料集合，資料內容格式為 JSON 字串。
        /// </summary>
        [JsonPropertyName( "data" )]
        [JsonProperty( PropertyName = "data" )]
        public virtual string Data { get; set; }

        /// <summary>請求時間戳記
        /// </summary>
        [JsonPropertyName( "publishTime" )]
        [JsonProperty( PropertyName = "publishTime", NullValueHandling = NullValueHandling.Ignore )]
        public virtual DateTime? PublishTime { get; set; }
    }
}
