﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;


namespace ZayniFramework.Middle.Service.Entity
{
    /// <summary>遠端服務回應包裹
    /// </summary>
    [Serializable()]
    public class ResponsePackage : IPackage
    {
        /// <summary>包裹種類
        /// </summary>
        [JsonPropertyName( "packageType" )]
        [JsonProperty( PropertyName = "packageType" )]
        public virtual string PackageType { get; set; }

        /// <summary>原始請求代碼
        /// </summary>
        [JsonPropertyName( "reqId" )]
        [JsonProperty( PropertyName = "reqId" )]
        public virtual string RequestId { get; set; }

        /// <summary>動作名稱
        /// </summary>
        [JsonPropertyName( "action" )]
        [JsonProperty( PropertyName = "action" )]
        public virtual string ActionName { get; set; }

        /// <summary>動作執行是否成功
        /// </summary>
        [JsonPropertyName( "success" )]
        [JsonProperty( PropertyName = "success" )]
        public virtual bool Success { get; set; }

        /// <summary>資料集合，資料內容格式為 JSON 字串。
        /// </summary>
        [JsonPropertyName( "data" )]
        [JsonProperty( PropertyName = "data" )]
        public virtual string Data { get; set; }

        /// <summary>動作執行結果代碼
        /// </summary>
        [JsonPropertyName( "code" )]
        [JsonProperty( PropertyName = "code" )]
        public virtual string Code { get; set; }

        /// <summary>執行結果訊息
        /// </summary>
        [JsonPropertyName( "message" )]
        [JsonProperty( PropertyName = "message" )]
        public virtual string Message { get; set; }
    }
}
