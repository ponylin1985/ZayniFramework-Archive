﻿using NeoSmart.AsyncLock;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>服務的交易鎖定
    /// </summary>
    internal static class ServiceTransaction
    {
        /// <summary>服務層級的非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        internal static AsyncLock AsyncLock { get; private set; } = new AsyncLock();
    }
}
