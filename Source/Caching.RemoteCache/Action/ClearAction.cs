﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>清空所有快取資料的動作
    /// </summary>
    internal sealed class ClearAction : ServiceAction<FooDTO, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public ClearAction() : base( "Clear" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            using ( ServiceTransaction.AsyncLock.Lock() )
            {
                Response         = Result.Create();
                var success      = MemoryCache.Clear();
                Response.Code    = success ? StatusCode.ACTION_SUCCESS : StatusCode.INTERNAL_ERROR;
                Response.Message = MemoryCache.Message;
                Response.Success = success;
            }
        }
    }

    /// <summary>清空所有快取資料的動作
    /// </summary>
    internal sealed class ClearActionAsync : ServiceActionAsync<FooDTO, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public ClearActionAsync() : base( "ClearAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            using ( await ServiceTransaction.AsyncLock.LockAsync() )
            {
                Response         = Result.Create();
                var success      = await MemoryCache.ClearAsync();
                Response.Code    = success ? StatusCode.ACTION_SUCCESS : StatusCode.INTERNAL_ERROR;
                Response.Message = MemoryCache.Message;
                Response.Success = success;
            }
        }
    }
}
