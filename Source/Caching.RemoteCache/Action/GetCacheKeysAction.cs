﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>取得快取倉儲中的 Key 值集合的動作
    /// </summary>
    internal sealed class GetCacheKeysAction : ServiceAction<GetCacheKeysReqDTO, Result<GetCacheKeysResDTO>>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetCacheKeysAction() : base( "GetCacheKeys" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            using ( ServiceTransaction.AsyncLock.Lock() )
            {
                Response = Result.Create<GetCacheKeysResDTO>();

                var r = MemoryCache.GetCacheKeys( Request.Pattern );

                if ( !r.Success )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = r.Message;
                    Logger.WriteErrorLog( this, r.Message, "Get all cache keys fail." );
                    return;
                }

                var cacheKeys = r.Data;

                var resDTO = new GetCacheKeysResDTO()
                {
                    CacheKeys = cacheKeys
                };
                
                Response.Data      = resDTO;
                Response.Code      = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }

    /// <summary>取得快取倉儲中的 Key 值集合的動作
    /// </summary>
    internal sealed class GetCacheKeysActionAsync : ServiceActionAsync<GetCacheKeysReqDTO, Result<GetCacheKeysResDTO>>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetCacheKeysActionAsync() : base( "GetCacheKeysAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            using ( await ServiceTransaction.AsyncLock.LockAsync() )
            {
                Response = Result.Create<GetCacheKeysResDTO>();

                var r = MemoryCache.GetCacheKeys( Request.Pattern );

                if ( !r.Success )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = r.Message;
                    Logger.WriteErrorLog( this, r.Message, "Get all cache keys fail." );
                    return;
                }

                var cacheKeys = r.Data;

                var resDTO = new GetCacheKeysResDTO()
                {
                    CacheKeys = cacheKeys
                };
                
                Response.Data      = resDTO;
                Response.Code      = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }
}
