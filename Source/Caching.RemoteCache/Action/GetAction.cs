﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>取得指定快取識別代碼的資料的動作
    /// </summary>
    internal sealed class GetAction : ServiceAction<CacheReqDTO, Result<GetCacheResult>>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetAction() : base( "Get" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            Response = Result.Create<GetCacheResult>();

            var cacheId = Request.CacheId;
            var result  = MemoryCache.Get( cacheId );
                
            Response.Data    = result;
            Response.Code    = StatusCode.ACTION_SUCCESS;
            Response.Success = true;
        }
    }

    /// <summary>取得指定快取識別代碼的資料的動作
    /// </summary>
    internal sealed class GetActionAsync : ServiceActionAsync<CacheReqDTO, Result<GetCacheResult>>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetActionAsync() : base( "GetAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            Response = Result.Create<GetCacheResult>();

            var cacheId = Request.CacheId;
            var result  = await MemoryCache.GetAsync( cacheId );
                
            Response.Data    = result;
            Response.Code    = StatusCode.ACTION_SUCCESS;
            Response.Success = true;
        }
    }
}
