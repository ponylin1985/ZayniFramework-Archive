﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>移除 HashProperty 資料的動作
    /// </summary>
    internal sealed class RemoveHashPropertyAction : ServiceAction<HashPropertyReqDTO, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public RemoveHashPropertyAction() : base( "RemoveHashProperty" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            using ( ServiceTransaction.AsyncLock.Lock() )
            {
                Response = Result.Create();

                if ( !MemoryCache.RemoveHashProperty( Request.CacheId, Request.Subkey ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Remove hash property data from cache repository fail. {MemoryCache.Message}.", Logger.GetTraceLogTitle( this, nameof ( Execute ) ) );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }

    /// <summary>移除 HashProperty 資料的動作
    /// </summary>
    internal sealed class RemoveHashPropertyActionAsync : ServiceActionAsync<HashPropertyReqDTO, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public RemoveHashPropertyActionAsync() : base( "RemoveHashPropertyAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            using ( await ServiceTransaction.AsyncLock.LockAsync() )
            {
                Response = Result.Create();

                if ( !await MemoryCache.RemoveHashPropertyAsync( Request.CacheId, Request.Subkey ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Remove hash property data from cache repository fail. {MemoryCache.Message}.", Logger.GetTraceLogTitle( this, nameof ( ExecuteAsync ) ) );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }
}
