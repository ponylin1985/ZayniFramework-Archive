﻿using NeoSmart.AsyncLock;
using System;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>服務動作容器
    /// </summary>
    public sealed class ActionContainer
    {
        #region 宣告私有的欄位

        /// <summary>非同步作業鎖定物件
        /// </summary>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        #endregion 宣告私有的欄位


        #region 宣告方法

        // Pony Says: 此方法在 Middle.Service module v2.2.5 之後的版本後，暫時可以不需要使用，因為 Caching.RemoteCache module 也採用了自動組件載入並且註冊 ServiceAction 的機制。
        /// <summary>初始化服務動作容器
        /// </summary>
        /// <returns>初始化結果</returns>
        [Obsolete( "此方法在 Middle.Service module v2.2.5 之後的版本後，暫時可以不需要使用", false )]
        public static IResult Initialize() 
        {
            var result = Result.Create();

            #region 暫時註解掉沒有使用的程式碼

            //try
            //{
            //    lock ( _lockThiz )
            //    {
            //        #region 註冊服務動作

            //        var countAction = new CountAction();
            //        ServiceActionContainerManager.RegisterServiceAction( countAction.Name, countAction.GetType() );

            //        var getCacheKeysAction = new GetCacheKeysAction();
            //        ServiceActionContainerManager.RegisterServiceAction( getCacheKeysAction.Name, getCacheKeysAction.GetType() );

            //        var getAction = new GetAction();
            //        ServiceActionContainerManager.RegisterServiceAction( getAction.Name, getAction.GetType() );

            //        var putAction = new PutAction();
            //        ServiceActionContainerManager.RegisterServiceAction( putAction.Name, putAction.GetType() );

            //        var updateAction = new UpdateAction();
            //        ServiceActionContainerManager.RegisterServiceAction( updateAction.Name, updateAction.GetType() );

            //        var removeAction = new RemoveAction();
            //        ServiceActionContainerManager.RegisterServiceAction( removeAction.Name, removeAction.GetType() );

            //        var containsAction = new ContainsAction();
            //        ServiceActionContainerManager.RegisterServiceAction( containsAction.Name, containsAction.GetType() );

            //        var clearAction = new ClearAction();
            //        ServiceActionContainerManager.RegisterServiceAction( clearAction.Name, clearAction.GetType() );

            //        var getHashPropertyAction = new GetHashPropertyAction();
            //        ServiceActionContainerManager.RegisterServiceAction( getHashPropertyAction.Name, getHashPropertyAction.GetType() );

            //        var getHashPropertiesAction = new GetHashPropertiesAction();
            //        ServiceActionContainerManager.RegisterServiceAction( getHashPropertiesAction.Name, getHashPropertiesAction.GetType() );

            //        var putHashPropertyAction = new PutHashPropertyAction();
            //        ServiceActionContainerManager.RegisterServiceAction( putHashPropertyAction.Name, putHashPropertyAction.GetType() );

            //        var removeHashPropertyAction = new RemoveHashPropertyAction();
            //        ServiceActionContainerManager.RegisterServiceAction( removeHashPropertyAction.Name, removeHashPropertyAction.GetType() );

            //        #endregion 註冊服務動作
            //    }
            //}
            //catch ( Exception ex )
            //{
            //    result.Message = $"{Logger.GetTraceLogTitle( nameof ( ActionContainer ), nameof ( Initialize ) )} occur exception: {ex.ToString()}";
            //    Logger.WriteExceptionLog( nameof ( ActionContainer ), ex, result.Message );
            //    return result;
            //}

            #endregion 暫時註解掉沒有使用的程式碼

            result.Success = true;
            return result;
        }

        /// <summary>取得服務動作容器的狀態資訊
        /// </summary>
        /// <returns>取得結果</returns>
        internal static Result<dynamic> GetServiceActionsInfo( string serviceName ) => ServiceActionContainerManager.GetServiceActionsInfo( serviceName );

        #endregion 宣告方法
    }
}
