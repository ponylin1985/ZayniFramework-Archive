﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>更新指定的快取資料的動作
    /// </summary>
    internal sealed class UpdateAction : ServiceAction<CacheProperty, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public UpdateAction() : base( "Update" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            using ( ServiceTransaction.AsyncLock.Lock() )
            {
                Response = Result.Create();

                var cacheProp = Request;

                if ( !MemoryCache.Update( cacheProp ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Update data to cache repository fail. {MemoryCache.Message}.", "Update data to cache repository fail." );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }

    /// <summary>更新指定的快取資料的動作
    /// </summary>
    internal sealed class UpdateActionAsync : ServiceActionAsync<CacheProperty, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public UpdateActionAsync() : base( "UpdateAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            using ( await ServiceTransaction.AsyncLock.LockAsync() )
            {
                Response = Result.Create();

                var cacheProp = Request;

                if ( !await MemoryCache.UpdateAsync( cacheProp ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Update data to cache repository fail. {MemoryCache.Message}.", "Update data to cache repository fail." );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }
}
