﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>存放 HashProperty 資料的動作
    /// </summary>
    internal sealed class PutHashPropertyAction : ServiceAction<PutHashPropertyReqDTO, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public PutHashPropertyAction() : base( "PutHashProperty" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            using ( ServiceTransaction.AsyncLock.Lock() )
            {
                Response = Result.Create();

                if ( !MemoryCache.PutHashProperty( Request.CacheId, Request.HashProperties ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Put hash property data into in-process memory cache fail. {MemoryCache.Message}.", Logger.GetTraceLogTitle( this, nameof ( Execute ) ) );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }

    /// <summary>存放 HashProperty 資料的動作
    /// </summary>
    internal sealed class PutHashPropertyActionAsync : ServiceActionAsync<PutHashPropertyReqDTO, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public PutHashPropertyActionAsync() : base( "PutHashPropertyAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            using ( await ServiceTransaction.AsyncLock.LockAsync() )
            {
                Response = Result.Create();

                if ( !await MemoryCache.PutHashPropertyAsync( Request.CacheId, Request.HashProperties ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Put hash property data into in-process memory cache fail. {MemoryCache.Message}.", Logger.GetTraceLogTitle( this, nameof ( ExecuteAsync ) ) );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }
}
