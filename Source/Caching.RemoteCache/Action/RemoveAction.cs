﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>將指定的資料從快取倉儲中移除的動作
    /// </summary>
    internal sealed class RemoveAction : ServiceAction<CacheReqDTO, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public RemoveAction() : base( "Remove" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            using ( ServiceTransaction.AsyncLock.Lock() )
            {
                Response = Result.Create();

                if ( !MemoryCache.Remove( Request.CacheId ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Remove data from cache repository fail. {MemoryCache.Message}.", "Remove data to cache repository fail." );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }

    /// <summary>將指定的資料從快取倉儲中移除的動作
    /// </summary>
    internal sealed class RemoveActionAsync : ServiceActionAsync<CacheReqDTO, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public RemoveActionAsync() : base( "RemoveAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            using ( await ServiceTransaction.AsyncLock.LockAsync() )
            {
                Response = Result.Create();

                if ( !await MemoryCache.RemoveAsync( Request.CacheId ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Remove data from cache repository fail. {MemoryCache.Message}.", "Remove data to cache repository fail." );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }
}
