﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>取得快取料 Key 值筆數的動作
    /// </summary>
    internal sealed class CountAction : ServiceAction<FooDTO, Result<int>>
    {
        /// <summary>預設建構子
        /// </summary>
        public CountAction() : base( "Count" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            using ( ServiceTransaction.AsyncLock.Lock() )
            {
                Response         = Result.Create<int>();
                Response.Data    = MemoryCache.Count;
                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }

    /// <summary>取得快取料 Key 值筆數的動作
    /// </summary>
    internal sealed class CountActionAsync : ServiceActionAsync<FooDTO, Result<int>>
    {
        /// <summary>預設建構子
        /// </summary>
        public CountActionAsync() : base( "CountAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            using ( await ServiceTransaction.AsyncLock.LockAsync() )
            {
                Response         = Result.Create<int>();
                Response.Data    = MemoryCache.Count;
                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }
}
