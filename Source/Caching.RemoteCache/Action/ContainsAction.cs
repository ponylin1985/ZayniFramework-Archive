﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>檢查是否包含指定 CacheId 的快取資料的動作
    /// </summary>
    internal sealed class ContainsAction : ServiceAction<CacheReqDTO, Result<bool>>
    {
        /// <summary>預設建構子
        /// </summary>
        public ContainsAction() : base( "Contains" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            using ( ServiceTransaction.AsyncLock.Lock() )
            {
                Response         = Result.Create<bool>();
                Response.Data    = MemoryCache.Contains( Request.CacheId );
                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }

    /// <summary>檢查是否包含指定 CacheId 的快取資料的動作
    /// </summary>
    internal sealed class ContainsActionAsync : ServiceActionAsync<CacheReqDTO, Result<bool>>
    {
        /// <summary>預設建構子
        /// </summary>
        public ContainsActionAsync() : base( "ContainsAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            using ( await ServiceTransaction.AsyncLock.LockAsync() )
            {
                Response         = Result.Create<bool>();
                Response.Data    = await MemoryCache.ContainsAsync( Request.CacheId );
                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }
}
