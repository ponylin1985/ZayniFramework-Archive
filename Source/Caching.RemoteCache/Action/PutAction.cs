﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>將指定的資料放入記憶體快取的動作
    /// </summary>
    internal sealed class PutAction : ServiceAction<CacheProperty, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public PutAction() : base( "Put" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            using ( ServiceTransaction.AsyncLock.Lock() )
            {
                Response = Result.Create();

                var cacheProp = Request;

                if ( !MemoryCache.Put( cacheProp ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Put data into cache repository fail. {MemoryCache.Message}.", "Put data to cache repository fail." );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }

    /// <summary>將指定的資料放入記憶體快取的動作
    /// </summary>
    internal sealed class PutActionAsync : ServiceActionAsync<CacheProperty, Result>
    {
        /// <summary>預設建構子
        /// </summary>
        public PutActionAsync() : base( "PutAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            using ( await ServiceTransaction.AsyncLock.LockAsync() )
            {
                Response = Result.Create();

                var cacheProp = Request;

                if ( !await MemoryCache.PutAsync( cacheProp ) )
                {
                    Response.Code    = StatusCode.INTERNAL_ERROR;
                    Response.Message = MemoryCache.Message;
                    Logger.WriteErrorLog( this, $"Put data into cache repository fail. {MemoryCache.Message}.", "Put data to cache repository fail." );
                    return;
                }

                Response.Code    = StatusCode.ACTION_SUCCESS;
                Response.Success = true;
            }
        }
    }
}
