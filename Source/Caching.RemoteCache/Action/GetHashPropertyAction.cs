﻿using System.Threading.Tasks;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>取得 HashProperty 結構資料的動作
    /// </summary>
    internal sealed class GetHashPropertyAction : ServiceAction<HashPropertyReqDTO, GetHashCacheResult>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetHashPropertyAction() : base( "GetHashProperty" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            Response      = MemoryCache.GetHashProperty( Request.CacheId, Request.Subkey );
            Response.Code = Response.Success ? StatusCode.ACTION_SUCCESS : StatusCode.INTERNAL_ERROR;
        }
    }

    /// <summary>取得 HashProperty 結構資料的動作
    /// </summary>
    internal sealed class GetHashPropertyActionAsync : ServiceActionAsync<HashPropertyReqDTO, GetHashCacheResult>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetHashPropertyActionAsync() : base( "GetHashPropertyAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            Response      = await MemoryCache.GetHashPropertyAsync( Request.CacheId, Request.Subkey );
            Response.Code = Response.Success ? StatusCode.ACTION_SUCCESS : StatusCode.INTERNAL_ERROR;
        }
    }
}
