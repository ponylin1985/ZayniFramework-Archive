﻿using System.Threading.Tasks;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>取得 HashProperty 資料集合的動作
    /// </summary>
    internal sealed class GetHashPropertiesAction : ServiceAction<CacheReqDTO, GetHashPropertiesCacheResult>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetHashPropertiesAction() : base( "GetHashProperties" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute()
        {
            Response      = MemoryCache.GetHashProperties( Request.CacheId );
            Response.Code = Response.Success ? StatusCode.ACTION_SUCCESS : StatusCode.INTERNAL_ERROR;
        }
    }

    /// <summary>取得 HashProperty 資料集合的動作
    /// </summary>
    internal sealed class GetHashPropertiesActionAsync : ServiceActionAsync<CacheReqDTO, GetHashPropertiesCacheResult>
    {
        /// <summary>預設建構子
        /// </summary>
        public GetHashPropertiesActionAsync() : base( "GetHashPropertiesAsync" )
        {
            // pass
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync()
        {
            Response      = await MemoryCache.GetHashPropertiesAsync( Request.CacheId );
            Response.Code = Response.Success ? StatusCode.ACTION_SUCCESS : StatusCode.INTERNAL_ERROR;
        }
    }
}
