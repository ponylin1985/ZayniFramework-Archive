﻿using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Grpc;
using ZayniFramework.Middle.Service.HttpCore;


namespace ZayniFramework.Caching.RemoteCacheService
{
    /// <summary>Zayni Framework 的遠端資料快取服務
    /// </summary>
    public sealed class RemoteCacheService
    {
        /// <summary>Middle.Service module 框架提供的 Middleware 中介層服務物件
        /// </summary>
        /// <returns></returns>
        private static readonly MiddlewareService _middlewareService = new MiddlewareService();

        /// <summary>啟動資料快取服務
        /// </summary>
        /// <returns>啟動結果</returns>
        public Result StartService()
        {
            var result = Result.Create();

            var rg = MiddlewareService.RegisterRemoteHostType<gRPCRemoteHost>( "gRPC" );

            if ( !rg.Success )
            {
                result.Message = $"Register the gRPCRemoteHost to cache service fail. {rg.Message}";
                Logger.WriteErrorLog( nameof ( RemoteCacheService ), result.Message, nameof ( StartService ) );
                return result;
            }

            // 採用 Middle.Service module 的 ServiceAction 自動註冊機制。
            //MiddlewareService.InitializeServiceActionHandler = () => ActionContainer.Initialize();

            var r = _middlewareService.StartServiceHosts();

            if ( !r.Success )
            {
                result.Message = $"Initialize process fail. Create remote service host fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( StartService ) ) );
                return result;
            }

            result.Success = true;
            return result;
        }

        /// <summary>啟動資料快取服務
        /// </summary>
        /// <param name="extensionModules">是否註冊並且啟動 Middle.Service Extension Modules 的 Service Host，預設不啟動。</param>
        /// <returns>啟動結果</returns>
        public async Task<Result> StartServiceAsync( bool extensionModules = false )
        {
            var result = Result.Create();

            if ( extensionModules )
            {
                var rg = MiddlewareService.RegisterRemoteHostType<gRPCRemoteHost>( "gRPC" );

                if ( !rg.Success )
                {
                    result.Message = $"Register the gRPCRemoteHost to cache service fail. {rg.Message}";
                    Logger.WriteErrorLog( nameof ( RemoteCacheService ), result.Message, nameof ( StartServiceAsync ) );
                    return result;
                }

                rg = MiddlewareService.RegisterRemoteHostType<HttpCoreRemoteHost>( "HttpCore" );

                if ( !rg.Success )
                {
                    result.Message = $"Register the HttpCoreRemoteHost to cache service fail. {rg.Message}";
                    Logger.WriteErrorLog( nameof ( RemoteCacheService ), result.Message, nameof ( StartServiceAsync ) );
                    return result;
                }
            }
            
            // 採用 Middle.Service module 的 ServiceAction 自動註冊機制。
            //MiddlewareService.InitializeServiceActionHandler = () => ActionContainer.Initialize();

            var r = await _middlewareService.StartServiceHostsAsync();

            if ( !r.Success )
            {
                result.Message = $"Initialize process fail. Create remote service host fail. {r.Message}";
                Logger.WriteErrorLog( this, result.Message, Logger.GetTraceLogTitle( this, nameof ( StartServiceAsync ) ) );
                return result;
            }

            result.Success = true;
            return result;
        }
    }
}
