#!/bin/bash

version=${1:-2.14.122}
runtime=${2:-alpine}

echo zayni.sharedata.service version: $version
echo images runtime OS: $runtime

docker login

docker tag zayni.client.app:$runtime-x64 ponylin1985/zayni.client.app:$runtime-x64
# docker tag zayni.server.app:$runtime-x64 ponylin1985/zayni.server.app:$runtime-x64
docker tag webapi.test:$runtime-x64 ponylin1985/webapi.test:$runtime-x64
docker tag zayni.sharedata.service:$runtime-x64 ponylin1985/zayni.sharedata.service:$version

docker push ponylin1985/zayni.sharedata.service:$version
docker push ponylin1985/zayni.client.app:$runtime-x64
# docker push ponylin1985/zayni.server.app:$runtime-x64
docker push ponylin1985/webapi.test:$runtime-x64

docker rmi -f zayni.client.app:$runtime-x64
# docker rmi -f zayni.server.app:$runtime-x64
docker rmi -f webapi.test:$runtime-x64
docker rmi -f zayni.sharedata.service:$runtime-x64

docker rmi -f ponylin1985/zayni.sharedata.service:$version
docker rmi -f ponylin1985/zayni.client.app:$runtime-x64
# docker rmi -f ponylin1985/zayni.server.app:$runtime-x64
docker rmi -f ponylin1985/webapi.test:$runtime-x64