# 第一階段: 執行 dotnet build 與 dotnet publish
# ================
FROM mcr.microsoft.com/dotnet/core/sdk:3.1.201-alpine3.11 AS build

# 從 HostOS 中的專案根目錄，完整複製整個專案結構到 image 作業系統中的 project_root 目錄。
COPY . ./project_root
RUN ls -la

# 設定 dockerfile 在 build 階段的工作目錄。
WORKDIR /project_root/Test/WebAPI.Test
RUN ls -la

# 第二階端選用的 base image 是 ubuntu 18.04 的作業系統鏡像，因此，dotnet publish 的 --runtime 參數值使用 ubuntu.18.04-x64 會是最好效能的 SCD 部署
RUN dotnet clean -c Release
RUN dotnet publish -c Release -r ubuntu.18.04-x64 --output ./publish

# ================

# 第二階段: 打包 dotnet publish SCD 後的應用程式至 runtime 的 base image 中。
# 說明: 
# 此 Dockerfile 在進行 docker build 建置時，dotnet publish 需要使用 SCD 模式部署，執行階段應用程式 (--runtime 參數) 需要指定為 linux-x64，
# 即為 dotnet publish -c Release -r linux-x64
FROM mcr.microsoft.com/dotnet/core/runtime-deps:3.1.3-bionic AS runtime 
WORKDIR /app
RUN mkdir Log
COPY --from=build ./project_root/Test/WebAPI.Test/publish .
ENTRYPOINT [ "./WebAPI.Test" ]