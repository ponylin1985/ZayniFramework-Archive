# 第一階段: 執行 dotnet build 與 dotnet publish
# ================
FROM mcr.microsoft.com/dotnet/core/sdk:3.1.201-alpine3.11 AS build

# 從 HostOS 中的專案根目錄，完整複製整個專案結構到 image 作業系統中的 project_root 目錄。
COPY . ./project_root
RUN ls -la

# 設定 dockerfile 在 build 階段的工作目錄。
WORKDIR /project_root/Source/Remote.ShareData.ServiceApp
RUN ls -la

# 執行 .NET Core 應用程式的 SCD 部署，注意 -r 參數要依照實際第二階段 runtime 打包的 image 作業系統鏡像選擇正確的參數。
# RUN dotnet publish -c Release -r linux-musl-x64 --output ./publish

# 第二階端選用的 base image 是 alpine 的作業系統鏡像，因此，dotnet publish 的 --runtime 參數值使用 alpine-x64 會是最好效能的 SCD 部署
RUN dotnet clean -c Release
RUN dotnet publish -c Release -r alpine.3.11-x64 --output ./publish

# ================

# 第二階段: 打包 dotnet publish SCD 後的應用程式至 runtime 的 base image 中。
# 說明: 
# 此 base image 以 alpine 當作基底，必且完全不包含 .NET Core SDK 和 Runtime Library，只有 .NET Core runtime 針對 alpine 作業系統原生的相依性套件。
# 因此，在此 runtime-deps 鏡像上執行應用程式時，不可以使用 dotnet xxx.dll 的方式，因為沒有安裝 dotnet CLI 套件，而是直接執行 dotnet publish 後在 alpine 作業系統原生的 binary 可執行檔案。
# 此 Dockerfile 在進行 docker build 建置時，dotnet publish 需要使用 SCD 模式部署，執行階段應用程式 (--runtime 參數) 需要指定為 linux-musl-x64 或 alpine-x64
FROM mcr.microsoft.com/dotnet/core/runtime-deps:3.1.3-alpine3.11 AS runtime 
# 注意! 如果要執行 gRPC.Client 通訊，就一定要對 runtime-deps-alpine 這個 base image 額外安裝 libc6-compat 原生套件
# 參考: https://github.com/grpc/grpc/issues/15605
# RUN apk update \
#  && apk add libc6-compat \
#  && apk add busybox-extras

# 在 alpine 3.9 以上版本的 alpine image 如果要啟動 gRPC host，必須要安裝 glibc-2.30-r0.apk 原生套件。
# 而不是再安裝 libc6-compat 套件。
RUN apk update \
 && apk --no-cache add ca-certificates wget \
 && wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
 && wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.30-r0/glibc-2.30-r0.apk \
 && apk add glibc-2.30-r0.apk \
 # telnet client for alpine OS.
 && apk add busybox-extras

WORKDIR /app
RUN mkdir Log
COPY --from=build ./project_root/Source/Remote.ShareData.ServiceApp/publish .
ENTRYPOINT [ "./ZayniFramework.Remote.ShareData.Service" ]