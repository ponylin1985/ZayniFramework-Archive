﻿using Elasticsearch.Net;
using NeoSmart.AsyncLock;
using Nest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;


namespace Elasticsearch.ConsoleApp.Test
{
    /// <summary>主程式
    /// </summary>
    class Program
    {
        // ElasticSearchDocClient and NEST ElasticClient Test
        /// <summary>程式進入點方法
        /// </summary>
        /// <param name="args"></param>
        static async Task Main( string[] args ) 
        {
            #region Upsert Document Test

            var model = new UserModel() 
            {
                UserId      = "Katie",
                UserName    = "Katie L.",
                UserAge     = "28",
                UserGender  = "male",
                Birthday    = new DateTime( 1998, 2, 14, 15, 3, 46 ).ToString( "yyyy-MM-dd HH:mm:ss" ),
                Description = "我是 Katie 喔。",
                Products    = new List<ProductModel>() 
                {
                    new ProductModel() 
                    {
                        ProductId   = "001",
                        ProductName = "EMQ-887",
                        Inventory   = "500"
                    },
                    new ProductModel() 
                    {
                        ProductId   = "002",
                        ProductName = "SE223-665",
                        Inventory   = "200"
                    },
                    new ProductModel() 
                    {
                        ProductId   = "003",
                        ProductName = "SE223-445",
                        Inventory   = "122"
                    }
                }
            };

            var esBroker = new ElasticSearchBroker( "http://localhost:9200" );
            var r = await esBroker.UpsertAsync( "doc_test_user", model.UserId, model );

            if ( !r.Success )
            {
                Console.WriteLine( r.Message );
                return;   
            }

            model = new UserModel() 
            {
                UserId      = "Katie",
                UserName    = "Katie L.",
                UserAge     = "22",
                UserGender  = "male",
                Birthday    = new DateTime( 2000, 2, 14 ).ToString( "yyyy-MM-dd HH:mm:ss" ),
                Description = "我是 Katie 喔，想認識我嗎？哈哈",
                Products    = new List<ProductModel>() 
                {
                    new ProductModel() 
                    {
                        ProductId   = "001",
                        ProductName = "KE6-887",
                        Inventory   = "500"
                    },
                    new ProductModel() 
                    {
                        ProductId   = "002",
                        ProductName = "SE223-665",
                        Inventory   = "200"
                    },
                    new ProductModel() 
                    {
                        ProductId   = "003",
                        ProductName = "SE223-445",
                        Inventory   = "122"
                    }
                }
            };

            r = await esBroker.UpsertAsync( "doc_test_user", model.UserId, model );

            if ( !r.Success )
            {
                Console.WriteLine( r.Message );
                return;   
            }

            var productModel = new ProductModel() 
            {
                ProductId   = "ABC-123",
                ProductName = "Test ABC",
                Inventory   = "3346"
            };

            r = await esBroker.UpsertAsync( "doc_test_product", productModel.ProductId, productModel );

            if ( !r.Success )
            {
                Console.WriteLine( r.Message );
                return;   
            }

            // 經過 ElasticSearch 實測後發現: 在 ElasticSearch 6.x 以後版本，同一個 index 只能允許傳入一個 type，但是，實際上在 runtime 允許傳入不同資料結構的 document。
            // 在 doc_test_product 這個 index 下，只能有 product 這種 type。但卻都可以寫入 ProductModel 和 UserModel 兩種不同結構的資料模型。
            // 但是!!! 請注意!!! 如果真的要這樣在「同一個 index 下」寫入不同資料結構的 document 物件，則不能使用 ZayniFramework 提供的 Upsert 方法，要自行使用 NEST 寫入!!! 
            if ( !esBroker.ElasticClient.Indices.Exists( "doc_test_product" ).Exists )
            {
                var i = await esBroker.ElasticClient.Indices.CreateAsync( "doc_test_product" );

                if ( !i.IsValid )
                {
                    Console.WriteLine( r.Message );
                    return;   
                }
            }

            var d = 
                await esBroker.ElasticClient
                    .IndexAsync( model, 
                        i => i 
                            .Index( "doc_test_product" )
                            .Id( model.UserId )
                            .Refresh( Refresh.True )
                    );

            Console.WriteLine( $"Upsert document data to Elasticsearch successfully." );

            #endregion Upsert Document Test

            #region Search Document Test

            var client = esBroker.ElasticClient;

            // 搜尋整個 index 下的文件。
            var query = 
                await client.SearchAsync<UserModel>( s => s
                    .Index( "doc_test_user" )
                    .MatchAll( m => m ) 
                );

            if ( !query.IsValid )
            {
                Console.WriteLine( query.DebugInformation );
                return;
            }

            if ( query.Hits.IsNullOrEmptyCollection() )
            {
                Console.WriteLine( $"Search result: No data found from elasticsearch." );
                return;
            }

            var userModels = query.Hits.Select( s => s.Source );
            var json = System.Text.Json.JsonSerializer.Serialize( userModels, new JsonSerializerOptions() { WriteIndented = true } );
            Console.WriteLine( $"MatchAll Search Result:" );
            Console.WriteLine( json );
            Console.WriteLine();
            Console.WriteLine();

            // 某個欄位前綴字符合「字段片語」的搜尋。
            query = 
                await client.SearchAsync<UserModel>( s => s
                    .Index( "doc_test_user" )
                    .From( 0 ) 
                    .Size( 10 )
                    .Query( q => q
                        .MatchPhrasePrefix( m => m
                            .Field( f => f.UserName )
                            .Query( "Katie" ) ) )
                );

            if ( !query.IsValid )
            {
                Console.WriteLine( query.DebugInformation );
                return;
            }

            if ( query.Hits.IsNullOrEmptyCollection() )
            {
                Console.WriteLine( $"Search result: No data found from elasticsearch." );
                return;
            }

            userModels = query.Hits.Select( s => s.Source );
            json = System.Text.Json.JsonSerializer.Serialize( userModels, new JsonSerializerOptions() { WriteIndented = true } );
            Console.WriteLine( $"MatchPhrasePrefix Search Result:" );
            Console.WriteLine( json );
            Console.WriteLine();
            Console.WriteLine();

            query = 
                await client.SearchAsync<UserModel>( s => s
                    .Index( "doc_test_user" )
                    .From( 0 ) 
                    .Size( 10 )
                    .Query( w => w
                        .Nested( c => c
                            .InnerHits( i => i.Explain() ) 
                            .Path( p => p.Products )
                            .Query( nq => nq
                                .MatchPhrasePrefix( m => m
                                .Field( f => f.Products.FirstOrDefault().ProductName ) 
                                .Query( "SE223" )
                            ) )
                        ) 
                    )

                );

            if ( !query.IsValid )
            {
                Console.WriteLine( query.DebugInformation );
                return;
            }

            if ( query.Hits.IsNullOrEmptyCollection() )
            {
                Console.WriteLine( $"Search result: No data found from elasticsearch." );
                return;
            }

            userModels = query.Hits.Select( s => s.Source );
            json = System.Text.Json.JsonSerializer.Serialize( userModels, new JsonSerializerOptions() { WriteIndented = true } );
            Console.WriteLine( $"Nested MatchPhrasePrefix Search Result:" );
            Console.WriteLine( json );
            Console.WriteLine();
            Console.WriteLine();

            #endregion Search Document Test

            Console.ReadLine();
        }
    }
    
    /// <summary>使用者 ES 文件資料模型
    /// * 對 Elasticsearch 進行寫入或搜尋時，實際上會以 Newtonsoft.Json 進行 JSON 序列化操作。
    /// * 但仍然有標記 System.Text.Json 的 JsonPropertyName，這是可以用來在: 不往 Elasticsearch 進行讀寫操作時，可以搭配使用 System.Text.Json 進行 JSON 序列化，以加快效能。
    /// * 主要，仍然需要標記 Nest 提供的 attribute，才會真正決定寫入到 Elasticsearch 中的 type mapping。
    /// </summary>
    [ElasticsearchType( RelationName = "user" )]
    public class UserModel 
    {
        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "id" )]
        [JsonProperty( PropertyName = "id" )]
        [Text( Name = "id" )]
        public string UserId { get; set; }

        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "name" )]
        [JsonProperty( PropertyName = "name" )]
        [Text( Name = "name" )]
        public string UserName { get; set; }

        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "age" )]
        [JsonProperty( PropertyName = "age" )]
        [Number( NumberType.Integer, Name = "age" )]
        public string UserAge { get; set; }

        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "gender" )]
        [JsonProperty( PropertyName = "gender" )]
        [Text( Name = "gender" )]
        public string UserGender { get; set; }

        // ES DateTime 型別必須宣告為 String
        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "birthday" )]
        [JsonProperty( PropertyName = "birthday" )]
        [Date( Name = "birthday", Format = "yyyy-MM-dd HH:mm:ss" )]
        public string Birthday { get; set; }

        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "description" )]
        [JsonProperty( PropertyName = "description" )]
        [Text( Name = "description" )]
        public string Description { get; set; }

        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "products" )]
        [JsonProperty( PropertyName = "products" )]
        [Nested( Name = "products" )]
        public IEnumerable<ProductModel> Products { get; set; }
    }

    /// <summary>產品 ES 資料模型
    /// </summary>
    [ElasticsearchType( RelationName = "product" )]
    public class ProductModel 
    {
        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "id" )]
        [JsonProperty( PropertyName = "id" )]
        [Text( Name = "id" )]
        public string ProductId { get; set; }

        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "product_name" )]
        [JsonProperty( PropertyName = "product_name" )]
        [Text( Name = "product_name" )]
        public string ProductName { get; set; }

        /// <summary>
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "product_inventory" )]
        [JsonProperty( PropertyName = "product_inventory" )]
        [Number( NumberType.Integer, Name = "product_inventory" )]
        public string Inventory { get; set; }
    }
}
