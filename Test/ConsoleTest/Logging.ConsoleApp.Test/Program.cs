﻿using System;
using System.Diagnostics;
using System.Threading;
using ZayniFramework.Common;
using ZayniFramework.Logging;

namespace Logging.ConsoleApp.Test
{
    /// <summary>主程式
    /// </summary>
    class Program
    {
        /// <summary>程式進入點方法
        /// </summary>
        /// <param name="args"></param>
        static void Main2( string[] args ) 
        {
            var testDir  = "./Log";
            var fullPath = FileHelper.GetFullPath( testDir );
            ConsoleLogger.WriteLine( $"FullPath: {fullPath}", ConsoleColor.Green );

            testDir  = "/Log";
            fullPath = FileHelper.GetFullPath( testDir, "\\" );
            ConsoleLogger.WriteLine( $"FullPath: {fullPath}", ConsoleColor.Green );

            testDir  = "Log";
            fullPath = FileHelper.GetFullPath( testDir, null );
            ConsoleLogger.WriteLine( $"FullPath: {fullPath}", ConsoleColor.Green );

            Console.ReadLine();
        }
        
        /// <summary>Loggger 測試
        /// </summary>
        /// <param name="args"></param>
        static void Main( string[] args )
        {
            Logger.Instance.WriteErrorLogEvent += ( sender, message, title, loggerName ) => 
            {
                ConsoleLogger.WriteLine( $"Ho Ho Ho, this is a error event fired..." );
                ConsoleLogger.WriteLine( $"LoggerName: {loggerName}." );
                ConsoleLogger.WriteLine( $"Error Title: {title}." );
                ConsoleLogger.WriteLine( $"Error Message: {message}" );
            };

            // ====================

            Logger.WriteInformationLog( nameof ( Program ), "Start ZayniFramework Logging.Test console.", Logger.GetTraceLogTitle( nameof ( Program ), nameof ( Main ) ), "TextLogWithEmail" );
            SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 2 ) );
            
            Logger.WriteErrorLog( nameof ( Program ), "Something error!!", "MyTest" );
            Logger.WriteErrorLog( nameof ( Program ), "Some Error Again!! Testing", "MyTest", loggerName: "TextLogWithEmail" );
            SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 2 ) );
            
            Logger.WriteInformationLog( nameof ( Program ), "End ZayniFramework Logging.Test console.", Logger.GetTraceLogTitle( nameof ( Program ), nameof ( Main ) ), "TextLogWithEmail" );
            SpinWait.SpinUntil( () => false, TimeSpan.FromMilliseconds( 2 ) );
            
            ConsoleLogger.WriteLine( "=======" );
            ConsoleLogger.Log( "This is a console log message", ConsoleColor.DarkGreen );
            ConsoleLogger.LogError( "This is a console log error message" );
            EventLogger.WriteLog( "ZayniFramework", "Testing", EventLogEntryType.Information, true );
            Console.ReadLine();
        }
    }
}
