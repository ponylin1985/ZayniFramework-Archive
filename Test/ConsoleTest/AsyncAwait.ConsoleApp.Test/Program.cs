﻿using NeoSmart.AsyncLock;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace AsyncAwait.ConsoleApp.Test
{
    /// <summary>主程式
    /// </summary>
    class Program
    {
        #region Private Fields

        /// <summary>多執行緒的鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly object _lockThiz = new object();

        /// <summary>非同步作業的鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLock = new AsyncLock();

        #endregion Private Fields


        #region Async Await Lab

        /// <summary>非同步方法呼叫與實作方式的 Lab
        /// </summary>
        /// <returns></returns>
        static async Task Main_AsyncAwait_Lab() 
        {
            // 不建議這種寫法，在宣告為 async 的 async method 最好要使用 await 關鍵字! (並且會有編譯警告)
            // async Task<Result<string>> GetSomethingAsync1() => Result.Create<string>( true, "AAA" );

            // 可行的寫法，但目前不確定會不會有效能影響。
            // 在 async method 中，另外使用 Task.Run 開啟一個新的 Task 去執行，並且利用 await 關鍵字。
            // 但應該也不是很建議這種寫法，因為 Task.Run() 方法其實是在 ThreadPool 請求一個 Task 去執行，屬於 CPU-bound 的情況，但是這邊又 await 這個不屬於 IO-bound 的 Task，會額外浪費掉 ThreadPool 中的資源。
            async Task<Result<string>> GetSomethingAsync2() => await Task.Run( () => Result.Create<string>( true, "AAA" ) );

            // 不建議這種寫法，雖然在 async method 中有使用 await 關鍵字，但 await Task.FromResult() 方法，編譯後會產生多餘的 IL code，造成不必要的效能浪費!
            async Task<Result<string>> GetSomethingAsync3() => await Task.FromResult( Result.Create<string>( true, "AAA" ) );

            // 建議的寫法，雖然預期 GetSomethingAsync 方法中要實作非同步的 IO 作業，但實際上無法呼叫到 async method 時
            // 可以直接把 GetSomethingAsync 方法宣告為 Non-async 的一般方法，利用 Task.FromResult() 直接回傳 Task，但不要 await Task.FromResult() 方法的回傳。
            Task<Result<string>> GetSomethingAsync4() => Task.FromResult( Result.Create<string>( true, "AAA" ) );

            // ============
            // 以下的基本結論是: 只要方法回傳 Task 型別，在呼叫端通通可以 await，即便方法沒有被宣告為 async，仍可以使用 await 呼叫。

            // var r1 = await GetSomethingAsync1();
            // await Console.Out.WriteLineAsync( r1.Data );

            var r2 = await GetSomethingAsync2();
            await Console.Out.WriteLineAsync( r2.Data );

            var r3 = await GetSomethingAsync3();
            await Console.Out.WriteLineAsync( r3.Data );

            var r4 = await GetSomethingAsync4();
            await Console.Out.WriteLineAsync( r4.Data );

            var r4plus = GetSomethingAsync4();
            await Console.Out.WriteLineAsync( r4plus.GetAwaiter().GetResult().Data );
        }

        #endregion Async Await Lab


        #region AsyncLock Tests

        /// <summary>以下為一個對於 AsyncLock 重要的測試，下面的程式碼測試，
        /// 分別在 async method 與 non-async method 中使用相同的 AsyncLock 物件取得鎖定物件，
        /// 是否可以正確的完成鎖定的動作。
        /// </summary>
        static async Task Main() 
        {
            /// <summary>有正確使用 AsyncLock 進行鎖定程式碼的測試，結果如預期，所有整數 dump 出來會依序顯示
            /// </summary>
            /// <returns></returns>
            async Task AsyncLockTest() 
            {
                var random = new Random( Guid.NewGuid().GetHashCode() );
                var queue  = new Queue<int>();

                /// <summary>確實使用 AsyncLock.LockAsync() 取得一個鎖定物件並且鎖定程式碼操作。
                /// </summary>
                /// <returns></returns>
                async Task EnqueueAsync() 
                {
                    using ( await _asyncLock.LockAsync() )
                    {
                        for ( int i = 0; i < 100; i++ )
                        {
                            queue.Enqueue( i + 1 );
                            await Task.Delay( random.Next( 1, 100 ) );
                        }

                        await Console.Out.WriteLineAsync( $"[EnqueueAsync] OK" );
                    }
                }

                /// <summary>確實使用 AsyncLock.Lock() 取得一個鎖定物件並且鎖定程式碼操作。
                /// </summary>
                void Enqueue() 
                {
                    using ( _asyncLock.Lock() )
                    {
                        for ( int i = 0; i < 100; i++ )
                        {
                            queue.Enqueue( i + 1 );
                            System.Threading.SpinWait.SpinUntil( () => false, random.Next( 1, 100 ) );
                        }

                        Console.WriteLine( $"[Enqueue] OK" );
                    }
                }

                Task task1 = null;
                // Task task2 = null;
                Task task3 = null;
                // Task task4 = null;
                Task task5 = null;

                // var t1 = new Thread( () => task1 = EnqueueAsync() );
                // var t2 = new Thread( () => task2 = EnqueueAsync() );
                // var t3 = new Thread( () => task3 = EnqueueAsync() );
                // var t4 = new Thread( () => task4 = EnqueueAsync() );
                // var t5 = new Thread( () => task5 = EnqueueAsync() );

                // var t1 = new Thread( () => Enqueue() );
                // var t2 = new Thread( () => Enqueue() );
                // var t3 = new Thread( () => Enqueue() );
                // var t4 = new Thread( () => Enqueue() );
                // var t5 = new Thread( () => Enqueue() );

                // 執行每一個方法時，都獨立開啟一條 thread 去執行，其中故意某些執行 async method，某些執行 non-async method 
                // 要注意的是: 在執行去中呼叫 async method 時，不可以直接 await 掉，需要在 Main Thread 進行 await。
                var t1 = new Thread( () => task1 = EnqueueAsync() );
                var t2 = new Thread( () => Enqueue() );
                var t3 = new Thread( () => task3 = EnqueueAsync() );
                var t4 = new Thread( () => Enqueue() );
                var t5 = new Thread( () => task5 = EnqueueAsync() );

                var threads = new List<Thread>() { t1, t2, t3, t4, t5 };

                t1.Start();
                t2.Start();
                t3.Start();
                t4.Start();
                t5.Start();

                foreach ( var thread in threads )
                {
                    thread.Join();
                }

                await task1;
                // await task2;
                await task3;
                // await task4;
                await task5;

                while ( true )
                {
                    if ( queue.IsNullOrEmptyCollection() ) 
                    {
                        System.Threading.SpinWait.SpinUntil( () => false, 100 );
                        continue;
                    }

                    foreach ( var q in queue )
                    {
                        Console.WriteLine( q );
                    }

                    break;
                }
            }

            /// <summary>沒有進行鎖定程式碼的測試，結果無法預期，被 dump 出來的整數不會依序顯示。
            /// </summary>
            /// <returns></returns>
            async Task NoLockTest() 
            {
                var random = new Random( Guid.NewGuid().GetHashCode() );
                var queue  = new Queue<int>();

                /// <summary>沒有鎖定程式碼的非同步方法
                /// </summary>
                /// <returns></returns>
                async Task EnqueueAsync() 
                {
                    for ( int i = 0; i < 100; i++ )
                    {
                        queue.Enqueue( i + 1 );
                        await Task.Delay( random.Next( 1, 100 ) );
                    }

                    Console.WriteLine( $"[EnqueueAsync] OK" );
                }

                /// <summary>沒有鎖定程式碼的同步方法
                /// </summary>
                void Enqueue() 
                {
                    for ( int i = 0; i < 100; i++ )
                    {
                        queue.Enqueue( i + 1 );
                        System.Threading.SpinWait.SpinUntil( () => false, random.Next( 1, 100 ) );
                    }

                    Console.WriteLine( $"[Enqueue] OK" );
                }

                Task task1 = null;
                Task task3 = null;
                Task task5 = null;

                // 執行每一個方法時，都獨立開啟一條 thread 去執行，其中故意某些執行 async method，某些執行 non-async method 
                // 要注意的是: 在執行去中呼叫 async method 時，不可以直接 await 掉，需要在 Main Thread 進行 await。
                var t1 = new Thread( () => task1 = EnqueueAsync() );
                var t2 = new Thread( () => Enqueue() );
                var t3 = new Thread( () => task3 = EnqueueAsync() );
                var t4 = new Thread( () => Enqueue() );
                var t5 = new Thread( () => task5 = EnqueueAsync() );

                var threads = new List<Thread>() { t1, t2, t3, t4, t5 };

                t1.Start();
                t2.Start();
                t3.Start();
                t4.Start();
                t5.Start();

                foreach ( var thread in threads )
                {
                    thread.Join();
                }

                await task1;
                await task3;
                await task5;

                while ( true )
                {
                    if ( queue.IsNullOrEmptyCollection() ) 
                    {
                        System.Threading.SpinWait.SpinUntil( () => false, 100 );
                        continue;
                    }

                    foreach ( var q in queue )
                    {
                        Console.WriteLine( q );
                    }

                    break;
                }
            }
        
            await AsyncLockTest();
            // await NoLockTest();
        }

        /// <summary>鎖定物件測試
        /// * 多執行緒鎖定物件測試
        /// * 非同步作業鎖定物件測試
        /// </summary>
        /// <returns></returns>
        static async Task Main_AsyncLock2() 
        {
            var random = new Random( Guid.NewGuid().GetHashCode() );

            async Task AsyncLockTest_AsyncAwaitCalling() 
            {
                var tasks  = new List<Task>();
                var models = new Queue<int>();

                /// <summary>使用 AsyncLock.LockAsync 進行鎖定
                /// </summary>
                async Task ProduceAsync_AsyncLock_LockAsync() 
                {
                    using ( await _asyncLock.LockAsync() )
                    {
                        for ( int i = 0; i < 100; i++ )
                        {
                            models.Enqueue( i + 1 );
                            await Task.Delay( random.Next( 1, 5 ) );
                        }

                        Console.WriteLine( $"[AsyncLockTest_AsyncAwaitCalling] OK" );
                    }
                }

                await Console.Out.WriteLineAsync( $"[AsyncLockTest_AsyncAwaitCalling]" );
                
                // 當要呼叫 async method 時，在多數情況下，最建議的呼叫方式，並且在 async method 中使用 AsyncLock.LockAsync() 也能確定鎖定機制。
                await ProduceAsync_AsyncLock_LockAsync();
                await Console.Out.WriteLineAsync( $"[AsyncLockTest_AsyncAwaitCalling] Not await here..." );
                await ProduceAsync_AsyncLock_LockAsync();
                await Console.Out.WriteLineAsync( $"[AsyncLockTest_AsyncAwaitCalling] Not await here..." );
                await ProduceAsync_AsyncLock_LockAsync();
                await Console.Out.WriteLineAsync( $"[AsyncLockTest_AsyncAwaitCalling] Not await here..." );
                await ProduceAsync_AsyncLock_LockAsync();
                await Console.Out.WriteLineAsync( $"[AsyncLockTest_AsyncAwaitCalling] Not await here..." );
                await ProduceAsync_AsyncLock_LockAsync();

                // 以下等同是在 sync method 中，直接呼叫 async method，這樣會有非預期的結果，甚至 exception。
                // ProduceAsync_AsyncLock_LockAsync();
                // ProduceAsync_AsyncLock_LockAsync();
                // ProduceAsync_AsyncLock_LockAsync();
                // ProduceAsync_AsyncLock_LockAsync();
                // ProduceAsync_AsyncLock_LockAsync();

                // 用這種方式，呼叫一個 async method，反而會有非預期的結果，甚至可能直接產生 exception，AsyncLock 也無法發揮作用。
                // tasks.Add( ProduceAsync_AsyncLock_LockAsync() );
                // tasks.Add( ProduceAsync_AsyncLock_LockAsync() );
                // tasks.Add( ProduceAsync_AsyncLock_LockAsync() );
                // tasks.Add( ProduceAsync_AsyncLock_LockAsync() );
                // tasks.Add( ProduceAsync_AsyncLock_LockAsync() );
                // Task.WaitAll( tasks.ToArray() );

                await Console.Out.WriteLineAsync( $"[AsyncLockTest_AsyncAwaitCalling] Not await here..." );
                await Console.Out.WriteLineAsync( $"==================" );
                await Console.Out.WriteLineAsync();
                await Console.Out.WriteLineAsync();

                await Task.Run( async () => 
                {
                    while ( true )
                    {
                        if ( models.IsNullOrEmptyCollection() ) 
                        {
                            await Task.Delay( 50 );
                            continue;
                        }

                        foreach ( var model in models )
                        {
                            Console.WriteLine( model );
                        }

                        break;
                    }
                } );
            }

            async Task AsyncLockTest_AwaitTaskRunCalling() 
            {
                var tasks  = new List<Task>();
                var models = new Queue<int>();

                /// <summary>使用 AsyncLock.LockAsync 進行鎖定
                /// </summary>
                async Task ProduceAsync_AsyncLock_LockAsync() 
                {
                    using ( await _asyncLock.LockAsync() )
                    {
                        for ( int i = 0; i < 100; i++ )
                        {
                            models.Enqueue( i + 1 );
                            await Task.Delay( random.Next( 1, 5 ) );
                        }

                        Console.WriteLine( $"[AsyncLockTest_AwaitTaskRunCalling] OK" );
                    }
                }

                await Console.Out.WriteLineAsync( $"[AsyncLockTest_AwaitTaskRunCalling]" );

                // 透過 Task.Run 開啟一個新的非同步 Task 作業去呼叫原有的 async method，只要有符合 async/await pattern 方式呼叫，結果也會於預期正常。
                await Task.Run( async () => await ProduceAsync_AsyncLock_LockAsync() );
                await Task.Run( async () => await ProduceAsync_AsyncLock_LockAsync() );
                await Task.Run( async () => await ProduceAsync_AsyncLock_LockAsync() );
                await Task.Run( async () => await ProduceAsync_AsyncLock_LockAsync() );
                await Task.Run( async () => await ProduceAsync_AsyncLock_LockAsync() );
                await Console.Out.WriteLineAsync( $"==================" );
                await Console.Out.WriteLineAsync();
                await Console.Out.WriteLineAsync();

                await Task.Run( async () => 
                {
                    while ( true )
                    {
                        if ( models.IsNullOrEmptyCollection() ) 
                        {
                            await Task.Delay( 50 );
                            continue;
                        }

                        foreach ( var model in models )
                        {
                            Console.WriteLine( model );
                        }

                        break;
                    }
                } );
            }

            void AsyncLockTest_CallFromSyncMethod_GetAwaiterGetResult() 
            {
                var tasks  = new List<Task>();
                var models = new Queue<int>();

                /// <summary>使用 AsyncLock.LockAsync 進行鎖定
                /// </summary>
                async Task ProduceAsync_AsyncLock_LockAsync() 
                {
                    using ( await _asyncLock.LockAsync() )
                    {
                        for ( int i = 0; i < 100; i++ )
                        {
                            models.Enqueue( i + 1 );
                            await Task.Delay( random.Next( 1, 5 ) );
                        }

                        Console.WriteLine( $"[AsyncLockTest_CallFromSyncMethod_GetAwaiterGetResult] OK" );
                    }
                }

                Console.WriteLine( $"[AsyncLockTest_CallFromSyncMethod_GetAwaiterGetResult]" );

                // 以下是在 sync method 中，呼叫 async method 的一種方式，這種的呼叫不會破壞 async method 中的 AsyncLock 機制。
                // 結果也如預期，因為這種呼叫方式，基本上大概就是把非同步方法，「同步地」依序執行，所以也沒有真的非同步作業的效果。
                ProduceAsync_AsyncLock_LockAsync().GetAwaiter().GetResult();
                ProduceAsync_AsyncLock_LockAsync().GetAwaiter().GetResult();
                ProduceAsync_AsyncLock_LockAsync().GetAwaiter().GetResult();
                ProduceAsync_AsyncLock_LockAsync().GetAwaiter().GetResult();
                ProduceAsync_AsyncLock_LockAsync().GetAwaiter().GetResult();
                Console.WriteLine( $"==================" );
                Console.WriteLine();
                Console.WriteLine();

                Task.Run( async () => 
                {
                    while ( true )
                    {
                        if ( models.IsNullOrEmptyCollection() ) 
                        {
                            await Task.Delay( 50 );
                            continue;
                        }

                        foreach ( var model in models )
                        {
                            Console.WriteLine( model );
                        }

                        break;
                    }
                } ).Wait();
            }

            void AsyncLockTest_CallFromSyncMethod_TaskWait() 
            {
                var tasks  = new List<Task>();
                var models = new Queue<int>();

                /// <summary>使用 AsyncLock.LockAsync 進行鎖定
                /// </summary>
                async Task ProduceAsync_AsyncLock_LockAsync() 
                {
                    using ( await _asyncLock.LockAsync() )
                    {
                        for ( int i = 0; i < 100; i++ )
                        {
                            models.Enqueue( i + 1 );
                            await Task.Delay( random.Next( 1, 5 ) );
                        }

                        Console.WriteLine( $"[AsyncLockTest_CallFromSyncMethod_TaskWait] OK" );
                    }
                }

                Console.WriteLine( $"[AsyncLockTest_CallFromSyncMethod_TaskWait]" );

                // 如果要從 sync method 呼叫 async method，以下方式仍可以適用，實測結果，在 async method 中的 AsyncLock 機制仍可以正常如預期運作。
                tasks.Add( Task.Run( ProduceAsync_AsyncLock_LockAsync ) );
                tasks.Add( Task.Run( ProduceAsync_AsyncLock_LockAsync ) );
                tasks.Add( Task.Run( ProduceAsync_AsyncLock_LockAsync ) );
                tasks.Add( Task.Run( ProduceAsync_AsyncLock_LockAsync ) );
                tasks.Add( Task.Run( ProduceAsync_AsyncLock_LockAsync ) );
                Task.WaitAll( tasks.ToArray() );

                Console.WriteLine( $"==================" );
                Console.WriteLine();
                Console.WriteLine();

                Task.Run( async () => 
                {
                    while ( true )
                    {
                        if ( models.IsNullOrEmptyCollection() ) 
                        {
                            await Task.Delay( 50 );
                            continue;
                        }

                        foreach ( var model in models )
                        {
                            Console.WriteLine( model );
                        }

                        break;
                    }
                } ).Wait();
            }

            await Console.Out.WriteLineAsync( $"AsyncLock test start..." );

            await AsyncLockTest_AsyncAwaitCalling();
            await AsyncLockTest_AwaitTaskRunCalling();
            AsyncLockTest_CallFromSyncMethod_GetAwaiterGetResult();
            AsyncLockTest_CallFromSyncMethod_TaskWait();

            await Console.Out.WriteLineAsync( $"AsyncLock test finished..." );
            await Console.Out.WriteLineAsync( $"=========================" );
            await Console.Out.WriteLineAsync();
            await Console.Out.WriteLineAsync();
            await Console.Out.WriteLineAsync();

            // ================
            
            await Task.Delay( 1000 * 3 );
            await Console.Out.WriteLineAsync( $"object lock test start..." );

            void AsyncLock_Lock_Test() 
            {
                var tasks  = new List<Task>();
                var models = new Queue<int>();

                /// <summary>使用 AsyncLock.Lock 進行鎖定
                /// * 在非 async 方法中，還是可以使用 AsyncLock 進行鎖定，只是呼叫同步的 Lock() 方法進行鎖定。
                /// * 結果是正確，正常預期的!
                /// </summary>
                void Produce_Async_Lock() 
                {
                    using ( _asyncLock.Lock() )
                    {
                        for ( int i = 0; i < 100; i++ )
                        {
                            models.Enqueue( i + 1 );
                            System.Threading.SpinWait.SpinUntil( () => false, random.Next( 1, 5 ) );
                        }

                        Console.WriteLine( $"[AsyncLock_Lock_Test] OK" );
                    }
                }

                Console.WriteLine( $"[AsyncLock_Lock_Test]" );

                // 以 Task.Run 去執行一個 sync method，無論對 Task.Run 有沒有加上 await，結果都是正常如預期的!
                Task.Run( () => Produce_Async_Lock() );
                Task.Run( () => Produce_Async_Lock() );
                Task.Run( () => Produce_Async_Lock() );
                Task.Run( () => Produce_Async_Lock() );
                Task.Run( () => Produce_Async_Lock() );

                // await Task.Run( () => Produce_Async_Lock() );
                // await Task.Run( () => Produce_Async_Lock() );
                // await Task.Run( () => Produce_Async_Lock() );
                // await Task.Run( () => Produce_Async_Lock() );
                // await Task.Run( () => Produce_Async_Lock() );

                Console.WriteLine( $"==================" );
                Console.WriteLine();
                Console.WriteLine();

                while ( true )
                {
                    if ( models.IsNullOrEmptyCollection() ) 
                    {
                        System.Threading.SpinWait.SpinUntil( () => false, 50 );
                        continue;
                    }

                    foreach ( var model in models )
                    {
                        Console.WriteLine( model );
                    }

                    break;
                }
            }

            async Task ObjectLock_Test() 
            {
                var tasks  = new List<Task>();
                var models = new Queue<int>();

                /// <summary>使用 lock + private static readonly object 進行鎖定
                /// * 只能使用在非 async 的方法中。
                /// </summary>
                void Produce_LockThiz() 
                {
                    lock ( _lockThiz )
                    {
                        for ( int i = 0; i < 100; i++ )
                        {
                            models.Enqueue( i + 1 );
                            System.Threading.SpinWait.SpinUntil( () => false, random.Next( 1, 5 ) );
                        }

                        Console.WriteLine( $"[ObjectLock_Test] OK" );
                    }
                }

                Console.WriteLine( $"[ObjectLock_Test]" );

                await Task.Run( () => Produce_LockThiz() );
                await Task.Run( () => Produce_LockThiz() );
                await Task.Run( () => Produce_LockThiz() );
                await Task.Run( () => Produce_LockThiz() );
                await Task.Run( () => Produce_LockThiz() );

                Console.WriteLine( $"==================" );
                Console.WriteLine();
                Console.WriteLine();

                await Task.Run( async () => 
                {
                    while ( true )
                    {
                        if ( models.IsNullOrEmptyCollection() ) 
                        {
                            await Task.Delay( 50 );
                            continue;
                        }

                        foreach ( var model in models )
                        {
                            Console.WriteLine( model );
                        }

                        break;
                    }
                } );
            }

            void NoLock_Test() 
            {
                var tasks  = new List<Task>();
                var models = new Queue<int>();

                /// <summary>完全不鎖定
                /// * 結果如預期: 最快跑完，但結果幾乎不能預測!
                /// </summary>
                void Produce_NoLock() 
                {
                    for ( int i = 0; i < 100; i++ )
                    {
                        models.Enqueue( i + 1 );
                        System.Threading.SpinWait.SpinUntil( () => false, random.Next( 1, 5 ) );
                    }

                    Console.WriteLine( $"OK" );
                }

                Console.WriteLine( $"[NoLock_Test]" );

                Task.Run( () => Produce_NoLock() );
                Task.Run( () => Produce_NoLock() );
                Task.Run( () => Produce_NoLock() );
                Task.Run( () => Produce_NoLock() );
                Task.Run( () => Produce_NoLock() );

                Console.WriteLine( $"==================" );
                Console.WriteLine();
                Console.WriteLine();

                Task.Run( async () => 
                {
                    while ( true )
                    {
                        if ( models.IsNullOrEmptyCollection() ) 
                        {
                            await Task.Delay( 50 );
                            continue;
                        }

                        foreach ( var model in models )
                        {
                            Console.WriteLine( model );
                        }

                        break;
                    }
                } ).Wait();
            }

            AsyncLock_Lock_Test();
            await ObjectLock_Test();
            NoLock_Test();

            await Console.Out.WriteLineAsync( $"object lock test finished..." );
            await Console.In.ReadLineAsync();
        }

        #endregion AsyncLock Tests


        #region Async and Non-Async Methods examples

        /// <summary>非同步作業的 Main 方法，需要呼叫 async method 的範例。
        /// </summary>
        /// <returns></returns>
        static async Task Main_Async() 
        {
            // 注意，需要進行 await 等待非同步 Task 作業的 Awaiter。
            // 在 await 之後的程式碼不會被 Main Thread 執行到! 不會印出 AAA 字串。
            await Task.Run( async () => 
            {
                while ( true )
                {
                    await Console.Out.WriteLineAsync( $"Please enter command..." );
                    var command = await Console.In.ReadLineAsync();
                    await Console.Out.WriteLineAsync( $"The command your enter: {command}" );
                    await Task.Delay( 500 );
                }
            } );

            Console.WriteLine( $"AAA" );
            Console.ReadLine();
        }

        /// <summary>一般 (Non-async) 的 Main 方法，需要呼叫 async method 的範例。
        /// </summary>
        /// <returns></returns>
        static void Main_Sync() 
        {
            // 注意，需要進行 Wait() 等待!
            // 在 Wait() 之後的程式碼不會被 Main Thread 執行到! 不會印出 AAA 字串。
            Task.Run( async () => 
            {
                while ( true )
                {
                    await Console.Out.WriteLineAsync( $"Please enter command..." );
                    var command = await Console.In.ReadLineAsync();
                    await Console.Out.WriteLineAsync( $"The command your enter: {command}" );
                    await Task.Delay( 500 );
                }
            } ).Wait();

            Console.WriteLine( $"AAA" );
            Console.ReadLine();
        }

        #endregion Async and Non-Async Methods examples
    }
}
