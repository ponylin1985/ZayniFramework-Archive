﻿using Microsoft.AspNetCore.Mvc;
using ServiceHost.Client.Proxy;
using ServiceHost.ServerSide;
using ServiceHost.Test.Entity;
using System.Threading.Tasks;
using ZayniFramework.Common;


namespace WebAPI.Test.Controllers
{
    /// <summary>測試用的 ValuesController
    /// </summary>
    [Route( "values" )]
    [ApiController()]
    public class ValuesController : ControllerBase
    {
        #region Private Fields

        /// <summary>測試動作
        /// </summary>
        /// <returns></returns>
        private static readonly MagicTestActionAsync _action = new MagicTestActionAsync();

        /// <summary>服務代理人物件<para/>
        /// 使用服務代理人 Proxy 執行 ServiceAction 的最大其中一個優勢就是:<para/>
        /// 當 InProcess 的 ServiceAction 要採用 Microservice 架構時，拆出去變成一個外部獨立的 microservice 的時候，<para/>
        /// 這邊呼叫端的程式碼是「可以完全不用」任何調整!!! 直接改 serviceClientConfig.json 的 remoteHostType 設定值就搞定了。<para/>
        /// 這才是真正把一些切都抽象化的威力!!!
        /// </summary>
        private static readonly IMyProxy _proxy = MyProxy.GetInstance( "./Configs/serviceClientConfig.json" );

        #endregion Private Fields


        #region Public Methods

        /// <summary>測試用動作
        /// * 採用 Zayni Frameowrk 提供的 Proxy 進行 InProcess 呼叫。
        /// * 1000 個 http request，平均約 0.4754911 消化完成。
        /// * 每秒平均可消化: 1000 / 0.4754911 = 2103.088785468329481 個請求。
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>動作結果</returns>
        [HttpPost()]
        public async Task<Result<MagicDTO>> Post( SomethingDTO request ) => 
            await _proxy.ExecuteAsync<SomethingDTO, MagicDTO>( "MagicTestActionAsync", request );

        /// <summary>測試用動作
        /// * 直接 Invoke 呼叫。
        /// * 效能上極度快的方式，1000 個 http request，平均約 0.2469501 秒消化完成。
        /// * 每秒平均可消化: 1000 / 0.2469501 = 4049.401073334248498 個請求。
        /// </summary>
        /// <param name="request">請求資料載體</param>
        /// <returns>動作結果</returns>
        [HttpPost( "test" )]
        public async Task<Result<MagicDTO>> ExecuteAsync( SomethingDTO request ) => await _action.ExecuteAsync( request );

        #endregion Public Methods
    }
}
