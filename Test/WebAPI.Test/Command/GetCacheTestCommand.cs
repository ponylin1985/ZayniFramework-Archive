using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using ServiceHost.Service.DataAccess;
using ServiceHost.Test.Entity;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace WebAPI.Test
{
    /// <summary>取得快取資料的測試命令 (原始命令: get cache)
    /// </summary>
    public class GetCacheTestCommand : RemoteCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            var key = "zayni:webapi:test-001";

            var g = CacheManager.Get( key, () => new 
            {
                Name   = "Happy Bear",
                Id     = RandomTextHelper.Create( 15 ),
                Age    = 4,
                Sex    = "Boy",
                Emails = new List<string>()
                {
                    "happy.bear@gmail.com",
                    "i_am_happy_bear@gmail.com"
                }
            } );

            if ( !g.Success )
            {
                StdoutErr( $"Get cache data fail. {g.Message}" );
                return Result;
            }

            Stdout( $"{Environment.NewLine}{JsonConvertUtil.Serialize( g )}", ConsoleColor.Yellow );
            Stdout( $"Get cache data success." );
            Result.Success = true;
            return Result;
        }
    }

    /// <summary>取得快取資料的測試命令 (原始命令: get cache)
    /// </summary>
    public class GetCacheTestCommandAsync : RemoteCommandAsync
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            var key = "zayni:webapi:cachemanager.tester";

            var dao = new UserDao();

            var g = await CacheManager.GetAsync( key, async () =>
            {
                var r = await dao.GetAsync( new UserModel(){ Account = "WebAPI.Test.CacheManagerTester" } );
                return r.Success && r.Data.IsNotNull() ? r.Data : null;
            } );

            if ( !g.Success )
            {
                await StdoutErrAsync( $"Get cache data fail. {g.Message}" );
                return Result;
            }

            if ( g.CacheData.IsNull() )
            {
                await StdoutAsync( $"No cache data found." );
                return Result;
            }

            if ( g.CacheData is JsonElement payload )
            {
                await StdoutAsync( $"{Environment.NewLine}{payload.ToString()}", ConsoleColor.Yellow );
            }
            else
            {
                await StdoutAsync( $"{Environment.NewLine}{JsonConvertUtil.Serialize( g )}", ConsoleColor.Yellow );
            }
            
            await StdoutAsync( $"Get cache data success." );
            Result.Success = true;
            return Result;
        }
    }
}