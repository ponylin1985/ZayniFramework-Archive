using System.Threading.Tasks;
using ServiceHost.ServerSide;
using ServiceHost.Test.Entity;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace WebAPI.Test
{
    // 從 Middle.Service 服務端觸發 ServiceEvent 的測試。
    /// <summary>ServiceEvent 訊息發布的測試命令 (原始命令: push)
    /// </summary>
    public class PushCommand : RemoteCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            var messageDTO = new SomethingDTO()
            {
                SomeMessage     = "This is happy bear!",
                SomeMagicNumber = 8833.324,
                LuckyNumber     = 23
            };

            // 第一種寫法:
            // 透過 MiddlewareService.Publish 方法觸發 ServerEvent。
            //MiddlewareService.Publish( "WebAPI.Test.TCPSocket", "HappyPublish", dto );

            // 第二種寫法:
            // 直接建立 ServiceEvent 物件，傳入 serviceHostConfig.json 的服務設定名稱，和訊息資料載體。
            new HappyServiceEvent( "WebAPI.Test.RabbitMQ", messageDTO ).Fire();

            Stdout( $"Publish SomethingingDTO message now..." );
            Stdout( JsonConvertUtil.Serialize( messageDTO ) );
            Result.Success = true;
            return Result;
        }
    }

    // 從 Middle.Service 服務端觸發 ServiceEvent 的測試。
    /// <summary>ServiceEvent 訊息發布的測試命令 (原始命令: push)
    /// </summary>
    public class PushCommandAsync : RemoteCommandAsync
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            var messageDTO = new SomethingDTO()
            {
                SomeMessage     = "This is happy bear!",
                SomeMagicNumber = 8833.324,
                LuckyNumber     = 23
            };

            // 第一種寫法:
            // 透過 MiddlewareService.Publish 方法觸發 ServerEvent。
            //MiddlewareService.Publish( "WebAPI.Test.TCPSocket", "HappyPublish", dto );

            // 第二種寫法:
            // 直接建立 ServiceEvent 物件，傳入 serviceHostConfig.json 的服務設定名稱，和訊息資料載體。
            new HappyServiceEvent( "WebAPI.Test.RabbitMQ", messageDTO ).Fire();

            await StdoutAsync( $"Publish SomethingingDTO message now..." );
            await StdoutAsync( JsonConvertUtil.Serialize( messageDTO ) );
            Result.Success = true;
            return Result;
        }
    }
}