using ServiceHost.Client.Proxy;
using ServiceHost.Test.Entity;
using System;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;


namespace WebAPI.Test
{
    /// <summary>在 WebAPI.Test 專案中執行 InProcessClient 的效能負載測試的命令 (原始命令: performance test {count})
    /// </summary>
    public class InProcessPerformanceTestCommand : RemoteCommand
    {
        /// <summary>服務代理人物件<para/>
        /// 使用服務代理人 Proxy 執行 ServiceAction 的最大其中一個優勢就是:<para/>
        /// 當 InProcess 的 ServiceAction 要採用 Microservice 架構時，拆出去變成一個外部獨立的 microservice 的時候，<para/>
        /// 這邊呼叫端的程式碼是「可以完全不用」任何調整!!! 直接改 serviceClientConfig.json 的 remoteHostType 設定值就搞定了。<para/>
        /// 這才是真正把一些切都抽象化的威力!!!
        /// </summary>
        private static readonly IMyProxy _proxy = MyProxy.GetInstance( "./Configs/serviceClientConfig.json" );

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            var t = CommandText.Trim()?.Split( ' ' );
            int count = 3 == t.Length && int.TryParse( t.LastOrDefault(), out int cmdCount ) ? cmdCount : 1000;

            var reqDTO = new SomethingDTO() 
            {
                SomeMessage     = "InProcess Proxy Performance Test.",
                SomeDate        = DateTime.UtcNow,
                SomeMagicNumber = 44.33D,
                LuckyNumber     = 88,
                IsSuperMan      = true
            };

            DateTime begin = DateTime.UtcNow;

            int i;

            for ( i = 0; i < count; i++ )
            {
                var r = _proxy.Execute<SomethingDTO, MagicDTO>( "MagicTestAction", reqDTO );

                if ( !r.Success )
                {
                    StdoutErr( $"Performance fail at {i + 1}." );
                    return Result;
                }
            }
            
            DateTime end = DateTime.UtcNow;
            var sp = ( end - begin ).TotalSeconds;

            Stdout( $"All success. ExecuteCount: {count}. Total Seconds: {sp}" );
            Result.Success = true;
            return Result;
        }
    }

    /// <summary>在 WebAPI.Test 專案中執行 InProcessClient 的效能負載測試的命令 (原始命令: performance test {count})
    /// </summary>
    public class InProcessPerformanceTestCommandAsync : RemoteCommandAsync
    {
        /// <summary>服務代理人物件<para/>
        /// 使用服務代理人 Proxy 執行 ServiceAction 的最大其中一個優勢就是:<para/>
        /// 當 InProcess 的 ServiceAction 要採用 Microservice 架構時，拆出去變成一個外部獨立的 microservice 的時候，<para/>
        /// 這邊呼叫端的程式碼是「可以完全不用」任何調整!!! 直接改 serviceClientConfig.json 的 remoteHostType 設定值就搞定了。<para/>
        /// 這才是真正把一些切都抽象化的威力!!!
        /// </summary>
        private static readonly IMyProxy _proxy = MyProxy.GetInstance( "./Configs/serviceClientConfig.json" );

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            var t = CommandText.Trim()?.Split( ' ' );
            int count = 3 == t.Length && int.TryParse( t.LastOrDefault(), out int cmdCount ) ? cmdCount : 1000;

            var reqDTO = new SomethingDTO() 
            {
                SomeMessage     = "InProcess Proxy Performance Test.",
                SomeDate        = DateTime.UtcNow,
                SomeMagicNumber = 44.33D,
                LuckyNumber     = 88,
                IsSuperMan      = true
            };

            DateTime begin = DateTime.UtcNow;

            int i;

            for ( i = 0; i < count; i++ )
            {
                var r = await _proxy.ExecuteAsync<SomethingDTO, MagicDTO>( "MagicTestActionAsync", reqDTO );

                if ( !r.Success )
                {
                    await StdoutErrAsync( $"Performance fail at {i + 1}." );
                    return Result;
                }
            }
            
            DateTime end = DateTime.UtcNow;
            var sp = ( end - begin ).TotalSeconds;

            await StdoutAsync( $"All success. ExecuteCount: {count}. Total Seconds: {sp}" );
            Result.Success = true;
            return Result;
        }
    }
}