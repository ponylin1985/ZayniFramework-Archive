﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using NeoSmart.AsyncLock;
using ServiceHost.Service.DataAccess;
using ServiceHost.Test.Entity;
using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Client;
using ZayniFramework.Middle.Service.Grpc;
using ZayniFramework.Middle.Service.Grpc.Client;
using ZayniFramework.Middle.Service.HttpCore;
using ZayniFramework.Middle.Service.HttpCore.Client;
using ZayniFramework.Middle.TelnetService;


namespace WebAPI.Test
{
    // 如果在 ubuntu linux 環境下，使用 dotnet run 執行應用程式發生異常，可以嘗試執行以下 command
    // echo fs.inotify.max_user_instances=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -pv
    // 參考 https://github.com/dotnet/aspnetcore/issues/8449
    // 或是修改 ubuntu /etc/sysctl.conf 設定檔，新增 fs.inotify.max_user_watches=524288 可以解決此問題。
    /// <summary>主程式
    /// </summary>
    public sealed class Program
    {
        #region Private Fields

        /// <summary>重新初始化快取的非同步作業鎖定物件
        /// </summary>
        /// <returns></returns>
        private static readonly AsyncLock _asyncLockCache = new AsyncLock();

        #endregion Private Fields


        #region Declare Main Methods

        /// <summary>程式進入點 (Non-async 的 Main 方法)
        /// </summary>
        /// <param name="args"></param>
        public static void Main_Sync( string[] args )
        {
            // 初始化 Zayni Framework 的 Config 設定
            InitializeConfigs();

            ConsoleLogger.Log( $"Starting zayni webapi and service host application now..." );
            ConsoleLogger.Log( Path.GetDirectoryName( Assembly.GetEntryAssembly().Location ) );

            // 初始化 .NET Core CLR 執行時期的 ThreadPool
            var tp = ThreadPoolSettingsHelper.ApplyConfig();
            ConsoleLogger.Log( $"Thread pool initialize result: {tp.Success}. MinWorkerThreads: {tp.Data.minWorkerThreadsNumber}, MinIOCPThreads: {tp.Data.minIOCPThreadsNumber}." );

            // 註冊 Middle.Service.Client 的 extension module
            RegisterExtensionRemoteClient();
            
            // 啟動 ServiceHost 並且註冊 ServiceAction
            StartServiceHosts();

            // 啟動 ServiceHost 並且註冊 ServiceActionAsync
            Task.Run( async () => await StartServiceHostsAsync() );
            
            // 啟動 ASP.NET Core Kestrl 自我裝載服務
            StartWebHosting( args );

            // 指定當遠端快取重新連線成功的處理委派!
            Task.Run( async () => await InitialCachesAsync() );

            // 啟動 CommandService 服務
            StartCommandServices();
        }

        /// <summary>程式進入點 (Async 的 Main 方法)
        /// </summary>
        /// <param name="args"></param>
        public static async Task Main( string[] args )
        {
            // 初始化 Zayni Framework 的 Config 設定
            InitializeConfigs();

            await ConsoleLogger.LogAsync( $"Starting zayni webapi and service host application now..." );
            await ConsoleLogger.LogAsync( Path.GetDirectoryName( Assembly.GetEntryAssembly().Location ) );

            // 初始化 .NET Core CLR 執行時期的 ThreadPool
            var tp = ThreadPoolSettingsHelper.ApplyConfig();
            await ConsoleLogger.LogAsync( $"Thread pool initialize result: {tp.Success}. MinWorkerThreads: {tp.Data.minWorkerThreadsNumber}, MinIOCPThreads: {tp.Data.minIOCPThreadsNumber}." );

            // 註冊 Middle.Service.Client 的 extension module
            RegisterExtensionRemoteClient();
            
            // 啟動 ServiceHost 並且註冊 ServiceAction
            StartServiceHosts();

            // 啟動 ServiceHost 並且註冊 ServiceActionAsync
            await StartServiceHostsAsync();

            // 啟動 ASP.NET Core Kestrl 自我裝載服務
            StartWebHosting( args );

            // 指定當遠端快取重新連線成功的處理委派!
            await InitialCachesAsync();

            // 啟動 CommandService 服務
            await StartCommandServicesAsync();
        }

        #endregion Declare Main Methods


        #region Private Methods

        /// <summary>初始化 Zayni Framework 的 config 設定檔
        /// </summary>
        private static void InitializeConfigs() 
        {
            // 因為這邊 app.config 的路徑放置在 Config 目錄下，所以一定要自行指定 app.config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location )}/Configs/WebAPI.Test.dll.config";
            ConfigManagement.ConfigFullPath = path;
        }

        /// <summary>註冊 Middle.Service.Client 的 extension module
        /// </summary>
        private static void RegisterExtensionRemoteClient() 
        {
            RemoteClientFactory.RegisterRemoteClientType<gRPCRemoteClient>( "gRPC" ); 
            RemoteClientFactory.RegisterRemoteClientType<HttpCoreRemoteClient>( "HttpCore" ); 
        }

        /// <summary>開啟服務
        /// </summary>
        private static void StartServiceHosts()
        {
            var configPath = "./Configs/serviceHostConfig.json";

            #region 註冊 Middle.Service extension module 

            // 註冊 Middle.Service extension module 的 gRPCRemoteHost IRemoteHost 型別
            MiddlewareService.RegisterRemoteHostType<gRPCRemoteHost>( "gRPC" );

            // 註冊 Middle.Service extension module 的 HttpCoreRemoteHost IRemoteHost 型別
            MiddlewareService.RegisterRemoteHostType<HttpCoreRemoteHost>( "HttpCore" );

            // 註冊 HttpCore remote host 的 全域性 JSON 序列化處理參數 (效能一定會稍微差一點!)
            HttpCoreRemoteHost.JsonSerializerOptions = ( o => 
            {
                o.JsonSerializerOptions.IgnoreNullValues = true;
                o.JsonSerializerOptions.WriteIndented    = true;
            } );

            #endregion 註冊 Middle.Service extension module 

            #region 採用 MiddlewareService 動態載入 assembly 的機制，就不需要指派 InitailzieServiceActionHandler 委派

            // Pony Says: 如果採用 MiddlewareService.StartServiceHosts() 的方式啟動服務，允許不用自行註冊 ServiceAction 動作，框架內部會動態的註冊!
            // 註冊服務端的 ServiceAction 服務動作
            // MiddlewareService.InitializeServiceActionHandler = () => 
            // {
            //     var testAction = new TestAction();
            //     ServiceActionContainerManager.RegisterServiceAction( testAction.Name, testAction.GetType(), configPath );

            //     var magicTestAction = new MagicTestAction();
            //     ServiceActionContainerManager.RegisterServiceAction( magicTestAction.Name, magicTestAction.GetType(), configPath );

            //     var largeDataTestAction = new LargeDataTestAction();
            //     ServiceActionContainerManager.RegisterServiceAction( largeDataTestAction.Name, largeDataTestAction.GetType(), configPath );

            //     var someEventAction = new SomeEventAction();
            //     ServiceActionContainerManager.RegisterServiceAction( someEventAction.Name, someEventAction.GetType(), configPath );

            //     var getUserModelsAction = new GetUserModelsAction();
            //     ServiceActionContainerManager.RegisterServiceAction( getUserModelsAction.Name, getUserModelsAction.GetType(), configPath );

            //     return Result.Create( true );
            // };

            #endregion 採用 MiddlewareService 動態載入 assembly 的機制，就不需要指派 InitailzieServiceActionHandler 委派

            #region 手動註冊 ServiceEventAction

            // var someEventAction = new SomeEventAction();
            // ServiceActionContainerManager.RegisterServiceAction( someEventAction.Name, someEventAction.GetType(), configPath );

            #endregion 手動註冊 ServiceEventAction

            #region 啟動組態設定的 ServiceHost 自我裝載服務
            
            var middlewareService = new MiddlewareService();
            StartServiceHost( middlewareService, configPath );

            #endregion 啟動組態設定的 ServiceHost 自我裝載服務
        }

        /// <summary>透過 MiddlewareServie 啟動組態設定的 ServiceHost 服務
        /// </summary>
        /// <param name="middlewareService">ZayniFramework 框架提供的 MiddlewareService 中介服務</param>
        /// <param name="configPath">serviceHostConfig.json 設定檔路徑</param>
        /// <returns>啟動結果</returns>
        private static IResult StartServiceHost( MiddlewareService middlewareService, string configPath ) 
        {
            // Pony Says: 如果採用 MiddlewareService.StartServiceHosts() 的方式啟動服務，允許不用自行註冊 ServiceAction 動作，框架內部會動態的註冊!
            IResult result = middlewareService.StartServiceHosts( configPath );

            if ( !result.Success )
            {
                ConsoleLogger.LogError( result.Message );
                return result;
            }

            ConsoleLogger.Log( $"All service host started successfully.", ConsoleColor.Green );
            return result;
        }

        /// <summary>開啟非同步的服務
        /// </summary>
        /// <param name="extensionModules">是否註冊並且啟動 Middle.Service Extension Modules 的 Service Host，預設不啟動。</param>
        private static async Task StartServiceHostsAsync( bool extensionModules = false )
        {
            var configPath = "./Configs/serviceHostConfig.json";

            #region 註冊 Middle.Service extension module 

            Check.IsTrue( extensionModules, () => 
            {
                // 註冊 Middle.Service extension module 的 gRPCRemoteHost IRemoteHost 型別
                MiddlewareService.RegisterRemoteHostType<gRPCRemoteHost>( "gRPC" );

                // 註冊 Middle.Service extension module 的 HttpCoreRemoteHost IRemoteHost 型別
                MiddlewareService.RegisterRemoteHostType<HttpCoreRemoteHost>( "HttpCore" );

                // 註冊 HttpCore remote host 的 全域性 JSON 序列化處理參數 (效能一定會稍微差一點!)
                HttpCoreRemoteHost.JsonSerializerOptions = ( o => 
                {
                    o.JsonSerializerOptions.IgnoreNullValues = true;
                    o.JsonSerializerOptions.WriteIndented    = true;
                } );
            } );

            #endregion 註冊 Middle.Service extension module

            #region 手動註冊 ServiceEventAction

            // var someEventAction = new SomeEventAction();
            // ServiceActionContainerManager.RegisterServiceAction( someEventAction.Name, someEventAction.GetType(), configPath );

            #endregion 手動註冊 ServiceEventAction

            #region 啟動組態設定的 ServiceHost 自我裝載服務

            var middlewareService = new MiddlewareService();
            var r = await middlewareService.StartServiceHostsAsync( configPath );

            if ( !r.Success )
            {
                await ConsoleLogger.LogErrorAsync( r.Message );
                throw new Exception( r.Message );
            }

            #endregion 啟動組態設定的 ServiceHost 自我裝載服務
        }

        /// <summary>啟動 ASP.NET Core Kestrel Http Web Host 自我裝載服務
        /// </summary>
        /// <param name="args"></param>
        private static void StartWebHosting( string[] args ) =>
            new Thread( () => 
                Host.CreateDefaultBuilder( args )
                    .ConfigureWebHostDefaults( webBuilder => 
                    {
                        webBuilder
                            .UseUrls( "http://0.0.0.0:5000" )
                            .UseStartup<Startup>();
                    } ).Build().Run() )
                .Start();

        /// <summary>初始化快取
        /// </summary>
        private static Task InitialCachesAsync() 
        {
            async Task ResetCacheAsync() 
            {
                var dao = new UserDao();

                using ( await _asyncLockCache.LockAsync() )
                {
                    var key = "zayni:webapi:cachemanager.tester";

                    var model = new UserModel() 
                    {
                        Account = "WebAPI.Test.CacheManagerTester",
                        Name    = "WebAPI.Test.CacheManagerTester",
                        Age     = 20,
                        Sex     = 0,
                        DOB     = DateTime.Today,
                        IsVip   = false,
                        IsGood  = true
                    };

                    var cacheProp = new CacheProperty()
                    {
                        CacheId         = key,
                        Data            = model,
                        RefreshCallback = () => 
                        {
                            var g = dao.GetAsync( new UserModel() { Account = model.Account } ).GetAwaiter().GetResult();
                            return g.Success && g.Data.IsNotNull() ? g.Data : null;
                        }
                    };

                    bool success;

                    if ( !await CacheManager.ContainsAsync( key ) )
                    {
                        success = await CacheManager.PutAsync( cacheProp );
                    }
                    else
                    {
                        success = await CacheManager.UpdateAsync( cacheProp );
                    }

                    if ( !success )
                    {
                        await ConsoleLogger.LogErrorAsync( $"Reset cache fail. {CacheManager.Message}" );
                        return;
                    }

                    await ConsoleLogger.LogAsync( $"Reset cache success.", ConsoleColor.Green );
                }
            }

            CacheManager.ConnectionRestored = async () => await ResetCacheAsync();
            Task.Run( async () => await ResetCacheAsync() );
            return Task.FromResult( 0 );
        }

        /// <summary>啟動 Command CLI 服務
        /// </summary>
        private static void StartCommandServices() 
        {
            CommandContainer RegisterCommands() 
            {
                return CommandContainer
                    .Build()
                    .Register<PushCommand>( "push", commandText => 0 == string.Compare( commandText.Trim(), "push", true ) )
                    .Register<InProcessPerformanceTestCommand>( "performance", commandText => commandText.Trim().StartsWith( "performance test", true, CultureInfo.InvariantCulture ) )
                    .Register<GetCacheTestCommand>( "get cache", commandText => commandText.Trim().StartsWith( "get cache", true, CultureInfo.InvariantCulture ) )
                    .Register<ExitConsoleCommand>( "exit", commandText => 0 == string.Compare( commandText.Trim(), "exit", true ) )
                    .Register<ClearConsoleCommand>( "cls", commandText => 0 == string.Compare( commandText.Trim(), "cls", true ) )
                    .RegisterUnknow<UnknowRemoteCommand>();
            }

            var commandContainer = RegisterCommands();
            Check.IsTrue( bool.TryParse( ConfigManagement.AppSettings[ "EnableTelnetCommandService" ], out bool enableTelnetCmdService ) ? enableTelnetCmdService : true, () => TelnetCommandService.StartDefaultTelnetService( commandContainer ) );
            Check.IsTrue( bool.TryParse( ConfigManagement.AppSettings[ "EnableConsoleCommandService" ], out bool enableConsoleCmdService ) ? enableConsoleCmdService : false, () => ConsoleCommandService.StartDefaultConsoleService( commandContainer ) );
        }

        /// <summary>啟動 Command CLI 服務
        /// </summary>
        private static async Task StartCommandServicesAsync() 
        {
            async Task<CommandContainer> RegisterCommands() 
            {
                // 真是有夠靠杯的寫法... async/await chaining... 實務專案中不太建議這種寫法...
                return 
                    ( await ( await ( await ( await ( await ( await ( await 
                        CommandContainer
                            .BuildAsync() )
                            .RegisterAsync<PushCommandAsync>( "push", commandText => 0 == string.Compare( commandText.Trim(), "push", true ) ) )
                            .RegisterAsync<InProcessPerformanceTestCommandAsync>( "performance", commandText => commandText.Trim().StartsWith( "performance test", true, CultureInfo.InvariantCulture ) ) )
                            .RegisterAsync<GetCacheTestCommandAsync>( "get cache", commandText => commandText.Trim().StartsWith( "get cache", true, CultureInfo.InvariantCulture ) ) )
                            .RegisterAsync<ExitConsoleCommandAsync>( "exit", commandText => 0 == string.Compare( commandText.Trim(), "exit", true ) ) )
                            .RegisterAsync<ClearConsoleCommandAsync>( "cls", commandText => 0 == string.Compare( commandText.Trim(), "cls", true ) ) )
                            .RegisterUnknowAsync<UnknowRemoteCommandAsync>() );
            }

            var commandContainer = await RegisterCommands();

            if ( bool.TryParse( ConfigManagement.AppSettings[ "EnableTelnetCommandService" ], out bool enableTelnetCmdService ) )
            {
                TelnetCommandService.StartDefaultAsyncTelnetService( commandContainer );
            }

            if ( bool.TryParse( ConfigManagement.AppSettings[ "EnableConsoleCommandService" ], out bool enableConsoleCmdService ) )
            {
                await ConsoleCommandService.StartDefaultConsoleServiceAsync( commandContainer );
            }
        }

        #endregion Private Methods
    }
}
