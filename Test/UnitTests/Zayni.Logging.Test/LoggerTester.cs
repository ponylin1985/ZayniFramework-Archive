﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Reflection;
using System.Threading;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Serialization;


namespace Logging.Test
{
    /// <summary>ZayniFramework 框架 Logger 日誌器的測試類別
    /// </summary>
    [TestClass()]
    public class LoggerTester
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testcontext"></param>
        [AssemblyInitialize()]
        public static void Init( TestContext testcontext ) 
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location )}/Zayni.Logging.Test.dll.config";
            ConfigManagement.ConfigFullPath = path;
        }

        #endregion 測試組件初始化


        /// <summary>寫入一般資訊事件層級的日誌訊息的測試
        /// </summary>
        [TestMethod()]
        [Description( "寫入一般資訊事件層級的日誌訊息的測試" )]
        public void Logger_WriteInformationLog_Test()
        {
            string textLogger = "TextLogWithoutEmail";
            Logger.WriteInformationLog( this, "This is ZayniFramework.Logging unit test.", "Zayni Logging UnitTest", loggerName: textLogger );
            Logger.WriteInformationLog( this, "This is ZayniFramework.Logging unit test.", "Zayni Logging UnitTest" );
            SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
        }

        /// <summary>寫入一般資訊事件層級的日誌訊息的測試，並且有 Email 警示機制的測試
        /// </summary>
        [TestMethod()]
        [Description( "寫入一般資訊事件層級的日誌訊息的測試" )]
        public void Logger_WriteInformationLog_WithEmailNotify_Test()
        {
            string textLogger = "TextLogWithEmail";    // 具有 emailNotifyLoggerName 設定的日誌處理器
            Logger.WriteInformationLog( this, "This is ZayniFramework.Logging unit test.", "Zayni Logging UnitTest", loggerName: textLogger );
            Logger.WriteInformationLog( this, "This is ZayniFramework.Logging unit test.", "Zayni Logging UnitTest" );
            SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );

            var something = new
            {
                OrderId   = 1234,
                OrderType = "LMT",
                LmtPrice  = 444.44,
                Tif       = "GTC",
                Symbol    = "GOOG",
                Exchange  = "NASDAQ",
                SecType   = "STK"
            };

            Logger.WriteInformationLog( this, $"This is ZayniFramework.Logging unit test. {Environment.NewLine}{JsonConvertUtil.SerializeInCamelCase( something )}", "Zayni Logging UnitTest", loggerName: textLogger );
            SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
        }

        /// <summary>寫入錯誤事件層級的日誌訊息的測試，並且有 Email 警示機制的測試
        /// </summary>
        [TestMethod()]
        [Description( "寫入錯誤事件層級的日誌訊息的測試" )]
        public void Logger_WriteErrorLog_WithEmailNotify_Test()
        {
            string textLogger = "TextLogWithEmail";    // 具有 emailNotifyLoggerName 設定的日誌處理器
            Logger.WriteErrorLog( this, "This is ZayniFramework.Logging unit test.", "Zayni Logging UnitTest", loggerName: textLogger );
            Logger.WriteErrorLog( this, "This is ZayniFramework.Logging unit test.", "Zayni Logging UnitTest" );
            SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
        }

        /// <summary>寫入警告事件層級的日誌訊息的測試，並且有 Email 警示機制的測試
        /// </summary>
        [TestMethod()]
        [Description( "寫入警告事件層級的日誌訊息的測試" )]
        public void Logger_WriteWarnLog_WithEmailNotify_Test()
        {
            string textLogger = "TextLogWithEmail";    // 具有 emailNotifyLoggerName 設定的日誌處理器
            Logger.WriteWarningLog( this, "This is ZayniFramework.Logging unit test.", "Zayni Logging UnitTest", loggerName: textLogger );
            Logger.WriteWarningLog( this, "This is ZayniFramework.Logging unit test.", "Zayni Logging UnitTest" );
            SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
        }

        /// <summary>寫入警告事件層級的日誌訊息的測試，並且有 Email 警示機制的測試
        /// </summary>
        [TestMethod()]
        [Description( "寫入警告事件層級的日誌訊息的測試" )]
        public void Logger_WriteExceptionLog_WithEmailNotify_Test()
        {
            string textLogger = "TextLogWithEmail";    // 具有 emailNotifyLoggerName 設定的日誌處理器
            Logger.WriteExceptionLog( this, new ApplicationException( "This is a test application exception from unit test." ), "This is ZayniFramework.Logging unit test.", loggerName: textLogger );
            Logger.WriteExceptionLog( this, new ApplicationException( "This is a test application exception from unit test." ), "This is ZayniFramework.Logging unit test." );
            SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
        }
    }
}
