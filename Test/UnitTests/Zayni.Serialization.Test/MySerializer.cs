﻿using ZayniFramework.Common;


namespace Serialization.Test
{
    /// <summary>測試用的 Serializer
    /// </summary>
    public class MySerializer : ISerializer
    {
        /// <summary>測試用的反序列化
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TResult Deserialize<TResult>( object obj ) => default ( TResult );

        /// <summary>測試用的序列化
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public object Serialize( object obj ) => "AAA";
    }
}
