﻿using System;


namespace Serialization.Test
{
    /// <summary>測試用的 Email 資料模型
    /// </summary>
    [Serializable()]
    public class EmailModel
    {
        /// <summary>哪家牌子的 Email
        /// </summary>
        public virtual string Kind { get; set; }

        /// <summary>Email 位址
        /// </summary>
        public virtual string Address { get; set; }
    }
}
