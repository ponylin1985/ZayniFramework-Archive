﻿using System;
using System.Text.Json.Serialization;

namespace Serialization.Test
{
    /// <summary>測試用的使用者資料模型
    /// </summary>
    [Serializable()]
    public class UserModel
    {
        /// <summary>使用者姓名
        /// </summary>
        [JsonPropertyName( "name" )]
        public virtual string Name { get; set; }

        /// <summary>使用者生日
        /// </summary>
        [JsonPropertyName( "birthday" )]
        public virtual DateTime Birthday { get; set; }

        /// <summary>使用者性別
        /// </summary>
        [JsonPropertyName( "sex" )]
        public virtual int Sex { get; set; }

        /// <summary>帳戶現金餘額
        /// </summary>
        [JsonPropertyName( "cash_balance" )]
        public virtual double CashBalance { get; set; }

        /// <summary>總共有多少小孩
        /// </summary>
        [JsonPropertyName( "total_kids" )]
        public virtual decimal TotalKids { get; set; }

        /// <summary>描述
        /// </summary>
        /// <value></value>
        [JsonPropertyName( "description" )]
        public virtual string Description { get; set; }

        /// <summary>應該要忽略序列化此欄位
        /// </summary>
        /// <value></value>
        [JsonIgnore()]
        public virtual string IgnoreThis { get; set; }
    }
}
