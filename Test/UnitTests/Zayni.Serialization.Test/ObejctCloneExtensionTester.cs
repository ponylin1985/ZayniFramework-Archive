﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ZayniFramework.Common;


namespace Serialization.Test
{
    /// <summary>ObejctCloneExtension 的測試類別
    /// </summary>
    [TestClass()]
    public class ObejctCloneExtensionTester
    {
        // BinaryFormatter serialization performance test result: 153781.096 ms
        /// <summary>使用 BinaryFormatter 序列化進行物件深度複製的測試 (反覆測試)
        /// </summary>
        [TestMethod()]
        [Description( "使用 BinaryFormatter 序列化進行物件深度複製的測試" )]
        public void ObjectClone_BinaryFormatter_Test()
        {
            var obj = new UserModel()
            {
                Name        = "Amber",
                Sex         = 0,
                Birthday    = new DateTime( 1997, 6, 24 ),
                CashBalance = 427783913.76,
                TotalKids   = 1
            };

            var dtBegin = DateTime.Now;

            for ( int i = 0; i < 10000000; i++ )
            {
                UserModel model = obj.Clone<UserModel>();
                Assert.IsNotNull( model );
                Assert.AreEqual( obj.Name, model.Name );
                Assert.AreEqual( obj.Sex, model.Sex );
                Assert.AreEqual( obj.Birthday, model.Birthday );
                Assert.AreEqual( obj.CashBalance, model.CashBalance );
                Assert.AreEqual( obj.TotalKids, model.TotalKids );
            }

            var dtEnd = DateTime.Now;
            var sp = ( dtEnd - dtBegin ).TotalMilliseconds;
            Console.WriteLine( $"BinaryFormatter serialization performance test result: {sp} ms" );
        }

        // Newtonsoft Json.NET serialization performance test result: 53562.105 ms
        /// <summary>使用 Newtonsoft Json.NET 序列化進行物件深度複製的測試 (反覆測試)
        /// </summary>
        [TestMethod()]
        [Description( "使用 Newtonsoft Json.NET 序列化進行物件深度複製的測試" )]
        public void ObjectClone_NewtonsoftJson_Test()
        {
            var obj = new UserModel()
            {
                Name        = "Amber",
                Sex         = 0,
                Birthday    = new DateTime( 1997, 6, 24 ),
                CashBalance = 427783913.76,
                TotalKids   = 1
            };

            var dtBegin = DateTime.Now;

            for ( int i = 0; i < 10000000; i++ )
            {
                UserModel model = obj.CloneObject<UserModel>();
                Assert.IsNotNull( model );
                Assert.AreEqual( obj.Name, model.Name );
                Assert.AreEqual( obj.Sex, model.Sex );
                Assert.AreEqual( obj.Birthday, model.Birthday );
                Assert.AreEqual( obj.CashBalance, model.CashBalance );
                Assert.AreEqual( obj.TotalKids, model.TotalKids );
            }

            var dtEnd = DateTime.Now;
            var sp = ( dtEnd - dtBegin ).TotalMilliseconds;
            Console.WriteLine( $"Newtonsoft Json.NET serialization performance test result: {sp} ms" );
        }

        // Duration: 20s 喔，Jil 雖然還沒到 ZeroFormatter 這麼變態，不過，似乎還算是個小鋼砲喔... XD
        // System.Text.Json serialization performance test result: 25515.598 ms
        /// <summary>使用 System.Text.Json 序列化進行物件深度複製的測試 (反覆測試)
        /// </summary>
        [TestMethod()]
        [Description( "使用 System.Text.Json 序列化進行物件深度複製的測試" )]
        public void ObjectClone_Json_Test()
        {
            var obj = new UserModel()
            {
                Name        = "Amber",
                Sex         = 0,
                Birthday    = new DateTime( 1997, 6, 24 ),
                CashBalance = 427783913.76,
                TotalKids   = 1
            };

            var dtBegin = DateTime.Now;

            for ( int i = 0; i < 10000000; i++ )
            {
                UserModel model = obj.CloneObjectJson<UserModel>();
                Assert.IsNotNull( model );
                Assert.AreEqual( obj.Name, model.Name );
                Assert.AreEqual( obj.Sex, model.Sex );
                Assert.AreEqual( obj.Birthday, model.Birthday );
                Assert.AreEqual( obj.CashBalance, model.CashBalance );
                Assert.AreEqual( obj.TotalKids, model.TotalKids );
            }

            var dtEnd = DateTime.Now;
            var sp = ( dtEnd - dtBegin ).TotalMilliseconds;
            Console.WriteLine( $"System.Text.Json serialization performance test result: {sp} ms" );
        }
    }
}
