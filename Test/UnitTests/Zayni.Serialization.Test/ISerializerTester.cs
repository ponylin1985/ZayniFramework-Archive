using System;
using System.Text.Json;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Common;
using ZayniFramework.Serialization;


namespace Serialization.Test
{
    /// <summary>ISerializer 序列化處理器測試類別
    /// </summary>
    [TestClass()]
    public class ISerializerTester
    {
        /// <summary>註冊 ISerializer 序列化處理器的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "註冊 ISerializer 序列化處理器的測試" )]
        [Description( "註冊 ISerializer 序列化處理器的測試" )]
        public void RegisterSerializer_Test()
        {
            var name = "MySerializer";
            var r = SerializerFactory.Register( name, new MySerializer() );
            Assert.IsTrue( r.Success );

            var serializer = SerializerFactory.Create( name );
            Assert.IsNotNull( serializer );

            var s = serializer.Serialize( new object() );
            Assert.AreEqual( "AAA", s );
        }

        /// <summary>Json.NET 序列化測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Json.NET 序列化測試" )]
        [Description( "Json.NET 序列化測試" )]
        public void Serialize_JsonNet_Test() 
        {
            #region Arrange

            var model = new UserModel()
            {
                Name     = "Kate",
                Birthday = new DateTime( 1998, 2, 14, 15, 24, 36 ).AddMilliseconds( 257 ).ToUtcKind(),
                Sex      = 0
            };

            #endregion Arrange

            #region Act

            var serializer = SerializerFactory.Create( "Json.NET" );
            var s1 = serializer.Serialize( model ) + "";
            var d1 = serializer.Deserialize<UserModel>( s1 );

            #endregion Act

            #region Assert

            Assert.AreEqual( model.Name, d1.Name );
            Assert.AreEqual( model.Birthday, d1.Birthday );
            Assert.AreEqual( model.Sex, d1.Sex );

            #endregion Assert
        }

        /// <summary>System.Text.Json 序列化測試 (將 DateTime 轉換成 UTC Kind)
        /// </summary>
        [TestMethod()]
        [TestCategory( "System.Text.Json 序列化測試  (將 DateTime 轉換成 UTC Kind)" )]
        [Description( "System.Text.Json 序列化測試  (將 DateTime 轉換成 UTC Kind)" )]
        public void Serialize_SystemTextJson_Test() 
        {
            #region Arrange

            var model = new UserModel()
            {
                Name     = "Kate",
                Birthday = new DateTime( 1998, 2, 14, 15, 24, 36 ).AddMilliseconds( 257 ).ToUtcKind(),
                Sex      = 0
            };

            #endregion Arrange

            #region Act

            var serializer = SerializerFactory.Create( "System.Text.Json" );
            var s1 = serializer.Serialize( model ) + "";
            var d1 = serializer.Deserialize<UserModel>( s1 );
            Console.WriteLine( s1 );

            #endregion Act

            #region Assert

            Assert.AreEqual( model.Name, d1.Name );
            Assert.AreEqual( model.Birthday, d1.Birthday );
            Assert.AreEqual( model.Sex, d1.Sex );

            #endregion Assert

            // ===================

            var json = JsonSerialize.SerializeObject( model );
            var obj  = JsonSerialize.DeserializeObject( json );
            var obj2 = JsonSerialize.DeserializeObject<UserModel>( json );
            Console.WriteLine( json );

            Assert.IsNotNull( obj );
            var jsonElement = (JsonElement)obj;
            var name        = jsonElement.GetProperty( "name" ).GetString();
            var birthday    = DateTime.Parse( jsonElement.GetProperty( "birthday" ).GetString() ).ToUniversalTime();
            var sex         = jsonElement.GetProperty( "sex" ).GetInt32();

            Assert.AreEqual( model.Name, name );
            Assert.AreEqual( model.Birthday, birthday );
            Assert.AreEqual( model.Sex, sex );

            Assert.AreEqual( model.Name, obj2.Name );
            Assert.AreEqual( model.Birthday, obj2.Birthday );
            Assert.AreEqual( model.Sex, obj2.Sex );
        }

        /// <summary>XML 序列化測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "XML 序列化測試" )]
        [Description( "XML 序列化測試" )]
        public void Serialize_XML_Test() 
        {
            #region Arrange

            var model = new UserModel()
            {
                Name     = "Kate",
                Birthday = new DateTime( 1998, 2, 14, 15, 24, 36 ).AddMilliseconds( 257 ),
                Sex      = 0
            };

            #endregion Arrange

            #region Act

            var serializer = SerializerFactory.Create( "XML" );
            var s1 = serializer.Serialize( model ) + "";
            var d1 = serializer.Deserialize<UserModel>( s1 );

            #endregion Act

            #region Assert

            Assert.AreEqual( model.Name, d1.Name );
            Assert.AreEqual( model.Birthday, d1.Birthday );
            Assert.AreEqual( model.Sex, d1.Sex );

            #endregion Assert
        }

        /// <summary>BinaryFormatter 序列化測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "BinaryFormatter 序列化測試" )]
        [Description( "BinaryFormatter 序列化測試" )]
        public void Serialize_BinaryFormatter_Test() 
        {
            #region Arrange

            var model = new UserModel()
            {
                Name     = "Kate",
                Birthday = new DateTime( 1998, 2, 14, 15, 24, 36 ).AddMilliseconds( 257 ),
                Sex      = 0
            };

            #endregion Arrange

            #region Act

            var serializer = SerializerFactory.Create( "BinaryFormatter" );
            var s1 = (byte[])serializer.Serialize( model );
            var d1 = serializer.Deserialize<UserModel>( s1 );

            #endregion Act

            #region Assert

            Assert.AreEqual( model.Name, d1.Name );
            Assert.AreEqual( model.Birthday, d1.Birthday );
            Assert.AreEqual( model.Sex, d1.Sex );

            #endregion Assert
        }
    }
}