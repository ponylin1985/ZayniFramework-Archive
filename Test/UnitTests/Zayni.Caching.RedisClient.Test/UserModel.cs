﻿using System;
using ZayniFramework.Caching.RedisClientComponent;


namespace Caching.RedisClientComponent.Test
{
    /// <summary>測試用的使用者資料模型
    /// </summary>
    internal class UserModel
    {
        /// <summary>姓名
        /// </summary>
        [HashObject( Name = "user_name" )]
        public string Name { get; set; }

        /// <summary>年紀
        /// </summary>
        [HashObject( Name = "user_age" )]
        public int Age { get; set; }

        /// <summary>是否單身
        /// </summary>
        [HashObject( Name = "is_single", DefaultValue = "true" )]
        public bool? IsSingle { get; set; }

        /// <summary>生日
        /// </summary>
        [HashObject( Name = "user_dob" )]
        public DateTime Birthday { get; set; }

        /// <summary>帳戶現金餘額
        /// </summary>
        [HashObject( Name = "cash_balance" )]
        public double? CashBalance { get; set; }

        /// <summary>總資產
        /// </summary>
        [HashObject( Name = "total_assets" )]
        public decimal TotalAssets { get; set; }

        /// <summary>生效日
        /// </summary>
        public DateTime? EffectiveDate { get; set; }
    }
}
