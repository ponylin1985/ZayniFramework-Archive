﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Reflection;
using ZayniFramework.Common;
using ZayniFramework.ExceptionHandling;


namespace ExceptionHandling.Test
{
    // dotnet test ./Test/UnitTests/Zayni.ExceptionHandling.Test/Zayni.ExceptionHandling.Test.csproj --no-build --filter ClassName=ExceptionHandling.Test.ExceptionPolicyTester
    /// <summary>例外處理政策的測試類別
    /// </summary>
    [TestClass()]
    public class ExceptionPolicyTester
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testcontext"></param>
        [AssemblyInitialize()]
        public static void Init( TestContext testcontext ) 
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location )}/Zayni.ExceptionHandling.Test.dll.config";
            ConfigManagement.ConfigFullPath = path;
        }

        #endregion 測試組件初始化


        /// <summary>預設文字日誌紀錄的例外處理政策測試
        /// </summary>
        [TestMethod()]
        [Description( "預設文字日誌紀錄的例外處理政策測試" )]
        public void DefaultTextLoggingPolicyTest()
        {
            try
            {
                DoSomethingWrong();
            }
            catch ( Exception ex )
            {
                ExceptionAgent.ExecuteDefaultExceptionPolicy( ex, eventTitle: "AAA" );
            }
        }

        /// <summary>電子郵件寄送的例外處理政策測試
        /// </summary>
        [TestMethod()]
        [Description( "電子郵件寄送的例外處理政策測試" )]
        public void EMailNotifyExceptionPolicyTest()
        {
            try
            {
                DoSomethingWrong();
            }
            catch ( Exception ex )
            {
                ExceptionAgent.ExecuteEMailNotifyExceptionPolicy( ex, eventTitle: "FFF" );
            }
        }

        /// <summary>拋出異常的測試方法
        /// </summary>
        public static void DoSomethingWrong() => throw new ApplicationException( "Opps... Something goes wrong..." );
    }
}
