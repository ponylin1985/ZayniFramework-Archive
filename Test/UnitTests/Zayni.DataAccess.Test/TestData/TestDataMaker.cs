﻿using System;
using System.IO;
using System.Threading.Tasks;


namespace DataAccess.Test.TestData
{
    /// <summary>測試資料建立者
    /// </summary>
    internal class TestDataMaker
    {
        #region 宣告私有的欄位

        /// <summary>測試資料的資料存取類別
        /// </summary>
        /// <returns></returns>
        private static readonly TestDataDao _dao = new TestDataDao();

        /// <summary>建立使用者測試資料的SQL敘述
        /// </summary>
        private string _sqlUser;

        /// <summary>建立使用者連絡電話測試資料的SQL敘述
        /// </summary>
        private string _sqlPhone;

        #endregion 宣告私有的欄位


        #region 宣告內部的方法

        /// <summary>初始話測試資料作業
        /// </summary>
        internal async Task InitializeAsync()
        {
            await LoadTestDataFileAsync();
            await MakeDatabaseTestDataAsync();
        }

        /// <summary>初始話測試資料作業
        /// </summary>
        internal void Initialize()
        {
            LoadTestDataFile();
            MakeDatabaseTestData();
        }

        /// <summary>清除測試資料
        /// </summary>
        internal static async Task ClearTestDataAsync() => await _dao.ClearDataAsync();

        /// <summary>清除測試資料
        /// </summary>
        internal static void ClearTestData() => _dao.ClearData();

        #endregion 宣告內部的方法


        #region 宣告私有的方法

        /// <summary>讀取測試資料
        /// </summary>
        private async Task LoadTestDataFileAsync()
        {
            _sqlUser  = await File.ReadAllTextAsync( Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "TestData", "user_initial.sql" ) );
            _sqlPhone = await File.ReadAllTextAsync( Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "TestData", "user_phone_detail_initial.sql" ) );
        }

        /// <summary>讀取測試資料
        /// </summary>
        private void LoadTestDataFile()
        {
            _sqlUser  = File.ReadAllText( Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "TestData", "user_initial.sql" ) );
            _sqlPhone = File.ReadAllText( Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "TestData", "user_phone_detail_initial.sql" ) );
        }

        /// <summary>寫入建立測試資料
        /// </summary>
        private async Task MakeDatabaseTestDataAsync() => await _dao.MakeDataAsync( _sqlUser, _sqlPhone );

        /// <summary>寫入建立測試資料
        /// </summary>
        private void MakeDatabaseTestData() => _dao.MakeData( _sqlUser, _sqlPhone );

        #endregion 宣告私有的方法
    }
}
