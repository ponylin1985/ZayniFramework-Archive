﻿using Newtonsoft.Json;
using System;
using ZayniFramework.Common.ORM;


namespace DataAccess.Test.TestModel
{
    /// <summary>多個主索引鍵的測試資料模型
    /// </summary>
    [MappingTable( TableName = "FS_UNIT_TEST_USER2" )]
    internal class UserModel2
    {
        /// <summary>帳號 (主索引鍵1)
        /// </summary>
        [JsonProperty( PropertyName = "ACCOUNT_ID" )]
        [TableColumn( ColumnName = "ACCOUNT_ID", IsPrimaryKey = true )]
        public string Account { get; set; }

        /// <summary>使用者代碼 (主索引鍵2)
        /// </summary>
        [JsonProperty( PropertyName = "USER_ID" )]
        [TableColumn( ColumnName = "USER_ID", IsPrimaryKey = true )]
        public string UserId { get; set; }

        /// <summary>姓名
        /// </summary>
        [JsonProperty( PropertyName = "USER_NAME" )]
        [TableColumn( ColumnName = "USER_NAME" )]
        public string UserName { get; set; }

        /// <summary>密碼
        /// </summary>
        [JsonProperty( PropertyName = "USER_PASSWORD" )]
        [TableColumn( ColumnName = "USER_PASSWORD", IsAllowNull = true, DefaultValue = null )]
        public string UserPassword { get; set; }

        /// <summary>性別
        /// </summary>
        [JsonProperty( PropertyName = "SEX" )]
        [TableColumn( ColumnName = "SEX" )]
        public int Sex { get; set; }

        /// <summary>生日
        /// </summary>
        [JsonProperty( PropertyName = "BIRTHDAY" )]
        [TableColumn( ColumnName = "BIRTHDAY", IsAllowNull = true, DefaultValue = null )]
        public DateTime? Birthday { get; set; }

        /// <summary>金額
        /// </summary>
        [JsonProperty( PropertyName = "MONEY" )]
        [TableColumn( ColumnName = "MONEY", IsAllowNull = true, DefaultValue = null )]
        public double? Money { get; set; }

        /// <summary>描述
        /// </summary>
        [JsonProperty( PropertyName = "DESCRIPTION" )]
        [TableColumn( ColumnName = "DESCRIPTION" )]
        public string Description { get; set; }
    }
}
