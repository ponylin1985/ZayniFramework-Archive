﻿using DataAccess.Test.TestModel;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;


namespace DataAccess.Test.TestDao
{
    /// <summary>單元測試用的 BaseMySqlDal 測試類別
    /// </summary>
    internal class UserMySqlDal : BaseMySqlDal
    {
        /// <summary>預設建構子
        /// </summary>
        internal UserMySqlDal() : base( "ZayniUnitTest" )
        {
            // pass
        }

        /// <summary>新增資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>新增結果</returns>
        internal IResult Insert_ExecuteNonQuery( UserModel model )
        {
            var result = Result.Create();

            string sql = @" INSERT INTO FS_UNIT_TEST_USER (
                                  ACCOUNT_ID
                                , NAME
                                , AGE
                                , SEX
                                , BIRTHDAY

                                , IS_VIP
                            ) VALUES (
                                  ?AccountId
                                , ?Name
                                , ?Age
                                , ?Sex
                                , ?Birthday

                                , ?IsVIP
                            ) ";

            using ( MySqlConnection conn = base.CreateOpenConnection() )
            {
                if ( conn.IsNull() )
                {
                    result.Message = base.Message;
                    return result;
                }

                MySqlTransaction trans = base.BeginTransaction( conn );

                using ( MySqlCommand cmd = base.GetSqlStringCommand( conn, sql ) )
                {
                    cmd.Transaction = trans;

                    base.AddInParameter( cmd, "?AccountId", MySqlDbType.VarChar,  model.Account );
                    base.AddInParameter( cmd, "?Name",      MySqlDbType.VarChar,  model.Name );
                    base.AddInParameter( cmd, "?Age",       MySqlDbType.Int32,    model.Age );
                    base.AddInParameter( cmd, "?Sex",       MySqlDbType.Int32,    model.Sex );
                    base.AddInParameter( cmd, "?Birthday",  MySqlDbType.DateTime, model.DOB );

                    base.AddInParameter( cmd, "?IsVIP",     MySqlDbType.Bit,      model.IsVip );

                    int count;

                    if ( !base.ExecuteNonQuery( cmd, out count ) )
                    {
                        result.Message = base.Message;
                        return result;
                    }

                    if ( 1 != count )
                    {
                        result.Message = $"Insert into test data to FS_UNIT_TEST_USER fail.";
                        return result;
                    }

                    if ( !base.Commit( trans ) )
                    {
                        result.Message = base.Message;
                        return result;
                    }
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>查詢資料 LoadData 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<List<UserModel>> Select_LoadData( UserModel query )
        {
            var result = Result.Create<List<UserModel>>();
            List<UserModel> models;

            string sql = @"SELECT ACCOUNT_ID
                                , NAME
                                , AGE
                                , SEX
                                , BIRTHDAY

                                , IS_VIP
                                , DATA_FLAG
                            FROM FS_UNIT_TEST_USER
                                WHERE SEX = ?Sex ";

            using ( MySqlConnection conn = base.CreateConnection() )
            {
                if ( !base.OpenConnection( conn ) )
                {
                    result.Message = base.Message;
                    return result;
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( conn, sql ) )
                {
                    base.AddInParameter( cmd, "?Sex", MySqlDbType.Int32, query.Sex );

                    if ( !base.LoadData( cmd, out models ) )
                    {
                        result.Message = base.Message;
                        return result;
                    }
                }

                conn.Close();
            }

            result.Data    = models;
            result.Success = true;
            return result;
        }

        /// <summary>Master-Detail 查詢的測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<UserModel> Select_MasterDetail( UserModel query )
        {
            var result = Result.Create<UserModel>();

            string sql = @" -- Master Table 的查詢
                            SELECT ACCOUNT_ID
                                 , NAME
                                 , AGE
                                 , SEX
                                 , BIRTHDAY

                                 , IS_VIP
                                 , DATA_FLAG
                            FROM FS_UNIT_TEST_USER
                                WHERE ACCOUNT_ID = ?AccountId;
                           
                           -- Detail Table 的查詢
                           SELECT ACCOUNT_ID
                                , PHONE_TYPE
                                , PHONE_NUMBER
                            FROM FS_UNIT_TEST_USER_PHONE_DETAIL 
                                WHERE ACCOUNT_ID = ?AccountId ";
            
            List<UserModel> models;

            using ( MySqlConnection conn = base.CreateConnection() )
            {
                if ( !base.OpenConnection( conn ) )
                {
                    result.Message = base.Message;
                    return result;
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( conn, sql ) )
                {
                    base.AddInParameter( cmd, "?AccountId", MySqlDbType.VarChar, query.Account );

                    if ( !base.LoadMasterDetailData( cmd, new string[] { "PhoneDetails" }, out models ) )
                    {
                        result.Message = base.Message;
                        return result;
                    }
                }

                conn.Close();
            }

            result.Data    = models.FirstOrDefault();
            result.Success = true;
            return result;
        }

        /// <summary>多個 Select 語法 LoadDataDynamic 的查詢測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<UserModel> Select_LoadDataDynamic( UserModel query )
        {
            var result = Result.Create<UserModel>();

            string sql = @" -- Master Table 的查詢
                            SELECT ACCOUNT_ID
                                 , NAME
                                 , AGE
                                 , SEX
                                 , BIRTHDAY

                                 , IS_VIP
                                 , DATA_FLAG
                            FROM FS_UNIT_TEST_USER
                                WHERE ACCOUNT_ID = ?AccountId;
                           
                           -- Detail Table 的查詢
                           SELECT ACCOUNT_ID
                                , PHONE_TYPE
                                , PHONE_NUMBER
                            FROM FS_UNIT_TEST_USER_PHONE_DETAIL 
                                WHERE ACCOUNT_ID = ?AccountId ";
            
            dynamic r;

            using ( MySqlConnection conn = base.CreateConnection() )
            {
                if ( !base.OpenConnection( conn ) )
                {
                    result.Message = base.Message;
                    return result;
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( conn, sql ) )
                {
                    base.AddInParameter( cmd, "?AccountId", MySqlDbType.VarChar, query.Account );

                    if ( !base.LoadDataDynamic<UserModel>( cmd, new string[] { "User", "PhoneDetails" }, out r ) )
                    {
                        result.Message = base.Message;
                        return result;
                    }
                }

                conn.Close();
            }

            List<UserModel> masters = r.User;
            List<UserPhoneDetailModel> details = r.PhoneDetails;

            if ( masters.IsNullOrEmptyList() )
            {
                result.Message = "No data from database.";
                return result;
            }

            UserModel model    = r.User[ 0 ];
            model.PhoneDetails = r.PhoneDetails;

            result.Data    = model;
            result.Success = true;
            return result;
        }

        /// <summary>多個 Select 語法 LoadDataSet 的查詢測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<UserModel> Select_LoadDataSet( UserModel query )
        {
            var result = Result.Create<UserModel>();

            string sql = @" -- Master Table 的查詢
                            SELECT ACCOUNT_ID
                                 , NAME
                                 , AGE
                                 , SEX
                                 , BIRTHDAY

                                 , IS_VIP
                            FROM FS_UNIT_TEST_USER
                                WHERE ACCOUNT_ID = ?AccountId;
                           
                           -- Detail Table 的查詢
                           SELECT ACCOUNT_ID
                                , PHONE_TYPE
                                , PHONE_NUMBER
                            FROM FS_UNIT_TEST_USER_PHONE_DETAIL 
                                WHERE ACCOUNT_ID = ?AccountId ";
            
            DataSet ds;

            using ( MySqlConnection conn = base.CreateConnection() )
            {
                if ( !base.OpenConnection( conn ) )
                {
                    result.Message = base.Message;
                    return result;
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( conn, sql ) )
                {
                    base.AddInParameter( cmd, "?AccountId", MySqlDbType.VarChar, query.Account );

                    if ( !base.LoadDataSet( cmd, new string[] { "User", "UserPhoneDetail" }, out ds ) )
                    {
                        result.Message = base.Message;
                        return result;
                    }
                }

                conn.Close();
            }

            var dtUser            = ds.Tables[ "User" ];
            var dtUserPhoneDetail = ds.Tables[ "UserPhoneDetail" ];

            var masters = JsonConvert.DeserializeObject<List<UserModel>>( JsonConvert.SerializeObject( dtUser ) );
            var details = JsonConvert.DeserializeObject<List<UserPhoneDetailModel>>( JsonConvert.SerializeObject( dtUserPhoneDetail ) );

            UserModel model    = masters.FirstOrDefault();
            model.PhoneDetails = details;

            result.Data    = model;
            result.Success = true;
            return result;
        }

        /// <summary>查詢純量資料 ExecuteScalar 測試
        /// </summary>
        /// <returns>查詢結果</returns>
        internal IResult<int> SelectCount_ExecuteScalar()
        {
            var result = Result.Create<int>();

            int count = -1;

            string sql = @" SELECT COUNT(*) FROM FS_UNIT_TEST_USER; ";

            using ( MySqlConnection conn = base.CreateConnection() )
            {
                if ( !base.OpenConnection( conn ) )
                {
                    result.Message = base.Message;
                    return result;
                }

                using ( MySqlCommand cmd = base.GetSqlStringCommand( conn, sql ) )
                {
                    count = base.ExecuteScalar( cmd );
                }

                conn.Close();
            }

            result.Data      = count;
            result.Success = true;
            return result;
        }
    }
}
