﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using ZayniFramework.Caching;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Client;
using ZayniFramework.Middle.Service.Grpc.Client;


namespace Caching.Test
{
    /// <summary>CacheManager 的快取管理員測試類別
    /// </summary>
    [TestClass()]
    public class CacheManagerTester
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testcontext"></param>
        [AssemblyInitialize()]
        public static void Init( TestContext testcontext ) 
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location )}/Zayni.Caching.Test.dll.config";
            ConfigManagement.ConfigFullPath = path;

            var tp = ThreadPoolSettingsHelper.ApplyConfig();
            ConsoleLogger.Log( $"Thread pool initialize result: {tp.Success}. MinWorkerThreads: {tp.Data.minWorkerThreadsNumber}, MinIOCPThreads: {tp.Data.minIOCPThreadsNumber}." );

            // 這邊要對 Middle.Service.Client module 註冊 gRPC 通訊，使用 RemoteCache 遠端快取才可以使用 gRPC 通訊技術。
            var r = RemoteClientFactory.RegisterRemoteClientType<gRPCRemoteClient>( "gRPC" ); 

            if ( !r.Success )
            {
                throw new Exception( $"Register gRPC protocal fail. {r.Message}" );
            }
        }

        #endregion 測試組件初始化


        #region 宣告測試初始化事件

        /// <summary>初始化測試資料
        /// </summary>
        /// <param name="context">測試環境上下文</param>
        [ClassInitialize()]
        public static async Task InitializeAsync( TestContext context ) => await CacheManager.ClearAsync();

        /// <summary>清除測試資料
        /// </summary>
        [TestCleanup()]
        public async Task CleanUpAsync() => await CacheManager.ClearAsync();

        #endregion 宣告測試事件


        #region 宣告測試方法

        /// <summary>Hash 快取資料結構的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager Hash 快取資料結構的測試" )]
        public void PutHashProperty_GetHashProperty_Test()
        {
            bool exists = CacheManager.Contains( "ddd" );
            Assert.IsFalse( exists );

            var hashs = new HashProperty[] 
            {
                new HashProperty( "a1", "Fiona 費媽最好了! 人又正!" ),
                new HashProperty( "a2", true ),
                new HashProperty( "a3", new DateTime( 2018, 2, 7 ) ),
                new HashProperty( "a4", 45.26 ),
                new HashProperty( "a5", 500 ),
                new HashProperty( "a6", 3333.77M ),
            };

            bool success = CacheManager.PutHashProperty( "aaa", hashs );
            Assert.IsTrue( success );

            bool exist1 = CacheManager.Contains( "aaa" );
            Assert.IsTrue( exist1 );

            var a1 = CacheManager.GetHashProperty( "aaa", "a1" );
            Assert.AreEqual( "Fiona 費媽最好了! 人又正!", a1.Data + "" );

            var a2 = CacheManager.GetHashProperty( "aaa", "a2" );
            Assert.AreEqual( true, Convert.ToBoolean( a2.Data + "" ) );

            var a3 = CacheManager.GetHashProperty( "aaa", "a3" );
            Assert.AreEqual( new DateTime( 2018, 2, 7 ), Convert.ToDateTime( a3.Data ) );

            var a4 = CacheManager.GetHashProperty( "aaa", "a4" );
            Assert.AreEqual( 45.26, Convert.ToDouble( a4.Data ) );

            var a5 = CacheManager.GetHashProperty( "aaa", "a5" );
            Assert.IsTrue( Convert.ToInt32( a5.Data ) == (int)500 );
            Assert.AreEqual( (int)500, Convert.ToInt32( a5.Data ) );

            var a6 = CacheManager.GetHashProperty( "aaa", "a6" );
            Assert.AreEqual( (decimal)3333.77M, Convert.ToDecimal( a6.Data + "" ) );

            //========================

            success = CacheManager.PutHashProperty( "aaa", new HashProperty[] { new HashProperty( "a1", "Fiona 就是正!" ) } );
            Assert.IsTrue( success );

            a1 = CacheManager.GetHashProperty( "aaa", "a1" );
            Assert.AreEqual( "Fiona 就是正!", a1.Data + "" );
            a2 = CacheManager.GetHashProperty( "aaa", "a2" );
            Assert.AreEqual( true, Convert.ToBoolean( a2.Data + "" ) );
            a3 = CacheManager.GetHashProperty( "aaa", "a3" );
            Assert.AreEqual( new DateTime( 2018, 2, 7 ), Convert.ToDateTime( a3.Data ) );
            a4 = CacheManager.GetHashProperty( "aaa", "a4" );
            Assert.AreEqual( 45.26, Convert.ToDouble( a4.Data ) );

            success = CacheManager.PutHashProperty( "aaa", new HashProperty[] { new HashProperty( "a2", false ) } );
            Assert.IsTrue( success );

            a2 = CacheManager.GetHashProperty( "aaa", "a2" );
            Assert.AreEqual( false, Convert.ToBoolean( a2.Data + "" ) );

            success = CacheManager.PutHashProperty( "aaa", new HashProperty[] { new HashProperty( "a3", 9983.234 ) } );
            Assert.IsTrue( success );

            a3 = CacheManager.GetHashProperty( "aaa", "a3" );
            Assert.AreEqual( 9983.234, Convert.ToDouble( a3.Data ) );

            //========================

            success = CacheManager.PutHashProperty( "aaa", new HashProperty[] { new HashProperty( "b1", "BelleClaire" ) } );
            Assert.IsTrue( success );

            success = CacheManager.PutHashProperty( "aaa", new HashProperty[] 
            {
                new HashProperty( "b2", new DateTime( 2000, 2, 14 ) ),
                new HashProperty( "b3", 532 )
            } );

            Assert.IsTrue( success );

            var b1 = CacheManager.GetHashProperty( "aaa", "b1" );
            Assert.AreEqual( "BelleClaire", b1.Data + "" );
            var b2 = CacheManager.GetHashProperty( "aaa", "b2" );
            Assert.AreEqual( new DateTime( 2000, 2, 14 ), Convert.ToDateTime( b2.Data ) );
            var b3 = CacheManager.GetHashProperty( "aaa", "b3" );
            Assert.AreEqual( 532, Convert.ToInt32( b3.Data ) );

            a1 = CacheManager.GetHashProperty( "aaa", "a1" );
            Assert.AreEqual( "Fiona 就是正!", a1.Data + "" );
            a2 = CacheManager.GetHashProperty( "aaa", "a2" );
            Assert.AreEqual( false, Convert.ToBoolean( a2.Data + "" ) );
            a3 = CacheManager.GetHashProperty( "aaa", "a3" );
            Assert.AreEqual( 9983.234, Convert.ToDouble( a3.Data ) );
            a4 = CacheManager.GetHashProperty( "aaa", "a4" );
            Assert.AreEqual( 45.26, Convert.ToDouble( a4.Data ) );
            a5 = CacheManager.GetHashProperty( "aaa", "a5" );
            Assert.AreEqual( 500, Convert.ToInt32( a5.Data ) );
            a6 = CacheManager.GetHashProperty( "aaa", "a6" );
            Assert.AreEqual( 3333.77M, Convert.ToDecimal( a6.Data + "" ) );

            //========================

            bool r5 = CacheManager.RemoveHashProperty( "aaa", "a5" );
            Assert.IsTrue( r5 );

            a5 = CacheManager.GetHashProperty( "aaa", "a5" );
            Assert.IsFalse( a5.Success );
            
            bool r3 = CacheManager.RemoveHashProperty( "aaa", "a3" );
            Assert.IsTrue( r3 );

            a3 = CacheManager.GetHashProperty( "aaa", "a3" );
            Assert.IsFalse( a3.Success );

            //========================

            var gp = CacheManager.GetHashProperties( "aaa" );
            Assert.IsTrue( gp.Success );
            Assert.IsTrue( gp.Data.IsNotNullOrEmptyArray() );

            bool rv = CacheManager.Remove( "aaa" );
            Assert.IsTrue( rv );

            bool re = CacheManager.Contains( "aaa" );
            Assert.IsFalse( re );

            a1 = CacheManager.GetHashProperty( "aaa", "a1" );
            Assert.IsFalse( a1.Success );

            #if DEBUG
            System.Threading.SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
            #endif
        }

        /// <summary>CacheManager.Contains 的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager.Contains 的測試" )]
        public void Put_Contains_Test()
        {
            bool exists = CacheManager.Contains( "ddd" );
            Assert.IsFalse( exists );

            var hashs = new HashProperty[] 
            {
                new HashProperty( "a1", "Fiona 費媽最好了! 人又正!" ),
                new HashProperty( "a2", true ),
                new HashProperty( "a3", new DateTime( 2018, 2, 7 ) ),
                new HashProperty( "a4", 45.26 ),
                new HashProperty( "a5", 500 ),
                new HashProperty( "a6", 3333.77M ),
            };

            // 先塞一個 HashProperty
            bool success = CacheManager.PutHashProperty( "zayni.test", hashs );
            Assert.IsTrue( success );

            bool has = CacheManager.Contains( "zayni.test" );
            Assert.IsTrue( has );

            // =========================

            var cacheProp = new CacheProperty()
            {
                CacheId = "zayni.test",
                Data    = "rrr"
            };

            // 故意用相同的 CacheId 嘗試塞一個一般的資料進入記憶體快取，應該要失敗!
            var r = CacheManager.Put( cacheProp );
            Assert.IsFalse( r );

            var rv = CacheManager.Remove( "zayni.test" );
            Assert.IsTrue( rv );

            #if DEBUG
            System.Threading.SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
            #endif
        }

        /// <summary>存放與取得完整 HashProperty 強型別資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 存放與取得完整 HashProperty 強型別資料的測試" )]
        public void PutHashObject_GetHashObject_Test()
        {
            string cacheId = "zayni.hash1";

            var model = new UserTestModel()
            {
                Name      = "Amy",
                Age       = 21,
                Sex       = "female",
                Balance   = 3000,
                PnL       = null,
                Something = 123
            };

            var r = CacheManager.PutHashObject( cacheId, model );
            Assert.IsTrue( r.Success );

            bool success = CacheManager.PutHashProperty( cacheId, new HashProperty[] { new HashProperty( "balance", null ) } );
            Assert.IsTrue( success );

            var g = CacheManager.GetHashObject<UserTestModel>( cacheId );
            Assert.IsTrue( g.Success );
            Assert.IsTrue( g.Data.IsNotNull() );

            UserTestModel userModel = g.Data;
            Assert.AreEqual( model.Name, userModel.Name );
            Assert.AreEqual( model.Age, userModel.Age );
            Assert.AreEqual( model.Sex, userModel.Sex );
            Assert.AreEqual( 0, userModel.Balance );
            Assert.IsNull( userModel.PnL );
            Assert.AreEqual( 0, userModel.Something );

            #if DEBUG
            System.Threading.SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
            #endif
        }

        /// <summary>CacheManager 新增快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試" )]
        public void PutDataTest()
        {
            string key = "test01";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var cacheProp = new CacheProperty()
            {
                CacheId         = key,
                Data            = model,
                RefreshCallback = () => new
                {
                    Name   = "Belle Claire",
                    Age    = 23,
                    Sex    = "Nasty Slut",
                    Emails = new List<string>()
                    {
                        "amber@gmail.com",
                        "fuck.me.hard@i.am.a.bitch.com"
                    },
                    Profile = new
                    {
                        Token   = Guid.NewGuid().ToString(),
                        DoB     = new DateTime( 1998, 2, 14 ),
                        Comment = "Come and fuck me."
                    }
                }
            };

            bool success = CacheManager.Put( cacheProp );
            Assert.IsTrue( success );

            #if DEBUG
            System.Threading.SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
            #endif
        }

        /// <summary>CacheManager 新增快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試" )]
        public async Task PutDataTestAsync()
        {
            string key = "test01";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var cacheProp = new CacheProperty()
            {
                CacheId         = key,
                Data            = model,
                RefreshCallback = () => new
                {
                    Name   = "Belle Claire",
                    Age    = 23,
                    Sex    = "Nasty Slut",
                    Emails = new List<string>()
                    {
                        "amber@gmail.com",
                        "fuck.me.hard@i.am.a.bitch.com"
                    },
                    Profile = new
                    {
                        Token   = Guid.NewGuid().ToString(),
                        DoB     = new DateTime( 1998, 2, 14 ),
                        Comment = "Come and fuck me."
                    }
                }
            };

            bool success = await CacheManager.PutAsync( cacheProp );
            Assert.IsTrue( success );

            #if DEBUG
            await Task.Delay( 100 );
            #endif
        }

        /// <summary>CacheManager 新增快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試 2" )]
        public void PutDataTest2()
        {
            string key = "test01-1";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var cacheProp = new CacheProperty()
            {
                CacheId         = key,
                Data            = model,
                RefreshInterval = 2,
                RefreshCallback = () => new
                {
                    Name   = "Belle Claire",
                    Age    = 21,
                    Sex    = "Bitch",
                    Emails = new List<string>()
                    {
                        "belle.claire@gmail.com",
                        "iLoveFatCock@hotmail.com"
                    },
                    Profile = new
                    {
                        Token   = Guid.NewGuid().ToString(),
                        DoB     = new DateTime( 1999, 3, 14 ),
                        Comment = "Fuck me harder."
                    }
                }
            };

            bool success  = CacheManager.Put( cacheProp );
            Assert.IsTrue( success );

            cacheProp.CacheId         = "test01-2";
            cacheProp.RefreshInterval = null;
            bool success2 = CacheManager.Put( cacheProp );
            Assert.IsTrue( success2 );
        }

        /// <summary>CacheManager 新增快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試 2" )]
        public async Task PutDataTest2Async()
        {
            string key = "test01-1";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var cacheProp = new CacheProperty()
            {
                CacheId         = key,
                Data            = model,
                RefreshInterval = 2,
                RefreshCallback = () => new
                {
                    Name   = "Belle Claire",
                    Age    = 21,
                    Sex    = "Bitch",
                    Emails = new List<string>()
                    {
                        "belle.claire@gmail.com",
                        "iLoveFatCock@hotmail.com"
                    },
                    Profile = new
                    {
                        Token   = Guid.NewGuid().ToString(),
                        DoB     = new DateTime( 1999, 3, 14 ),
                        Comment = "Fuck me harder."
                    }
                }
            };

            bool success  = await CacheManager.PutAsync( cacheProp );
            Assert.IsTrue( success );

            cacheProp.CacheId         = "test01-2";
            cacheProp.RefreshInterval = null;
            bool success2 = await CacheManager.PutAsync( cacheProp );
            Assert.IsTrue( success2 );
        }

        /// <summary>CacheManager 新增快取資料的測試 3
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試 3" )]
        public void PutDataTest3()
        {
            string key = "test01-3";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            bool success = CacheManager.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );
        }

        /// <summary>CacheManager 新增快取資料的測試 3
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試 3" )]
        public async Task PutDataTest3Async()
        {
            string key = "test01-3";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            bool success = await CacheManager.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );
        }

        /// <summary>CacheManager 新增快取資料的測試 4
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試 4" )]
        public void PutDataTest4()
        {
            string key = "test01-4";

            var model = new
            {
                Name   = "Kate",
                Age    = 23,
                Sex    = "Girl",
                Emails = new List<string>()
                {
                    "kate@gmail.com",
                    "kate@yahoo.com.tw"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "來幹我嘛... 拜託..."
                }
            };

            bool success = CacheManager.Put( new CacheProperty() { CacheId = key, Data = model, ExpireInterval = 2 } );
            Assert.IsTrue( success );

            #if DEBUG
            System.Threading.SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
            #endif
        }

        /// <summary>CacheManager 新增快取資料的測試 4
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試 4" )]
        public async Task PutDataTest4Async()
        {
            string key = "test01-4";

            var model = new
            {
                Name   = "Kate",
                Age    = 23,
                Sex    = "Girl",
                Emails = new List<string>()
                {
                    "kate@gmail.com",
                    "kate@yahoo.com.tw"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "來幹我嘛... 拜託..."
                }
            };

            bool success = await CacheManager.PutAsync( new CacheProperty() { CacheId = key, Data = model, ExpireInterval = 2 } );
            Assert.IsTrue( success );

            #if DEBUG
            await Task.Delay( 100 );
            #endif
        }

        /// <summary>CacheManager 新增快取資料的測試 5
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試 5" )]
        public void PutExpireDataTest()
        {
            string key = "test444";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            // 注意，在程式中假若期望放入快取中的資料，是真的會有過期功能的話，
            // 傳入的 CacheProperty.RefreshInterval 和 CacheProperty.RefreshCallback 都不可以傳入
            // 都必須為 Null 空值，這樣資料在 CacheManager 中才會有自動回收的效果。
            bool success = CacheManager.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = CacheManager.Get( key );
            Assert.IsTrue( r.Success );
            Assert.IsNotNull( r.CacheData );

            //SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 67 ) );
            //r = CacheManager.Get( key );
            //Assert.IsFalse( r.IsSuccess );
        }

        /// <summary>CacheManager 新增快取資料的測試 5
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 新增快取資料的測試 5" )]
        public async Task PutExpireDataTestAsync()
        {
            string key = "test444";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            // 注意，在程式中假若期望放入快取中的資料，是真的會有過期功能的話，
            // 傳入的 CacheProperty.RefreshInterval 和 CacheProperty.RefreshCallback 都不可以傳入
            // 都必須為 Null 空值，這樣資料在 CacheManager 中才會有自動回收的效果。
            bool success = await CacheManager.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = await CacheManager.GetAsync( key );
            Assert.IsTrue( r.Success );
            Assert.IsNotNull( r.CacheData );

            //SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 67 ) );
            //r = CacheManager.Get( key );
            //Assert.IsFalse( r.IsSuccess );
        }

        /// <summary>CacheManager 取得快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 取得快取資料的測試" )]
        public void GetDataTest()
        {
            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = CacheManager.Put( new CacheProperty() { CacheId = "test02", Data = model } );
            Assert.IsTrue( success );

            var r = CacheManager.Get<UserTestModel>( "test02" );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            // ===================

            var g = CacheManager.Get( "zayni.test", () => model.CloneObject() );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            
            var exists = CacheManager.Contains( "zayni.test" );
            Assert.IsTrue( exists );

            #if DEBUG
            System.Threading.SpinWait.SpinUntil( () => false, TimeSpan.FromSeconds( 1 ) );
            #endif
        }

        /// <summary>CacheManager 取得快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 取得快取資料的測試" )]
        public async Task GetDataTestAsync()
        {
            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = await CacheManager.PutAsync( new CacheProperty() { CacheId = "test02", Data = model } );
            Assert.IsTrue( success );

            var r = await CacheManager.GetAsync<UserTestModel>( "test02" );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            // ===================

            var g = await CacheManager.GetAsync( "zayni.test", () => Task.FromResult( (object)model.CloneObject() ) );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            
            var exists = await CacheManager.ContainsAsync( "zayni.test" );
            Assert.IsTrue( exists );

            #if DEBUG
            await Task.Delay( 100 );
            #endif
        }

        /// <summary>CacheManager 取得快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager 取得快取資料的測試" )]
        [Description( "CacheManager 取得快取資料的測試 2" )]
        public void GetDataTest2()
        {
            string key = "test034";

            bool contains = CacheManager.Contains( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var model2 = new UserTestModel()
            {
                Name = "Vivian",
                Age  = 19,
                Sex  = "Bitch"
            };

            var models = new List<UserTestModel>()
            {
                model, model2
            };

            bool success = CacheManager.Put( new CacheProperty() { CacheId = key, Data = models } );
            Assert.IsTrue( success );

            var r = CacheManager.Get<List<UserTestModel>>( key );

            contains = CacheManager.Contains( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );

            List<UserTestModel> resModels = r.CacheData;
            Assert.IsTrue( resModels.IsNotNullOrEmptyList() );
        }

        /// <summary>CacheManager 取得快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager 取得快取資料的測試" )]
        [Description( "CacheManager 取得快取資料的測試 2" )]
        public async Task GetDataTest2Async()
        {
            string key = "test034";

            bool contains = await CacheManager.ContainsAsync( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var model2 = new UserTestModel()
            {
                Name = "Vivian",
                Age  = 19,
                Sex  = "Bitch"
            };

            var models = new List<UserTestModel>()
            {
                model, model2
            };

            bool success = await CacheManager.PutAsync( new CacheProperty() { CacheId = key, Data = models } );
            Assert.IsTrue( success );

            var r = await CacheManager.GetAsync<List<UserTestModel>>( key );

            contains = await CacheManager.ContainsAsync( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );

            List<UserTestModel> resModels = r.CacheData;
            Assert.IsTrue( resModels.IsNotNullOrEmptyList() );
        }

        /// <summary>CacheManager 取得快取資料的測試 3
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 取得快取資料的測試 3" )]
        public void GetTest_NotExistCacheId_Test()
        {
            var r = CacheManager.Get<UserTestModel>( "no-fucking-way-ha-ha-ha" );
            Assert.IsFalse( r.Success );
            Assert.IsTrue( r.CacheData.IsNull() );
            Assert.IsTrue( r.DataId.IsNullOrEmpty() );
        }

        /// <summary>CacheManager 取得快取資料的測試 3
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 取得快取資料的測試 3" )]
        public async Task GetTest_NotExistCacheId_TestAsync()
        {
            var r = await CacheManager.GetAsync<UserTestModel>( "no-fucking-way-ha-ha-ha" );
            Assert.IsFalse( r.Success );
            Assert.IsTrue( r.CacheData.IsNull() );
            Assert.IsTrue( r.DataId.IsNullOrEmpty() );
        }

        /// <summary>CacheManager 更新快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 更新快取資料的測試" )]
        public void UpdateDataTest()
        {
            string key = "test03";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = CacheManager.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = CacheManager.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsNotNull( r.RefreshTime );

            string    rDataId      = r.DataId;
            DateTime? rRefreshTime = r.RefreshTime;

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            model.Name = "Belle Claire";
            model.Age  = 22;
            model.Sex  = "Russian Bitch";

            System.Threading.SpinWait.SpinUntil( () => false, 20 );
            success = CacheManager.Update( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            r = CacheManager.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsNotNull( r.RefreshTime );
            Assert.AreNotEqual( r.DataId, rDataId );

            rDataId      = r.DataId;
            rRefreshTime = r.RefreshTime;

            target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            var model2 = new UserTestModel()
            {
                Name = "Amy",
                Age  = 19,
                Sex  = "淫蕩小騷B"
            };

            System.Threading.SpinWait.SpinUntil( () => false, 20 );
            success = CacheManager.Update( new CacheProperty() { CacheId = key, Data = model2 } );
            Assert.IsTrue( success );

            r = CacheManager.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsNotNull( r.RefreshTime );
            Assert.AreNotEqual( r.DataId, rDataId );

            target = r.CacheData;
            Assert.AreEqual( model2.Name, target.Name );
            Assert.AreEqual( model2.Age,  target.Age );
            Assert.AreEqual( model2.Sex,  target.Sex );

            success = CacheManager.Remove( key );
            Assert.IsTrue( success );

            bool contains = CacheManager.Contains( key );
            Assert.IsFalse( contains );

            model2.Sex = "Oh come, fuck my ass...";
            success    = CacheManager.Update( new CacheProperty() { CacheId = key, Data = model2 } );
            Assert.IsFalse( success );

            #if DEBUG
            System.Threading.SpinWait.SpinUntil( () => false, 500 );
            #endif
        }

        /// <summary>CacheManager 更新快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 更新快取資料的測試" )]
        public async Task UpdateDataTestAsync()
        {
            string key = "test03";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = await CacheManager.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = await CacheManager.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsNotNull( r.RefreshTime );

            string    rDataId      = r.DataId;
            DateTime? rRefreshTime = r.RefreshTime;

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            model.Name = "Belle Claire";
            model.Age  = 22;
            model.Sex  = "Russian Bitch";

            await Task.Delay( 20 );
            success = await CacheManager.UpdateAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            r = await CacheManager.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsNotNull( r.RefreshTime );
            Assert.AreNotEqual( r.DataId, rDataId );

            rDataId      = r.DataId;
            rRefreshTime = r.RefreshTime;

            target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            var model2 = new UserTestModel()
            {
                Name = "Amy",
                Age  = 19,
                Sex  = "淫蕩小騷B"
            };

            await Task.Delay( 20 );
            success = await CacheManager.UpdateAsync( new CacheProperty() { CacheId = key, Data = model2 } );
            Assert.IsTrue( success );

            r = await CacheManager.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsNotNull( r.RefreshTime );
            Assert.AreNotEqual( r.DataId, rDataId );

            target = r.CacheData;
            Assert.AreEqual( model2.Name, target.Name );
            Assert.AreEqual( model2.Age,  target.Age );
            Assert.AreEqual( model2.Sex,  target.Sex );

            success = await CacheManager.RemoveAsync( key );
            Assert.IsTrue( success );

            bool contains = await CacheManager.ContainsAsync( key );
            Assert.IsFalse( contains );

            model2.Sex = "Oh come, fuck my ass...";
            success    = await CacheManager.UpdateAsync( new CacheProperty() { CacheId = key, Data = model2 } );
            Assert.IsFalse( success );

            #if DEBUG
            await Task.Delay( 150 );
            #endif
        }

        /// <summary>CacheManager 更新快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 更新快取資料的測試 2" )]
        public void UpdateDataTest2()
        {
            string key = "test03";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = CacheManager.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            model.Name = "Belle Claire";
            model.Age  = 22;
            model.Sex  = "Russian Bitch";

            success = CacheManager.Update( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            #if DEBUG
            System.Threading.SpinWait.SpinUntil( () => false, 500 );
            #endif
        }

        /// <summary>CacheManager 更新快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 更新快取資料的測試 2" )]
        public async Task UpdateDataTest2Async()
        {
            string key = "test03";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = await CacheManager.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            model.Name = "Belle Claire";
            model.Age  = 22;
            model.Sex  = "Russian Bitch";

            success = await CacheManager.UpdateAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            #if DEBUG
            await Task.Delay( 100 );
            #endif
        }

        /// <summary>CacheManager 清空快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 清空快取資料的測試" )]
        public void ClearDataTest()
        {
            string key = "test11";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = CacheManager.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = CacheManager.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            model.Age = 22;

            success = CacheManager.Update( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            r = CacheManager.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );

            target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );
            
            success = CacheManager.Put( new CacheProperty() { CacheId = "test12", Data = model } );
            Assert.IsTrue( success );

            success = CacheManager.Put( new CacheProperty() { CacheId = "test13", Data = model } );
            Assert.IsTrue( success );

            success = CacheManager.Put( new CacheProperty() { CacheId = "test14", Data = model } );
            Assert.IsTrue( success );
            
            success = CacheManager.Put( new CacheProperty() { CacheId = "test15", Data = model } );
            Assert.IsTrue( success );

            success = CacheManager.Put( new CacheProperty() { CacheId = "test16", Data = model } );
            Assert.IsTrue( success );

            success = CacheManager.Clear();
            Assert.IsTrue( success );
            Assert.AreEqual( 0, CacheManager.Count );

            bool contains = CacheManager.Contains( key );
            Assert.IsFalse( contains );

            contains = CacheManager.Contains( "test12" );
            Assert.IsFalse( contains );

            contains = CacheManager.Contains( "test13" );
            Assert.IsFalse( contains );

            contains = CacheManager.Contains( "test14" );
            Assert.IsFalse( contains );

            contains = CacheManager.Contains( "test15" );
            Assert.IsFalse( contains );

            contains = CacheManager.Contains( "test16" );
            Assert.IsFalse( contains );

            contains = CacheManager.Contains( "kdsfjoaerkljkju-23548%^$%^#79" );
            Assert.IsFalse( contains );

            #if DEBUG
            System.Threading.SpinWait.SpinUntil( () => false, 500 );
            #endif
        }

        /// <summary>CacheManager 清空快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - CacheManager" )]
        [Description( "CacheManager 清空快取資料的測試" )]
        public async Task ClearDataTestAsync()
        {
            string key = "test11";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = await CacheManager.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = await CacheManager.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            model.Age = 22;

            success = await CacheManager.UpdateAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            r = await CacheManager.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );

            target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );
            
            success = await CacheManager.PutAsync( new CacheProperty() { CacheId = "test12", Data = model } );
            Assert.IsTrue( success );

            success = await CacheManager.PutAsync( new CacheProperty() { CacheId = "test13", Data = model } );
            Assert.IsTrue( success );

            success = await CacheManager.PutAsync( new CacheProperty() { CacheId = "test14", Data = model } );
            Assert.IsTrue( success );
            
            success = await CacheManager.PutAsync( new CacheProperty() { CacheId = "test15", Data = model } );
            Assert.IsTrue( success );

            success = CacheManager.Put( new CacheProperty() { CacheId = "test16", Data = model } );
            Assert.IsTrue( success );

            success = await CacheManager.ClearAsync();
            Assert.IsTrue( success );
            Assert.AreEqual( 0, CacheManager.Count );

            bool contains = await CacheManager.ContainsAsync( key );
            Assert.IsFalse( contains );

            contains = await CacheManager.ContainsAsync( "test12" );
            Assert.IsFalse( contains );

            contains = await CacheManager.ContainsAsync( "test13" );
            Assert.IsFalse( contains );

            contains = await CacheManager.ContainsAsync( "test14" );
            Assert.IsFalse( contains );

            contains = await CacheManager.ContainsAsync( "test15" );
            Assert.IsFalse( contains );

            contains = await CacheManager.ContainsAsync( "test16" );
            Assert.IsFalse( contains );

            contains = await CacheManager.ContainsAsync( "kdsfjoaerkljkju-23548%^$%^#79" );
            Assert.IsFalse( contains );

            #if DEBUG
            await Task.Delay( 100 );
            #endif
        }

        #endregion 宣告測試方法
    }
}
