﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ZayniFramework.Caching;
using ZayniFramework.Common;


namespace Caching.Test
{
    /// <summary>Redis Cache Server 快取服務的測試類別
    /// </summary>
    [TestClass()]
    public class RedisCacheTester
    {
        #region 宣告私有的欄位

        /// <summary>Redis 資料快取
        /// </summary>
        private static RedisCache _redisCache;

        #endregion 宣告私有的欄位


        #region 宣告測試初始化事件

        /// <summary>測試類別初始化
        /// </summary>
        /// <param name="context">測試環境上下文</param>
        [ClassInitialize()]
        public static async Task InitializeAsync( TestContext context )
        {
            _redisCache = RedisCacheContainer.GetDefault().Data;
            await _redisCache.ClearAsync();
        }

        /// <summary>清除測試資料
        /// </summary>
        [TestCleanup()]
        public async Task CleanUpAsync() => await _redisCache.ClearAsync();

        #endregion 宣告測試事件


        #region 宣告測試方法

        /// <summary>RedisCache 新增快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試" )]
        public void PutDataTest()
        {
            string key = "test01";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var cacheProp = new CacheProperty()
            {
                CacheId         = key,
                Data            = model,
                RefreshCallback = () => new
                {
                    Name   = "Belle Claire",
                    Age    = 23,
                    Sex    = "Nasty Slut",
                    Emails = new List<string>()
                    {
                        "amber@gmail.com",
                        "fuck.me.hard@i.am.a.bitch.com"
                    },
                    Profile = new
                    {
                        Token   = Guid.NewGuid().ToString(),
                        DoB     = new DateTime( 1998, 2, 14 ),
                        Comment = "Come and fuck me."
                    }
                }
            };

            bool success = _redisCache.Put( cacheProp );
            Assert.IsTrue( success );

            System.Threading.SpinWait.SpinUntil( () => false, 100 );
        }

        /// <summary>RedisCache 新增快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試" )]
        public async Task PutDataTestAsync()
        {
            string key = "test01";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var cacheProp = new CacheProperty()
            {
                CacheId         = key,
                Data            = model,
                RefreshCallback = () => new
                {
                    Name   = "Belle Claire",
                    Age    = 23,
                    Sex    = "Nasty Slut",
                    Emails = new List<string>()
                    {
                        "amber@gmail.com",
                        "fuck.me.hard@i.am.a.bitch.com"
                    },
                    Profile = new
                    {
                        Token   = Guid.NewGuid().ToString(),
                        DoB     = new DateTime( 1998, 2, 14 ),
                        Comment = "Come and fuck me."
                    }
                }
            };

            bool success = await _redisCache.PutAsync( cacheProp );
            Assert.IsTrue( success );

            await Task.Delay( 100 );
        }

        /// <summary>RedisCache 新增快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試" )]
        public void PutDataTest2()
        {
            string key = "test01-1";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var cacheProp = new CacheProperty()
            {
                CacheId         = key,
                Data            = model,
                RefreshInterval = 2,
                RefreshCallback = () => new
                {
                    Name   = "Belle Claire",
                    Age    = 21,
                    Sex    = "Bitch",
                    Emails = new List<string>()
                    {
                        "belle.claire@gmail.com",
                        "iLoveFatCock@hotmail.com"
                    },
                    Profile = new
                    {
                        Token   = Guid.NewGuid().ToString(),
                        DoB     = new DateTime( 1999, 3, 14 ),
                        Comment = "Fuck me harder."
                    }
                }
            };

            bool success = _redisCache.Put( cacheProp );
            Assert.IsTrue( success );

            cacheProp.CacheId         = "test01-2";
            cacheProp.RefreshInterval = null;
            bool success2 = _redisCache.Put( cacheProp );
            Assert.IsTrue( success2 );
            
            System.Threading.SpinWait.SpinUntil( () => false, 100 );
        }

        /// <summary>RedisCache 新增快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試" )]
        public async Task PutDataTestAsync2()
        {
            string key = "test01-1";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var cacheProp = new CacheProperty()
            {
                CacheId         = key,
                Data            = model,
                RefreshInterval = 2,
                RefreshCallback = () => new
                {
                    Name   = "Belle Claire",
                    Age    = 21,
                    Sex    = "Bitch",
                    Emails = new List<string>()
                    {
                        "belle.claire@gmail.com",
                        "iLoveFatCock@hotmail.com"
                    },
                    Profile = new
                    {
                        Token   = Guid.NewGuid().ToString(),
                        DoB     = new DateTime( 1999, 3, 14 ),
                        Comment = "Fuck me harder."
                    }
                }
            };

            bool success = await _redisCache.PutAsync( cacheProp );
            Assert.IsTrue( success );

            cacheProp.CacheId         = "test01-2";
            cacheProp.RefreshInterval = null;
            bool success2 = await _redisCache.PutAsync( cacheProp );
            Assert.IsTrue( success2 );
            
            await Task.Delay( 100 );
        }

        /// <summary>RedisCache 新增快取資料的測試 3
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試 3" )]
        public void PutDataTest3()
        {
            string key = "test01-3";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var g = RedisCacheContainer.GetDefault();
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;
            Assert.IsNotNull( redisCache );

            bool success = _redisCache.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            System.Threading.SpinWait.SpinUntil( () => false, 100 );
        }

        /// <summary>RedisCache 新增快取資料的測試 3
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試 3" )]
        public async Task PutDataTestAsync3()
        {
            string key = "test01-3";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var g = RedisCacheContainer.GetDefault();
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;
            Assert.IsNotNull( redisCache );

            bool success = await _redisCache.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            await Task.Delay( 100 );
        }

        /// <summary>RedisCache 新增快取資料的測試 4
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試 4" )]
        public void PutDataTest4()
        {
            string key = "test01-4";

            var model = new
            {
                Name   = "Kate",
                Age    = 23,
                Sex    = "Girl",
                Emails = new List<string>()
                {
                    "kate@gmail.com",
                    "kate@yahoo.com.tw"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "來幹我嘛... 拜託..."
                }
            };

            bool success = _redisCache.Put( new CacheProperty() { CacheId = key, Data = model, ExpireInterval = 2 } );
            Assert.IsTrue( success );

            System.Threading.SpinWait.SpinUntil( () => false, 100 );
        }

        /// <summary>RedisCache 新增快取資料的測試 4
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試 4" )]
        public async Task PutDataTestAsync4()
        {
            string key = "test01-4";

            var model = new
            {
                Name   = "Kate",
                Age    = 23,
                Sex    = "Girl",
                Emails = new List<string>()
                {
                    "kate@gmail.com",
                    "kate@yahoo.com.tw"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "來幹我嘛... 拜託..."
                }
            };

            bool success = await _redisCache.PutAsync( new CacheProperty() { CacheId = key, Data = model, ExpireInterval = 2 } );
            Assert.IsTrue( success );

            await Task.Delay( 100 );
        }

        /// <summary>RedisCache 新增快取資料的測試 5
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試 5" )]
        public void PutExpireDataTest()
        {
            string key = "test444";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            // 注意，在程式中假若期望放入快取中的資料，是真的會有過期功能的話，
            // 傳入的 CacheProperty.RefreshInterval 和 CacheProperty.RefreshCallback 都不可以傳入
            // 都必須為 Null 空值，這樣資料在 MemoryCache 中才會有自動回收的效果。
            bool success = _redisCache.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = _redisCache.Get( key );
            Assert.IsTrue( r.Success );
            Assert.IsNotNull( r.CacheData );

            System.Threading.SpinWait.SpinUntil( () => false, 200 );
        }

        /// <summary>RedisCache 新增快取資料的測試 5
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 新增快取資料的測試 5" )]
        public async Task PutExpireDataTestAsync()
        {
            string key = "test444";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            // 注意，在程式中假若期望放入快取中的資料，是真的會有過期功能的話，
            // 傳入的 CacheProperty.RefreshInterval 和 CacheProperty.RefreshCallback 都不可以傳入
            // 都必須為 Null 空值，這樣資料在 MemoryCache 中才會有自動回收的效果。
            bool success = await _redisCache.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = await  _redisCache.GetAsync( key );
            Assert.IsTrue( r.Success );
            Assert.IsNotNull( r.CacheData );

            await Task.Delay( 100 );
        }

        /// <summary>RedisCache 取得快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試" )]
        public void GetDataTest()
        {
            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var g = RedisCacheContainer.Get( "Local-Dev" );
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;

            bool success = _redisCache.Put( new CacheProperty() { CacheId = "test02", Data = model } );
            Assert.IsTrue( success );

            var r = _redisCache.Get<UserTestModel>( "test02" );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            System.Threading.SpinWait.SpinUntil( () => false, 300 );
        }

        /// <summary>RedisCache 取得快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試" )]
        public async Task GetDataTestAsync()
        {
            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var g = RedisCacheContainer.Get( "Local-Dev" );
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;

            bool success = await _redisCache.PutAsync( new CacheProperty() { CacheId = "test02", Data = model } );
            Assert.IsTrue( success );

            var r = await _redisCache.GetAsync<UserTestModel>( "test02" );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            await Task.Delay( 100 );
        }

        /// <summary>取得快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試 2" )]
        public void GetDataTest2()
        {
            string key = "test05";

            var g = RedisCacheContainer.Get( "Local-Dev" );
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;
            Assert.IsNotNull( redisCache );

            bool contains = _redisCache.Contains( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var r = _redisCache.Get<UserTestModel>( key, () => 
            {
                return model;
            } );

            contains = _redisCache.Contains( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            System.Threading.SpinWait.SpinUntil( () => false, 200 );
        }

        /// <summary>取得快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試 2" )]
        public async Task GetDataTestAsync2()
        {
            string key = "test05";

            var g = RedisCacheContainer.Get( "Local-Dev" );
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;
            Assert.IsNotNull( redisCache );

            bool contains = await _redisCache.ContainsAsync( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var r = await _redisCache.GetAsync<UserTestModel>( key, () => Task.FromResult<Object>( model ) );

            contains = await _redisCache.ContainsAsync( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            await Task.Delay( 100 );
        }

        /// <summary>取得快取資料的測試 3
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試 3" )]
        public void GetDataTest3()
        {
            string key = "test033";

            var g = RedisCacheContainer.Get( "Local-Dev" );
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;
            Assert.IsNotNull( redisCache );

            bool contains = _redisCache.Contains( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var model2 = new UserTestModel()
            {
                Name = "Vivian",
                Age  = 19,
                Sex  = "Bitch"
            };

            var r = redisCache.Get<List<UserTestModel>>( key, timeoutMinuntes: 1, handler: () => new List<UserTestModel>() { model, model2 } );

            contains = redisCache.Contains( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );

            List<UserTestModel> resModels = r.CacheData;
            Assert.IsTrue( resModels.IsNotNullOrEmptyList() );
        }

        /// <summary>取得快取資料的測試 3
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試 3" )]
        public async Task GetDataTestAsync3()
        {
            string key = "test033";

            var g = RedisCacheContainer.Get( "Local-Dev" );
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;
            Assert.IsNotNull( redisCache );

            bool contains = await _redisCache.ContainsAsync( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var model2 = new UserTestModel()
            {
                Name = "Vivian",
                Age  = 19,
                Sex  = "Bitch"
            };

            var r = await redisCache.GetAsync<List<UserTestModel>>( key, timeoutMinuntes: 1, handler: () => Task.FromResult<object>( new List<UserTestModel>() { model, model2 } ) );

            contains = await redisCache.ContainsAsync( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );

            List<UserTestModel> resModels = r.CacheData;
            Assert.IsTrue( resModels.IsNotNullOrEmptyList() );
        }

        /// <summary>取得快取資料的測試 4
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試 4" )]
        public void GetDataTest4()
        {
            string key = "test034";

            bool contains = _redisCache.Contains( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var model2 = new UserTestModel()
            {
                Name = "Vivian",
                Age  = 19,
                Sex  = "Bitch"
            };

            var models = new List<UserTestModel>()
            {
                model, model2
            };

            bool success = _redisCache.Put( new CacheProperty() { CacheId = key, Data = models } );
            Assert.IsTrue( success );

            var r = _redisCache.Get<List<UserTestModel>>( key );

            contains = _redisCache.Contains( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );

            List<UserTestModel> resModels = r.CacheData;
            Assert.IsTrue( resModels.IsNotNullOrEmptyList() );
            
            System.Threading.SpinWait.SpinUntil( () => false, 300 );
        }

        /// <summary>取得快取資料的測試 4
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試 4" )]
        public async Task GetDataTestAsync4()
        { 
            string key = "test034";

            bool contains = await _redisCache.ContainsAsync( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var model2 = new UserTestModel()
            {
                Name = "Vivian",
                Age  = 19,
                Sex  = "Bitch"
            };

            var models = new List<UserTestModel>()
            {
                model, model2
            };

            bool success = await _redisCache.PutAsync( new CacheProperty() { CacheId = key, Data = models } );
            Assert.IsTrue( success );

            var r = await _redisCache.GetAsync<List<UserTestModel>>( key );

            contains = await _redisCache.ContainsAsync( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );

            List<UserTestModel> resModels = r.CacheData;
            Assert.IsTrue( resModels.IsNotNullOrEmptyList() );
            
            await Task.Delay( 100 );
        }

        /// <summary>取得快取資料的測試 5
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試 5" )]
        public void GetDataTest5()
        {
            string key = "test035";

            bool contains = _redisCache.Contains( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var model2 = new UserTestModel()
            {
                Name = "Vivian",
                Age  = 19,
                Sex  = "Bitch"
            };

            var r = _redisCache.Get<List<UserTestModel>>( key, handler: () => new List<UserTestModel>() { model, model2 } );

            contains = _redisCache.Contains( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );

            List<UserTestModel> resModels = r.CacheData;
            Assert.IsTrue( resModels.IsNotNullOrEmptyList() );
            
            System.Threading.SpinWait.SpinUntil( () => false, 200 );
        }

        /// <summary>取得快取資料的測試 5
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取資料的測試 5" )]
        public async Task GetDataTestAsync5()
        {
            string key = "test035";

            bool contains = await _redisCache.ContainsAsync( key );
            Assert.IsFalse( contains );

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            var model2 = new UserTestModel()
            {
                Name = "Vivian",
                Age  = 19,
                Sex  = "Bitch"
            };

            var r = await _redisCache.GetAsync<List<UserTestModel>>( key, handler: () => Task.FromResult<object>( new List<UserTestModel>() { model, model2 } ) );

            contains = await _redisCache.ContainsAsync( key );
            Assert.IsTrue( contains );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );

            List<UserTestModel> resModels = r.CacheData;
            Assert.IsTrue( resModels.IsNotNullOrEmptyList() );
            
            await Task.Delay( 100 );
        }

        /// <summary>RedisCache 取得快取 Keys 的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取 Keys 的測試" )]
        public void GetKeysTest1_NoKeyPattern()
        {
            var g = RedisCacheContainer.Get( "Local-Dev" );
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;
            Assert.IsNotNull( redisCache );

            var p1 = redisCache.Put( new CacheProperty()
            {
                CacheId = "AAA",
                Data    = "AAA"
            } );

            var p2 = redisCache.Put( new CacheProperty()
            {
                CacheId = "BBB",
                Data    = "BBB"
            } );

            var p3 = redisCache.Put( new CacheProperty()
            {
                CacheId = "CCC",
                Data    = "CCC"
            } );

            var r = redisCache.GetCacheKeys();
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.Data.IsNotNullOrEmptyList() );
            Assert.AreEqual( 3, r.Data.Count );
            redisCache.Clear();

            // ============================================

            _redisCache.Put( new CacheProperty()
            {
                CacheId = "A1",
                Data    = "A1"
            } );

            _redisCache.Put( new CacheProperty()
            {
                CacheId = "A2",
                Data    = "A2"
            } );

            _redisCache.Put( new CacheProperty()
            {
                CacheId = "A3",
                Data    = "A3"
            } );

            var d = _redisCache.GetCacheKeys();
            Assert.IsTrue( d.Success );
            Assert.IsTrue( d.Data.IsNotNullOrEmptyList() );
            Assert.AreEqual( 3, d.Data.Count );
        }

        /// <summary>RedisCache 取得快取 Keys 的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 取得快取 Keys 的測試 2" )]
        public void GetKeysTest1_KeyPattern()
        {
            var g = RedisCacheContainer.Get( "Local-Dev" );
            Assert.IsTrue( g.Success );

            RedisCache redisCache = g.Data;
            Assert.IsNotNull( redisCache );

            var p1 = redisCache.Put( new CacheProperty()
            {
                CacheId = "Something.AAA",
                Data    = "AAA"
            } );

            var p2 = redisCache.Put( new CacheProperty()
            {
                CacheId = "Something.BBB",
                Data    = "BBB"
            } );

            var p3 = redisCache.Put( new CacheProperty()
            {
                CacheId = "Something.CCC",
                Data    = "CCC"
            } );

            var p4 = redisCache.Put( new CacheProperty()
            {
                CacheId = "HaHaHa.DDD",
                Data    = "DDD"
            } );

            var r = redisCache.GetCacheKeys( "Something." );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.Data.IsNotNullOrEmptyList() );
            Assert.AreEqual( 3, r.Data.Count );

            redisCache.Clear();

            // ============================================

            _redisCache.Put( new CacheProperty()
            {
                CacheId = "FuckYourThroat.A1",
                Data    = "A1"
            } );

            _redisCache.Put( new CacheProperty()
            {
                CacheId = "FuckYourThroat.A2",
                Data    = "A2"
            } );

            _redisCache.Put( new CacheProperty()
            {
                CacheId = "FuckYourThroat.A3",
                Data    = "A3"
            } );

            _redisCache.Put( new CacheProperty()
            {
                CacheId = "FuckYourShitHole.A4",
                Data    = "A4"
            } );

            var d = _redisCache.GetCacheKeys( "FuckYourThroat." );
            Assert.IsTrue( d.Success );
            Assert.IsTrue( d.Data.IsNotNullOrEmptyList() );
            Assert.AreEqual( 3, d.Data.Count );

            System.Threading.SpinWait.SpinUntil( () => false, 200 );
        }

        /// <summary>RedisCache 更新快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 更新快取資料的測試" )]
        public void UpdateDataTest()
        {
            string key = "test03";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = _redisCache.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = _redisCache.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsTrue( r.RefreshTime.IsNotNull() );

            string   rDataId      = r.DataId;
            DateTime? rRefreshTime = r.RefreshTime;

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            model.Name = "Belle Claire";
            model.Age  = 22;
            model.Sex  = "Russian Bitch";

            //SpinWait.SpinUntil( () => false, 20 );
            success = _redisCache.Update( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            r = _redisCache.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsTrue( r.RefreshTime.IsNotNull() );
            Assert.AreNotEqual( r.DataId, rDataId );
            Assert.AreNotEqual( r.RefreshTime, rRefreshTime );

            rDataId      = r.DataId;
            rRefreshTime = r.RefreshTime;

            target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            var model2 = new UserTestModel()
            {
                Name = "Amy",
                Age  = 19,
                Sex  = "淫蕩小騷B"
            };

            System.Threading.SpinWait.SpinUntil( () => false, 20 );
            success = _redisCache.Update( new CacheProperty() { CacheId = key, Data = model2 } );
            Assert.IsTrue( success );

            r = _redisCache.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsTrue( r.RefreshTime.IsNotNull() );
            Assert.AreNotEqual( r.DataId, rDataId );
            Assert.AreNotEqual( r.RefreshTime, rRefreshTime );

            target = r.CacheData;
            Assert.AreEqual( model2.Name, target.Name );
            Assert.AreEqual( model2.Age,  target.Age );
            Assert.AreEqual( model2.Sex,  target.Sex );

            success = _redisCache.Remove( key );
            Assert.IsTrue( success );

            bool contains = _redisCache.Contains( key );
            Assert.IsFalse( contains );

            model2.Sex = "Oh come, fuck my ass...";
            success    = _redisCache.Update( new CacheProperty() { CacheId = key, Data = model2 } );
            Assert.IsFalse( success );

            System.Threading.SpinWait.SpinUntil( () => false, 200 );
        }

        /// <summary>RedisCache 更新快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 更新快取資料的測試" )]
        public async Task UpdateDataTestAsync()
        {
            string key = "test03";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = await _redisCache.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = await _redisCache.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsTrue( r.RefreshTime.IsNotNull() );

            string    rDataId      = r.DataId;
            DateTime? rRefreshTime = r.RefreshTime;

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            model.Name = "Belle Claire";
            model.Age  = 22;
            model.Sex  = "Russian Bitch";

            //SpinWait.SpinUntil( () => false, 20 );
            success = await _redisCache.UpdateAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            r = await _redisCache.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsTrue( r.RefreshTime.IsNotNull() );
            Assert.AreNotEqual( r.DataId, rDataId );
            Assert.AreNotEqual( r.RefreshTime, rRefreshTime );

            rDataId      = r.DataId;
            rRefreshTime = r.RefreshTime;

            target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            var model2 = new UserTestModel()
            {
                Name = "Amy",
                Age  = 19,
                Sex  = "淫蕩小騷B"
            };

            await Task.Delay( 20 );
            success = _redisCache.Update( new CacheProperty() { CacheId = key, Data = model2 } );
            Assert.IsTrue( success );

            r = await _redisCache.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            Assert.IsTrue( r.RefreshTime.IsNotNull() );
            Assert.AreNotEqual( r.DataId, rDataId );
            Assert.AreNotEqual( r.RefreshTime, rRefreshTime );

            target = r.CacheData;
            Assert.AreEqual( model2.Name, target.Name );
            Assert.AreEqual( model2.Age,  target.Age );
            Assert.AreEqual( model2.Sex,  target.Sex );

            success = await _redisCache.RemoveAsync( key );
            Assert.IsTrue( success );

            bool contains = await _redisCache.ContainsAsync( key );
            Assert.IsFalse( contains );

            model2.Sex = "Oh come, fuck my ass...";
            success    = await _redisCache.UpdateAsync( new CacheProperty() { CacheId = key, Data = model2 } );
            Assert.IsFalse( success );

            await Task.Delay( 100 );
        }

        /// <summary>RedisCache 更新快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 更新快取資料的測試 2" )]
        public void UpdateDataTest2()
        {
            string key = "test03";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = _redisCache.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            model.Name = "Belle Claire";
            model.Age  = 22;
            model.Sex  = "Russian Bitch";

            success = _redisCache.Update( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );
            
            System.Threading.SpinWait.SpinUntil( () => false, 200 );
        }

        /// <summary>RedisCache 更新快取資料的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 更新快取資料的測試 2" )]
        public async Task UpdateDataTestAsync2()
        {
            string key = "test03";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = await _redisCache.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            model.Name = "Belle Claire";
            model.Age  = 22;
            model.Sex  = "Russian Bitch";

            success = await _redisCache.UpdateAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );
            
            await Task.Delay( 100 );
        }

        /// <summary>RedisCache 清空快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 清空快取資料的測試" )]
        public void ClearDataTest()
        {
            string key = "test11";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = _redisCache.Put( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = _redisCache.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            model.Age = 22;

            success = _redisCache.Update( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            r = _redisCache.Get<UserTestModel>( key );
            Assert.IsTrue( r.Success );

            target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );
            
            success = _redisCache.Put( new CacheProperty() { CacheId = "test12", Data = model } );
            Assert.IsTrue( success );

            success = _redisCache.Put( new CacheProperty() { CacheId = "test13", Data = model } );
            Assert.IsTrue( success );

            success = _redisCache.Put( new CacheProperty() { CacheId = "test14", Data = model } );
            Assert.IsTrue( success );
            
            success = _redisCache.Put( new CacheProperty() { CacheId = "test15", Data = model } );
            Assert.IsTrue( success );

            success = _redisCache.Put( new CacheProperty() { CacheId = "test16", Data = model } );
            Assert.IsTrue( success );

            success = _redisCache.Clear();
            Assert.IsTrue( success );
            Assert.AreEqual( 0, _redisCache.Count );

            bool contains = _redisCache.Contains( key );
            Assert.IsFalse( contains );

            contains = _redisCache.Contains( "test12" );
            Assert.IsFalse( contains );

            contains = _redisCache.Contains( "test13" );
            Assert.IsFalse( contains );

            contains = _redisCache.Contains( "test14" );
            Assert.IsFalse( contains );

            contains = _redisCache.Contains( "test15" );
            Assert.IsFalse( contains );

            contains = _redisCache.Contains( "test16" );
            Assert.IsFalse( contains );

            contains = _redisCache.Contains( "kdsfjoaerkljkju-23548%^$%^#79" );
            Assert.IsFalse( contains );
        }

        /// <summary>RedisCache 清空快取資料的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Caching - RedisCache" )]
        [Description( "RedisCache 清空快取資料的測試" )]
        public async Task ClearDataTestAsync()
        {
            string key = "test11";

            var model = new UserTestModel()
            {
                Name = "Amber",
                Age  = 23,
                Sex  = "Nasty Slut"
            };

            bool success = await _redisCache.PutAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            var r = await _redisCache.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );

            UserTestModel target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );

            model.Age = 22;

            success = await _redisCache.UpdateAsync( new CacheProperty() { CacheId = key, Data = model } );
            Assert.IsTrue( success );

            r = await _redisCache.GetAsync<UserTestModel>( key );
            Assert.IsTrue( r.Success );

            target = r.CacheData;
            Assert.AreEqual( model.Name, target.Name );
            Assert.AreEqual( model.Age, target.Age );
            Assert.AreEqual( model.Sex, target.Sex );
            
            success = await _redisCache.PutAsync( new CacheProperty() { CacheId = "test12", Data = model } );
            Assert.IsTrue( success );

            success = await _redisCache.PutAsync( new CacheProperty() { CacheId = "test13", Data = model } );
            Assert.IsTrue( success );

            success = await _redisCache.PutAsync( new CacheProperty() { CacheId = "test14", Data = model } );
            Assert.IsTrue( success );
            
            success = await _redisCache.PutAsync( new CacheProperty() { CacheId = "test15", Data = model } );
            Assert.IsTrue( success );

            success = await _redisCache.PutAsync( new CacheProperty() { CacheId = "test16", Data = model } );
            Assert.IsTrue( success );

            success = await _redisCache.ClearAsync();
            Assert.IsTrue( success );
            Assert.AreEqual( 0, _redisCache.Count );

            bool contains = await _redisCache.ContainsAsync( key );
            Assert.IsFalse( contains );

            contains = await _redisCache.ContainsAsync( "test12" );
            Assert.IsFalse( contains );

            contains = await _redisCache.ContainsAsync( "test13" );
            Assert.IsFalse( contains );

            contains = await _redisCache.ContainsAsync( "test14" );
            Assert.IsFalse( contains );

            contains = await _redisCache.ContainsAsync( "test15" );
            Assert.IsFalse( contains );

            contains = await _redisCache.ContainsAsync( "test16" );
            Assert.IsFalse( contains );

            contains = await _redisCache.ContainsAsync( "kdsfjoaerkljkju-23548%^$%^#79" );
            Assert.IsFalse( contains );
        }

        /// <summary>RedisCache 效能測試
        /// * 在 ubuntu linux 18.04 虛擬機器上進行測試， 2 CPU + 8 GB Memory，redis server 為此 ubuntu 作業系統下的 docker container。
        /// * 進行 10000 次的 Put 與 Get 的效能測試結果，平均回應時間大約 0.9 ~ 1.1 ms，詳細測試結果如下:
        /// * RedisCache performance test result: 9596.1658  ms. Average response time: 0.959 ms.
        /// * RedisCache performance test result: 9837.9037  ms. Average response time: 0.983 ms.
        /// * RedisCache performance test result: 11234.9721 ms. Average response time: 1.123 ms.
        /// * RedisCache performance test result: 10253.712  ms. Average response time: 1.025 ms.
        /// * RedisCache performance test result: 10800.4056 ms. Average response time: 1.080 ms.
        /// </summary>
        [TestMethod()]
        [TestCategory( "RedisCache-PerformanceTest" )]
        public void PerformanceTest()
        {
            string key = "redis-performance-test-";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var dtBegin = DateTime.Now;

            for ( int i = 0; i < 1; i++ )
            {
                var cacheKey = $"{key}{i}";

                var cacheProp = new CacheProperty()
                {
                    CacheId = cacheKey,
                    Data    = model
                };

                bool success = _redisCache.Put( cacheProp );
                Assert.IsTrue( success );

                var r = _redisCache.Get( cacheKey );
                Assert.IsTrue( r.Success );
                Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            }

            var dtEnd = DateTime.Now;
            var sp = ( dtEnd - dtBegin ).TotalMilliseconds;
            Console.WriteLine( $"RedisCache performance test result: {sp} ms" );
        }

        // dotnet test ./Test/UnitTests/Zayni.Caching.Test/Zayni.Caching.Test.csproj --no-build --filter TestCategory=RedisCache-PerformanceTestAsync
        /// <summary>RedisCache 效能測試
        /// * 在 ubuntu linux 18.04 虛擬機器上進行測試， 2 CPU + 8 GB Memory，redis server 為此 ubuntu 作業系統下的 docker container。
        /// * 進行 10000 次的 Put 與 Get 的效能測試結果，平均回應時間大約 0.7 ~ 0.8 ms，詳細測試結果如下:
        /// * RedisCache performance test result: 8680.9446 ms. Average response time: 0.868 ms.
        /// * RedisCache performance test result: 8712.0025 ms. Average response time: 0.871 ms.
        /// * RedisCache performance test result: 8447.2811 ms. Average response time: 0.844 ms.
        /// * RedisCache performance test result: 7974.9865 ms. Average response time: 0.797 ms.
        /// * RedisCache performance test result: 7955.0423 ms. Average response time: 0.795 ms.
        /// </summary>
        [TestMethod()]
        [TestCategory( "RedisCache-PerformanceTestAsync" )]
        public async Task PerformanceTestAsync()
        {
            string key = "redis-performance-test-";

            var model = new
            {
                Name   = "Amber",
                Age    = 23,
                Sex    = "Nasty Slut",
                Emails = new List<string>()
                {
                    "amber@gmail.com",
                    "fuck.me.hard@i.am.a.bitch.com"
                },
                Profile = new
                {
                    Token   = Guid.NewGuid().ToString(),
                    DoB     = new DateTime( 1998, 2, 14 ),
                    Comment = "Come and fuck me."
                }
            };

            var dtBegin = DateTime.Now;

            for ( int i = 0; i < 1; i++ )
            {
                var cacheKey = $"{key}{i}";

                var cacheProp = new CacheProperty()
                {
                    CacheId = cacheKey,
                    Data    = model
                };

                bool success = await _redisCache.PutAsync( cacheProp );
                Assert.IsTrue( success );

                var r = await _redisCache.GetAsync( cacheKey );
                Assert.IsTrue( r.Success );
                Assert.IsTrue( r.DataId.IsNotNullOrEmpty() );
            }

            var dtEnd = DateTime.Now;
            var sp = ( dtEnd - dtBegin ).TotalMilliseconds;
            Console.WriteLine( $"RedisCache performance test result: {sp} ms" );
        }

        #endregion 宣告測試方法
    }
}
