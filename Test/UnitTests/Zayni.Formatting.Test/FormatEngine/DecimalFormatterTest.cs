﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    //  20130304 Created by Pony
    /// <summary>DecimalFormatter的單元測試類別
    /// </summary>
    [TestClass]
    public class DecimalFormatterTest
    {
        /// <summary>建構子
        /// </summary>
        public DecimalFormatterTest()
        {
            // pass
        }


        #region TestContext屬性

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion TestContext屬性


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [Description( "對DecimalFormatter的正常測試，小數位數三位" )]
        [TestMethod()]
        public void TryFormatTest()
        {
            DecimalFormatter formatter = new DecimalFormatter();

            decimal target    = 2548.86985M;
            string  formatted = "";
            string  message   = "";

            FormatInfo info = new FormatInfo 
            {
                TypeName     = "Decimal",
                FormatString = "{0:N3}"     // 自動四捨五入，小數位數3位
            };

            bool actual = formatter.TryFormat( target, info, out formatted, out message );
            Assert.IsTrue( actual );

            string expected = "2,548.870";
            Assert.AreEqual( expected, formatted, "格式化結果錯誤" );
        }

        [Description( "對DecimalFormatter的正常測試，小數位數兩位" )]
        [TestMethod()]
        public void TryFormatTest2()
        {
            DecimalFormatter formatter = new DecimalFormatter();

            decimal target    = 255574448.84233M;
            string  formatted = "";
            string  message   = "";

            FormatInfo info = new FormatInfo 
            {
                TypeName     = "Decimal",
                FormatString = "{0:N2}"     // 自動四捨五入，小數位數2位
            };

            bool actual = formatter.TryFormat( target, info, out formatted, out message );
            Assert.IsTrue( actual );

            string expected = "255,574,448.84";
            Assert.AreEqual( expected, formatted, "格式化結果錯誤" );
        }

        [Description( "對DecimalFormatter的反向測試" )]
        [TestMethod()]
        public void TryFormatOppositeTest()
        {
            DecimalFormatter formatter = new DecimalFormatter();

            decimal target    = 255574448.84233M;
            string  formatted = "";
            string  message   = "";

            FormatInfo info = new FormatInfo 
            {
                TypeName     = "Decimal",      
                FormatString = "JKJIJIJIJ"     // 亂七八糟的格式化字串
            };

            bool actual = formatter.TryFormat( target, info, out formatted, out message );
            Assert.IsTrue( actual );

            string expected = "255,574,448.84";
            Assert.AreNotEqual( expected, formatted, "格式化結果錯誤" );
        }

        [Description( "對DecimalFormatter的單元測試，傳入目標型別為double" )]
        [TestMethod()]
        public void TryFormatOppositeTest2()
        {
            DecimalFormatter formatter = new DecimalFormatter();

            double target2    = 255574448.84233;   // 故意傳入一個double的目標型別
            string formatted2 = "";
            string message2   = "";

            FormatInfo info2 = new FormatInfo 
            {
                TypeName     = "Decimal",
                FormatString = "{0:N2}"
            };

            bool actual2 = formatter.TryFormat( target2, info2, out formatted2, out message2 );
            Assert.IsTrue( actual2 );

            string expected = "255,574,448.84";
            Assert.AreEqual( expected, formatted2, "格式化結果錯誤" );
        }

        [Description( "對DecimalFormatter的例外測試" )]
        [TestMethod()]
        public void TryFormatExceptionTest()
        {
            DecimalFormatter formatter = new DecimalFormatter();

            decimal target    = 255574448.84233M;
            string  formatted = "";
            string  message   = "";

            FormatInfo info = new FormatInfo 
            {
                TypeName     = "Decimal",      
                FormatString = null         // 故意傳入Null
            };

            bool actual = formatter.TryFormat( target, info, out formatted, out message );
            Assert.IsFalse( actual );
        }

        [Description( "對DecimalFormatter的例外測試" )]
        [TestMethod()]
        public void TryFormatExceptionTest2()
        {
            DecimalFormatter formatter = new DecimalFormatter();

            object target2     = null;      // 故意傳入一個Null值
            string formatted2 = "";
            string message2   = "";

            FormatInfo info2 = new FormatInfo 
            {
                TypeName     = "Decimal",
                FormatString = "{0:N2}"
            };

            bool actual2 = formatter.TryFormat( target2, info2, out formatted2, out message2 );
            Assert.IsFalse( actual2 );
        }
    }
}
