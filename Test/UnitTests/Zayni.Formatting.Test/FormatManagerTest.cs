﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    // dotnet test ./Test/UnitTests/Zayni.Formatting.Test/Zayni.Formatting.Test.csproj --no-build --filter ClassName=Formatting.Test.FormatManagerTest
    /// <summary>
    ///這是 FormatManagerTest 的測試類別，應該包含
    ///所有 FormatManagerTest 單元測試
    ///</summary>
    [TestClass()]
    public class FormatManagerTest
    {
        #region ToCurrencyString( decimal, int) 單元測試

        [Description( "對ToCurrencyString的正常測試，小數位數一位" )]
        [TestMethod()]
        public void ToCurrencyStringTest()
        {
            Decimal target = 255574448.84233M;
            int decLength  = 1;

            FormatResult r = FormatManager.ToCurrencyString( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.8";
            Assert.AreEqual ( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToCurrencyString的正常測試，小數位數三位" )]
        [TestMethod()]
        public void ToCurrencyStringTest2()
        {
            Decimal target = 255574448.84233M;
            int decLength  = 3;

            FormatResult r = FormatManager.ToCurrencyString( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.842";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToCurrencyString的反向測試，輸入錯誤小數位數長度" )]
        [TestMethod()]
        public void ToCurrencyStringOppositeTest()
        {
            Decimal target = 255574448.84233M;
            int decLength  = -3;

            FormatResult r = FormatManager.ToCurrencyString( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.842";
            Assert.AreNotEqual( expected, r.Result, "格式化結果錯誤" );
        }
        
        #endregion ToCurrencyString( decimal, int) 單元測試

        #region ToCurrencyString(decimal, decimal=0M) 單元測試

        [Description( "對ToCurrencyString的正常測試，無輸入小數點位數長度" )]
        [TestMethod()]
        public void ToCurrencyString_DecimalDotLengthTest()
        {
            Decimal target = 255574448.84233M;

            FormatResult r = FormatManager.ToCurrencyString( target );
            Assert.IsTrue( r.Success );

            string expected = "255,574,449";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToCurrencyString的正常測試，小數位數三位" )]
        [TestMethod()]
        public void ToCurrencyString_DecimalDotLengthTest2()
        {
            Decimal target    = 255574448.84233M;
            Decimal decLength = 3M;

            FormatResult r = FormatManager.ToCurrencyString( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.842";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToCurrencyString的反向測試，輸入錯誤小數位數長度" )]
        [TestMethod()]
        public void ToCurrencyString_DecimalDotLengthOppositeTest()
        {
            Decimal target    = 255574448.84233M;
            Decimal decLength = 0.3M;   // 錯誤的小數位數長度

            FormatResult r = FormatManager.ToCurrencyString( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.842";
            Assert.AreNotEqual( expected, r.Result, "格式化結果錯誤" );
        }

        #endregion ToCurrencyString(decimal, decimal=0M) 單元測試

        #region ToNumber( decimal,int=0 ) 單元測試

        [Description( "對ToNumber的測試，無小數" )]
        [TestMethod()]
        public void ToNumber_DecimalTargetTest()
        {
            Decimal target = 255574448.84233M;
            int decLength  = 0;

            FormatResult r = FormatManager.ToNumber( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,449";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToNumber的測試，小數位數二位" )]
        [TestMethod()]
        public void ToNumber_DecimalTargetTest2()
        {
            Decimal target = 255574448.84233M;
            int decLength  = 2;

            FormatResult r = FormatManager.ToNumber( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.84";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToNumber的反向測試，輸入錯誤小數位數長度" )]
        [TestMethod()]
        public void ToNumber_DecimalTargetOppositeTest()
        {
            Decimal target = 255574448.84233M;
            int decLength = -2; // 小數位數資料為負Int值.

            FormatResult r = FormatManager.ToNumber( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.84";
            Assert.AreNotEqual( expected, r.Result, "格式化結果錯誤" );
        }

        #endregion ToNumber( decimal,int=0 )  單元測試

        #region ToNumber( string, int=0 ) 單元測試
        [Description( "對ToNumber的測試，無小數" )]
        [TestMethod()]
        public void ToNumber_StringTargetTest()
        {
            string target = "255574448.84233";
            int decLength = 0;

            FormatResult r = FormatManager.ToNumber( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,449";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToNumber的測試，小數位數二位" )]
        [TestMethod()]
        public void ToNumber_StringTargetTest2()
        {
            string target = "255574448.84233";
            int decLength = 2;

            FormatResult r = FormatManager.ToNumber( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.84";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToNumber的反向測試，輸入錯誤小數位數長度" )]
        [TestMethod()]
        public void ToNumber_StringTargetOppositeTest()
        {
            string target = "255574448.84233";
            int decLength = -2; // 小數位數資料為負Int值.

            FormatResult r = FormatManager.ToNumber( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.84";
            Assert.AreNotEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToNumber的例外測試，格式化目標為Null" )]
        [TestMethod()]
        public void ToNumber_StringTargetExceptionTest()
        {
            string target = null;   // 格式化目標為Null
            int decLength = 2;

            FormatResult r = FormatManager.ToNumber( target, decLength );
            Assert.IsFalse( r.Success );
        }

        [Description( "對ToNumber的例外測試，格式化目標為空字串" )]
        [TestMethod()]
        public void ToNumber_StringTargetExceptionTest2()
        {
            string target = "";   // 格式化目標為空字串
            int decLength = 2;

            FormatResult r = FormatManager.ToNumber( target, decLength );
            Assert.IsFalse( r.Success );
        }

        #endregion ToNumber( string, int=0 )  單元測試

        #region ToMoney( Decimal, int=0 ) 單元測試

        [Description( "對ToMoney的測試，無小數" )]
        [TestMethod()]
        public void ToMoney_DecimalTargetTest()
        {
            Decimal target = 255574448.84233M;
            int decLength  = 0;

            FormatResult r = FormatManager.ToMoney( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "$255,574,449";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToMoney的測試，小數位數二位" )]
        [TestMethod()]
        public void ToMoney_DecimalTargetTest2()
        {
            Decimal target = 255574448.84233M;
            int decLength  = 2;

            FormatResult r = FormatManager.ToMoney( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "$255,574,448.84";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToMoney的反向測試，輸入錯誤小數位數長度" )]
        [TestMethod()]
        public void ToMoney_DecimalTargetOppositeTest()
        {
            Decimal target = 255574448.84233M;
            int decLength  = -2; // 小數位數資料為負Int值.

            FormatResult r = FormatManager.ToMoney( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "$255,574,448.84";
            Assert.AreNotEqual( expected, r.Result, "格式化結果錯誤" );
        }

        #endregion ToMoney( Decimal, int=0 ) 單元測試

        #region ToMoney( string, int=0 ) 單元測試
        [Description( "對ToMoney的測試，無小數" )]
        [TestMethod()]
        public void ToMoney_StringTargetTest()
        {
            string target = "255574448.84233";
            int decLength = 0;

            FormatResult r = FormatManager.ToMoney( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "$255,574,449";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToMoney的測試，小數位數二位" )]
        [TestMethod()]
        public void ToMoney_StringTargetTest2()
        {
            string target = "255574448.84233";
            int decLength = 2;

            FormatResult r = FormatManager.ToMoney( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "$255,574,448.84";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToMoney的反向測試，輸入錯誤小數位數長度" )]
        [TestMethod()]
        public void ToMoney_StringTargetOppositeTest()
        {
            string target = "255574448.84233";
            int decLength = -2; // 小數位數資料為負Int值.

            FormatResult r = FormatManager.ToMoney( target, decLength );
            Assert.IsTrue( r.Success );

            string expected = "$255,574,448.84";
            Assert.AreNotEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對ToMoney的例外測試，輸入格式化字串為Null" )]
        [TestMethod()]
        public void ToMoney_StringTargetExceptionTest()
        {
            string target = null;   // 格式化目標為Null
            int decLength = 2;

            FormatResult r = FormatManager.ToMoney( target, decLength );
            Assert.IsFalse( r.Success );
        }

        [Description( "對ToMoney的例外測試，輸入格式化字串為空字串" )]
        [TestMethod()]
        public void ToMoney_StringTargetExceptionTest2()
        {
            string target = ""; // 格式化目標為空字串
            int decLength = 2; 

            FormatResult r = FormatManager.ToMoney( target, decLength );
            Assert.IsFalse( r.Success );
        }

        #endregion ToMoney( string, int=0 ) 單元測試

        #region Format 單元測試

        [Description( "對Format的正常測試，格式化目標為DateTime" )]
        [TestMethod()]
        public void FormatTest()
        {
            DateTime target = DateTime.Now;
            string format   = "yyyy-MM-dd HH:mm:ss";
            string type     = "DateTime";

            FormatResult r = FormatManager.Format( target, format, type );
            Assert.IsTrue( r.Success );

            string expected = target.ToString( format );
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對Format的正常測試，格式化目標為Decimal" )]
        [TestMethod()]
        public void FormatTest2()
        {
            Decimal target = 255574448.84233M; ;
            string format  = "{0:N2}";
            string type    = "Decimal";

            FormatResult r = FormatManager.Format( target, format, type );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.84";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對Format的反向測試，輸入錯誤TypeName" )]
        [TestMethod()]
        public void FormatOppositeTest()
        {
            Decimal target = 255574448.84233M; ;
            string format  = "{0:N2}";
            string type    = "fdsfds";   // 輸入錯誤TypeName

            FormatResult r = FormatManager.Format( target, format, type );
            Assert.IsFalse( r.Success );
        }

        [Description( "對Format的反向測試，輸入目標型態為double" )]
        [TestMethod()]
        public void FormatOppositeTest2()
        {
            double target = 255574448.84233; // 輸入錯誤的格式化目標型態
            string format = "{0:N2}";
            string type   = "Decimal";

            FormatResult r = FormatManager.Format( target, format, type );
            Assert.IsTrue( r.Success );

            string expected = "255,574,448.84";
            Assert.AreEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對Format的反向測試，輸入錯誤的格式化字串" )]
        [TestMethod()]
        public void FormatOppositeTest3()
        {
            decimal target = 255574448.84233M;
            string format  = "jjjhhdss"; // 輸入錯誤的格式化字串
            string type    = "Decimal";

            FormatResult r = FormatManager.Format( target, format, type );
            Assert.IsTrue( r.Success );

            string expected = "255574448.84";
            Assert.AreNotEqual( expected, r.Result, "格式化結果錯誤" );
        }

        [Description( "對Format的列外測試，輸入格式化目標為Null" )]
        [TestMethod()]
        public void FormatExceptionTest()
        {
            object target = null; ;
            string format = "{0:N2}";
            string type   = "Decimal";

            FormatResult r = FormatManager.Format( target, format, type );
            Assert.IsFalse( r.Success );
        }

        [Description( "對Format的列外測試，輸入的格式化字串為Null" )]
        [TestMethod()]
        public void FormatExceptionTest2()
        {
            Decimal target = 255574448.84233M;
            string format  = null;
            string type    = "Decimal";

            FormatResult r = FormatManager.Format( target, format, type );
            Assert.IsFalse( r.Success );
        }

        [Description( "對Format的列外測試，輸入的TypeName為Null" )]
        [TestMethod()]
        public void FormatExceptionTest3()
        {
            Decimal target = 255574448.84233M;
            string format  = "{0:N2}";
            string type    = null;

            FormatResult r = FormatManager.Format( target, format, type );
            Assert.IsFalse( r.Success );
        }

        #endregion Format 單元測試
    }
}
