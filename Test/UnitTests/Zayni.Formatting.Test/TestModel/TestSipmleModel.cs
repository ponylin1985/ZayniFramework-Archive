﻿using System;
using System.Collections.Generic;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    // 正常基本型別的Model.
    public class TestModel : BasicModel<TestVModel>
        {
            [NumberFormat( Length = 2 )]
            public decimal Money { get; set; }

            [DateFormat()]
            public DateTime Date { get; set; }
        }

        public class TestVModel : BasicViewModel
        {
            public string Money { get; set; }

            public string Date { get; set; }
        }

        // 正常基本型別的Model.
        public class TestModel2 : BasicModel<TestVModel2>
        {
            [NumberFormat( Length = 2 )]
            public decimal Money { get; set; }

            public DateTime Date { get; set; }
        }

        public class TestVModel2 : BasicViewModel
        {
            public string Money { get; set; }

            public string Date { get; set; }
        }

        // 正常基本型別的Model.
        public class TestModel3 : BasicModel<TestVModel3>
        {
            public decimal Money { get; set; }

            public DateTime Date { get; set; }
        }

        public class TestVModel3 : BasicViewModel
        {
            public string Money { get; set; }

            public string Date { get; set; }
        }

        // 含有List屬性的Model.
        public class TestListModel : BasicModel<TestListVModel>
        {
            [CollectionFormat()]
            public List<decimal> Moneys { get; set; }

            [DateFormat()]
            public DateTime Date { get; set; }
        }

        public class TestListVModel : BasicViewModel
        {
            //public List< string > Moneys { get; set; }    // 20130316 Pony Comment: Stephen還是搞不清楚狀況，ViewModel中不允許有這種型別的屬性

            public string Date { get; set; }
        }

        // attribute 標示錯誤的Model
        public class TestErrorAttriModel : BasicModel<TestErrorAttriVModel>
        {
            [CollectionFormat()]
            public decimal Money { get; set; }

            [DateFormat()]
            public DateTime Date { get; set; }
        }

        public class TestErrorAttriVModel : BasicViewModel
        {
            public string Money { get; set; }

            public string Date { get; set; }
        }

        public class TestMaskFormatAttrModel : BasicModel
        {
            [MaskFormat( StartIndex = 1 )]
            public string Name { get; set; }

            [MaskFormat( StartIndex = 2, Length = 3 )]
            public string Name2 { get; set; }

            [MaskFormat( StartIndex = 2, Length = 3, ReplaceChar = '!' )]
            public string Name3 { get; set; }

            [MaskFormat( StartIndex = 1, Length = 1, ReplaceChar = '!' )]
            public int Age { get; set; }

            [MaskFormat( StartIndex = 2, Length = 3, ReplaceChar = '!' )]
            public DateTime Time { get; set; }
        }

        public class TestMaskModel : BasicModel<TestMaskVModel>
        {
            [MaskFormat( StartIndex = 1 )]
            public string Name { get; set; }

            [MaskFormat( StartIndex = 2, Length = 3 )]
            public string Name2 { get; set; }

            [MaskFormat( StartIndex = 2, Length = 3, ReplaceChar = '!' )]
            public string Name3 { get; set; }

            [MaskFormat( StartIndex = 1, Length = 1, ReplaceChar = '!' )]
            public int Age { get; set; }

            [MaskFormat( StartIndex = 2, Length = 3, ReplaceChar = '!' )]
            public DateTime Time { get; set; }

            [CollectionFormat()]
            public VeryDeepModel DeepData { get; set; }
        }

        public class VeryDeepModel : BasicModel<VeryDeepVModel>
        {
            [MaskFormat( StartIndex = 2, Length = 3 )]
            public string DeepName { get; set; }
        }

        public class TestMaskVModel : BasicViewModel
        {
            public string Name { get; set; }

            public string Name2 { get; set; }

            public string Name3 { get; set; }

            public string Age { get; set; }

            public string Time { get; set; }
        }

        public class VeryDeepVModel : BasicViewModel
        {
            public string DeepName { get; set; }
        }
}
