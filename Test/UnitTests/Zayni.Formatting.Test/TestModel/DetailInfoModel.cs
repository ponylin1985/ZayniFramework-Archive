﻿using System;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    public class DetailInfoModel : BasicModel<DetailInfoVModel>
    {
        public string InfoName
        {
            get;
            set;
        }

        [DateFormat()]
        public DateTime NAVDate
        {
            get;
            set;
        }
    }

    public class DetailInfoModel2 : BasicDynamicModel
    {
        public string InfoName
        {
            get;
            set;
        }

        [DateFormat()]
        public DateTime NAVDate
        {
            get;
            set;
        }
    }

    public class DetailInfoVModel : BasicViewModel
    {
        public string InfoName
        {
            get;
            set;
        }

        public string NAVDate
        {
            get;
            set;
        }
    }
}
