﻿using System;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    public class DetailModel : BasicModel<DetailVModel>
    {
        public string DetailName
        {
            get;
            set;
        }

        public decimal Length
        {
            get;
            set;
        }

        [NumberFormat( LengthPropertyName = "Length" )]
        public decimal Pay
        {
            get;
            set;
        }

        [DateFormat()]
        public DateTime NavDate
        {
            get;
            set;
        }

        [MaskFormatAttribute( StartIndexPropertyName = "MaskStartIndex", LengthPropertyName = "MaskLength" )]
        public string Name
        {
            get;
            set;
        }

        public int MaskStartIndex
        {
            get;
            set;
        }

        public int MaskLength
        {
            get;
            set;
        }        

        [MaskFormatAttribute( Length = 2 )]
        public string Name2
        {
            get;
            set;
        }
    }

    public class DetailModel2 : BasicDynamicModel
    {
        public string DetailName
        {
            get;
            set;
        }

        public decimal Length
        {
            get;
            set;
        }

        [NumberFormat( LengthPropertyName = "Length" )]
        public decimal Pay
        {
            get;
            set;
        }

        [DateFormat()]
        public DateTime NavDate
        {
            get;
            set;
        }
    }   

    public class DetailVModel : BasicViewModel
    {
        public string DetailName
        {
            get;
            set;
        }

        public string Length
        {
            get;
            set;
        }

        public string Pay
        {
            get;
            set;
        }

        public string NavDate
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Name2
        {
            get;
            set;
        }
    }
}
