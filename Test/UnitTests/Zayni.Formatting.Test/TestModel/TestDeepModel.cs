﻿using System;
using System.Collections.Generic;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    // List的資料Model.
    public class TestDataModel : BasicModel
    {
        [NumberFormat( LengthPropertyName = "Length" )]
        public decimal Price { get; set; }
            
        public int Length { get; set; }
    }

    public class TestDeepModel : BasicModel
    {
        [NumberFormat( Length = 2 )]
        public decimal Money { get; set; }

        [DateFormat()]
        public DateTime Date { get; set; }

        [CollectionFormat()]
        public List<TestDataModel> Numbers { get; set; }
    }

    public class UserTestModel : BasicModel<UserTestVModel>
    {
        public string UserName { get; set; }

        [DateTimeFormat()]
        public DateTime UserBDay { get; set; }
        
        [NumberFormat( LengthPropertyName = "SalaryLength" )]
        public decimal UserSalary { get; set; }

        public int SalaryLength { get; set; }

        [CollectionFormat()]
        public List<MemberModel> Members { get; set; }

        [CollectionFormat()]
        public MemberModel Member { get; set; }
    }

    public class MemberModel : BasicModel<MemberVModel>
    {
        [DateTimeFormat()]
        public DateTime MemberDate { get; set; }
    }

    public class MemberVModel : BasicViewModel
    {
        public string MemberDate { get; set; }
    }

    public class UserTestVModel : BasicViewModel
    {
        public string UserName { get; set; }

        public string UserBDay { get; set; }
        
        public string UserSalary { get; set; }

        // 在ViewModel中不會放string以外型別的屬性
        //public List<MemberModel> Members { get; set; }
    }

    public class UserModel : BasicModel<UserVModel>
    {
        // 20130313 Comment by Pony: 故意什麼FormatAttribute都沒標記

        public string UserName { get; set; }

        public DateTime UserBDay { get; set; }
        
        public decimal UserSalary { get; set; }
    }

    public class UserVModel : BasicViewModel
    {
        public string UserName { get; set; }

        public string UserBDay { get; set; }
        
        public string UserSalary { get; set; }
    }
}
