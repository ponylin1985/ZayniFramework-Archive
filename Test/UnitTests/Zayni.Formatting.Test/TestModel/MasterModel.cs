﻿using System;
using System.Collections.Generic;
using ZayniFramework.Formatting;


namespace Formatting.Test
{
    public class MasterModel : BasicModel<MasterVModel>
    {
        [CollectionFormat()]
        public List<DetailModel> Details
        {
            get;
            set;
        }
        
        public string Name
        {
            get;
            set;
        }

        [DateFormat()]
        public DateTime Birthday
        {
            get;
            set;
        }

        [NumberFormat( Length = 2 )]
        public decimal Age
        {
            get;
            set;
        }
        
        [CollectionFormat()]
        public DetailInfoModel DetailInfo
        {
            get;
            set;
        }
    }

    public class MasterModel2 : BasicDynamicModel
    {
        [CollectionFormat()]
        public List<DetailModel2> Details
        {
            get;
            set;
        }
        
        public string Name
        {
            get;
            set;
        }

        [DateFormat()]
        public DateTime Birthday
        {
            get;
            set;
        }

        [NumberFormat( Length = 2 )]
        public decimal Age
        {
            get;
            set;
        }
        
        [CollectionFormat()]
        public DetailInfoModel2 DetailInfo
        {
            get;
            set;
        }
    }

    public class MasterVModel : BasicViewModel
    {
        public string Name
        {
            get;
            set;
        }

        public string Birthday
        {
            get;
            set;
        }

        public string Age
        {
            get;
            set;
        }

        // ViewModel中不應該有String以外型別的屬性
        //public DetailInfoModel DetailInfo
        //{
        //    get;
        //    set;
        //}
    }
}
