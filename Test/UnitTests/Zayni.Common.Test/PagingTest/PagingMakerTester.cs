﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Common.Test.TestModel;


namespace ZayniFramework.Common.Test
{
    /// <summary>分頁 PagingMaker 的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class PagingMakerTester
    {
        #region Private Methods

        /// <summary>準備測試假資料
        /// </summary>
        /// <returns></returns>
        private IEnumerable<object> PrepareData()
        {
            var data = new List<object>();

            for ( int i = 0; i < 500; i++ )
            {
                yield return new UserModel() 
                {
                    Age  = i,
                    Name = "aaa",
                    Sex  = "bbb"
                };
            }
        }

        #endregion Private Methods

        #region Test Methods

        /// <summary>分頁處理測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Common.PagingMaker Test - 分頁處理測試" )]
        public void PagingMaker_Paging_Test()
        {
            List<object> data = PrepareData().ToList();
            List<object> p = PagingMaker.Paging( data, 1, 20 ).ToList();
            Assert.IsTrue( 0 != p.Count );
            Assert.IsTrue( 20 == p.Count );

            p = PagingMaker.Paging( data, 2, 20 ).ToList();
            Assert.IsTrue( 0 != p.Count );
            Assert.IsTrue( 20 == p.Count );
        }

        /// <summary>分頁處理測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Common.PagingMaker Test - 分頁處理測試" )]
        public async Task PagingMaker_PagingAsync_Test()
        {
            List<object> data = PrepareData().ToList();
            IEnumerable<object> p = await PagingMaker.PagingAsync( data, 1, 20 );
            List<object> f = p.ToList();
            Assert.IsTrue( 0 != f.Count );
            Assert.IsTrue( 20 == f.Count );

            p = await PagingMaker.PagingAsync( data, 2, 20 );
            f = p.ToList();
            Assert.IsTrue( 0 != f.Count );
            Assert.IsTrue( 20 == f.Count );
        }

        /// <summary>分頁處理測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Common.PagingMaker Test - 分頁處理測試" )]
        public void PagingMaker_Paging_Test2()
        {
            var data = PrepareData();
            var p = PagingMaker.Paging( data, 1, 5 );
            Assert.IsTrue( 0 != p.Count() );
            Assert.IsTrue( 5 == p.Count() );

            p = PagingMaker.Paging( data, 2, 5 );
            Assert.IsTrue( 0 != p.Count() );
            Assert.IsTrue( 5 == p.Count() );
        }

        #endregion Test Methods
    }
}
