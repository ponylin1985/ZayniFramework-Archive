using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using ZayniFramework.Logging;


namespace ZayniFramework.Common.Test
{
    /// <summary>代理人呼叫測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class ProxyTester
    {
        /// <summary>代理人呼叫測試，沒有參數，沒有回傳值的代理呼叫測試
        /// </summary>
        [TestMethod()]
        [Description( "代理人呼叫測試" )]
        public void ProxyInvoke_NoParameters_NoReturn_Test() 
        {
            var service = new SomeService();

            var proxy = Proxy.Create( 
                (Action)( () => service.DoSomething() ), 
                parameters => 
                {
                    var msg = "Judy loves me.";
                    Debug.Print( msg );
                    ConsoleLogger.WriteLine( msg );
                    return parameters;
                } );

            proxy.Invoke();
        }

        /// <summary>代理人呼叫測試，有參數，沒有回傳值的代理呼叫測試
        /// </summary>
        [TestMethod()]
        [Description( "代理人呼叫測試" )]
        public void ProxyInvoke_Parameters_NoReturn_Test() 
        {
            var service = new SomeService();

            var proxy = Proxy.Create( 
                (Action<UserInfoModel>)( model => service.SetMyFavoriteUser( model ) ), 
                parameters => 
                {
                    var model = (UserInfoModel)parameters[ 0 ];

                    if ( model.UserId == "Judy" )
                    {
                        model.UserId = "Judy Bitch";
                    }

                    return parameters;
                } );

            proxy.Invoke( new UserInfoModel() { UserId = "Judy", Age = 19 } );

            var r = service.GetMyFavoriteUser();
            Assert.IsTrue( r.Success );
            Assert.AreEqual( r.Data.UserId, "Judy Bitch" );
            Assert.AreEqual( r.Data.Age, 19 );
        }

        /// <summary>代理人呼叫測試，沒有參數，有回傳值的代理呼叫測試
        /// </summary>
        [TestMethod()]
        [Description( "代理人呼叫測試" )]
        public void ProxyInvoke_NoParameters_Return_Test() 
        {
            var service = new SomeService();
            service.SetMyFavoriteUser( new UserInfoModel() { UserId = "Kate", Age = 18 } );

            var proxy = Proxy.Create<IResult<UserInfoModel>>( 
                (Func<IResult<UserInfoModel>>)( () => service.GetMyFavoriteUser() ), 
                afterInvoke: ( parameters, result ) => 
                {
                    result.Message = "I still love fuck Judy's ass and throat. Fuck Yeah~~~";
                    return result;
                } );

            var r = proxy.Invoke();
            Assert.IsTrue( r.Success );
            Assert.AreEqual( "Kate", r.Data.UserId );
            Assert.AreEqual( 18, r.Data.Age );
            Assert.AreEqual( "I still love fuck Judy's ass and throat. Fuck Yeah~~~", r.Message );
        }

        /// <summary>代理人呼叫測試，有參數，有回傳值的代理呼叫測試
        /// </summary>
        [TestMethod()]
        [Description( "代理人呼叫測試" )]
        public void ProxyInvoke_Parameters_ReturnValue_Test()
        {
            var service = new SomeService();

            var proxy = Proxy.Create<IResult<UserInfoModel>>( 
                (Func<string, IResult<UserInfoModel>>)( userId => service.GetUser( userId ) ), 
                parameters => 
                {
                    string userId   = parameters[ 0 ] + "";
                    parameters[ 0 ] = userId != "Judy" ? "Sylvia" : userId;
                    return parameters;
                }, 
                ( parameters, result ) => 
                {
                    if ( !result.Success )
                    {
                        result.Message = "I love fuck Sylvia. Oh yeah!!!";
                        return result;
                    }
                    
                    result.Message = "I love fuck Judy. Oh yeah!!!";
                    return result;
                } );

            var r = proxy.Invoke( "Judy" );
            Assert.IsTrue( r.Success );
            Assert.AreEqual( "I love fuck Judy. Oh yeah!!!", r.Message );

            var z = proxy.Invoke( "Kelly" );
            Assert.IsFalse( z.Success );
            Assert.AreEqual( "I love fuck Sylvia. Oh yeah!!!", z.Message );
        }
    }

    /// <summary>某個服務類別，單元測試使用
    /// </summary>
    internal class SomeService 
    {
        /// <summary>測試訊息
        /// </summary>
        /// <value></value>
        public string SomeMessage { get; set; }

        /// <summary>私有測試欄位
        /// </summary>
        private UserInfoModel _myFavoriteUser;

        /// <summary>某個測試方法，沒有參數，沒有回傳值
        /// </summary>
        public void DoSomething() 
        {
            SomeMessage = "I love fuck Jody's ass hole.";
        }

        /// <summary>某個測試方法，沒有參數，但有回傳值
        /// </summary>
        /// <returns></returns>
        public IResult<UserInfoModel> GetMyFavoriteUser() 
        {
            return Result.Create<UserInfoModel>( true, _myFavoriteUser );
        }

        /// <summary>某個測試方法，沒有參數，但有回傳值
        /// </summary>
        /// <returns></returns>
        public void SetMyFavoriteUser( UserInfoModel model ) 
        {
            _myFavoriteUser = model;
        }

        /// <summary>某個查詢之類的測試方法，有需要參數，有回傳值
        /// </summary>
        /// <param name="userId">使用者帳號</param>
        /// <returns>查詢結果</returns>
        public IResult<UserInfoModel> GetUser( string userId ) 
        {
            if ( userId != "Judy" )
            {
                return Result.Create<UserInfoModel>();
            }

            var model = new UserInfoModel() 
            {
                UserId = "Judy",
                Age    = 19
            };

            return Result.Create<UserInfoModel>( true, model );
        }
    }
}
