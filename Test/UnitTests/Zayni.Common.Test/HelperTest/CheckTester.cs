﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>真假值條件測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class CheckTester
    {
        /// <summary>當條件為真的正向測試
        /// </summary>
        [TestMethod()]
        [Description( "當條件為真的正向測試" )]
        public void CheckIsTrue_Test()
        {
            string message = "";
            int j = 9;
            Check.IsTrue( () => 9 == j, () => message = "ok" );
            Assert.AreEqual( "ok", message );

            //===========================================================================

            int k = 9;
            bool check = k == j;
            Check.IsTrue( check, () => message = "nice" );
            Assert.AreEqual( "nice", message );
        }

        /// <summary>當條件為真的反向測試
        /// </summary>
        [TestMethod()]
        [Description( "當條件為真的反向測試" )]
        public void CheckIsTrue_ReverseTest()
        {
            string message = "";
            int j = 9;
            Check.IsTrue( () => 100 == j, () => message = "ok" );
            Assert.AreNotEqual( "ok", message );

            //===========================================================================

            int k = 7788;
            bool check = k == j;
            Check.IsTrue( check, () => message = "nice" );
            Assert.AreNotEqual( "nice", message );
        }

        /// <summary>當條件為假的正向測試
        /// </summary>
        [TestMethod()]
        [Description( "當條件為假的正向測試" )]
        public void CheckIsFalse_Test()
        {
            string message = "";
            int j = 9;
            Check.IsFalse( () => 123 == j, () => message = "ok" );
            Assert.AreEqual( "ok", message );

            //===========================================================================

            bool check = 77 == j;
            Check.IsFalse( check, () => message = "nice" );
            Assert.AreEqual( "nice", message );
        }

        /// <summary>當條件為假的反向測試
        /// </summary>
        [TestMethod()]
        [Description( "當條件為假的反向測試" )]
        public void CheckIsFalse_ReverseTest()
        {
            string message = "";
            int j = 9;
            Check.IsFalse( () => 9 == j, () => message = "ok" );
            Assert.AreNotEqual( "ok", message );

            //===========================================================================

            bool check = 9 == j;
            Check.IsFalse( check, () => message = "nice" );
            Assert.AreNotEqual( "nice", message );
        }
    }
}
