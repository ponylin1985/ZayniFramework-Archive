using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>IP 位址 Helper 的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class IPAddressHelperTester
    {
        /// <summary>取得本地端在區域網路的 Local IP 位址的測試
        /// </summary>
        [TestMethod()]
        [Description( "取得本地端在區域網路的 Local IP 位址的測試" )]
        public void GetLocalIPAddress_Test()
        {
            var ipAddress = IPAddressHelper.GetLocalIPAddress();
            Assert.IsTrue( ipAddress.IsNotNullOrEmpty() );
            System.Diagnostics.Debug.Print( $"Local IP Address: {ipAddress}." );
            System.Console.WriteLine( $"Local IP Address: {ipAddress}." );
        }

        /// <summary>取得本地端在公有網際網路的 IP 位址的測試
        /// </summary>
        [TestMethod()]
        [Description( "取得本地端在公有網際網路的 IP 位址的測試" )]
        public void GetExternalIPAddress_Test()
        {
            var ipAddress = IPAddressHelper.GetExternalIPAddress();
            Assert.IsTrue( ipAddress.IsNotNullOrEmpty() );
            System.Diagnostics.Debug.Print( $"Local Public IP Address: {ipAddress}." );
            System.Console.WriteLine( $"Local Public IP Address: {ipAddress}." );
        }
    }
}
