﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;


namespace ZayniFramework.Common.Test
{
    /// <summary>亂數字串產生器的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class RandomTextHelperTester
    {
        /// <summary>產生亂數字串的測試
        /// </summary>
        [TestMethod()]
        public void RandomTextHelper_Create_Test()
        {
            string result1 = RandomTextHelper.Create( 6 );
            Assert.AreEqual( 6, result1.Length );

            string result2 = RandomTextHelper.Create( 6 );
            Assert.AreEqual( 6, result2.Length );
            Assert.IsTrue( result1 != result2 );

            string result3 = RandomTextHelper.Create( 15 );
            Assert.AreEqual( 15, result3.Length );

            string result4 = RandomTextHelper.Create( 15 );
            Assert.AreEqual( 15, result4.Length );
            Assert.IsTrue( result3 != result4 );

            string result5 = RandomTextHelper.Create( 0 );
            Assert.IsTrue( result5.IsNullOrEmpty() );

            string result6 = RandomTextHelper.Create( -4 );
            Assert.IsTrue( result6.IsNullOrEmpty() );

            string result7 = RandomTextHelper.Create( 15, withSpecialChars: true );
            Assert.IsTrue( result7 != result3 );

            var s1 = RandomTextHelper.Create( 25, withSpecialChars: true );
            var s2 = RandomTextHelper.Create( 25, withSpecialChars: true );
            var s3 = RandomTextHelper.Create( 25, withSpecialChars: true );
            var s4 = RandomTextHelper.Create( 25, withSpecialChars: true );
            var s5 = RandomTextHelper.Create( 25, withSpecialChars: true );
            var s6 = RandomTextHelper.Create( 25, withSpecialChars: true );
            var s7 = RandomTextHelper.Create( 25, withSpecialChars: true );
            var s8 = RandomTextHelper.Create( 25, withSpecialChars: true );
            var s9 = RandomTextHelper.Create( 25, withSpecialChars: true );
            var s0 = RandomTextHelper.Create( 25, withSpecialChars: true );

            var check = new string[] { "!", "@", "#", "$", "%", "^", "&", "*", "`" };
            Assert.IsTrue( new string[] { s1, s2, s3, s4, s5, s6, s7, s8, s9, s0 }.Any( s => check.Any( s.Contains ) ) );
        }

        /// <summary>產生數字亂數的測試
        /// </summary>
        [TestMethod()]
        public void RandomTextHelper_CreateInt64String_Test() 
        {
            var n1 = RandomTextHelper.CreateInt64String( 15 );
            Assert.IsTrue( n1.Length == 15 );

            var n2 = RandomTextHelper.CreateInt64String( 15 );
            Assert.IsTrue( n2.Length == 15 );

            var n3 = RandomTextHelper.CreateInt64String( 15 );
            Assert.IsTrue( n3.Length == 15 );
        }

        /// <summary>產生亂數字串的測試
        /// </summary>
        [TestMethod()]
        public void RandomTextHelper_CreateRandom_Test()
        {
            var r = RandomTextHelper.CreateRandom( 6 );
            Assert.IsTrue( r.Success );
            Assert.AreEqual( 6, r.Data.Length );

            var r2 = RandomTextHelper.CreateRandom( 6 );
            Assert.IsTrue( r2.Success );
            Assert.AreEqual( 6, r2.Data.Length );
            Assert.IsTrue( r.Data != r2.Data );

            var r3 = RandomTextHelper.CreateRandom( 0 );
            Assert.IsFalse( r3.Success );
            Assert.IsTrue( r3.Data.IsNullOrEmpty() );

            var r4 = RandomTextHelper.CreateRandom( -456 );
            Assert.IsFalse( r4.Success );
            Assert.IsTrue( r4.Data.IsNullOrEmpty() );
        }
    }
}
