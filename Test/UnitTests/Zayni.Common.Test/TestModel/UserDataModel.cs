﻿namespace ZayniFramework.Common.Test
{
    /// <summary>測試用資料模型
    /// </summary>
    public class UserDataModel
    {
        /// <summary>名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>性別
        /// </summary>
        public string Sex { get; set; }
    }
}
