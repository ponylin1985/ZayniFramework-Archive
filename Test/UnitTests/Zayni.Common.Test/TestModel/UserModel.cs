﻿using System;
using System.Collections.Generic;


namespace ZayniFramework.Common.Test.TestModel
{
    /// <summary>測試用使用者資料模型
    /// </summary>
    public class UserModel : IUser
    {
        /// <summary>姓名
        /// </summary>
        public string Name { get; set; }
    
        /// <summary>性別
        /// </summary>
        public string Sex { get; set; }
    
        /// <summary>年紀
        /// </summary>
        public int Age { get; set; }

        /// <summary>生日
        /// </summary>
        /// <value></value>
        public DateTime Birthday { get; set; }

        /// <summary>餘額
        /// </summary>
        /// <value></value>
        public string Balance { get; set; }

        /// <summary>玩具數量
        /// </summary>
        /// <value></value>
        public string ToyCount { get; set; }

        /// <summary>到期日
        /// </summary>
        /// <value></value>
        public string ExpireDate { get; set; }
    }

    /// <summary>測試用使用者資料模型
    /// </summary>
    public class UserModel_ConvertTest : IUser
    {
        /// <summary>姓名
        /// </summary>
        public string Name { get; set; }
    
        /// <summary>性別
        /// </summary>
        public string Sex { get; set; }
    
        /// <summary>年紀
        /// </summary>
        public string Age { get; set; }

        /// <summary>星星數
        /// </summary>
        /// <value></value>
        public decimal Starts { get; set; }

        /// <summary>餘額
        /// </summary>
        /// <value></value>
        public decimal Balance { get; set; }

        /// <summary>玩具數量
        /// </summary>
        /// <value></value>
        public long? ToyCount { get; set; }

        /// <summary>到期日
        /// </summary>
        /// <value></value>
        public DateTime? ExpireDate { get; set; }
    }

    /// <summary>
    /// </summary>
    public class UserModelComparer : IEqualityComparer<UserModel>
    {
        /// <summary>
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public bool Equals( UserModel x, UserModel y )
        {
            if ( object.ReferenceEquals( x, y ) )
            {
                return true;
            }

            if ( object.ReferenceEquals( x, null ) || object.ReferenceEquals( null, y ) )
            {
                return false;
            }

            return x.Name == y.Name;
        }

        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int GetHashCode( UserModel obj )
        {
            if ( obj.IsNull() )
            {
                return 0;
            }

            int hashName = obj.Name.IsNull() ? 0 : obj.Name.GetHashCode();
            return hashName;
        }
    }

}
