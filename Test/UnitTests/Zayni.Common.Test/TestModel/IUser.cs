﻿namespace ZayniFramework.Common.Test.TestModel
{
    /// <summary>測試用資料介面
    /// </summary>
    public interface IUser
    {
        /// <summary>姓名
        /// </summary>
        string Name { get; set; }
    }
}
