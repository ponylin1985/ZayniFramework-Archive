﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Data;
using ZayniFramework.Common.Test.TestModel;
using ZayniFramework.Logging;


namespace ZayniFramework.Common.Test
{
    /// <summary>List擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class IListExtensionTester
    {
        /// <summary>對 List 串列進行串接的擴充方法的測試，MergeList 的測試，預設會進行重複過濾!
        /// </summary>
        [TestMethod()]
        [Description( "對 List 串列進行串接的擴充方法的測試，MergeList 的測試" )]
        public void MergeList_Test()
        {
            var aList  = new List<string>() { "A", "B", "C", "D" };
            var bList  = new List<string>() { "A", "B", "E", "F" };

            // =================

            var result = aList.MergeList<string>( bList );
            Assert.AreEqual( 6, result.Count );

            result = aList.MergeList<string>( bList, false );
            Assert.AreEqual( 8, result.Count );

            // =================

            var myList = new List<UserModel>()
            {
                new UserModel()
                {
                    Name = "A",
                    Age  = 22
                },
                new UserModel()
                {
                    Name = "B",
                    Age  = 22
                },
                new UserModel()
                {
                    Name = "C",
                    Age  = 22
                }
            };

            var myList2 = new List<UserModel>()
            {
                new UserModel()
                {
                    Name = "A",
                    Age  = 22
                },
                new UserModel()
                {
                    Name = "B",
                    Age  = 22
                },
                new UserModel()
                {
                    Name = "D",
                    Age  = 22
                }
            };

            var merged = myList.MergeList( myList2, distinct: true, equalityComparer: new UserModelComparer() );
            Assert.AreEqual( 4, merged.Count );

            var merged2 = myList.MergeList( myList2, distinct: false );
            Assert.AreEqual( 6, merged2.Count );
        }

        /// <summary>從來源串列集合中隨便取出一個元素的測試
        /// </summary>
        [TestMethod()]
        [Description( "從來源串列集合中隨便取出一個元素的測試" )]
        public void TakeRandomTest()
        {
            var source = new List<string>() { "A", "B", "C", "D", "E", "F" };
            
            new For().Do( 50, () => 
            {
                string result = source.TakeRandom<string>();
                Assert.IsNotNull( result );
            } );

            source = new List<string>();

            new For().Do( 50, () => 
            {
                string result = source.TakeRandom<string>();
                Assert.IsNull( result );
            } );
        }

        /// <summary>將 IList 轉換成 DataTable 資料結構的測試
        /// </summary>
        [TestMethod()]
        [Description( "將 IList 轉換成 DataTable 資料結構的測試" )]
        public void ConvertToDataTableTest()
        {
            var list = new List<UserModel>() 
            {
                new UserModel() 
                {
                    Name = "Sylvia",
                    Age  = 35,
                    Sex  = "正妹"
                },
                new UserModel() 
                {
                    Name = "Judy",
                    Age  = 20,
                    Sex  = "辣妹"
                },
                new UserModel() 
                {
                    Name = "Cindy",
                    Age  = 20,
                    Sex  = "酒店妹"
                }
            };

            DataTable dt = list.ConvertToDataTable( "酒店妹名單" );
            Assert.IsNotNull( dt );            

            DataTable dt2 = list.ConvertToDataTable();
            Assert.IsNotNull( dt2 );      
        }

        /// <summary>對 IsNullOrEmptyList 擴充方法的正常測試
        /// </summary>
        [TestMethod()]
        [Description( "對 IsNullOrEmptyList 擴充方法的正常測試" )]
        public void IsNullOrEmptyListTest()
        {
            List<string> list1 = new List<string>();
            Assert.IsTrue( list1.IsNullOrEmptyList() );

            List<string> list2 = null;
            Assert.IsTrue( list2.IsNullOrEmptyList() );

            List<string> list3 = new List<string>() { "K", "R" };
            Assert.IsFalse( list3.IsNullOrEmptyList() );
        }

        /// <summary>對 IsNotNullAndEmptyList 擴充方法的正常測試
        /// </summary>
        [TestMethod()]
        [Description( "對 IsNotNullAndEmptyList 擴充方法的正常測試" )]
        public void IsNotNullAndEmptyListTest()
        {
            List<string> list1 = new List<string>();
            Assert.IsFalse( list1.IsNotNullOrEmptyList() );

            List<string> list2 = null;
            Assert.IsFalse( list2.IsNotNullOrEmptyList() );

            List<string> list3 = new List<string>() { "K", "R" };
            Assert.IsTrue( list3.IsNotNullOrEmptyList() );
        }

        /// <summary>IsNullOrEmptyList() 與 IsNullOrEmptyCollection() 擴充方法的效能測試
        /// </summary>
        [TestMethod()]
        public void IsNullOrEmptyList_Performance_Test() 
        {
            var models = new List<string>();

            for ( int i = 0; i < 1000; i++ )
            {
                models.Add( RandomTextHelper.Create( 15 ) );
            }

            var sw = new System.Diagnostics.Stopwatch();
            sw.Reset();
            sw.Start();

            for ( int i = 0; i < 100000; i++ )
            {
                // 10000000 筆測試假資料下，00:00:00.0017914 在已經明確知道為一種 IList 或 ICollection 的情況下，在 IsNullOrEmptyList() 擴充方法中採用 Count 屬性的判斷是最快的!
                Assert.IsFalse( models.IsNullOrEmptyList() );

                // 10000000 筆測試假資料下，00:00:00.0041572 但如果都還以 IEnumerable 介面進行操作下，建議使用 IsNullOrEmptyCollection() 擴充方法，雖然比 IList 擴充方法中的 Count 屬性還慢一點，但是內部使用的 Any() LINQ 方法，所以會比 Count() 還要快!
                // Assert.IsFalse( models.IsNullOrEmptyCollection() );    
            }

            sw.Stop();
            ConsoleLogger.WriteLine( sw.Elapsed.ToString() );
            System.Diagnostics.Debug.Print( sw.Elapsed.ToString() );
        }
    }
}
