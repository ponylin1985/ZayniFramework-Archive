﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;


namespace ZayniFramework.Common.Test.Extension.Test
{
    /// <summary>DateTime日期時間擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class DateTimeExtensionTester
    {
        /// <summary>檢查日期在某個區間的測試
        /// </summary>
        [TestMethod()]
        [Description( "檢查日期在某個區間的測試" )]
        public void IsBetweenTest()
        {
            DateTime test = new DateTime( 2013, 12, 24 );

            #region 基礎測試

            DateTime begin = new DateTime( 2013, 12, 1 );
            DateTime end   = new DateTime( 2013, 12, 31 );

            Assert.IsTrue( test.IsBetween( begin, end ) );
            Assert.IsFalse( test.IsBetween( new DateTime( 2013, 12, 28 ), end ) );

            #endregion 基礎測試

            #region 邊界值測試

            begin = new DateTime( 2013, 12, 24 );

            Assert.IsTrue( test.IsBetween( begin, end, true ) );
            Assert.IsFalse( test.IsBetween( begin, end, false ) );

            begin = new DateTime( 2013, 12, 1 );
            end   = new DateTime( 2013, 12, 24 );

            Assert.IsTrue( test.IsBetween( begin, end, includeEndDate: true ) );
            Assert.IsFalse( test.IsBetween( begin, end, includeEndDate: false ) );

            #endregion 邊界值測試
        }

        /// <summary>比較是否大於比較日期的測試
        /// </summary>
        [TestMethod()]
        [Description( "比較是否大於比較日期的測試" )]
        public void IsGreaterTest()
        {
            DateTime test = new DateTime( 2013, 12, 24 );
            Assert.IsTrue( test.IsGreater( new DateTime( 2013, 11, 30 ) ) );
            Assert.IsFalse( test.IsGreater( new DateTime( 2013, 12, 30 ) ) );
            Assert.IsTrue( test.IsGreater( new DateTime( 2013, 12, 24 ), true ) );
            Assert.IsFalse( test.IsGreater( new DateTime( 2013, 12, 24 ), false ) );
        }

        /// <summary>比較是否小於比較日期的測試
        /// </summary>
        [TestMethod()]
        [Description( "比較是否小於比較日期的測試" )]
        public void IsSmallerTest()
        {
            DateTime test = new DateTime( 2013, 12, 24 );
            Assert.IsTrue( test.IsSmaller( new DateTime( 2013, 12, 30 ) ) );
            Assert.IsFalse( test.IsSmaller( new DateTime( 2013, 11, 30 ) ) );
            Assert.IsTrue( test.IsSmaller( new DateTime( 2013, 12, 24 ), true ) );
            Assert.IsFalse( test.IsSmaller( new DateTime( 2013, 12, 24 ), false ) );
        }

        /// <summary>時區轉換測試
        /// </summary>
        [TestMethod()]
        [Description( "時區轉換測試" )]
        public void TimeConvertTest()
        {
            DateTime localNow  = DateTime.Now;
            DateTime utcNow    = localNow.ToUniversalTime();
            DateTime tokyoTime = utcNow.AddHours( 9D );
            DateTime time      = localNow.ConvertTime( "Tokyo Standard Time" );

            Assert.AreEqual( tokyoTime.Year, time.Year );
            Assert.AreEqual( tokyoTime.Month, time.Month );
            Assert.AreEqual( tokyoTime.Date, time.Date );

            Assert.AreEqual( tokyoTime.Hour, time.Hour );
            Assert.AreEqual( tokyoTime.Minute, time.Minute );
            Assert.AreEqual( tokyoTime.Second, time.Second );

            DateTime time2 = localNow.ConvertTimeFromSourceToTarget( "Taipei Standard Time", "Tokyo Standard Time" );
            Assert.AreEqual( tokyoTime.Year, time2.Year );
            Assert.AreEqual( tokyoTime.Month, time2.Month );
            Assert.AreEqual( tokyoTime.Date, time2.Date );

            Assert.AreEqual( tokyoTime.Hour, time2.Hour );
            Assert.AreEqual( tokyoTime.Minute, time2.Minute );
            Assert.AreEqual( tokyoTime.Second, time2.Second );

            Assert.AreEqual( time.Year, time2.Year );
            Assert.AreEqual( time.Month, time2.Month );
            Assert.AreEqual( time.Date, time2.Date );

            Assert.AreEqual( time.Hour, time2.Hour );
            Assert.AreEqual( time.Minute, time2.Minute );
            Assert.AreEqual( time.Second, time2.Second );

            // 把DateTime當作是Central America Standard Time這個時區，在轉換成Tokyo Standard Time這個時區的時間，需要自行將DateTime.Kind屬性設為Unspecified
            DateTime time3 = new DateTime( localNow.Ticks, DateTimeKind.Unspecified ).ConvertTimeFromSourceToTarget( "Central America Standard Time", "Tokyo Standard Time" );
            Assert.AreNotEqual( time2.Hour, time3.Hour );   // 至少小時一定會不一樣!!!

            DateTime time4 = localNow.ConvertTimeFromSourceToTarget( "Central America Standard Time", "Tokyo Standard Time" );
            Assert.AreNotEqual( time2.Hour, time4.Hour );   // 至少小時一定會不一樣!!!
            
            // 但是經由ConvertTimeFromSourceToTarget和ConvertTimeFromSourceToTarget2方法，轉換出來的日期時間必須是一致的
            Assert.AreEqual( time3.Year, time4.Year );
            Assert.AreEqual( time3.Month, time4.Month );
            Assert.AreEqual( time3.Date, time4.Date );

            Assert.AreEqual( time3.Hour, time4.Hour );
            Assert.AreEqual( time3.Minute, time4.Minute );
            Assert.AreEqual( time3.Second, time4.Second );
        }

        /// <summary>轉換 DateTime 的 Kind 屬性為 DateTimeKind.Utc，但不改變 DateTime 本身的 Value 值的測試
        /// </summary>
        [TestMethod()]
        public void DateTimeExtension_ToUtcKind_Test() 
        {
            var dt     = new DateTime( 2018, 5, 12, 17, 22, 56 ).AddMilliseconds( 657 );
            var result = dt.ToUtcKind();

            Assert.AreEqual( DateTimeKind.Utc, result.Kind );
            Assert.AreEqual( dt.Year, result.Year );
            Assert.AreEqual( dt.Month, result.Month );
            Assert.AreEqual( dt.Day, result.Day );
            Assert.AreEqual( dt.Hour, result.Hour );
            Assert.AreEqual( dt.Minute, result.Minute );
            Assert.AreEqual( dt.Second, result.Second );
            Assert.AreEqual( dt.Millisecond, result.Millisecond );

            // =====================

            DateTime? dt2     = new DateTime( 2018, 5, 12, 17, 22, 56 ).AddMilliseconds( 657 );
            DateTime? result2 = dt2.ToUtcKind();

            DateTime source = (DateTime)dt2;
            DateTime actual = (DateTime)result2;

            Assert.AreEqual( DateTimeKind.Utc, actual.Kind );
            Assert.AreEqual( source.Year, actual.Year );
            Assert.AreEqual( source.Month, actual.Month );
            Assert.AreEqual( source.Day, actual.Day );
            Assert.AreEqual( source.Hour, actual.Hour );
            Assert.AreEqual( source.Minute, actual.Minute );
            Assert.AreEqual( source.Second, actual.Second );
            Assert.AreEqual( source.Millisecond, actual.Millisecond );

            // =====================

            DateTime? dt3     = null;
            DateTime? result3 = dt3.ToUtcKind();
            Assert.IsNull( result3 );
        }
    }
}
