﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Text;
using ZayniFramework.Common.Test.TestModel;


namespace ZayniFramework.Common.Test
{
    /// <summary>單元測試用的 User 使用者類別
    /// </summary>
    public class User
    {
        /// <summary>年齡
        /// </summary>
        /// <value></value>
        public int Age { get; set; }

        /// <summary>運算子 + 方法覆寫
        /// </summary>
        /// <param name="i"></param>
        /// <param name="j"></param>
        /// <returns></returns>
        public static User operator +( User i, User j ) 
        {
            return new User() 
            {
                Age = i.Age + j.Age
            };
        }
    }

    /// <summary>測試用的 Enum 列舉值
    /// </summary>
    public enum State 
    {
        /// <summary>啟用
        /// </summary>
        Enable,

        /// <summary>停用
        /// </summary>
        Disable
    }

    /// <summary>字串型別擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class StringExtensionTester
    {
        /// <summary>資料庫連線字串遮罩的測試
        /// </summary>
        [TestMethod()]
        public void ConnectionStringMask_Test() 
        {
            var mysqlConnString = $"Server=localhost;Database=zayni;Uid=pony;Password=Admin123;Allow User Variables=True;Charset=utf8;Convert Zero Datetime=True;SslMode=none;";
            var mssqlConnString = $"Server=localhost;Database=ZayniFramework;User Id=pony;Password=Admin123;Max Pool Size=20;Min Pool Size=5;Connection Lifetime=10;";
            var pgsqlConnString = $"Server=localhost;Port=5432;Database=zayni;User Id=pony;Password=Admin123;MaxPoolSize=20;MinPoolSize=5;";

            var mysqlExpected   = $"Server=localhost;Database=zayni;Uid=*****;Password=*****;Allow User Variables=True;Charset=utf8;Convert Zero Datetime=True;SslMode=none;";
            var mssqlExpected   = $"Server=localhost;Database=ZayniFramework;User Id=*****;Password=*****;Max Pool Size=20;Min Pool Size=5;Connection Lifetime=10;";
            var pgsqlExpected   = $"Server=localhost;Port=5432;Database=zayni;User Id=*****;Password=*****;MaxPoolSize=20;MinPoolSize=5;";
            
            var result1 = ConnectionStringMask.Mask( mysqlConnString );
            Assert.AreEqual( mysqlExpected, result1 );

            var result2 = ConnectionStringMask.Mask( mssqlConnString );
            Assert.AreEqual( mssqlExpected, result2 );

            var result3 = ConnectionStringMask.Mask( pgsqlConnString );
            Assert.AreEqual( pgsqlExpected, result3 );
        }

        /// <summary>字串開頭檢查的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Common.StringExtension - 字串擴充方法測試" )]
        [Description( "字串開頭檢查的測試" )]
        public void StartsLike_Test() 
        {
            var source = "BBKKIItt_123_rrr";
            var check1 = source.StartsLike( "bbKkiItT" );
            Assert.IsTrue( check1 );

            var check2 = source.StartsLike( "BbkkIItT_" );
            Assert.IsTrue( check2 );

            var check3 = source.StartsLike( "KKKKjdie" );
            Assert.IsFalse( check3 );
        }

        /// <summary>字串結尾檢查的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Common.StringExtension - 字串擴充方法測試" )]
        [Description( "字串結尾檢查的測試" )]
        public void EndsLike_Test() 
        {
            var source = "BBKKIItt_123_HelloHappyBear";
            var check1 = source.EndsLike( "_helloHAPPYbear" );
            Assert.IsTrue( check1 );

            var check2 = source.EndsLike( "hAPPyBeaR" );
            Assert.IsTrue( check2 );

            var check3 = source.EndsLike( "Testing" );
            Assert.IsFalse( check3 );
        }

        /// <summary>字串列舉值轉換的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Common.StringExtension - 字串擴充方法測試" )]
        [Description( "字串列舉值轉換的測試" )]
        public void ParseEnum_Test() 
        {
            string color = "Green";
            ConsoleColor consoleColor = color.ParseEnum<ConsoleColor>();
            Assert.AreEqual( ConsoleColor.Green, consoleColor );

            // ============================

            ConsoleColor? consoleColor2 = color.ParseEnum<ConsoleColor>();
            Assert.AreEqual( ConsoleColor.Green, consoleColor2 );

            // ============================

            string strState = "Disable";
            State state = strState.ParseEnum<State>();
            Assert.AreEqual( State.Disable, state );

            strState = "Enable";
            state    = strState.ParseEnum<State>();
            Assert.AreEqual( State.Enable, state );

            strState = "enable";
            state    = strState.ParseEnum<State>();
            Assert.AreEqual( State.Enable, state );

            strState = "disable";
            state    = strState.ParseEnum<State>();
            Assert.AreEqual( State.Disable, state );

            // ============================

            var strState2 = "Disable";
            State? state2 = strState2.ParseEnum<State>();
            Assert.AreEqual( State.Disable, state2 );

            strState2 = "disable";
            state2    = strState2.ParseEnum<State>();
            Assert.AreEqual( State.Disable, state2 );

            strState2 = "Enable";
            state2    = strState2.ParseEnum<State>();
            Assert.AreEqual( State.Enable, state2 );

            strState2 = "enable";
            state2    = strState2.ParseEnum<State>();
            Assert.AreEqual( State.Enable, state2 );
        }

        /// <summary>日期時間字串轉換測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Common.StringExtension - 字串擴充方法測試" )]
        [Description( "日期時間字串轉換測試" )]
        public void TryParseDateTimeTest()
        {
            string strDateTime = "20160526";
            DateTime dateTime  = new DateTime( 2016, 5, 26 );

            DateTime dt;
            bool isSuccess = strDateTime.TryParseDateTime( "yyyyMMdd", out dt );
            Assert.IsTrue( isSuccess );
            Assert.AreEqual( dateTime, dt );

            strDateTime = "201605";
            isSuccess   = strDateTime.TryParseDateTime( "yyyyMM", out dt );
            Assert.IsTrue( isSuccess );
            Assert.AreEqual( dateTime.Year,  dt.Year );
            Assert.AreEqual( dateTime.Month, dt.Month );

            strDateTime = "2016-05/26   14:23~45";
            dateTime    = new DateTime( 2016, 5, 26, 14, 23, 45 ); 

            isSuccess   = strDateTime.TryParseDateTime( "yyyy-MM/dd   HH:mm~ss", out dt );
            Assert.IsTrue( isSuccess );
            Assert.AreEqual( dateTime, dt );
        }

        /// <summary>具名參數字串格式化測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Common.StringExtension - 字串擴充方法測試" )]
        [Description( "具名參數字串格式化" )]
        public void StringFormatNamedParameterTest()
        {
            string message = "Hi {name}";
            message        = message.Format( name => "Pony" );
            Assert.AreEqual( "Hi Pony", message );

            string msg = "{framework} is very cool".Format( framework => new StringBuilder( "Zayni Framework" ) );
            Assert.AreEqual( "Zayni Framework is very cool", msg );
        }

        /// <summary>擷取子字串的單元測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Common.StringExtension - 字串擴充方法測試" )]
        [Description( "擷取子字串的單元測試" )]
        public void FetchSubstringTest()
        {
            string test = "AAkkkBBAAiiiBB";

            string[] tokens = test.FetchSubstring( "AA", "BB" ).ToArray();
            
            Assert.IsTrue( tokens.IsNotNullOrEmptyArray() );
            Assert.IsFalse( tokens.HasNullOrEmptyElemants() );

            Assert.AreEqual( "kkk", tokens[ 0 ] );
            Assert.AreEqual( "iii", tokens[ 1 ] );
        }

        [TestMethod()]
        [TestCategory( "Common.StringExtension - 字串擴充方法測試" )]
        [Description( "IsNullOrEmpty 回呼處理委派的單元測試" )]
        public void IsNullOrEmptyCallbackTest1()
        {
            var model = new UserModel() 
            {
                Name = "Sylvia"
            };

            string test = "";
            
            bool isNullOrEmpty = test.IsNullOrEmpty( () => {
                test = model.Name;
            } );

            Assert.IsTrue( isNullOrEmpty );
            Assert.AreEqual( "Sylvia", test );

            //=============================================================================

            string test2 = null;

            bool isNullOrEmpty2 = test2.IsNullOrEmpty( () => {
                test2 = model.Name;
            } );

            Assert.IsTrue( isNullOrEmpty2 );
            Assert.AreEqual( "Sylvia", test2 );
        }

        [TestMethod()]
        [Description( "IsNullOrEmpty 回呼處理委派的反向單元測試" )]
        public void IsNullOrEmptyCallbackTest2()
        {
            var model = new UserModel() 
            {
                Name = "Sylvia"
            };

            string test = "hello";
            
            bool isNullOrEmpty = test.IsNullOrEmpty( () => {
                test = model.Name;
            } );

            Assert.IsFalse( isNullOrEmpty );
            Assert.AreEqual( "hello", test );
        }

        [TestMethod()]
        [Description( "IsNotNullOrEmpty 回呼處理委派的單元測試" )]
        public void IsNotNullOrEmptyCallbackTest1()
        {
            string test = "Sylvia";

            bool isNotNullOrEmpty = test.IsNotNullOrEmpty( s => {
                test = s + " loves Pony very much.";
            } );

            Assert.IsTrue( isNotNullOrEmpty );
            Assert.AreEqual( "Sylvia loves Pony very much.", test );

            string test2 = "aaa";

            bool isNotNullOrEmpty2 = test2.IsNotNullOrEmpty( r => {
                r = "bbb";      // Pony Says: 請注意! 這邊對r重新做指派，並沒有辦法改變test指向的物件，因為不是Pass-by-reference
            } );

            Assert.IsTrue( isNotNullOrEmpty2 );
            Assert.AreEqual( "aaa", test2 );
        }

        [TestMethod()]
        [Description( "IsNotNullOrEmpty 回呼處理委派的反向單元測試" )]
        public void IsNotNullOrEmptyCallbackTest2()
        {
            string test = "";   

            bool isNotNullOrEmpty = test.IsNotNullOrEmpty( s => {
                test = s + " loves Pony very much.";
            } );

            Assert.IsFalse( isNotNullOrEmpty );
            Assert.AreNotEqual( "Sylvia loves Pony very much.", test );
            Assert.IsTrue( test.IsNullOrEmpty() );
        }

        [TestMethod()]
        [Description( "IsNullOrEmptyTest擴充方法的單元測試" )]
        public void IsNullOrEmptyTest1() 
        {
            string test = "";
            Assert.IsTrue( test.IsNullOrEmpty() );
        }

        [TestMethod()]
        [Description( "IsNullOrEmptyTest擴充方法的單元測試" )]
        public void IsNullOrEmptyTest2() 
        {
            string test = null;
            Assert.IsTrue( test.IsNullOrEmpty() );
        }

        [TestMethod()]
        [Description( "IsNullOrEmptyTest擴充方法的單元測試" )]
        public void IsNullOrEmptyTest3() 
        {
            string test = "   ";
            Assert.IsTrue( test.IsNullOrEmpty() );      // 請注意，IsNullOrEmpty預設是會Trim的
        }

        [TestMethod()]
        [Description( "IsNullOrEmptyTest擴充方法的單元測試" )]
        public void IsNullOrEmptyTest4() 
        {
            string test = "   ";
            Assert.IsFalse( test.IsNullOrEmpty( false ) );      // 強制IsNullOrEmpty先不要Trim再進行檢查
        }

        [TestMethod()]
        [Description( "IsNullOrEmptyTest擴充方法的單元測試" )]
        public void IsNullOrEmptyTest5() 
        {
            string test = "Sylvia";
            Assert.IsFalse( test.IsNullOrEmpty( false ) );      // 強制IsNullOrEmpty先不要Trim再進行檢查
        }

        [TestMethod()]
        [Description( "IsNullOrEmptyString擴充方法的單元測試" )]
        public void IsNullOrEmptyStringTest1()
        {
            string test   = "";
            string result = test.IsNullOrEmptyString( "Sylvia is my lover." );
            Assert.AreEqual( "Sylvia is my lover.", result );

            string test2   = "   ";
            string result2 = test2.IsNullOrEmptyString( "Sylvia is my lover." );    // 請注意，IsNullOrEmptyString預設是不會Trim的
            Assert.AreNotEqual( "Sylvia is my lover.", result2 );
            Assert.AreEqual( test2, result2 );

            string test3   = null;
            string result3 = test3 ?? "Hello Sylvia";   // 檢查是否為Null的運算子
            Assert.AreEqual( "Hello Sylvia", result3 );

            object test4    = null;
            var    result4  = test4 ?? new User() { Age = 32 };
            Assert.IsTrue( result4 is User );
            Assert.AreEqual( 32, ((User)result4).Age );
        }

        [TestMethod()]
        [Description( "IsNullOrEmptyString擴充方法的單元測試" )]
        public void IsNullOrEmptyStringTest2()
        {
            string test   = "Pony loves Sylvia.";
            string result = test.IsNullOrEmptyString( "Sylvia is my lover." );
            Assert.AreEqual( "Pony loves Sylvia.", result );
        }

        [TestMethod()]
        [Description( "IsNullOrEmptyString擴充方法的單元測試" )]
        public void IsNullOrEmptyStringTest3()
        {
            string test   = "   ";
            string result = test.IsNullOrEmptyString( "Sylvia is my lover.", true );    // 強制IsNullOrEmptyString先Trim之後再進行檢查
            Assert.AreEqual( "Sylvia is my lover.", result );
        }

        [TestMethod()]
        [Description( "IsNullOrEmptyString擴充方法的單元測試" )]
        public void IsNullOrEmptyStringTest4()
        {
            string test   = null;
            string result = test.IsNullOrEmptyString( "Sylvia is my lover.", true );
            Assert.AreEqual( "Sylvia is my lover.", result );
            
            string result2 = test.IsNullOrEmptyString( "Sylvia is my lover.", false );
            Assert.AreEqual( "Sylvia is my lover.", result );
        }
        
        [TestMethod()]
        [Description( "覆寫運算子的單元測試" )]
        public void IsNullOrEmptyTest222()
        {
            User u = new User() 
            {
                Age = 9
            };
            
            User u2 = new User() 
            {
                Age = 9
            };
            
            User result = u + u2;

            Assert.AreEqual( 18, result.Age );
        }

        [TestMethod()]
        [Description( "對StringExtension的ReplaceChar的正向單元測試" )]
        public void ReplaceCharTest()
        {
            string name   = "林品辰";
            string cpName = "博暉科技股份有限公司";

            string result1 = name.ReplaceChar( 'X', 1 );
            Assert.AreEqual( "林XX", result1 );

            string result2 = cpName.ReplaceChar( 'X', 1, 5 );
            Assert.AreEqual( "博XXXXX有限公司", result2 );
        }

        [TestMethod()]
        [Description( "對StringExtension的ReplaceChar的單元測試" )]
        public void ReplaceCharTest2() 
        {
            string name   = "林品辰";
            string cpName = "博暉科技股份有限公司";

            string result1 = name.ReplaceChar( 'X', 1, 65451 );          // 故意給定一個一定會爆掉的長度
            Assert.AreEqual( "林XX", result1 );

            string result2 = cpName.ReplaceChar( 'X', 1, 6321324 );      // 故意給定一個一定會爆掉的長度
            Assert.AreEqual( "博XXXXXXXXX", result2 );                   // 結果應該會得到一個字串，replace到原本字串整個結束掉，算是一種防止爆Exception的機制
        }

        [TestMethod()]
        [Description( "對StringExtension的擴充的單元測試" )]
        public void StringExtensionTest()
        {
            string test1 = "";
            Assert.IsTrue( test1.IsNullOrEmpty() );
            Assert.IsFalse( test1.IsNotNullOrEmpty() );

            string test2 = null;
            Assert.IsTrue( test2.IsNullOrEmpty() );
            Assert.IsFalse( test2.IsNotNullOrEmpty() );

            string test3 = "kkk";
            Assert.IsFalse( test3.IsNullOrEmpty() );
            Assert.IsTrue( test3.IsNotNullOrEmpty() );

            string expected1 = "Hello my name is Sylvia. I love Pony.";
            string expected2 = "I'm Pony. Sylvia is my lover.";

            string test4  = "Hello my name is {0}. I love {1}.";
            string result = test4.FormatTo( "Sylvia", "Pony" );
            Assert.AreEqual( expected1, result );

            string result2 = "I'm {0}. {1} is my lover.".FormatTo( "Pony", "Sylvia" );
            Assert.AreEqual( expected2, result2 );
        }

        [TestMethod()]
        [Description( "對SplitString字串擴充方法的測試" )]
        public void SplitStringExtensionTest()
        {
            string   test   = "a;;b;;c;;d;;e;;f";
            string[] tokens = test.SplitString( ";;" );

            Assert.IsTrue( tokens.IsNotNullOrEmptyArray() );
            Assert.IsTrue( 6 == tokens.Length );
            Assert.AreEqual( "a", tokens[ 0 ] );
            Assert.AreEqual( "b", tokens[ 1 ] );
            Assert.AreEqual( "c", tokens[ 2 ] );
            Assert.AreEqual( "d", tokens[ 3 ] );
            Assert.AreEqual( "e", tokens[ 4 ] );
            Assert.AreEqual( "f", tokens[ 5 ] );
        }

        [TestMethod()]
        [Description( "對SplitString字串擴充方法的測試" )]
        public void StringArrayJoinExtensionTest()
        {
            string separator = ";;";

            string   test   = "a;;b;;c;;d;;e;;f";
            string[] tokens = test.SplitString( separator );

            Assert.IsTrue( tokens.IsNotNullOrEmptyArray() );
            Assert.IsTrue( 6 == tokens.Length );
            Assert.AreEqual( "a", tokens[ 0 ] );
            Assert.AreEqual( "b", tokens[ 1 ] );
            Assert.AreEqual( "c", tokens[ 2 ] );
            Assert.AreEqual( "d", tokens[ 3 ] );
            Assert.AreEqual( "e", tokens[ 4 ] );
            Assert.AreEqual( "f", tokens[ 5 ] );

            string result = tokens.Join( separator );
            Assert.AreEqual( test, result );
        }

        [TestMethod()]
        [Description( "對字串陣列TrimElements擴充方法的測試" )]
        public void StringArrayTrimElementsTest()
        {
            string[] test = { " hh ", "g  ", "Sylvia   " };
            test.TrimElements();

            Assert.AreEqual( "hh", test[ 0 ] );
            Assert.AreEqual( "g", test[ 1 ] );
            Assert.AreEqual( "Sylvia", test[ 2 ] );
        }

        [TestMethod()]
        [Description( "對字串陣列TrimElements擴充方法的測試2" )]
        public void StringArrayTrimElementsTest2()
        {
            string[] test = {  };   // 故意傳一個空陣列
            test.TrimElements();

            Assert.IsTrue( test.IsNullOrEmptyArray() );
        }

        [TestMethod()]
        [ExpectedException( typeof ( NullReferenceException ) )]
        [Description( "對字串陣列TrimElements擴充方法的測試3 - Exception預期測試" )]
        public void StringArrayTrimElementsTest3()
        {
            string[] test = null;   // 故意傳一個Null的值
            test.TrimElements();
        }

        [TestMethod()]
        [Description( "對字串轉換成Binary二進位資料的測試" )]
        public void ToBytesTest()
        {
            string test   = "This is a test string.";
            byte[] binary = test.ToBytes();
            Assert.IsTrue( binary.IsNotNullOrEmptyArray() );

            string result = System.Text.Encoding.UTF8.GetString( binary );
            Assert.AreEqual( test, result );
        }

        [TestMethod()]
        [Description( "對字串進行Base64編碼的測試" )]
        public void Base64EncodeTest()
        {
            string test   = "This is a test string.";
            string result = test.Base64Encode();
            Assert.IsTrue( result.IsNotNullOrEmpty() );
        }

        [TestMethod()]
        [Description( "對字串進行Base64解碼的測試" )]
        public void Base64DecodeTest()
        {
            string test   = "This is a test string.";
            string result = test.Base64Encode();
            Assert.IsTrue( result.IsNotNullOrEmpty() );

            string recover = result.Base64Decode();
            Assert.AreEqual( test, recover );
            
            test    = null;
            result  = null;
            recover = null;

            GC.Collect();
        }

        [TestMethod()]
        [Description( "對字串進行GZip壓縮的測試" )]
        public void StringCompressionTest()
        {
            string test   = "This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string.";
            string result = test.GZipCompress();
            Assert.IsTrue( result.IsNotNullOrEmpty() );
            Assert.IsTrue( result.Length < test.Length );

            string recorver = result.GZipDecompress();
            Assert.IsTrue( recorver.IsNotNullOrEmpty() );
            Assert.AreEqual( test, recorver );
        }

        [TestMethod()]
        [Description( "對字串進行GZip壓縮的測試 - 壓縮成二進位資料的測試" )]
        public void StringCompressionTest2()
        {
            string test   = "This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string.";
            byte[] result = test.GZipCompress2Binary();
            Assert.IsTrue( result.IsNotNullOrEmptyArray() );
            Assert.IsTrue( result.Length < test.Length );
        }

        [TestMethod()]
        [Description( "對字串特定字元Trim的測試1" )]
        public void StringTrimCharsTest()
        {
            string test = "&&Sylvia loves Pony very much.&&&&&";
            string result = test.TrimChars( '&' );
            Assert.AreEqual( "Sylvia loves Pony very much.", result );
        }

        [TestMethod()]
        [Description( "對字串特定字元Trim的測試2" )]
        public void StringTrimCharsTest2()
        {
            string test = "&&Sylvia loves Pony very much.&&&&&###";
            string result = test.TrimChars( '&', '#' );
            Assert.AreEqual( "Sylvia loves Pony very much.", result );
        }

        [TestMethod()]
        [Description( "對字串特定字元Trim的測試3" )]
        public void StringTrimCharsTest3()
        {
            string test = "Sylvia-loves-Pony-very-much.";   // 請注意，TrimChars只會去頭去尾
            string result = test.TrimChars( '-' );
            Assert.AreEqual( "Sylvia-loves-Pony-very-much.", result );
        }

        [TestMethod()]
        [Description( "對字串特定字元Trim的測試4" )]
        public void StringTrimCharsTest4()
        {
            string test = "Sylvia loves Pony very much.";       // 故意傳入一個沒有要目標要trim的字元的字串
            string result = test.TrimChars( '&' );
            Assert.AreEqual( "Sylvia loves Pony very much.", result );
        }

        [TestMethod()]
        [Description( "對字串特定字元Trim的測試5" )]
        public void StringTrimCharsTest5()
        {
            string test = "&&&Sylvia loves Pony very much.&&&&&&&&&&&&&&";       // 故意傳入一個沒有要目標要trim的字元的字串
            string result = test.TrimChars( '&', '*', '#' );                     // 故意讓他trim不到 * 和 #
            Assert.AreEqual( "Sylvia loves Pony very much.", result );
        }

        [TestMethod()]
        [Description( "對字串特定字元Trim的測試6" )]
        public void StringTrimCharsTest6()
        {
            string test = "&&###&&&&Sylvia loves Pony very much.&&&&&&&&&&&&&&";       // 故意傳入一個沒有要目標要trim的字元的字串
            string result = test.TrimChars( '&', '*', '#' );                           // 故意讓他trim不到 *
            Assert.AreEqual( "Sylvia loves Pony very much.", result );
        }

        [TestMethod()]
        [Description( "檢查字串陣列中是否包含任何Null或空字串的元素的測試" )]
        public void HasNullOrEmptyElementsTest() 
        {
            string[] arr = { "", "asdf" };
            Assert.IsTrue( arr.HasNullOrEmptyElemants() );

            string[] arr2 = { null, "ff" };
            Assert.IsTrue( arr2.HasNullOrEmptyElemants() );
        }

        [TestMethod()]
        [Description( "檢查字串陣列中是否包含任何Null或空字串的元素的反向測試" )]
        public void HasNullOrEmptyElementsTest2() 
        {
            string[] arr = { "ddd", "asdf" };
            Assert.IsFalse( arr.HasNullOrEmptyElemants() );
        }

        /// <summary>String RemoveFirstAppeared 擴充方法的測試
        /// </summary>
        [TestMethod()]
        [Description( "String RemoveFirstAppeared 擴充方法的測試" )]
        public void RemoveFirstAppeared_Test()
        {
            // 測試目標字串存在於原始字串中的正常情況
            string source = "AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch...";
            string result = source.RemoveFirstAppeared( "AAANN,KKK-333-" );
            Assert.AreEqual( "888+DFJ{22_SuckMyCock_YouFuckingBitch...", result );

            // ===============================

            // 測試目標字串不存在於原始字串中的例外情況
            result = source.RemoveFirstAppeared( "操妳個騷機八" );
            Assert.AreEqual( "AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch...", result );
        }

        /// <summary>String RemoveLastAppeared 擴充方法的測試
        /// </summary>
        [TestMethod()]
        [Description( "String RemoveLastAppeared 擴充方法的測試" )]
        public void RemoveLastAppeared_Test()
        {
            // 測試目標字串存在於原始字串中的正常情況
            string source = "AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch...";
            string result = source.RemoveLastAppeared( "..." );
            Assert.AreEqual( "AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch", result );

            // ===============================

            // 測試目標字串不存在於原始字串中的例外情況
            result = source.RemoveLastAppeared( "操妳個騷機八" );
            Assert.AreEqual( "AAANN,KKK-333-888+DFJ{22_SuckMyCock_YouFuckingBitch...", result );
        }
    }
}
