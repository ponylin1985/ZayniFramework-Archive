﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;


namespace ZayniFramework.Common.Test.Extension.Test
{
    /// <summary>字典擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class IDictionaryExtensionTester
    {
        /// <summary>擷取出字典物件中的 Key 集合的測試
        /// </summary>
        [TestMethod()]
        [Description( "擷取出字典物件中的 Key 集合的測試" )]
        public void FetchKeysTest()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
	
	        dict.Add( "a", "1" );
	        dict.Add( "b", "2" );
	        dict.Add( "c", "3" );
	        dict.Add( "d", "4" );
	        dict.Add( "e", "5" );
	
	        List<string> list = new List<string>() 
	        {
		        "a", "b", "c"
	        };

            List<string> result = dict.FetchKeys( list ).ToList();
            Assert.IsTrue( 0 != result.Count );
        }

        /// <summary>擷取出字典物件中對應 Key 的 Values 集合的測試
        /// </summary>
        [TestMethod()]
        [Description( "擷取出字典物件中對應 Key 的 Values 集合的測試" )]
        public void FetchValuesTest()
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
	
	        dict.Add( "a", "1" );
	        dict.Add( "b", "2" );
	        dict.Add( "c", "3" );
	        dict.Add( "d", "4" );
	        dict.Add( "e", "5" );
	
	        List<string> list = new List<string>() 
	        {
		        "a", "b", "c"
	        };

            List<string> result = dict.FetchValues<string>( list ).ToList();
            Assert.IsTrue( 0 != result.Count );
        }
    }
}
