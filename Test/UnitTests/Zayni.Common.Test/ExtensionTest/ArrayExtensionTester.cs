﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Common;


namespace ZayniFramework.Common.Test
{
    /// <summary>陣列擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class ArrayExtensionTester
    {
        /// <summary>對字串陣列以指定分格符號串接所有陣列內元素的測試 JoinToken的測試
        /// </summary>
        [TestMethod()]
        [Description( "對字串陣列以指定分格符號串接所有陣列內元素的測試 JoinToken的測試" )]
        public void IEnumerableJoinTokenTest()
        {
            var models    = new string[] { "AA", "BB", "CC", "DD" };
            string result = models.JoinToken( "-" );
            Assert.AreEqual( "AA-BB-CC-DD", result );
        }

        [TestMethod()]
        [Description( "對IsNullOrEmptyArray擴充方法的正常測試" )]
        public void IsNullOrEmptyArrayTest()
        {
            string[] stringArray1 = {};
            Assert.IsTrue( stringArray1.IsNullOrEmptyArray() );

            string[] stringArray2 = null;
            Assert.IsTrue( stringArray2.IsNullOrEmptyArray() );

            string[] stringArray3 = { "k", "u" };
            Assert.IsFalse( stringArray3.IsNullOrEmptyArray() );
        }

        [TestMethod()]
        [Description( "對IsNotNullAndEmptyArray擴充方法的正常測試" )]
        public void IsNotNullAndEmptyArrayTest()
        {
            string[] stringArray1 = {};
            Assert.IsFalse( stringArray1.IsNotNullOrEmptyArray() );

            string[] stringArray2 = null;
            Assert.IsFalse( stringArray2.IsNotNullOrEmptyArray() );

            string[] stringArray3 = { "k", "u" };
            Assert.IsTrue( stringArray3.IsNotNullOrEmptyArray() );
        }

        [TestMethod()]
        [Description( "Binary二進位資料轉換成字串的測試" )]
        public void ByteArrayGetStringTest()
        {
            string test = "This is a test string.";

            byte[] binary = test.ToBytes();
            Assert.IsTrue( binary.IsNotNullOrEmptyArray() );

            string result = binary.GetStringFromUtf8Bytes();
            Assert.AreEqual( test, result );
        }

        [TestMethod()]
        [Description( "Binary二進位資料進行GZip解壓縮成字串的測試" )]
        public void GZipDecompress2StringTest()
        {
            string test   = "This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string. This is a test string.";
            byte[] result = test.GZipCompress2Binary();
            Assert.IsTrue( result.IsNotNullOrEmptyArray() );
            Assert.IsTrue( result.Length < test.Length );

            string recover = result.GZipDecompress2String();
            Assert.AreEqual( test, recover );
        }
    }
}
