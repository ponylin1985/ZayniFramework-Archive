﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test.Extension.Test
{
    /// <summary>Decimal 擴充方法的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class DecimalExtensionTester
    {
        /// <summary>Decimal IsRealDecimal 擴充方法的測試
        /// </summary>
        [TestMethod()]
        public void DecimalExtension_IsRealDecimal_Test()
        {
            decimal d = 123.5m;
            bool r = d.IsRealDecimal();
            Assert.IsTrue( r );

            decimal d2 = 123m;
            bool r2 = d2.IsRealDecimal();
            Assert.IsFalse( r2 );
        }
    }
}
