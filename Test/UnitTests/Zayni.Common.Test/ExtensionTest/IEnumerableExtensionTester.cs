﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using ZayniFramework.Common;
using ZayniFramework.Common.Test.TestModel;


namespace ZayniFramework.Common.Test.Extension.Test
{
    /// <summary>IEnumerable LINQ 擴充的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class IEnumerableExtensionTester
    {
        /// <summary>檢查是否有元素重覆的測試
        /// </summary>
        [TestMethod()]
        [Description( "檢查是否有元素重覆的測試" )]
        public void IEnumerable_HasDuplicates_Test() 
        {
            var source1 = new List<string>() { "A", "B", "C", "C" };
            var result1 = source1.HasDuplicates( q => q );
            Assert.IsTrue( result1 );

            // ===============================

            var source2 = new List<UserModel>() 
            {
                new UserModel() 
                {
                    Name = "A",
                    Age  = 20,
                    Sex  = "female"
                },
                new UserModel() 
                {
                    Name = "A",
                    Age  = 20,
                    Sex  = "female"
                },
                new UserModel() 
                {
                    Name = "B",
                    Age  = 20,
                    Sex  = "female"
                }
            };

            var result2 = source2.HasDuplicates( q => q.Name );
            Assert.IsTrue( result2 );

            // ===============================

            var source3 = new int[] { 1, 1, 3, 3, 4 };
            var result3 = source3.HasDuplicates( j => j );
            Assert.IsTrue( result3 );

            // ===============================

            var source4 = new string[] { "1", "2", "A", "BBB" };
            var result4 = source4.HasDuplicates( s => s );
            Assert.IsFalse( result4 );

            // ===============================

            var source5 = new List<UserModel>() 
            {
                new UserModel() 
                {
                    Name = "Kate",
                    Age  = 20,
                    Sex  = "female"
                },
                new UserModel() 
                {
                    Name = "Amber",
                    Age  = 20,
                    Sex  = "female"
                },
                new UserModel() 
                {
                    Name = "Sylvia",
                    Age  = 20,
                    Sex  = "female"
                }
            };
            
            var result5 = source5.HasDuplicates( m => m.Name );
            Assert.IsFalse( result5 );
        }

        /// <summary>對集合物件以指定分格符號串接所有集合內元素的測試 JoinToken的測試
        /// </summary>
        [TestMethod()]
        [Description( "對集合物件以指定分格符號串接所有集合內元素的測試 JoinToken的測試" )]
        public void IEnumerableJoinTokenTest()
        {
            var models    = new List<string>() { "AA", "BB", "CC", "DD" };
            string result = models.JoinToken( "-" );
            Assert.AreEqual( "AA-BB-CC-DD", result );
        }

        /// <summary>檢查 IEnumerable 物件是否為 Null 或空集合的測試
        /// </summary>
        [TestMethod()]
        [Description( "檢查 IEnumerable 物件是否為 Null 或空集合的測試" )]
        public void IEnumerable_IsNullOrEmptyCollectionTest() 
        {
            IEnumerable<string> source = null;
            Assert.IsTrue( source.IsNullOrEmptyCollection() );
            Assert.IsFalse( source.IsNotNullOrEmptyCollection() );

            IEnumerable<string> models = new List<string>();
            Assert.IsTrue( models.IsNullOrEmptyCollection() );
            Assert.IsFalse( models.IsNotNullOrEmptyCollection() );

            models = new List<string>() { "Test" };
            Assert.IsFalse( models.IsNullOrEmptyCollection() );
            Assert.IsTrue( models.IsNotNullOrEmptyCollection() );
        }

        /// <summary>檢查 IEnumerable 物件是否為 Null 或空集合的測試
        /// </summary>
        [TestMethod()]
        [Description( "檢查 IEnumerable 物件是否為 Null 或空集合的測試" )]
        public void IEnumerable_IsNullOrEmptyTest()
        {
            IEnumerable<string> source = null;
            Assert.IsTrue( source.IsNullOrEmpty() );
            Assert.IsFalse( source.IsNotNullOrEmpty() );

            IEnumerable<string> models = new List<string>();
            Assert.IsTrue( models.IsNullOrEmpty() );
            Assert.IsFalse( models.IsNotNullOrEmpty() );

            models = new List<string>() { "Test" };
            Assert.IsFalse( models.IsNullOrEmpty() );
            Assert.IsTrue( models.IsNotNullOrEmpty() );
        }

        /// <summary>對單一欄位的 Distinct 測試
        /// </summary>
        [TestMethod()]
        [Description( "對單一欄位的 Distinct 測試" )]
        public void IEnumerable_Distinct_SingleFields_Test()
        {
            List<UserModel> models = MakeFakeUsers();
            Assert.AreEqual( 12, models.Count );

            // Distinct單一欄位
            List<UserModel> results = models.Distinct( k => k.Name ).ToList();
            Assert.IsTrue( results.IsNotNullOrEmptyList() );
            Assert.AreEqual( 2, results.Count );
        }

        /// <summary>對多個欄位的 Distinct 測試
        /// </summary>
        [TestMethod()]
        [Description( "對多個欄位的 Distinct 測試" )]
        public void IEnumerable_Distinct_MultiplueFields_Test()
        {
            List<UserModel> models = MakeFakeUsers();
            Assert.AreEqual( 12, models.Count );

            // Distinct多個欄位
            List<UserModel> results = models.Distinct( k => new { Name = k.Name, Age = k.Age } ).ToList();
            Assert.IsTrue( results.IsNotNullOrEmptyList() );
            Assert.AreEqual( 3, results.Count );
        }

        /// <summary>IEnumerable 的 Insert 擴充方法測試
        /// </summary>
        [TestMethod()]
        public void IEnumerable_Insert_Test() 
        {
            IEnumerable<UserModel> models = new List<UserModel>() 
            {
                new UserModel() 
                {
                    Name = "Kate",
                    Sex  = "Female",
                    Age  = 19
                }
            };

            var model = 
                    models
                        .Insert( new UserModel() { Name = "Sylvia" } )
                        .Insert( new UserModel() { Name = "Amber" } )
                        .Insert( new UserModel() { Name = "Nancy" } )
                        .Where( u => u.Name == "Amber" )
                        .FirstOrDefault();

            Assert.AreEqual( "Amber", model.Name );
            Assert.IsTrue( models.Any( q => q.Name == "Amber" ) );
        }

        /// <summary>IEnumerable Foreach 擴充方法的測試
        /// </summary>
        [TestMethod()]
        public void IEnumerable_Foreach_Test() 
        {
            var models  = MakeFakeUsers();
            var results = models.Foreach( m => m.Name = "AngelaBaby" );
            Assert.IsTrue( models.All( m => m.Name == "AngelaBaby" ) );
            Assert.IsTrue( results.All( m => m.Name == "AngelaBaby" ) );

            // ===================

            var models2 = MakeFakeUsers();
            var results2 = models2.ForeachYield( m => m.Name = "Amber" );
            Assert.IsFalse( models2.All( m => m.Name == "Amber" ) );
            Assert.IsTrue( results2.All( m => m.Name == "Amber" ) );
        }

        #region Private Methdos

        /// <summary>建立測試用的假資料
        /// </summary>
        /// <returns></returns>
        private static List<UserModel> MakeFakeUsers() 
        {
            var result = new List<UserModel>();
    
            for ( int i = 0; i < 10; i++ )
            {
                var model = new UserModel() 
                {
                    Name = "Sylvia",
                    Sex  = "female",
                    Age  = 33
                };
        
                result.Add( model );
            }
    
            result.Add( new UserModel() {
                Name = "Sylvia",
                Sex  = "female",
                Age  = 30
            } );
    
            result.Add( new UserModel() 
            {
                Name = "Pony",
                Sex  = "male",
                Age  = 28
            } );
    
            return result;
        }

        #endregion Private Methdos
    }
}
