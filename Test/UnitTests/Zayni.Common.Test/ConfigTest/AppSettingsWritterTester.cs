﻿using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace ZayniFramework.Common.Test
{
    /// <summary>
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class AppSettingsWritterTester
    {
        /// <summary>
        /// </summary>
        [TestMethod()]
        public void AddConfigSettingTest()
        {
            var d = AppSettingsWritter.Instance.AddConfigSetting( "h1", "hello" );
            Assert.IsTrue( d.Success );

            //var r = AppSettingsLoader.Instance.LoadStringTextConfigSetting( "h1" );
            //Assert.IsTrue( r.IsSuccess );
            //Assert.AreEqual( "hello", r.Data );
        }
    }
}
