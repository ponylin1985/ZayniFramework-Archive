﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;


namespace ZayniFramework.Common.Test
{
    /// <summary>
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class AppSettingsLoaderTester
    {
        #region 測試組件初始化

        /// <summary>初始化整個 Test project
        /// </summary>
        /// <param name="testcontext"></param>
        [AssemblyInitialize()]
        public static void Init( TestContext testcontext ) 
        {
            // 在 .NET Core 版本的 MSTest，一定要自行指定 .config 的完整絕對路徑到 ConfigManagement.ConfigFullPath 中，整個 ZayniFramework 舊有在 XML Config 的機制才會正常運作！
            var path = $"{Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location )}/Zayni.Common.Test.dll.config";
            ConfigManagement.ConfigFullPath = path;
        }

        #endregion 測試組件初始化

        /// <summary>
        /// </summary>
        [TestMethod()]
        public void LoadBooleanConfigSettingTest()
        {
            var r = AppSettingsLoader.Instance.LoadBooleanConfigSetting( "test" );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.Data.Value );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public async Task LoadBooleanConfigSettingAsync()
        {
            var r = await AppSettingsLoader.Instance.LoadBooleanConfigSettingAsync( "test" );
            Assert.IsTrue( r.Success );
            Assert.IsTrue( r.Data.Value );
        }
        /// <summary>
        /// </summary>
        /// <returns></returns>

        [TestMethod()]
        public void LoadBooleanConfigSettingTest_NegativeTest_NotExistKey()
        {
            var r = AppSettingsLoader.Instance.LoadBooleanConfigSetting( "testfasdfsadfsad" );  // 測試一個不存在的Key
            Assert.IsFalse( r.Success );
            Assert.IsFalse( r.Data.Value );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public void LoadTimeSpanConfigSettingTest()
        {
            var r = AppSettingsLoader.Instance.LoadTimeSpanConfigSetting( "myTimeSpan" );
            Assert.IsTrue( r.Success );
            Assert.AreEqual( new TimeSpan( 0, 12, 15 ), r.Data.Value );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public async Task LoadTimeSpanConfigSettingAsyncTest()
        {
            var r = await AppSettingsLoader.Instance.LoadTimeSpanConfigSettingAsync( "myTimeSpan" );
            Assert.IsTrue( r.Success );
            Assert.AreEqual( new TimeSpan( 0, 12, 15 ), r.Data.Value );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public void LoadTimeSpanConfigSettingTest_NegativeTest()
        {
            var r = AppSettingsLoader.Instance.LoadTimeSpanConfigSetting( "myTimeSpan555" );
            Assert.IsFalse( r.Success );
            Assert.IsTrue( r.Message.IsNotNullOrEmpty() );

            r = AppSettingsLoader.Instance.LoadTimeSpanConfigSetting( "test" );
            Assert.IsFalse( r.Success );
            Assert.IsTrue( r.Message.IsNotNullOrEmpty() );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public void LoadIntConfigSettingTest()
        {
            var r = AppSettingsLoader.Instance.LoadIntConfigSetting( "myInt" );
            Assert.IsTrue( r.Success );
            Assert.AreEqual( 52, r.Data.Value );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public async Task LoadIntConfigSettingAsyncTest()
        {
            var r = await AppSettingsLoader.Instance.LoadIntConfigSettingAsync( "myInt" );
            Assert.IsTrue( r.Success );
            Assert.AreEqual( 52, r.Data.Value );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public void LoadIntConfigSettingTest_NegativeTest()
        {
            var r = AppSettingsLoader.Instance.LoadIntConfigSetting( "testfasdfsadfsad" );  // 測試一個不存在的Key
            Assert.IsFalse( r.Success );

            r = AppSettingsLoader.Instance.LoadIntConfigSetting( "test" );
            Assert.IsFalse( r.Success );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public void LoadDecimalConfigSettingTest()
        {
            var r = AppSettingsLoader.Instance.LoadDecimalConfigSetting( "myDecimal" );
            Assert.IsTrue( r.Success );
            Assert.AreEqual( 52.55m, r.Data.Value );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public async Task LoadDecimalConfigSettingAsyncTest()
        {
            var r = await AppSettingsLoader.Instance.LoadDecimalConfigSettingAsync( "myDecimal" );
            Assert.IsTrue( r.Success );
            Assert.AreEqual( 52.55m, r.Data.Value );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public void LoadDecimalConfigSettingTest_NegativeTest()
        {
            var r = AppSettingsLoader.Instance.LoadDecimalConfigSetting( "123123sad" );  // 測試一個不存在的Key
            Assert.IsFalse( r.Success );
            Assert.IsTrue( r.Message.IsNotNullOrEmpty() );

            r = AppSettingsLoader.Instance.LoadDecimalConfigSetting( "myTimeSpan" );
            Assert.IsFalse( r.Success );
            Assert.IsTrue( r.Message.IsNotNullOrEmpty() );
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        [TestMethod()]
        public async Task LoadStringTextConfigSettingAsync()
        {
            var r = await AppSettingsLoader.Instance.LoadStringTextConfigSettingAsync( "myInt" );
            Assert.IsTrue( r.Success );
            Assert.AreEqual( "52", r.Data );
        }
    }
}
