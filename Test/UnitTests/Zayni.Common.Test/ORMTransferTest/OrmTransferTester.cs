﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ZayniFramework.DataAccess;


namespace ZayniFramework.Common.Test
{
    /// <summary>ORM 資料轉換的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class OrmTransferTester
    {
        /// <summary>OrmTransfer 的資料轉換測試
        /// </summary>
        [TestMethod()]
        [Description( "ORM轉換資料對應測試" )]
        public void OrmTransfer_TryConvert_Test()
        {
            #region Arrange Fake Data

            DataSet ds = new DataSet();

            DataTable dt1 = new DataTable( "UserInfo" );

            dt1.Columns.Add( "UserId", typeof( String ) );
            dt1.Columns.Add( "Age",    typeof( Int32 ) );

            DataRow r1 = dt1.NewRow();
            
            r1[ "UserId" ] = "A123";
            r1[ "Age" ]    = 23;

            dt1.Rows.Add( r1 );

            DataRow r2 = dt1.NewRow();
            
            r2[ "UserId" ] = "A124";
            r2[ "Age" ]    = 27;

            dt1.Rows.Add( r2 );

            DataTable dt2 = new DataTable( "UserData" );

            dt2.Columns.Add( "Name", typeof( String ) );
            dt2.Columns.Add( "Sex",  typeof( String ) );

            DataRow rr1 = dt2.NewRow();
            
            rr1[ "Name" ] = "Sylvia";
            rr1[ "Sex" ]  = "Female";

            dt2.Rows.Add( rr1 );

            DataRow rr2 = dt2.NewRow();
            
            rr2[ "Name" ] = "Vivian";
            rr2[ "Sex" ]  = "Female";

            dt2.Rows.Add( rr2 );

            ds.Tables.Add( dt1 );
            ds.Tables.Add( dt2 );

            #endregion Arrange Fake Data

            #region Act

            string[] tableNames = { "UserInfo", "UserData" };
            Type[]   types      = { typeof ( UserInfoModel ), typeof ( UserDataModel ) };

            ArrayList datas;
            string    message;

            bool isSuccess = OrmTransfer.TryConvert( ds, "UserInfo", typeof ( UserInfoModel ), out datas, out message );

            #endregion Act

            #region Assert

            Assert.IsTrue( isSuccess );
            Assert.IsNotNull( datas );
            Assert.AreNotEqual( 0, datas.Count );

            var array  = datas.ToArray();
            var models = array.ToList<UserInfoModel>();

            var userId = models.FirstOrDefault().UserId;
            var age    = models.FirstOrDefault().Age;

            Assert.AreEqual( "A123", userId );
            Assert.AreEqual( 23, age );

            #endregion Assert
        }
    }
}
