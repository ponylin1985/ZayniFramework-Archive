﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;


namespace ZayniFramework.Common.Test
{
    /// <summary>迭代器物件的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class IteratorTester
    {
        /// <summary>For 迭代器的測試
        /// </summary>
        [TestMethod()]
        public void ForTest()
        {
            var f = new For();
	        f.Do( 5, () => Console.WriteLine( f.Count ) );
	        f.Reset();
	        Console.WriteLine();
	        f.Do( 10, () => Console.WriteLine( f.Count + 1 ) );
        }

        /// <summary>ForEach 迭代器的測試
        /// </summary>
        [TestMethod()]
        public void ForEachTest() 
        {
            var a = new string[] { "a", "b", "c", "d", "e" };
            var f = new ForEach( a );
            f.Do( () => Console.WriteLine( "AAA" ) );
        }
    }
}
