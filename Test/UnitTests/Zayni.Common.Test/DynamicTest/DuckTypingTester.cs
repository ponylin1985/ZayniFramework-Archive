﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Common.Dynamic;


namespace ZayniFramework.Common.Test
{
    #region 外部現有的API

    /// <summary>
    /// </summary>
    public class ResultModel
    {
        /// <summary>
        /// </summary>
        /// <value></value>
        public string Message { get; set; }

        /// <summary>
        /// </summary>
        /// <value></value>
        public int Code { get; set; }
    }

    /// <summary>
    /// </summary>
    public class QueryModel
    {
        /// <summary>
        /// </summary>
        /// <value></value>
        public string DuckTypeCode { get; set; }

        /// <summary>
        /// </summary>
        /// <value></value>
        public int DuckWight { get; set; }
    }

    /// <summary>紅色小鴨，外部的某個API型別 (我們無法控制的情況)，這裡的情境是，RedDuck類別是無法去繼承自行開發的IDuck介面的情況
    /// </summary>
    public class RedDuck 
    {
        /// <summary>鴨名
        /// </summary>
        public string Name { get; set; }

        /// <summary>鴨重
        /// </summary>
        public double Weight { get; set; }
        
        /// <summary>呱呱叫
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string Quack( string message )
        {
            return "Red Duck Says: {0}".FormatTo( message );
        }
    }

    /// <summary>鴨子查詢器，外部的某個API型別 (我們無法控制的情況)，這裡的情境是，DuckSearcher類別是無法去繼承自行開發的IDuckQueryable介面的情況
    /// </summary>
    public class DuckSearcher
    {
        /// <summary>小鴨搜尋
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        public ResultModel GetDucks( QueryModel queryModel )
        {
            return new ResultModel() 
            {
                Message = "鴨子查詢成功",
                Code    = 1
            };
        }
    }

    #endregion 外部現有的API


    #region 自行開發的系統

    #region 自行開發的interface介面

    /// <summary>自行開發的某個介面，呱呱叫介面
    /// </summary>
    public interface IQuackable
    {
        /// <summary>鴨名
        /// </summary>
        string Name { get; set; }

        /// <summary>呱呱叫
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        string Quack( string message );
    }

    /// <summary>自行開發的某個介面，鴨子查詢介面
    /// </summary>
    public interface IDuckQueryable
    {
        /// <summary>小鴨搜尋
        /// </summary>
        /// <param name="queryModel"></param>
        /// <returns></returns>
        ResultModel GetDucks( QueryModel queryModel );
    }

    #endregion 自行開發的interface介面

    #region 自行開發的轉接器Adapter

    /// <summary>自行開發的一種轉接器Adapter物件，紅色小鴨轉接器
    /// </summary>
    /// <remarks>沒有Duck Typing編程，只能用一般物件導向Design Pattern的解決方案，缺點就是:
    /// 1. 首先，你要會一些基本的物件導向，至少要知道如何定義一個介面interface
    /// 2. 再來你至少需要知道怎麼寫一個Adapter Patter轉接器模式，你必須知道如何實做一個轉接器
    /// 3. 最糟的就是，就是以上的物件導向手法你都會的話，你就得必須在編譯時期寫一個轉接器實作類別，就要多寫程式碼!!!
    /// </remarks>
    public class RedDuckAdapter : IQuackable
    {
        /// <summary>
        /// </summary>
        private RedDuck _redDuck;

        /// <summary>
        /// </summary>
        public RedDuckAdapter()
        {
            _redDuck = new RedDuck();
        }

        /// <summary>
        /// </summary>
        /// <param name="name"></param>
        public RedDuckAdapter( string name )
        {
            _redDuck = new RedDuck() 
            {
                Name = name
            };
        }

        /// <summary>
        /// </summary>
        /// <param name="redDuck"></param>
        public RedDuckAdapter( RedDuck redDuck )
        {
            _redDuck = redDuck;
        }

        /// <summary>紅色小鴨
        /// </summary>
        public RedDuck Duck
        {
            get
            {
                return _redDuck;
            }
            set
            {
                _redDuck = value;
            }
        }

        /// <summary>鴨名
        /// </summary>
        public string Name
        {
            get
            {
                return _redDuck.Name;
            }
            set
            {
                _redDuck.Name = value;
            }
        }

        /// <summary>呱呱叫
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public string Quack( string message )
        {
            return _redDuck.Quack( message );
        }
    }

    #endregion 自行開發的轉接器Adaptera
    
    /// <summary>自行開發的某個型別，目標是不想與外界的API有直接的緊密耦合
    /// </summary>
    public class DuckController
    {
        /// <summary>鴨鴨訊息
        /// </summary>
        public string DuckMessage { get; set; }

        /// <summary>設定鴨鴨訊息
        /// </summary>
        /// <param name="duckName"></param>
        /// <param name="message"></param>
        private void SetDuckMessage( string duckName, string message )
        {
            DuckMessage = string.Format( "{0}呱呱叫說: {1}", duckName, message );
        }

        /// <summary>自行開發的某的Method，參數不想直接與外界的API有直接進密的耦合
        /// </summary>
        /// <param name="duck"></param>
        /// <returns></returns>
        public string DoSomething( dynamic duck, string message )
        {
            SetDuckMessage( duck.Name, message );
            return duck.Quack( message );    // Pony Says: Duck Typing概念，完全不在乎是哪一種真正的物件被傳入，只要傳入的物件身上有Quack方法可以讓我調用就好!!!
        }

        /// <summary>自行開發的某個Method，這邊的參數是吃自行開發的介面，稍微好一點的做法，也許還有機會使用Adapter Pattern進行一些鬆綁的動作
        /// </summary>
        /// <param name="duck"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public string DoSomething( IQuackable duck, string message )
        {
            SetDuckMessage( duck.Name, message );
            return duck.Quack( message );   // Pony Says: 這是一般物件導向多型的應用，一般來說已經遠遠超過博暉科技程式設計師的寫法
        }

        /// <summary>自行開發的某個Method，這邊的參數值皆與外界的API嚴重耦合 (這是最糟糕的寫法!!! 外界的API一旦有變動，或是一旦有更新，直接影響這個Method，可能要調整的可能性極高)
        /// </summary>
        /// <param name="redDuck"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public string DoSomething( RedDuck redDuck, string message )
        {
            SetDuckMessage( redDuck.Name, message );
            return redDuck.Quack( message );    // Pony Says: 博暉科技的程式設計師的寫法，最爛最蠢的寫法! 直接與外界API的ReDuck型別嚴重耦合，這意味著，以後只要外界的API有改版，這邊的程式碼隨時都可能要翻掉重寫!!
        }

        /// <summary>自行開發的某的Method，參數不想直接與外界的API有直接進密的耦合
        /// </summary>
        /// <param name="searcher"></param>
        /// <returns></returns>
        public string DoQuery( dynamic searcher )
        {
            var queryModel = new QueryModel();
            ResultModel model = searcher.GetDucks( queryModel );    // Pony Says: Duck Typing概念，完全不在乎是哪一種真正的物件被傳入，只要傳入的物件身上有GetDucks方法可以讓我調用就好!!!
            return model.Message;
        }

        /// <summary>自行開發的某的Method，這個方法搭配Duck Typing Programming是終極的解決方案!
        /// </summary>
        /// <remarks>以下為情境說明:
        /// 在這個方法得實作內容中，想要對傳入的物件進行GetDucks方法呼叫，然後也想要對傳入的物件進行Quack的方法呼叫，
        /// 但是以外界的API來看，沒有同一種物件，同時實作這兩個方法，RedDuck只有實作Quack方法，而DuckSearcher也只有實作GetDucks方法，
        /// 而以內部自行開發的API來看，目前也只有兩種不同的介面，分別規定必須要實作這兩種方法，但是實際上，內部自行開發的API都還沒有實作類別。
        /// 如果一般的物件導向編程，這時候有幾種解法:
        /// 1. 在內部自行開發的API，自行去新增一個新的類別，此類別要同時實作IQuackable介面和IDuckQueryable介面，但是這會讓DoAnything方法的參數宣告變得困難，或是內容實作時，需要強制轉型... 所以也不是很好的做法
        /// 2. 在內部自行開發的API，先去新增一個新的父介面，譬如叫做IDuck，讓IQuackable和IDuckQueryable都去繼承這個父介面，接著，還是要新增一個新的實作類別，去繼承IDuck介面，這會讓DoAnything方法的參數宣告變簡單，可是這樣就要寫更多程式碼
        /// 3. 在高竿一點，就要用Decorated Pattern、Adapter Pattern這些設計模式來解決問題了... 但是博暉科技的人不可能會這些東西... 博暉科技的Programmer只會寫最蠢的程式碼，這根本無解...
        /// 因此，終極的解決方案是: 應用Dynamic Programming的Duck Typing Programming概念!
        /// 直接利用現有外部的API RedDuck和DuckSearcher這兩個類別，在執行時期動態生成一種匿名但是符合DoAnything方法內容需要呼叫的動態物件，然後，DoAnything方法的實作與宣告也都超級簡單。
        /// 請看CastToTest3單元測試
        /// </remarks>
        /// <param name="duck"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public string DoAnything( dynamic duck, string message )
        {
            QueryModel  queryModel = new QueryModel();
            ResultModel model      = duck.GetDucks( queryModel );   // 要先執行鴨子查詢

            string duckName = duck.Name + "";
            SetDuckMessage( duckName, message );

            string result   = duck.Quack( model.Message );          // 然後又要呱呱叫
            return result;
        }
    }

    #endregion 自行開發的系統

    /// <summary>
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class DuckTypingTester
    {
        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description( "對ZayniFramework 框架的DuckTyping的CastTo轉換的測試" )]
        public void CastToTest()
        {
            var controller = new DuckController();

            #region 物件導向Adapter轉接器模式的解決方案 - 使用紅小鴨轉接器內部預設的紅小鴨

            var redDuckAdapter = new RedDuckAdapter() 
            {
                Name = "紅小鴨"
            };

            string message2 = controller.DoSomething( redDuckAdapter, "Hello Sylvia" );
            Assert.AreEqual( "Red Duck Says: Hello Sylvia", message2 );
            Assert.AreEqual( "紅小鴨呱呱叫說: Hello Sylvia", controller.DuckMessage );

            #endregion 物件導向Adapter轉接器模式的解決方案 - 使用紅小鴨轉接器內部預設的紅小鴨

            #region 物件導向Adapter轉接器模式的解決方案 - 外界指定紅色小鴨給紅小鴨轉接器

            var myRedDuck = new RedDuck() 
            {
                Name = "一隻紅色的小鴨"
            };

            var redDuckAdapter2 = new RedDuckAdapter( myRedDuck );
            string message3 = controller.DoSomething( redDuckAdapter2, "Hello Sylvia" );
            Assert.AreEqual( "Red Duck Says: Hello Sylvia", message3 );
            Assert.AreEqual( "一隻紅色的小鴨呱呱叫說: Hello Sylvia", controller.DuckMessage );

            #endregion 物件導向Adapter轉接器模式的解決方案 - 外界指定紅色小鴨給紅小鴨轉接器

            #region Duck Typing的解決方案

            var redDuck = new RedDuck() 
            {
                Name = "紅小鴨"
            };

            Result<dynamic> castResult = DuckTypingConverter.CastTo<IQuackable>( redDuck );
            Assert.IsNotNull( castResult );

            dynamic duck = castResult.Data;
            Assert.IsNotNull( duck );

            string message = controller.DoSomething( duck, "Hello Sylvia" );
            Assert.AreEqual( "Red Duck Says: Hello Sylvia", message );
            Assert.AreEqual( "紅小鴨呱呱叫說: Hello Sylvia", controller.DuckMessage );

            #endregion Duck Typing的解決方案
        }

        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description( "對ZayniFramework 框架的DuckTyping的CastTo轉換的測試2" )]
        public void CastToTest2()
        {
            Result<dynamic> castResult = DuckTypingConverter.CastTo<IDuckQueryable>( new DuckSearcher() );
            Assert.IsNotNull( castResult );

            dynamic duckSearcher = castResult.Data;
            Assert.IsNotNull( duckSearcher );

            var controller = new DuckController();
            string message = controller.DoQuery( duckSearcher );
            Assert.AreEqual( "鴨子查詢成功", message );
        }

        /// <summary>
        /// </summary>
        [TestMethod()]
        [Description( "對ZayniFramework 框架的DuckTyping的CastTo轉換的測試3 - DuckTypingConverter的終極解決方案" )]
        public void CastToTest3()
        {
            #region 先轉接IQuackable介面上的方法

            Result<dynamic> castResult = DuckTypingConverter.CastTo<IQuackable>( new RedDuck() { Name = "偽黃小鴨" } );
            dynamic duck = castResult.Data;
            Assert.IsNotNull( duck );

            #endregion 先轉接IQuackable介面上的方法

            #region 再拿同一個轉接成功的動態物件去轉接IDuckQueryable介面上的方法

            castResult = DuckTypingConverter.CastTo<IDuckQueryable>( new DuckSearcher(), duck );    // 注意，這裡還是拿duck去進行Cast轉換!!! 這就是DuckTypingConverter強大的地方!!!
            duck = castResult.Data;
            Assert.IsNotNull( duck );

            #endregion 再拿同一個轉接成功的動態物件去轉接IDuckQueryable介面上的方法

            #region 最後使用有轉接到IQuackable和IDuckQueryable介面的動態物件傳入到API中呼叫方法

            var controller = new DuckController();
            string message = controller.DoAnything( duck, "Hello Sylvia" );
            Assert.AreEqual( "Red Duck Says: 鴨子查詢成功", message );
            Assert.AreEqual( "偽黃小鴨呱呱叫說: Hello Sylvia", controller.DuckMessage );

            #endregion 最後使用有轉接到IQuackable和IDuckQueryable介面的動態物件傳入到API中呼叫方法
        }
    }
}
