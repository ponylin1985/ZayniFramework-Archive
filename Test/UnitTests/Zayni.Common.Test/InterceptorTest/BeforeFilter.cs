﻿using System.Linq;


namespace ZayniFramework.Common.Test
{
    /// <summary>前置攔截器
    /// </summary>
    public class BeforeFilter : BeforeMethodInterceptor
    {
        /// <summary>執行前置攔截的動作
        /// </summary>
        /// <param name="aspectParameters"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public override object[] AspectExecute( dynamic aspectParameters = null, params object[] parameters )
        {
            string target = parameters.FirstOrDefault() + "";
            target += " B_Filter ";
            return new object[] { target };
        }
    }
}
