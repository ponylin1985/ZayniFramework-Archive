﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using ZayniFramework.Common.Test.TestModel;


namespace ZayniFramework.Common.Test
{
    /// <summary>反射器的測試類別
    /// </summary>
    [TestClass()]
    [TestCategory( "Common.Test" )]
    public class ReflectorTester
    {
        /// <summary>取得動態物件的屬性值的正向測試
        /// </summary>
        [TestMethod()]
        [Description( "取得動態物件的屬性值的正向測試" )]
        public void Reflector_GetPropertyValue_DynamicObject_Test()
        {
            dynamic model = new MemberModel()
            {
                Name = "Kate"
            };

            object obj = Reflector.GetPropertyValue( model, "Name" );
            Assert.IsNotNull( obj );

            string val = Reflector.GetPropertyValue<string>( model, "Name" );
            Assert.AreEqual( model.Name, val );

            string name = obj + "";
            Assert.AreEqual( model.Name, name );

            model = new
            {
                UserName = "Sylvia",
                Age      = 24
            };

            obj = Reflector.GetPropertyValue( model, "UserName" );
            Assert.IsNotNull( obj );

            name = obj + "";
            Assert.AreEqual( model.UserName, name );

            obj = Reflector.GetPropertyValue( model, "Age" );
            int age = int.Parse( obj + "" );
            Assert.AreEqual( model.Age, age );
        }

        /// <summary>測試委派
        /// </summary>
        public delegate void TestDelegate();

        /// <summary>檢查目標物件是否為一種委派的測試
        /// </summary>
        [TestMethod()]
        [Description( "檢查目標物件是否為一種委派的測試" )]
        public void Reflector_IsItDelegate_Test()
        {
            TestDelegate aDelegate = () => {};
            bool isDelegate = Reflector.IsItDelegate( aDelegate );
            Assert.IsTrue( isDelegate );

            Action action = () => { int i = 3; i++; };
            bool isDelegate2 = Reflector.IsItDelegate( action );
            Assert.IsTrue( isDelegate2 );
        }

        /// <summary>檢查目標動態物件是否為一種委派的測試
        /// </summary>
        [TestMethod]
        [Description( "檢查目標動態物件是否為一種委派的測試" )]
        public void Reflector_IsThatDelegate_Test()
        {
            dynamic aDelegate = (TestDelegate)( () => {} );
            bool isDelegate = Reflector.IsThatDelegate( aDelegate );
            Assert.IsTrue( isDelegate );

            Action action = () => { int i = 3; i++; };
            bool isDelegate2 = Reflector.IsThatDelegate( action );
            Assert.IsTrue( isDelegate2 );
        }   

        /// <summary>檢查目標物件是否為指定父型別的子型態物件的測試
        /// </summary>
        [TestMethod]
        [Description( "檢查目標物件是否為指定父型別的子型態物件的測試" )]
        public void Reflector_IsTypeof_Test()
        {
            Type type = Type.GetType( "System.Exception" );
            var  ex   = new DataAccessException();
            Assert.IsTrue( Reflector.IsTypeof( ex, type ) );

            Assert.IsTrue( Reflector.IsTypeof( ex, typeof ( DataAccessException ) ) );

            var str = "test";
            Assert.IsTrue( Reflector.IsTypeof( str, typeof ( string ) ) );
        }

        /// <summary>檢查目標物件是否為指定父型別的子型態物件的測試
        /// </summary>
        [TestMethod]
        [Description( "檢查目標物件是否為指定父型別的子型態物件的測試" )]
        public void Reflector_IsTypeOf_Test2()
        {
            Type type = Type.GetType( "System.Exception" );
            var  str  = "test";
            Assert.IsFalse( Reflector.IsTypeof( str, type ) );
        }

        /// <summary>檢查目標物件是否為指定父型別的子型態物件的測試
        /// </summary>
        [TestMethod]
        [Description( "檢查目標物件是否為指定父型別的子型態物件的測試" )]
        public void Reflector_IsTypeOf_Test3()
        {
            var model = new UserModel();
            bool isOk = Reflector.IsTypeof( model, Path.Combine( AppDomain.CurrentDomain.BaseDirectory, "Zayni.Common.Test.dll" ), "ZayniFramework.Common.Test.TestModel.UserModel" );
            Assert.IsTrue( isOk );
        }

        /// <summary>用反射取得目標型別第一個泛型的測試
        /// </summary>
        [TestMethod()]
        public void GetListFirstGenericTypeTest()
        {
            Type target = typeof( List<UserModel> );

            Type t = ListReflector.GetListFirstGenericType( target );
            Assert.IsTrue( t.FullName == typeof ( UserModel ).FullName );

            Type target2 = typeof( List<string> );

            Type t2 = ListReflector.GetListFirstGenericType( target2 );
            Assert.IsTrue( t2.FullName != typeof ( UserModel ).FullName );
        }
    }
}
