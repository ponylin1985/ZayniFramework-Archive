﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Common;
using ZayniFramework.Cryptography;


namespace Cryptography.Test
{
    /// <summary>DES對稱式加解密元件的測試類別
    /// </summary>
    [TestClass]
    public class DesEncryptorTester
    {
        /// <summary>對 DES 加解密元件的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Cryptography DES Encryption Test" )]
        [Description( "對 DES 加解密元件的測試" )]
        public void DesEncryptionTest()
        {
            string test = "12345";
            
            var    encryptor     = new DesEncryptor();
            string encryptString = encryptor.Encrypt( test );
            Assert.IsTrue( encryptString.IsNotNullOrEmpty() );

            string result = encryptor.Decrypt( encryptString );
            Assert.AreEqual( test, result );
        }

        /// <summary>對 DES 加解密元件的測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Cryptography DES Encryption Test" )]
        [Description( "對 DES 加解密元件的測試" )]
        public void DesEncryptionTest_ChineseTest()
        {
            string test = "你好嗎? 有吃飯了嗎++";
            
            var    encryptor     = new DesEncryptor();
            string encryptString = encryptor.Encrypt( test );
            Assert.IsTrue( encryptString.IsNotNullOrEmpty() );

            string result = encryptor.Decrypt( encryptString );
            Assert.AreEqual( test, result );
        }

        /// <summary>對 DES 加解密元件的測試 2
        /// </summary>
        [TestMethod()]
        [TestCategory( "Cryptography DES Encryption Test" )]
        [Description( "對 DES 加解密元件的測試 2" )]
        public void DesEncryptionTest2()
        {
            string test = "12345";
            
            var    encryptor     = new DesEncryptor();
            string encryptString = encryptor.Encrypt( test );
            Assert.IsTrue( encryptString.IsNotNullOrEmpty() );

            byte[] binary = encryptString.ToBytes();
            string str    = binary.GetStringFromUtf8Bytes();

            string result = encryptor.Decrypt( str );
            Assert.AreEqual( test, result );
        }
    }
}
