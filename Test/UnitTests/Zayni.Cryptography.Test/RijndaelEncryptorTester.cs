﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Common;
using ZayniFramework.Cryptography;


namespace Cryptography.Test
{
    /// <summary>Rijndael對稱式加解密元件的測試類別
    /// </summary>
    [TestClass]
    public class RijndaelEncryptorTester
    {
        /// <summary>對 Rijndael 加解密元件的測試
        /// </summary>
        [TestMethod()]
        [Description( "對 Rijndael 加解密元件的測試" )]
        public void RijndaelEncryptionTest()
        {
            string test = "12345";
            
            var    encryptor     = new RijndaelEncryptor();
            string encryptString = encryptor.Encrypt( test );
            Assert.IsTrue( encryptString.IsNotNullOrEmpty() );

            string result = encryptor.Decrypt( encryptString );
            Assert.AreEqual( test, result );
        }

        /// <summary>對 Rijndael 加解密元件的測試
        /// </summary>
        [TestMethod()]
        [Description( "對 Rijndael 加解密元件的測試" )]
        public void RijndaelEncryptionTest_ChineseTest()
        {
            string test = "你好嗎? 有吃飯了嗎++";
            
            var    encryptor     = new RijndaelEncryptor();
            string encryptString = encryptor.Encrypt( test );
            Assert.IsTrue( encryptString.IsNotNullOrEmpty() );

            string result = encryptor.Decrypt( encryptString );
            Assert.AreEqual( test, result );
        }

        /// <summary>對 Rijndael 加解密元件的測試 2
        /// </summary>
        [TestMethod()]
        [Description( "對 Rijndael 加解密元件的測試 2" )]
        public void RijndaelEncryptionTest2()
        {
            string test = "12345";
            
            var    encryptor     = new RijndaelEncryptor();
            string encryptString = encryptor.Encrypt( test );
            Assert.IsTrue( encryptString.IsNotNullOrEmpty() );

            byte[] binary = encryptString.ToBytes();
            string str    = binary.GetStringFromUtf8Bytes();

            string result = encryptor.Decrypt( str );
            Assert.AreEqual( test, result );
        }
    }
}
