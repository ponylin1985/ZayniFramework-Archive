﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZayniFramework.Common;
using ZayniFramework.Cryptography;


namespace Cryptography.Test
{
    /// <summary>雜湊式加密的測試類別
    /// </summary>
    [TestClass()]
    public class HashEncryptorTester
    {
        /// <summary>SHA256 雜湊加密 有 Salt 測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Cryptography Hash Encryption Test" )]
        [Description( "SHA256 雜湊加密 有 Salt 測試" )]
        public void SHA256HashTest()
        {
            CryptographySettings.HashNeedSalt = true;

            string password = "123456";
            string userId   = "Pony";

            IHashEncryptor encryptor = HashEncryptorFactory.Create( "sha256" );

            string result = encryptor.HashEncrypt( password, userId );
            Assert.IsTrue( result.IsNotNullOrEmpty() );

            string result2 = encryptor.HashEncrypt( password, userId );
            Assert.IsTrue( result2.IsNotNullOrEmpty() );
            Assert.IsTrue( result == result2 );

            string result3 = encryptor.HashEncrypt( password, "ddd" );     // 測試用另一個id產生salt
            Assert.IsTrue( result2.IsNotNullOrEmpty() );
            Assert.IsFalse( result == result3 );

            string result4 = encryptor.HashEncrypt( "222222", userId );     // 測試用不同的password進行hash加密
            Assert.IsTrue( result4.IsNotNullOrEmpty() );
            Assert.IsFalse( result == result4 );
        }

        /// <summary>SHA256 雜湊加密 沒有 Salt 測試 (執行此測試之前，需要將 Config 中的 needSalt 設定為 false)
        /// </summary>
        [TestMethod()]
        [TestCategory( "Cryptography Hash Encryption Test" )]
        [Description( "SHA256 雜湊加密 沒有Salt測試" )]
        public void SHA256HashTest2()
        {
            //CryptographySettings.HashNeedSalt = false;

            //string password = "123456";
            //string userId   = null;

            //IHashEncryptor encryptor = HashEncryptorFactory.Create( "sha256" );

            //string result = encryptor.HashEncrypt( password, userId );
            //Assert.IsTrue( result.IsNotNullOrEmpty() );

            //string result2 = encryptor.HashEncrypt( password, userId );
            //Assert.IsTrue( result2.IsNotNullOrEmpty() );
            //Assert.IsTrue( result == result2 );

            //string result3 = encryptor.HashEncrypt( password, "pony" );     // 沒有啟用Salt，就算傳如id，雜湊過的密文也都會是同一組
            //Assert.IsTrue( result2.IsNotNullOrEmpty() );
            //Assert.IsTrue( result == result3 );
        }

        /// <summary>SHA512 雜湊加密 有 Salt 測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Cryptography Hash Encryption Test" )]
        [Description( "SHA512 雜湊加密 有 Salt 測試" )]
        public void SHA512HashTest()
        {
            CryptographySettings.HashNeedSalt = true;

            string password = "123456";
            string userId   = "PonyE";

            IHashEncryptor encryptor = HashEncryptorFactory.Create( "sha512" );

            string result = encryptor.HashEncrypt( password, userId );
            Assert.IsTrue( result.IsNotNullOrEmpty() );

            string result2 = encryptor.HashEncrypt( password, userId );
            Assert.IsTrue( result2.IsNotNullOrEmpty() );
            Assert.IsTrue( result == result2 );

            string result3 = encryptor.HashEncrypt( password, "sylvia" );       // 測試用另一個id產生salt
            Assert.IsTrue( result2.IsNotNullOrEmpty() );
            Assert.IsFalse( result == result3 );

            string result4 = encryptor.HashEncrypt( "222222", userId );         // 測試用不同的password進行hash加密
            Assert.IsTrue( result4.IsNotNullOrEmpty() );
            Assert.IsFalse( result == result4 );
        }

        /// <summary>SHA512 雜湊加密 沒有 Salt 測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Cryptography Hash Encryption Test" )]
        [Description( "SHA512 雜湊加密 沒有 Salt 測試" )]
        public void SHA512HashTest2()
        {
            // CryptographySettings.HashNeedSalt = false;

            // string password = "123456";
            // string userId   = "Belle Claire & 三上悠亞 & 明日花蘿琪";

            // IHashEncryptor encryptor = HashEncryptorFactory.Create( "sha512" );

            // string result = encryptor.HashEncrypt( password, userId );
            // Assert.IsTrue( result.IsNotNullOrEmpty() );

            // string result2 = encryptor.HashEncrypt( password, userId );
            // Assert.IsTrue( result2.IsNotNullOrEmpty() );
            // Assert.IsTrue( result == result2 );

            // string result3 = encryptor.HashEncrypt( password, "pony" );     // 沒有啟用 Salt，就算傳如id，雜湊過的密文也都會是同一組
            // Assert.IsTrue( result2.IsNotNullOrEmpty() );
            // Assert.AreEqual( result, result3 );
            // Assert.IsTrue( result == result3 );
        }

        /// <summary>SHA384 雜湊加密 有Salt測試
        /// </summary>
        [TestMethod()]
        [TestCategory( "Cryptography Hash Encryption Test" )]
        [Description( "SHA384 雜湊加密 有 Salt 測試" )]
        public void SHA384HashTest()
        {
            CryptographySettings.HashNeedSalt = true;

            string password = "123456";
            string userId   = "Pony";

            IHashEncryptor encryptor = HashEncryptorFactory.Create( "sha384" );

            string result = encryptor.HashEncrypt( password, userId );
            Assert.IsTrue( result.IsNotNullOrEmpty() );

            string result2 = encryptor.HashEncrypt( password, userId );
            Assert.IsTrue( result2.IsNotNullOrEmpty() );
            Assert.IsTrue( result == result2 );

            string result3 = encryptor.HashEncrypt( password, "pony" );     // 測試用另一個id產生salt
            Assert.IsTrue( result2.IsNotNullOrEmpty() );
            Assert.IsFalse( result == result3 );

            string result4 = encryptor.HashEncrypt( "222222", userId );     // 測試用不同的password進行hash加密
            Assert.IsTrue( result4.IsNotNullOrEmpty() );
            Assert.IsFalse( result == result4 );
        }

        /// <summary>SHA384 雜湊加密 沒有 Salt 測試 (執行此測試之前，需要將 Config 中的 needSalt 設定為 false)
        /// </summary>
        [TestMethod()]
        [TestCategory( "Cryptography Hash Encryption Test" )]
        [Description( "SHA384 雜湊加密 沒有 Salt 測試" )]
        public void SHA384HashTest2()
        {
            // CryptographySettings.HashNeedSalt = false;

            // string password = "123456";
            // string userId   = null;

            // IHashEncryptor encryptor = HashEncryptorFactory.Create( "sha384" );

            // string result = encryptor.HashEncrypt( password, userId );
            // Assert.IsTrue( result.IsNotNullOrEmpty() );

            // string result2 = encryptor.HashEncrypt( password, userId );
            // Assert.IsTrue( result2.IsNotNullOrEmpty() );
            // Assert.IsTrue( result == result2 );

            // string result3 = encryptor.HashEncrypt( password, "pony" );     // 沒有啟用 Salt，就算傳如id，雜湊過的密文也都會是同一組
            // Assert.IsTrue( result2.IsNotNullOrEmpty() );
            // Assert.IsTrue( result == result3 );
        }
    }
}
