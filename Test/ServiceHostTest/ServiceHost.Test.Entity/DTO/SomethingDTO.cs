﻿using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;
using ZayniFramework.Validation;


namespace ServiceHost.Test.Entity
{
    /// <summary>測試用的傳輸資料載體
    /// </summary>
    [Serializable()]
    public class SomethingDTO
    {
        /// <summary>測試訊息
        /// </summary>
        [NotNullOrEmpty( Message = "'someMsg' is invalid." )]
        [JsonProperty( PropertyName = "someMsg" )]
        [JsonPropertyName( "someMsg" )]
        public string SomeMessage { get; set; }

        /// <summary>測試日期
        /// </summary>
        [JsonProperty( PropertyName = "someDate" )]
        [JsonPropertyName( "someDate" )]
        public DateTime SomeDate { get; set; }

        /// <summary>測試浮點數
        /// </summary>
        [JsonProperty( PropertyName = "magicNumber" )]
        [JsonPropertyName( "magicNumber" )]
        public double SomeMagicNumber { get; set; }

        /// <summary>測試整數
        /// </summary>
        [JsonProperty( PropertyName = "luckyNumber" )]
        [JsonPropertyName( "luckyNumber" )]
        [Int64Range( Min = "1", IncludeMin = true, Message = "The 'luckyNumber' must be a positive Int64 integer number." )]
        public long LuckyNumber { get; set; }

        /// <summary>測試整布林值
        /// </summary>
        [JsonProperty( PropertyName = "isSuperXMan" )]
        [JsonPropertyName( "isSuperXMan" )]
        public bool IsSuperMan { get; set; }
    }
}
