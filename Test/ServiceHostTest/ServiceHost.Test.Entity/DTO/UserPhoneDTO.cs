using System.Text.Json.Serialization;
using Newtonsoft.Json;


namespace ServiceHost.Test.Entity
{
    /// <summary>使用者帳號的聯絡電話資料載體 (測試使用)
    /// </summary>
    public class UserPhoneDTO
    {
        /// <summary>帳號
        /// </summary>
        [JsonProperty( PropertyName = "account_id" )]
        [JsonPropertyName( "account_id" )]
        public string Account { get; set; }

        /// <summary>連絡電話類型
        /// </summary>
        [JsonProperty( PropertyName = "phone_type" )]
        [JsonPropertyName( "phone_type" )]
        public string PhoneType { get; set; }

        /// <summary>電話號碼
        /// </summary>
        [JsonProperty( PropertyName = "phone_number" )]
        [JsonPropertyName( "phone_number" )]
        public string PhoneNumber { get; set; }
    }
}