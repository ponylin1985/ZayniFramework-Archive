using Newtonsoft.Json;
using ZayniFramework.Common.ORM;


namespace ServiceHost.Test.Entity
{
    /// <summary>單元測試用的使用者連絡電話明細資料模型
    /// </summary>
    public class UserPhoneDetailModel
    {
        /// <summary>帳號
        /// </summary>
        [JsonProperty( PropertyName = "ACCOUNT_ID" )]
        [TableColumn( ColumnName = "ACCOUNT_ID" )]
        public string Account { get; set; }

        /// <summary>連絡電話類型
        /// </summary>
        [JsonProperty( PropertyName = "PHONE_TYPE" )]
        [TableColumn( ColumnName = "PHONE_TYPE" )]
        public string PhoneType { get; set; }

        /// <summary>電話號碼
        /// </summary>
        [JsonProperty( PropertyName = "PHONE_NUMBER" )]
        [TableColumn( ColumnName = "PHONE_NUMBER" )]
        public string PhoneNumber { get; set; }
    }
}
