﻿using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service.Client;
using ZayniFramework.Middle.Service.Grpc.Client;
using ZayniFramework.Middle.Service.HttpCore.Client;
using ZayniFramework.Middle.TelnetService;


namespace ServiceHost.Client.App
{
    /// <summary>主程式
    /// </summary>
    sealed class Program
    {
        #region 宣告進入點方法

        /// <summary>主程式進入點 (Non-async Main method)
        /// </summary>
        /// <param name="args"></param>
        static void Main_Sync( string[] args )
        {
            ConsoleLogger.WriteLine( "Start ServiceHost.Client.App console application now..." );
            RemoteClientFactory.RegisterRemoteClientType<gRPCRemoteClient>( "gRPC" ); 
            RemoteClientFactory.RegisterRemoteClientType<HttpCoreRemoteClient>( "HttpCore" );

            #region Start non-async CommandService

            var commandContainer = RegisterCommands();
            Check.IsTrue( bool.TryParse( ConfigManagement.AppSettings[ "EnableTelnetCommandService" ], out bool enableTelnetCmdService ) ? enableTelnetCmdService : true, () => TelnetCommandService.StartDefaultTelnetService( commandContainer ) );
            Check.IsTrue( bool.TryParse( ConfigManagement.AppSettings[ "EnableConsoleCommandService" ], out bool enableConsoleCmdService ) ? enableConsoleCmdService : false, () => ConsoleCommandService.StartDefaultConsoleService( commandContainer ) );

            #endregion Start non-async CommandService

            #region Start async CommandService

            // 如果要在 Non-Async Main 方法中，啟動 async 非同步的 Command Service，則使用以下的寫法。
            // Task.Run( async () => 
            // {
            //     var asyncCommandContainer = await RegisterCommandsAsync();

            //     if ( bool.TryParse( ConfigManagement.AppSettings[ "EnableTelnetCommandService" ], out bool enableTelnetCmdService ) )
            //     {
            //         TelnetCommandService.StartDefaultAsyncTelnetService( asyncCommandContainer );
            //     }

            //     if ( bool.TryParse( ConfigManagement.AppSettings[ "EnableConsoleCommandService" ], out bool enableConsoleCmdService ) )
            //     {
            //         await ConsoleCommandService.StartDefaultConsoleServiceAsync( asyncCommandContainer );
            //     }
            // } ).Wait();

            #endregion Start async CommandService
        }

        /// <summary>主程式進入點 (Async Main method)
        /// </summary>
        /// <param name="args"></param>
        static async Task Main( string[] args )
        {
            ConsoleLogger.WriteLine( "Start ServiceHost.Client.App console application now..." );
            RemoteClientFactory.RegisterRemoteClientType<gRPCRemoteClient>( "gRPC" ); 
            RemoteClientFactory.RegisterRemoteClientType<HttpCoreRemoteClient>( "HttpCore" );

            #region Start async CommandService

            var asyncCommandContainer = await RegisterCommandsAsync();

            if ( bool.TryParse( ConfigManagement.AppSettings[ "EnableTelnetCommandService" ], out bool enableTelnetCmdService ) )
            {
                TelnetCommandService.StartDefaultAsyncTelnetService( asyncCommandContainer );
            }

            if ( bool.TryParse( ConfigManagement.AppSettings[ "EnableConsoleCommandService" ], out bool enableConsoleCmdService ) )
            {
                await ConsoleCommandService.StartDefaultConsoleServiceAsync( asyncCommandContainer );
            }

            #endregion Start async CommandService
        }

        #endregion 宣告進入點方法


        #region 宣告私有的方法

        // 別以為這種老派的寫法很遜... Sometimes, the old way is the best way...
        /// <summary>註冊命令處理物件
        /// </summary>
        /// <returns>命令容器池</returns>
        private static CommandContainer RegisterCommands()
        {
            var commandContainer = new CommandContainer();
            commandContainer.RegisterCommand<InitCommand>( "init", commandText => 0 == string.Compare( commandText.Trim(), "init", true ) );
            commandContainer.RegisterCommand<SendMagicActionCommand>( "send magic", commandText => 0 == string.Compare( commandText.Trim(), "send magic action", true ) );
            commandContainer.RegisterCommand<GetUsersCommand>( "get users", commandText => 0 == string.Compare( commandText.Trim(), "get users", true ) );
            commandContainer.RegisterCommand<PerformanceTestCommand>( "performance", commandText => commandText.Trim().StartsWith( "performance test", true, CultureInfo.InvariantCulture ) );
            commandContainer.RegisterCommand<AspNetCorePerformanceTestCommand>( "aspnet core performance", commandText => commandText.Trim().StartsWith( "aspnet performance test", true, CultureInfo.InvariantCulture ) );
            commandContainer.RegisterCommand<FireCommand>( "fire", commandText => commandText.Trim().StartsWith( "fire", true, CultureInfo.InvariantCulture ) );
            commandContainer.RegisterCommand<ExitConsoleCommand>( "exit", commandText => 0 == string.Compare( commandText.Trim(), "exit", true ) );
            commandContainer.RegisterCommand<ClearConsoleCommand>( "cls", commandText => 0 == string.Compare( commandText.Trim(), "cls", true ) );
            commandContainer.RegisterUnknowCommand<UnknowRemoteCommand>();
            return commandContainer;
        }

        /// <summary>註冊命令處理物件
        /// </summary>
        /// <returns>命令容器池</returns>
        private static async Task<CommandContainer> RegisterCommandsAsync()
        {
            var commandContainer = new CommandContainer();
            await commandContainer.RegisterCommandAsync<InitCommandAsync>( "init", commandText => 0 == string.Compare( commandText.Trim(), "init async", true ) );
            await commandContainer.RegisterCommandAsync<SendMagicActionCommandAsync>( "send magic async", commandText => 0 == string.Compare( commandText.Trim(), "send magic action async", true ) );
            await commandContainer.RegisterCommandAsync<GetUsersCommandAsync>( "get users async", commandText => 0 == string.Compare( commandText.Trim(), "get users async", true ) );
            await commandContainer.RegisterCommandAsync<PerformanceTestCommandAsync>( "performance", commandText => commandText.Trim().StartsWith( "performance test async", true, CultureInfo.InvariantCulture ) );
            await commandContainer.RegisterCommandAsync<AspNetCorePerformanceTestCommandAsync>( "aspnet core performance", commandText => commandText.Trim().StartsWith( "aspnet performance test", true, CultureInfo.InvariantCulture ) );
            await commandContainer.RegisterCommandAsync<FireCommandAsync>( "fire", commandText => commandText.Trim().StartsWith( "fire", true, CultureInfo.InvariantCulture ) );
            await commandContainer.RegisterCommandAsync<ExitConsoleCommandAsync>( "exit", commandText => 0 == string.Compare( commandText.Trim(), "exit", true ) );
            await commandContainer.RegisterCommandAsync<ClearConsoleCommandAsync>( "cls", commandText => 0 == string.Compare( commandText.Trim(), "cls", true ) );
            await commandContainer.RegisterUnknowCommandAsync<UnknowRemoteCommandAsync>();
            return commandContainer;
        }

        #endregion 宣告私有的方法
    }
}
