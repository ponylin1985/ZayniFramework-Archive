using ServiceHost.Test.Entity;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;


namespace ServiceHost.Client.App
{
    /// <summary>執行 ASP.NET Core Web API 效能負載測試的命令 (原始命令: aspnet performance test {count})
    /// </summary>
    public class AspNetCorePerformanceTestCommand : RemoteCommand
    {
        /// <summary>ASP.NET Core Web API 的 URL 位址
        /// </summary>
        private string _apiUrl = ConfigManagement.AppSettings[ "WebAPIUrl" ];

        /// <summary>HttpClient 客戶端獨體物件
        /// </summary>
        /// <returns></returns>
        private static HttpClient _httpClient;

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            if ( _apiUrl.IsNullOrEmpty() )
            {
                StdoutErr( $"Please set the 'WebAPIUrl' in the app.config file first." );
                return Result;
            }

            if ( _httpClient.IsNull() )
            {
                var baseUrl = new Uri( _apiUrl );

                _httpClient = new HttpClient() 
                {
                    BaseAddress = baseUrl,
                    Timeout     = TimeSpan.FromSeconds( 5 )
                };

                var spm = ServicePointManager.FindServicePoint( baseUrl );
                spm.ConnectionLeaseTimeout = (int)TimeSpan.FromMinutes( 30 ).TotalMilliseconds;
                ServicePointManager.DnsRefreshTimeout = (int)TimeSpan.FromMinutes( 30 ).TotalMilliseconds;
            }

            var t = CommandText.Trim()?.Split( ' ' );
            int count = 4 == t.Length && int.TryParse( t.LastOrDefault(), out int cmdCount ) ? cmdCount : 1000;

            var reqDTO = new SomethingDTO() 
            {
                SomeMessage     = "ASP.NET Core Performance Test!",
                SomeDate        = DateTime.UtcNow,
                SomeMagicNumber = 44.33D,
                LuckyNumber     = 88,
                IsSuperMan      = true
            };

            string reqJson = JsonSerializer.Serialize( reqDTO );
            DateTime begin = DateTime.UtcNow;

            int i;

            for ( i = 0; i < count; i++ )
            {
                _httpClient.DefaultRequestHeaders.Clear();

                var reqContent   = new StringContent( reqJson, Encoding.UTF8, "application/json" );
                var httpResponse = _httpClient.PostAsync( "test", reqContent ).ConfigureAwait( false ).GetAwaiter().GetResult();

                if ( httpResponse.IsNull() || httpResponse.StatusCode != HttpStatusCode.OK )
                {
                    StdoutErr( $"Performance fail at {i + 1} due to http response is not OK." );
                    return Result;
                }

                var resContent = httpResponse.Content.ReadAsStringAsync().ConfigureAwait( false ).GetAwaiter().GetResult();

                var r = JsonSerializer.Deserialize<Result<MagicDTO>>( resContent );

                if ( !r.Success )
                {
                    StdoutErr( $"Performance fail at {i + 1}." );
                    return Result;
                }
            }

            DateTime end = DateTime.UtcNow;
            var sp = ( end - begin ).TotalSeconds;
            Stdout( $"All success. ExecuteCount: {count}. Total Seconds: {sp}" );

            Result.Success = true;
            return Result;
        }
    }

    /// <summary>執行 ASP.NET Core Web API 效能負載測試的命令 (原始命令: aspnet performance test {count})
    /// </summary>
    public class AspNetCorePerformanceTestCommandAsync : RemoteCommandAsync
    {
        /// <summary>ASP.NET Core Web API 的 URL 位址
        /// </summary>
        private string _apiUrl = ConfigManagement.AppSettings[ "WebAPIUrl" ];

        /// <summary>HttpClient 客戶端獨體物件
        /// </summary>
        /// <returns></returns>
        private static HttpClient _httpClient;

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            if ( _apiUrl.IsNullOrEmpty() )
            {
                await StdoutErrAsync( $"Please set the 'WebAPIUrl' in the app.config file first." );
                return Result;
            }

            if ( _httpClient.IsNull() )
            {
                var baseUrl = new Uri( _apiUrl );

                _httpClient = new HttpClient() 
                {
                    BaseAddress = baseUrl,
                    Timeout     = TimeSpan.FromSeconds( 5 )
                };

                var spm = ServicePointManager.FindServicePoint( baseUrl );
                spm.ConnectionLeaseTimeout = (int)TimeSpan.FromMinutes( 30 ).TotalMilliseconds;
                ServicePointManager.DnsRefreshTimeout = (int)TimeSpan.FromMinutes( 30 ).TotalMilliseconds;
            }

            var t = CommandText.Trim()?.Split( ' ' );
            int count = 4 == t.Length && int.TryParse( t.LastOrDefault(), out int cmdCount ) ? cmdCount : 1000;

            var reqDTO = new SomethingDTO() 
            {
                SomeMessage     = "ASP.NET Core Performance Test!",
                SomeDate        = DateTime.UtcNow,
                SomeMagicNumber = 44.33D,
                LuckyNumber     = 88,
                IsSuperMan      = true
            };

            string reqJson = JsonSerializer.Serialize( reqDTO );
            DateTime begin = DateTime.UtcNow;

            int i;

            for ( i = 0; i < count; i++ )
            {
                _httpClient.DefaultRequestHeaders.Clear();

                var reqContent   = new StringContent( reqJson, Encoding.UTF8, "application/json" );
                var httpResponse = await _httpClient.PostAsync( "test", reqContent ).ConfigureAwait( false );

                if ( httpResponse.IsNull() || httpResponse.StatusCode != HttpStatusCode.OK )
                {
                    await StdoutErrAsync( $"Performance fail at {i + 1} due to http response is not OK." );
                    return Result;
                }

                var resContent = await httpResponse.Content.ReadAsStringAsync().ConfigureAwait( false );

                var r = JsonSerializer.Deserialize<Result<MagicDTO>>( resContent );

                if ( !r.Success )
                {
                    await StdoutErrAsync( $"Performance fail at {i + 1}." );
                    return Result;
                }
            }

            DateTime end = DateTime.UtcNow;
            var sp = ( end - begin ).TotalSeconds;
            await StdoutAsync( $"All success. ExecuteCount: {count}. Total Seconds: {sp}" );

            Result.Success = true;
            return Result;
        }
    }
}