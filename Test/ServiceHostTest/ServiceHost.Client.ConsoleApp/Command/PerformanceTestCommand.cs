using ServiceHost.Client.Proxy;
using ServiceHost.Test.Entity;
using System;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;


namespace ServiceHost.Client.App
{
    /// <summary>執行 RPC 效能負載測試的令命 (原始命令: performance test {count})
    /// </summary>
    public class PerformanceTestCommand : RemoteCommand
    {
        /// <summary>測試用的服務代理人 Proxy 物件
        /// </summary>
        private MyProxy _proxy = AppContext.MyProxy;

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            if ( _proxy.IsNull() )
            {
                StdoutErr( $"Please execute the 'init' command first." );
                return Result;
            }

            // 直接使用 Command 屬性取得原始 Console 或 Telent 的命令列。
            // var t = Command.Trim()?.Split( ' ' );

            // 使用 Command 基底抽象的 ParameterCollection 取得 Console 或 Telnet 的遠端命令。
            var t = Parameters[ "CommandText" ]?.ToString().Trim()?.Split( ' ' );

            int count = 3 == t.Length && int.TryParse( t.LastOrDefault(), out int cmdCount ) ? cmdCount : 1000;

            var reqDTO = new SomethingDTO() 
            {
                SomeMessage     = "Performance Test!",
                SomeDate        = DateTime.UtcNow,
                SomeMagicNumber = 44.33D,
                LuckyNumber     = 88,
                IsSuperMan      = true
            };

            DateTime begin = DateTime.UtcNow;

            int i;

            for ( i = 0; i < count; i++ )
            {
                var r = _proxy.GetMagicDTO( reqDTO );

                if ( !r.Success )
                {
                    StdoutErr( $"Performance fail at {i + 1}." );
                    return Result;
                }
            }
            
            DateTime end = DateTime.UtcNow;
            var sp = ( end - begin ).TotalSeconds;

            Stdout( $"All success. ExecuteCount: {count}. Total Seconds: {sp}" );
            Result.Success = true;
            return Result;
        }
    }

    /// <summary>執行 RPC 效能負載測試的令命 (原始命令: performance test async {count})
    /// </summary>
    public class PerformanceTestCommandAsync : RemoteCommandAsync
    {
        /// <summary>測試用的服務代理人 Proxy 物件
        /// </summary>
        private MyProxy _proxy = AppContext.MyProxy;

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            if ( _proxy.IsNull() )
            {
                await StdoutErrAsync( $"Please execute the 'init' command first." );
                return Result;
            }

            // 直接使用 Command 屬性取得原始 Console 或 Telent 的命令列。
            // var t = Command.Trim()?.Split( ' ' );

            // 使用 Command 基底抽象的 ParameterCollection 取得 Console 或 Telnet 的遠端命令。
            var t = Parameters[ "CommandText" ]?.ToString().Trim()?.Split( ' ' );

            int count = 4 == t.Length && int.TryParse( t.LastOrDefault(), out int cmdCount ) ? cmdCount : 1000;

            var reqDTO = new SomethingDTO() 
            {
                SomeMessage     = "Async Performance Test!",
                SomeDate        = DateTime.UtcNow,
                SomeMagicNumber = 44.33D,
                LuckyNumber     = 88,
                IsSuperMan      = true
            };

            DateTime begin = DateTime.UtcNow;

            int i;

            for ( i = 0; i < count; i++ )
            {
                var r = await _proxy.GetMagicDTOAsync( reqDTO );

                if ( !r.Success )
                {
                    await StdoutErrAsync( $"Performance fail at {i + 1}." );
                    return Result;
                }
            }
            
            DateTime end = DateTime.UtcNow;
            var sp = ( end - begin ).TotalSeconds;

            await StdoutAsync( $"All success. ExecuteCount: {count}. Total Seconds: {sp}" );
            Result.Success = true;
            return Result;
        }
    }
}