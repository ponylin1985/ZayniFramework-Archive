using ServiceHost.Client.Proxy;
using ServiceHost.Test.Entity;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace ServiceHost.Client.App
{
    /// <summary>發送 RPC 請求的測試命令 (原始命令: send magic action)
    /// </summary>
    public class SendMagicActionCommand : RemoteCommand
    {
        /// <summary>測試用的服務代理人 Proxy 物件
        /// </summary>
        private MyProxy _proxy = AppContext.MyProxy;

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            if ( _proxy.IsNull() )
            {
                StdoutErr( $"Please execute the 'init' command first." );
                return Result;
            }

            var reqDTO = new SomethingDTO() 
            {
                SomeMessage     = "This is a message from masOS or linux.",
                SomeDate        = DateTime.UtcNow,
                SomeMagicNumber = 24.52D,
                LuckyNumber     = 333,
                IsSuperMan      = true
            };

            var r = _proxy.GetMagicDTO( reqDTO );
            Stdout( JsonConvertUtil.SerializeInCamelCase( r ) );

            Result.Success = r.Success;
            return Result;
        }
    }

    /// <summary>發送 RPC 請求的測試命令 (原始命令: send magic action async)
    /// </summary>
    public class SendMagicActionCommandAsync : RemoteCommandAsync, ICommandAsync
    {
        /// <summary>測試用的服務代理人 Proxy 物件
        /// </summary>
        private MyProxy _proxy = AppContext.MyProxy;

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            if ( _proxy.IsNull() )
            {
                await StdoutErrAsync( $"Please execute the 'init' command first." );
                return Result;
            }

            var reqDTO = new SomethingDTO() 
            {
                SomeMessage     = "This is a message from masOS or linux.",
                SomeDate        = DateTime.UtcNow,
                SomeMagicNumber = 24.52D,
                LuckyNumber     = 333,
                IsSuperMan      = true
            };

            var r = await _proxy.GetMagicDTOAsync( reqDTO );
            await StdoutAsync( JsonConvertUtil.SerializeInCamelCase( r ) );

            Result.Success = r.Success;
            return Result;
        }
    }
}