using System.Threading.Tasks;
using ServiceHost.Client.Proxy;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;


namespace ServiceHost.Client.App
{
    /// <summary>Init 初始化命令 (原始命令: init)
    /// </summary>
    public class InitCommand : RemoteCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            var eventProcess = new HappyEventHandler();
            AppContext.MyProxy.IsNull( () => AppContext.MyProxy = MyProxy.GetInstance( AppContext.ServiceClientConfigPath ) );
            AppContext.MyProxy.Add( eventProcess.Name, eventProcess.GetType() );
            Stdout( $"Initialize successfully!" );
            Result.Success = true;
            return Result;
        }
    }

    /// <summary>Init 初始化命令 (原始命令: init async)
    /// </summary>
    public class InitCommandAsync : RemoteCommandAsync
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            var eventProcess = new HappyEventHandler();
            AppContext.MyProxy.IsNull( () => AppContext.MyProxy = MyProxy.GetInstance( AppContext.ServiceClientConfigPath ) );
            AppContext.MyProxy.Add( eventProcess.Name, eventProcess.GetType() );
            await StdoutAsync( $"Initialize successfully!" );
            Result.Success = true;
            return Result;
        }
    }
}