using ServiceHost.Client.Proxy;
using ServiceHost.Test.Entity;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace ServiceHost.Client.App
{
    /// <summary>取得使用者測試資料的命令 (原始命令: get users)
    /// </summary>
    public class GetUsersCommand : RemoteCommand
    {
        /// <summary>測試用的服務代理人 Proxy 物件
        /// </summary>
        private MyProxy _proxy = AppContext.MyProxy;

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            if ( _proxy.IsNull() )
            {
                StdoutErr( $"Please execute the 'init' command first." );
                return Result;
            }

            var reqDTO = new UserDTO() { Sex = 0 };
            var r = _proxy.GetUserModels( reqDTO );
            var j = JsonConvertUtil.SerializeInCamelCase( r );

            Action action = r.Success ? 
                (Action)( () => Stdout( j ) ) :
                (Action)( () => StdoutErr( j ) );

            action();

            Result.Success = r.Success;
            return Result;
        }
    }

    /// <summary>取得使用者測試資料的命令 (原始命令: get users async)
    /// </summary>
    public class GetUsersCommandAsync : RemoteCommandAsync
    {
        /// <summary>測試用的服務代理人 Proxy 物件
        /// </summary>
        private MyProxy _proxy = AppContext.MyProxy;

        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override async Task<IResult> ExecuteAsync( ParameterCollection parameters )
        {
            if ( _proxy.IsNull() )
            {
                await StdoutErrAsync( $"Please execute the 'init' command first." );
                return Result;
            }

            var reqDTO = new UserDTO() { Sex = 0 };
            var r = await _proxy.GetUserModelsAsync( reqDTO );
            var j = JsonConvertUtil.SerializeInCamelCase( r );

            Action action = r.Success ? 
                (Action)( async () => await StdoutAsync( j ) ) :
                (Action)( async () => await StdoutErrAsync( j ) );

            action();

            Result.Success = r.Success;
            return Result;
        }
    }
}