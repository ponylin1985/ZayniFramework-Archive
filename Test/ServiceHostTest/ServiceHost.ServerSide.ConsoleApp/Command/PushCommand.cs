using ServiceHost.Test.Entity;
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
using ZayniFramework.Serialization;


namespace ServiceHost.ServerSide.App
{
    /// <summary>ServiceEvent 訊息發布的測試命令 (原始命令: push)
    /// </summary>
    public class PushCommand : RemoteCommand
    {
        /// <summary>執行命令處理
        /// </summary>
        /// <param name="parameters">命令參數集合</param>
        /// <returns>執行結果</returns>
        public override IResult Execute( ParameterCollection parameters )
        {
            var messageDTO = new SomethingDTO()
            {
                SomeMessage     = "This is happy bear!",
                SomeMagicNumber = 8833.324,
                LuckyNumber     = 23
            };

            // 第一種寫法:
            //MiddlewareService.Publish( "ServiceHost.Test.gRPC", "HappyPublish", dto );
            //MiddlewareService.Publish( "ServiceHost.Test.TCPSocket", "HappyPublish", dto );

            // 第二種寫法:
            new HappyServiceEvent( "ServiceHost.Test.TCPSocket", messageDTO ).Fire();

            Stdout( $"Publish SomethingingDTO message now..." );
            Stdout( JsonConvertUtil.Serialize( messageDTO ) );
            Result.Success = true;
            return Result;
        }
    }
}