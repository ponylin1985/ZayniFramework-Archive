﻿using ServiceHost.Test.Entity;
using System;
using System.Collections.Generic;
using ZayniFramework.Middle.Service;


namespace ServiceHost.ServerSide
{
    /// <summary>某一種服務事件
    /// </summary>
    public sealed class HappyServiceEvent : ServiceEvent
    {
        /// <summary>多載建構子
        /// </summary>
        /// <param name="serviceName">服務設定名稱</param>
        /// <param name="messageDTO">訊息資料內容</param>
        public HappyServiceEvent( string serviceName, SomethingDTO messageDTO ) : base( serviceName, eventName: "HappyPublish", messageContent: messageDTO ) 
        {
            // pass
        }

        /// <summary>強型別資料內容
        /// </summary>
        public SomethingDTO DataContent => MessageContent as SomethingDTO;

        /// <summary>事件觸發前的處理
        /// </summary>
        protected override void DoExecuting()
        {
            SomethingDTO messageDTO    = MessageContent as SomethingDTO;
            messageDTO.SomeMessage     = "AAABBBCCC";
            messageDTO.SomeDate        = new DateTime( 2015, 5, 19 );
            messageDTO.SomeMagicNumber = 777.777;
            messageDTO.LuckyNumber     = 666666;
            messageDTO.IsSuperMan      = false;
            base.DoExecuting();
        }
    }
}
