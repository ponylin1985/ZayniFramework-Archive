using ServiceHost.Service.DataAccess;
using ServiceHost.Test.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess.Lightweight;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ServiceHost.ServerSide
{
    /// <summary>取得使用者資料模型的測試動作
    /// </summary>
    public class GetUserModelsAction : ServiceAction<UserDTO, Result<List<UserDTO>>>
    {
        /// <summary>資料存取物件
        /// </summary>
        private static readonly UserDao _dao = new UserDao();

        /// <summary>預設建構子
        /// </summary>
        public GetUserModelsAction() : base( actionName: "GetUserModelsAction" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Response = Execute( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public Result<List<UserDTO>> Execute( UserDTO request )
        {
            var result = Result.Create<List<UserDTO>>();

            if ( request.Sex > 1 || request.Sex < 0 )
            {
                result.Message = $"Invalie request. Sex is invalid.";
                result.Code    = StatusCode.INVALID_REQUEST;
                return result;
            }

            var args = new UserModel() 
            {
                Sex = request.Sex
            };

            var r = _dao.Select_LoadDataToModel( args );

            if ( !r.Success )
            {
                result.Message = $"Get user data from database fail. {r.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            if ( r.Data.IsNullOrEmptyList() )
            {
                result.Message = $"No data found.";
                result.Code    = StatusConstant.NO_DATA_FOUND;
                result.Success = true;
                return result;
            }

            List<UserModel> models = r.Data;

            result.Data    = models.Select( m => { m.DOB = m.DOB?.ToUtcKind(); return m.ConvertTo<UserDTO>(); } ).ToList();
            result.Code    = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }
    }

    /// <summary>取得使用者資料模型的測試動作
    /// </summary>
    public class GetUserModelsActionAsync : ServiceActionAsync<UserDTO, Result<List<UserDTO>>>
    {
        /// <summary>資料存取物件
        /// </summary>
        private static readonly UserDao _dao = new UserDao();

        /// <summary>預設建構子
        /// </summary>
        public GetUserModelsActionAsync() : base( actionName: "GetUserModelsActionAsync" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync() => Response = await ExecuteAsync( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<List<UserDTO>>> ExecuteAsync( UserDTO request )
        {
            var result = Result.Create<List<UserDTO>>();

            if ( request.Sex > 1 || request.Sex < 0 )
            {
                result.Message = $"Invalie request. Sex is invalid.";
                result.Code    = StatusCode.INVALID_REQUEST;
                return result;
            }

            var args = new UserModel() 
            {
                Sex = request.Sex
            };

            var r = await _dao.Select_LoadDataToModelAsync( args );

            if ( !r.Success )
            {
                result.Message = $"Get user data from database fail. {r.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            if ( r.Data.IsNullOrEmptyList() )
            {
                result.Message = $"No data found.";
                result.Code    = StatusConstant.NO_DATA_FOUND;
                result.Success = true;
                return result;
            }

            List<UserModel> models = r.Data;

            result.Data    = models.Select( m => { m.DOB = m.DOB?.ToUtcKind(); return m.ConvertTo<UserDTO>(); } ).ToList();
            result.Code    = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }
    }
}