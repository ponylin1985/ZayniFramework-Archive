using ServiceHost.Service.DataAccess;
using ServiceHost.Test.Entity;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ServiceHost.Service.Library.Action
{
    /// <summary>刪除使用者資料模型的動作
    /// </summary>
    public class DeleteUserModelAction : ServiceAction<UserDTO, Result>
    {
        /// <summary>資料存取物件
        /// </summary>
        private static readonly UserDao _dao = new UserDao();

        /// <summary>預設建構子
        /// </summary>
        public DeleteUserModelAction() : base( actionName: "DeleteUserModelAction" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Response = Execute( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public Result Execute( UserDTO request )
        {
            var result = Result.Create();

            var model = new UserModel() 
            {
                Account = request.Account
            };           

            var r = _dao.Delete_ExecuteNonQuery( model );

            if ( !r.Success )
            {
                result.Message = $"Delete user data from database fail. {r.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            result.Code    = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }
    }

    /// <summary>刪除使用者資料模型的動作
    /// </summary>
    public class DeleteUserModelActionAsync : ServiceActionAsync<UserDTO, Result>
    {
        /// <summary>資料存取物件
        /// </summary>
        private static readonly UserDao _dao = new UserDao();

        /// <summary>預設建構子
        /// </summary>
        public DeleteUserModelActionAsync() : base( actionName: "DeleteUserModelActionAsync" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync() => Response = await ExecuteAsync( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public async Task<Result> ExecuteAsync( UserDTO request )
        {
            var result = Result.Create();

            var model = new UserModel() 
            {
                Account = request.Account
            };           

            var r = await _dao.DeleteAsync( model );

            if ( !r.Success )
            {
                result.Message = $"Delete user data from database fail. {r.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            result.Code    = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }
    }
}