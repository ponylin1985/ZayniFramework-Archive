using ServiceHost.Service.DataAccess;
using ServiceHost.Test.Entity;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ServiceHost.Service.Library.Action
{
    /// <summary>建立使用者資料模型的動作
    /// </summary>
    public class CreateUserModelAction : ServiceAction<UserDTO, Result<UserDTO>>
    {
        /// <summary>資料存取物件
        /// </summary>
        private static readonly UserDao _dao = new UserDao();

        /// <summary>預設建構子
        /// </summary>
        public CreateUserModelAction() : base( actionName: "CreateUserModelAction" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Response = Execute( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public Result<UserDTO> Execute( UserDTO request )
        {
            var result = Result.Create<UserDTO>();

            var model = new UserModel() 
            {
                Account = RandomTextHelper.Create( 10 ),
                Name    = request.Name,
                Age     = request.Age,
                Sex     = request.Sex,
                DOB     = request.DOB,
                IsVip   = request.IsVip,
                IsGood  = request.IsGood
            };           

            var r = _dao.Insert_ExecuteNonQuery( model );

            if ( !r.Success )
            {
                result.Message = $"Insert user data to database fail. {r.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var g = _dao.Get( model );

            if ( !g.Success || g.Data.IsNull() )
            {
                result.Message = $"Insert user data to database fail due to can not get created data after insert. {g.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var resModel   = g.Data;
            var resDTO     = resModel.ConvertTo<UserDTO>();
            resDTO.DOB     = resDTO.DOB?.ToUtcKind();

            result.Data    = resDTO;
            result.Code    = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }
    }

    /// <summary>建立使用者資料模型的動作
    /// </summary>
    public class CreateUserModelActionAsync : ServiceActionAsync<UserDTO, Result<UserDTO>>
    {
        /// <summary>資料存取物件
        /// </summary>
        private static readonly UserDao _dao = new UserDao();

        /// <summary>預設建構子
        /// </summary>
        public CreateUserModelActionAsync() : base( actionName: "CreateUserModelActionAsync" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync() => Response = await ExecuteAsync( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<UserDTO>> ExecuteAsync( UserDTO request )
        {
            var result = Result.Create<UserDTO>();

            var model = new UserModel() 
            {
                Account = RandomTextHelper.Create( 10 ),
                Name    = request.Name,
                Age     = request.Age,
                Sex     = request.Sex,
                DOB     = request.DOB,
                IsVip   = request.IsVip,
                IsGood  = request.IsGood
            };           

            var r = await _dao.InsertAsync( model );

            if ( !r.Success )
            {
                result.Message = $"Insert user data to database fail. {r.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var g = await _dao.GetAsync( model );

            if ( !g.Success || g.Data.IsNull() )
            {
                result.Message = $"Insert user data to database fail due to can not get created data after insert. {g.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var resModel   = g.Data;
            var resDTO     = resModel.ConvertTo<UserDTO>();
            resDTO.DOB     = resDTO.DOB?.ToUtcKind();

            result.Data    = resDTO;
            result.Code    = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }
    }
}