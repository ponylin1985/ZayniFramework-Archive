using ServiceHost.Service.DataAccess;
using ServiceHost.Test.Entity;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess.Lightweight;
using ZayniFramework.Middle.Service;
using ZayniFramework.Middle.Service.Entity;


namespace ServiceHost.ServerSide
{
    /// <summary>取得使用者資料模型的測試動作
    /// </summary>
    public class GetUserModelAction : ServiceAction<UserDTO, Result<UserDTO>>
    {
        /// <summary>資料存取物件
        /// </summary>
        private static readonly UserDao _dao = new UserDao();

        /// <summary>預設建構子
        /// </summary>
        public GetUserModelAction() : base( actionName: "GetUserModelAction" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Response = Execute( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public Result<UserDTO> Execute( UserDTO request )
        {
            var result = Result.Create<UserDTO>();

            if ( request.Account.IsNullOrEmpty() && request.Name.IsNullOrEmpty() )
            {
                result.Message = $"Invalie request. The 'account_id' and 'name' can not both null or empty string.";
                result.Code    = StatusCode.INVALID_REQUEST;
                return result;
            }

            var query = new UserModel() 
            {
                Account = request.Account,
                Name    = request.Name
            };

            var r = _dao.Get( query );

            if ( !r.Success )
            {
                result.Message = $"Get user data from database fail. {r.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var model = r.Data;

            if ( model.IsNull() )
            {
                result.Message = $"No data found.";
                result.Code    = StatusConstant.NO_DATA_FOUND;
                result.Success = true;
                return result;
            }

            var resDTO = model.ConvertTo<UserDTO>();
            resDTO.DOB = resDTO.DOB?.ToUtcKind();

            result.Data    = resDTO;
            result.Code    = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }
    }

    /// <summary>取得使用者資料模型的測試動作
    /// </summary>
    public class GetUserModelActionAsync : ServiceActionAsync<UserDTO, Result<UserDTO>>
    {
        /// <summary>資料存取物件
        /// </summary>
        private static readonly UserDao _dao = new UserDao();

        /// <summary>預設建構子
        /// </summary>
        public GetUserModelActionAsync() : base( actionName: "GetUserModelActionAsync" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync() => Response = await ExecuteAsync( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行動作
        /// </summary>
        /// <param name="request">查詢參數</param>
        /// <returns>查詢結果</returns>
        public async Task<Result<UserDTO>> ExecuteAsync( UserDTO request )
        {
            var result = Result.Create<UserDTO>();

            if ( request.Account.IsNullOrEmpty() && request.Name.IsNullOrEmpty() )
            {
                result.Message = $"Invalie request. The 'account_id' and 'name' can not both null or empty string.";
                result.Code    = StatusCode.INVALID_REQUEST;
                return result;
            }

            var query = new UserModel() 
            {
                Account = request.Account,
                Name    = request.Name
            };

            var r = await _dao.GetAsync( query );

            if ( !r.Success )
            {
                result.Message = $"Get user data from database fail. {r.Message}";
                result.Code    = StatusCode.INTERNAL_ERROR;
                return result;
            }

            var model = r.Data;

            if ( model.IsNull() )
            {
                result.Message = $"No data found.";
                result.Code    = StatusConstant.NO_DATA_FOUND;
                result.Success = true;
                return result;
            }

            var resDTO = model.ConvertTo<UserDTO>();
            resDTO.DOB = resDTO.DOB?.ToUtcKind();

            result.Data    = resDTO;
            result.Code    = StatusCode.ACTION_SUCCESS;
            result.Success = true;
            return result;
        }
    }
}