﻿using ServiceHost.Test.Entity;
using System;
using ZayniFramework.Middle.Service;
using ZayniFramework.Serialization;


namespace ServiceHost.ServerSide
{
    /// <summary>測試用的事件動作
    /// </summary>
    public class SomeEventAction : EventAction<SomethingDTO>
    {
        /// <summary>預設建構子
        /// </summary>
        public SomeEventAction() : base( "SomeEventAction" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Console.Write( $"{JsonConvertUtil.SerializeInCamelCase( Request )}" );
    }
}
