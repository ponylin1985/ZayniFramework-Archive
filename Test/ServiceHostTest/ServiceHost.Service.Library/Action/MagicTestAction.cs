﻿using ServiceHost.Test.Entity;
using System;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Serialization;


namespace ServiceHost.ServerSide
{
    /// <summary>神奇的測試動作
    /// </summary>
    public class MagicTestAction : ServiceAction<SomethingDTO, Result<MagicDTO>>
    {
        /// <summary>預設建構子
        /// </summary>
        public MagicTestAction() : base( actionName: "MagicTestAction" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override void Execute() => Response = Execute( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行測試用的動作
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Result<MagicDTO> Execute( SomethingDTO request ) 
        {
            var result = Result.Create<MagicDTO>();
            Logger.WriteInformationLog( this, $"Receive SomethingDTO is {Environment.NewLine}{JsonConvertUtil.SerializeInCamelCase( request )}", nameof ( Execute ) );

            var resDTO = new MagicDTO()
            {
                MagicWords = $"{RandomTextHelper.Create( 8 )}",
                MagicTime  = DateTime.UtcNow
            };

            Check.IsTrue( 7.77 == request.SomeMagicNumber, () => resDTO.TestDouble = 999.999 );

            result.Data    = resDTO;
            result.Success = true;
            return result;
        }
    }

    /// <summary>非同步作業的服務測試動作
    /// </summary>
    public class MagicTestActionAsync : ServiceActionAsync<SomethingDTO, Result<MagicDTO>>
    {
        /// <summary>預設建構子
        /// </summary>
        public MagicTestActionAsync() : base( actionName: "MagicTestActionAsync" )
        {
        }

        /// <summary>執行動作
        /// </summary>
        public override async Task ExecuteAsync() => Response = await ExecuteAsync( Request );

        // Pony Says: 這種方法標記可以當作預留，也不是沒有好處，這種呼叫方式理論上會得到效能最佳化。
        /// <summary>執行測試用的動作
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<Result<MagicDTO>> ExecuteAsync( SomethingDTO request ) 
        {
            var result = Result.Create<MagicDTO>();
            Logger.WriteInformationLog( this, $"Receive SomethingDTO is {Environment.NewLine}{JsonSerialize.SerializeObject( request )}", nameof ( ExecuteAsync ) );

            var resDTO = new MagicDTO()
            {
                MagicWords = $"{RandomTextHelper.Create( 8 )}",
                MagicTime  = DateTime.UtcNow
            };

            Check.IsTrue( 7.77 == request.SomeMagicNumber, () => resDTO.TestDouble = 999.999 );

            result.Data    = resDTO;
            result.Success = true;
            return Task.FromResult( result );
        }
    }
}
