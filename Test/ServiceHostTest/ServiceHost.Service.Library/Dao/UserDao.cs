using ServiceHost.Test.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ZayniFramework.Common;
using ZayniFramework.DataAccess;
using ZayniFramework.DataAccess.Lightweight;


namespace ServiceHost.Service.DataAccess
{
    /// <summary>單元測試用 BaseDataAccess 測試 Dao 類別
    /// </summary>
    public class UserDao : BaseDataAccess
    {
        #region 宣告建構子

        /// <summary>預設建構子
        /// </summary>
        public UserDao() : base( "ZayniUnitTest" )
        {
        }

        #endregion 宣告建構子


        #region 宣告測試方法

        /// <summary>新增資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>新增結果</returns>
        internal IResult Insert_ExecuteNonQuery( UserModel model )
        {
            var result = Result.Create();

            string sql = @" 
                INSERT INTO `FS_UNIT_TEST_USER` (
                      `ACCOUNT_ID`
                    , `NAME`
                    , `AGE`
                    , `SEX`
                    , `BIRTHDAY`
                    , `IS_VIP`
                    , `IS_GOOD`
                ) VALUES (
                      @AccountId
                    , @Name
                    , @Age
                    , @Sex
                    , @Birthday
                    , @IsVip
                    , @IsGood
                ); ";

            using ( var conn  = base.CreateConnection() )
            using ( var trans = base.BeginTransaction( conn ) )
            {
                var cmd = GetSqlStringCommand( sql, conn, trans );
                base.AddInParameter( cmd, "@AccountId", DbTypeCode.String,   model.Account );
                base.AddInParameter( cmd, "@Name",      DbTypeCode.String,   model.Name );
                base.AddInParameter( cmd, "@Age",       DbTypeCode.Int32,    model.Age );
                base.AddInParameter( cmd, "@Sex",       DbTypeCode.Int32,    model.Sex );
                base.AddInParameter( cmd, "@Birthday",  DbTypeCode.DateTime, model.DOB );
                base.AddInParameter( cmd, "@IsVip",     DbTypeCode.Boolean,  model.IsVip );
                base.AddInParameter( cmd, "@IsGood",    DbTypeCode.Boolean,  model.IsGood );

                if ( !base.ExecuteNonQuery( cmd ) )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if ( 1 != DataCount )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = $"Insert into test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if ( !base.Commit( trans ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>新增資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>新增結果</returns>
        internal async Task<IResult> InsertAsync( UserModel model )
        {
            var result = Result.Create();

            string sql = @" 
                INSERT INTO `FS_UNIT_TEST_USER` (
                      `ACCOUNT_ID`
                    , `NAME`
                    , `AGE`
                    , `SEX`
                    , `BIRTHDAY`
                    , `IS_VIP`
                    , `IS_GOOD`
                ) VALUES (
                      @AccountId
                    , @Name
                    , @Age
                    , @Sex
                    , @Birthday
                    , @IsVip
                    , @IsGood
                ); ";

            using ( var conn  = await base.CreateConnectionAsync() )
            using ( var trans = base.BeginTransaction( conn ) )
            {
                var cmd = GetSqlStringCommand( sql, conn, trans );
                base.AddInParameter( cmd, "@AccountId", DbTypeCode.String,   model.Account );
                base.AddInParameter( cmd, "@Name",      DbTypeCode.String,   model.Name );
                base.AddInParameter( cmd, "@Age",       DbTypeCode.Int32,    model.Age );
                base.AddInParameter( cmd, "@Sex",       DbTypeCode.Int32,    model.Sex );
                base.AddInParameter( cmd, "@Birthday",  DbTypeCode.DateTime, model.DOB );
                base.AddInParameter( cmd, "@IsVip",     DbTypeCode.Boolean,  model.IsVip );
                base.AddInParameter( cmd, "@IsGood",    DbTypeCode.Boolean,  model.IsGood );

                if ( !await base.ExecuteNonQueryAsync( cmd ) )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if ( 1 != DataCount )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = $"Insert into test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if ( !base.Commit( trans ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>更新資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>更新結果</returns>
        internal IResult Update_ExecuteNonQuery( UserModel model )
        {
            var result = Result.Create();

            string sql = @" 
                UPDATE `FS_UNIT_TEST_USER`
                   SET `NAME`       = @Name,
                       `SEX`        = @Sex,
                       `AGE`        = @Age, 
                       `BIRTHDAY`   = @Birthday
                 WHERE `ACCOUNT_ID` = @AccountId; ";

            using ( var conn  = base.CreateConnection() )
            using ( var trans = base.BeginTransaction( conn ) )
            {
                var cmd = GetSqlStringCommand( sql, conn, trans );
                base.AddInParameter( cmd, "@Name",      DbTypeCode.String,   model.Name );
                base.AddInParameter( cmd, "@Sex",       DbTypeCode.Int32,    model.Sex );
                base.AddInParameter( cmd, "@Age",       DbTypeCode.Int32,    model.Age );
                base.AddInParameter( cmd, "@Birthday",  DbTypeCode.DateTime, model.DOB );
                base.AddInParameter( cmd, "@AccountId", DbTypeCode.String,   model.Account );

                if ( !base.ExecuteNonQuery( cmd ) )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if ( 1 != DataCount )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = $"Update test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if ( !base.Commit( trans ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>更新資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>更新結果</returns>
        internal async Task<IResult> UpdateAsync( UserModel model )
        {
            var result = Result.Create();

            string sql = @" 
                UPDATE `FS_UNIT_TEST_USER`
                   SET `NAME`       = @Name,
                       `SEX`        = @Sex,
                       `AGE`        = @Age, 
                       `BIRTHDAY`   = @Birthday
                 WHERE `ACCOUNT_ID` = @AccountId; ";

            using ( var conn  = await base.CreateConnectionAsync() )
            using ( var trans = base.BeginTransaction( conn ) )
            {
                var cmd = GetSqlStringCommand( sql, conn, trans );
                base.AddInParameter( cmd, "@Name",      DbTypeCode.String,   model.Name );
                base.AddInParameter( cmd, "@Sex",       DbTypeCode.Int32,    model.Sex );
                base.AddInParameter( cmd, "@Age",       DbTypeCode.Int32,    model.Age );
                base.AddInParameter( cmd, "@Birthday",  DbTypeCode.DateTime, model.DOB );
                base.AddInParameter( cmd, "@AccountId", DbTypeCode.String,   model.Account );

                if ( !await base.ExecuteNonQueryAsync( cmd ) )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if ( 1 != DataCount )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = $"Update test data to FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if ( !base.Commit( trans ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>刪除資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>刪除結果</returns>
        internal IResult Delete_ExecuteNonQuery( UserModel model )
        {
            var result = Result.Create();

            string sql = @" DELETE FROM `FS_UNIT_TEST_USER`
                             WHERE `ACCOUNT_ID` = @AccountId; ";

            using ( var conn  = base.CreateConnection() )
            using ( var trans = base.BeginTransaction( conn ) ) 
            {
                var cmd = GetSqlStringCommand( sql, conn, trans );
                base.AddInParameter( cmd, "@AccountId", DbTypeCode.String, model.Account );

                if ( !base.ExecuteNonQuery( cmd ) )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if ( 1 != DataCount )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = $"Delete test data from FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if ( !base.Commit( trans ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>刪除資料 ExecuteNoeQuery 測試
        /// </summary>
        /// <param name="model">資料模型</param>
        /// <returns>刪除結果</returns>
        internal async Task<IResult> DeleteAsync( UserModel model )
        {
            var result = Result.Create();

            string sql = @" DELETE FROM `FS_UNIT_TEST_USER`
                             WHERE `ACCOUNT_ID` = @AccountId; ";

            using ( var conn  = await base.CreateConnectionAsync() )
            using ( var trans = base.BeginTransaction( conn ) ) 
            {
                var cmd = GetSqlStringCommand( sql, conn, trans );
                base.AddInParameter( cmd, "@AccountId", DbTypeCode.String, model.Account );

                if ( !await base.ExecuteNonQueryAsync( cmd ) )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                if ( 1 != DataCount )
                {
                    base.Rollback( trans );
                    conn.Close();
                    result.Message = $"Delete test data from FS_UNIT_TEST_USER fail.";
                    return result;
                }

                if ( !base.Commit( trans ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
            }

            result.Success = true;
            return result;
        }

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<UserModel> Get( UserModel query )
        {
            var result = Result.Create<UserModel>();

            string sqlWhere = "";

            if ( query.Account.IsNotNullOrEmpty() )
            {
                sqlWhere += $"AND `ACCOUNT_ID` = @AccountId";
            }

            if ( query.Name.IsNotNullOrEmpty() )
            {
                sqlWhere += $"{Environment.NewLine}AND `NAME` = @Name";
            }

            string sql = $@"
                SELECT 
                    `ACCOUNT_ID`
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY`

                  , `IS_VIP`
                  , `IS_GOOD`
                  , `DATA_FLAG`
                 FROM `FS_UNIT_TEST_USER`
                WHERE 1 = 1
                  {sqlWhere}; ";

            using ( var conn = base.CreateConnection() )
            {
                var cmd = base.GetSqlStringCommand( sql, conn );
                query.Account.IsNotNullOrEmpty( n => base.AddInParameter( cmd, "@AccountId", DbTypeCode.String, query.Account ) );
                query.Name.IsNotNullOrEmpty(    m => base.AddInParameter( cmd, "@Name",      DbTypeCode.String, query.Name ) );

                if ( !base.LoadDataToModel( cmd, out List<UserModel> models ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();

                if ( models.IsNullOrEmptyList() || models.Count != 1 )
                {
                    result.Message = $"No data found.";
                    result.Code    = StatusConstant.NO_DATA_FOUND;
                    result.Success = true;
                    return result;
                }

                var model = models.FirstOrDefault();

                result.Data    = model;
                result.Success = true;
                return result;
            }
        }

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        public async Task<IResult<UserModel>> GetAsync( UserModel query )
        {
            var result = Result.Create<UserModel>();

            string sqlWhere = "";

            if ( query.Account.IsNotNullOrEmpty() )
            {
                sqlWhere += $"AND `ACCOUNT_ID` = @AccountId";
            }

            if ( query.Name.IsNotNullOrEmpty() )
            {
                sqlWhere += $"{Environment.NewLine}AND `NAME` = @Name";
            }

            string sql = $@"
                SELECT 
                    `ACCOUNT_ID`
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY`

                  , `IS_VIP`
                  , `IS_GOOD`
                  , `DATA_FLAG`
                 FROM `FS_UNIT_TEST_USER`
                WHERE 1 = 1
                  {sqlWhere}; ";

            using ( var conn = await base.CreateConnectionAsync() )
            {
                var cmd = base.GetSqlStringCommand( sql, conn );
                query.Account.IsNotNullOrEmpty( n => base.AddInParameter( cmd, "@AccountId", DbTypeCode.String, query.Account ) );
                query.Name.IsNotNullOrEmpty(    m => base.AddInParameter( cmd, "@Name",      DbTypeCode.String, query.Name ) );

                var r = await base.LoadDataToModelAsync<UserModel>( cmd );

                if ( !r.Success )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();

                var models = r.Data;

                if ( models.IsNullOrEmptyList() || models.Count != 1 )
                {
                    result.Message = $"No data found.";
                    result.Code    = StatusConstant.NO_DATA_FOUND;
                    result.Success = true;
                    return result;
                }

                var model = models.FirstOrDefault();

                result.Data    = model;
                result.Success = true;
                return result;
            }
        }

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<List<UserModel>> Select_LoadDataToModel( UserModel query )
        {
            var result = Result.Create<List<UserModel>>();

            string sql = @"
                SELECT 
                    `ACCOUNT_ID`
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY`

                  , `IS_VIP`
                  , `IS_GOOD`
                  , `DATA_FLAG`
                 FROM `FS_UNIT_TEST_USER`
                WHERE `SEX` = @Sex; ";

            using ( var conn = base.CreateConnection() )
            {
                var cmd = base.GetSqlStringCommand( sql, conn );
                base.AddInParameter( cmd, "@Sex", DbTypeCode.Int32, query.Sex );

                if ( !base.LoadDataToModel( cmd, out List<UserModel> models ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();

                result.Data    = models;
                result.Success = true;
                return result;
            }
        }

        /// <summary>查詢資料 LoadDataToModel 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal async Task<IResult<List<UserModel>>> Select_LoadDataToModelAsync( UserModel query )
        {
            var result = Result.Create<List<UserModel>>();

            string sql = @"
                SELECT 
                    `ACCOUNT_ID`
                  , `NAME`
                  , `AGE`
                  , `SEX`
                  , `BIRTHDAY`

                  , `IS_VIP`
                  , `IS_GOOD`
                  , `DATA_FLAG`
                 FROM `FS_UNIT_TEST_USER`
                WHERE `SEX` = @Sex; ";

            using ( var conn = base.CreateConnection() )
            {
                var cmd = base.GetSqlStringCommand( sql, conn );
                base.AddInParameter( cmd, "@Sex", DbTypeCode.Int32, query.Sex );

                var g = await base.LoadDataToModelAsync<UserModel>( cmd );

                if ( !g.Success )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();

                var models = g.Data;

                result.Data    = models;
                result.Success = true;
                return result;
            }
        }

        /// <summary>查詢資料 LoadDataToModels 測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult Select_LoadDataToModels( UserModel query )
        {
            dynamic result = Result.CreateDynamicResult();  

            string sql = @" -- QueryNo1
                            SELECT `ACCOUNT_ID` AS Account
                                 , `NAME`
                                 , `AGE`
                                 , `SEX`
                                 , `BIRTHDAY` AS DoB

                                 , `IS_VIP`
                            FROM `FS_UNIT_TEST_USER`
                                WHERE `SEX` = @Sex;

                            -- QueryNo2
                            SELECT `ACCOUNT_ID` AS Account
                                 , `NAME`
                                 , `AGE`
                                 , `SEX`
                                 , `BIRTHDAY` AS DoB
                            FROM `FS_UNIT_TEST_USER`
                                WHERE `SEX` = @Sex; ";

            using ( var conn = base.CreateConnection() ) 
            {
                var cmd = base.GetSqlStringCommand( sql, conn );
                base.AddInParameter( cmd, "@Sex", DbTypeCode.Int32, query.Sex );
                
                Type[] types = { typeof ( UserModel ), typeof ( UserModel ) };

                if ( !base.LoadDataToModels( cmd, types, out List<object[]> datas ) )
                {
                    conn.Close();
                    return result;
                }

                conn.Close();

                List<UserModel> queryResult1 = datas.FirstOrDefault().ToList<UserModel>();
                List<UserModel> queryResult2 = datas.LastOrDefault().ToList<UserModel>();

                result.Rst1    = queryResult1;
                result.Rst2    = queryResult2;
                result.Success = true;
                return result;
            }
        }

        /// <summary>查詢資料，動態 ORM 繫結 LoadDataToDynamicModel 的測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<List<dynamic>> Select_LoadDataToDynamicModel( UserModel query )
        {
            var result = Result.Create<List<dynamic>>();

            string sql = @"SELECT `ACCOUNT_ID` AS `Account`
                                , `NAME`
                                , `AGE`
                                , `SEX`
                                , `BIRTHDAY` AS `DoB`

                                , `IS_VIP`
                            FROM `FS_UNIT_TEST_USER`
                                WHERE `SEX` = @Sex ";

            using ( var conn = base.CreateConnection() ) 
            {
                var cmd = base.GetSqlStringCommand( sql, conn );
                base.AddInParameter( cmd, "@Sex", DbTypeCode.Int32, query.Sex );

                if ( !base.LoadDataToDynamicModel( cmd, out var models ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();

                result.Data    = models;
                result.Success = true;
                return result;
            }
        }

        /// <summary>查詢資料，動態 ORM 繫結多個查詢到 LoadDataToDynamicCollection 的測試
        /// </summary>
        /// <param name="query">查詢條件</param>
        /// <returns>查詢結果</returns>
        internal IResult<Dictionary<string, List<dynamic>>> Select_LoadDataToDynamicCollection( UserModel query )
        {
            var result = Result.Create<Dictionary<string, List<dynamic>>>();

            string sql = @" -- QueryNo1
                            SELECT `ACCOUNT_ID` AS Account
                                 , `NAME`
                                 , `AGE`
                                 , `SEX`
                                 , `BIRTHDAY` AS DoB
                                 , `IS_VIP`
                            FROM `FS_UNIT_TEST_USER`
                                WHERE `SEX` = @Sex;

                            -- QueryNo2
                            SELECT `ACCOUNT_ID` AS Account
                                 , `NAME`
                                 , `AGE`
                                 , `SEX`
                                 , `BIRTHDAY` AS DoB
                            FROM `FS_UNIT_TEST_USER`
                                WHERE `SEX` = @Sex; ";

            using ( var conn = base.CreateConnection() ) 
            {
                var cmd = base.GetSqlStringCommand( sql, conn );
                base.AddInParameter( cmd, "@Sex", DbTypeCode.Int32, query.Sex );

                if ( !base.LoadDataToDynamicCollection( cmd, out var collects, "QueryNo1", "QueryNo2" ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();

                result.Data      = collects;
                result.Success = true;
                return result;
            }
        }

        /// <summary>查詢純量資料 ExecuteScalar 測試
        /// </summary>
        /// <returns>查詢結果</returns>
        internal IResult<int> SelectCount_ExecuteScalar()
        {
            var result = Result.Create<int>();

            var sql = @" SELECT COUNT(*) FROM `FS_UNIT_TEST_USER`; ";

            using ( var conn = base.CreateConnection() ) 
            {
                var cmd = base.GetSqlStringCommand( sql, conn );

                if ( !base.ExecuteScalar( cmd, out object obj ) )
                {
                    conn.Close();
                    result.Message = base.Message;
                    return result;
                }

                conn.Close();
                int count = int.Parse( obj + "" );

                result.Data    = count;
                result.Success = true;
                return result;
            }
        }

        /// <summary>清空所有測試資料
        /// </summary>
        /// <returns></returns>
        internal IResult Clear() 
        {
            var result = Result.Create();

            var sql = @" DELETE FROM `{tableName}`; ";

            using ( var conn = base.CreateConnection() ) 
            {
                var cmd = base.GetSqlStringCommand( sql.Format( tableName => "FS_UNIT_TEST_USER" ), conn );

                if ( !base.ExecuteNonQuery( cmd ) ) 
                {
                    conn.Close();
                    result.Message = $"Clear FS_UNIT_TEST_USER table fail.";
                    return result;
                }

                cmd = base.GetSqlStringCommand( sql.Format( tableName => "FS_UNIT_TEST_USER_PHONE_DETAIL" ), conn );

                if ( !base.ExecuteNonQuery( cmd ) ) 
                {
                    conn.Close();
                    result.Message = $"Clear FS_UNIT_TEST_USER_PHONE_DETAIL table fail.";
                    return result;
                }

                conn.Close();

                result.Success = true;
                return result;
            }
        }

        #endregion 宣告測試方法
    }
}