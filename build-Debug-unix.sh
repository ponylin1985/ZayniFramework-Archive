#!/bin/bash

find . -type f -name '*.nupkg' -delete

dotnet clean -c Debug
dotnet msbuild /t:Rebuild /p:Configuration=Debug /clp:Summary

find . -type f -name '*.nupkg' -delete