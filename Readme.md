# Zayni Framework

--------------------

*   **Create By:** Lin, Ping-chen
*   **Create Date:** 2016-05-17
*   **Update Date:** 2020-04-15

--------------------

The **Zayni Framework** is an application framework developed in C# programming language with .NET Standard 2.0. The **Zayni Framework** is try to help every .NET developer to build their applications and libraries easier.
_The concept is make every C# code or .NET libraries reusable, flexible and combinable._

**Zayni Framework** provide many kinds of common used libraries, extensions and service middleware framework for any .NET application (both .NET Core or .NET Framework application). If you want to know how to use **Zayni Framework**, you can take a look from the unit test projects or other test projects.

The modules in the **Zayni Framework**:

* **Common** module (Configs, interfaces, extensions, AOP interceptors, dynamic APIs, reflection APIs and converters... etc)
* **Caching** module (Memory, Redis, MySQL Memory Table Engine, ZayniFramework.Remote.ShareData.Service.)
* **Caching.RemoteCache** module (Out-process remote cache service library.)
* **Caching.RedisClient** module (The Redis client components.)
* **Compression** module (Provide GZip compression and extensions.)
* **Cryptography** module (AES, DES, SHA256, SHA512... etc and KeyMaker SDK tool.)
* **Data Access & ORM** module (This is a wrapper for ADO.NET. Micro ORM, dynamic ORM for data access. Support the SQL Server, MySQL, PostgreSQL, SQL logging and database mirgarion CLI tool.)
* **Data Access with LightORM** module (Provide CRUD ORM DataContext template & SQL Engine.)
* **Exception Handling** module (Exception handler & exception policies.)
* **Formatting** module (Numerical data format, DateTime format, masking... etc)
* **Logging** module (Support log output to file, database, elasticsearch, email, console or windows event manager.)
* **Serialization** module (System.Text.Json, Newtonsoft.Json, XML and BinaryFormatter.)
* **Validation** module (Provide validator and many kinds of validtion attributes.)
* **Middle.Service** (Self-hosted middleware framework. Support multi-hosted remote host services like: TCP Socket, WebSocket, RabbitMQ and also **In-Process** service mode.)
* **Middle.Service.Client** module (The client-side proxy for Middle.Service module. Provide remote client such as the TCPSocket, WebSocket, RabbitMQ and In-Process Proxy to connect and access the Middle.Service remote host service.)
* **Middle.Service.Entity** module (The entities and DTOs for service middleware.)
* **Middle.Service.Grpc** module (The extension module for Middle.Service module. It provide the **gRPC** remote service host which is very fast RPC communication. You don't need to know any details about gRPC or ProtoBuf anymore. Just use the **Middle.Service** module to write your ServiceAction and you will have the benefit in **gRPC** and **Zayni Framework** both! :) )
* **Middle.Service.Grpc.Client** module (The extension module for Middle.Service.Client module. It provide the **gRPC** remote client.)
* **Middle.Service.Grpc.Entity** module (The extension module for Middle.Service.Entity module.)
* **Middle.Service.HttpCore** module (The extension module for Middle.Service module. It provide the **ASP.NET Core Kestrel self-hosted** http remote service. Using this extension module allow you to have more flexible in your application. You can build the http microservice in you existed console apps or any .NET Core applications, not just only in a ASP.NET Core website application.)
* **Middle.Service.HttpCore.Client** module (The extension module for Middle.Service.Client module. It provide the http remote client.)
* **Middle.TelnetService** module (Self-hosted telnet service for any .NET application)
* **Remote.ShareData.Service** 

[Here](https://drive.google.com/file/d/1GquNxi8cQhiLvwaRCkuQK160DYlfzj_x/view?usp=sharing) is the overview and main concept about the **Zayni Framework**.

You are welcome to contact with me if you have any questions or advises. Or create an new issue in this project. My email address is ponylin1985@gmail.com.

--------------------

## The Performance Test Report about the ***Middle.Service module***

The test environment:

* MacBook Pro 15', Intel(R) Core(TM) i7-8750H CPU @ 2.20GHz, 16 GB DDR3 RAM memory, 512 GB SSD.
* The server side application was build into a docker image and run in a docker container.
* The server side and client side application runs in the same MacBook Pro machine.
* Both the server side and client side application is .NET Core 3.1 application.
* Here is the test application.
    * [Server Side test application.](https://gitlab.com/ponylin1985/ZayniFramework/tree/master/Test/WebAPI.Test)
    * [Client Side test application.](https://gitlab.com/ponylin1985/ZayniFramework/tree/master/Test/ServiceHostTest/ServiceHost.Client.ConsoleApp)

* How to run the performance by yourself:
    * You need to `git clone` this project.
    * You need to install the .NET Core SDK with the dotnet CLI.
    * The server-side service host application.
        * The configurations is in `./Test/WebAPI.Test/Configs` folder. You will need to use this path when you're going to run the docker container.
        * Install the **Docker** in your machine and register an account in the [Docker Hub](https://hub.docker.com/).
        * Start the docker service in your machine.
        * Run the `docker pull ponylin1985/webapi.test` to install the server-side application docker image.
        * Run the `docker run -d -v /YourConfigurationPathInYourMachineHere:/app/Configs -p 5590:5590 -p 5580:5580 -p 2221:2221 -p 1110:1110 -p 5000:5000 ponylin1985/webapi.test` to run the server-side docker container.
        * Or you can use the following docker-compose.yml sample, save it and run `docker-compose up -d` to run the docker container.<br/>(Don't forget to change the config path volume mapping in the docker-compose.yml file. :) )
        
        ```yaml
        version: "3"

        services:
            api-service-host:
                image: "webapi.test:${BASE_IMAGE_TAG:-alpine}-x64"
                volumes:
                    # Configuration Path
                    # Change the left-side path if you want to.
                    - ~/GitRepo/MyGitLab/ZayniFramework/Test/WebAPI.Test/Configs:/app/Configs

                    # File Log Path
                    # Change the left-side path if you want to.
                    - ~/ApplicationLog/WebAPI.Test:/app/Log
                ports:
                    - 1110:1110   # gRPC Host Service Port
                    - 5590:5590   # TCPSocket Host Port
                    - 5580:5580   # WebSocket Host Port
                    - 5000:5000   # ASP.NET Core Http Port
                    - 5100:5100   # HttpCore Host Port
                    - 2221:2221   # Remote Telnet Command Service Port
                container_name: zayni.webapi.test.${BASE_IMAGE_TAG:-alpine}
        ```


    * The client-side remote client application.
        * Open your terminal and `cd ./Test/ServiceHostTest/ServiceHost.Client.ConsoleApp`.
        * Restore the nuget packages. Run `dotnet restore`.
        * Build the application. Run `dotnet build`.
        * Run the ServiceHost.Client.ConsoleApp application. Run `dotnet run`.
        * Enter the `init` command to connect to the WebAPI.Test application's service host.
        * Test the connection status is good. Enter `send magic action async` command.
        * Run the `performance test async 1000` command to execute the performance test.
        * Enter `exit` to exit the ServiceHost.Client.ConsoleApp application.



### gRPC extension module.

_The fastest protocol in Middle.Service module._

All success. ExecuteCount: 1000. Total Seconds: 0.703946

All success. ExecuteCount: 1000. Total Seconds: 0.708776

All success. ExecuteCount: 1000. Total Seconds: 0.704821

All success. ExecuteCount: 1000. Total Seconds: 0.694161

All success. ExecuteCount: 1000. Total Seconds: 0.707185

All success. ExecuteCount: 1000. Total Seconds: 0.697523

All success. ExecuteCount: 1000. Total Seconds: 0.711107

All success. ExecuteCount: 1000. Total Seconds: 0.695447

All success. ExecuteCount: 1000. Total Seconds: 0.688807

All success. ExecuteCount: 1000. Total Seconds: 0.697563

**Average Seconds: 0.7089336 sec.**<br/>
**Average: 1000 / 0.7089336 = 1410 req/sec.**

====================

### WebSocket.

_Also a very fast protocol in Middle.Service built-in module._

All success. ExecuteCount: 1000. Total Seconds: 0.859616

All success. ExecuteCount: 1000. Total Seconds: 0.778100

All success. ExecuteCount: 1000. Total Seconds: 0.846912

All success. ExecuteCount: 1000. Total Seconds: 0.767052

All success. ExecuteCount: 1000. Total Seconds: 0.804194

All success. ExecuteCount: 1000. Total Seconds: 0.770630

All success. ExecuteCount: 1000. Total Seconds: 0.812853

All success. ExecuteCount: 1000. Total Seconds: 0.764357

All success. ExecuteCount: 1000. Total Seconds: 0.783368

**Average Seconds: 0.718055 sec.**<br/>
**Average: 1000 / 0.718055 = 1392 req/sec.**

====================

### TCPSocket.

All success. ExecuteCount: 1000. Total Seconds: 2.480603

All success. ExecuteCount: 1000. Total Seconds: 2.520240

All success. ExecuteCount: 1000. Total Seconds: 2.321144

All success. ExecuteCount: 1000. Total Seconds: 2.417020

All success. ExecuteCount: 1000. Total Seconds: 2.394493

All success. ExecuteCount: 1000. Total Seconds: 2.305157

All success. ExecuteCount: 1000. Total Seconds: 2.424263

All success. ExecuteCount: 1000. Total Seconds: 2.437105

All success. ExecuteCount: 1000. Total Seconds: 2.374363

All success. ExecuteCount: 1000. Total Seconds: 2.492791

**Average Seconds: 2.416717 sec.**<br/>
**Average: 1000 / 2.416717 = 413 req/sec.**

====================

### RabbitMQ.

All success. ExecuteCount: 1000. Total Seconds: 4.324135

All success. ExecuteCount: 1000. Total Seconds: 4.295902

All success. ExecuteCount: 1000. Total Seconds: 4.251610

All success. ExecuteCount: 1000. Total Seconds: 4.252727

All success. ExecuteCount: 1000. Total Seconds: 4.191846

All success. ExecuteCount: 1000. Total Seconds: 4.243388

All success. ExecuteCount: 1000. Total Seconds: 4.189890

All success. ExecuteCount: 1000. Total Seconds: 4.247766

All success. ExecuteCount: 1000. Total Seconds: 4.215274

All success. ExecuteCount: 1000. Total Seconds: 4.210999

**Average Seconds: 4.2423537 sec.**<br/>
**Average: 1000 / 4.2423537 = 235 req/sec.**

====================

### HttpCore extension module. (ASP.NET Core Kestrel Wrapper)

All success. ExecuteCount: 1000. Total Seconds: 1.411461

All success. ExecuteCount: 1000. Total Seconds: 1.128453

All success. ExecuteCount: 1000. Total Seconds: 1.122988

All success. ExecuteCount: 1000. Total Seconds: 1.152

All success. ExecuteCount: 1000. Total Seconds: 1.12064

All success. ExecuteCount: 1000. Total Seconds: 1.123421

All success. ExecuteCount: 1000. Total Seconds: 1.127011

All success. ExecuteCount: 1000. Total Seconds: 1.126553

All success. ExecuteCount: 1000. Total Seconds: 1.120762

All success. ExecuteCount: 1000. Total Seconds: 1.154408

**Avage Seconds: 1.1587697 sec.**<br/>
**Average: 1000 / 1.1587697 = 862 req/sec.**

====================

### ASP.NET Core Web API

_This is a performance test compare with the pure ASP.NET Core Web API._<br/>
_The pure ASP.NET Core Kestrel is a really fast http service! (with System.Text.Json serialization)_

All success. ExecuteCount: 1000. Total Seconds: 0.292937

All success. ExecuteCount: 1000. Total Seconds: 0.279686

All success. ExecuteCount: 1000. Total Seconds: 0.293024

All success. ExecuteCount: 1000. Total Seconds: 0.288345

All success. ExecuteCount: 1000. Total Seconds: 0.28721

All success. ExecuteCount: 1000. Total Seconds: 0.289224

All success. ExecuteCount: 1000. Total Seconds: 0.285907

All success. ExecuteCount: 1000. Total Seconds: 0.282055

All success. ExecuteCount: 1000. Total Seconds: 0.282455

All success. ExecuteCount: 1000. Total Seconds: 0.290321

**Avage Seconds: 0.2871164 sec.**<br/>
**Average: 1000 / 0.2871164 = 3482 req/sec.**

====================

### InProcess Client. (The Service-side and RemoteClient is in the **same runtime process**.)

_This is always the fastest!_<br/>

**_If you are using the ***v2.2.7*** version (or higher version) of Middle.Service and Middle.Service.Client module's nuget packages. You will have the best performance when you using InProcessClient._**

_And the beauty of using this **InProcessClient** is:_<br/>
_You don't need to change **any codes** when the in process ServiceHost is seperate out of from your application._
_You only need to change the config in your serviceClientConfig.json. :)_

All success. ExecuteCount: 1000. Total Seconds: 0.0923607

All success. ExecuteCount: 1000. Total Seconds: 0.1034135

All success. ExecuteCount: 1000. Total Seconds: 0.1085959

All success. ExecuteCount: 1000. Total Seconds: 0.1058037

All success. ExecuteCount: 1000. Total Seconds: 0.1064739

All success. ExecuteCount: 1000. Total Seconds: 0.1076791

All success. ExecuteCount: 1000. Total Seconds: 0.0984187

All success. ExecuteCount: 1000. Total Seconds: 0.0943638

All success. ExecuteCount: 1000. Total Seconds: 0.0962036

All success. ExecuteCount: 1000. Total Seconds: 0.0968316

**Avage Seconds: 0.10101145 sec.**<br/>
**Average: 9899.867 req/sec.**

--------------------

## Docker Repository

There are some docker images has been release on the public DockerHub.

### Remote.ShareData.Service (Cache Service Image)

-  Run `docker pull ponylin1985/zayni.sharedata.service:2.14.122` command to download the docker image.
-  More detail you can go to the [DockerHub Repository](https://hub.docker.com/r/ponylin1985/zayni.sharedata.service).

### WebAPI.Test (Middle.Service module test image)

-  Run `docker pull ponylin1985/webapi.test:alpine-x64` command to download the docker image.
-  More detail you can go to the [DockerHub Repository](https://hub.docker.com/r/ponylin1985/webapi.test).


### ServiceHost.Client.ConsoleApp (Middle.Service.Client module test image)

-  Run `docker pull ponylin1985/zayni.client.app:alpine-x64` command to download the docker image.
-  More detail you can go to the [DockerHub Repository](https://cloud.docker.com/repository/docker/ponylin1985/zayni.client.app).

--------------------

## How to install the Zayni Framework.

You need to install .NET Core SDK or nuget first. Then you can use .NET Core CLI command to install each module's nuget package independently.

```js
dotnet add package ZayniFramework.Common
dotnet add package ZayniFramework.Logging
dotnet add package ZayniFramework.DataAccess
dotnet add package ZayniFramework.DataAccess.LightORM
dotnet add package ZayniFramework.Caching
dotnet add package ZayniFramework.Caching.RedisClient
dotnet add package ZayniFramework.Caching.RemoteCache
dotnet add package ZayniFramework.ExceptionHandling
dotnet add package ZayniFramework.Compression
dotnet add package ZayniFramework.Cryptography
dotnet add package ZayniFramework.Formatting
dotnet add package ZayniFramework.Serialization
dotnet add package ZayniFramework.Validation
dotnet add package ZayniFramework.Middle.Service
dotnet add package ZayniFramework.Middle.Service.Client
dotnet add package ZayniFramework.Middle.Service.Entity
dotnet add package ZayniFramework.Middle.Service.Grpc
dotnet add package ZayniFramework.Middle.Service.Grpc.Client
dotnet add package ZayniFramework.Middle.Service.Grpc.Entity
dotnet add package ZayniFramework.Middle.Service.HttpCore
dotnet add package ZayniFramework.Middle.Service.HttpCore.Client
dotnet add package ZayniFramework.Middle.TelnetService
dotnet tool install --global zch
dotnet tool install --global zdm
```

--------------------

## How to use Zayni Framework and Sample Code.


### Add namespace using in your code.

```csharp
using ZayniFramework.Common;
```

### Configuration API example.

#### Read the app.config sample.

If you app.config's location is the same with your entry assembly. You can also use **ConfigurationManager** to read the configurations.<br/>
**BUT** if your app.config's location is placed **in different path with your entry assembly** in runtime. You need to set the app.config file path to **ConfigManagement.ConfigFullPath** property before you using any Zayni Framework's module or libraries. So, the Zayni Framework can load all the configurations correctlly.


```csharp
// This is the case that you place your app.config in different path with your entry assembly. You need to set the ConfigManagement.ConfigFullPath property before you using any Zayni Framework's module.
// If the app.config's path is the same with your entry assembly. You don't need to set the ConfigManagement.ConfigFullPath property.
// You app.config full path here...
var path = $"{Path.GetDirectoryName( Assembly.GetExecutingAssembly().Location )}/Configs/WebAPI.Test.dll.config";
ConfigManagement.ConfigFullPath = path;
```

In your app.config.

```xml
<connectionStrings>
    <add name="AAA" connectionString="SERVER=XXX.XXX.XXX.XXX;Port=3306;DATABASE=zayni;UID=XXX;PASSWORD=XXX;Allow User Variables=True;Charset=utf8;Convert Zero Datetime=True;"       providerName="MySql.Data.MySqlClient"/>
    <add name="BBB" connectionString="Server=XXX.XXX.XXX.XXX,1433;Database=ZayniFramework;User Id=XXX;Password=XXX;Max Pool Size=20;Min Pool Size=5;Connection Lifetime=30;" providerName="System.Data.SqlClient" />
</connectionStrings>
<appSettings>
    <add key="ABC" value="Hello, Zayni Framework" />
    <add key="DEF" value="Zayni Framework with Docker" />
    <add key="WWW" value="true" />
</appSettings>
```

Then you can read the configurations like this.

```csharp
// Read the connection string.
string connectionStringA = ConfigManagement.ConnectionStrings[ "AAA" ].ConnectionString;
string connectionStringB = ConfigManagement.ConnectionStrings[ "BBB" ].ConnectionString;

// Read the appSettings
string a = ConfigManagement.AppSettings[ "ABC" ];
string b = ConfigManagement.AppSettings[ "DEF" ];

// or your can read appSettings like this.
Result r = await AppSettingsLoader.Instance.LoadBooleanConfigSettingAsync( "WWW" );
Assert.IsTrue( r.Success );
Assert.IsTrue( r.Data.Value );  // r.Data.Value is a Boolean type. The value will be true
```

#### Read the json config sample.

If you have a JSON file as a configuarion file like **xxx.json**. You can use **ConfigManager.GetConfig( string path )** method to read the json config file.

```csharp
// The path of your json config file.
string configPath = "./someDir/someConfig.json";

// Load the json configs with dynamic type.
dynamic configs = ConfigManager.GetConfig( configPath );

// Load the json configs with strong C# type. You can define the strong type class if you want.
var cfg = ConfigManager.GetConfig<TheStrongConfigType>( configPath );
```


### DataAccess & ORM example.

Just like using **Dapper** and **plain old ADO.NET** style. :)
* Support Microsoft SQL Server, MySQL and PostgreSQL database with same Dao class.
* Support SQL profile log (input SQL statement & output results).
* Support micro ORM and dynamic ORM.
* Support DataContext CRUD template.
* Support async/await non-blocking methods.
* Support database schema migration CLI. (The **zdm** stands for **Z**ayni framework **D**atabase **M**igration. :) )
* **NO MORE Entity Framework!!!** (We don't like Entity Framework......... )


Add config in your app.config or web.config file.

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <configSections>
        <section name="ZayniFramework" type="ZayniFramework.Common.ZayniConfigSection, ZayniFramework.Common"/>
    </configSections>
    <ZayniFramework>
        <LoggingSettings eventLogEnable="false" logEnable="true">
            <DefaultTextLogger>
                <add name="_DefaultLog" enable="true" maxSize="8">
                    <!-- ZayniFramework's default internal logger. -->
                    <LogFile path="D:\Logs\Zayni\Tracing.log"/>
                    <Filter filterType="deny" category="Information,Warning"/>
                </add>
            </DefaultTextLogger>
        </LoggingSettings>
        <DataAccessSettings>
            <DatabaseProviders>
                <add name="Zayni" dataBaseProvider="MSSQL" sqlLogEnable="false"/>
                <add name="ZayniUnitTest" dataBaseProvider="MSSQL" sqlLogEnable="true"/>
            </DatabaseProviders>
        </DataAccessSettings>
    </ZayniFramework>
    <connectionStrings>
        <!--MSSQL-->
        <add name="Zayni" connectionString="Server=XXX;Database=XXX;User Id=XXX;Password=XXX;Max Pool Size=20;Min Pool Size=5;Connection Lifetime=30;"
             providerName="System.Data.SqlClient" />
        <add name="ZayniUnitTest" connectionString="Server=XXX;Database=XXX;User Id=XXX;Password=XXX;Max Pool Size=20;Min Pool Size=5;Connection Lifetime=30;"
             providerName="System.Data.SqlClient" />

        <!--MySQL-->
        <!--<add name="Zayni" connectionString="SERVER=XXX;DATABASE=XXX;UID=XXX;PASSWORD=XXX;Allow User Variables=True;Charset=utf8;Convert Zero Datetime=True;"
             providerName="MySql.Data.MySqlClient"/>
        <add name="ZayniUnitTest" connectionString="SERVER=XXX;DATABASE=XXX;UID=XXX;PASSWORD=XXX;Allow User Variables=True;Charset=utf8;Convert Zero Datetime=True;"
             providerName="MySql.Data.MySqlClient"/>-->
    </connectionStrings>
</configuration>

```

Add using.

```csharp
using ZayniFramework.Common;
using ZayniFramework.DataAccess;
```

Write your Dao (Data Access Object) class and extends the **BaseDataAccess** class.

```csharp
/// <summary>Your Data Access Object (Dao) class
/// </summary>
public class UserDao : BaseDataAccess
```

Select data from database using **BaseDataAccess.LoadDataToModel()** method.

**If your column name and table name in your database naming rule is not the lower snake case style. You need to use <font color=red>`</font> character to encapsulated the column name or table name in you SQL statement no matter your Dao application is access to MSSQL, MySQL or PostgreSQL database.**


```csharp
public async Task<IResult<List<UserModel>>> Select_LoadDataToModelAsync( UserModel query )
{
    var result = Result.Create<List<UserModel>>();

    string sql = @"
        SELECT 
          `ACCOUNT_ID`
        , `NAME`
        , `AGE`
        , `SEX`
        , `BIRTHDAY`

        , `IS_VIP`
        , `IS_GOOD`
        , `DATA_FLAG`
         FROM `FS_UNIT_TEST_USER`
        WHERE `SEX` = @Sex; ";

    using ( var conn = await base.CreateConnectionAsync() )
    {
        var cmd = base.GetSqlStringCommand( sql, conn );
        base.AddInParameter( cmd, "@Sex", DbTypeCode.Int32, query.Sex );

        var r = await base.LoadDataToModelAsync<UserModel>( cmd );

        if ( !r.Success )
        {
            await conn.CloseAsync();
            result.Message = base.Message;
            return result;
        }

        await conn.CloseAsync();

        var models = r.Data;

        result.Data    = models;
        result.Success = true;
        return result;
    }
}
```
Multi-select SQL statements from database by using **BaseDataAccess.LoadDataToModels()** method.

```csharp
public async Task<IResult> Select_LoadDataToModelsAsync( UserModel query )
{
    dynamic result = Result.CreateDynamicResult();  

    string sql = @" 
        -- QueryNo1
        SELECT `ACCOUNT_ID` AS Account
                , `NAME`
                , `AGE`
                , `SEX`
                , `BIRTHDAY` AS DoB

                , `IS_VIP`
        FROM `FS_UNIT_TEST_USER`
            WHERE `SEX` = @Sex;

        -- QueryNo2
        SELECT `ACCOUNT_ID` AS Account
                , `NAME`
                , `AGE`
                , `SEX`
                , `BIRTHDAY` AS DoB
        FROM `FS_UNIT_TEST_USER`
            WHERE `SEX` = @Sex; ";

    using ( var conn = await base.CreateConnectionAsync() ) 
    {
        var cmd = base.GetSqlStringCommand( sql, conn );
        base.AddInParameter( cmd, "@Sex", DbTypeCode.Int32, query.Sex );
        
        Type[] types = { typeof ( UserModel ), typeof ( UserModel ) };

        var r = await base.LoadDataToModelsAsync( cmd, types );

        if ( !r.Success )
        {
            await conn.CloseAsync();
            return result;
        }

        await conn.CloseAsync();

        var data = r.Data;

        List<UserModel> queryResult1 = data.FirstOrDefault().ToList<UserModel>();
        List<UserModel> queryResult2 = data.LastOrDefault().ToList<UserModel>();

        result.Rst1    = queryResult1;
        result.Rst2    = queryResult2;
        result.Success = true;
        return result;
    }
}
```

Dynamic ORM query using **BaseDataAccess.LoadDataToDynamicModel()** method. You don't need to define a entity class anymore.

```csharp
public async Task<IResult<List<dynamic>>> Select_LoadDataToDynamicModelAsync( UserModel query )
{
    var result = Result.Create<List<dynamic>>();

    string sql = @"
        SELECT 
          `ACCOUNT_ID` AS `Account`
        , `NAME`
        , `AGE`
        , `SEX`
        , `BIRTHDAY` AS `DoB`

        , `IS_VIP`
         FROM `FS_UNIT_TEST_USER`
        WHERE `SEX` = @Sex ";

    using ( var conn = await base.CreateConnectionAsync() ) 
    {
        var cmd = base.GetSqlStringCommand( sql, conn );
        base.AddInParameter( cmd, "@Sex", DbTypeCode.Int32, query.Sex );

        var r = await base.LoadDataToDynamicModelAsync( cmd );

        if ( !r.Success )
        {
            await conn.CloseAsync();
            result.Message = base.Message;
            return result;
        }

        await conn.CloseAsync();

        var models = r.Data;

        result.Data    = models;
        result.Success = true;
        return result;
    }
}
```

For more **BaseDataAccess** examples see [here](https://gitlab.com/ponylin1985/ZayniFramework/blob/master/Test/UnitTests/Zayni.DataAccess.Test/TestDao/UserDao.cs).

### Using DataContext template to execute CRUD methods.

Add using.

```csharp
using ZayniFramework.Common;
using ZayniFramework.DataAccess.Lightweight;
```

Write your own DataContext class and extends the **DataContext** class.

```csharp
/// <summary>Your DataContext class.
/// </summary>
internal class UserDataContext : DataContext<UserReqArgs, UserModel>
```

Then you can do some CRUD action like...<br/>

DataContext.**Insert()**;<br/>
DataContext.**Update()**;<br/>
DataContext.**Delete()**;<br/>
DataContext.**Select()**;<br/>

or

await DataContext.**InsertAsync()**;<br/>
await DataContext.**UpdateAsync()**;<br/>
await DataContext.**DeleteAsync()**;<br/>
await DataContext.**SelectAsync()**;<br/>

```csharp
var model = new UserModel()
{
    Account = "Amber",
    Name    = "Amber Jane",
    Age     = 22,
    DOB     = new DateTime( 1993, 2, 17 ),
    Sex     = 0,
    IsVip   = true
};

var reqArgs = new UserReqArgs()
{
    Account = "Amber"
};

var wheres = new SqlParameter[] 
{ 
    new SqlParameter() { ColumnName = "AccountId", Name = "Account",  DbType = DbTypeCode.String, Value = "Amber001" },
    new SqlParameter() { ColumnName = "Name",      Name = "UserName", DbType = DbTypeCode.String, Value = "Amber" }
};

var r = await dataContext.SelectAsync( new SelectInfo<UserReqArgs>() { DataContent = reqArgs } );
var c = await dataContext.InsertAsync( model );
var u = await dataContext.UpdateAsync( model, wheres: wheres, ignoreColumns: new string[] { "Birthday", "IsVIP" } );
var d = await dataContext.DeleteAsync( model );
```

More **DataContext** examples. You can go to [here](https://gitlab.com/ponylin1985/ZayniFramework/blob/master/Test/UnitTests/Zayni.DataAccess.Test/TestDataContext/UserDataContext.cs) and [here](https://gitlab.com/ponylin1985/ZayniFramework/blob/master/Test/UnitTests/Zayni.DataAccess.Test/DataContextTester.cs).

### Middleware Middle.Service module example.

The **Middle.ServiceHost** is a self-host application middleware service module. Any kind of .NET application can add package reference of the **ZayniFramework.Middle.Service** nupkg, then your application will be able to serve any clinet-side application by using this middleware serivce.

First, add a **serviceHostConfig.json** config file in your app project.<br/>
You can use **TCPSocket**, **WebSocket** or **RabbitMQ** protocol. Each of them support **RPC (Request & Response)**, **Message Publish** and **Service Event Listening**.<br/>

If your application use the _Middle.Service.Grpc module_ you can also config the **gRPC** protocol too.
If your application use the _Middle.Service.HttpCore module_ you can also config the **ASP.NET Core Http** protocol too.

Something like this...<br/>

```json
{
    "enableConsoleTracingLog": true,
    "serviceHosts": [
        {
            "serviceName": "WebAPI.Test.TCPSocket",
            "remoteHostType": "TCPSocket",
            "hostName": "Local-Dev",
            "enableActionLogToDB": true,
            "enableActionLogToFile": true,
            "esLoggerName": "ESServiceHostActionAppender"
        },
        {
            "serviceName": "WebAPI.Test.WebSocket",
            "remoteHostType": "WebSocket",
            "hostName": "Local-Dev",
            "enableActionLogToDB": true,
            "enableActionLogToFile": true,
            "esLoggerName": "ESServiceHostActionAppender",
            "autoStart": true
        },
        {
            "serviceName": "WebAPI.Test.gRPC",
            "remoteHostType": "gRPC",
            "hostName": "Local-Dev",
            "enableActionLogToDB": true,
            "enableActionLogToFile": true,
            "esLoggerName": "ESServiceHostActionAppender",
            "autoStart": true
        },
        {
            "serviceName": "WebAPI.Test.RabbitMQ",
            "remoteHostType": "RabbitMQ",
            "hostName": "Local-Dev",
            "enableActionLogToDB": true,
            "enableActionLogToFile": true,
            "esLoggerName": "ESServiceHostActionAppender",
            "autoStart": false
        },
        {
            "serviceName": "WebAPI.Test.HttpCore",
            "remoteHostType": "HttpCore",
            "hostName": "Local-Dev",
            "enableActionLogToDB": true,
            "enableActionLogToFile": true,
            "esLoggerName": "ESServiceHostActionAppender",
            "autoStart": true
        }
    ],
    "gRPCHostSettings": {
        "defaultHost": "Local-Dev",
        "hosts": [
            {
                "hostName": "Local-Dev",
                "gRPCHostServer": "0.0.0.0",
                "port": 1110
            }
        ]
    },
    "tcpSocketHostSettings": {
        "defaultHost": "Local-Dev",
        "hosts": [
            {
                "hostName": "Local-Dev",
                "tcpMsgEndsWith": "u0003",
                "tcpPort": 5590,
                "whitelist": [],
                "tcpBufferSizeKB": 5
            }
        ]
    },
    "webSocketHostSettings": {
        "defaultHost": "Local-Dev",
        "hosts": [
            {
                "hostName": "Local-Dev",
                "hostBaseUrl": "ws://0.0.0.0:5580",
                "whitelist": []
            }
        ]
    },
    "rabbitMQSettings": {
        "defaultHost": "Local-Dev",
        "hosts": [
            {
                "hostName": "Local-Dev",
                "mbHost": "localhost",
                "mbPort": 5672,
                "mbUserId": "pony",
                "mbPassword": "Admin123",
                "mbVHost": "ZayniTest",

                // 監聽 Queue 的訊息: 不支援 auto-scaling consumer。
                // "msgRoutingKey": "Event.Client.",
                // "msgQueue": "Middle.Server.ListenClient.Msg",
                
                // 監聽 Exchange 的訊息: 支援 auto-scaling consumer。
                "listenExchange": "Middle.Test.Topic",
                "listenQueuePrefix": "Middle.Server.ListenClient.",
                "listenRoutingKey": "Event.Client.",

                "rpcQueue": "Middle.Server.RPC",
                "rpcRoutingKey": "RPC.Server.",
                "publishDefaultExchange": "Middle.Test.Topic",
                "publishExchange": "Middle.Test.Topic",
                "publishRoutingKey": "Event.Server."
            }
        ]
    },
    "httpCoreSettings": {
        "hosts": [
            {
                "hostName": "Local-Dev",
                "hostBaseUrl": "http://0.0.0.0:5100",
                "keepAliveTimeout": "00:10:00"
            }
        ]
    }
}
```

Then, write your DTO (Data Transfer Object) class for communication with outside applications.<br/>
You can also use **ZayniFramework.Validation**'s validation attribute on your DTO's properties.

```csharp
using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;
using ZayniFramework.Validation;


namespace ServiceHost.Test.Entity
{
    /// <summary>Some DTO class
    /// </summary>
    [Serializable()]
    public class SomethingDTO
    {
        /// <summary>測試訊息
        /// </summary>
        [NotNullOrEmpty( Message = "'someMsg' is invalid." )]
        [JsonProperty( PropertyName = "someMsg" )]
        [JsonPropertyName( "someMsg" )]
        public string SomeMessage { get; set; }

        /// <summary>測試日期
        /// </summary>
        [JsonProperty( PropertyName = "someDate" )]
        [JsonPropertyName( "someDate" )]
        public DateTime SomeDate { get; set; }

        /// <summary>測試浮點數
        /// </summary>
        [JsonProperty( PropertyName = "magicNumber" )]
        [JsonPropertyName( "magicNumber" )]
        public double SomeMagicNumber { get; set; }

        /// <summary>測試整數
        /// </summary>
        [JsonProperty( PropertyName = "luckyNumber" )]
        [JsonPropertyName( "luckyNumber" )]
        [Int64Range( Min = "1", IncludeMin = true, Message = "The 'luckyNumber' must be a positive Int64 integer number." )]
        public long LuckyNumber { get; set; }

        /// <summary>測試整布林值
        /// </summary>
        [JsonProperty( PropertyName = "isSuperXMan" )]
        [JsonPropertyName( "isSuperXMan" )]
        public bool IsSuperMan { get; set; }
    }
}

```

Then, you can write your **ServiceAction** in your own class library project.<br/>
Add namespace using.

```csharp
using ZayniFramework.Common;
using ZayniFramework.Logging;
using ZayniFramework.Middle.Service;
using ZayniFramework.Serialization;
```

Write your ServieAction class and extends **ServiceAction** or **ServiceActionAsync** abstract class.

```csharp
/// <summary>Some Servie Action (Non-async action)
/// </summary>
public class MagicTestAction : ServiceAction<SomethingDTO, Result<MagicDTO>>
```

or

```csharp
/// <summary>Some Servie Action (Async, Non-blocking action)
/// </summary>
public class MagicTestActionAsync : ServiceActionAsync<SomethingDTO, Result<MagicDTO>>
```

Write your constructor and pass the **action name** to the base class.

```csharp
/// <summary>Constructor.
/// </summary>
public MagicTestAction() : base( "MagicTestAction" )
{
    // pass
}
```

or

```csharp
/// <summary>Constructor.
/// </summary>
public MagicTestActionAsync() : base( "MagicTestActionAsync" )
{
    // pass
}
```

Override the **Execute** or **ExecuteAsync** method. You can implement your business logic here.<br/> 
See, your application don't know what kind of protocol or communication framework is using now. The only you need to focus is your business logic!

```csharp
/// <summary>Execute Action. Your business logic here...
/// </summary>
public override void Execute()
{
    Response = Result.Create<MagicDTO()>();

    Logger.WriteInformationLog( this, $"Receive SomethingDTO is {Environment.NewLine}{JsonConvertUtil.SerializeInCamelCase( Request )}", nameof ( Execute ) );

    SomethingDTO reqDTO = Request;

    var resDTO = new MagicDTO()
    {
        MagicWords = $"You should not wake me up... {Guid.NewGuid().ToString()}",
        MagicTime  = DateTime.Now
    };

    if ( 7.77 == reqDTO.SomeMagicNumber )
    {
        resDTO.TestDouble = 999.999;
    }

    Response.Data    = resDTO;
    Response.Success = true;
}
```

or

```csharp
/// <summary>取得使用者資料模型的測試動作
/// </summary>
public class GetUserModelsActionAsync : ServiceActionAsync<UserDTO, Result<List<UserDTO>>>
{
    /// <summary>資料存取物件
    /// </summary>
    private static readonly UserDao _dao = new UserDao();

    /// <summary>預設建構子
    /// </summary>
    public GetUserModelsActionAsync() : base( actionName: "GetUserModelsActionAsync" )
    {
    }

    /// <summary>執行動作
    /// </summary>
    public override async Task ExecuteAsync() => Response = await ExecuteAsync( Request );

    /// <summary>執行動作
    /// </summary>
    /// <param name="request">查詢參數</param>
    /// <returns>查詢結果</returns>
    public async Task<Result<List<UserDTO>>> ExecuteAsync( UserDTO request )
    {
        var result = Result.Create<List<UserDTO>>();

        if ( request.Sex > 1 || request.Sex < 0 )
        {
            result.Message = $"Invalie request. Sex is invalid.";
            result.Code    = StatusCode.INVALID_REQUEST;
            return result;
        }

        var args = new UserModel() 
        {
            Sex = request.Sex
        };

        var r = await _dao.Select_LoadDataToModelAsync( args );

        if ( !r.Success )
        {
            result.Message = $"Get user data from database fail. {r.Message}";
            result.Code    = StatusCode.INTERNAL_ERROR;
            return result;
        }

        if ( r.Data.IsNullOrEmptyList() )
        {
            result.Message = $"No data found.";
            result.Code    = StatusConstant.NO_DATA_FOUND;
            result.Success = true;
            return result;
        }

        List<UserModel> models = r.Data;

        result.Data    = models.Select( m => { m.DOB = m.DOB?.ToUtcKind(); return m.ConvertTo<UserDTO>(); } ).ToList();
        result.Code    = StatusCode.ACTION_SUCCESS;
        result.Success = true;
        return result;
    }
}
```


If you have your **own customized implementaion of IRemoteHost** as an extension protocol module. You can register you own implemented RemoteHost into the **Middle.Service's internal container**. Like this,

```csharp
// Register a 'gRPCRemoteHost' IRemoteHost type into Middle.Service. The name is 'gRPC'.
// You have to register your own IRemoteHost type before MiddlewareService.StartService().
MiddlewareService.RegisterRemoteHostType<gRPCRemoteHost>( "gRPC" );
```

Then, you need to inject your **ServiceAction** to the **ServiceActionContainer**.<br/>
There are two methods to register you ServiceAction into the internal ServiceActionContainer.


You can register your ServiceAction with the **explicit 'serviceName' in serviceHostConfig.json** file. Like this:


```csharp
// Register your servcie action by 'Add' method. You need to add with the 'ServiceName' in json config file explicitly.
MiddlewareService.InitializeServiceActionHandler = () => 
{
    var magicTestAction = new MagicTestAction();

    // Register the MagicTestAction to PaymentService and OrderService service host manually and explicitly.
    ServiceActionContainerManager.Add( "PaymentService", magicTestAction.Name, magicTestAction.GetType() );
    ServiceActionContainerManager.Add( "OrderService", magicTestAction.Name, magicTestAction.GetType() );
    return Result.Create( true );
};
```

You can also register your ServiceAction without the serviceName, the Zayni Framework will register the ServiceActions for **all the service host which the 'autoStart' setting value is true** in serviceHostConfig.json file. Like this:


```csharp
// Register your service action by 'RegisterServiceAction' method. You don't need to pass the 'ServiceName' argument anymore.
MiddlewareService.InitializeServiceActionHandler = () => 
{
    var magicTestAction = new MagicTestAction();

    // Register the MagicTestAction to all autoStarted service host in serviceHostConfig.json file.
    // Arguments are: ActionName, Type of ServiceAction and the serviceHostConfig.json's file path.
    ServiceActionContainerManager.RegisterServiceAction( magicTestAction.Name, magicTestAction.GetType(), configPath: "Your serviceHostConfig.json  path." );

    var getUserModelsAction = new GetUserModelsAction();
    ServiceActionContainerManager.RegisterServiceAction( getUserModelsAction.Name, getUserModelsAction.GetType(), configPath: "Your serviceHostConfig.json  path." );

    return Result.Create( true );
};
```


If you are using Middle.Service module **v2.2.5 higher version**. You event don't need to register your ServiceAction by yourself anymore. **The Zayni Framework will register all your ServiceAction automatically.**


BUT all your ServiceAction must have a ***default public non-parameter constructor.*** 
And your assemblies must be ***placed in the same path with the ZayniFramework.Middle.Service.dll*** in runtime.

```csharp
// If you want to let the Zayni Framework register all your ServiceAction objects. You don't need to assign the InitializeServiceActionHandler delegate anymore. Like this:
MiddlewareService.InitializeServiceActionHandler = null;
```

Finally, you just need to start the Middle.ServiceHost service in your application.

```csharp
var configPath = "./Configs/serviceHostConfig.json";

// Start the service host with the explicitly 'serviceName'.
var r = new MiddlewareService().StartService( "PaymentService", path: configPath );
var j = new MiddlewareService().StartService( "OrderService", path: configPath );

// Start all the autoStarted service hosts.
var g = new MiddlewareService().StartServiceHosts( configPath );
var q = await new MiddlewareService().StartServiceHostsAsync( configPath );
```
More sample, you can go to [/Test/WebAPI.Test](https://gitlab.com/ponylin1985/ZayniFramework/tree/master/Test/WebAPI.Test) directory to see the complete sample code.


### Middleware Middle.Service.Client module example.

You have define your own business logic service by using **Middle.Service** module. Now you can use **Middle.Service.Client** module to send a RPC request, trigger a service event or publish a message to your service action.


First, add a **serviceClientConfig.json** in your client-side app project.

```json
{
    "serviceClients": [
        {
            "serviceClientName": "ServiceHost.Test.Client",
            "remoteServiceName": "WebAPI.Test.gRPC",
            "remoteHostType": "gRPC",
            "remoteClient": "Local-Dev",
            "enableActionLogToDB": true,
            "esLoggerName": "ESServiceClientActionAppender"
        },
        {
            "serviceClientName": "Remote.ShareData",
            "remoteServiceName": "RemoteCache-UnitTest-gRPC",
            "remoteHostType": "gRPC",
            "remoteClient": "Local-Dev3",
            "enableActionLogToDB": true,
            "esLoggerName": "ESServiceClientActionAppender"
        }
    ],
    "gRPCProtocol": {
        "remoteClients": [
            {
                "name": "Local-Dev",
                "serverHost": "localhost",
                "serverPort": 1110
            },
            {
                "name": "Local-Dev2",
                "serverHost": "localhost",
                "serverPort": 1111
            },
            {
                "name": "Local-Dev3",
                "serverHost": "localhost",
                "serverPort": 5555
            }
        ]
    },
    "rabbitMQProtocol": {
        "defaultRemoteClient": "Local-Dev",
        "remoteClients": [
            {
                "name": "Local-Dev",
                "mbHost": "localhost",
                "mbPort": 5672,
                "mbUserId": "pony",
                "mbPassword": "Admin123",
                "mbVHost": "ZayniTest",
                "defaultExchange": "Middle.Test.Topic",
                "rpc": {
                    "exchange": "Middle.Test.Topic",
                    "routingKey": "RPC.Server.",
                    "rpcReplyQueue": "Middle.Server.RPC.Res",   // 經過測試，rpcReplyQueue 也可以填寫空字串。
                    "rcpResponseTimeout": 5
                },
                "publish": {
                    "exchange": "Middle.Test.Topic",
                    "routingKey": "Event.Client."
                },
                "listen": {
                    // 指定監聽某一個 Exchange 的事件，並且給定 runtime 動態產生 consumer 的 Queue 名稱前綴字，支援 auto-scaling。
                    "exchangeName": "Middle.Test.Topic",
                    "queueNamePrefix": "Middle.Client.ListenServer.",
                    "exchangeRoutingKey": "Event.Server."

                    // 明確指定某一個 Queue 的監聽，不支援 auto-scaling 機制。
                    // "msgQueue": "Middle.Client.ListenServer.Msg",
                    // "msgRoutingKey": "Event.Server."
                }
            }
        ]
    },
    "tcpSocketProtocol": {
        "defaultRemoteClient": "Local-Dev",
        "remoteClients": [
            {
                "name": "Local-Dev",
                "tcpMsgEndsWith": "u0003",
                "tcpServerHost": "localhost",
                "tcpServerPort": 5590,
                "tcpSendBufferSize": 5,
                "tcpReceiveBufferSize": 5,
                "connectionTimeout": 1,
                "tcpResponseTimeout": 1
            },
            {
                "name": "Local-Dev2",
                "tcpMsgEndsWith": "u0003",
                "tcpServerHost": "localhost",
                "tcpServerPort": 5591,
                "tcpSendBufferSize": 5,
                "tcpReceiveBufferSize": 5,
                "connectionTimeout": 2,
                "tcpResponseTimeout": 1
            },
            {
                "name": "Local-Dev3",
                "tcpMsgEndsWith": "u0003",
                "tcpServerHost": "localhost",
                "tcpServerPort": 5592,
                "tcpSendBufferSize": 5,
                "tcpReceiveBufferSize": 5,
                "connectionTimeout": 2,
                "tcpResponseTimeout": 1
            }
        ]
    },
    "webSocketProtocol": {
        "defaultRemoteClient": "Local-Dev",
        "remoteClients": [
            {
                "name": "Local-Dev",
                "webSocketHostBaseUrl": "ws://localhost:5580",
                "wsResponseTimeout": 5
            },
            {
                "name": "Local-Dev2",
                "webSocketHostBaseUrl": "ws://localhost:5581",
                "wsResponseTimeout": 5
            },
            {
                "name": "Local-Dev3",
                "webSocketHostBaseUrl": "ws://localhost:5582",
                "wsResponseTimeout": 5
            }
        ]
    },
    "httpCoreProtocol": {
        "remoteClients": [
            {
                // httpHostUrl 設定結尾 URL 必須要包含 / 字元!
                // httpHostUrl 假若為 /action/ 結尾，代表呼叫一般 Non-async 的 ServiceAction。
                // httpHostUrl 假若為 /action/async/ 結尾，代表呼叫非同步作業 Async 的 ServiceActionAsync。
                "name": "Local-Dev",
                "httpHostUrl": "http://localhost:5100/action/async/",
                "httpResponseTimeout": 5
            },
            {
                "name": "Local-Dev2",
                "httpHostUrl": "http://localhost:5110/action/",
                "httpResponseTimeout": 5
            }
        ]
    }
}
```

Write your ClientProxy class and extends **RemoteProxy** class.<br/>
Add namespace using.

```csharp
using ZayniFramework.Common;
using ZayniFramework.Middle.Service.Client;

/// <summary>Some client proxy.
/// </summary>
public class PaymentProxy : RemoteProxy
```

Write your constructor and pass the **service client name** to base class.

```csharp
/// <summary>預設建構子
/// </summary>
public PaymentProxy() : base( "PaymentServiceProxy" )
{
    // pass
}

/// <summary>多載建構子
/// </summary>
/// <param name="path">服務客戶端 serviceClientConfig.json 設定檔的路徑</param>
public PaymentProxy( string path = "./serviceClientConfig.json" ) : base( serviceClientName: "PaymentServiceProxy", configPath: "./serviceClientConfig.json" )
{
    // pass
}
```

Then, you can use the **RemoteProxy** base methods, you can send a RPC request or publish message to your **Middle.Service** service action.

```csharp
/// <summary>執行遠端服務動作的測試
/// </summary>
public new Result<TResDTO> Execute<TReqDTO, TResDTO>( string actionName, TReqDTO request ) => base.Execute<TReqDTO, TResDTO>( actionName, request );

/// <summary>非同步作業執行遠端服務動作的測試
/// </summary>
/// <typeparam name="TReqDTO">請求資料載體的泛型</typeparam>
/// <typeparam name="TResDTO">回應資料載體的泛型</typeparam>
/// <param name="actionName">動作名稱</param>
/// <param name="request">請求資料載體</param>
/// <returns>執行結果</returns>
public new async Task<Result<TResDTO>> ExecuteAsync<TReqDTO, TResDTO>( string actionName, TReqDTO request ) => await base.ExecuteAsync<TReqDTO, TResDTO>( actionName, request );

/// <summary>發佈 ServiceEvent 服務事件訊息的測試
/// </summary>
public new Result Publish<TMsgDTO>( string actionName, TMsgDTO dto ) => base.Publish<TMsgDTO>( actionName, dto );
```

You might need to register the extension IRemoteClient type into **RemoteClientFactory** before you create your RemoteProxy.<br/>
If you don't have other extension IRemoteClient, then you won't need to do this.

```csharp
// Register the 'gRPCRemoteClient' into the RemoteClientFactory. The name is 'gRPC'.
RemoteClientFactory.RegisterRemoteClientType<gRPCRemoteClient>( "gRPC" );
```

Finally, you can use your own **ClientProxy** object to send RPC request or publish message.

```csharp
var paymentProxy = new PaymentProxy( "./serviceClientConfig.json" );

var reqDTO = new SomethingDTO() 
{
    SomeMessage     = "This is a message from masOS or linux.",
    SomeDate        = DateTime.Now,
    SomeMagicNumber = 24.52D,
    LuckyNumber     = 333,
    IsSuperMan      = true
};

// Send a RPC request to the ServiceHost server-side.
var r1 = paymentProxy.Execute<SomethingDTO, MagicDTO>( "MagicTestAction", reqDTO );

// Send a async non-blocking RPC request to the ServiceHost server-side.
var r2 = await paymentProxy.ExecuteAsync<SomethingDTO, MagicDTO>( "MagicTestActionAsync", reqDTO );
```

More detail sample code. You can go to [/Test/ServieHostTest/ServiceHost.Client.ConsoleApp](https://gitlab.com/ponylin1985/ZayniFramework/tree/master/Test/ServiceHostTest/ServiceHost.Client.ConsoleApp) directory to see the complete sample code.

### Extension modules for Middle.Service & Middle.Service.Client.

The Zayni Framework **allow** you to write your own customized **RemoteHost** and **RemoteClient** implementaion as a **extension module** of Middle.Service and Middle.Service.Client.


* Create an .NET Standard C# project for the extension modules.
* Write your RemoteHost or RemoteClient class.
    * Your RemoteHost class need to extends the **IRemoteHost** interface. 
    * Your RemoteClient class need to extends the **IRemoteClient** interface.

* Implement you the **IRemoteHost** or **IRemoteClient** interface in your class.
* You can follow the json configuration pattern like Middle.Service and Middle.Service.Client to create your json config section.
    * Design the json config section.
    * Write your C# configuration entity class. 

* Register you **IRemoteHost** or **IRemoteClient** type into Zayni Framework's internal container.
* Then, use it.


More detail sample code. You can to to [Source/ServiceHost/Middle.Service.Grpc](https://gitlab.com/ponylin1985/ZayniFramework/tree/master/Source/ServiceHost/Middle.Service.Grpc) and [Source/ServiceHost/Middle.Service.Grpc.Client](https://gitlab.com/ponylin1985/ZayniFramework/tree/master/Source/ServiceHost/Middle.Service.Grpc.Client) directory to see how to write a extension module for the Middleware Service module.

<font color=red>The gRPC is **not** a built-in support protocol in Middle.Service and Middle.Service.Client. The **Middle.Service.Grpc** and **Middle.Service.Grpc.Client** are extension modules.</font>



### Middle.TelnetService example.

The **Middle.TelnetService** and **Common module** provide the API of **ICommand, ConsoleCommand and RemoteCommand** for your application.
You can use it to build a telnet service to receive the remote telnet command from any telent client very easily!


Add namespace using.

```csharp
using ZayniFramework.Common;
using ZayniFramework.Middle.TelnetService;
```

Write your **ConsoleCommand** class and extends the **ConsoleCommand** base class.

```csharp
public sealed class YourConsoleCommand : ConsoleCommand
{
    /// <summary>Execute the command.
    /// </summary>
    public override void ExecuteCommand() 
    {
        // Implement your code here...
        Result.Success = true;
    } 
}
```

Write your telnet **RemoteCommand** class and extends the **RemoteCommand** base class.


```csharp
public class YourRemoteCommand : RemoteCommand
{
    /// <summary>Execute the command.
    /// </summary>
    public override void ExecuteCommand()
    {
        // Implement your code here...
        Result.Success = r.Success;
    }
}
```

Then, you have to register your **ICommand** object to the **CommandContainer**. Like this,

```csharp
// Plain-old fashion way...
private static CommandContainer RegisterCommands()
{
    var commandContainer = new CommandContainer();
    commandContainer.RegisterCommand<YourConsoleCommand>( "the command name 1", commandText => 0 == string.Compare( commandText, "some command line", true ) );
    commandContainer.RegisterCommand<YourRemoteCommand>( "the command name 12", commandText => 0 == string.Compare( commandText, "some command line", true ) );
    commandContainer.RegisterCommand<ClearConsoleCommand>( "cls", commandText => 0 == string.Compare( commandText, "cls", true ) );
    commandContainer.RegisterUnknowCommand<UnknowRemoteCommand>();
    return commandContainer;
}

// Plain-old chain method invoke may
private static CommandContainer RegisterCommands() => 
    CommandContainer
        .Build()
        .Register<PushCommand>( "push", commandText => 0 == string.Compare( commandText, "push", true ) )
        .Register<ExitConsoleCommand>( "exit", commandText => 0 == string.Compare( commandText, "exit", true ) )
        .Register<ClearConsoleCommand>( "cls", commandText => 0 == string.Compare( commandText, "cls", true ) )
        .RegisterUnknow<UnknowRemoteCommand>();
```

Finally, you can start the console command and telnet remote command service to receive the commands. Like this.

```csharp
var commandContainer = RegisterCommands();
TelnetCommandService.StartDefaultTelnetService( commandContainer );
ConsoleCommandService.StartDefaultConsoleService( commandContainer );
```

More detail and sample. You can go to the [/Test/ServiceHostTest/ServiceHost.Client.ConsoleApp](https://gitlab.com/ponylin1985/ZayniFramework/tree/master/Test/ServiceHostTest/ServiceHost.Client.ConsoleApp) directory.



### CacheManager example.

Add namespace using.

```csharp
using ZayniFramework.Caching;
using ZayniFramework.Common;
```

Add **CachingSettings** config section in your app.config or web.config file.

```xml
<!--managerMode support: Memory, Redis, MySQLMemory, RemoteCache-->
<CachingSettings managerMode="Redis">
    <CachePool resetEnable="true" cleanerIntervalSecond="3" maxCapacity="10000" refreshIntervalMinute="1" />
    <CacheData timeoutInterval="0" />
    <RedisCache>
        <add name="XXXRedisService" isDefault="true" dbActionLog="true" host="XXX.XXX.XXX.XXX" port="6379" account="" password="" dbNumber="15" connectTimeout="1000" responseTimeout="3500" />
        <!--<add name="BBBRedisService" isDefault="true" dbActionLog="true" host="XXX.XXX.XXX.XXX" port="6379" account="" password="" dbNumber="15" connectTimeout="1000" responseTimeout="3500" />-->
    </RedisCache>
    <RemoteCache defaultCacheClientName="RemoteCacheTest1"></RemoteCache>
    <!--<MySQLMemoryCache defaultConnectionName="ZayniCache"></MySQLMemoryCache>-->
</CachingSettings>
```

Put data into **CacheManager**. Use **CacheManager.Put()** static method.

```csharp
public async Task PutDataTestAsync()
{
    string key = "test01";

    var model = new
    {
        Name   = "Amber",
        Age    = 23,
        Sex    = "female",
        Emails = new List<string>()
        {
            "amber@gmail.com"
        },
        Profile = new
        {
            Token   = Guid.NewGuid().ToString(),
            DoB     = new DateTime( 1998, 2, 14 ),
            Comment = "AAAA"
        }
    };

    var cacheProp = new CacheProperty()
    {
        CacheId         = key,
        Data            = model,
        RefreshCallback = () => new
        {
            Name   = "Belle Claire",
            Age    = 23,
            Sex    = "females",
            Emails = new List<string>()
            {
                "amber@gmail.com"
            },
            Profile = new
            {
                Token   = Guid.NewGuid().ToString(),
                DoB     = new DateTime( 1998, 2, 14 ),
                Comment = "Good afternoon."
            }
        }
    };

    bool success = await CacheManager.PutAsync( cacheProp );
s}
```

Put data into CacheManager in a **HashProperty** data structure.<br/>
Get **HashProperty** data from CacheManager.

```csharp
var hashs = new HashProperty[] 
{
    new HashProperty( "a1", "Hello Fiona!" ),
    new HashProperty( "a2", true ),
    new HashProperty( "a3", new DateTime( 2018, 2, 7 ) ),
    new HashProperty( "a4", 45.26 ),
    new HashProperty( "a5", 500 ),
    new HashProperty( "a6", 3333.77M ),
};

bool success = await CacheManager.PutHashPropertyAsync( "aaa", hashs );

var a1 = await CacheManager.GetHashPropertyAsync( "aaa", "a1" );
Assert.AreEqual( "Hello Fiona", a1.Data + "" );

var a2 = await CacheManager.GetHashPropertyAsync( "aaa", "a2" );
Assert.AreEqual( true, Convert.ToBoolean( a2.Data + "" ) );

var a3 = await CacheManager.GetHashPropertyAsync( "aaa", "a3" );
Assert.AreEqual( new DateTime( 2018, 2, 7 ), Convert.ToDateTime( a3.Data ) );

// Get all hash properties with the main cacheId.
var gp = await CacheManager.GetHashPropertiesAsync( "aaa" );
```

**Put** and **Get** data with CacheManager.

```csharp
var model = new UserTestModel()
{
    Name = "Amber",
    Age  = 23,
    Sex  = "F"
};

bool success = await CacheManager.PutAsync( new CacheProperty() { CacheId = "test02", Data = model } );
var r = await CacheManager.GetAsync<UserTestModel>( "test02" );
```

Clear all the data in CacheManager. User `CacheManager.Clear()` or `await CacheManager.ClearAsync()` static method.

```csharp
CacheManager.Clear();
await CacheManager.ClearAsync();
```

More detail sample code. You can go to [/Test/UnitTests/Zayni.Caching.Test](https://gitlab.com/ponylin1985/ZayniFramework/tree/master/Test/UnitTests/Zayni.Caching.Test) directory.




### Caching.RedisClient module example.

You can use the **ZayniFramework.Caching.RedisClient module** to help you to access the Redis Server. It helps you control the connection between and provide all the request & response action log between your application and Redis Server.

Add namespace using. You might need to add the **StackExchange.Redis** namespace too.

```csharp
using StackExchange.Redis;
using ZayniFramework.Caching.RedisClientComponent;
using ZayniFramework.Common;
using ZayniFramework.Serialization;
```

Then, config your Redis Server connection in your app.config file. Like this,

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <configSections>
        <section name="ZayniFramework" type="ZayniFramework.Common.ZayniConfigSection, ZayniFramework.Common" />
    </configSections>
    <ZayniFramework>
        <LoggingSettings eventLogEnable="false" logEnable="true">
            <DefaultTextLogger>
                <add name="_DefaultLog" enable="true" maxSize="8">
                    <LogFile path="D:\Logs\Zayni\Tracing.log" />
                    <Filter filterType="deny" category="Information,Warning" />
                </add>

                <!-- The action log between you application and redis server -->
                <add name="RedisClientTrace:MyLab" enable="true" maxSize="8" dbLoggerName="Zayni" dbLoggerType="MySQL">
                    <LogFile path="D:\Logs\Zayni\RedisClient-Tracing-Taipei-Dev.log" />
                    <!--<Filter filterType="deny" category="Information,Warning" />-->
                </add>
            </DefaultTextLogger>
        </LoggingSettings>
        <RedisClientSettings>
            <RedisClients>
                <add name="RedisServer1" isDefault="true" traceLoggerName="RedisClientTrace:MyLab" host="XXX.XXX.XXX.XXX " port="6379" password="" dbNumber="1" connectTimeout="5000" responseTimeout="1000" />
            </RedisClients>
        </RedisClientSettings>
    </ZayniFramework>
    <connectionStrings>
        <add name="Zayni" connectionString="SERVER=XXX.XXX.XXX.XXX;DATABASE=zayni;UID=XXX;PASSWORD=XXX;Allow User Variables=True;Charset=utf8;Convert Zero Datetime=True"
             providerName="MySql.Data.MySqlClient" />
        <!--<add name="Zayni" connectionString="Server=localhost;Database=ZayniFramework;User Id=XXX;Password=XXX;Max Pool Size=20;Min Pool Size=5;Connection Lifetime=30;"
             providerName="System.Data.SqlClient" />-->
    </connectionStrings>
</configuration>
```

Get a **RedisClient** object from the **RedisClientContainer**. 

```csharp
// Get the 'isDefault=ture'.
RedisClient redisClient = RedisClientContainer.Get();

// Get a RedisClient from the RedisClientContainer. By name.
RedisClient redisClient = RedisClientContainer.Get( "RedisServer1" );
```

Using **StackExchange.Redis** method to access redis server.

* With better performance.
* But no request and response action log between redis server.


```csharp
// You can use the StackExchange.Redis' method like this. BUT you won't have a action log if you choose this way to access redis server. (Better performance!)
// Put string data into redis server. Using StringSet method.
bool isSuccess = redisClient.Db.StringSet( "MyKey", "AAA", when: When.Always );

// Get string data from redis sever. Using StringGet method.
string result = redisClient.Db.StringGet( "MyKey" );
```

Using **RedisClient**'s wrapper method to access redis server.

* You will have the request and response log between redis server if you config the app.config correctly.
* The performance will be slower...

```csharp
// Declare a delegate to wrap the StachExchange.Redis' method like this.
Delegate stringSet = (Func<string, string, When, bool>)( ( key, value, when ) => redisClient.Db.StringSet( key, value, when: when ) );

// Use RedisClient's ExecResult wrapper method to invoke the delegate like this.
// Put string data into redis server.
var r1 = redisClient.ExecResult<bool>( stringSet, "ZayniTest2", "BBB", When.Always );
Assert.IsTrue( r1.Success );
Assert.IsTrue( r1.Data );

Delegate stringGet = (Func<string, string>)( key => redisClient.Db.StringGet( key ) );
var r2 = redisClient.ExecResult<string>( stringGet, "ZayniTest2" );
Assert.IsTrue( r2.Success );
Assert.AreEqual( "BBB", r2.Data );

// =================

var hashName = new HashEntry( "name", "Pony Lin" );
var hashAge  = new HashEntry( "age", 32 );
var hashSex  = new HashEntry( "sex", 1 );
var hashDoB  = new HashEntry( "dob", "1985-05-25" );

var hashSet = (Action<string, HashEntry[]>)( ( key, hashFields ) => redisClient.Db.HashSet( key, hashFields ) );
var r = redisClient.Exec( hashSet, "ZayniTest3", new HashEntry[] { hashName, hashAge, hashSex, hashDoB } );
Assert.IsTrue( r.Success );

var hashGet = (Func<string, string, string>)( ( key, hashField ) => redisClient.Db.HashGet( key, hashField ) );
var name    = redisClient.ExecResult<string>( hashGet, "ZayniTest3", "name" ).Data;
var age     = redisClient.ExecResult<string>( hashGet, "ZayniTest3", "age" ).Data;
var sex     = redisClient.ExecResult<string>( hashGet, "ZayniTest3", "sex" ).Data;
var dob     = redisClient.ExecResult<string>( hashGet, "ZayniTest3", "dob" ).Data;

Assert.AreEqual( "Pony Lin", name );
Assert.AreEqual( "32", age );
Assert.AreEqual( "1", sex );
Assert.AreEqual( "1985-05-25", dob );
```

Use the RedisClient's **PutHashObject** and **GetHashObject** methods.

```csharp
var source = new UserModel()
{
    Name        = "Nancy",
    Age         = 26,
    Birthday    = new DateTime( 1993, 2, 24 ),
    CashBalance = 3000,
    TotalAssets = 4100
};

string key = $"ZayniTest123:{source.Name}";

// Put a data into redis in a hash type.
var p = redisClient.PutHashObject( key, source );
Assert.IsTrue( p.Success );

// Get a data from redis in a hash type.
var g = redisClient.GetHashObject<UserModel>( key );
Assert.IsTrue( g.Success );
Assert.AreEqual( source.Name, g.Data.Name );
Assert.AreEqual( source.Age, g.Data.Age );
Assert.AreEqual( source.Birthday, g.Data.Birthday );
Assert.AreEqual( source.CashBalance, g.Data.CashBalance );
Assert.AreEqual( source.TotalAssets, g.Data.TotalAssets );
```

More detail sample code. You can go to [/Test/UnitTests/Zayni.Caching.RedisClient.Test](https://gitlab.com/ponylin1985/ZayniFramework/tree/master/Test/UnitTests/Zayni.Caching.RedisClient.Test) directory.



### Serialization module example.

Add namespace using.

```csharp
using ZayniFramework.Common;
using ZayniFramework.Serialization;
```

Using **ISerializer** and **SerializerFactory** to serialize and deserialize.

```csharp
var model = new UserModel()
{
    Name     = "Kate",
    Birthday = new DateTime( 1998, 2, 14, 15, 24, 36 ).AddMilliseconds( 257 ),
    Sex      = 0
};

// Using System.Text.Json.JsonSerializer (Best performance!)
var serializer1 = SerializerFactory.Create( "System.Text.Json" );
var s1 = serializer.Serialize( model ) + "";
var d1 = serializer.Deserialize<UserModel>( s1 );

// Using Newtonsoft.Json JSON serilizer.
var serializer2 = SerializerFactory.Create( "Json.NET" );
var s2 = serializer.Serialize( model ) + "";
var d2 = serializer.Deserialize<UserModel>( s2 );

// Using XmlSerializer.
var serializer3 = SerializerFactory.Create( "XML" );
var s3 = serializer3.Serialize( model ) + "";
var d3 = serializer3.Deserialize<UserModel>( s3 );

// Using BinaryFormatSerializer.
var serializer4 = SerializerFactory.Create( "BinaryFormatter" );
var s4 = serializer4.Serialize( model ) + "";
var d4 = serializer4.Deserialize<UserModel>( s4 );
```

You can even define your own **ISerializer** and register it into **SerializerFactory**.


```csharp
/// <summary>You customer Serializer implementation class
/// </summary>
public class YourSerializer : ISerializer
{
    /// <summary>How to deserialize here...
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public TResult Deserialize<TResult>( object obj ) => default ( TResult );

    /// <summary>How to serialize here...
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public object Serialize( object obj ) => "AAA";
}
```

And then, you can register your **YourSerializer** into **SerializerFactory** like this:

```csharp
var name = "YourSerializer";
var r = SerializerFactory.Register( name, new YourSerializer() );
Assert.IsTrue( r.IsSuccess );

var serializer = SerializerFactory.Create( name );
Assert.IsNotNull( serializer );

var s = serializer.Serialize( new object() );
Assert.AreEqual( "AAA", s );
```

### Logging module example.

Add **LoggingSettings** section in your app.config or web.config file.

```xml
<LoggingSettings eventLogEnable="false" logEnable="true">
    <DefaultTextLogger>
        <add name="_DefaultLog" enable="true" maxSize="8" consoleOutput="true">
            <LogFile path="D:\Logs\Zayni\Logging.TestLog.log"/>
            <!--<Filter filterType="deny" category="Information,Warning" />-->
        </add>
        <add name="TextLogWithoutEmail" enable="true" maxSize="5">
            <LogFile path="D:\Logs\Zayni\Zayni.UnitTest.log"/>
            <!--<Filter filterType="deny" category="Information,Warning" />-->
        </add>
        <add name="TextLogWithEmail" enable="true" maxSize="5" consoleOutput="true" emailNotifyLoggerName="EMailSmtpNotify1" dbLoggerName="ZayniLogging" dbLoggerType="MySQL">
            <LogFile path="D:\Logs\Zayni\Zayni.UnitTest.log"/>
            <!--<Filter filterType="deny" category="Information,Warning" />-->
        </add>
    </DefaultTextLogger>
    <EmailNotifyLogger>
        <add name="EMailSmtpNotify1" smtpPort="587" smtpHost="smtp.gmail.com" enableSsl="true" smtpDomain="" smtpAccount="test@gmail.com" smtpPassword="XXX" fromEmailAddress="system@gmail.com" fromDisplayName="Some Title" toEmailAddress="test@gmail.com"/>
        <add name="EMailSmtpNotify2" smtpPort="25" smtpHost="XXX.XXX.XXX.XXX" enableSsl="false" smtpDomain="" smtpAccount="tester" smtpPassword="Admin123" fromEmailAddress="system@gmail.com" fromDisplayName="Email Logger Test" toEmailAddress="test@gmail.com"/>
    </EmailNotifyLogger>
</LoggingSettings>
```

Then, you can use the **Logging module**.

```csharp
using ZayniFramework.Logging;

static void Main( string[] args )
{
    // Register the error log event handler.
    // When an error log event is fired, your error log event handler will be trigger.
    Logger.Instance.WriteErrorLogEvent += ( sender, message, title ) => 
    {
        Console.WriteLine( $"Ho Ho Ho, this is a error event fired..." );
        Console.WriteLine( $"Error Title: {title}" );
        Console.WriteLine( $"Error Message: {message}" );
    };

    // ===========

    // Use Logger to write a log.
    Logger.WriteInformationLog( nameof ( Program ), "Start console.", Logger.GetTraceLogTitle( nameof ( Program ), nameof ( Main ) ), "TextLogWithEmail" );
    Logger.WriteErrorLog( nameof ( Program ), "Something error!!", "MyTest" );
    Logger.WriteInformationLog( nameof ( Program ), "End console.", Logger.GetTraceLogTitle( nameof ( Program ), nameof ( Main ) ), "TextLogWithEmail" );
    
    // Use ConsoleLogger to output a log to console.
    ConsoleLogger.Log( "This is a console log message", ConsoleColor.DarkGreen );
    ConsoleLogger.LogError( "This is a console log error message" );

    // Write a log to windows event log.
    EventLogger.WriteLog( "ZayniFramework", "Testing", EventLogEntryType.Information, true );
    Console.ReadLine();
}
```

### ExceptionHandling module example.

Add **LoggingSettings** and **ExceptionHandling** section in your app.config or web.config file.

```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <configSections>
        <section name="ZayniFramework" type="ZayniFramework.Common.ZayniConfigSection, ZayniFramework.Common"/>
    </configSections>
    <ZayniFramework>
        <LoggingSettings eventLogEnable="false" logEnable="true">
            <DefaultTextLogger>
                <add name="_DefaultLog" enable="true" maxSize="8" consoleOutput="true">
                    <LogFile path="D:\Logs\Zayni\Logging.TestLog.log"/>
                    <!--<Filter filterType="deny" category="Information,Warning" />-->
                </add>
            </DefaultTextLogger>
        </LoggingSettings>
        <ExceptionHandling>
            <DefaultTextLoggingPolicy enableDefaultPolicy="true" enableDefaultPolicyEventLog="true" needRethrow="false" defaultPolicyLogFilePath="D:\Logs\Zayni\ExceptionHandling.UnitTest.Text.log"/>
            <EMailNotifyPolicy textLogEnable="true" textLogPath="D:\Logs\Zayni\ExceptionHandling.UnitTest.Email.log" eventLogEnable="false" needRethrow="false" emailNotifyEnable="true" smtpPort="587" smtpHost="smtp.gmail.com" enableSsl="true" smtpDomain="" smtpAccount="test@gmail.com" smtpPassword="XXX" fromEmailAddress="system@gmail.com" fromDisplayName="Some Error Happend" toEmailAddress="test@gmail.com" mailSubject="ZayniFramework Exception Handling Unit Test"/>
        </ExceptionHandling>
    </ZayniFramework>
</configuration>
```

Add namespace using

```csharp
using ZayniFramework.ExceptionHandling;
```

Then, you can handle the exception like this:

```csharp
public static void DoSomethingWrong() => throw new ApplicationException( "Opps... Something goes wrong..." );

try
{
    DoSomethingWrong();
}
catch ( Exception ex )
{
    ExceptionAgent.ExecuteDefaultExceptionPolicy( ex, eventTitle: "AAA" );
}

try
{
    DoSomethingWrong();
}
catch ( Exception ex )
{
    ExceptionAgent.ExecuteEMailNotifyExceptionPolicy( ex, eventTitle: "FFF" );
}
```

### Cryptography module example.

Add **CryptographySettings** section in your app.config or web.config file.

```xml
<CryptographySettings symmetricAlgorithmKeyPath="~\ZayniCryptoTest.key">
    <AesEncryptor keySize="128"/>
    <RijndaelEncryptor blockSize="256" keySize="256"/>
    <HashEncryptor needSalt="true"/>
</CryptographySettings>
```

You need to use **zch** dotnet tool CLI application to generate a symmetric algorithm key. See [Source/SDKTools/Zayni.Crypto.Helper](https://gitlab.com/ponylin1985/ZayniFramework/-/tree/master/Source/SDKTools/Zayni.Crypto.Helper)

The **DeaultKey.key**

```
o6iPM3HirEntMdZqd0t5i7yYOTmJrJon8SHfE87SV6ivxKCxrQu2mkJh7A0rGwqYO0lsLscVsRYbO3a4GoRROm1M9ZX37ECYFAWw
BnUGGlKbmhOIlDLr9MPX3q8IPpRKKCVkbA5K9SsAV0SbttOIZnXODw1KfCpQK0HZCdga80ifSF22OWOVQ8jWd6We9kevcSoUirOF
YFTlBGUJTwaXiXibYn6Nyq2j4cgaE6IQ6XeHyCWQbUgvyUYFWLufCPUhCjhYYt4Me9RArQcLHflT96jY3R3sBbDg2w4EUwqhQJGp
IjNE6RVHPs9vEWUrMJ0mglLg92jyqlGgmSfYPhEkX4T5dAAWTmgBmsoWOf53hVQdcR8lpuRe4FEcHF3xrCV345stc9AjcA21dpRB
tOCJXn1m1f2pu8rONYc2UtvU67ZnUNlAnOgQqNjrv2Urk7JJgt8YTieySgDPEDIcsq2VRRWyoj5SP3jxmF6aEDf5jgw9AJdijqys
```

Then you can encrypt or decrypt something like:

```csharp
ISymmetricEncryptor encryptor = SymmetricEncryptorFactory.Create( _encryptMode );
string source = "ABCDEFG_HIJKLMN_OPQRST_UVW_XYZ";
string result = encryptor.Encrypt( source );
string plain  = encryptor.Decrypt( result );
```

You can also use the HASH algorithm like:

```csharp
string userId   = "SomeUserIdHere";
string password = "SomePasswordHere";

IHashEncryptor encryptor = HashEncryptorFactory.Create( "sha256" );
string result = encryptor.HashEncrypt( password, saltKey: userId );
```


### Formatting module example.

Add **Formatting** section in your app.config or web.config file **if you are going to do some DateTime formatting**.

```xml
<FormattingSettings>
    <DateTimeFormatStyle>
        <add language="zh-TW" format="yyyy/MM/dd" isDefault="true"/>
        <add language="zh-CN" format="yyyy/MM/dd"/>
        <add language="en-US" format="MM-dd-yyyy"/>
    </DateTimeFormatStyle>
</FormattingSettings>
```

Then you can add the **Formatting Attribute** in your Properties.

```csharp
using ZayniFramework.Formatting;

public class MasterModel : BasicModel<MasterVModel>
{
    // For deep formatting
    [CollectionFormat()]
    public List<DetailModel> Details { get; set; }
    
    public string Name { get; set; }

    [DateFormat()]
    public DateTime Birthday { get; set; }

    [NumberFormat( Length = 2 )]
    public decimal Age { get; set; }
    
    [CollectionFormat()]
    public DetailInfoModel DetailInfo { get; set; }
}

public class MasterVModel : BasicViewModel
{
    public string Name { get; set; }

    public string Birthday { get; set; }

    public string Age { get; set; }

    // In the ViewModel only suppory 'String' property.
    //public DetailInfoModel DetailInfo { get; set; }
}
```

Finally, you can format the **Model** class like this:

```csharp
// Test data here...
var model = new MasterModel();

model.Name       = "John";
model.Birthday   = new DateTime( 1988, 1, 1 );
model.Age        = 16;
model.DetailInfo = new DetailInfoModel() 
{ 
    InfoName = "JJJ", 
    NAVDate  = new DateTime( 2013, 5, 7 ) 
};
model.Details = new List<DetailModel>() 
{
    new DetailModel 
    {
        DetailName       = "IUIU",
        Length          = 3M,
        Pay             = 45673.52434M,
        NavDate         = new DateTime( 2013, 3, 5 ),
        Name            = "Joe",
        MaskStartIndex  = 1,
        MaskLength      = 1,
        Name2           = "Jimmy"
    },
    new DetailModel 
    {
        DetailName      = "Jack",
        Length          = 2M,
        Pay             = 785.5497M,
        NavDate         = new DateTime( 2013, 7, 5 ),
        Name            = "Kindding",
        MaskStartIndex  = 2,
        MaskLength      = 3,
        Name2           = "Ruby"
    }
};

// do the format here...
var viewModelType = typeof( MasterVModel );
var formatter     = new DataFormatter();
var result        = formatter.DoModelDeepFormat( model, viewModelType );

// get the format result here...
MasterModel  resultModel  = (MasterModel)result;
MasterVModel resultVModel = resultModel.ViewModel;
```

Or you can also use the **Dynamic Formatting**.

Then, you don't have to define a **ViewModel** class anymore. But the dynamic formatting is a little slower than using **ViewModel** formatting. 

```csharp
// Your class will extend the BasicDynamicModel instead of BasicModel
public class MasterModel2 : BasicDynamicModel
```

```csharp
// Use dynamic formatting here...
var formatter = new DataFormatter();
object result = formatter.FormatDynamicModel( model );

// Get the dynamic formatting here...
var     resultModel  = (MasterModel2)result;
dynamic resultVModel = resultModel.ViewModel;  // dynamic type
```
--------------------

## Development Require Softwares & SDKs:

-  Git.
-  Docker and docker-compose.
-  Minikube.
-  Microsoft .NET Core SDK v3.1.100 or higher version.
-  Visual Studio 2017 Version 15.8.6 or higher version.
-  Visual Studio Code and extensions.

    -  [C# for Visual Studio Code (powered by OmniSharp)](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csharp)
    -  [C# Extensions](https://marketplace.visualstudio.com/items?itemName=jchannon.csharpextensions)
    -  [C# FixFormat](https://marketplace.visualstudio.com/items?itemName=Leopotam.csharpfixformat)
    -  [C# Snippets](https://marketplace.visualstudio.com/items?itemName=jorgeserrano.vscode-csharp-snippets)
    -  [C# XML Documentation Comments](https://marketplace.visualstudio.com/items?itemName=k--kato.docomment)
    -  [Super Sharp (C# extension)](https://marketplace.visualstudio.com/items?itemName=craigthomas.supersharp)
    -  [MSBuild project tools](https://marketplace.visualstudio.com/items?itemName=tintoy.msbuild-project-tools)
    -  [.NET Core Tools](https://marketplace.visualstudio.com/items?itemName=formulahendry.dotnet)
    -  [.NET Core Test Explorer](https://marketplace.visualstudio.com/items?itemName=formulahendry.dotnet-test-explorer)
    -  [Nuget Package Manager](https://marketplace.visualstudio.com/items?itemName=jmrog.vscode-nuget-package-manager)
    -  [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker)
    -  [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client)

-  Databases
   -  [SQL Server 2017 or 2019](https://gitlab.com/ponylin1985/MyDockerContainers/-/tree/master/mssql)
   -  [MySQL 5.6 or higher version](https://gitlab.com/ponylin1985/MyDockerContainers/-/tree/master/mysql)
   -  [PostgreSQL 12.1 or higher version](https://gitlab.com/ponylin1985/MyDockerContainers/-/tree/master/postgresql)
   -  [Redis 5.0.7 or higher version](https://gitlab.com/ponylin1985/MyDockerContainers/-/tree/master/redis)
   -  [Elasticsearch 7.1.0 or higher version](https://gitlab.com/ponylin1985/MyDockerContainers/-/tree/master/elasticsearch)

## See more sample code

You can `git clone` the project and see all the unit tests and test applications in [./Test](https://gitlab.com/ponylin1985/ZayniFramework/-/tree/master/Test) directory. There are many sample codes and sample app.config and json config files in the [./Test](https://gitlab.com/ponylin1985/ZayniFramework/-/tree/master/Test) directory.