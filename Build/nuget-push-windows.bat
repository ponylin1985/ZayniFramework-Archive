@echo off
dotnet nuget push *.nupkg --source https://api.nuget.org/v3/index.json -k %1

rem cd to the sln root directory
rem del /s /q *.nupkg