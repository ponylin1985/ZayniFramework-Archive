#!/bin/bash
#! In macOS, this 'dotnet nuget push' can not publish all the nupkg to server. It only push ONE nupkg to server each time... What the fuck!!!
#! dotnet nuget push *.nupkg --source https://api.nuget.org/v3/index.json -k {apiKey}

version=${1:-2.14.122}
echo ${version}

apiKey=${2}
# echo ${apiKey}

dotnet nuget push ZayniFramework.Caching.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Caching.RedisClient.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Common.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Caching.RemoteCache.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Compression.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Cryptography.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.DataAccess.LightORM.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.DataAccess.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.ExceptionHandling.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Formatting.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Logging.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Serialization.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Validation.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.RabbitMQ.Msg.Client.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}

dotnet nuget push ZayniFramework.Middle.Service.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Middle.Service.Client.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Middle.Service.Entity.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Middle.Service.Grpc.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Middle.Service.Grpc.Client.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Middle.Service.Grpc.Entity.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Middle.Service.HttpCore.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Middle.Service.HttpCore.Client.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Middle.Service.HttpCore.Entity.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push ZayniFramework.Middle.TelnetService.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}

dotnet nuget push zch.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}
dotnet nuget push zdm.${version}.nupkg --source https://api.nuget.org/v3/index.json -k ${apiKey}


#! cd to the sln root directory
#! find . -type f -name '*.nupkg' -delete