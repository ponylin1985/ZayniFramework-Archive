#!/bin/bash
# 注意，要手動在 terminal 執行 docker build 指令，需要在 ZayniFramework 專案的根目錄下執行

runtime=${1:-alpine}
echo dotnet publish target runtime OS: $runtime

# 建置 Remote.ShareData.Service 遠端快取服務的 docker image
docker build --no-cache --file ./Docker/zayni.remote.sharedata.service/dockerfile . -t zayni.sharedata.service:$runtime-x64

# 建置 WebAPI.Test 專案的 docker image
docker build --no-cache --file ./Docker/api-service-host/dockerfile . -t webapi.test:$runtime-x64

# 建置 ServiceHost.Client.App 專案的 docker image
docker build --no-cache --file ./Docker/service-host-clientside/dockerfile . -t zayni.client.app:$runtime-x64

# 建置 ServiceHost.ServerSide.App 專案的 docker image
# docker build --no-cache --file ./Docker/service-host-serverside/dockerfile . -t zayni.server.app:$runtime-x64

# ====================

# 建置 Remote.ShareData.Service 遠端快取服務的 docker image
# docker build --no-cache --file ./Docker/zayni.remote.sharedata.service/Dockerfiles/ubuntu-x64.dockerfile . -t zayni.sharedata.service:ubuntu-x64

# 建置 WebAPI.Test 專案的 docker image
# docker build --no-cache --file ./Docker/api-service-host/Dockerfiles/ubuntu-x64.dockerfile . -t webapi.test:ubuntu-x64

# 建置 ServiceHost.Client.App 專案的 docker image
# docker build --no-cache --file ./Docker/service-host-clientside/Dockerfiles/ubuntu-x64.dockerfile . -t zayni.client.app:ubuntu-x64

# 建置 ServiceHost.ServerSide.App 專案的 docker image
# # docker build --no-cache --file ./Docker/service-host-serverside/Dockerfiles/ubuntu-x64.dockerfile . -t zayni.server.app:ubuntu-x64